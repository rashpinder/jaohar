package jaohar.com.jaohar.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.NewRequestActivity;
import jaohar.com.jaohar.activities.chat_module.ChatMessagesListActivity;
import jaohar.com.jaohar.activities.forum_module.ChatScreenForumActivity;
import jaohar.com.jaohar.activities.forum_module.UKOfficeChatScreenForumActivity;
import jaohar.com.jaohar.utils.JaoharConstants;

/**
 * Created by Dharmani Apps on 1/12/2018.
 */

public class JaoharFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = "FirebaseMessagingService";
    PendingIntent contentIntent;
    String strType = "", title = "", message = "", forumId = "", roomID,group_name="",group_id="",image="";
    Intent intent;
    private static int NOTIFICATION_ID = 6578;
    Bitmap bitmap;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData() != null) {
            title = remoteMessage.getData().get("title");
            message = remoteMessage.getData().get("body");
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        sendNotification(title, message, remoteMessage);

    }

    private void sendNotification(String title, String message, RemoteMessage remoteMessage) {
        JSONObject jsonObject2 = null;

        String resultResponse = String.valueOf(remoteMessage.getData().get("notificationData"));
        try {
            jsonObject2 = new JSONObject(resultResponse);

            if (!jsonObject2.isNull("notification_type")) {
                strType = jsonObject2.getString("notification_type");
            }

            if (!jsonObject2.isNull("image_url") && !jsonObject2.getString("image_url").equals("")) {
                bitmap = getBitmapFromURL(jsonObject2.getString("image_url").trim());
            }

            if (!jsonObject2.isNull("room_id")) {
                roomID = jsonObject2.getString("room_id");
            }

            if (!jsonObject2.isNull("forum_id")) {
                forumId = jsonObject2.getString("forum_id");
            }

            if (!jsonObject2.getString("group_id").equals(""))
                group_id=jsonObject2.getString("group_id");

            if (!jsonObject2.getString("image").equals(""))
                image=jsonObject2.getString("image");

            if (!jsonObject2.getString("name").equals(""))
                group_name= jsonObject2.getString("name");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (strType != null) {
            if (strType.equals("mass")) {
                JaoharConstants.Is_click_form_Manager = true;
                intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(JaoharConstants.LOGIN, "mass_push");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);
                }
                else{
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
                }
//                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            } else if (strType.equals("signup")) {
                JaoharConstants.Is_click_form_Push = true;
                intent = new Intent(this, NewRequestActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);
                }
                else{
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
                }
            } else if (strType.equals("chat")) {
                JaoharConstants.Is_click_form_Push = true;
                intent = new Intent(this, ChatMessagesListActivity.class);
                if (group_id!=null && !group_id.equals("")){
                intent.putExtra("group_id",group_id);
                intent.putExtra("group_name",group_name);
                intent.putExtra(JaoharConstants.RECEIVER_PIC,image);}
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(JaoharConstants.NOTIFICATION_TYPE, JaoharConstants.PUSH);
                JSONObject jsonObject;

                String Full_name = "", First_name = "", Last_name = "";
                String resultResponseDetails;

                try {
                    resultResponseDetails = String.valueOf(jsonObject2.getJSONObject("sender_detail"));
                    jsonObject = new JSONObject(resultResponseDetails);

                    if (!jsonObject.getString("id").equals(""))
                        intent.putExtra(JaoharConstants.RECEIVER_ID, jsonObject.getString("id"));

                    if (!jsonObject.getString("room_id").equals(""))
                        intent.putExtra(JaoharConstants.ROOM_ID, jsonObject.getString("room_id"));

                    if (!jsonObject.getString("first_name").equals("")) {
                        Full_name = jsonObject.getString("first_name");
                    }

                    if (group_id==null || group_id.equals("") || group_id.equals("null")) {
                        if (!jsonObject.getString("first_name").equals("") && !jsonObject.getString("last_name").equals("")) {
                            Full_name = jsonObject.getString("first_name") + " " + jsonObject.getString("last_name");
                            intent.putExtra(JaoharConstants.USER_NAME, Full_name);
                        }
                        if (!jsonObject.getString("first_name").equals("")) {
                            First_name = jsonObject.getString("first_name");
                            intent.putExtra(JaoharConstants.USER_FIRST_NAME, First_name);
                        }
                        if (!jsonObject.getString("last_name").equals("")) {
                            Last_name = jsonObject.getString("last_name");
                            intent.putExtra(JaoharConstants.USER_LAST_NAME, Last_name);
                        }
                    }
//                    if (!jsonObject.getString("name").equals(""))
//                        intent.putExtra(JaoharConstants.USER_NAME, jsonObject.getString("name"));


                    if (!jsonObject.getString("image").equals("")) {
                        intent.putExtra(JaoharConstants.RECEIVER_PIC, jsonObject.getString("image"));
                    }
                    if (!jsonObject.getString("online_state").equals("")) {
                        intent.putExtra(JaoharConstants.ONLINE_STATE, jsonObject.getString("online_state"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);
                }
                else{
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
                }

            } else if (strType.equals("staff forum")) {
                JaoharConstants.Is_click_form_Push = true;
                intent = new Intent(this, ChatScreenForumActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("forum_id", forumId);
                intent.putExtra("room_id", roomID);
                intent.putExtra(JaoharConstants.NOTIFICATION_TYPE, JaoharConstants.PUSH);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);
                }
                else{
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
                }
//                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            } else if (strType.equals("forum") || strType.equals("uk_forum")) {
                JaoharConstants.Is_click_form_Push = true;
                intent = new Intent(this, UKOfficeChatScreenForumActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("forum_id", forumId);
                intent.putExtra("room_id", roomID);
                intent.putExtra(JaoharConstants.NOTIFICATION_TYPE, JaoharConstants.PUSH);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);
                }
                else{
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
                }
//                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            } else {
                intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(JaoharConstants.LOGIN, "mass_push");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);
                }
                else{
                    contentIntent = PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
                }
//                contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
            }
        }
        PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
             pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                    intent,
                    PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);
        }
        else{
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
             pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
        }
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
//                intent,
//                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_notification_icon)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_HIGH).setDefaults(NotificationCompat.DEFAULT_ALL);

        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();

        if (bitmap != null) {
            bigPicStyle.bigPicture(bitmap);
            notificationBuilder.setStyle(bigPicStyle);
            bitmap=null;
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            } else {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            }
        }
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
