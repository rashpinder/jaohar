package jaohar.com.jaohar.fragments;

/**
 * Created by dharmaniz on 7/11/17.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.DetailsActivity;
import jaohar.com.jaohar.activities.EditVesselActivity;
import jaohar.com.jaohar.activities.GalleryActivity;
import jaohar.com.jaohar.activities.SearchVesselsActivity;
import jaohar.com.jaohar.activities.SendingMailActivity;
import jaohar.com.jaohar.activities.mail_list_all_activities.SendingMailToLIst_Vessels;
import jaohar.com.jaohar.activities.vessels_module.CopyVesselActivity;
import jaohar.com.jaohar.adapters.UserNewsAdapter;
import jaohar.com.jaohar.adapters.VesselesForSaleAdapter;
import jaohar.com.jaohar.beans.VessalClassModel;
import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.models.GetVesselClassData;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.SearchModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.VesselForSaleModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class VesselsForSaleFragment extends Fragment {

    String TAG = VesselsForSaleFragment.this.getClass().getSimpleName();

    /* widgets */
    View rootView;
    RelativeLayout downArrowRL, resetRL1;
    EditText editSearchET;
    ImageView linkIMG;
    TextView text;
    RecyclerView vesselsRV;
    SwipeRefreshLayout swipeToRefresh;
    VesselesForSaleAdapter mVesselesAdapter;

    public String strCapacityFrom, strCapacityTo, strName, strLoaFrom, strLoaTo, strType, strGrainFrom,
            strGrainTo, strYearofBuildFrom, strClass, strYearofBuildTo, strBaleFrom, strBaleTo, strStatus,
            strGeared, strRecordID, IMO_number,strPhotoURL = "", strTExt, strNormalText, strPhotoUrlDEs = "", strTEUFrom,
            strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo, strLiquidFrom, strLiquidTo, strPlaceBuild, strOffMarket = "";
    public String strLastPage = "FALSE", strTextNEWS = "";

    ArrayList<AllVessel> mArrayList = new ArrayList<>();
    ArrayList<AllVessel> loadMoreArrayList = new ArrayList<>();
    boolean isSwipeRefresh = false;
    String strIsBack = "";
    int page_no = 1;
    boolean isAdvanceSearch = false;
    boolean isNormalSearch = false;
    ArrayList<GetVesselTypeData> modelArrayList = new ArrayList<GetVesselTypeData>();
    ArrayList<GetVesselClassData> mArrayListClass = new ArrayList<GetVesselClassData>();
    String[] arrayStatus;
    String[] typeArray;
    String strNews;
    ArrayList<String> mImageArryList = new ArrayList<>();
    ProgressBar progressBottomPB;

    RecyclerView newsRV;
    UserNewsAdapter mNewsAdapter;
    boolean isOpenDialogBOX = false;
    String strSearchText, UserId = "";

    //for pagination
    boolean isScrolling = false;
    LinearLayoutManager linearLayoutManager;
    NestedScrollView nestedScrollView;

    ArrayList<Datum> mNewsArrayList = new ArrayList<Datum>();

    OpenNewsPopUpInterFace mOpenNewsPopUP = new OpenNewsPopUpInterFace() {
        @Override
        public void openNewsPopUpInterFace(String strWebTxt, String strPhotoURL) {
            if (isOpenDialogBOX == true) {
                isOpenDialogBOX = false;
            } else {
                setUpNewsDialog(strWebTxt, strPhotoURL);
            }
        }
    };

    public VesselsForSaleFragment() {
        // Required empty public constructor
    }

    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                if (isAdvanceSearch == false) {
                    if (isNormalSearch == false) {

                        progressBottomPB.setVisibility(View.VISIBLE);

                        ++page_no;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (strLastPage.equals("FALSE")) {
                                    executeAPI(page_no, UserId);
                                } else {
                                    progressBottomPB.setVisibility(View.GONE);
                                }
                            }
                        }, 1000);
                    }
                }
            }
        }
    };

    OpenPopUpVesselInterface mOpenPoUpInterface = new OpenPopUpVesselInterface() {
        @Override
        public void mOpenPopUpVesselInterface(String strTextnews, String strPhotoURLs) {
            JaoharConstants.is_VessalsForSale = true;
            setUpNewsDialog(strNews, strPhotoUrlDEs);
        }
    };

    FavoriteVesselInterface mFavoriteVesselInterface = new FavoriteVesselInterface() {
        @Override
        public void mFavoriteVesselInterface(int position, String status, ImageView imageView) {
            String vessel_id = mArrayList.get(position).getRecordId();
            if (status.equals("1")) {
                executeFavoriteAPI(vessel_id, imageView, UserId);
            } else if (status.equals("0")) {
                executeUnFavoriteAPI(vessel_id, imageView, UserId);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            strIsBack = getArguments().getString(JaoharConstants.STR_BACK);
        }
        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_vessel_for_sale, container, false);

        JaoharSingleton.getInstance().setSearchedVessel("");

        typeArray = getActivity().getResources().getStringArray(R.array.vessel_type_array_advanced);
        arrayStatus = getActivity().getResources().getStringArray(R.array.status_array_user);
        mArrayList = JaoharSingleton.getInstance().getVessaelesArrayList();
        if (strIsBack.equals(JaoharConstants.STR_BACK)) {
            JaoharConstants.IS_BACK = true;
            Utilities.hideKeyBoad(getActivity(), rootView);
        }

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.ic_magnifying_glass);

        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {

            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
            } else {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "");
            }

        } else {
            UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
        }

        setWidgetIDs(rootView);
        setClickListner();

        JaoharConstants.BOOLEAN_USER_LOGOUT = true;
        JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isAdvanceSearch = false;
                isNormalSearch = false;
                page_no = 1;
                editSearchET.setText("");
                mArrayList.clear();
                loadMoreArrayList.clear();
                executeAPI(page_no, UserId);
            }
        });

        HomeActivity.txtCenter.setText("Vessels For Sale");

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mArrayList != null)
            mArrayList.clear();

        if (loadMoreArrayList != null)
            loadMoreArrayList.clear();

        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {

            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
            } else {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "");
            }

        } else {
            UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
        }

        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //execute API
            page_no = 1;
            executeGettingAllUserNews();
        }
    }

    private void setWidgetIDs(View v) {
        nestedScrollView = v.findViewById(R.id.nestedScrollView);
        progressBottomPB = v.findViewById(R.id.progressBottomPB);
        vesselsRV = v.findViewById(R.id.vesselsRV);
        editSearchET = v.findViewById(R.id.editSearchET);
        swipeToRefresh = v.findViewById(R.id.swipeToRefresh);
        text = v.findViewById(R.id.text);
        downArrowRL = v.findViewById(R.id.downArrowRL);
        resetRL1 = v.findViewById(R.id.resetRL1);
        linkIMG = v.findViewById(R.id.linkIMG);
        newsRV = v.findViewById(R.id.newsRV);
    }

    private void setAdapter() {
        vesselsRV.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        vesselsRV.setLayoutManager(linearLayoutManager);
        mVesselesAdapter = new VesselesForSaleAdapter(getActivity(), mArrayList, strTextNEWS,
                strNews, strPhotoURL, mPagination, mOpenPoUpInterface, "true", new OnClickInterface() {
            @Override
            public void mOnClickInterface(int position) {
                PerformOptionsClick(position);
            }
        }, mFavoriteVesselInterface);
        vesselsRV.setAdapter(mVesselesAdapter);

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {

                    if (strLastPage.equals("FALSE")) {
                        progressBottomPB.setVisibility(View.VISIBLE);
                        ++page_no;
                    }

                    // your pagination code
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                executeAPI(page_no, UserId);
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 1000);
                }
            }
        });
    }

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_vessels_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout shareRL = view.findViewById(R.id.shareRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailToListRL = view.findViewById(R.id.mailToListRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);
        View shareView = view.findViewById(R.id.shareView);
        View editView = view.findViewById(R.id.editView);
        View copyView = view.findViewById(R.id.copyView);
        View mailToListView = view.findViewById(R.id.mailToListView);
        ImageView mailImg = view.findViewById(R.id.mailImg);

        shareRL.setVisibility(View.GONE);
        editRL.setVisibility(View.GONE);
        copyRL.setVisibility(View.GONE);
        mailToListRL.setVisibility(View.GONE);
        shareView.setVisibility(View.GONE);
        editView.setVisibility(View.GONE);
        copyView.setVisibility(View.GONE);
        mailToListView.setVisibility(View.GONE);

        mailImg.setImageResource(R.drawable.ic_email_);

        shareRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                shareViaWhatsApp();
            }
        });

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), EditVesselActivity.class);
                mIntent.putExtra("Model", mArrayList);
                mIntent.putExtra("Title", "Edit Vessels");
                startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), CopyVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.Is_click_form_All_Vessals = true;
                Intent mIntent = new Intent(getActivity(), DetailsActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("isEditShow", true);
                startActivity(mIntent);
            }
        });

        mailToListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IsAllVesselsClick = true;
                Intent mIntent = new Intent(getActivity(), SendingMailToLIst_Vessels.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), SendingMailActivity.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void shareViaWhatsApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra("Intent.EXTRA_SUBJECT", "Jaohar");
            String shareMessage = "\nSee this Video\n";
            shareMessage = "https://play.google.com/store/apps/details?id=jaohar.com.jaohar&hl=en_IN&gl=US";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void setClickListner() {
        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    executeNormalSearch(UserId);
                    return true;
                }
                return false;
            }
        });

        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gettingVessal_Type();
                gettingVessal_Class();
                advancedSearchView(typeArray);
            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAdvanceSearch = false;
                isNormalSearch = false;
                mArrayList.clear();
                loadMoreArrayList.clear();
                editSearchET.setText("");
                page_no = 1;
                executeAPI(page_no, UserId);
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.is_VessalsForSale = true;
                if (getActivity() != null)
                    startActivity(new Intent(getActivity(), SearchVesselsActivity.class)
                            .putExtra(JaoharConstants.SearchType, JaoharConstants.VesselForSaleType));
            }
        });
    }

    public void setUpNewsDialog(String strNormalTEXT, final String strPhotoURL) {
        mImageArryList.clear();
        final Dialog searchDialog = new Dialog(getActivity());
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.activity_full_news_description);
        searchDialog.setCanceledOnTouchOutside(true);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        ImageView add_image1 = searchDialog.findViewById(R.id.add_image1);
        WebView showTXT = searchDialog.findViewById(R.id.showTXT);
        final ProgressBar progressbar1 = searchDialog.findViewById(R.id.progressbar1);
        progressbar1.setVisibility(View.VISIBLE);
        if (!strPhotoURL.equals("")) {
            Glide.with(getActivity()).load(strPhotoURL).into(add_image1);
        }
        add_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!strPhotoURL.equals("")) {
                    mImageArryList.add(strPhotoURL);
                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putStringArrayListExtra("LIST", mImageArryList);
                    startActivity(intent);
                }
            }
        });
        showTXT.getSettings().setJavaScriptEnabled(true);
        showTXT.getSettings().setLoadWithOverviewMode(true);
        showTXT.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressbar1.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progressbar1.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressbar1.setVisibility(View.GONE);
            }
        });

        showTXT.loadDataWithBaseURL(null, strNormalTEXT, "text/html", "UTF-8", null);
        WebSettings webSettings = showTXT.getSettings();
        Resources res = getActivity().getResources();
        webSettings.setDefaultFontSize((int) res.getDimension(R.dimen._3sdp));
        searchDialog.show();
    }

    private void executeNormalSearch(String strUserId) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.searchVesselRequest(strSearchText, "user", strUserId);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                isNormalSearch = true;
                SearchModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mArrayList.clear();
                    mArrayList = mModel.getAllSearchedVessels();
                    /*SetAdapter*/
                    setAdapter();

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    public void executeAPI(int page_no, String strUserId) {
        isScrolling = true;

        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<VesselForSaleModel> call1 = mApiInterface.getAllVesselsRequest(String.valueOf(page_no), strUserId);
        call1.enqueue(new Callback<VesselForSaleModel>() {
            @Override
            public void onResponse(Call<VesselForSaleModel> call, retrofit2.Response<VesselForSaleModel> response) {
                Log.e(TAG, "******Response*****" + response);
                isScrolling = false;
                AlertDialogManager.hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);
                resetRL1.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                loadMoreArrayList.clear();
                VesselForSaleModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    strLastPage = mModel.getData().getLastPage();

                    if (page_no == 1) {
                        mArrayList = mModel.getData().getAllVessels();
                    } else if (page_no > 1) {
                        loadMoreArrayList = mModel.getData().getAllVessels();
                    }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    }

                    if (page_no == 1) {

                        /*SetAdapter*/
                        setAdapter();
                    } else {
                        mVesselesAdapter.notifyDataSetChanged();
                    }
                } else {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<VesselForSaleModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public void advancedSearchView(String[] vesselTypesArray) {
        final Dialog searchDialog = new Dialog(getActivity());
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advanced_search);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editRecordIDET = searchDialog.findViewById(R.id.editRecordIDET);
        final EditText editFromCapacityET = searchDialog.findViewById(R.id.editFromCapacityET);
        final EditText editToCapacityET = searchDialog.findViewById(R.id.editToCapacityET);
        final EditText editNameET = searchDialog.findViewById(R.id.editNameET);
        final EditText editFromLoaET = searchDialog.findViewById(R.id.editFromLoaET);
        final EditText editToLoaET = searchDialog.findViewById(R.id.editToLoaET);
        final EditText editFromGrainET = searchDialog.findViewById(R.id.editFromGrainET);
        final EditText editToGrainET = searchDialog.findViewById(R.id.editToGrainET);
        final EditText editFromYearBuiltET = searchDialog.findViewById(R.id.editFromYearBuiltET);
        final EditText editToYearBuiltET = searchDialog.findViewById(R.id.editToYearBuiltET);
        final EditText editFromBaleET = searchDialog.findViewById(R.id.editFromBaleET);
        final EditText editToBaleET = searchDialog.findViewById(R.id.editToBaleET);
        final EditText editStatusET = searchDialog.findViewById(R.id.editStatusET);
        final Spinner spinnerGeared = searchDialog.findViewById(R.id.spinnerGeared);
        final EditText editFromTEUET = searchDialog.findViewById(R.id.editFromTEUET);
        final EditText editToTEUET = searchDialog.findViewById(R.id.editToTEUET);
        final EditText editFromPassengerET = searchDialog.findViewById(R.id.editFromPassengerET);
        final EditText editToPassendgerET = searchDialog.findViewById(R.id.editToPassendgerET);
        final EditText editFromDraftrET = searchDialog.findViewById(R.id.editFromDraftrET);
        final EditText editToDraftET = searchDialog.findViewById(R.id.editToDraftET);
        final EditText editFromLiquidET = searchDialog.findViewById(R.id.editFromLiquidET);
        final EditText editToLiquidET = searchDialog.findViewById(R.id.editToLiquidET);
        final EditText editTypeET = searchDialog.findViewById(R.id.editTypeET);
        final EditText editClassET = searchDialog.findViewById(R.id.editClassET);
        final EditText editPlaceBuiltET = searchDialog.findViewById(R.id.editPlaceBuiltET);
        final EditText editOffMarketET = searchDialog.findViewById(R.id.editOffMarketET);
        final EditText editGearedET = searchDialog.findViewById(R.id.editGearedET);
        final EditText editIMONumET = searchDialog.findViewById(R.id.editIMONumET);

        List<String> list = new ArrayList<String>();
        list.add("Select Geared");
        list.add("Yes");
        list.add("No");
        ArrayAdapter<String> mSpAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        mSpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGeared.setAdapter(mSpAdapter);
        spinnerGeared.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strGeared = adapterView.getItemAtPosition(i).toString();
                if (strGeared.equals("Select Geared")) {
                    strGeared = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editStatusET.setKeyListener(null);
        editStatusET.setCursorVisible(false);
        final TextView txtHeaderStatus = searchDialog.findViewById(R.id.txtHeaderStatus);
        final LinearLayout layoutNameLL = searchDialog.findViewById(R.id.layoutNameLL);
        final LinearLayout layoutPlaceBuiltLL = searchDialog.findViewById(R.id.layoutPlaceBuiltLL);
        final LinearLayout layoutRecordIDLL = searchDialog.findViewById(R.id.layoutRecordIDLL);
        final LinearLayout layoutStatusLL = searchDialog.findViewById(R.id.layoutStatusLL);

        layoutNameLL.setVisibility(View.VISIBLE);
        layoutPlaceBuiltLL.setVisibility(View.VISIBLE);
        layoutRecordIDLL.setVisibility(View.VISIBLE);

        layoutStatusLL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(getActivity(), "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        txtHeaderStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(getActivity(), "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });
        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(getActivity(), "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editTypeET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Type");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<GetVesselTypeData> adapter = new ArrayAdapter<GetVesselTypeData>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, modelArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strType = modelArrayList.get(position).getName();
                            editTypeET.setText(strType);
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editClassET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Class");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    // Define a new Adapter
                    // First parameter - Context
                    // Second parameter - Layout for the row
                    // Third parameter - ID of the TextView to which the data is written
                    // Forth - the Array of data

                    ArrayAdapter<GetVesselClassData> adapter = new ArrayAdapter<GetVesselClassData>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListClass);

                    // Assign adapter to ListView
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strClass = mArrayListClass.get(position).getClassName();
                            editClassET.setText(strClass);
                            categoryDialog.dismiss();

                        }
                    });

                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchDialog.dismiss();
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strCapacityFrom = editFromCapacityET.getText().toString();
                strCapacityTo = editToCapacityET.getText().toString();

                strName = editNameET.getText().toString();

                strLoaFrom = editFromLoaET.getText().toString();
                strLoaTo = editToLoaET.getText().toString();


                strGrainFrom = editFromGrainET.getText().toString();
                strGrainTo = editToGrainET.getText().toString();

                strYearofBuildFrom = editFromYearBuiltET.getText().toString();
                strYearofBuildTo = editToYearBuiltET.getText().toString();

                strBaleFrom = editFromBaleET.getText().toString();
                strBaleTo = editToBaleET.getText().toString();

                strStatus = editStatusET.getText().toString();

                strRecordID = editRecordIDET.getText().toString();
                IMO_number = editIMONumET.getText().toString();
                strTEUFrom = editFromTEUET.getText().toString();
                strTEUto = editToTEUET.getText().toString();
                strDraftsTo = editToDraftET.getText().toString();
                strDraftsFrom = editFromDraftrET.getText().toString();
                strPassangerFrom = editFromPassengerET.getText().toString();
                strPassangerTo = editToPassendgerET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();
                strLiquidFrom = editFromLiquidET.getText().toString();

                strPlaceBuild = editPlaceBuiltET.getText().toString();

                strGeared = editGearedET.getText().toString();
                strOffMarket = editOffMarketET.getText().toString();

                if (Utilities.isNetworkAvailable(getActivity()) == false) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchAPI(searchDialog, UserId);
                }
            }
        });
        searchDialog.show();
    }

    private void gettingVessal_Type() {
        if (strType == null) {
            strType = "";
        }
        if (strClass == null) {
            strClass = "";
        }
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
                          @Override
                          public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselTypeModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  modelArrayList = mModel.getData();
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    public void executeAdvanceSearchAPI(final Dialog mDialog, String strUserId) {
        mArrayList.clear();
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.advanceSearchVesselRequest(strCapacityFrom, strCapacityTo, strLoaFrom, strLoaTo,
                strType, strYearofBuildFrom, strYearofBuildTo, strGrainFrom, strGrainTo, strBaleFrom, strName, strRecordID, IMO_number,strBaleTo, strStatus,
                strGeared, "staff", strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo,
                strLiquidFrom, strLiquidTo, strClass, strPlaceBuild, strUserId, strOffMarket);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                /*Paras Search Data*/
                isAdvanceSearch = true;
                SearchModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mDialog.dismiss();
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mArrayList.clear();
                    mArrayList = mModel.getAllSearchedVessels();

                    /*SetAdapter*/
                    setAdapter();

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus().equals("0")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void gettingVessal_Class() {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselClassModel> call1 = mApiInterface.getVesselClassRequest();
        call1.enqueue(new Callback<GetVesselClassModel>() {
                          @Override
                          public void onResponse(Call<GetVesselClassModel> call, retrofit2.Response<GetVesselClassModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselClassModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  mArrayListClass = mModel.getData();
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselClassModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }


    private void setUserNewsAdapter() {
        newsRV.setNestedScrollingEnabled(false);
        newsRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mNewsAdapter = new UserNewsAdapter(getActivity(), mNewsArrayList, mOpenNewsPopUP);
        newsRV.setAdapter(mNewsAdapter);
    }

    private void executeGettingAllUserNews() {
        mNewsArrayList.clear();
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AllNewsModel> call1 = mApiInterface.getallNewsRequest();
        call1.enqueue(new Callback<AllNewsModel>() {
                          @Override
                          public void onResponse(Call<AllNewsModel> call, retrofit2.Response<AllNewsModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              AllNewsModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  mNewsArrayList = mModel.getData();
                                  executeAPI(page_no, UserId);

                                  /*setAdapter*/
                                  setUserNewsAdapter();

                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<AllNewsModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    /* *
     * Execute Favorite API for vessel
     * @param
     * @user_id
     * */
    public void executeFavoriteAPI(String vessel_id, final ImageView imageView, String strUserId) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.makeFavoriteVesselsRequest(vessel_id, strUserId);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_yellow);
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.the_vessel_has_been_fav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /* *
     * Execute UnFavorite API for vessel
     * @param
     * @user_id
     * */
    public void executeUnFavoriteAPI(String vessel_id, final ImageView imageView, String strUserId) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.unFavoriteVesselsRequest(vessel_id, strUserId);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_border);
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }
}


