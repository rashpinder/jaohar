package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static android.content.Context.DOWNLOAD_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.InvoiceDetailsActivity;
import jaohar.com.jaohar.activities.SendingInvoiceEmailActivity;
import jaohar.com.jaohar.activities.invoices_module.AddPendingPaymentsActivity;
import jaohar.com.jaohar.activities.invoices_module.InvoiceEmailActivity;
import jaohar.com.jaohar.activities.invoices_module.PendingPaymentDetailsActivity;
import jaohar.com.jaohar.activities.invoices_module.SearchInvoiceActivity;
import jaohar.com.jaohar.adapters.invoices_module.PendingPaymentsAdapter;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.pendingPayments.AllInvoice;
import jaohar.com.jaohar.models.pendingPayments.PendingPaymentData;
import jaohar.com.jaohar.models.pendingPayments.PendingPaymentModels;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;


public class PendingPaymentsFragment extends BaseFragment {

    static int IsInvoiceActive = 0;
    private static int page_no = 1;
    public final int REQUEST_PERMISSIONS = 1;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    String TAG = PendingPaymentsFragment.this.getClass().getSimpleName();
    RelativeLayout imgRightLL, downArrowRL, resetRL1;
    SwipeRefreshLayout swipeToRefresh;
    boolean isSwipeRefresh = false;
    boolean isNormalSearch = false, isAdvanceSearch = false;
    ProgressBar progressBottomPB;
    NestedScrollView pendingNestedScroll;
    RecyclerView pendingPaymentsRV;
    PendingPaymentsAdapter pendingPaymentsAdapter;
    ArrayList<AllInvoice> modelArrayList = new ArrayList<>();
    PendingPaymentData pendingPaymentData;
    ArrayList<AllInvoice> mLoadMore = new ArrayList<>();
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    String mPDF = "", mPDFName = "";
    SendMultipleInvoiceMAIlInterface mMultimaIlInterface = new SendMultipleInvoiceMAIlInterface() {
        @Override
        public void mSendMutliInvoice(InVoicesModel mInVoivce, boolean mDelete) {
        }
    };
    SinglemailInvoiceInterface msinglemailInvoiceInterface = new SinglemailInvoiceInterface() {
        @Override
        public void mSinglemailInvoice(InVoicesModel mInvoiceModel) {
            strInvoiceNumber = "JAORO" + mInvoiceModel.getInvoice_number();
            strInvoiceVesselName = "   " + mInvoiceModel.getmVesselSearchInvoiceModel().getVessel_name();
            /*Send Email*/
            Intent mIntent = new Intent(getActivity(), SendingInvoiceEmailActivity.class);
            mIntent.putExtra("SubjectName", "Invoice    " + strInvoiceNumber + strInvoiceVesselName);
            mIntent.putExtra("vesselID", mInvoiceModel.getInvoice_id());
            startActivity(mIntent);
        }
    };
    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            if (modelArrayList.get(position).getAllData().getAllDocs().size() == 0) {
                PerformOptionsClick(position, "", "");
            } else {
                PerformOptionsClick(position, modelArrayList.get(position).getAllData().getAllDocs().get(0) + "", "");
            }

            for (int j = 0; j < modelArrayList.get(position).getAllData().getAllDocs().size(); j++) {
                mPDF = modelArrayList.get(position).getAllData().getAllDocs().get(j);
                mPDFName = mPDF.substring(mPDF.lastIndexOf("/") + 1);
            }
//            mPDFName = modelArrayList.get(position).getAllData().;
        }
    };
    private String strLastPage = "FALSE", strInvoiceNumber, strInvoiceVesselName;
    private DownloadManager downloadManager;
    private long downloadReference;
    PDFdownloadInterface mPdfDownloader = new PDFdownloadInterface() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void pdfDownloadInterface(final String strPDF, final String strPDFNAme) {
            if (!strPDF.equals("")) {
                // your code
                downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(strPDF);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(strPDFNAme);
                //Set a description of this download, to be displayed in notifications (if enabled)
                request.setDescription("" + "Invoice PDF is Download Please Wait...");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);
            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Their is no PDF");
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pending_payments, container, false);

        setViewsIDs(view);
        listeners();

        setAdapter();
        return view;
    }

//    private void parseResponse(String response) {
//        mLoadMore.clear();
//        try {
//            JSONObject mJsonObject = new JSONObject(response);
//            if (mJsonObject.getString("status").equals("1")) {
//                JSONObject mDataObject = mJsonObject.getJSONObject("data");
//                JSONArray mJsonArray = mDataObject.getJSONArray("all_invoices");
//                strLastPage = mDataObject.getString("last_page");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJson = mJsonArray.getJSONObject(i);
//                    PendingPaymentModels mModel = new PendingPaymentModels();
//                    JSONObject mAllDataObj = mJson.getJSONObject("all_data");
//                    if (!mAllDataObj.isNull("invoice_id"))
//                        mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
//                    if (!mAllDataObj.getString("pdf").equals(""))
//                        mModel.setPdf(mAllDataObj.getString("pdf"));
//                    if (!mAllDataObj.getString("pdf_name").equals(""))
//                        mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
//                    if (!mAllDataObj.isNull("invoice_no"))
//                        mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
//                    if (!mAllDataObj.isNull("invoice_date"))
//                        mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
//                    if (!mAllDataObj.isNull("term_days"))
//                        mModel.setTerm_days(mAllDataObj.getString("term_days"));
//                    if (!mAllDataObj.isNull("currency"))
//                        mModel.setCurrency(mAllDataObj.getString("currency"));
//                    if (!mAllDataObj.isNull("status"))
//                        mModel.setStatus(mAllDataObj.getString("status"));
//                    if (!mAllDataObj.isNull("reference"))
//                        mModel.setRefrence1(mAllDataObj.getString("reference"));
//                    if (!mAllDataObj.isNull("reference1"))
//                        mModel.setRefrence2(mAllDataObj.getString("reference1"));
//                    if (!mAllDataObj.isNull("reference2"))
//                        mModel.setRefrence3(mAllDataObj.getString("reference2"));
//                    if (!mAllDataObj.isNull("payment_id"))
//                        mModel.setPayment_id(mAllDataObj.getString("payment_id"));
//                    if (!mAllDataObj.isNull("inv_state"))
//                        mModel.setInv_state(mAllDataObj.getString("inv_state"));
//
//                    if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
//                        JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
//                        SignatureModel mSignModel = new SignatureModel();
//                        if (!mSignDataObj.isNull("sign_id"))
//                            mSignModel.setId(mSignDataObj.getString("sign_id"));
//                        if (!mSignDataObj.isNull("sign_name"))
//                            mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
//                        if (!mSignDataObj.isNull("sign_image"))
//                            mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
//                        mModel.setmSignatureModel(mSignModel);
//                    }
//                    if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
//                        JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
//                        StampsModel mStampsModel = new StampsModel();
//                        if (!mStampDataObj.isNull("stamp_id"))
//                            mStampsModel.setId(mStampDataObj.getString("stamp_id"));
//                        if (!mStampDataObj.isNull("stamp_name"))
//                            mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
//                        if (!mStampDataObj.isNull("stamp_image"))
//                            mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
//                        mModel.setmStampsModel(mStampsModel);
//                    }
//                    if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
//                        JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
//                        BankModel mBankModel = new BankModel();
//                        if (!mBankDataObj.isNull("bank_id"))
//                            mBankModel.setId(mBankDataObj.getString("bank_id"));
//                        if (!mBankDataObj.isNull("beneficiary"))
//                            mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
//                        if (!mBankDataObj.isNull("bank_name"))
//                            mBankModel.setBankName(mBankDataObj.getString("bank_name"));
//                        if (!mBankDataObj.isNull("address1"))
//                            mBankModel.setAddress1(mBankDataObj.getString("address1"));
//                        if (!mBankDataObj.isNull("address2"))
//                            mBankModel.setAddress2(mBankDataObj.getString("address2"));
//                        if (!mBankDataObj.isNull("iban_ron"))
//                            mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
//                        if (!mBankDataObj.isNull("iban_usd"))
//                            mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
//                        if (!mBankDataObj.isNull("iban_eur"))
//                            mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
//                        if (!mBankDataObj.isNull("swift"))
//                            mBankModel.setSwift(mBankDataObj.getString("swift"));
//                        mModel.setmBankModel(mBankModel);
//                    }
//                    if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
//                        JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
//                        VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
//                        if (!mSearchVesselObj.isNull("vessel_id"))
//                            mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
//                        if (!mSearchVesselObj.isNull("vessel_name"))
//                            mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
//                        if (!mSearchVesselObj.isNull("IMO_no"))
//                            mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
//                        if (!mSearchVesselObj.isNull("flag"))
//                            mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
//                        mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
//                    }
//                    if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
//                        JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
//                        CompaniesModel mCompaniesModel = new CompaniesModel();
//                        if (!mSearchCompanyObj.isNull("id"))
//                            mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
//                        if (!mSearchCompanyObj.isNull("company_name"))
//                            mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
//                        if (!mSearchCompanyObj.isNull("Address1"))
//                            mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
//                        if (!mSearchCompanyObj.isNull("Address2"))
//                            mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
//                        if (!mSearchCompanyObj.isNull("Address3"))
//                            mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
//                        if (!mSearchCompanyObj.isNull("Address4"))
//                            mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
//                        if (!mSearchCompanyObj.isNull("Address5"))
//                            mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
//                        mModel.setmCompaniesModel(mCompaniesModel);
//                    }
//                    if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
//                        JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
//                        PaymentModel mPaymentModel = new PaymentModel();
//                        if (!mPaymentObject.isNull("payment_id")) {
//                            mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
//                        }
//                        if (!mPaymentObject.isNull("sub_total")) {
//                            mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
//                        }
//                        if (!mPaymentObject.isNull("VAT")) {
//                            mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
//                        }
//                        if (!mPaymentObject.isNull("vat_price")) {
//                            mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
//                        }
//                        if (!mPaymentObject.isNull("total")) {
//                            mPaymentModel.setTotal(mPaymentObject.getString("total"));
//                        }
//                        if (!mPaymentObject.isNull("paid")) {
//                            mPaymentModel.setPaid(mPaymentObject.getString("paid"));
//                        }
//                        if (!mPaymentObject.isNull("due")) {
//                            mPaymentModel.setPaid(mPaymentObject.getString("due"));
//                        }
//                        mModel.setmPaymentModel(mPaymentModel);
//                    }
//                    if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
//                        JSONArray mItemArray = mJson.getJSONArray("items_data");
//                        for (int k = 0; k < mItemArray.length(); k++) {
//                            JSONObject mItemObj = mItemArray.getJSONObject(k);
//                            InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
//                            if (!mItemObj.isNull("item_id"))
//                                mItemModel.setItemID(mItemObj.getString("item_id"));
//                            if (!mItemObj.isNull("item_serial_no"))
//                                mItemModel.setItem(mItemObj.getString("item_serial_no"));
//                            if (!mItemObj.isNull("quantity"))
//                                mItemModel.setQuantity(mItemObj.getInt("quantity"));
//                            if (!mItemObj.isNull("price"))
//                                mItemModel.setUnitprice(mItemObj.getString("price"));
//                            if (!mItemObj.isNull("description"))
//                                mItemModel.setDescription(mItemObj.getString("description"));
//                            mInvoiceItemArrayList.add(mItemModel);
//                        }
//                        mModel.setmItemModelArrayList(mInvoiceItemArrayList);
//                    }
//                    if (page_no == 1) {
//                        modelArrayList.add(mModel);
//                    } else if (page_no > 1) {
//                        mLoadMore.add(mModel);
//                    }
//                }
//                if (mLoadMore.size() > 0) {
//                    modelArrayList.addAll(mLoadMore);
//                }
//                /*Set Adapter*/
//                setAdapter();
//
//            } else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
//        } catch (Exception e) {
//            Log.e(TAG, "*exception*" + e);
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();
        /* for back press */
        JaoharConstants.IS_INVOICE_BACK = false;
        JaoharConstants.IS_INVOICE_BACK_FROM_DRAFT = false;

//        IsInvoiceActive = 0;
//        txtMailTV.setVisibility(View.GONE);
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Vesseles API*//*
            modelArrayList.clear();
            mLoadMore.clear();
            page_no = 1;
            if (strLastPage.equals("FALSE")) {
                executeAPI();
            }
        }


//        executeAPI();
    }

    private void listeners() {
        try {
            HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    if (getActivity() != null) {
                        Intent intent = new Intent(getActivity(), SearchInvoiceActivity.class);
                        intent.putExtra("from", "pending_payment");
                        startActivity(intent);
                    }
//                        startActivity(new Intent(getActivity(), SearchInvoiceActivity.class));}
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }


        HomeActivity.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strLastPage = "FALSE";
                Intent mIntent = new Intent(getActivity(), AddPendingPaymentsActivity.class);
                mIntent.putExtra("Title", "Add Invoice");
                getActivity().startActivity(mIntent);
                overridePendingTransitionEnter(getActivity());
            }
        });
    }

    private void setViewsIDs(View view) {
        progressBottomPB = view.findViewById(R.id.progressBottomPB);
        resetRL1 = view.findViewById(R.id.resetRL1);
        pendingPaymentsRV = view.findViewById(R.id.pendingPaymentsRV);
        pendingNestedScroll = view.findViewById(R.id.pendingNestedScroll);

        swipeToRefresh = view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);

        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isNormalSearch = false;
                isAdvanceSearch = false;
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        executeAPI();
                    }
                }, 500);
            }
        });


        pendingNestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (isAdvanceSearch == false) {
                        if (isNormalSearch == false) {
//                            isScroolling = true;
                            ++page_no;
                            progressBottomPB.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (!isSwipeRefresh) {
                                        ++page_no;
                                        progressBottomPB.setVisibility(View.VISIBLE);
                                        if (strLastPage.equals("FALSE")) {
                                            executeAPI();
                                        } else {
                                            progressBottomPB.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            }, 500);
                        }
                    }
                }
            }
        });
    }

    public void executeAPI() {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
        }
        if (page_no == 1) {
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.GONE);
            showProgressDialog(getActivity());
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }


        Log.e("User_id", JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<PendingPaymentModels> call1 = mApiInterface.getAllPendingPaymentsRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<PendingPaymentModels>() {
            @Override
            public void onResponse(Call<PendingPaymentModels> call, retrofit2.Response<PendingPaymentModels> response) {
                resetRL1.setVisibility(View.GONE);
                hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e("**ResponseALLPending**", String.valueOf(response.body()));
//                parseResponse(response.body().toString());

                if (response.isSuccessful()) {
                    pendingPaymentData = response.body().getData();
                    strLastPage = response.body().getData().getLastPage();
                    if (page_no == 1) {
                        modelArrayList = pendingPaymentData.getAllInvoices();
//                        modelArrayList.add(mModel);
                    } else if (page_no > 1) {
                        mLoadMore = pendingPaymentData.getAllInvoices();
                    }

                    if (mLoadMore.size() > 0) {
                        modelArrayList.addAll(mLoadMore);
                    }

                    setAdapter();

                }

            }

            @Override
            public void onFailure(Call<PendingPaymentModels> call, Throwable t) {
                hideProgressDialog();
                Log.e("******error*****", t.getMessage());
            }
        });
    }

    private void setAdapter() {
        pendingPaymentsRV.setNestedScrollingEnabled(false);
        pendingPaymentsRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        pendingPaymentsAdapter = new PendingPaymentsAdapter(getActivity(), modelArrayList,
                mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface, mOnClickInterface);
        pendingPaymentsRV.setAdapter(pendingPaymentsAdapter);
    }


    public void executeDownloadLinkAPI(String ppId) {
        showProgressDialog(getActivity());

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getDownloadPdfLinkRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""),
                ppId);

        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {


                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    if (jsonObject.getInt("status") == 1) {
                        mPDF = jsonObject.getString("url");
                        mPDFName = mPDF.substring(mPDF.lastIndexOf("/") + 1);
                    }

                    if (!mPDF.equals("")) {
                        executePDFWithPermission("");
                    } else {
                        AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Their is no PDF");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.e(TAG, "*****ResponseALLk****" + response.body());
//                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


    private void PerformOptionsClick(int position, String strPDF, String strPDFNAme) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_pending_payments_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IS_Click_From_Edit = true;
                JaoharConstants.IS_Click_From_Copy = false;
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.Invoice_ID = modelArrayList.get(position).getAllData().getPpId();
                Intent mIntent = new Intent(getActivity(), AddPendingPaymentsActivity.class);
                mIntent.putExtra("id", modelArrayList.get(position).getAllData().getPpId());
                mIntent.putExtra("pp_number", modelArrayList.get(position).getAllData().getPpNo());
                mIntent.putExtra("company_id", modelArrayList.get(position).getAllData().getPpCompany());
                mIntent.putExtra("pp_date", modelArrayList.get(position).getAllData().getPpDate());
                mIntent.putExtra("companyName", modelArrayList.get(position).getPpCompanyDetail().getCompanyName());
                mIntent.putExtra("vessel1", modelArrayList.get(position).getPpVessel1Detail().getVesselName());
                mIntent.putExtra("vessel2", modelArrayList.get(position).getPpVessel2Detail().getVesselName());
                mIntent.putExtra("vessel3", modelArrayList.get(position).getPpVessel3Detail().getVesselName());
                mIntent.putExtra("vessel1_id", modelArrayList.get(position).getAllData().getPpVessel1());
                mIntent.putExtra("vessel2_id", modelArrayList.get(position).getAllData().getPpVessel2());
                mIntent.putExtra("vessel3_id", modelArrayList.get(position).getAllData().getPpVessel3());
                mIntent.putExtra("amt_due", modelArrayList.get(position).getAllData().getPpAmtDue());
                mIntent.putExtra("remarks", modelArrayList.get(position).getAllData().getPpRemarks());
                mIntent.putExtra("currency", modelArrayList.get(position).getAllData().getPpCurrency());
                mIntent.putExtra("status", modelArrayList.get(position).getAllData().getPpStatus());
                mIntent.putExtra("from", "edit");
                mIntent.putStringArrayListExtra("docs", modelArrayList.get(position).getAllData().getAllDocs());
                startActivity(mIntent);
            }
        });


        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                JaoharConstants.IS_Click_From_DRAFT = false;
//                JaoharConstants.IS_Click_From_Edit = false;
//                JaoharConstants.IS_Click_From_Copy = true;
//                JaoharConstants.Invoice_ID = modelArrayList.get(position).getInvoice_id();

                Intent mIntent = new Intent(getActivity(), PendingPaymentDetailsActivity.class);
//                mIntent.putExtra("Model", modelArrayList.get(position));
                mIntent.putExtra("id", modelArrayList.get(position).getAllData().getPpId());
                mIntent.putExtra("pp_number", modelArrayList.get(position).getAllData().getPpNo());
                mIntent.putExtra("company_id", modelArrayList.get(position).getAllData().getPpCompany());
                mIntent.putExtra("pp_date", modelArrayList.get(position).getAllData().getPpDate());
                mIntent.putExtra("companyName", modelArrayList.get(position).getPpCompanyDetail().getCompanyName());
                mIntent.putExtra("vessel1", modelArrayList.get(position).getPpVessel1Detail().getVesselName());
                mIntent.putExtra("vessel2", modelArrayList.get(position).getPpVessel2Detail().getVesselName());
                mIntent.putExtra("vessel3", modelArrayList.get(position).getPpVessel3Detail().getVesselName());
                mIntent.putExtra("vessel1_id", modelArrayList.get(position).getAllData().getPpVessel1());
                mIntent.putExtra("vessel2_id", modelArrayList.get(position).getAllData().getPpVessel2());
                mIntent.putExtra("vessel3_id", modelArrayList.get(position).getAllData().getPpVessel3());
                mIntent.putExtra("amt_due", modelArrayList.get(position).getAllData().getPpAmtDue());
                mIntent.putExtra("remarks", modelArrayList.get(position).getAllData().getPpRemarks());
                mIntent.putExtra("currency", modelArrayList.get(position).getAllData().getPpCurrency());
                mIntent.putExtra("status", modelArrayList.get(position).getAllData().getPpStatus());
                mIntent.putExtra("position", String.valueOf(position));
                mIntent.putStringArrayListExtra("docs", modelArrayList.get(position).getAllData().getAllDocs());
                startActivity(mIntent);
            }
        });


        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), InvoiceEmailActivity.class);
                mIntent.putExtra("SubjectName", "Invoice    " + modelArrayList.get(position).getAllData().getPpNo() + " " + modelArrayList.get(position).getPpVessel1Detail().getVesselName());
                mIntent.putExtra("pp_id", modelArrayList.get(position).getAllData().getPpId());
//                mIntent.putExtra("Model", modelArrayList.get(position));
                mIntent.putExtra("from", "pending_payment");
                startActivity(mIntent);
            }
        });


        downloadRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dialog.dismiss();
                executeDownloadLinkAPI(modelArrayList.get(position).getAllData().getPpId());

//                Log.e("DownaloadbaleLink", JaoharConstants.DEV_SERVER_URL + modelArrayList.get(position).getAllData().getPpId());
//                String url = JaoharConstants.DEV_SERVER_URL + modelArrayList.get(position).getAllData().getPpId();
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);

            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteConfirmDialog(modelArrayList.get(position), position);
            }
        });


        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void executePDFWithPermission(String position) {
        if (checkPermission()) {
            mDownloadPDFMethod(mPDF, mPDFName, position);
        } else {
            requestPermission();
        }
    }

    private String outputPath() {
//        String path =
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                        .toString();
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath()
                + "/jaohar");
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        } else {
            folder.delete();
            folder.mkdirs();
        }
        return path;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS);
    }

    private void mDownloadPDFMethod(String mPDF, String mPDFName, String position) {
        // write the document content
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            new AsyncCaller().execute();
//        } else {
//        showProgressDialog(getActivity());
        downloadPDF(mPDF, outputPath(), mPDFName, position);
//        }

        // your code
//        downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
//        Uri Download_Uri = Uri.parse(mPDF);
//        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
//        //Restrict the types of networks over which this download may proceed.
//        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
//        //Set whether this download may proceed over a roaming connection.
//        request.setAllowedOverRoaming(false);
//        //Set the title of this download, to be displayed in notifications (if enabled).
//        request.setTitle(mPDFName);
//        //Set a description of this download, to be displayed in notifications (if enabled)
//        request.setDescription("" + "Invoice PDF is Download Please Wait...");
//        //Set the local destination for the downloaded file to a path within the application's external files directory
//        request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
//        //Enqueue a new download and same the referenceId
//        downloadReference = downloadManager.enqueue(request);
    }

    private void downloadPDF(String url, String dirPath, String fileName, String position) {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                })

                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("PRDOWNLOADER", "Download completed at::::" + dirPath);
                        hideProgressDialog();
                        AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Downloaded Successfully!");
//                        showPDFAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Downloaded Successfully!", position);
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("PRDOWNLOADER", "Download failed at::::" + dirPath);
                        hideProgressDialog();
                        showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "PDF format is not correct!");
                    }
                });
    }

    /*
     * Show Alert Dailog Box
     * */
    public void showPDFAlertDialog(Activity mActivity, String strTitle, String strMessage, String position) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
                mIntent.putExtra("SubjectName", "Invoice    " + modelArrayList.get(Integer.parseInt(position)).getAllData().getPpNo() + " " + modelArrayList.get(Integer.parseInt(position)).getPpVessel1Detail().getVesselName());
                mIntent.putExtra("vesselID", modelArrayList.get(Integer.parseInt(position)).getAllData().getPpId());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        JaoharConstants.Invoice_ID = modelArrayList.get(Integer.parseInt(position)).getAllData().getPpId();
                        Gson gson = new Gson();
                        String modelClass = gson.toJson(modelArrayList.get(Integer.parseInt(position)));
                        mIntent.putExtra("Model", modelClass);
                        startActivity(mIntent);
                    }
                }, 500);

                mIntent.putExtra("Model", modelArrayList);

            }
        });
        alertDialog.show();
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    public void deleteConfirmDialog(final AllInvoice mInVoicesModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice));
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mInVoicesModel, position);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    public void executeDeleteAPI(AllInvoice mInVoicesModel, int position) {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deletePPRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), mInVoicesModel.getAllData().getPpId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    modelArrayList.remove(position);

                    if (pendingPaymentsAdapter != null)
                        pendingPaymentsAdapter.notifyDataSetChanged();

                    showAlertDialog(getActivity(), getActivity().getTitle().toString(), mModel.getMessage());

//                    modelArrayList.clear();
//                    executeAPI();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /*DeletePendingInterface mDeleteInvoiceInterface = new DeletePendingInterface() {
        @Override
        public void deleteInvoice(AllInvoice mInVoicesModel) {
            deleteConfirmDialog(mInVoicesModel);
        }
    };*/


}