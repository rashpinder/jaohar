package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static android.content.Context.DOWNLOAD_SERVICE;
import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddInvoiceActivity;
import jaohar.com.jaohar.activities.EditInvoiceActivity;
import jaohar.com.jaohar.activities.InvoiceDetailsActivity;
import jaohar.com.jaohar.activities.SendingInvoiceEmailActivity;
import jaohar.com.jaohar.activities.invoices_module.AllDraftsInvoicesActivity;
import jaohar.com.jaohar.activities.invoices_module.CopyInvoiceActivity;
import jaohar.com.jaohar.activities.invoices_module.InvoiceEmailActivity;
import jaohar.com.jaohar.activities.invoices_module.SearchInvoiceActivity;
import jaohar.com.jaohar.adapters.InVoicesAdapter;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllInVoicesFragment extends BaseFragment {

    static int IsInvoiceActive = 0;
    private static int page_no = 1;
    public final int REQUEST_PERMISSIONS = 1;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private final ArrayList<InVoicesModel> multiSelectArrayList = new ArrayList<InVoicesModel>();
    String TAG = AllInVoicesFragment.this.getClass().getSimpleName();
    /*unbinder*/
    Unbinder unbinder;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL, downArrowRL, resetRL1;
    TextView txtCenter, txtRight, txtMailTV, mailAllInvoices;
    ImageView imgBack, imgRight;
    RecyclerView inVoicesRV;
    InVoicesAdapter mInVoicesAdapter;
    NestedScrollView invoiceNestedScroll;
    SwipeRefreshLayout swipeToRefresh;
    EditText editSearchET;
    ProgressBar progressBottomPB;
    Boolean isScroolling = false;
    boolean isSwipeRefresh = false;
    ArrayList<InVoicesModel> modelArrayList = new ArrayList<InVoicesModel>();
    ArrayList<InVoicesModel> mLoadMore = new ArrayList<InVoicesModel>();
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    ArrayList<String> mArrayListStamps = new ArrayList<String>();
    ArrayList<String> mArrayListCompanys = new ArrayList<String>();
    ArrayList<String> mArrayListBankDetails = new ArrayList<String>();
    ArrayList<String> vesselArrayList = new ArrayList<String>();
    ArrayList<String> mArrayListCurrency = new ArrayList<String>();
    ArrayList<String> mArrayListSignDATA = new ArrayList<String>();
    ArrayList<String> mInvoiceID = new ArrayList<String>();
    boolean isNormalSearch = false, isAdvanceSearch = false;
    String strInvoiceNum, strInvoiceDateFrom, strInvoiceDateTo, strInvoiceCompany, strInvoiceVessel, strInvoiceCurrency, strInvoiceStamp, strInvoiceSign, strInvoiceStatus, strInvoiceBank;
    String[] arrayVesselsType, arrayStatus, arrayCurrency;
    String mPDF = "", mPDFName = "";
    DeleteInvoiceInterface mDeleteInvoiceInterface = new DeleteInvoiceInterface() {
        @Override
        public void deleteInvoice(InVoicesModel mInVoicesModel) {
//            deleteConfirmDialog(mInVoicesModel);
        }
    };
    SinglemailInvoiceInterface msinglemailInvoiceInterface = new SinglemailInvoiceInterface() {
        @Override
        public void mSinglemailInvoice(InVoicesModel mInvoiceModel) {
            strInvoiceNumber = "JAORO" + mInvoiceModel.getInvoice_number();
            strInvoiceVesselName = "   " + mInvoiceModel.getmVesselSearchInvoiceModel().getVessel_name();
            /*Send Email*/
            Intent mIntent = new Intent(getActivity(), SendingInvoiceEmailActivity.class);
            mIntent.putExtra("SubjectName", "Invoice    " + strInvoiceNumber + strInvoiceVesselName);
            mIntent.putExtra("vesselID", mInvoiceModel.getInvoice_id());
            startActivity(mIntent);
        }
    };
    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            PerformOptionsClick(position, modelArrayList.get(position).getPdf(), modelArrayList.get(position).getPdf_name());
            mPDF = modelArrayList.get(position).getPdf();
            mPDFName = modelArrayList.get(position).getPdf_name();
        }
    };
    SendMultipleInvoiceMAIlInterface mMultimaIlInterface = new SendMultipleInvoiceMAIlInterface() {
        @Override
        public void mSendMutliInvoice(InVoicesModel mInVoivce, boolean mDelete) {
            if (mInVoivce != null) {
                if (!mDelete) {

                    IsInvoiceActive = IsInvoiceActive + 1;
                    multiSelectArrayList.add(mInVoivce);
                    String strID = mInVoivce.getInvoice_number();
                    mInvoiceID.add(mInVoivce.getInvoice_number());

                } else {
                    IsInvoiceActive = IsInvoiceActive - 1;
                    multiSelectArrayList.remove(mInVoivce);
                    String strID = mInVoivce.getInvoice_number();

                    mInvoiceID.remove(mInVoivce.getInvoice_number());
                }
                if (IsInvoiceActive > 0) {
                    txtMailTV.setVisibility(View.VISIBLE);
                } else {
                    txtMailTV.setVisibility(View.GONE);
                }
            }
        }
    };
    private String strLastPage = "FALSE", strInvoiceNumber, strInvoiceVesselName;
    private DownloadManager downloadManager;
    private long downloadReference;
    PDFdownloadInterface mPdfDownloader = new PDFdownloadInterface() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void pdfDownloadInterface(final String strPDF, final String strPDFNAme) {
            if (!strPDF.equals("")) {
                // your code
                downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(strPDF);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(strPDFNAme);
                //Set a description of this download, to be displayed in notifications (if enabled)
                request.setDescription("" + "Invoice PDF is Download Please Wait...");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);
            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Their is no PDF");
            }
        }
    };

    public AllInVoicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_in_voices, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        setStatusBar();

        JaoharSingleton.getInstance().setSearchedVessel("");

        setViewsIDs(view);
        setClickListner();

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.all_invoices));

        HomeActivity.imgDraft.setVisibility(View.VISIBLE);
        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.ic_magnifying_glass);

        return view;
    }

    private void setViewsIDs(View view) {
        progressBottomPB = view.findViewById(R.id.progressBottomPB);
        editSearchET = view.findViewById(R.id.editSearchET);

        txtCenter = view.findViewById(R.id.txtCenter);
        txtMailTV = view.findViewById(R.id.txtMailTV);
        mailAllInvoices = view.findViewById(R.id.mailAllInvoices);
        txtMailTV.setVisibility(View.GONE);
        llLeftLL = view.findViewById(R.id.llLeftLL);
        imgRightLL = view.findViewById(R.id.imgRightLL);
        downArrowRL = view.findViewById(R.id.downArrowRL);
        resetRL1 = view.findViewById(R.id.resetRL1);
        imgBack = view.findViewById(R.id.imgBack);
        imgRight = view.findViewById(R.id.imgRight);
        inVoicesRV = view.findViewById(R.id.inVoicesRV);
        invoiceNestedScroll = view.findViewById(R.id.invoiceNestedScroll);
        swipeToRefresh = view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);

        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isNormalSearch = false;
                isAdvanceSearch = false;
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        executeAPI();
                    }
                }, 500);

            }
        });

        invoiceNestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (isAdvanceSearch == false) {
                        if (isNormalSearch == false) {
//                            isScroolling = true;
                            if (!isSwipeRefresh) {
                                ++page_no;
                                progressBottomPB.setVisibility(View.VISIBLE);
                                if (strLastPage.equals("FALSE")) {
                                    executeAPI();
                                } else {
                                    progressBottomPB.setVisibility(View.GONE);
                                }
                            }
                           /*     }
                            }, 1500);*/
                        }
                    }
                }
            }
        });
    }

    private void setClickListner() {
        HomeActivity.imgDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strLastPage = "FALSE";
                Intent mIntent = new Intent(getActivity(), AllDraftsInvoicesActivity.class);
                getActivity().startActivity(mIntent);
                overridePendingTransitionEnter(getActivity());
            }
        });

        HomeActivity.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strLastPage = "FALSE";
                Intent mIntent = new Intent(getActivity(), AddInvoiceActivity.class);
                mIntent.putExtra("Title", "Add Invoice");
                getActivity().startActivity(mIntent);
                overridePendingTransitionEnter(getActivity());
            }
        });

        try {
            HomeActivity.imgRightLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    JaoharConstants.is_Staff_FragmentClick = true;

                    Intent intent = new Intent(getActivity(), SearchInvoiceActivity.class);
                    intent.putExtra("from", "all_invoices");
                    startActivity(intent);

                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        try {
            HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    if (getActivity() != null) {
                        Intent intent = new Intent(getActivity(), SearchInvoiceActivity.class);
                        intent.putExtra("from", "all_invoices");
                        startActivity(intent);
                    }
//                    startActivity(new Intent(getActivity(), SearchInvoiceActivity.class));}
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeStampSAPI();
                advancedSearchView();
            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });

        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Send Email*/
                Intent mIntent = new Intent(getActivity(), SendingInvoiceEmailActivity.class);
                mIntent.putExtra("vessalName1", "Your Requested list of Invoices from Jaohar UK Limited");
                mIntent.putExtra("recordIDArray", getMultiVesselDetailsData());
                startActivity(mIntent);
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsInvoiceActive = 0;
                isAdvanceSearch = false;
                isNormalSearch = false;
                editSearchET.setText("");
                txtMailTV.setVisibility(View.GONE);
                //*Execute Vesseles API*//*
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });

        mailAllInvoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Send All Inoice*/
                /*Send Email*/
                Intent mIntent = new Intent(getActivity(), SendingInvoiceEmailActivity.class);
                mIntent.putExtra("vessalName2", "Your Requested list of Invoices from Jaohar UK Limited");
                mIntent.putExtra("recordIDArray", getMultiVesselDetailsData());
                startActivity(mIntent);
            }
        });
    }

    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mInvoiceID.size());
        for (int i = 0; i < mInvoiceID.size(); i++) {
            strData.add(mInvoiceID.get(i));
        }
        mInvoiceID.clear();
        return strData;
    }

    @Override
    public void onResume() {
        super.onResume();

        /* for back press */
        JaoharConstants.IS_INVOICE_BACK = false;
        JaoharConstants.IS_INVOICE_BACK_FROM_DRAFT = false;

        IsInvoiceActive = 0;
        txtMailTV.setVisibility(View.GONE);
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Vesseles API*//*
            modelArrayList.clear();
            mLoadMore.clear();
            page_no = 1;
            if (strLastPage.equals("FALSE")) {
                executeAPI();
            }
        }
    }

    public void executeAPI() {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
        }
        if (page_no == 1) {
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.GONE);
            showProgressDialog(getActivity());
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                resetRL1.setVisibility(View.GONE);
                hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e(TAG, "*****ResponseALLk****" + response.body());
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeNormalSearch() {
        modelArrayList.clear();
        resetRL1.setVisibility(View.VISIBLE);
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.searchInvoiceRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), editSearchET.getText().toString(), "staff");
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                editSearchET.setText("");
                isNormalSearch = true;
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject111 = new JSONObject(response.body().toString());
                    if (mJsonObject111.getString("status").equals("1")) {
                        hideProgressDialog();
                        JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_invoices");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            InVoicesModel mModel = new InVoicesModel();
                            JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                            if (!mAllDataObj.getString("pdf").equals("")) {
                                mModel.setPdf(mAllDataObj.getString("pdf"));
                            }
                            if (!mAllDataObj.getString("pdf_name").equals("")) {
                                mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                            }
                            if (!mAllDataObj.isNull("invoice_id"))
                                mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                            if (!mAllDataObj.isNull("invoice_no"))
                                mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                            if (!mAllDataObj.isNull("invoice_date"))
                                mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                            if (!mAllDataObj.isNull("term_days"))
                                mModel.setTerm_days(mAllDataObj.getString("term_days"));
                            if (!mAllDataObj.isNull("currency"))
                                mModel.setCurrency(mAllDataObj.getString("currency"));
                            if (!mAllDataObj.isNull("status"))
                                mModel.setStatus(mAllDataObj.getString("status"));
                            if (!mAllDataObj.isNull("reference"))
                                mModel.setRefrence1(mAllDataObj.getString("reference"));
                            if (!mAllDataObj.isNull("reference1"))
                                mModel.setRefrence2(mAllDataObj.getString("reference1"));
                            if (!mAllDataObj.isNull("reference2"))
                                mModel.setRefrence3(mAllDataObj.getString("reference2"));
                            if (!mAllDataObj.isNull("payment_id"))
                                mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                            if (!mAllDataObj.isNull("inv_state"))
                                mModel.setInv_state(mAllDataObj.getString("inv_state"));

                            if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                                JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                                SignatureModel mSignModel = new SignatureModel();
                                if (!mSignDataObj.isNull("sign_id"))
                                    mSignModel.setId(mSignDataObj.getString("sign_id"));
                                if (!mSignDataObj.isNull("sign_name"))
                                    mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                                if (!mSignDataObj.isNull("sign_image"))
                                    mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                                mModel.setmSignatureModel(mSignModel);
                            }
                            if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                                JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                                StampsModel mStampsModel = new StampsModel();
                                if (!mStampDataObj.isNull("stamp_id"))
                                    mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                                if (!mStampDataObj.isNull("stamp_name"))
                                    mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                                if (!mStampDataObj.isNull("stamp_image"))
                                    mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                                mModel.setmStampsModel(mStampsModel);
                            }
                            if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                                JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                                BankModel mBankModel = new BankModel();
                                if (!mBankDataObj.isNull("bank_id"))
                                    mBankModel.setId(mBankDataObj.getString("bank_id"));
                                if (!mBankDataObj.isNull("beneficiary"))
                                    mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                                if (!mBankDataObj.isNull("bank_name"))
                                    mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                                if (!mBankDataObj.isNull("address1"))
                                    mBankModel.setAddress1(mBankDataObj.getString("address1"));
                                if (!mBankDataObj.isNull("address2"))
                                    mBankModel.setAddress2(mBankDataObj.getString("address2"));
                                if (!mBankDataObj.isNull("iban_ron"))
                                    mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                                if (!mBankDataObj.isNull("iban_usd"))
                                    mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                                if (!mBankDataObj.isNull("iban_eur"))
                                    mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                                if (!mBankDataObj.isNull("swift"))
                                    mBankModel.setSwift(mBankDataObj.getString("swift"));
                                mModel.setmBankModel(mBankModel);
                            }
                            if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                                JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                                VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                                if (!mSearchVesselObj.isNull("vessel_id"))
                                    mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                                if (!mSearchVesselObj.isNull("vessel_name"))
                                    mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                                if (!mSearchVesselObj.isNull("IMO_no"))
                                    mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                                if (!mSearchVesselObj.isNull("flag"))
                                    mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                                mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                            }
                            if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                                JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                                CompaniesModel mCompaniesModel = new CompaniesModel();
                                if (!mSearchCompanyObj.isNull("id"))
                                    mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                                if (!mSearchCompanyObj.isNull("company_name"))
                                    mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                                if (!mSearchCompanyObj.isNull("Address1"))
                                    mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                                if (!mSearchCompanyObj.isNull("Address2"))
                                    mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                                if (!mSearchCompanyObj.isNull("Address3"))
                                    mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                                if (!mSearchCompanyObj.isNull("Address4"))
                                    mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                                if (!mSearchCompanyObj.isNull("Address5"))
                                    mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                                mModel.setmCompaniesModel(mCompaniesModel);
                            }
                            if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                                JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                                PaymentModel mPaymentModel = new PaymentModel();
                                if (!mPaymentObject.isNull("payment_id")) {
                                    mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                                }
                                if (!mPaymentObject.isNull("sub_total")) {
                                    mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                                }
                                if (!mPaymentObject.isNull("VAT")) {
                                    mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                                }
                                if (!mPaymentObject.isNull("vat_price")) {
                                    mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                                }
                                if (!mPaymentObject.isNull("total")) {
                                    mPaymentModel.setTotal(mPaymentObject.getString("total"));
                                }
                                if (!mPaymentObject.isNull("paid")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                                }
                                if (!mPaymentObject.isNull("due")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("due"));
                                }
                                mModel.setmPaymentModel(mPaymentModel);
                            }
                            if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                                JSONArray mItemArray = mJson.getJSONArray("items_data");
                                for (int k = 0; k < mItemArray.length(); k++) {
                                    JSONObject mItemObj = mItemArray.getJSONObject(k);
                                    InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                                    if (!mItemObj.isNull("item_id"))
                                        mItemModel.setItemID(mItemObj.getString("item_id"));
                                    if (!mItemObj.isNull("item_serial_no"))
                                        mItemModel.setItem(mItemObj.getString("item_serial_no"));
                                    if (!mItemObj.isNull("quantity"))
                                        mItemModel.setQuantity(mItemObj.getInt("quantity"));
                                    if (!mItemObj.isNull("price"))
                                        mItemModel.setUnitprice(mItemObj.getString("price"));
                                    if (!mItemObj.isNull("description"))
                                        mItemModel.setDescription(mItemObj.getString("description"));
                                    mInvoiceItemArrayList.add(mItemModel);
                                }
                                mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                            }

                            modelArrayList.add(mModel);
                        }

                        /*Set Adapter*/
                        setAdapter();

                    } else if (mJsonObject111.getString("status").equals("100")) {
                        hideProgressDialog();
                        AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    } else {
                        hideProgressDialog();
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse(String response) {
        mLoadMore.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            if (mJsonObject.getString("status").equals("1")) {
                JSONObject mDataObject = mJsonObject.getJSONObject("data");
                JSONArray mJsonArray = mDataObject.getJSONArray("all_invoices");
                strLastPage = mDataObject.getString("last_page");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    InVoicesModel mModel = new InVoicesModel();
                    JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                    if (!mAllDataObj.isNull("invoice_id"))
                        mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                    if (!mAllDataObj.getString("pdf").equals(""))
                        mModel.setPdf(mAllDataObj.getString("pdf"));
                    if (!mAllDataObj.getString("pdf_name").equals(""))
                        mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                    if (!mAllDataObj.isNull("invoice_no"))
                        mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                    if (!mAllDataObj.isNull("invoice_date"))
                        mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                    if (!mAllDataObj.isNull("term_days"))
                        mModel.setTerm_days(mAllDataObj.getString("term_days"));
                    if (!mAllDataObj.isNull("currency"))
                        mModel.setCurrency(mAllDataObj.getString("currency"));
                    if (!mAllDataObj.isNull("status"))
                        mModel.setStatus(mAllDataObj.getString("status"));
                    if (!mAllDataObj.isNull("reference"))
                        mModel.setRefrence1(mAllDataObj.getString("reference"));
                    if (!mAllDataObj.isNull("reference1"))
                        mModel.setRefrence2(mAllDataObj.getString("reference1"));
                    if (!mAllDataObj.isNull("reference2"))
                        mModel.setRefrence3(mAllDataObj.getString("reference2"));
                    if (!mAllDataObj.isNull("payment_id"))
                        mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                    if (!mAllDataObj.isNull("inv_state"))
                        mModel.setInv_state(mAllDataObj.getString("inv_state"));

                    if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                        JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                        SignatureModel mSignModel = new SignatureModel();
                        if (!mSignDataObj.isNull("sign_id"))
                            mSignModel.setId(mSignDataObj.getString("sign_id"));
                        if (!mSignDataObj.isNull("sign_name"))
                            mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                        if (!mSignDataObj.isNull("sign_image"))
                            mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                        mModel.setmSignatureModel(mSignModel);
                    }
                    if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                        JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                        StampsModel mStampsModel = new StampsModel();
                        if (!mStampDataObj.isNull("stamp_id"))
                            mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                        if (!mStampDataObj.isNull("stamp_name"))
                            mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                        if (!mStampDataObj.isNull("stamp_image"))
                            mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                        mModel.setmStampsModel(mStampsModel);
                    }
                    if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                        JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                        BankModel mBankModel = new BankModel();
                        if (!mBankDataObj.isNull("bank_id"))
                            mBankModel.setId(mBankDataObj.getString("bank_id"));
                        if (!mBankDataObj.isNull("beneficiary"))
                            mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                        if (!mBankDataObj.isNull("bank_name"))
                            mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                        if (!mBankDataObj.isNull("address1"))
                            mBankModel.setAddress1(mBankDataObj.getString("address1"));
                        if (!mBankDataObj.isNull("address2"))
                            mBankModel.setAddress2(mBankDataObj.getString("address2"));
                        if (!mBankDataObj.isNull("iban_ron"))
                            mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                        if (!mBankDataObj.isNull("iban_usd"))
                            mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                        if (!mBankDataObj.isNull("iban_eur"))
                            mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                        if (!mBankDataObj.isNull("swift"))
                            mBankModel.setSwift(mBankDataObj.getString("swift"));
                        mModel.setmBankModel(mBankModel);
                    }
                    if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                        JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                        VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                        if (!mSearchVesselObj.isNull("vessel_id"))
                            mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                        if (!mSearchVesselObj.isNull("vessel_name"))
                            mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                        if (!mSearchVesselObj.isNull("IMO_no"))
                            mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                        if (!mSearchVesselObj.isNull("flag"))
                            mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                        mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                    }
                    if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                        JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                        CompaniesModel mCompaniesModel = new CompaniesModel();
                        if (!mSearchCompanyObj.isNull("id"))
                            mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                        if (!mSearchCompanyObj.isNull("company_name"))
                            mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                        if (!mSearchCompanyObj.isNull("Address1"))
                            mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                        if (!mSearchCompanyObj.isNull("Address2"))
                            mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                        if (!mSearchCompanyObj.isNull("Address3"))
                            mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                        if (!mSearchCompanyObj.isNull("Address4"))
                            mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                        if (!mSearchCompanyObj.isNull("Address5"))
                            mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                        mModel.setmCompaniesModel(mCompaniesModel);
                    }
                    if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                        JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                        PaymentModel mPaymentModel = new PaymentModel();
                        if (!mPaymentObject.isNull("payment_id")) {
                            mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                        }
                        if (!mPaymentObject.isNull("sub_total")) {
                            mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                        }
                        if (!mPaymentObject.isNull("VAT")) {
                            mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                        }
                        if (!mPaymentObject.isNull("vat_price")) {
                            mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                        }
                        if (!mPaymentObject.isNull("total")) {
                            mPaymentModel.setTotal(mPaymentObject.getString("total"));
                        }
                        if (!mPaymentObject.isNull("paid")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                        }
                        if (!mPaymentObject.isNull("due")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("due"));
                        }
                        mModel.setmPaymentModel(mPaymentModel);
                    }
                    if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                        JSONArray mItemArray = mJson.getJSONArray("items_data");
                        for (int k = 0; k < mItemArray.length(); k++) {
                            JSONObject mItemObj = mItemArray.getJSONObject(k);
                            InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                            if (!mItemObj.isNull("item_id"))
                                mItemModel.setItemID(mItemObj.getString("item_id"));
                            if (!mItemObj.isNull("item_serial_no"))
                                mItemModel.setItem(mItemObj.getString("item_serial_no"));
                            if (!mItemObj.isNull("quantity"))
                                mItemModel.setQuantity(mItemObj.getInt("quantity"));
                            if (!mItemObj.isNull("price"))
                                mItemModel.setUnitprice(mItemObj.getString("price"));
                            if (!mItemObj.isNull("description"))
                                mItemModel.setDescription(mItemObj.getString("description"));
                            mInvoiceItemArrayList.add(mItemModel);
                        }
                        mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                    }
                    if (page_no == 1) {
                        modelArrayList.add(mModel);
                    } else if (page_no > 1) {

                        mLoadMore.add(mModel);
                    }
                }
                if (mLoadMore.size() > 0) {
                    modelArrayList.addAll(mLoadMore);
                }
                /*Set Adapter*/
                setAdapter();

            } else if (mJsonObject.getString("status").equals("100")) {
                AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject.getString("message"));
            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "*exception*" + e);
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        inVoicesRV.setNestedScrollingEnabled(false);
        inVoicesRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mInVoicesAdapter = new InVoicesAdapter(getActivity(), modelArrayList, mDeleteInvoiceInterface,
                mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface, mOnClickInterface);
        inVoicesRV.setAdapter(mInVoicesAdapter);
//        mInVoicesAdapter.notifyDataSetChanged();
    }

    private void PerformOptionsClick(int position, String strPDF, String strPDFNAme) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_invoice_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IS_Click_From_Edit = true;
                JaoharConstants.IS_Click_From_Copy = false;
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.Invoice_ID = modelArrayList.get(position).getInvoice_id();
                Intent mIntent = new Intent(getActivity(), EditInvoiceActivity.class);
                mIntent.putExtra("Model", modelArrayList.get(position));
                startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.IS_Click_From_Edit = false;
                JaoharConstants.IS_Click_From_Copy = true;
                JaoharConstants.Invoice_ID = modelArrayList.get(position).getInvoice_id();
                Intent mIntent = new Intent(getActivity(), CopyInvoiceActivity.class);
                mIntent.putExtra("Model", modelArrayList.get(position));
                startActivity(mIntent);
            }
        });


        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), InvoiceEmailActivity.class);
                mIntent.putExtra("SubjectName", "Invoice    " + modelArrayList.get(position).getInvoice_number() + " " + modelArrayList.get(position).getmVesselSearchInvoiceModel().getVessel_name());
                mIntent.putExtra("vesselID", modelArrayList.get(position).getInvoice_id());
                mIntent.putExtra("Model", modelArrayList.get(position));
                mIntent.putExtra("from", "all_invoices");
                startActivity(mIntent);
            }
        });


        downloadRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!strPDF.equals("")) {
                    executePDFWithPermission(String.valueOf(position));
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Their is no PDF");
                }
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteConfirmDialog(modelArrayList.get(position), position);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void executePDFWithPermission(String position) {
        if (checkPermission()) {
            mDownloadPDFMethod(mPDF, mPDFName, position);
        } else {
            requestPermission();
        }
    }

    private void mDownloadPDFMethod(String mPDF, String mPDFName, String position) {
        // write the document content
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            new AsyncCaller().execute();
//        } else {
        showProgressDialog(getActivity());
        downloadPDF(mPDF, outputPath(), mPDFName, position);
//        }

        // your code
//        downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
//        Uri Download_Uri = Uri.parse(mPDF);
//        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
//        //Restrict the types of networks over which this download may proceed.
//        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
//        //Set whether this download may proceed over a roaming connection.
//        request.setAllowedOverRoaming(false);
//        //Set the title of this download, to be displayed in notifications (if enabled).
//        request.setTitle(mPDFName);
//        //Set a description of this download, to be displayed in notifications (if enabled)
//        request.setDescription("" + "Invoice PDF is Download Please Wait...");
//        //Set the local destination for the downloaded file to a path within the application's external files directory
//        request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
//        //Enqueue a new download and same the referenceId
//        downloadReference = downloadManager.enqueue(request);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void saveFileUsingMediaStore(Context context, String url, String fileName) {
//        String extStorageDirectory = Environment.getExternalStorageDirectory()
//                .toString();
//        File folder = new File(extStorageDirectory, "pdf");
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/jaoharr");
        folder.mkdir();
        File file = new File(folder, fileName);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        Downloader.DownloadFile(url, file);


//        ContentValues contentValues = new ContentValues();
//        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
//        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "application/pdf");
//        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS);
//
//        final ContentResolver resolver = context.getContentResolver();
//        OutputStream stream = null;
//        Uri uri = resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);
//        ParcelFileDescriptor pfd;
//
//        if (uri != null) {
//            try {
//                pfd = resolver.openFileDescriptor(uri, "w");
//                assert pfd != null;
//                FileOutputStream out = new FileOutputStream(pfd.getFileDescriptor());
//                out.write(uri.getPath().getBytes());
//                out.close();
//                pfd.close();
//
//                contentValues.clear();
//                contentValues.put(MediaStore.Video.Media.IS_PENDING, 0);
//                resolver.update(uri, contentValues, null, null);
//                stream = resolver.openOutputStream(uri);
//                if (stream == null) {
//                    throw new IOException("Failed to get output stream.");
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
    }

    /* get path of pdf */
    private String outputPath() {
//        String path =
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                        .toString();
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath()
                + "/jaohar");
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        } else {
            folder.delete();
            folder.mkdirs();
        }
        return path;
    }

    private void downloadPDF(String url, String dirPath, String fileName, String position) {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("PRDOWNLOADER", "Download completed at::::" + dirPath);
                        hideProgressDialog();
                        showPDFAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Downloaded Successfully!", position);
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("PRDOWNLOADER", "Download failed at::::" + dirPath);
                        hideProgressDialog();
                        showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "PDF format is not correct!");
                    }
                });
    }

    /*
     * Show Alert Dailog Box
     * */
    public void showPDFAlertDialog(Activity mActivity, String strTitle, String strMessage, String position) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
//                mIntent.putExtra("SubjectName", "Invoice    " +  modelArrayList.get(position).getInvoice_number() +" "+modelArrayList.get(position).getmVesselSearchInvoiceModel().getVessel_name());
//                mIntent.putExtra("vesselID", modelArrayList.get(position).getInvoice_id());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        JaoharConstants.Invoice_ID = modelArrayList.get(Integer.parseInt(position)).getInvoice_id();
                        mIntent.putExtra("Model", modelArrayList.get(Integer.parseInt(position)));
                        mIntent.putExtra("from", "invoice");
                        startActivity(mIntent);
                    }
                }, 500);

//                mIntent.putExtra("Model", modelArrayList);

            }
        });
        alertDialog.show();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean writeStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (writeStorage && readStorage) {

                    } else {
                        Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_LONG).show();
                        requestPermission();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    public void deleteConfirmDialog(final InVoicesModel mInVoicesModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice));
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mInVoicesModel, position);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    public void executeDeleteAPI(InVoicesModel mInVoicesModel, int position) {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteInvoiceRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), mInVoicesModel.getInvoice_id());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    modelArrayList.remove(position);

                    if (mInVoicesAdapter != null)
                        mInVoicesAdapter.notifyDataSetChanged();

                    showAlertDialog(getActivity(), getActivity().getTitle().toString(), mModel.getMessage());

//                    modelArrayList.clear();
//                    executeAPI();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public void advancedSearchView() {
        final Dialog searchDialog = new Dialog(getActivity());
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advance_search_invoice);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editInvoiceET = searchDialog.findViewById(R.id.editInvoiceET);
        final EditText editDateFromET = searchDialog.findViewById(R.id.editDateFromET);
        final EditText editDateToET = searchDialog.findViewById(R.id.editDateToET);
        final EditText editCompanyET = searchDialog.findViewById(R.id.editCompanyET);
        final EditText editVesselET = searchDialog.findViewById(R.id.editVesselET);
        final EditText editCurrencyET = searchDialog.findViewById(R.id.editCurrencyET);
        final EditText editStampET = searchDialog.findViewById(R.id.editStampET);
        final EditText editSignET = searchDialog.findViewById(R.id.editSignET);
        final EditText editStatusET = searchDialog.findViewById(R.id.editStatusET);
        final EditText editBankET = searchDialog.findViewById(R.id.editBankET);
        final EditText editNameET = searchDialog.findViewById(R.id.editNameET);

        arrayStatus = getActivity().getResources().getStringArray(R.array.status_array);
        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(getActivity(), "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });
        editDateFromET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    int mYear, mMonth, mDay, mHour, mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;
//                                  String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateFromET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });

        editDateToET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final int mYear;
                    final int mMonth;
                    final int mDay;
                    int mHour;
                    int mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;

//                          String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateToET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });


        editStampET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Stamps");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListStamps);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editStampET.setText(mArrayListStamps.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editCompanyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Company");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCompanys);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editCompanyET.setText(mArrayListCompanys.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editBankET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Bank Details");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListBankDetails);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editBankET.setText(mArrayListBankDetails.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editVesselET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessels");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, vesselArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editVesselET.setText(vesselArrayList.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });
        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Currency");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCurrency);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editCurrencyET.setText(mArrayListCurrency.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });
        editSignET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Signatures");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListSignDATA);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editSignET.setText(mArrayListSignDATA.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strInvoiceNum = editInvoiceET.getText().toString();
                strInvoiceDateFrom = editDateFromET.getText().toString();
                strInvoiceDateTo = editDateToET.getText().toString();
                strInvoiceCompany = editCompanyET.getText().toString();
                strInvoiceVessel = editVesselET.getText().toString();
                strInvoiceCurrency = editCurrencyET.getText().toString();
                strInvoiceStamp = editStampET.getText().toString();
                strInvoiceSign = editSignET.getText().toString();
                strInvoiceStatus = editStatusET.getText().toString();
                strInvoiceBank = editBankET.getText().toString();
                if (Utilities.isNetworkAvailable(getActivity()) == false) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchAPI(searchDialog);
                }

            }
        });
        searchDialog.show();

    }

    public void executeAdvanceSearchAPI(final Dialog mDialog) {
        modelArrayList.clear();
        resetRL1.setVisibility(View.VISIBLE);
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.advancedSearchInvoiceRequest(strInvoiceNum, "staff", strInvoiceDateFrom, strInvoiceDateTo,
                strInvoiceCompany, strInvoiceVessel, strInvoiceCurrency, strInvoiceStamp, strInvoiceSign, strInvoiceStatus, strInvoiceBank, JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();

                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject111 = new JSONObject(response.body().toString());
                    if (mJsonObject111.getString("status").equals("1")) {
                        isAdvanceSearch = true;
                        JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_invoices");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            InVoicesModel mModel = new InVoicesModel();
                            JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                            if (!mAllDataObj.isNull("invoice_id"))
                                mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                            if (!mAllDataObj.getString("pdf").equals("")) {
                                mModel.setPdf(mAllDataObj.getString("pdf"));
                            }
                            if (!mAllDataObj.getString("pdf_name").equals("")) {
                                mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                            }
                            if (!mAllDataObj.isNull("invoice_no"))
                                mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                            if (!mAllDataObj.isNull("invoice_date"))
                                mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                            if (!mAllDataObj.isNull("term_days"))
                                mModel.setTerm_days(mAllDataObj.getString("term_days"));
                            if (!mAllDataObj.isNull("currency"))
                                mModel.setCurrency(mAllDataObj.getString("currency"));
                            if (!mAllDataObj.isNull("status"))
                                mModel.setStatus(mAllDataObj.getString("status"));
                            if (!mAllDataObj.isNull("reference"))
                                mModel.setRefrence1(mAllDataObj.getString("reference"));
                            if (!mAllDataObj.isNull("reference1"))
                                mModel.setRefrence2(mAllDataObj.getString("reference1"));
                            if (!mAllDataObj.isNull("reference2"))
                                mModel.setRefrence3(mAllDataObj.getString("reference2"));
                            if (!mAllDataObj.isNull("payment_id"))
                                mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                            if (!mAllDataObj.isNull("inv_state"))
                                mModel.setInv_state(mAllDataObj.getString("inv_state"));

                            if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                                JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                                SignatureModel mSignModel = new SignatureModel();
                                if (!mSignDataObj.isNull("sign_id"))
                                    mSignModel.setId(mSignDataObj.getString("sign_id"));
                                if (!mSignDataObj.isNull("sign_name"))
                                    mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                                if (!mSignDataObj.isNull("sign_image"))
                                    mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                                mModel.setmSignatureModel(mSignModel);
                            }
                            if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                                JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                                StampsModel mStampsModel = new StampsModel();
                                if (!mStampDataObj.isNull("stamp_id"))
                                    mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                                if (!mStampDataObj.isNull("stamp_name"))
                                    mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                                if (!mStampDataObj.isNull("stamp_image"))
                                    mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                                mModel.setmStampsModel(mStampsModel);
                            }
                            if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                                JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                                BankModel mBankModel = new BankModel();
                                if (!mBankDataObj.isNull("bank_id"))
                                    mBankModel.setId(mBankDataObj.getString("bank_id"));
                                if (!mBankDataObj.isNull("beneficiary"))
                                    mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                                if (!mBankDataObj.isNull("bank_name"))
                                    mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                                if (!mBankDataObj.isNull("address1"))
                                    mBankModel.setAddress1(mBankDataObj.getString("address1"));
                                if (!mBankDataObj.isNull("address2"))
                                    mBankModel.setAddress2(mBankDataObj.getString("address2"));
                                if (!mBankDataObj.isNull("iban_ron"))
                                    mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                                if (!mBankDataObj.isNull("iban_usd"))
                                    mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                                if (!mBankDataObj.isNull("iban_eur"))
                                    mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                                if (!mBankDataObj.isNull("swift"))
                                    mBankModel.setSwift(mBankDataObj.getString("swift"));
                                mModel.setmBankModel(mBankModel);
                            }
                            if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                                JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                                VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                                if (!mSearchVesselObj.isNull("vessel_id"))
                                    mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                                if (!mSearchVesselObj.isNull("vessel_name"))
                                    mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                                if (!mSearchVesselObj.isNull("IMO_no"))
                                    mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                                if (!mSearchVesselObj.isNull("flag"))
                                    mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                                mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                            }
                            if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                                JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                                CompaniesModel mCompaniesModel = new CompaniesModel();
                                if (!mSearchCompanyObj.isNull("id"))
                                    mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                                if (!mSearchCompanyObj.isNull("company_name"))
                                    mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                                if (!mSearchCompanyObj.isNull("Address1"))
                                    mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                                if (!mSearchCompanyObj.isNull("Address2"))
                                    mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                                if (!mSearchCompanyObj.isNull("Address3"))
                                    mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                                if (!mSearchCompanyObj.isNull("Address4"))
                                    mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                                if (!mSearchCompanyObj.isNull("Address5"))
                                    mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                                mModel.setmCompaniesModel(mCompaniesModel);
                            }
                            if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                                JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                                PaymentModel mPaymentModel = new PaymentModel();
                                if (!mPaymentObject.isNull("payment_id")) {
                                    mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                                }
                                if (!mPaymentObject.isNull("sub_total")) {
                                    mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                                }
                                if (!mPaymentObject.isNull("VAT")) {
                                    mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                                }
                                if (!mPaymentObject.isNull("vat_price")) {
                                    mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                                }
                                if (!mPaymentObject.isNull("total")) {
                                    mPaymentModel.setTotal(mPaymentObject.getString("total"));
                                }
                                if (!mPaymentObject.isNull("paid")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                                }
                                if (!mPaymentObject.isNull("due")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("due"));
                                }
                                mModel.setmPaymentModel(mPaymentModel);
                            }
                            if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                                JSONArray mItemArray = mJson.getJSONArray("items_data");
                                for (int k = 0; k < mItemArray.length(); k++) {
                                    JSONObject mItemObj = mItemArray.getJSONObject(k);
                                    InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                                    if (!mItemObj.isNull("item_id"))
                                        mItemModel.setItemID(mItemObj.getString("item_id"));
                                    if (!mItemObj.isNull("item_serial_no"))
                                        mItemModel.setItem(mItemObj.getString("item_serial_no"));
                                    if (!mItemObj.isNull("quantity"))
                                        mItemModel.setQuantity(mItemObj.getInt("quantity"));
                                    if (!mItemObj.isNull("price"))
                                        mItemModel.setUnitprice(mItemObj.getString("price"));
                                    if (!mItemObj.isNull("description"))
                                        mItemModel.setDescription(mItemObj.getString("description"));
                                    mInvoiceItemArrayList.add(mItemModel);
                                }
                                mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                            }
                            modelArrayList.add(mModel);
                        }

                        /*Set Adapter*/
                        setAdapter();

                    } else if (mJsonObject111.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    } else {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    public void executeStampSAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAlStampsRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                parseResponse1(response.body().toString());
                executeCompaniesAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse1(String response) {
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    StampsModel mModel = new StampsModel();
                    if (!mJson.isNull("stamp_id"))
                        mModel.setId(mJson.getString("stamp_id"));
                    if (!mJson.isNull("stamp_image"))
                        mModel.setStamp_image(mJson.getString("stamp_image"));
                    if (!mJson.isNull("stamp_name"))
                        mModel.setStamp_name(mJson.getString("stamp_name"));
                    mArrayListStamps.add(mJson.getString("stamp_name"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeCompaniesAPI() {
        mArrayListCompanys.clear();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAlCompaniesRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                parseResponse12(response.body().toString());
                executeBankDetailAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse12(String response) {

        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CompaniesModel mModel = new CompaniesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("company_name")) {
                        mModel.setCompany_name(mJson.getString("company_name"));
                        mArrayListCompanys.add(mJson.getString("company_name"));
                    }
                    if (!mJson.isNull("Address1")) {
                        mModel.setAddress1(mJson.getString("Address1"));
                    }
                    if (!mJson.isNull("Address2")) {
                        mModel.setAddress2(mJson.getString("Address2"));
                    }
                    if (!mJson.isNull("Address3")) {
                        mModel.setAddress3(mJson.getString("Address3"));
                    }
                    if (!mJson.isNull("Address4")) {
                        mModel.setAddress4(mJson.getString("Address4"));
                    }
                    if (!mJson.isNull("Address5")) {
                        mModel.setAddress5(mJson.getString("Address5"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeBankDetailAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBankDetailsRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "******Response*****" + response);
                JaoharConstants.IS_BANK_DETAILS_EDIT = false;
                hideProgressDialog();
                parseResponse13(response.body().toString());
                executeGetAllVesselsAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse13(String response) {
        mArrayListBankDetails.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    BankModel mModel = new BankModel();
                    mModel.setId(mJson.getString("bank_id"));
                    mModel.setBenificiary(mJson.getString("beneficiary"));
                    mModel.setBankName(mJson.getString("bank_name"));
                    mArrayListBankDetails.add(mJson.getString("bank_name"));
                    mModel.setAddress1(mJson.getString("address1"));
                    mModel.setAddress2(mJson.getString("address2"));
                    mModel.setIbanRON(mJson.getString("iban_ron"));
                    mModel.setIbanUSD(mJson.getString("iban_usd"));
                    mModel.setIbanEUR(mJson.getString("iban_eur"));
                    mModel.setSwift(mJson.getString("swift"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeGetAllVesselsAPI() {
        vesselArrayList.clear();
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceVesselsRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
//                hideProgressDialog();
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            VesselSearchInvoiceModel mModel = new VesselSearchInvoiceModel();
                            mModel.setVessel_id(mJson.getString("vessel_id"));
                            mModel.setVessel_name(mJson.getString("vessel_name"));
                            vesselArrayList.add(mJson.getString("vessel_name"));
                            mModel.setIMO_no(mJson.getString("IMO_no"));
                            mModel.setFlag(mJson.getString("flag"));
                        }
                        /*Set Adapter*/
                        executeCurrencyAPI();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeCurrencyAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllCurrenciesRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();

                JaoharConstants.IS_CURRENCY_EDIT = false;
                parseResponse14(response.body().toString());
                executeSignDataAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse14(String response) {
        mArrayListCurrency.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CurrenciesModel mModel = new CurrenciesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("currency_name")) {
                        mModel.setCurrency_name(mJson.getString("currency_name"));
                        mArrayListCurrency.add(mJson.getString("currency_name"));
                    }
                    if (!mJson.isNull("alias_name")) {
                        mModel.setAlias_name(mJson.getString("alias_name"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeSignDataAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllSignsRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();

                Log.e(TAG, "******Response*****" + response);

                JaoharConstants.IS_SIGNATURE_EDIT = false;
                hideProgressDialog();
                parseResponse15(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse15(String response) {
        mArrayListSignDATA.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    SignatureModel mModel = new SignatureModel();
                    if (!mJson.isNull("sign_id"))
                        mModel.setId(mJson.getString("sign_id"));
                    if (!mJson.isNull("sign_name"))
                        mModel.setSignature_name(mJson.getString("sign_name"));
                    mArrayListSignDATA.add(mJson.getString("sign_name"));
                    if (!mJson.isNull("sign_image"))
                        mModel.setSignature_image(mJson.getString("sign_image"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }

        if (progressDialog != null && progressDialog.isShowing())
            hideProgressDialog();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressDialog != null && progressDialog.isShowing())
            hideProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (progressDialog != null && progressDialog.isShowing())
            hideProgressDialog();
    }

    public static class Downloader {

        public static void DownloadFile(String fileURL, File directory) {
            try {

                FileOutputStream f = new FileOutputStream(directory);
                URL u = new URL(fileURL);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncCaller extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            showProgressDialog(getActivity());
        }

        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        protected Void doInBackground(Void... params) {

            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
            if (getActivity() != null)
                saveFileUsingMediaStore(getActivity(), mPDF, mPDFName);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //this method will be running on UI thread
            hideProgressDialog();
            showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Downloaded Successfully!");
        }
    }
}
