package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static android.content.Context.DOWNLOAD_SERVICE;

import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddInvoiceActivity;
import jaohar.com.jaohar.activities.EditInvoiceActivity;
import jaohar.com.jaohar.activities.InvoiceDetailsActivity;
import jaohar.com.jaohar.activities.SendingInvoiceEmailActivity;
import jaohar.com.jaohar.activities.invoices_module.CopyInvoiceActivity;
import jaohar.com.jaohar.activities.invoices_module.CopyUniversalInvoiceActivity;
import jaohar.com.jaohar.activities.invoices_module.InvoiceEmailActivity;
import jaohar.com.jaohar.activities.invoices_module.SearchInvoiceActivity;
import jaohar.com.jaohar.activities.invoices_module.SearchUniversalInvoiceActivity;
import jaohar.com.jaohar.activities.universal_invoice_module.AddUniversalInvoiceActivity;
import jaohar.com.jaohar.activities.universal_invoice_module.AllUniversalInvoiceActivity;
import jaohar.com.jaohar.activities.universal_invoice_module.EditUniversalInvoiceActivity;
import jaohar.com.jaohar.activities.universal_invoice_module.InvoiceCompanyActivity;
import jaohar.com.jaohar.activities.universal_invoice_module.MailUniversalInvoiceActivity;
import jaohar.com.jaohar.adapters.AllUniversalInvoiceAdapter;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.DataItem;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllUniversalInvoiceFragment extends BaseFragment {

    String TAG = AllUniversalInvoiceFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    LinearLayout llLeftLL;
    RelativeLayout imgRightLL, downArrowRL, resetRL1, invoiceCompanyRL;
    TextView txtCenter, txtRight, txtMailTV, mailAllInvoices, selectedCompanyTV;
    ImageView imgBack, imgRight;
    RecyclerView inVoicesRV;
    AllUniversalInvoiceAdapter mUniversalAdapter;
    SwipeRefreshLayout swipeToRefresh;
    EditText editSearchET;
    ProgressBar progressBottomPB;
    ArrayList<InVoicesModel> modelArrayList = new ArrayList<InVoicesModel>();
    ArrayList<InVoicesModel> mLoadMore = new ArrayList<InVoicesModel>();
    private DownloadManager downloadManager;
    private static String strLastPage = "FALSE";
    static int IsInvoiceActive = 0;
    private long downloadReference;
    private static int page_no = 1;
    private static int OwnerID = 0;
    boolean isSwipeRefresh = false;
    NestedScrollView invoiceNestedScroll;
    DataItem mTermModel = new DataItem();

    private String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    public final int REQUEST_PERMISSIONS = 1;
    String mPDF = "", mPDFName = "";

    private ArrayList<InVoicesModel> multiSelectArrayList = new ArrayList<InVoicesModel>();
    SendMultipleInvoiceMAIlInterface mMultimaIlInterface = new SendMultipleInvoiceMAIlInterface() {
        @Override
        public void mSendMutliInvoice(InVoicesModel mInVoivce, boolean mDelete) {
//            if (mInVoivce != null) {
//
//                if (!mDelete) {
//
//                    IsInvoiceActive = IsInvoiceActive + 1;
//                    multiSelectArrayList.add(mInVoivce);
//                    String strID = mInVoivce.getInvoice_number();
//
//
//                } else {
//                    IsInvoiceActive = IsInvoiceActive - 1;
//                    multiSelectArrayList.remove(mInVoivce);
//                    String strID = mInVoivce.getInvoice_number();
//
//
//                }
//
//
//                if (IsInvoiceActive > 0) {
//                    txtMailTV.setVisibility(View.VISIBLE);
//                } else {
//                    txtMailTV.setVisibility(View.GONE);
//                }
//
//            }
        }
    };

    SinglemailInvoiceInterface msinglemailInvoiceInterface = new SinglemailInvoiceInterface() {
        @Override
        public void mSinglemailInvoice(InVoicesModel mInvoiceModel) {
            String strInvoiceNumber = mInvoiceModel.getInvoice_number();
            if (mInvoiceModel.getmVesselSearchInvoiceModel() != null) {

                String strInvoiceVesselName = "   " + mInvoiceModel.getmVesselSearchInvoiceModel().getVessel_name();

                /*Send Email*/
                Intent mIntent = new Intent(getActivity(), MailUniversalInvoiceActivity.class);
                mIntent.putExtra("SubjectName", "Invoice " + strInvoiceNumber + strInvoiceVesselName);
                mIntent.putExtra("vesselIDUniversal", mInvoiceModel.getInvoice_id());
                mIntent.putExtra("OwnerID", mInvoiceModel.getOwner_id());
                startActivity(mIntent);
            } else {

                /*Send Email*/
                Intent mIntent = new Intent(getActivity(), MailUniversalInvoiceActivity.class);
                mIntent.putExtra("SubjectName", "Invoice  " + strInvoiceNumber);
                mIntent.putExtra("vesselIDUniversal", mInvoiceModel.getInvoice_id());
                mIntent.putExtra("OwnerID", mInvoiceModel.getOwner_id());
                startActivity(mIntent);
            }
        }
    };



    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS);
    }

    private void executePDFWithPermission(String position) {
        if (checkPermission()) {
            mDownloadPDFMethod(mPDF, mPDFName,position);
        } else {
            requestPermission();
        }
    }

    private void mDownloadPDFMethod(String mPDF, String mPDFName,String position) {
        showProgressDialog(getActivity());
        downloadPDF(mPDF, outputPath(), mPDFName,position);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void saveFileUsingMediaStore(Context context, String url, String fileName) {
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/jaoharr");
        folder.mkdir();
        File file = new File(folder, fileName);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        AllInVoicesFragment.Downloader.DownloadFile(url, file);
    }


    /* get path of pdf */
    private String outputPath() {
//        String path =
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                        .toString();
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath()
                + "/jaohar");
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        } else {
            folder.delete();
            folder.mkdirs();
        }
        return path;
    }

    private void downloadPDF(String url, String dirPath, String fileName,String position) {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("PRDOWNLOADER", "Download completed at::::" + dirPath);
                        hideProgressDialog();
                        showPDFAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Downloaded Successfully!",position);
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("PRDOWNLOADER", "Download failed at::::" + dirPath);
                        hideProgressDialog();
                        showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "PDF format is not correct!");
                    }
                });
    }



    /*
     * Show Alert Dailog Box
     * */
    public void showPDFAlertDialog(Activity mActivity, String strTitle, String strMessage,String position) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
//                mIntent.putExtra("SubjectName", "Invoice    " +  modelArrayList.get(position).getInvoice_number() +" "+modelArrayList.get(position).getmVesselSearchInvoiceModel().getVessel_name());
//                mIntent.putExtra("vesselID", modelArrayList.get(position).getInvoice_id());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        JaoharConstants.Universal_Invoice_ID = modelArrayList.get(Integer.parseInt(position)).getInvoice_id();
                        mIntent.putExtra("Model", modelArrayList.get(Integer.parseInt(position)));
                        mIntent.putExtra("from", "invoice");
                        startActivity(mIntent);
                    }
                },500);

//                mIntent.putExtra("Model", modelArrayList);

            }
        });
        alertDialog.show();
    }


    private void PerformOptionsClick(int position, String strPDF, String strPDFNAme) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_invoice_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedCompanyTV.getText().toString().equals("")||selectedCompanyTV.getText().toString().equals(null)){
                    showAlertDialog(getActivity(),getString(R.string.app_name),"Please select the Invoicing Company to edit Universal Invoice");
                    dialog.dismiss();
                }
                else{
                dialog.dismiss();
                JaoharConstants.IS_Click_From_Edit = true;
                JaoharConstants.IS_Click_From_Copy = false;
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.Universal_Invoice_ID = modelArrayList.get(position).getInvoice_id();
                Intent mIntent = new Intent(getActivity(), EditUniversalInvoiceActivity.class);
                mIntent.putExtra("owner_id", OwnerID);
                mIntent.putExtra("Model", modelArrayList.get(position));
                startActivity(mIntent);
            }}
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.IS_Click_From_Edit = false;
                JaoharConstants.IS_Click_From_Copy = true;
                JaoharConstants.Universal_Invoice_ID = modelArrayList.get(position).getInvoice_id();
                Intent mIntent = new Intent(getActivity(), CopyUniversalInvoiceActivity.class);
                mIntent.putExtra("owner_id", OwnerID);
                mIntent.putExtra("Model", modelArrayList.get(position));
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), MailUniversalInvoiceActivity.class);
                mIntent.putExtra("SubjectName", "Invoice    " + modelArrayList.get(position).getInvoice_number() + " " + modelArrayList.get(position).getmVesselSearchInvoiceModel().getVessel_name());
                mIntent.putExtra("vesselID", modelArrayList.get(position).getInvoice_id());
                mIntent.putExtra("Model", modelArrayList.get(position));
                startActivity(mIntent);
            }
        });

        downloadRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!strPDF.equals("")) {
                    executePDFWithPermission(String.valueOf(position));
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Their is no PDF");
                }
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteConfirmDialog(modelArrayList.get(position), String.valueOf(position));
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

//
//        @RequiresApi(api = Build.VERSION_CODES.M)
//        @Override
//        public void pdfDownloadInterface(final String strPDF, final String strPDFNAme) {
//            if (!strPDF.equals("")) {
//
//                // your code
//                downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
//                Uri Download_Uri = Uri.parse(strPDF);
//                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
//                //Restrict the types of networks over which this download may proceed.
//                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
//                //Set whether this download may proceed over a roaming connection.
//                request.setAllowedOverRoaming(false);
//                //Set the title of this download, to be displayed in notifications (if enabled).
//                request.setTitle(strPDFNAme);
//                //Set a description of this download, to be displayed in notifications (if enabled)
//                request.setDescription("" + "Universal Invoice PDF is Download Please Wait...");
//                //Set the local destination for the downloaded file to a path within the application's external files directory
//                request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, strPDFNAme);
//                //Enqueue a new download and same the referenceId
//                downloadReference = downloadManager.enqueue(request);
//
//            } else {
//                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Their is no PDF");
//            }
    }

    public AllUniversalInvoiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_universal_invoice, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        setStatusBar();

        JaoharSingleton.getInstance().setSearchedVessel("");

        setViewsIDs(view);
        setClickListner();

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getString(R.string.universal_invoices));

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.ic_magnifying_glass);

        return view;
    }

    private void setViewsIDs(View view) {
        progressBottomPB = view.findViewById(R.id.progressBottomPB);
        editSearchET = (EditText) view.findViewById(R.id.editSearchET);

        txtCenter = (TextView) view.findViewById(R.id.txtCenter);
        txtMailTV = (TextView) view.findViewById(R.id.txtMailTV);
        selectedCompanyTV = (TextView) view.findViewById(R.id.selectedCompanyTV);
        mailAllInvoices = (TextView) view.findViewById(R.id.mailAllInvoices);
        txtMailTV.setVisibility(View.GONE);
        llLeftLL = (LinearLayout) view.findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) view.findViewById(R.id.imgRightLL);
        downArrowRL = (RelativeLayout) view.findViewById(R.id.downArrowRL);
        invoiceCompanyRL = (RelativeLayout) view.findViewById(R.id.invoiceCompanyRL);
        resetRL1 = (RelativeLayout) view.findViewById(R.id.resetRL1);
        imgBack = (ImageView) view.findViewById(R.id.imgBack);
        imgRight = (ImageView) view.findViewById(R.id.imgRight);
        inVoicesRV = (RecyclerView) view.findViewById(R.id.inVoicesRV);
        invoiceNestedScroll = (NestedScrollView) view.findViewById(R.id.invoiceNestedScroll);
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                modelArrayList.clear();
                editSearchET.setText("");
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });

    }

    private void setClickListner() {
        HomeActivity.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strLastPage = "FALSE";
                if (selectedCompanyTV.getText().toString().equals("")||selectedCompanyTV.getText().toString().equals(null)){
                    showAlertDialog(getActivity(),getString(R.string.app_name),"Please select the Invoicing Company to add Universal Invoice");
                }
                else{
//                    Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
                    Intent mIntent = new Intent(getActivity(), AddUniversalInvoiceActivity.class);
                    mIntent.putExtra("owner_id", OwnerID);
                    getActivity().startActivity(mIntent);
                    overridePendingTransitionEnter(getActivity());
                }
            }
        });

        HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.is_Staff_FragmentClick = true;
                if(getActivity()!=null){
                startActivity(new Intent(getActivity(), SearchUniversalInvoiceActivity.class));}
            }
        });

        invoiceCompanyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), InvoiceCompanyActivity.class);
                startActivityForResult(mIntent, 111);
            }
        });

        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });

        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelArrayList.clear();
                editSearchET.setText("");
                txtMailTV.setVisibility(View.GONE);
                //*Execute Vesseles API*//*
                page_no = 1;
                executeAPI();
            }
        });

        mailAllInvoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        invoiceNestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {

                    progressBottomPB.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                ++page_no;
                                executeAPI();
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 500);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        IsInvoiceActive = 0;
        modelArrayList.clear();
        txtMailTV.setVisibility(View.GONE);
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Vesseles API*//*
            page_no = 1;
            executeAPI();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (data != null) {
                String strOwnerName = "", strOwnerID = "";
                if (data.getStringExtra("owner_ID") != null && data.getStringExtra("owner_NAME") != null) {
                    OwnerID = Integer.parseInt(data.getStringExtra("owner_ID"));
                    strOwnerName = data.getStringExtra("owner_NAME");
                    Toast.makeText(getActivity(), strOwnerID + strOwnerName, Toast.LENGTH_SHORT).show();

                    selectedCompanyTV.setText(strOwnerName);

                    IsInvoiceActive = 0;
                    modelArrayList.clear();
                    txtMailTV.setVisibility(View.GONE);
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        //*Execute Vesseles API*//*
                        page_no = 1;
                        executeAPI();
                    }
                }
            }
        }
    }

    public void executeAPI() {
        modelArrayList.clear();
        if (strLastPage.equals("FALSE")) {
            progressBottomPB.setVisibility(View.GONE);
        } else {
            if (page_no > 1) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
            if (page_no == 1) {
                isSwipeRefresh = false;
                swipeToRefresh.setRefreshing(false);
                progressBottomPB.setVisibility(View.GONE);
            }
            if (!isSwipeRefresh) {
                progressBottomPB.setVisibility(View.GONE);
            }
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllUniversalInvoiceOwnerRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), String.valueOf(page_no), String.valueOf(OwnerID));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (!getActivity().isFinishing()) {
                AlertDialogManager.hideProgressDialog();}
                resetRL1.setVisibility(View.GONE);
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e(TAG, "*****ResponseALL****" + response);
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse(String response) {
        try {
            JSONObject mJSonObject = new JSONObject(response);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_invoices");
                if (mjsonArrayData != null) {
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        InVoicesModel mModel = new InVoicesModel();
                        JSONObject mJsonGetDATA = mJsonDATA.getJSONObject("all_data");
                        if (!mJsonGetDATA.getString("invoice_id").equals("")) {
                            mModel.setInvoice_id(mJsonGetDATA.getString("invoice_id"));
                        }
                        if (!mJsonGetDATA.getString("invoice_date").equals("")) {
                            mModel.setInvoice_date(mJsonGetDATA.getString("invoice_date"));
                        }
                        if (!mJsonGetDATA.getString("owner_id").equals("")) {
                            mModel.setOwner_id(mJsonGetDATA.getString("owner_id"));
                        }
                        if (!mJsonGetDATA.getString("invoice_no").equals("")) {
                            mModel.setInvoice_number(mJsonGetDATA.getString("invoice_no"));
                        }
                        if (!mJsonGetDATA.getString("pdf").equals("")) {
                            mModel.setPdf(mJsonGetDATA.getString("pdf"));
                        }
                        if (!mJsonGetDATA.getString("pdf_name").equals("")) {
                            mModel.setPdf_name(mJsonGetDATA.getString("pdf_name"));
                        }
                        if (!mJsonGetDATA.getString("owner_id").equals("")) {
                            mModel.setOwner_id(mJsonGetDATA.getString("owner_id"));
                        }
                        if (!mJsonGetDATA.getString("currency").equals("")) {
                            mModel.setCurrency(mJsonGetDATA.getString("currency"));
                        }
                        if (!mJsonDATA.getString("search_company_data").equals("")) {
                            JSONObject mSearchCompanyObj = mJsonDATA.getJSONObject("search_company_data");
                            CompaniesModel mCompaniesModel = new CompaniesModel();
                            if (!mSearchCompanyObj.isNull("id"))
                                mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                            if (!mSearchCompanyObj.isNull("company_name"))
                                mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                            if (!mSearchCompanyObj.isNull("Address1"))
                                mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                            if (!mSearchCompanyObj.isNull("Address2"))
                                mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                            if (!mSearchCompanyObj.isNull("Address3"))
                                mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                            if (!mSearchCompanyObj.isNull("Address4"))
                                mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                            if (!mSearchCompanyObj.isNull("Address5"))
                                mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                            mModel.setmCompaniesModel(mCompaniesModel);
                        }
                        if (!mJsonDATA.getString("search_vessel_data").equals("")) {
                            JSONObject mSearchVesselObj = mJsonDATA.getJSONObject("search_vessel_data");
                            VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                            if (!mSearchVesselObj.isNull("vessel_id"))
                                mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                            if (!mSearchVesselObj.isNull("vessel_name"))
                                mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                            if (!mSearchVesselObj.isNull("IMO_no"))
                                mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                            if (!mSearchVesselObj.isNull("flag"))
                                mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                            mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                        }
                        if (!mJsonDATA.getString("payment_data").equals("")) {
                            JSONObject mPaymentObj = mJsonDATA.getJSONObject("payment_data");
                            PaymentModel mPaymentModel = new PaymentModel();
                            if (!mPaymentObj.isNull("payment_id"))
                                mPaymentModel.setPayment_id(mPaymentObj.getString("payment_id"));
                            if (!mPaymentObj.isNull("user_id"))
                                mPaymentModel.setUser_id(mPaymentObj.getString("user_id"));
                            if (!mPaymentObj.isNull("sub_total"))
                                mPaymentModel.setSubTotal(mPaymentObj.getString("sub_total"));
                            if (!mPaymentObj.isNull("VAT"))
                                mPaymentModel.setVAT(mPaymentObj.getString("VAT"));
                            if (!mPaymentObj.isNull("vat_price"))
                                mPaymentModel.setVATPrice(mPaymentObj.getString("vat_price"));
                            if (!mPaymentObj.isNull("total"))
                                mPaymentModel.settotal(mPaymentObj.getString("total"));
                            if (!mPaymentObj.isNull("paid"))
                                mPaymentModel.setPaid(mPaymentObj.getString("paid"));
                            if (!mPaymentObj.isNull("due"))
                                mPaymentModel.setDue(mPaymentObj.getString("due"));
                            if (!mPaymentObj.isNull("status"))
                                mPaymentModel.setStatus(mPaymentObj.getString("status"));
                            mModel.setmPaymentModel(mPaymentModel);
                        }
                        if (mJsonDATA.has("stamp_data") && !mJsonDATA.get("stamp_data").equals("")) {
                            JSONObject mStampDataObj = mJsonDATA.getJSONObject("stamp_data");
                            StampsModel mStampsModel = new StampsModel();
                            if (!mStampDataObj.isNull("stamp_id"))
                                mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                            if (!mStampDataObj.isNull("stamp_name"))
                                mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                            if (!mStampDataObj.isNull("stamp_image"))
                                mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                            mModel.setmStampsModel(mStampsModel);
                        }
                        if (mJsonDATA.has("bank_data") && !mJsonDATA.getString("bank_data").equals("")) {
                            JSONObject mBankDataObj = mJsonDATA.getJSONObject("bank_data");
                            BankModel mBankModel = new BankModel();
                            if (!mBankDataObj.isNull("bank_id"))
                                mBankModel.setId(mBankDataObj.getString("bank_id"));
                            if (!mBankDataObj.isNull("beneficiary"))
                                mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                            if (!mBankDataObj.isNull("bank_name"))
                                mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                            if (!mBankDataObj.isNull("address1"))
                                mBankModel.setAddress1(mBankDataObj.getString("address1"));
                            if (!mBankDataObj.isNull("address2"))
                                mBankModel.setAddress2(mBankDataObj.getString("address2"));
                            if (!mBankDataObj.isNull("iban_ron"))
                                mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                            if (!mBankDataObj.isNull("iban_usd"))
                                mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                            if (!mBankDataObj.isNull("iban_eur"))
                                mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                            if (!mBankDataObj.isNull("iban_gbp"))
                                mBankModel.setIban_gbp(mBankDataObj.getString("iban_gbp"));
                            if (!mBankDataObj.isNull("swift"))
                                mBankModel.setSwift(mBankDataObj.getString("swift"));
                            mModel.setmBankModel(mBankModel);
                        }
                        if (mJsonDATA.has("term_data") && !mJsonDATA.getString("term_data").equals("")) {
                            JSONObject mTermObject = mJsonDATA.getJSONObject("term_data");
                            if (!mTermObject.isNull("term_id"))
                                mTermModel.setTermId(mTermObject.getString("term_id"));
                            if (!mTermObject.isNull("term_title"))
                                mTermModel.setTermTitle(mTermObject.getString("term_title"));
                            if (!mTermObject.isNull("term_desc"))
                                mTermModel.setTermDesc(mTermObject.getString("term_desc"));
                            if (!mTermObject.isNull("added_by"))
                                mTermModel.setAddedBy(mTermObject.getString("added_by"));
                            if (!mTermObject.isNull("modification_date"))
                                mTermModel.setModificationDate(mTermObject.getString("modification_date"));
                            if (!mTermObject.isNull("enable"))
                                mTermModel.setEnable(mTermObject.getString("enable"));
                            mModel.setmUniversalModel(mTermModel);
                            mModel.setTerm_id(mTermModel.getTermId());
                        }
                        if (mJsonDATA.has("sign_data") && !mJsonDATA.getString("sign_data").equals("")) {
                            JSONObject mSignDataObj = mJsonDATA.getJSONObject("sign_data");
                            SignatureModel mSignModel = new SignatureModel();
                            if (!mSignDataObj.isNull("sign_id"))
                                mSignModel.setId(mSignDataObj.getString("sign_id"));
                            if (!mSignDataObj.isNull("sign_name"))
                                mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                            if (!mSignDataObj.isNull("sign_image"))
                                mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                            mModel.setmSignatureModel(mSignModel);
                        }
                        if (mJsonDATA.has("itemdiscount") && !mJsonDATA.getString("itemdiscount").equals("")) {
                            JSONArray mItemDiscountArray = mJsonDATA.getJSONArray("itemdiscount");

                            for (int k = 0; k < mItemDiscountArray.length(); k++) {
                                AddTotalInvoiceDiscountModel mTotalDiscountModel = new AddTotalInvoiceDiscountModel();
                                JSONObject mItemObJDiscount = mItemDiscountArray.getJSONObject(k);

                                if (!mItemObJDiscount.getString("discount_id").equals("")) {
                                    mTotalDiscountModel.setDiscount_id(mItemObJDiscount.getString("discount_id"));
                                }
                                if (!mItemObJDiscount.getString("discount_subtract_description").equals("")) {
                                    mTotalDiscountModel.setSubtract_Description(mItemObJDiscount.getString("discount_subtract_description"));
                                }
                                if (!mItemObJDiscount.getString("discount_subtract_value").equals("")) {
                                    mTotalDiscountModel.setSubtract_DiscountValue(mItemObJDiscount.getString("discount_subtract_value"));
                                }
                                if (!mItemObJDiscount.getString("discount_subtract_unitprice").equals("")) {
                                    mTotalDiscountModel.setSubtract_UnitPrice(Double.parseDouble(mItemObJDiscount.getString("discount_subtract_unitprice")));
                                }
                                if (!mItemObJDiscount.getString("discount_add_value").equals("")) {
                                    mTotalDiscountModel.setADD_DiscountValue(mItemObJDiscount.getString("discount_add_value"));
                                }
                                if (!mItemObJDiscount.getString("discount_add_description").equals("")) {
                                    mTotalDiscountModel.setADD_Description(mItemObJDiscount.getString("discount_add_description"));
                                }
                                if (!mItemObJDiscount.getString("discount_add_unitprice").equals("")) {

                                    mTotalDiscountModel.setADD_UnitPrice(Double.parseDouble(mItemObJDiscount.getString("discount_add_unitprice")));
                                }
                                if (!mItemObJDiscount.getString("discount_total_value").equals("")) {
                                    mTotalDiscountModel.setStrTotalvalue(mItemObJDiscount.getString("discount_total_value"));
                                }
                                if (!mItemObJDiscount.getString("discount_type").equals("")) {
                                    mTotalDiscountModel.setDiscountType(mItemObJDiscount.getString("discount_type"));
                                }
                                if (!mItemObJDiscount.getString("discount_percent").equals("")) {
                                    mTotalDiscountModel.setStrAddAndSubtracttotalPercent(mItemObJDiscount.getString("discount_percent"));
                                }
                                if (!mItemObJDiscount.getString("discount_add_and_subtract_total").equals("")) {
                                    mTotalDiscountModel.setAddAndSubtractTotal(Double.parseDouble(mItemObJDiscount.getString("discount_add_and_subtract_total")));
                                }
                            }
                        }

                        if (page_no == 1) {
                            modelArrayList.add(mModel);
                        } else if (page_no > 1) {
                            mLoadMore.add(mModel);
                        }
                    }
                    if (mLoadMore.size() > 0) {
                        modelArrayList.addAll(mLoadMore);
                    }
                    setAdapter();
                }

            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    DeleteInvoiceInterface mDeleteInvoiceInterface = new DeleteInvoiceInterface() {
        @Override
        public void deleteInvoice(InVoicesModel mInVoicesModel) {
//            deleteConfirmDialog(mInVoicesModel);
        }
    };


    PDFdownloadInterface mPdfDownloader = new PDFdownloadInterface() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void pdfDownloadInterface(final String strPDF, final String strPDFNAme) {
            if (!strPDF.equals("")) {
                // your code
                downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(strPDF);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(strPDFNAme);
                //Set a description of this download, to be displayed in notifications (if enabled)
                request.setDescription("" + "Invoice PDF is Download Please Wait...");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);
            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), "Their is no PDF");
            }
        }
    };


    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            Log.e(TAG, "mOnClickInterface: "+modelArrayList.size() );
            PerformOptionsClick(position, modelArrayList.get(position).getPdf(), modelArrayList.get(position).getPdf_name());
            mPDF = modelArrayList.get(position).getPdf();
            Log.e(TAG, "mOnClickInterface: "+mPDF );
            mPDFName = modelArrayList.get(position).getPdf_name();
            Log.e(TAG, "mOnClickInterface: "+mPDFName );
        }
    };


    private void setAdapter() {
        inVoicesRV.setNestedScrollingEnabled(false);
        inVoicesRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mUniversalAdapter = new AllUniversalInvoiceAdapter(getActivity(), modelArrayList, mDeleteInvoiceInterface, mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface,mOnClickInterface);
        inVoicesRV.setAdapter(mUniversalAdapter);
        mUniversalAdapter.notifyDataSetChanged();
    }

    public void deleteConfirmDialog(final InVoicesModel mInVoicesModel,String position) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteApi(mInVoicesModel.getInvoice_id(),position);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    private void executeDeleteApi(String strInvoiceID,String position) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteUniversalInvoice(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), strInvoiceID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        resetRL1.setVisibility(View.GONE);
                        progressBottomPB.setVisibility(View.GONE);
                        Log.e(TAG, "*****Response****" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mLoadMore.clear();
                            modelArrayList.clear();
                            page_no = 1;
                            executeAPI();
                        } else {
                            AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }

    /*
     * Serach API Implementation
     * */
    private void executeNormalSearch() {
        modelArrayList.clear();
        resetRL1.setVisibility(View.VISIBLE);
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.simpleSearchUniversalInvoiceRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), editSearchET.getText().toString(), "staff");
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.GONE);
                progressBottomPB.setVisibility(View.GONE);
                Log.e(TAG, "*****Response****" + response);
                try {
                    AlertDialogManager.hideProgressDialog();
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    String strMessage = mJsonObject.getString("message");
                    if (strStatus.equals("1")) {
                        parseResponseSearch(response.body().toString());
                    } else {
                        AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), strMessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponseSearch(String response) {
        try {
            JSONObject mJSonObject = new JSONObject(response);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
//                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
//                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mJSonObject.getJSONArray("all_searched_invoices");
                if (mjsonArrayData != null) {
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        InVoicesModel mModel = new InVoicesModel();
                        JSONObject mJsonGetDATA = mJsonDATA.getJSONObject("all_data");
                        if (!mJsonGetDATA.getString("invoice_id").equals("")) {
                            mModel.setInvoice_id(mJsonGetDATA.getString("invoice_id"));
                        }
                        if (!mJsonGetDATA.getString("invoice_date").equals("")) {
                            mModel.setInvoice_date(mJsonGetDATA.getString("invoice_date"));
                        }
                        if (!mJsonGetDATA.getString("owner_id").equals("")) {
                            mModel.setOwner_id(mJsonGetDATA.getString("owner_id"));
                        }
                        if (!mJsonGetDATA.getString("invoice_no").equals("")) {
                            mModel.setInvoice_number(mJsonGetDATA.getString("invoice_no"));
                        }
                        if (!mJsonGetDATA.getString("pdf").equals("")) {
                            mModel.setPdf(mJsonGetDATA.getString("pdf"));
                        }
                        if (!mJsonGetDATA.getString("pdf_name").equals("")) {
                            mModel.setPdf_name(mJsonGetDATA.getString("pdf_name"));
                        }
                        if (!mJsonGetDATA.getString("currency").equals("")) {
                            mModel.setCurrency(mJsonGetDATA.getString("currency"));
                        }
                        if (!mJsonDATA.getString("search_company_data").equals("")) {
                            JSONObject mSearchCompanyObj = mJsonDATA.getJSONObject("search_company_data");
                            CompaniesModel mCompaniesModel = new CompaniesModel();
                            if (!mSearchCompanyObj.isNull("id"))
                                mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                            if (!mSearchCompanyObj.isNull("company_name"))
                                mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                            if (!mSearchCompanyObj.isNull("Address1"))
                                mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                            if (!mSearchCompanyObj.isNull("Address2"))
                                mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                            if (!mSearchCompanyObj.isNull("Address3"))
                                mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                            if (!mSearchCompanyObj.isNull("Address4"))
                                mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                            if (!mSearchCompanyObj.isNull("Address5"))
                                mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                            mModel.setmCompaniesModel(mCompaniesModel);
                        }
                        if (!mJsonDATA.getString("search_vessel_data").equals("")) {
                            JSONObject mSearchVesselObj = mJsonDATA.getJSONObject("search_vessel_data");
                            VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                            if (!mSearchVesselObj.isNull("vessel_id"))
                                mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                            if (!mSearchVesselObj.isNull("vessel_name"))
                                mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                            if (!mSearchVesselObj.isNull("IMO_no"))
                                mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                            if (!mSearchVesselObj.isNull("flag"))
                                mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                            mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                        }
                        if (!mJsonDATA.getString("payment_data").equals("")) {
                            JSONObject mPaymentObj = mJsonDATA.getJSONObject("payment_data");
                            PaymentModel mPaymentModel = new PaymentModel();
                            if (!mPaymentObj.isNull("payment_id"))
                                mPaymentModel.setPayment_id(mPaymentObj.getString("payment_id"));
                            if (!mPaymentObj.isNull("user_id"))
                                mPaymentModel.setUser_id(mPaymentObj.getString("user_id"));
                            if (!mPaymentObj.isNull("sub_total"))
                                mPaymentModel.setSubTotal(mPaymentObj.getString("sub_total"));
                            if (!mPaymentObj.isNull("VAT"))
                                mPaymentModel.setVAT(mPaymentObj.getString("VAT"));
                            if (!mPaymentObj.isNull("vat_price"))
                                mPaymentModel.setVATPrice(mPaymentObj.getString("vat_price"));
                            if (!mPaymentObj.isNull("total"))
                                mPaymentModel.settotal(mPaymentObj.getString("total"));
                            if (!mPaymentObj.isNull("paid"))
                                mPaymentModel.setPaid(mPaymentObj.getString("paid"));
                            if (!mPaymentObj.isNull("due"))
                                mPaymentModel.setDue(mPaymentObj.getString("due"));
                            if (!mPaymentObj.isNull("status"))
                                mPaymentModel.setStatus(mPaymentObj.getString("status"));
                            mModel.setmPaymentModel(mPaymentModel);
                        }
                        if (page_no == 1) {
                            modelArrayList.add(mModel);
                        } else if (page_no > 1) {
                            mLoadMore.add(mModel);
                        }
                    }
                    if (mLoadMore.size() > 0) {
                        modelArrayList.addAll(mLoadMore);
                    }
                    setAdapter();
                }

            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();}

}