package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static android.view.View.GONE;
import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.Collator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddVesselActivity;
import jaohar.com.jaohar.activities.DetailsActivity;
import jaohar.com.jaohar.activities.EditVesselActivity;
import jaohar.com.jaohar.activities.GalleryActivity;
import jaohar.com.jaohar.activities.SearchVesselsActivity;
import jaohar.com.jaohar.activities.SendingMailActivity;
import jaohar.com.jaohar.activities.mail_list_all_activities.SendingMailToLIst_Vessels;
import jaohar.com.jaohar.activities.vessels_module.CopyVesselActivity;
import jaohar.com.jaohar.adapters.UserNewsAdapter;
import jaohar.com.jaohar.adapters.VesselsAdapter;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.beans.VessalClassModel;
import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.DeleteVesselsInterface;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.MailSelectedVesselsInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.SendEmailInterface;
import jaohar.com.jaohar.interfaces.SendMultiVesselDataInterface;
import jaohar.com.jaohar.interfaces.SendingMailToVesselInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.models.GetVesselClassData;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.SearchModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.VesselForSaleModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllVesselsFragment extends BaseFragment {
    String TAG = AllVesselsFragment.this.getClass().getSimpleName();
    Activity activity = getActivity();
    /*unbinder*/
    Unbinder unbinder;

    public String strCapacityFrom, strCapacityTo, strName, strLoaFrom, strLoaTo, strType, strGrainFrom, strGrainTo, strClass, strYearofBuildFrom,
            strYearofBuildTo, strBaleFrom, strBaleTo, strPlaceBuild, strRecordID, IMO_number, strGeared, strStatus, strTEUFrom, strTEUto, strPassangerFrom,
            strPassangerTo, strDraftsFrom, strDraftsTo, strLiquidFrom, strLiquidTo, strTextNEWS = "", strOffMarket = "";
    public String strLastPage = "FALSE", strPhotoURL = "", strNormalTEXT;

    private LinearLayout mailSelectedLL;
    private RelativeLayout downArrowRL, resetRL1;
    private WebView scrollTextTV;
    private EditText editSearchET;
    private TextView txtMailTV, mailSelectedTV;
    private RecyclerView vesselsRV;
    private SwipeRefreshLayout swipeToRefresh;
    private VesselsAdapter mVesselsAdapter;
    private ArrayList<AllVessel> mArrayList = new ArrayList<>();
    private ArrayList<DocumentModel> mDocArrayList = new ArrayList<>();
    private ArrayList<AllVessel> loadMoreArrayList = new ArrayList<>();
    private ArrayList<String> modellArrayList = new ArrayList<>();
    private ArrayList<String> mArrayListtClass = new ArrayList<>();
    private int page_no = 1;
    String stafff;

    private boolean isSwipeRefresh = false;
    private boolean isAdvanceSearch = false;
    private boolean isNormalSearch = false;
    static int IsVessalActive = 0;

    private String arrayStatus[];
    private ArrayList<GetVesselClassData> modelArrayList = new ArrayList<>();
    private ArrayList<GetVesselTypeData> mArrayListClass = new ArrayList<>();
    private ArrayList<String> mRecordID = new ArrayList<>();
    private ProgressBar progressBottomPB;
    private ArrayList<String> mImageArryList = new ArrayList<>();
    private String UserId = "";

    private RecyclerView newsRV;
    private UserNewsAdapter mNewsAdapter;
    boolean isOpenDialogBOX = false;
    private String strSearchText;

    //for pagination
    private boolean isScrolling = false;
    private LinearLayoutManager linearLayoutManager;
    private NestedScrollView nestedScrollView;

    private ArrayList<AllVessel> multiSelectArrayList = new ArrayList<AllVessel>();
    private ArrayList<Datum> mNewsArrayList = new ArrayList<Datum>();

    OpenNewsPopUpInterFace mOpenNewsPopUP = new OpenNewsPopUpInterFace() {
        @Override
        public void openNewsPopUpInterFace(String strWebTxt, String strPhotoURL) {
            if (isOpenDialogBOX == true) {
                isOpenDialogBOX = false;
            } else {
                setUpNewsDialog(strWebTxt, strPhotoURL);
            }
        }
    };

    FavoriteVesselInterface mFavoriteVesselInterface = new FavoriteVesselInterface() {
        @Override
        public void mFavoriteVesselInterface(int position, String status, ImageView imageView) {
            String vessel_id = mArrayList.get(position).getRecordId();
            if (status.equals("1")) {
                executeFavoriteAPI(vessel_id, imageView, position);
            } else if (status.equals("0")) {
                executeUnFavoriteAPI(vessel_id, imageView, position);
            }
        }
    };

    DeleteVesselsInterface mDeleteVesselsInterface = new DeleteVesselsInterface() {
        @Override
        public void mDeleteVessel(String mVesselID) {
            deleteConfirmDialog(mVesselID);
        }
    };

    SendingMailToVesselInterface mVesselToMail = new SendingMailToVesselInterface() {
        @Override
        public void mSendingMailVessel(AllVessel mModel) {
            /*Send Email Using Mail to List*/
            JaoharConstants.IsAllVesselsClick = true;
            Intent mIntent = new Intent(getActivity(), SendingMailToLIst_Vessels.class);
            mIntent.putExtra("vessalName", "ID " + mModel.getRecordId() + "  " + "-" + "  " + mModel.getVesselName() + ", IMO No. " + mModel.getIMONumber());
            mIntent.putExtra("recordID", mModel.getRecordId());
            startActivity(mIntent);
        }
    };

    SendEmailInterface mSendEmailInterface = new SendEmailInterface() {
        @Override
        public void sendEmail(AllVessel mVesselesModel) {
            /*Send Email*/
            Intent mIntent = new Intent(getActivity(), SendingMailActivity.class);
            mIntent.putExtra("vessalName", "ID " + mVesselesModel.getRecordId() + "  " + "-" + "  " + mVesselesModel.getVesselName() + ", IMO No. " + mVesselesModel.getIMONumber());
            mIntent.putExtra("recordID", mVesselesModel.getRecordId());
            startActivity(mIntent);
        }
    };

    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                if (isAdvanceSearch == false) {
                    if (isNormalSearch == false) {

                        progressBottomPB.setVisibility(View.VISIBLE);

                        ++page_no;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (strLastPage.equals("FALSE")) {
                                    executeAPI(page_no);
                                } else {
                                    progressBottomPB.setVisibility(View.GONE);
                                }
                            }
                        }, 1000);
                    }
                }
            }
        }
    };

    SendMultiVesselDataInterface mSendMultiVesselDataInterface = new SendMultiVesselDataInterface() {
        @Override
        public void sendMultipleVesselsData(AllVessel mVesselesModel, boolean mDelete) {
            if (mVesselesModel != null) {
                if (!mDelete) {
                    IsVessalActive = IsVessalActive + 1;
                    multiSelectArrayList.add(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.add(mVesselesModel.getRecordId());
                } else {
                    IsVessalActive = IsVessalActive - 1;
                    multiSelectArrayList.remove(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.remove(mVesselesModel.getRecordId());
                }
                if (IsVessalActive > 0) {
                    txtMailTV.setVisibility(View.VISIBLE);
                } else {
                    txtMailTV.setVisibility(View.GONE);
                }
            }
        }
    };

    OpenPopUpVesselInterface mOpenPoUpInterface = new OpenPopUpVesselInterface() {
        @Override
        public void mOpenPopUpVesselInterface(String strTextnews, String strPhotoURLs) {
            JaoharConstants.IS_VESSAL_ACTIVITY = true;
            setUpNewsDialog(strTextnews, strPhotoURLs);
        }
    };

    MailSelectedVesselsInterface mMailSelectedVesselsInterface = new MailSelectedVesselsInterface() {
        @Override
        public void MailSelectedVessels(final int position, String Vessel_id, ArrayList<String> Items,
                                        AllVessel mVesselesModel, boolean b) {

            if (mVesselesModel != null) {
                if (!b) {
                    IsVessalActive = IsVessalActive + 1;
                    multiSelectArrayList.add(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.add(mVesselesModel.getRecordId());
                } else {
                    IsVessalActive = IsVessalActive - 1;
                    multiSelectArrayList.remove(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.remove(mVesselesModel.getRecordId());
                }
            }

            if (Items != null && Items.size() > 0) {
                mailSelectedLL.setVisibility(View.VISIBLE);

                mailSelectedTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JaoharConstants.IsAllVesselsClick = true;
                        Intent mIntent = new Intent(getActivity(), SendingMailToLIst_Vessels.class);
                        mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                        mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                        startActivity(mIntent);
                    }
                });

            } else {
                mailSelectedLL.setVisibility(View.GONE);
            }
        }
    };

    public AllVesselsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_vessels, container, false);

        setStatusBar();

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        JaoharSingleton.getInstance().setSearchedVessel("");
        JaoharSingleton.getInstance().setmAllVessel(null);
        JaoharSingleton.getInstance().setmAllDocument(null);

        setViewData(view);
        setClickListner();

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.imgAdd.setVisibility(View.VISIBLE);
        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.all_vessels));

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.ic_magnifying_glass);
        if (getActivity() != null) {
            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                    UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
                } else {
                    UserId = JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "");
                }
            } else {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
            }
        }

//        if (Utilities.isNetworkAvailable(getActivity()) == false) {
//            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
//        } else {
//            //execute API
//            if (mArrayList != null)
//                mArrayList.clear();
//            if (loadMoreArrayList != null)
//                loadMoreArrayList.clear();
//
//            page_no = 1;
//            executeGettingAllNews();
//        }

        return view;
    }

    private void setViewData(View view) {
        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        mailSelectedTV = view.findViewById(R.id.mailSelectedTV);
        mailSelectedLL = view.findViewById(R.id.mailSelectedLL);
        progressBottomPB = view.findViewById(R.id.progressBottomPB);
        editSearchET = view.findViewById(R.id.editSearchET);
        scrollTextTV = view.findViewById(R.id.scrollTextTV);
        downArrowRL = view.findViewById(R.id.downArrowRL);
        resetRL1 = view.findViewById(R.id.resetRL1);
        vesselsRV = view.findViewById(R.id.vesselsRV);
        newsRV = view.findViewById(R.id.newsRV);
        txtMailTV = view.findViewById(R.id.txtMailTV);

        swipeToRefresh = view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isAdvanceSearch = false;
                isNormalSearch = false;
                mArrayList.clear();
                loadMoreArrayList.clear();
                page_no = 1;
                editSearchET.setText("");
                IsVessalActive = 0;
                txtMailTV.setVisibility(View.GONE);
                executeAPI(page_no);
            }
        });

        IsVessalActive = 0;
        txtMailTV.setVisibility(View.GONE);

        scrollTextViewMethod(view);
    }

    private void scrollTextViewMethod(View view) {
        final TextView first = view.findViewById(R.id.first);
        final TextView second = view.findViewById(R.id.second);

        final ValueAnimator animator = ValueAnimator.ofFloat(1.0f, 0.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(9000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = first.getWidth();
                final float translationX = width * progress;
                first.setTranslationX(translationX);
                second.setTranslationX(translationX - width);
            }
        });
        animator.start();
    }

    private void setClickListner() {
        HomeActivity.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    Intent mIntent = new Intent(getActivity(), AddVesselActivity.class);
                    mIntent.putExtra("From", "AllVesselsActivity");
                    getActivity().startActivity(mIntent);
                    overridePendingTransitionEnter(getActivity());
                }
            }
        });

        try {
            HomeActivity.imgRightLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    startActivity(new Intent(getActivity(), SearchVesselsActivity.class)
                            .putExtra(JaoharConstants.SearchType, JaoharConstants.AllVesselType));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    resetRL1.setVisibility(View.VISIBLE);
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });

        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gettingVessal_Type();
                gettingVessal_Class();
                mArrayList.clear();
                loadMoreArrayList.clear();
                advancedSearchView();
            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsVessalActive = 0;
                isAdvanceSearch = false;
                isNormalSearch = false;
                txtMailTV.setVisibility(View.GONE);
                mArrayList.clear();
                loadMoreArrayList.clear();
                editSearchET.setText("");
                page_no = 1;
                executeAPI(page_no);
            }
        });


        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Send Email*/
                Intent mIntent = new Intent(getActivity(), SendingMailActivity.class);
                mIntent.putExtra("vessalName1", "Your Requested list of Vessels from Jaohar UK Limited");
                mIntent.putExtra("recordIDArray", getMultiVesselDetailsData());
                startActivity(mIntent);
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        // disable scroll on touch
        scrollTextTV.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

        scrollTextTV.loadDataWithBaseURL(null, "Site is now up to date.", "text/html", "UTF-8", null);
    }

    public void setUpNewsDialog(final String strNormalTEXT, final String strPhotoURL) {
        final Dialog searchDialog = new Dialog(getActivity());
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.activity_full_news_description);
        searchDialog.setCanceledOnTouchOutside(true);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        ImageView add_image1 = (ImageView) searchDialog.findViewById(R.id.add_image1);
        final WebView showTXT = (WebView) searchDialog.findViewById(R.id.showTXT);
        final ProgressBar progressbar1 = searchDialog.findViewById(R.id.progressbar1);
        progressbar1.setVisibility(View.VISIBLE);
        if (!strPhotoURL.equals("")) {
            Glide.with(getActivity()).load(strPhotoURL).into(add_image1);
        }
        add_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!strPhotoURL.equals("")) {
                    mImageArryList.clear();
                    mImageArryList.add(strPhotoURL);
                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putStringArrayListExtra("LIST", mImageArryList);
                    startActivity(intent);
                }
            }
        });
        showTXT.getSettings().setJavaScriptEnabled(true);
        showTXT.getSettings().setLoadWithOverviewMode(true);
        showTXT.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressbar1.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progressbar1.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressbar1.setVisibility(View.GONE);
            }
        });

        showTXT.loadDataWithBaseURL(null, strNormalTEXT, "text/html", "UTF-8", null);
        WebSettings webSettings = showTXT.getSettings();
        Resources res = getActivity().getResources();
        webSettings.setDefaultFontSize((int) res.getDimension(R.dimen._3sdp));
        searchDialog.show();
    }

    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mRecordID.size());
        for (int i = 0; i < mRecordID.size(); i++) {
            strData.add(mRecordID.get(i));
        }
        mRecordID.clear();
        return strData;
    }


    private void gettingVessal_Class() {
        modelArrayList.clear();
        modellArrayList.clear();
//        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselClassModel> call1 = mApiInterface.getVesselClassRequest();
        call1.enqueue(new Callback<GetVesselClassModel>() {
                          @Override
                          public void onResponse(Call<GetVesselClassModel> call, retrofit2.Response<GetVesselClassModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselClassModel mModel = response.body();
                              if (mModel.getStatus().equals("1")) {
                                  modelArrayList = mModel.getData();
                                  for (int i = 0; i < modelArrayList.size(); i++) {
                                      modellArrayList.add(mModel.getData().get(i).getClassName());
                                  }
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselClassModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void gettingVessal_Type() {
        mArrayListClass.clear();
        mArrayListtClass.clear();
//        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
                          @Override
                          public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselTypeModel mModel = response.body();
                              if (mModel.getStatus().equals("1")) {
                                  mArrayListClass = mModel.getData();
                                  for (int i = 0; i < mArrayListClass.size(); i++) {
                                      mArrayListtClass.add(mModel.getData().get(i).getName());
                                  }
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    //    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onResume() {
        super.onResume();
//        if (mDocArrayList!=null){
//            mDocArrayList.clear();
//        }
//        if (JaoharSingleton.getInstance().getDocumentArrayList() != null) {
//        mDocArrayList = JaoharSingleton.getInstance().getDocumentArrayList();}
//        Log.e(TAG, "mDocArrayList: "+mDocArrayList );
//        AllVessel mAllVessel = new AllVessel();
//        boolean isEditVessel = false;
//        if (JaoharSingleton.getInstance().getmAllVessel() != null) {
//            mAllVessel = JaoharSingleton.getInstance().getmAllVessel();
//            if (JaoharSingleton.getInstance().getDocumentArrayList() != null) {
//                if(JaoharSingleton.getInstance().getDocumentArrayList().size()!=0){
//                if (mAllVessel.getDocument1() != null) {
//                    mAllVessel.setDocument1(null);
//                    mAllVessel.setDocument1name(null);
//                }
//                if (mAllVessel.getDocument2() != null) {
//                    mAllVessel.setDocument2(null);
//                    mAllVessel.setDocument2name(null);
//                }
//                if (mAllVessel.getDocument3() != null) {
//                    mAllVessel.setDocument3(null);
//                    mAllVessel.setDocument3name(null);
//                }
//                if (mAllVessel.getDocument4() != null) {
//                    mAllVessel.setDocument4(null);
//                    mAllVessel.setDocument4name(null);
//                }
//                if (mAllVessel.getDocument5() != null) {
//                    mAllVessel.setDocument5(null);
//                    mAllVessel.setDocument5name(null);
//                }
//                if (mAllVessel.getDocument6() != null) {
//                    mAllVessel.setDocument6(null);
//                    mAllVessel.setDocument6name(null);
//                }
//                if (mAllVessel.getDocument7() != null) {
//                    mAllVessel.setDocument7(null);
//                    mAllVessel.setDocument7name(null);
//                }
//                if (mAllVessel.getDocument8() != null) {
//                    mAllVessel.setDocument8(null);
//                    mAllVessel.setDocument8name(null);
//                }
//                if (mAllVessel.getDocument9() != null) {
//                    mAllVessel.setDocument9(null);
//                    mAllVessel.setDocument9name(null);
//                }
//                if (mAllVessel.getDocument10() != null) {
//                    mAllVessel.setDocument10(null);
//                    mAllVessel.setDocument10name(null);
//                }}
//            }
//            if (mDocArrayList!=null) {
//                if (mDocArrayList.size()==1){
//                if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                }
//                if (mDocArrayList.size()==2){
//                       if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//                }
//               }
//                if (mDocArrayList.size()==3){
//
//                 if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                }
//                if (mDocArrayList.get(2).getDocumentPath()!=null){
//                    mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                    mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                }
//
//                }
//                if (mDocArrayList.size()==4){
//                       if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                }
//                if (mDocArrayList.get(2).getDocumentPath()!=null){
//                    mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                    mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                }
//
//                if (mDocArrayList.get(3).getDocumentPath()!=null) {
//                    mAllVessel.setDocument4(mDocArrayList.get(3).getDocumentPath());
//                    mAllVessel.setDocument4name(mDocArrayList.get(3).getDocumentName());
//
//                }
//            }
//                if (mDocArrayList.size()==5){
//                     if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                }
//                if (mDocArrayList.get(2).getDocumentPath()!=null){
//                    mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                    mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                }
//
//                if (mDocArrayList.get(3).getDocumentPath()!=null) {
//                    mAllVessel.setDocument4(mDocArrayList.get(3).getDocumentPath());
//                    mAllVessel.setDocument4name(mDocArrayList.get(3).getDocumentName());
//
//                }
//                if (mDocArrayList.get(4).getDocumentPath()!=null) {
//                    mAllVessel.setDocument5(mDocArrayList.get(4).getDocumentPath());
//                    mAllVessel.setDocument5name(mDocArrayList.get(4).getDocumentName());
//
//                }
//            }
//                if (mDocArrayList.size()==6){
//                         if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                }
//                if (mDocArrayList.get(2).getDocumentPath()!=null){
//                    mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                    mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                }
//
//                if (mDocArrayList.get(3).getDocumentPath()!=null) {
//                    mAllVessel.setDocument4(mDocArrayList.get(3).getDocumentPath());
//                    mAllVessel.setDocument4name(mDocArrayList.get(3).getDocumentName());
//
//                }
//                if (mDocArrayList.get(4).getDocumentPath()!=null) {
//                    mAllVessel.setDocument5(mDocArrayList.get(4).getDocumentPath());
//                    mAllVessel.setDocument5name(mDocArrayList.get(4).getDocumentName());
//
//                }
//
//                if (mDocArrayList.get(5).getDocumentPath()!=null) {
//                    mAllVessel.setDocument6(mDocArrayList.get(5).getDocumentPath());
//                    mAllVessel.setDocument6name(mDocArrayList.get(5).getDocumentName());
//                }
//                }
//
//                if (mDocArrayList.size()==7){
//                       if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                }
//                if (mDocArrayList.get(2).getDocumentPath()!=null){
//                    mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                    mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                }
//
//                if (mDocArrayList.get(3).getDocumentPath()!=null) {
//                    mAllVessel.setDocument4(mDocArrayList.get(3).getDocumentPath());
//                    mAllVessel.setDocument4name(mDocArrayList.get(3).getDocumentName());
//
//                }
//                if (mDocArrayList.get(4).getDocumentPath()!=null) {
//                    mAllVessel.setDocument5(mDocArrayList.get(4).getDocumentPath());
//                    mAllVessel.setDocument5name(mDocArrayList.get(4).getDocumentName());
//
//                }
//
//                if (mDocArrayList.get(5).getDocumentPath()!=null) {
//                    mAllVessel.setDocument6(mDocArrayList.get(5).getDocumentPath());
//                    mAllVessel.setDocument6name(mDocArrayList.get(5).getDocumentName());
//
//                }
//                if (mDocArrayList.get(6).getDocumentPath()!=null) {
//                    mAllVessel.setDocument7(mDocArrayList.get(6).getDocumentPath());
//                    mAllVessel.setDocument7name(mDocArrayList.get(6).getDocumentName());
//                }
//                }
//                if (mDocArrayList.size()==8){
//
//                       if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                }
//                if (mDocArrayList.get(2).getDocumentPath()!=null){
//                    mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                    mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                }
//
//                if (mDocArrayList.get(3).getDocumentPath()!=null) {
//                    mAllVessel.setDocument4(mDocArrayList.get(3).getDocumentPath());
//                    mAllVessel.setDocument4name(mDocArrayList.get(3).getDocumentName());
//
//                }
//                if (mDocArrayList.get(4).getDocumentPath()!=null) {
//                    mAllVessel.setDocument5(mDocArrayList.get(4).getDocumentPath());
//                    mAllVessel.setDocument5name(mDocArrayList.get(4).getDocumentName());
//
//                }
//
//                if (mDocArrayList.get(5).getDocumentPath()!=null) {
//                    mAllVessel.setDocument6(mDocArrayList.get(5).getDocumentPath());
//                    mAllVessel.setDocument6name(mDocArrayList.get(5).getDocumentName());
//
//                }
//                if (mDocArrayList.get(6).getDocumentPath()!=null) {
//                    mAllVessel.setDocument7(mDocArrayList.get(6).getDocumentPath());
//                    mAllVessel.setDocument7name(mDocArrayList.get(6).getDocumentName());
//                }
//                if (mDocArrayList.get(7).getDocumentPath()!=null) {
//                    mAllVessel.setDocument8(mDocArrayList.get(7).getDocumentPath());
//                    mAllVessel.setDocument8name(mDocArrayList.get(7).getDocumentName());
//                }
//                }
//                if (mDocArrayList.size()==9){
//                        if (mDocArrayList.get(0).getDocumentPath()!=null){
//                mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//            }
//                if (mDocArrayList.get(1).getDocumentPath()!=null){
//                mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                    mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                }
//                if (mDocArrayList.get(2).getDocumentPath()!=null){
//                    mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                    mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                }
//
//                if (mDocArrayList.get(3).getDocumentPath()!=null) {
//                    mAllVessel.setDocument4(mDocArrayList.get(3).getDocumentPath());
//                    mAllVessel.setDocument4name(mDocArrayList.get(3).getDocumentName());
//
//                }
//                if (mDocArrayList.get(4).getDocumentPath()!=null) {
//                    mAllVessel.setDocument5(mDocArrayList.get(4).getDocumentPath());
//                    mAllVessel.setDocument5name(mDocArrayList.get(4).getDocumentName());
//
//                }
//
//                if (mDocArrayList.get(5).getDocumentPath()!=null) {
//                    mAllVessel.setDocument6(mDocArrayList.get(5).getDocumentPath());
//                    mAllVessel.setDocument6name(mDocArrayList.get(5).getDocumentName());
//
//                }
//                if (mDocArrayList.get(6).getDocumentPath()!=null) {
//                    mAllVessel.setDocument7(mDocArrayList.get(6).getDocumentPath());
//                    mAllVessel.setDocument7name(mDocArrayList.get(6).getDocumentName());
//                }
//                if (mDocArrayList.get(7).getDocumentPath()!=null) {
//                    mAllVessel.setDocument8(mDocArrayList.get(7).getDocumentPath());
//                    mAllVessel.setDocument8name(mDocArrayList.get(7).getDocumentName());
//                }
//                if (mDocArrayList.get(8).getDocumentPath()!=null) {
//                    mAllVessel.setDocument9(mDocArrayList.get(8).getDocumentPath());
//                    mAllVessel.setDocument9name(mDocArrayList.get(8).getDocumentName());
//                }
//                }
//                if (mDocArrayList.size()==10){
//                    if (mDocArrayList.get(0).getDocumentPath()!=null){
//                        mAllVessel.setDocument1(mDocArrayList.get(0).getDocumentPath());
//                        mAllVessel.setDocument1name(mDocArrayList.get(0).getDocumentName());
//                    }
//                    if (mDocArrayList.get(1).getDocumentPath()!=null){
//                        mAllVessel.setDocument2(mDocArrayList.get(1).getDocumentPath());
//                        mAllVessel.setDocument2name(mDocArrayList.get(1).getDocumentName());
//
//                    }
//                    if (mDocArrayList.get(2).getDocumentPath()!=null){
//                        mAllVessel.setDocument3(mDocArrayList.get(2).getDocumentPath());
//                        mAllVessel.setDocument3name(mDocArrayList.get(2).getDocumentName());
//                    }
//
//                    if (mDocArrayList.get(3).getDocumentPath()!=null) {
//                        mAllVessel.setDocument4(mDocArrayList.get(3).getDocumentPath());
//                        mAllVessel.setDocument4name(mDocArrayList.get(3).getDocumentName());
//
//                    }
//                    if (mDocArrayList.get(4).getDocumentPath()!=null) {
//                        mAllVessel.setDocument5(mDocArrayList.get(4).getDocumentPath());
//                        mAllVessel.setDocument5name(mDocArrayList.get(4).getDocumentName());
//
//                    }
//
//                    if (mDocArrayList.get(5).getDocumentPath()!=null) {
//                        mAllVessel.setDocument6(mDocArrayList.get(5).getDocumentPath());
//                        mAllVessel.setDocument6name(mDocArrayList.get(5).getDocumentName());
//
//                    }
//                    if (mDocArrayList.get(6).getDocumentPath()!=null) {
//                        mAllVessel.setDocument7(mDocArrayList.get(6).getDocumentPath());
//                        mAllVessel.setDocument7name(mDocArrayList.get(6).getDocumentName());
//                    }
//                    if (mDocArrayList.get(7).getDocumentPath()!=null) {
//                        mAllVessel.setDocument8(mDocArrayList.get(7).getDocumentPath());
//                        mAllVessel.setDocument8name(mDocArrayList.get(7).getDocumentName());
//                    }
//                    if (mDocArrayList.get(8).getDocumentPath()!=null) {
//                        mAllVessel.setDocument9(mDocArrayList.get(8).getDocumentPath());
//                        mAllVessel.setDocument9name(mDocArrayList.get(8).getDocumentName());
//                    }
//                if (mDocArrayList.get(9).getDocumentPath()!=null) {
//                    mAllVessel.setDocument10(mDocArrayList.get(9).getDocumentPath());
//                    mAllVessel.setDocument10name(mDocArrayList.get(9).getDocumentName());
//                }
//                }
//                  }
//
//            for (int i = 0; i < mArrayList.size(); i++) {
//                if (mArrayList.get(i).getRecordId().equals(mAllVessel.getRecordId())) {
//                    mArrayList.set(i, mAllVessel);
//                    isEditVessel = true;
//                }
//            }
//            if (isEditVessel == false) {
//                mArrayList.add(mAllVessel);
//            }
//
//            Collections.sort(mArrayList, byDate);
//            Log.e("SortedList: ", mArrayList.toString());
//            if (mVesselsAdapter != null)
//                mVesselsAdapter.notifyDataSetChanged();
//        }
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //execute API
            if (mArrayList != null)
                mArrayList.clear();
            if (loadMoreArrayList != null)
                loadMoreArrayList.clear();

            page_no = 1;
            executeGettingAllNews();
        }

    }

    static final Comparator<AllVessel> byDate = new Comparator<AllVessel>() {
        SimpleDateFormat sdf = new SimpleDateFormat("y-m-d");

        public int compare(AllVessel ord1, AllVessel ord2) {
            Date d1 = null;
            Date d2 = null;
            try {
                d1 = sdf.parse(ord1.getDateForVessel());
                d2 = sdf.parse(ord2.getDateForVessel());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return (d1.getTime() > d2.getTime() ? -1 : 1);     //descending
//            return (d1.getTime() > d2.getTime() ? 1 : -1);     //ascending
        }
    };

    public void executeAPI(int page_no) {
//        isScrolling = true;
        if (page_no == 1) {
            if (isSwipeRefresh) {

            } else {
                if (getActivity() != null) {
                    if (progressDialog != null) {
                        if (progressDialog.isShowing()) {

                        }
                    } else {
                        AlertDialogManager.showProgressDialog(getActivity());
                    }
                }
            }
        }
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        if (getActivity() != null) {
            stafff = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
        }

        Call<VesselForSaleModel> call1 = mApiInterface.getAllVesselsRequest(String.valueOf(page_no), stafff);
        call1.enqueue(new Callback<VesselForSaleModel>() {
                          @Override
                          public void onResponse(Call<VesselForSaleModel> call, retrofit2.Response<VesselForSaleModel> response) {
                              Log.e(TAG, "******Response*****" + response);
                              isScrolling = false;
                              resetRL1.setVisibility(View.GONE);
                              Log.e(TAG, "******Response*****" + response);
                              AlertDialogManager.hideProgressDialog();
                              progressBottomPB.setVisibility(View.GONE);
                              if (isSwipeRefresh) {
                                  swipeToRefresh.setRefreshing(false);
                              }
                              VesselForSaleModel mModel = response.body();
                              if (mModel.getStatus().equals("1")) {
                                  strLastPage = mModel.getData().getLastPage();
                                  if (loadMoreArrayList.size() > 0) {
                                      mArrayList.addAll(loadMoreArrayList);
                                  }
                                  if (page_no == 1) {
                                      mArrayList = mModel.getData().getAllVessels();
                                  } else if (page_no > 1) {
                                      loadMoreArrayList = mModel.getData().getAllVessels();
                                  }
                                  if (loadMoreArrayList.size() > 0) {
                                      mArrayList.addAll(loadMoreArrayList);
                                  }
                                  /*SetAdapter*/
                                  if (page_no == 1) {
                                      setAdapter();
                                  } else {
                                      mVesselsAdapter.notifyDataSetChanged();
                                  }
                              } else if (mModel.getStatus().equals("100")) {
                                  if (activity != null) {
                                      AlertDialogManager.showAccountDiableDialog(activity, getString(R.string.app_name), "" + mModel.getMessage());
                                  }
                              } else {
                                  if (activity != null) {
                                      AlertDialogManager.showAccountDiableDialog(activity, requireActivity().getString(R.string.app_name), "" + mModel.getMessage());
                                  }
                              }
                          }

                          @Override
                          public void onFailure(Call<VesselForSaleModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void setAdapter() {
        vesselsRV.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        vesselsRV.setLayoutManager(linearLayoutManager);
        mVesselsAdapter = new VesselsAdapter(getActivity(), mArrayList, mDeleteVesselsInterface, mSendEmailInterface, mSendMultiVesselDataInterface, strTextNEWS,
                strNormalTEXT, strPhotoURL, mPagination, mOpenPoUpInterface,
                mVesselToMail, new OnClickInterface() {
            @Override
            public void mOnClickInterface(int position) {
                PerformOptionsClick(position);
            }
        }, mFavoriteVesselInterface, mMailSelectedVesselsInterface);
        vesselsRV.setAdapter(mVesselsAdapter);

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView.getScrollY()));

                if (diff == 0) {

                    // your pagination code
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                progressBottomPB.setVisibility(View.VISIBLE);
                                ++page_no;
                                executeAPI(page_no);
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 1000);
                }
            }
        });
    }

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_vessels_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout shareRL = view.findViewById(R.id.shareRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailToListRL = view.findViewById(R.id.mailToListRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        shareRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                shareViaWhatsApp();
            }
        });

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), EditVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                getActivity().startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), CopyVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                getActivity().startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.Is_click_form_All_Vessals = true;
                Intent mIntent = new Intent(getActivity(), DetailsActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("isEditShow", true);
                getActivity().startActivity(mIntent);
            }
        });

        mailToListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IsAllVesselsClick = true;
                Intent mIntent = new Intent(getActivity(), SendingMailToLIst_Vessels.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), SendingMailActivity.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void shareViaWhatsApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra("Intent.EXTRA_SUBJECT", "Jaohar");
            String shareMessage = "\nSee this Video\n";
            shareMessage = "https://play.google.com/store/apps/details?id=jaohar.com.jaohar&hl=en_IN&gl=US";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void executeDeleteVesselseAPI(String strVesselID) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteVesselsRequest(strVesselID, JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Toast.makeText(getActivity(), "" + mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    page_no = 1;
                    executeAPI(page_no);
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    public void deleteConfirmDialog(final String mVesselID) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteVesselseAPI(mVesselID);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeNormalSearch() {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Log.e(TAG, "executeNormalSearch: " + JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        Call<SearchModel> call1 = mApiInterface.searchVesselRequest(strSearchText, "staff", JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                isNormalSearch = true;
                SearchModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mArrayList.clear();
                    mArrayList = mModel.getAllSearchedVessels();
                    /*SetAdapter*/
                    setAdapter();

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    public void advancedSearchView() {
        final Dialog searchDialog = new Dialog(getActivity());
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advanced_search);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = (Button) searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = (ImageView) searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editFromCapacityET = (EditText) searchDialog.findViewById(R.id.editFromCapacityET);
        final EditText editToCapacityET = (EditText) searchDialog.findViewById(R.id.editToCapacityET);
        final EditText editFromLoaET = (EditText) searchDialog.findViewById(R.id.editFromLoaET);
        final EditText editToLoaET = (EditText) searchDialog.findViewById(R.id.editToLoaET);
//        final Spinner typeSpinner = (Spinner) searchDialog.findViewById(R.id.typeSpinner);
        final EditText editFromGrainET = (EditText) searchDialog.findViewById(R.id.editFromGrainET);
        final EditText editToGrainET = (EditText) searchDialog.findViewById(R.id.editToGrainET);
        final EditText editFromYearBuiltET = (EditText) searchDialog.findViewById(R.id.editFromYearBuiltET);
        final EditText editToYearBuiltET = (EditText) searchDialog.findViewById(R.id.editToYearBuiltET);
        final EditText editFromBaleET = (EditText) searchDialog.findViewById(R.id.editFromBaleET);
        final EditText editToBaleET = (EditText) searchDialog.findViewById(R.id.editToBaleET);
        final EditText editNameET = (EditText) searchDialog.findViewById(R.id.editNameET);
        final EditText editPlaceBuiltET = (EditText) searchDialog.findViewById(R.id.editPlaceBuiltET);
        final EditText editRecordIDET = (EditText) searchDialog.findViewById(R.id.editRecordIDET);
        final EditText editStatusET = (EditText) searchDialog.findViewById(R.id.editStatusET);
        final EditText editFromTEUET = (EditText) searchDialog.findViewById(R.id.editFromTEUET);
        final EditText editToTEUET = (EditText) searchDialog.findViewById(R.id.editToTEUET);
        final EditText editFromPassengerET = (EditText) searchDialog.findViewById(R.id.editFromPassengerET);
        final EditText editToPassendgerET = (EditText) searchDialog.findViewById(R.id.editToPassendgerET);
        final EditText editFromDraftrET = (EditText) searchDialog.findViewById(R.id.editFromDraftrET);
        final EditText editToDraftET = (EditText) searchDialog.findViewById(R.id.editToDraftET);
        final EditText editFromLiquidET = (EditText) searchDialog.findViewById(R.id.editFromLiquidET);
        final EditText editToLiquidET = (EditText) searchDialog.findViewById(R.id.editToLiquidET);
        final EditText editTypeET = (EditText) searchDialog.findViewById(R.id.editTypeET);
        final EditText editClassET = (EditText) searchDialog.findViewById(R.id.editClassET);
        final EditText editIMONumET = (EditText) searchDialog.findViewById(R.id.editIMONumET);
        final TextView recordTV = (TextView) searchDialog.findViewById(R.id.recordTV);

        final EditText editOffMarketET = searchDialog.findViewById(R.id.editOffMarketET);
        final EditText editGearedET = searchDialog.findViewById(R.id.editGearedET);

//        editStatusET.setText("Available");
        recordTV.setText("IMO No.");
        editRecordIDET.setHint("IMO No.");
        editStatusET.setKeyListener(null);
        editStatusET.setCursorVisible(false);
        final TextView txtHeaderStatus = (TextView) searchDialog.findViewById(R.id.txtHeaderStatus);
        final LinearLayout layoutNameLL = (LinearLayout) searchDialog.findViewById(R.id.layoutNameLL);
        final LinearLayout layoutPlaceBuiltLL = (LinearLayout) searchDialog.findViewById(R.id.layoutPlaceBuiltLL);
        final LinearLayout layoutRecordIDLL = (LinearLayout) searchDialog.findViewById(R.id.layoutRecordIDLL);
        final LinearLayout layoutStatusLL = (LinearLayout) searchDialog.findViewById(R.id.layoutStatusLL);
        final LinearLayout layoutIMONumLL = (LinearLayout) searchDialog.findViewById(R.id.layoutIMONumLL);
        layoutNameLL.setVisibility(View.VISIBLE);
        layoutPlaceBuiltLL.setVisibility(View.VISIBLE);
        layoutIMONumLL.setVisibility(View.VISIBLE);
        layoutRecordIDLL.setVisibility(View.GONE);

        final Spinner spinnerGeared = (Spinner) searchDialog.findViewById(R.id.spinnerGeared);

        List<String> list = new ArrayList<String>();
        list.add("Select Geared");
        list.add("Yes");
        list.add("No");
        ArrayAdapter<String> mSpAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        mSpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGeared.setAdapter(mSpAdapter);
        spinnerGeared.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strGeared = adapterView.getItemAtPosition(i).toString();
                if (strGeared.equals("Select Geared")) {
                    strGeared = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editTypeET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Type");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, modellArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strType = modellArrayList.get(position);
                            editTypeET.setText(strType);
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editClassET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(getActivity());
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
//                  categoryDialog.setCanceledOnTouchOutside(true);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Class");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    // Define a new Adapter
                    // First parameter - Context
                    // Second parameter - Layout for the row
                    // Third parameter - ID of the TextView to which the data is written
                    // Forth - the Array of data

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListtClass);

                    // Assign adapter to ListView
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strClass = mArrayListtClass.get(position);
                            editClassET.setText(strClass);
                            categoryDialog.dismiss();

                        }
                    });


                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });


        layoutStatusLL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(getActivity(), "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        txtHeaderStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(getActivity(), "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(getActivity(), "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });


        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchDialog.dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strCapacityFrom = editFromCapacityET.getText().toString();
                strCapacityTo = editToCapacityET.getText().toString();

                strName = editNameET.getText().toString();

                strLoaFrom = editFromLoaET.getText().toString();
                strLoaTo = editToLoaET.getText().toString();

                strGrainFrom = editFromGrainET.getText().toString();
                strGrainTo = editToGrainET.getText().toString();

                strYearofBuildFrom = editFromYearBuiltET.getText().toString();
                strYearofBuildTo = editToYearBuiltET.getText().toString();

                strBaleFrom = editFromBaleET.getText().toString();
                strBaleTo = editToBaleET.getText().toString();

                strPlaceBuild = editPlaceBuiltET.getText().toString();
                strRecordID = editRecordIDET.getText().toString();
                IMO_number = editIMONumET.getText().toString();


                strStatus = editStatusET.getText().toString();
                strTEUFrom = editFromTEUET.getText().toString();
                strTEUto = editToTEUET.getText().toString();
                strDraftsTo = editToDraftET.getText().toString();
                strDraftsFrom = editFromDraftrET.getText().toString();
                strPassangerFrom = editFromPassengerET.getText().toString();
                strPassangerTo = editToPassendgerET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();
                strLiquidFrom = editFromLiquidET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();

//                strGeared = spinnerGeared.getSelectedItem().toString();

                strGeared = editGearedET.getText().toString();
                strOffMarket = editOffMarketET.getText().toString();

                if (Utilities.isNetworkAvailable(getActivity()) == false) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchAPI(searchDialog);
                }
            }
        });
        searchDialog.show();
    }

    public void executeAdvanceSearchAPI(final Dialog mDialog) {
        mArrayList.clear();
        if (strType == null) {
            strType = "";
        }
        if (strClass == null) {
            strClass = "";
        }
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.advanceSearchVesselRequest(strCapacityFrom, strCapacityTo, strLoaFrom, strLoaTo,
                strType, strYearofBuildFrom, strYearofBuildTo, strGrainFrom, strGrainTo, strBaleFrom, strName, strRecordID, IMO_number, strBaleTo, strStatus,
                strGeared, "staff", strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo,
                strLiquidFrom, strLiquidTo, strClass, strPlaceBuild, JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), strOffMarket);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                /*Paras Search Data*/
                isAdvanceSearch = true;
                SearchModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mDialog.dismiss();
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mArrayList.clear();
                    mArrayList = mModel.getAllSearchedVessels();
                    /*SetAdapter*/
                    setAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus().equals("0")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_no = 1;
                txtMailTV.setVisibility(View.GONE);
                mArrayList.clear();
                loadMoreArrayList.clear();
                executeAPI(page_no);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setUserNewsAdapter() {
        newsRV.setNestedScrollingEnabled(false);
        newsRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mNewsAdapter = new UserNewsAdapter(getActivity(), mNewsArrayList, mOpenNewsPopUP);
        newsRV.setAdapter(mNewsAdapter);
    }

    private void executeGettingAllNews() {
        mNewsArrayList.clear();
//        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AllNewsModel> call1 = mApiInterface.getallNewsRequest();
        call1.enqueue(new Callback<AllNewsModel>() {
                          @Override
                          public void onResponse(Call<AllNewsModel> call, retrofit2.Response<AllNewsModel> response) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              AllNewsModel mModel = response.body();
                              if (mModel.getStatus().equals("1")) {
                                  mNewsArrayList = mModel.getData();
                                  /*setAdapter*/
                                  setUserNewsAdapter();
                                  executeAPI(page_no);
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<AllNewsModel> call, Throwable t) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    /* *
     * Execute Favorite API for vessel
     * @param
     * @user_id
     * */
    public void executeFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.makeFavoriteVesselsRequest(vessel_id, JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_yellow);
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.the_vessel_has_been_fav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /* *
     * Execute UnFavorite API for vessel
     * @param
     * @user_id
     * */
    public void executeUnFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.unFavoriteVesselsRequest(vessel_id, JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_border);
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}