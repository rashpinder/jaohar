package jaohar.com.jaohar.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tokenautocomplete.TokenCompleteTextView;

import jaohar.com.jaohar.R;

public class ContactsCompletionView extends TokenCompleteTextView<String> {
    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject(String object) {
        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        TextView view = (TextView) l.inflate(R.layout.layout_right_chat, (ViewGroup) getParent(), false);
        view.setText(object);

        return view;
    }

    @Override
    protected String defaultObject(String completionText) {
        //Oversimplified example of guessing if we have an email or not
        return completionText;
    }


}