package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by dharmaniz on 22/3/19.
 */

public class TemplatesInvoicModel implements Serializable {
    private  String template_id ="";
    private  String template_image ="";
    private  String heading_color ="";
    private  String content_color ="";
    private  String table_head_color ="";
    private  String table_row_color ="";
    private  String border_gradient_color ="";

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getTemplate_image() {
        return template_image;
    }

    public void setTemplate_image(String template_image) {
        this.template_image = template_image;
    }

    public String getHeading_color() {
        return heading_color;
    }

    public void setHeading_color(String heading_color) {
        this.heading_color = heading_color;
    }

    public String getContent_color() {
        return content_color;
    }

    public void setContent_color(String content_color) {
        this.content_color = content_color;
    }

    public String getTable_head_color() {
        return table_head_color;
    }

    public void setTable_head_color(String table_head_color) {
        this.table_head_color = table_head_color;
    }

    public String getTable_row_color() {
        return table_row_color;
    }

    public void setTable_row_color(String table_row_color) {
        this.table_row_color = table_row_color;
    }

    public String getBorder_gradient_color() {
        return border_gradient_color;
    }

    public void setBorder_gradient_color(String border_gradient_color) {
        this.border_gradient_color = border_gradient_color;
    }
}
