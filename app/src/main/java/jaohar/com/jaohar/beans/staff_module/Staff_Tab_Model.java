package jaohar.com.jaohar.beans.staff_module;

import java.io.Serializable;

public class Staff_Tab_Model implements Serializable {
    String module_id="";
    String access_type="";
    String module_name="";
    String module_link="";
    String module_image="";
    String app_image="";
    String type="";
    String access_to="";
    String disable="";
    String creation_date="";

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getAccess_type() {
        return access_type;
    }

    public void setAccess_type(String access_type) {
        this.access_type = access_type;
    }

    public String getModule_name() {
        return module_name;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }

    public String getModule_link() {
        return module_link;
    }

    public void setModule_link(String module_link) {
        this.module_link = module_link;
    }

    public String getModule_image() {
        return module_image;
    }

    public void setModule_image(String module_image) {
        this.module_image = module_image;
    }

    public String getApp_image() {
        return app_image;
    }

    public void setApp_image(String app_image) {
        this.app_image = app_image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccess_to() {
        return access_to;
    }

    public void setAccess_to(String access_to) {
        this.access_to = access_to;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }
}
