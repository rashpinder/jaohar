package jaohar.com.jaohar.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Dharmani Apps on 12/26/2017.
 */

public class InvoiceAddItemModel implements Serializable {
    String itemID = "";
    String item = "";
    int quantity = 0;
    String unitprice = "";

    public String getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    String description = "";
    String TotalAmount = "";
    String strTotalunitPrice = "";
    String strTotalAmountUnit = "";
    ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();


    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getStrTotalunitPrice() {
        return strTotalunitPrice;
    }

    public void setStrTotalunitPrice(String strTotalunitPrice) {
        this.strTotalunitPrice = strTotalunitPrice;
    }

    public String getStrTotalAmountUnit() {
        return strTotalAmountUnit;
    }

    public void setStrTotalAmountUnit(String strTotalAmountUnit) {
        this.strTotalAmountUnit = strTotalAmountUnit;
    }

    public ArrayList<DiscountModel> getmDiscountModelArrayList() {
        return mDiscountModelArrayList;
    }

    public void setmDiscountModelArrayList(ArrayList<DiscountModel> mDiscountModelArrayList) {
        this.mDiscountModelArrayList = mDiscountModelArrayList;
    }


    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
