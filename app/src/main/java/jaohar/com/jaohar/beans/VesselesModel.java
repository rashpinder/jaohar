package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 11/9/2017.
 */

public class VesselesModel implements Serializable {
    String Record_ID = "";
    String IMO_Number = "";
    String Short_Discription = "";
    String Vessel_Name = "";
    String Vessel_Type = "";
    String currency = "";
    String Year_Built = "";
    String Place_of_Built = "";
    String mClass = "";
    String Flag = "";
    String DWT_Capacity = "";
    String LOA_M = "";
    String Breadth_M = "";
    String Depth = "";
    String Price_Idea = "";
    String Vessel_Location = "";
    String Full_Discription = "";
    String machinery_detail = "";
    String owner_detail = "";
    String remarks = "";
    String Photo1 = "";
    String Photo2 = "";
    String Photo3 = "";
    String Photo4 = "";
    String Photo5 = "";
    String Photo6 = "";
    String Photo7 = "";
    String Photo8 = "";
    String Photo9 = "";
    String Photo10 = "";
    String Photo11 = "";
    String Photo12 = "";
    String Photo13 = "";
    String Photo14 = "";
    String Photo15 = "";
    String Photo16 = "";
    String Photo17 = "";
    String Photo18 = "";
    String Photo19 = "";
    String Photo20 = "";
    String Photo21 = "";
    String Photo22 = "";
    String Photo23 = "";
    String Photo24 = "";
    String Photo25 = "";
    String Photo26 = "";
    String Photo27 = "";
    String Photo28 = "";
    String Photo29 = "";
    String Photo30 = "";
    String Date_Entered = "";
    String Status = "";
    String Bale = "";
    String Grain = "";
    String Date_for_Vessel = "";
    String vessel_added_by = "";
    String vessel_add_time = "";
    String document1 = "";
    String document2 = "";
    String document3 = "";
    String document4 = "";
    String document5 = "";
    String document6 = "";
    String document7 = "";
    String document8 = "";
    String document9 = "";
    String document10 = "";
    String document11 = "";
    String document12 = "";
    String document13 = "";
    String document14 = "";
    String document15 = "";
    String document16 = "";
    String document17 = "";
    String document18 = "";
    String document19 = "";
    String document20 = "";
    String document21 = "";
    String document22 = "";
    String document23 = "";
    String document24 = "";
    String document25 = "";
    String document26 = "";
    String document27 = "";
    String document28 = "";
    String document29 = "";
    String document30 = "";
    String mail_data = "";
    String document1name = "";
    String document2name = "";
    String document3name = "";
    String document4name = "";
    String document5name = "";
    String document6name = "";
    String document7name = "";
    String document8name = "";
    String document9name = "";
    String document10name = "";
    String document11name = "";
    String document12name = "";
    String document13name = "";
    String document14name = "";
    String document15name = "";
    String document16name = "";
    String document17name = "";
    String document18name = "";
    String document19name = "";
    String document20name = "";
    String document21name = "";
    String document22name = "";
    String document23name = "";
    String document24name = "";
    String document25name = "";
    String document26name = "";
    String document27name = "";
    String document28name = "";
    String document29name = "";
    String document30name = "";
    String geared = "";
    String builder = "";
    String gross_tonnage = "";
    String net_tonnage = "";
    String gas = "";
    String draught = "";
    String url = "";
    String teu = "";
    String no_passengers = "";
    String liquid = "";
    String draft = "";
    String deleted_by = "";
    String favorite_status = "";
    String ldt = "";
    String tcm = "";

    int image;
    private boolean isSelected;

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getPhoto11() {
        return Photo11;
    }

    public void setPhoto11(String photo11) {
        Photo11 = photo11;
    }

    public String getPhoto12() {
        return Photo12;
    }

    public void setPhoto12(String photo12) {
        Photo12 = photo12;
    }

    public String getPhoto13() {
        return Photo13;
    }

    public void setPhoto13(String photo13) {
        Photo13 = photo13;
    }

    public String getPhoto14() {
        return Photo14;
    }

    public void setPhoto14(String photo14) {
        Photo14 = photo14;
    }

    public String getPhoto15() {
        return Photo15;
    }

    public void setPhoto15(String photo15) {
        Photo15 = photo15;
    }

    public String getPhoto16() {
        return Photo16;
    }

    public void setPhoto16(String photo16) {
        Photo16 = photo16;
    }

    public String getPhoto17() {
        return Photo17;
    }

    public void setPhoto17(String photo17) {
        Photo17 = photo17;
    }

    public String getPhoto18() {
        return Photo18;
    }

    public void setPhoto18(String photo18) {
        Photo18 = photo18;
    }

    public String getPhoto19() {
        return Photo19;
    }

    public void setPhoto19(String photo19) {
        Photo19 = photo19;
    }

    public String getPhoto20() {
        return Photo20;
    }

    public void setPhoto20(String photo20) {
        Photo20 = photo20;
    }

    public String getPhoto21() {
        return Photo21;
    }

    public void setPhoto21(String photo21) {
        Photo21 = photo21;
    }

    public String getPhoto22() {
        return Photo22;
    }

    public void setPhoto22(String photo22) {
        Photo22 = photo22;
    }

    public String getPhoto23() {
        return Photo23;
    }

    public void setPhoto23(String photo23) {
        Photo23 = photo23;
    }

    public String getPhoto24() {
        return Photo24;
    }

    public void setPhoto24(String photo24) {
        Photo24 = photo24;
    }

    public String getPhoto25() {
        return Photo25;
    }

    public void setPhoto25(String photo25) {
        Photo25 = photo25;
    }

    public String getPhoto26() {
        return Photo26;
    }

    public void setPhoto26(String photo26) {
        Photo26 = photo26;
    }

    public String getPhoto27() {
        return Photo27;
    }

    public void setPhoto27(String photo27) {
        Photo27 = photo27;
    }

    public String getPhoto28() {
        return Photo28;
    }

    public void setPhoto28(String photo28) {
        Photo28 = photo28;
    }

    public String getPhoto29() {
        return Photo29;
    }

    public void setPhoto29(String photo29) {
        Photo29 = photo29;
    }

    public String getPhoto30() {
        return Photo30;
    }

    public void setPhoto30(String photo30) {
        Photo30 = photo30;
    }

    public String getDocument11() {
        return document11;
    }

    public void setDocument11(String document11) {
        this.document11 = document11;
    }

    public String getDocument12() {
        return document12;
    }

    public void setDocument12(String document12) {
        this.document12 = document12;
    }

    public String getDocument13() {
        return document13;
    }

    public void setDocument13(String document13) {
        this.document13 = document13;
    }

    public String getDocument14() {
        return document14;
    }

    public void setDocument14(String document14) {
        this.document14 = document14;
    }

    public String getDocument15() {
        return document15;
    }

    public void setDocument15(String document15) {
        this.document15 = document15;
    }

    public String getDocument16() {
        return document16;
    }

    public void setDocument16(String document16) {
        this.document16 = document16;
    }

    public String getDocument17() {
        return document17;
    }

    public void setDocument17(String document17) {
        this.document17 = document17;
    }

    public String getDocument18() {
        return document18;
    }

    public void setDocument18(String document18) {
        this.document18 = document18;
    }

    public String getDocument19() {
        return document19;
    }

    public void setDocument19(String document19) {
        this.document19 = document19;
    }

    public String getDocument20() {
        return document20;
    }

    public void setDocument20(String document20) {
        this.document20 = document20;
    }

    public String getDocument21() {
        return document21;
    }

    public void setDocument21(String document21) {
        this.document21 = document21;
    }

    public String getDocument22() {
        return document22;
    }

    public void setDocument22(String document22) {
        this.document22 = document22;
    }

    public String getDocument23() {
        return document23;
    }

    public void setDocument23(String document23) {
        this.document23 = document23;
    }

    public String getDocument24() {
        return document24;
    }

    public void setDocument24(String document24) {
        this.document24 = document24;
    }

    public String getDocument25() {
        return document25;
    }

    public void setDocument25(String document25) {
        this.document25 = document25;
    }

    public String getDocument26() {
        return document26;
    }

    public void setDocument26(String document26) {
        this.document26 = document26;
    }

    public String getDocument27() {
        return document27;
    }

    public void setDocument27(String document27) {
        this.document27 = document27;
    }

    public String getDocument28() {
        return document28;
    }

    public void setDocument28(String document28) {
        this.document28 = document28;
    }

    public String getDocument29() {
        return document29;
    }

    public void setDocument29(String document29) {
        this.document29 = document29;
    }

    public String getDocument30() {
        return document30;
    }

    public void setDocument30(String document30) {
        this.document30 = document30;
    }

    public String getDocument11name() {
        return document11name;
    }

    public void setDocument11name(String document11name) {
        this.document11name = document11name;
    }

    public String getDocument12name() {
        return document12name;
    }

    public void setDocument12name(String document12name) {
        this.document12name = document12name;
    }

    public String getDocument13name() {
        return document13name;
    }

    public void setDocument13name(String document13name) {
        this.document13name = document13name;
    }

    public String getDocument14name() {
        return document14name;
    }

    public void setDocument14name(String document14name) {
        this.document14name = document14name;
    }

    public String getDocument15name() {
        return document15name;
    }

    public void setDocument15name(String document15name) {
        this.document15name = document15name;
    }

    public String getDocument16name() {
        return document16name;
    }

    public void setDocument16name(String document16name) {
        this.document16name = document16name;
    }

    public String getDocument17name() {
        return document17name;
    }

    public void setDocument17name(String document17name) {
        this.document17name = document17name;
    }

    public String getDocument18name() {
        return document18name;
    }

    public void setDocument18name(String document18name) {
        this.document18name = document18name;
    }

    public String getDocument19name() {
        return document19name;
    }

    public void setDocument19name(String document19name) {
        this.document19name = document19name;
    }

    public String getDocument20name() {
        return document20name;
    }

    public void setDocument20name(String document20name) {
        this.document20name = document20name;
    }

    public String getDocument21name() {
        return document21name;
    }

    public void setDocument21name(String document21name) {
        this.document21name = document21name;
    }

    public String getDocument22name() {
        return document22name;
    }

    public void setDocument22name(String document22name) {
        this.document22name = document22name;
    }

    public String getDocument23name() {
        return document23name;
    }

    public void setDocument23name(String document23name) {
        this.document23name = document23name;
    }

    public String getDocument24name() {
        return document24name;
    }

    public void setDocument24name(String document24name) {
        this.document24name = document24name;
    }

    public String getDocument25name() {
        return document25name;
    }

    public void setDocument25name(String document25name) {
        this.document25name = document25name;
    }

    public String getDocument26name() {
        return document26name;
    }

    public void setDocument26name(String document26name) {
        this.document26name = document26name;
    }

    public String getDocument27name() {
        return document27name;
    }

    public void setDocument27name(String document27name) {
        this.document27name = document27name;
    }

    public String getDocument28name() {
        return document28name;
    }

    public void setDocument28name(String document28name) {
        this.document28name = document28name;
    }

    public String getDocument29name() {
        return document29name;
    }

    public void setDocument29name(String document29name) {
        this.document29name = document29name;
    }

    public String getDocument30name() {
        return document30name;
    }

    public void setDocument30name(String document30name) {
        this.document30name = document30name;
    }

    public String getTeu() {
        return teu;
    }

    public void setTeu(String teu) {
        this.teu = teu;
    }

    public String getNo_passengers() {
        return no_passengers;
    }

    public void setNo_passengers(String no_passengers) {
        this.no_passengers = no_passengers;
    }

    public String getLiquid() {
        return liquid;
    }

    public void setLiquid(String liquid) {
        this.liquid = liquid;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }

    public String getBuilder() {
        return builder;
    }

    public void setBuilder(String builder) {
        this.builder = builder;
    }

    public String getGross_tonnage() {
        return gross_tonnage;
    }

    public void setGross_tonnage(String gross_tonnage) {
        this.gross_tonnage = gross_tonnage;
    }

    public String getNet_tonnage() {
        return net_tonnage;
    }

    public void setNet_tonnage(String net_tonnage) {
        this.net_tonnage = net_tonnage;
    }

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }

    public String getDraught() {
        return draught;
    }

    public void setDraught(String draught) {
        this.draught = draught;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDocument1name() {
        return document1name;
    }

    public void setDocument1name(String document1name) {
        this.document1name = document1name;
    }

    public String getVessel_add_time() {
        return vessel_add_time;
    }

    public void setVessel_add_time(String vessel_add_time) {

        this.vessel_add_time = vessel_add_time;
    }

    public String getGeared() {
        return geared;
    }

    public void setGeared(String geared) {
        this.geared = geared;
    }

    public String getPhoto6() {
        return Photo6;
    }

    public void setPhoto6(String photo6) {
        Photo6 = photo6;
    }

    public String getDocument6() {
        return document6;
    }

    public void setDocument6(String document6) {
        this.document6 = document6;
    }

    public String getDocument7() {
        return document7;
    }

    public void setDocument7(String document7) {
        this.document7 = document7;
    }

    public String getDocument8() {
        return document8;
    }

    public void setDocument8(String document8) {
        this.document8 = document8;
    }

    public String getDocument9() {
        return document9;
    }

    public void setDocument9(String document9) {
        this.document9 = document9;
    }

    public String getDocument10() {
        return document10;
    }

    public void setDocument10(String document10) {
        this.document10 = document10;
    }

    public String getDocument6name() {
        return document6name;
    }

    public void setDocument6name(String document6name) {
        this.document6name = document6name;
    }

    public String getDocument7name() {
        return document7name;
    }

    public void setDocument7name(String document7name) {
        this.document7name = document7name;
    }

    public String getDocument8name() {
        return document8name;
    }

    public void setDocument8name(String document8name) {
        this.document8name = document8name;
    }

    public String getDocument9name() {
        return document9name;
    }

    public void setDocument9name(String document9name) {
        this.document9name = document9name;
    }

    public String getDocument10name() {
        return document10name;
    }

    public void setDocument10name(String document10name) {
        this.document10name = document10name;
    }

    public String getPhoto7() {
        return Photo7;
    }

    public void setPhoto7(String photo7) {
        Photo7 = photo7;
    }

    public String getPhoto8() {
        return Photo8;
    }

    public void setPhoto8(String photo8) {
        Photo8 = photo8;
    }

    public String getPhoto9() {
        return Photo9;
    }

    public void setPhoto9(String photo9) {
        Photo9 = photo9;
    }

    public String getPhoto10() {
        return Photo10;
    }

    public void setPhoto10(String photo10) {
        Photo10 = photo10;
    }

    public String getDocument2name() {
        return document2name;
    }

    public void setDocument2name(String document2name) {
        this.document2name = document2name;
    }

    public String getDocument3name() {
        return document3name;
    }

    public void setDocument3name(String document3name) {
        this.document3name = document3name;
    }

    public String getDocument4name() {
        return document4name;
    }

    public void setDocument4name(String document4name) {
        this.document4name = document4name;
    }

    public String getDocument5name() {
        return document5name;
    }

    public void setDocument5name(String document5name) {
        this.document5name = document5name;
    }

    public String getMail_data() {
        return mail_data;
    }

    public void setMail_data(String mail_data) {
        this.mail_data = mail_data;
    }

    public String getmClass() {
        return mClass;
    }

    public void setmClass(String mClass) {
        this.mClass = mClass;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getVessel_added_by() {
        return vessel_added_by;
    }

    public void setVessel_added_by(String vessel_added_by) {
        this.vessel_added_by = vessel_added_by;
    }

    public String getDocument1() {
        return document1;
    }

    public void setDocument1(String document1) {
        this.document1 = document1;
    }

    public String getDocument2() {
        return document2;
    }

    public void setDocument2(String document2) {
        this.document2 = document2;
    }

    public String getDocument3() {
        return document3;
    }

    public void setDocument3(String document3) {
        this.document3 = document3;
    }

    public String getDocument4() {
        return document4;
    }

    public void setDocument4(String document4) {
        this.document4 = document4;
    }

    public String getDocument5() {
        return document5;
    }

    public void setDocument5(String document5) {
        this.document5 = document5;
    }

    public String getDate_for_Vessel() {
        return Date_for_Vessel;
    }

    public void setDate_for_Vessel(String date_for_Vessel) {
        Date_for_Vessel = date_for_Vessel;
    }

    public String getBale() {
        return Bale;
    }

    public void setBale(String bale) {
        Bale = bale;
    }

    public String getGrain() {
        return Grain;
    }

    public void setGrain(String grain) {
        Grain = grain;
    }

    public String getRecord_ID() {
        return Record_ID;
    }

    public void setRecord_ID(String record_ID) {
        Record_ID = record_ID;
    }

    public String getIMO_Number() {
        return IMO_Number;
    }

    public void setIMO_Number(String IMO_Number) {
        this.IMO_Number = IMO_Number;
    }

    public String getShort_Discription() {
        return Short_Discription;
    }

    public void setShort_Discription(String short_Discription) {
        Short_Discription = short_Discription;
    }

    public String getVessel_Name() {
        return Vessel_Name;
    }

    public void setVessel_Name(String vessel_Name) {
        Vessel_Name = vessel_Name;
    }

    public String getVessel_Type() {
        return Vessel_Type;
    }

    public void setVessel_Type(String vessel_Type) {
        Vessel_Type = vessel_Type;
    }

    public String getYear_Built() {
        return Year_Built;
    }

    public void setYear_Built(String year_Built) {
        Year_Built = year_Built;
    }

    public String getPlace_of_Built() {
        return Place_of_Built;
    }

    public void setPlace_of_Built(String place_of_Built) {
        Place_of_Built = place_of_Built;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getDWT_Capacity() {
        return DWT_Capacity;
    }

    public void setDWT_Capacity(String DWT_Capacity) {
        this.DWT_Capacity = DWT_Capacity;
    }

    public String getLOA_M() {
        return LOA_M;
    }

    public void setLOA_M(String LOA_M) {
        this.LOA_M = LOA_M;
    }

    public String getBreadth_M() {
        return Breadth_M;
    }

    public void setBreadth_M(String breadth_M) {
        Breadth_M = breadth_M;
    }

    public String getDepth() {
        return Depth;
    }

    public void setDepth(String depth) {
        Depth = depth;
    }

    public String getPrice_Idea() {
        return Price_Idea;
    }

    public void setPrice_Idea(String price_Idea) {
        Price_Idea = price_Idea;
    }

    public String getVessel_Location() {
        return Vessel_Location;
    }

    public void setVessel_Location(String vessel_Location) {
        Vessel_Location = vessel_Location;
    }

    public String getFull_Discription() {
        return Full_Discription;
    }

    public void setFull_Discription(String full_Discription) {
        Full_Discription = full_Discription;
    }

    public String getPhoto1() {
        return Photo1;
    }

    public void setPhoto1(String photo1) {
        Photo1 = photo1;
    }

    public String getPhoto2() {
        return Photo2;
    }

    public void setPhoto2(String photo2) {
        Photo2 = photo2;
    }

    public String getPhoto3() {
        return Photo3;
    }

    public void setPhoto3(String photo3) {
        Photo3 = photo3;
    }

    public String getPhoto4() {
        return Photo4;
    }

    public void setPhoto4(String photo4) {
        Photo4 = photo4;
    }

    public String getPhoto5() {
        return Photo5;
    }

    public void setPhoto5(String photo5) {
        Photo5 = photo5;
    }

    public String getDate_Entered() {
        return Date_Entered;
    }

    public void setDate_Entered(String date_Entered) {
        Date_Entered = date_Entered;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getMachinery_detail() {
        return machinery_detail;
    }

    public void setMachinery_detail(String machinery_detail) {
        this.machinery_detail = machinery_detail;
    }

    public String getOwner_detail() {
        return owner_detail;
    }

    public void setOwner_detail(String owner_detail) {
        this.owner_detail = owner_detail;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getFavorite_status() {
        return favorite_status;
    }

    public void setFavorite_status(String favorite_status) {
        this.favorite_status = favorite_status;
    }

    public String getLdt() {
        return ldt;
    }

    public void setLdt(String ldt) {
        this.ldt = ldt;
    }

    public String getTcm() {
        return tcm;
    }

    public void setTcm(String tcm) {
        this.tcm = tcm;
    }
}
