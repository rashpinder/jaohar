package jaohar.com.jaohar.beans.blog_module;

import java.io.Serializable;

public class Blog_Replies_Model implements Serializable {
    String comment_id="";
    String blog_id="";
    String reply_id="";
    String user_id="";
    String message="";
    String posted_by="";
    String posted_on="";
    Blog_User_Detail_Model mUser_Detail_Model;

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(String blog_id) {
        this.blog_id = blog_id;
    }

    public String getReply_id() {
        return reply_id;
    }

    public void setReply_id(String reply_id) {
        this.reply_id = reply_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPosted_by() {
        return posted_by;
    }

    public void setPosted_by(String posted_by) {
        this.posted_by = posted_by;
    }

    public String getPosted_on() {
        return posted_on;
    }

    public void setPosted_on(String posted_on) {
        this.posted_on = posted_on;
    }

    public Blog_User_Detail_Model getmUser_Detail_Model() {
        return mUser_Detail_Model;
    }

    public void setmUser_Detail_Model(Blog_User_Detail_Model mUser_Detail_Model) {
        this.mUser_Detail_Model = mUser_Detail_Model;
    }
}
