package jaohar.com.jaohar.beans.ForumModule;

import java.io.Serializable;

public class ForumModel implements Serializable {
    String id = "";
    String vessel_id = "";
    String user_id = "";
    String heading = "";
    String creation_date = "";
    String disable = "";
    String vessel_record_id = "";
    String vessel_name = "";
    String IMO_number = "";
    String status = "";
    String pic = "";
    String total_messages = "";
    String unread_messages = "";
    String room_id = "";

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVessel_id() {
        return vessel_id;
    }

    public void setVessel_id(String vessel_id) {
        this.vessel_id = vessel_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getVessel_record_id() {
        return vessel_record_id;
    }

    public void setVessel_record_id(String vessel_record_id) {
        this.vessel_record_id = vessel_record_id;
    }

    public String getVessel_name() {
        return vessel_name;
    }

    public void setVessel_name(String vessel_name) {
        this.vessel_name = vessel_name;
    }

    public String getIMO_number() {
        return IMO_number;
    }

    public void setIMO_number(String IMO_number) {
        this.IMO_number = IMO_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getTotal_messages() {
        return total_messages;
    }

    public void setTotal_messages(String total_messages) {
        this.total_messages = total_messages;
    }

    public String getUnread_messages() {
        return unread_messages;
    }

    public void setUnread_messages(String unread_messages) {
        this.unread_messages = unread_messages;
    }
}
