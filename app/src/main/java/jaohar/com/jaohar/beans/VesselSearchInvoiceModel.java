package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 1/30/2018.
 */

public class VesselSearchInvoiceModel implements Serializable {
    String vessel_id = "";
    String vessel_name = "";
    String IMO_no = "";
    String flag = "";
    String added_by = "";

    public String getDeleted_by() {
        return added_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.added_by = deleted_by;
    }

    public String getVessel_id() {
        return vessel_id;
    }

    public void setVessel_id(String vessel_id) {
        this.vessel_id = vessel_id;
    }

    public String getVessel_name() {
        return vessel_name;
    }

    public void setVessel_name(String vessel_name) {
        this.vessel_name = vessel_name;
    }

    public String getIMO_no() {
        return IMO_no;
    }

    public void setIMO_no(String IMO_no) {
        this.IMO_no = IMO_no;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
