package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class StampsModel implements Serializable {
    String id = "";
    String stamp_name = "";
    String stamp_image = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStamp_name() {
        return stamp_name;
    }

    public void setStamp_name(String stamp_name) {
        this.stamp_name = stamp_name;
    }

    public String getStamp_image() {
        return stamp_image;
    }

    public void setStamp_image(String stamp_image) {
        this.stamp_image = stamp_image;
    }
}
