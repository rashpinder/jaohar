package jaohar.com.jaohar.beans.chat_module;

import java.io.Serializable;
import java.util.ArrayList;

import jaohar.com.jaohar.beans.ForumModule.ReplyForModelForum;
import jaohar.com.jaohar.beans.ForumModule.UserDetailForumChatModel;

public class ChatMessagesModel implements Serializable {

    public static final int Sender = 0;
    public static final int Receiver = 1;
    int intType;



    String message_id="";
    String reply_id="";
    String forum_id="";
    String room_id="";
    String user_id="";
    String message="";
    String image="";
    String creation_date="";
    String image2="";
    String image3="";
    String image4="";
    String image5="";
    String image6="";
    String image7="";
    String image8="";
    String image9="";
    String image10="";
    String edited="";
    String image_count="";

    String sender_id = "";
    String receiver_id = "";

    public String getImage_count() {
        return image_count;
    }

    public void setImage_count(String image_count) {
        this.image_count = image_count;
    }

    boolean editable=false;

    ArrayList<String> read_by = new ArrayList<String>() ;
    ReplyForModelForum replyForModel ;

    UserDetailForumChatModel userDetailForumModel ;

    public UserDetailForumChatModel getUserDetailForumModel() {
        return userDetailForumModel;
    }

    public void setUserDetailForumModel(UserDetailForumChatModel userDetailForumModel) {
        this.userDetailForumModel = userDetailForumModel;
    }

    public boolean isEditable() {
        return editable;
    }


    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    public int getIntType() {
        return intType;
    }

    public void setIntType(int intType) {
        this.intType = intType;
    }


    public ReplyForModelForum getReplyForModel() {
        return replyForModel;
    }

    public void setReplyForModel(ReplyForModelForum replyForModel) {
        this.replyForModel = replyForModel;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getReply_id() {
        return reply_id;
    }

    public void setReply_id(String reply_id) {
        this.reply_id = reply_id;
    }

    public String getForum_id() {
        return forum_id;
    }

    public void setForum_id(String forum_id) {
        this.forum_id = forum_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getImage6() {
        return image6;
    }

    public void setImage6(String image6) {
        this.image6 = image6;
    }

    public String getImage7() {
        return image7;
    }

    public void setImage7(String image7) {
        this.image7 = image7;
    }

    public String getImage8() {
        return image8;
    }

    public void setImage8(String image8) {
        this.image8 = image8;
    }

    public String getImage9() {
        return image9;
    }

    public void setImage9(String image9) {
        this.image9 = image9;
    }

    public String getImage10() {
        return image10;
    }

    public void setImage10(String image10) {
        this.image10 = image10;
    }

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

    public ArrayList<String> getRead_by() {
        return read_by;
    }

    public void setRead_by(ArrayList<String> read_by) {
        this.read_by = read_by;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }
}
