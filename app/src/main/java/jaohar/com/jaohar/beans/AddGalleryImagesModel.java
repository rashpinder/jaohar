package jaohar.com.jaohar.beans;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 5/29/2017.
 */

public class AddGalleryImagesModel implements Serializable {

    String strImageId = "";
    String strImagePath = "";
    String strImageName = "";
    String strImageBitmap = "";
    String strType = "";
    String strDocName = "";
    String strDocByteArray = "";
    Bitmap mBitmap ;
    Uri mUri ;
    byte byteArray[];
    String strImageCount = "";

    public String getOld_url() {
        return old_url;
    }

    public void setOld_url(String old_url) {
        this.old_url = old_url;
    }

    String old_url = "";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type = "";

    public Uri getmUri() {
        return mUri;
    }

    public void setmUri(Uri mUri) {
        this.mUri = mUri;
    }

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

    public String getStrDocName() {
        return strDocName;
    }

    public void setStrDocName(String strDocName) {
        this.strDocName = strDocName;
    }

    public String getStrDocByteArray() {
        return strDocByteArray;
    }

    public void setStrDocByteArray(String strDocByteArray) {
        this.strDocByteArray = strDocByteArray;
    }

    public String getStrImageCount() {
        return strImageCount;
    }

    public void setStrImageCount(String strImageCount) {
        this.strImageCount = strImageCount;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    public Bitmap getmBitmap() {
        return mBitmap;
    }

    public void setmBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
    }

    ;

    public String getStrImageBitmap() {
        return strImageBitmap;
    }

    public void setStrImageBitmap(String strImageBitmap) {
        this.strImageBitmap = strImageBitmap;
    }

    public String getStrImageId() {
        return strImageId;
    }

    public void setStrImageId(String strImageId) {
        this.strImageId = strImageId;
    }

    public String getStrImagePath() {
        return strImagePath;
    }

    public void setStrImagePath(String strImagePath) {
        this.strImagePath = strImagePath;
    }

    public String getStrImageName() {
        return strImageName;
    }

    public void setStrImageName(String strImageName) {
        this.strImageName = strImageName;
    }



    String id_doc = "";
    byte byteArrayDoc[] = null;
    String documentPath = "";
    String documentName = "";
    String documentBase64 = "";
    String documunetCount = "";
    String documunetExtension = "";
    Uri mUriDoc ;

    public String getDocumunetExtension() {
        return documunetExtension;
    }

    public void setDocumunetExtension(String documunetExtension) {
        this.documunetExtension = documunetExtension;
    }

    public Uri getmUriDoc() {
        return mUriDoc;
    }

    public void setmUriDoc(Uri mUriDoc) {
        this.mUriDoc = mUriDoc;
    }

    public String getDocumunetCount() {
        return documunetCount;
    }

    public void setDocumunetCount(String documunetCount) {
        this.documunetCount = documunetCount;
    }

    public String getIdDoc() {
        return id_doc;
    }

    public String getDocumentBase64() {
        return documentBase64;
    }

    public void setDocumentBase64(String documentBase64) {
        this.documentBase64 = documentBase64;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public void setIdDoc(String id_doc) {
        this.id_doc = id_doc;

    }


    public byte[] getByteArrayDoc() {
        return byteArrayDoc;

    }

    public void setByteArrayDoc(byte[] byteArrayDoc) {
        this.byteArrayDoc = byteArrayDoc;
    }

}
