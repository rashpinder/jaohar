package jaohar.com.jaohar.adapters.trash_module_adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.activities.DetailUniversalActivity;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.INVtrashRecoverDeleteInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteInvoiceTrashActivity;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;

public class InvoiceTrashAdapter  extends RecyclerView.Adapter<InvoiceTrashAdapter.ViewHolder> {
    DeleteInvoiceInterface mDeleteVesselsInterface;
    private Activity mActivity;
    private ArrayList<InVoicesModel> modelArrayList;
    paginationforVesselsInterface mPagination;
    INVtrashRecoverDeleteInterface mDELETERECOVER;
    private static HashMap<InVoicesModel, Boolean> checkedForModel = new HashMap<>();
    RecoverDeleteInvoiceTrashActivity mInvoiceSelectedInterface;
    private static boolean[] checkBoxState = null;

    public InvoiceTrashAdapter(Activity mActivity, ArrayList<InVoicesModel> modelArrayList,
                               paginationforVesselsInterface mPagination,
                               INVtrashRecoverDeleteInterface mDELETERECOVER,
                               RecoverDeleteInvoiceTrashActivity mInvoiceSelectedInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.mDELETERECOVER = mDELETERECOVER;
        this.mInvoiceSelectedInterface = mInvoiceSelectedInterface;
    }

    @Override
    public InvoiceTrashAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_trash, parent, false);
        return new InvoiceTrashAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final InvoiceTrashAdapter.ViewHolder holder, final int position) {
        checkBoxState=new boolean[modelArrayList.size()];
        final InVoicesModel tempValue = modelArrayList.get(position);

        // pagination for smooth scrooling
        if (position >= modelArrayList.size()-1 ){
            mPagination.mPaginationforVessels(true);
        }
        holder.iteminvoiceNUMTV.setText(tempValue.getInvoice_number());
        holder.deletedBYTV.setText(tempValue.getDeleted_by());

        String strDateFormat = null;

        /* checkBoxState has the value of checkBox ie true or false,
         * The position is used so that on scroll your selected checkBox maintain its state */
        if(checkBoxState != null){
            holder.imgMultiSelectIV.setChecked(checkBoxState[position]);
        }

        try {
            strDateFormat = Utilities.gettingFormatTime(tempValue.getInvoice_date().replace("_", " "));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.invoiceDateTV.setText(strDateFormat);
        if (tempValue.getmVesselSearchInvoiceModel() != null) {
            holder.invoice_vesselTV.setText(tempValue.getmVesselSearchInvoiceModel().getVessel_name());
        }else {
            holder.invoice_vesselTV.setText("");
        }
        if (tempValue.getmCompaniesModel() != null) {
            holder.invoiceCompanyTV.setText(tempValue.getmCompaniesModel().getCompany_name());
        }else {
            holder.invoiceCompanyTV.setText("");
        }

        holder.viewIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Invoice_ID = tempValue.getInvoice_id();
                Intent mIntent = new Intent(mActivity, DetailUniversalActivity.class);
                mIntent.putExtra("Model", tempValue);
                mActivity.startActivity(mIntent);
            }
        });

holder.recoverIMG.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        mDELETERECOVER.mInvTrashRecover(tempValue,"recover");
    }
});
holder.deleteIMG.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        mDELETERECOVER.mInvTrashRecover(tempValue,"delete");
    }
});

        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (
                    holder.imgMultiSelectIV.isChecked()) {
                    checkBoxState[position] = true;
                    ischecked(position,true);
                    mInvoiceSelectedInterface.mRecoverDeleteInvoice(tempValue,false);
                } else {
                    checkBoxState[position] = false;
                    ischecked(position,false);
                    mInvoiceSelectedInterface.mRecoverDeleteInvoice(tempValue,true);

                }

            }
        });
        /*if country is in checkedForCountry then set the checkBox to true */
        if (checkedForModel.get(tempValue) != null) {
            holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
        }

        /*Set tag to all checkBox*/
        holder.imgMultiSelectIV.setTag(tempValue);


    }

    private void ischecked(int position,boolean flag )
    {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView iteminvoiceNUMTV, invoiceDateTV, invoice_vesselTV, invoiceCompanyTV,deletedBYTV;
        public ImageView recoverIMG, viewIMG, deleteIMG;
        CheckBox imgMultiSelectIV;

        ViewHolder(View itemView) {
            super(itemView);
            recoverIMG = (ImageView) itemView.findViewById(R.id.recoverIMG);
            viewIMG = (ImageView) itemView.findViewById(R.id.viewIMG);
            deleteIMG = (ImageView) itemView.findViewById(R.id.deleteIMG);

            iteminvoiceNUMTV = (TextView) itemView.findViewById(R.id.iteminvoiceNUMTV);
            invoiceDateTV = (TextView) itemView.findViewById(R.id.invoiceDateTV);
            invoice_vesselTV = (TextView) itemView.findViewById(R.id.invoice_vesselTV);
            invoiceCompanyTV = (TextView) itemView.findViewById(R.id.invoiceCompanyTV);
            deletedBYTV = (TextView) itemView.findViewById(R.id.deletedBYTV);

            imgMultiSelectIV = (CheckBox) itemView.findViewById(R.id.imgMultiSelectIV);
        }
    }
}