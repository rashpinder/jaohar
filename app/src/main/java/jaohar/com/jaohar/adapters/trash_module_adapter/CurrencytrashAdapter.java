package jaohar.com.jaohar.adapters.trash_module_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteCurrencySelectedInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteSingleTarshInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.models.CurrencyData;

public class CurrencytrashAdapter extends RecyclerView.Adapter<CurrencytrashAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<CurrencyData> modelArrayList;

    private static HashMap<CurrencyData, Boolean> checkedForModel = new HashMap<>();
    RecoverDeleteSingleTarshInterface mrecoverDelete;
    RecoverDeleteCurrencySelectedInterface mRecoverDeleteSelected;
    private static boolean[] checkBoxState = null;
    public CurrencytrashAdapter(Activity mActivity, ArrayList<CurrencyData> modelArrayList,RecoverDeleteCurrencySelectedInterface mRecoverDeleteSelected,RecoverDeleteSingleTarshInterface  mrecoverDelete) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;

        this.mrecoverDelete = mrecoverDelete;
        this.mRecoverDeleteSelected = mRecoverDeleteSelected;
        checkBoxState=new boolean[modelArrayList.size()];
    }

    @Override
    public CurrencytrashAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_currency_trash, parent, false);
        return new CurrencytrashAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CurrencytrashAdapter.ViewHolder holder, final int position) {
        final CurrencyData tempValue = modelArrayList.get(position);

        holder.itemcurrencyNameTV.setText(tempValue.getAliasName());
        holder.deletedBYTV.setText(tempValue.getAddedBy());
        holder.currencyCodeTV.setText(tempValue.getCurrencyName());



        holder.recoverIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mrecoverDelete.mRecoverDeleteSingleTrash(tempValue,"recover");

            }
        });
        holder.deleteIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mrecoverDelete.mRecoverDeleteSingleTrash(tempValue,"delete");
            }
        });

        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (
                        holder.imgMultiSelectIV.isChecked()) {
                    checkBoxState[position] = true;
                    ischecked(position,true);
                    mRecoverDeleteSelected.mRecoverSelected(tempValue,false);
                } else {
                    checkBoxState[position] = false;
                    ischecked(position,false);
                    mRecoverDeleteSelected.mRecoverSelected(tempValue,true);

                }

            }
        });
        /*if country is in checkedForCountry then set the checkBox to true */
        if (checkedForModel.get(tempValue) != null) {
            holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
        }

        /*Set tag to all checkBox*/
        holder.imgMultiSelectIV.setTag(tempValue);


    }

    private void ischecked(int position,boolean flag )
    {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemcurrencyNameTV, currencyCodeTV, deletedBYTV;
        public ImageView recoverIMG, viewIMG, deleteIMG;
        CheckBox imgMultiSelectIV;

        ViewHolder(View itemView) {
            super(itemView);
            recoverIMG = (ImageView) itemView.findViewById(R.id.recoverIMG);
            viewIMG = (ImageView) itemView.findViewById(R.id.viewIMG);
            deleteIMG = (ImageView) itemView.findViewById(R.id.deleteIMG);

            itemcurrencyNameTV = (TextView) itemView.findViewById(R.id.itemcurrencyNameTV);
            currencyCodeTV = (TextView) itemView.findViewById(R.id.currencyCodeTV);
            deletedBYTV = (TextView) itemView.findViewById(R.id.deletedBYTV);
            imgMultiSelectIV = (CheckBox) itemView.findViewById(R.id.imgMultiSelectIV);
        }
    }
}
