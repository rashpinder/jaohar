package jaohar.com.jaohar.adapters.invoices_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.pendingPayments.AllInvoice;
import jaohar.com.jaohar.models.pendingPayments.SearchAllSearchedInvoice;

public class SearchPendingPaymentsAdapter extends RecyclerView.Adapter<SearchPendingPaymentsAdapter.ViewHolder> {
    Context activity;
    ArrayList<AllInvoice> modelArrayList;
    ArrayList<SearchAllSearchedInvoice> searchAllSearchedInvoices;
    OnClickInterface mOnClickInterface;

    DeleteInvoiceInterface mDeleteInvoiceInterface;
    PDFdownloadInterface mPdfDownloader;
    SinglemailInvoiceInterface msinglemailInvoiceInterface;
    SendMultipleInvoiceMAIlInterface mMultimaIlInterface;
    String from;

    public SearchPendingPaymentsAdapter(Activity mActivity, ArrayList<SearchAllSearchedInvoice> searchAllSearchedInvoices,
                                        DeleteInvoiceInterface mDeleteInvoiceInterface, PDFdownloadInterface mPdfDownloader,
                                        SinglemailInvoiceInterface msinglemailInvoiceInterface,
                                        SendMultipleInvoiceMAIlInterface mMultimaIlInterface, OnClickInterface mOnClickInterface, String from) {
        this.activity = mActivity;
        this.searchAllSearchedInvoices = searchAllSearchedInvoices;
        this.mOnClickInterface = mOnClickInterface;
        this.mDeleteInvoiceInterface = mDeleteInvoiceInterface;
        this.mPdfDownloader = mPdfDownloader;
        this.msinglemailInvoiceInterface = msinglemailInvoiceInterface;
        this.mMultimaIlInterface = mMultimaIlInterface;
        this.from = from;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_payments_row, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        SearchAllSearchedInvoice allInvoice = searchAllSearchedInvoices.get(position);

        holder.companyNameTV.setText(allInvoice.getPpCompanyDetail().getCompanyName());
        holder.vesselNameTextView.setText(allInvoice.getPpVessel1Detail().getVesselName() + "," + allInvoice.getPpVessel2Detail().getVesselName() + ","
                + allInvoice.getPpVessel3Detail().getVesselName());
        holder.numberValueTextView.setText(allInvoice.getAllData().getPpNo());
        holder.amountTextView.setText(allInvoice.getAllData().getPpAmtDue() + ".00 " + allInvoice.getAllData().getPpCurrency());
        holder.dateTextView.setText(allInvoice.getAllData().getPpDate());


        if (allInvoice.getAllData().getPpStatus().equalsIgnoreCase("Cancelled")) {
            holder.statusImageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.cancelled));
        } else if (allInvoice.getAllData().getPpStatus().equalsIgnoreCase("Pending")) {
            holder.statusImageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.pending));
        } else if (allInvoice.getAllData().getPpStatus().equalsIgnoreCase("Pending Approval")) {
            holder.statusImageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.pendingapproval));
        }

        holder.optionsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickInterface.mOnClickInterface(position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return searchAllSearchedInvoices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView companyNameTV;
        private final TextView vesselNameTextView;
        private final TextView numberValueTextView;
        private final TextView amountTextView;
        private final TextView dateTextView;
        private final ImageView optionsImageView;
        private final ImageView statusImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            companyNameTV = itemView.findViewById(R.id.companyNameTV);
            vesselNameTextView = itemView.findViewById(R.id.vesselNameTextView);
            numberValueTextView = itemView.findViewById(R.id.numberValueTextView);
            amountTextView = itemView.findViewById(R.id.amountTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            optionsImageView = itemView.findViewById(R.id.optionsImageView);
            statusImageView = itemView.findViewById(R.id.statusImageView);


        }
    }
}
