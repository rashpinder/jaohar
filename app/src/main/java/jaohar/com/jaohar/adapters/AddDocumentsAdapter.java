package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.interfaces.DeleteVesselsInterface;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class AddDocumentsAdapter extends RecyclerView.Adapter<AddDocumentsAdapter.ViewHolder> {
    DeleteVesselsInterface mDeleteVesselsInterface;
    private Activity mActivity;
    private ArrayList<DocumentModel> modelArrayList;

    public AddDocumentsAdapter(Activity mActivity, ArrayList<DocumentModel> modelArrayList) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final DocumentModel tempValue = modelArrayList.get(position);
        holder.txtItemsTV.setText(tempValue.getDocumentName());
        holder.imgRemoveIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelArrayList.remove(position);
                notifyDataSetChanged();
            }
        });
    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemsTV;
        public ImageView imgRemoveIV;

        ViewHolder(View itemView) {
            super(itemView);
            imgRemoveIV = (ImageView) itemView.findViewById(R.id.imgRemoveIV);
            txtItemsTV = (TextView) itemView.findViewById(R.id.txtItemsTV);
        }
    }
}