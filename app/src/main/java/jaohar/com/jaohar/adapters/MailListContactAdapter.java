package jaohar.com.jaohar.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.DeleteContacstMailInterface;
import jaohar.com.jaohar.interfaces.EditContactsMailListInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllMailListContact;

/**
 * Created by dharmaniz on 16/1/19.
 */

public class MailListContactAdapter extends RecyclerView.Adapter<MailListContactAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<AllMailListContact> modelArrayList;
    private EditContactsMailListInterface mEditList;
    private DeleteContacstMailInterface mDeleteList;
    paginationforVesselsInterface mPagination;

    public MailListContactAdapter(Activity mActivity, ArrayList<AllMailListContact> modelArrayList, DeleteContacstMailInterface mDeleteList, EditContactsMailListInterface mEditList, paginationforVesselsInterface mPagination) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditList = mEditList;
        this.mDeleteList = mDeleteList;
        this.mPagination = mPagination;
    }

    @Override
    public MailListContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact_mailing_list, parent, false);
        return new MailListContactAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MailListContactAdapter.ViewHolder holder, final int position) {
        final AllMailListContact tempValue = modelArrayList.get(position);
        if (position >= modelArrayList.size() - 1) {
            mPagination.mPaginationforVessels(true);
        }

//        String strCOUNT = String.valueOf(position + 1);
//        holder.txtEdit.setVisibility(View.VISIBLE);
//        holder.txtDelete.setVisibility(View.VISIBLE);
//        holder.txtMailTV.setVisibility(View.GONE);

        holder.modifyLL.setVisibility(View.GONE);
        holder.modifybyLL.setVisibility(View.GONE);

//        Log.e(String.valueOf(mActivity), "strCount" + strCOUNT);
        if (!tempValue.getEmail1().equals("")) {
            holder.email1TV.setText(tempValue.getEmail1());
        }
        if (!tempValue.getCompany().equals("")) {
            holder.companyTV.setText(tempValue.getCompany());
        }
        if (!tempValue.getFirstname().equals("")) {
            holder.listNameTV.setText(tempValue.getFirstname());
        }
        if (!tempValue.getLastname().equals("")) {
            holder.listNameTV.setText(tempValue.getFirstname() + " " + tempValue.getLastname());
        }

        if (!tempValue.getEmail2().equals("")) {
            holder.email2LL.setVisibility(View.VISIBLE);
            holder.email2TV.setText(tempValue.getEmail2());
        }

//        if (position % 2 == 0) {
//            holder.mainLL.setBackgroundResource(R.drawable.pdf_row_odd);
//            holder.startMAINLL.setBackgroundResource(R.drawable.pdf_row_odd);
//            holder.mainbgll.setBackgroundResource(R.drawable.pdf_row_odd);
////            holder.mainbgll.setBackgroundResource(R.drawable.pdf_row_odd);
//        } else {
//            holder.mainLL.setBackgroundResource(R.drawable.pdf_row_even);
//            holder.startMAINLL.setBackgroundResource(R.drawable.pdf_row_even);
//            holder.mainbgll.setBackgroundResource(R.drawable.pdf_row_even);
////            holder.mainbgll.setBackgroundResource(R.drawable.pdf_row_even);
//        }
//        holder.mainLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mIntent = new Intent(mActivity, ShowMailingContactLISTActivity.class);
//                mIntent.putExtra("listID", tempValue.getList_id());
//                mActivity.startActivity(mIntent);
//            }
//        });
//        holder.startMAINLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mIntent = new Intent(mActivity, ShowMailingContactLISTActivity.class);
//                mIntent.putExtra("listID", tempValue.getList_id());
//                mActivity.startActivity(mIntent);
//            }
//        });

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditList.editContactsMailListInterface(tempValue);
            }
        });
        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteList.deleteContacstMailInterface(tempValue.getContactId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList == null ? 0 : modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView listNameTV, txtEdit, txtMailTV, txtDelete, email1TV, email2TV, companyTV;
        public LinearLayout mainLL, startMAINLL, email2LL, modifyLL, modifybyLL;
        public RelativeLayout mainbgll;

        ViewHolder(View itemView) {
            super(itemView);

            companyTV = itemView.findViewById(R.id.companyTV);
            email2TV = itemView.findViewById(R.id.email2TV);
            email1TV = itemView.findViewById(R.id.email1TV);
            listNameTV = itemView.findViewById(R.id.listNameTV);
            txtMailTV = itemView.findViewById(R.id.txtMailTV);
            txtDelete = itemView.findViewById(R.id.txtDelete);
            txtEdit = itemView.findViewById(R.id.txtEdit);
            mainLL = itemView.findViewById(R.id.mainLL);
            startMAINLL = itemView.findViewById(R.id.startMAINLL);
            email2LL = itemView.findViewById(R.id.email2LL);
            modifyLL = itemView.findViewById(R.id.modifyLL);
            modifybyLL = itemView.findViewById(R.id.modifybyLL);
            mainbgll = itemView.findViewById(R.id.mainbgll);
//            mainbgll = itemView.findViewById(R.id.mainbgll);
        }
    }
}