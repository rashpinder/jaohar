package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.interfaces.DeleteCurrencyInterface;
import jaohar.com.jaohar.interfaces.EditCurrencyInterface;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class CurrenciesAdapter extends RecyclerView.Adapter<CurrenciesAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<CurrenciesModel> modelArrayList;
    private DeleteCurrencyInterface mDeleteCurrencyInterface;
    private EditCurrencyInterface mEditCurrencyInterface;

    public CurrenciesAdapter(Activity mActivity, ArrayList<CurrenciesModel> modelArrayList, DeleteCurrencyInterface mDeleteCurrencyInterface, EditCurrencyInterface mEditCurrencyInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteCurrencyInterface = mDeleteCurrencyInterface;
        this.mEditCurrencyInterface = mEditCurrencyInterface;
    }

    @Override
    public CurrenciesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_currency, null);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(CurrenciesAdapter.ViewHolder holder, int position) {
        final CurrenciesModel tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getAlias_name());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", tempValue);
                mActivity.setResult(777, returnIntent);
                mActivity.finish();
            }
        });
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditCurrencyInterface.editCurrency(tempValue);
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteCurrencyInterface.deleteCurrency(tempValue, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}
