package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.DetailUniversalActivity;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;

/**
 * Created by dharmaniz on 1/3/19.
 */

public class AllUniversalInvoiceAdapter extends RecyclerView.Adapter<AllUniversalInvoiceAdapter.ViewHolder> {
    private static final HashMap<InVoicesModel, Boolean> checkedForModel = new HashMap<>();
    private static boolean[] checkBoxState = null;
    DeleteInvoiceInterface mDeleteVesselsInterface;
    PDFdownloadInterface mPdfDownloader;
    SinglemailInvoiceInterface mInvoiceI;
    SendMultipleInvoiceMAIlInterface sendMultipleInvoiceMAIlInterface;
    private final Activity mActivity;
    private final ArrayList<InVoicesModel> modelArrayList;
    private final OnClickInterface onClickInterface;

    public AllUniversalInvoiceAdapter(Activity mActivity, ArrayList<InVoicesModel> modelArrayList, DeleteInvoiceInterface mDeleteVesselsInterface, PDFdownloadInterface mPdfDownloader, SinglemailInvoiceInterface mInvoiceI, SendMultipleInvoiceMAIlInterface sendMultipleInvoiceMAIlInterface
            , OnClickInterface onClickInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteVesselsInterface = mDeleteVesselsInterface;
        this.mPdfDownloader = mPdfDownloader;

        this.mInvoiceI = mInvoiceI;
        this.sendMultipleInvoiceMAIlInterface = sendMultipleInvoiceMAIlInterface;
        checkBoxState = new boolean[modelArrayList.size()];
        this.onClickInterface = onClickInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_new_universal_invoice, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final InVoicesModel tempValue = modelArrayList.get(position);
        if(tempValue.getmPaymentModel().gettotal()!=null){
        holder.inVoiceTotalTV.setText(tempValue.getmPaymentModel().gettotal()+" "+tempValue.getCurrency());}

        holder.inVoiceNumberTV.setText(tempValue.getInvoice_number());
        String strDateFormat = null;


        /* checkBoxState has the value of checkBox ie true or false,
         * The position is used so that on scroll your selected checkBox maintain its state */
        if (checkBoxState != null) {
            holder.imgMultiSelectIV.setChecked(checkBoxState[position]);
        }

        try {
            strDateFormat = Utilities.gettingFormatTime(tempValue.getInvoice_date().replace("_", " "));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.dateTV.setText(strDateFormat);

        if (tempValue.getmVesselSearchInvoiceModel() != null) {
            holder.inVoiceVesselTV.setText(tempValue.getmVesselSearchInvoiceModel().getVessel_name());
        }
        if (tempValue.getmCompaniesModel() != null) {
            holder.invoiceCompanyTV.setText(tempValue.getmCompaniesModel().getCompany_name());
        }

//        holder.item_FileDownloadIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mPdfDownloader.pdfDownloadInterface(tempValue.getPdf(), tempValue.getPdf_name());
//            }
//        });
//
//        holder.item_EditIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
//                JaoharConstants.Universal_Invoice_ID = tempValue.getInvoice_id();
//                Intent mIntent = new Intent(mActivity, EditUniversalInvoiceActivity.class);
//                mIntent.putExtra("Model", tempValue);
//                mActivity.startActivity(mIntent);
//            }
//        });
//        holder.item_DeleteIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDeleteVesselsInterface.deleteInvoice(tempValue);
//            }
//        });
//
//        holder.item_MailDetailsIV.setVisibility(View.VISIBLE);
//        holder.item_MailDetailsIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mInvoiceI.mSinglemailInvoice(tempValue);
//            }
//        });
//
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JaoharConstants.Universal_Invoice_ID = tempValue.getInvoice_id();
                Intent mIntent = new Intent(mActivity, DetailUniversalActivity.class);
                mIntent.putExtra("Model", tempValue);
                mActivity.startActivity(mIntent);
            }
        });

        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
                if (
                        holder.imgMultiSelectIV.isChecked()) {
                    checkBoxState[position] = true;
                    ischecked(position, true);
                    sendMultipleInvoiceMAIlInterface.mSendMutliInvoice(tempValue, false);
                } else {
                    checkBoxState[position] = false;
                    ischecked(position, false);
                    sendMultipleInvoiceMAIlInterface.mSendMutliInvoice(tempValue, true);
                }

            }
        });
        /*if country is in checkedForCountry then set the checkBox to true */
        if (checkedForModel.get(tempValue) != null) {
            holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
        }

        /*Set tag to all checkBox*/
        holder.imgMultiSelectIV.setTag(tempValue);

        holder.optionsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(position);
            }
        });

    }

    private void ischecked(int position, boolean flag) {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView inVoiceNumberTV, dateTV, inVoiceVesselTV, invoiceCompanyTV, inVoiceTotalTV;
        public ImageView optionsIV;
        //        RelativeLayout layoutItemRL;
        CheckBox imgMultiSelectIV;

        ViewHolder(View itemView) {
            super(itemView);
            optionsIV = itemView.findViewById(R.id.optionsIV);
            inVoiceNumberTV = itemView.findViewById(R.id.inVoiceNumberTV);
            dateTV = itemView.findViewById(R.id.dateTV);
            inVoiceVesselTV = itemView.findViewById(R.id.inVoiceVesselTV);
            invoiceCompanyTV = itemView.findViewById(R.id.invoiceCompanyTV);
            inVoiceTotalTV = itemView.findViewById(R.id.inVoiceTotalTV);
//            layoutItemRL = (RelativeLayout) itemView.findViewById(R.id.layoutItemRL);
            imgMultiSelectIV = itemView.findViewById(R.id.imgMultiSelectIV);
        }
    }
}