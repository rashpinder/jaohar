package jaohar.com.jaohar.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.GalleryActivity;


/**
 * Created by Dharmani Apps on 8/19/2017.
 */

public class DetailsPagerAdapter extends PagerAdapter {
    String TAG = "DetailsPagerAdapter";
    Context mContext;
    ArrayList<String> mImageArryList;
    LayoutInflater mLayoutInflater;

    public DetailsPagerAdapter(Context context, ArrayList<String> mImageArryList) {
        this.mContext = context;
        this.mImageArryList = mImageArryList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        Log.e(TAG,"******SIZE*****"+mImageArryList.size());
        return mImageArryList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_viewpager, container, false);
        String mString = mImageArryList.get(position);
        com.android.volley.toolbox.NetworkImageView  item_ImageView = (NetworkImageView) itemView.findViewById(R.id.item_ImageView);

        item_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GalleryActivity.class);
                intent.putStringArrayListExtra("LIST", mImageArryList);
                mContext.startActivity(intent);
            }
        });
        if (mString.length() > 0){
            item_ImageView.setImageUrl(mString, JaoharApplication.getInstance().getImageLoader());
            container.addView(itemView);
        }


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}