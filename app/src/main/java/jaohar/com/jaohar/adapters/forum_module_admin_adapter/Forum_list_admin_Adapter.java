package jaohar.com.jaohar.adapters.forum_module_admin_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;


import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.R;

import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.forum_module_admin.CloseAndDeleteForumInterface;


public class Forum_list_admin_Adapter extends RecyclerView.Adapter<Forum_list_admin_Adapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<ForumModel> modelArrayList;
    PaginationListForumAdapter mPagination;
    ForumItemClickInterace mInterfaceForum;
    CloseAndDeleteForumInterface mCloseAndDelInterface;
    private ImageLoader mImageLoader;

    public Forum_list_admin_Adapter(Activity mActivity, ArrayList<ForumModel> modelArrayList, PaginationListForumAdapter mPagination,ForumItemClickInterace mInterfaceForum,CloseAndDeleteForumInterface mCloseAndDelInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.mInterfaceForum = mInterfaceForum;
        this.mCloseAndDelInterface = mCloseAndDelInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_forum_admin, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ForumModel tempValue = modelArrayList.get(position);
        holder.setIsRecyclable(false);

        // pagination for smooth scrooling
        if (position >= modelArrayList.size()-1 ){
            mPagination.mPaginationforVessels(true);
        }
        holder.vesselNameTV.setText(tempValue.getVessel_name());

        if(!tempValue.getUnread_messages().equals("0")){
            holder.totalMessagesTV.setText(tempValue.getUnread_messages());
        }else {
            holder.totalMessagesTV.setVisibility(View.GONE);
        }
        holder.imoNumTV.setText(tempValue.getIMO_number());

        if(!tempValue.getPic().equals("")){
            Glide.with(mActivity).load(modelArrayList.get(position).getPic()).into(holder.forumIMG);
        }

        /* *
         * Setting Up Click for Viewing Forum
         * */
        holder.mainLayoutClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfaceForum.ForumItemClick(tempValue);


            }
        });

        holder.txtEdit.setText("Close");

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCloseAndDelInterface.mCloseAndDeleteInterFace(tempValue,"close");
            }
        });
         holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCloseAndDelInterface.mCloseAndDeleteInterFace(tempValue,"del");
            }
        });


        /*Vessels Status on Vessel Image*/
        if (tempValue.getStatus().equals("Sold")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_sold);
        } else if (tempValue.getStatus().equals("Withdrawn")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_withdrawn);
        } else if (tempValue.getStatus().equals("Committed")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_commited);
        } else if (tempValue.getStatus().equals("Scraped")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.scraped);
        } else if (tempValue.getStatus().equals("Hot Sale")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.icon_hot_sale);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vesselNameTV, totalMessagesTV, imoNumTV,txtEdit,txtDelete;
        public ImageView item_Image_Status;
        public LinearLayout mainLayoutClick;
        public CircleImageView forumIMG;

        ViewHolder(View itemView) {
            super(itemView);
            forumIMG = (CircleImageView) itemView.findViewById(R.id.forumIMG);
            vesselNameTV = (TextView) itemView.findViewById(R.id.vesselNameTV);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
            totalMessagesTV = (TextView) itemView.findViewById(R.id.totalMessagesTV);
            imoNumTV = (TextView) itemView.findViewById(R.id.imoNumTV);
            mainLayoutClick = (LinearLayout) itemView.findViewById(R.id.mainLayoutClick);
            item_Image_Status = (ImageView) itemView.findViewById(R.id.item_Image_Status);
        }
    }
}

