package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.animation.GlideAnimation;

import java.io.Serializable;
import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.AllNewsActivity;
import jaohar.com.jaohar.activities.DetailsActivity;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.utils.JaoharConstants;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class VesselesForSaleAdapter extends RecyclerView.Adapter<VesselesForSaleAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<AllVessel> modelArrayList;
    String strNormalText, strNewsTEXt, strPhotoURL;
    paginationforVesselsInterface mPagination;
    OpenPopUpVesselInterface mOpenPoUpInterface;
    String strIsVesselForsale = "";
    OnClickInterface onClickInterface;
    FavoriteVesselInterface favoriteVesselInterface;
    ArrayList<String> FavouriteItems = new ArrayList<>();

    public VesselesForSaleAdapter(Activity mActivity, ArrayList<AllVessel> modelArrayList,
                                  String strNormalText, String strNewsTEXt, String strPhotoURL,
                                  paginationforVesselsInterface mPagination,
                                  OpenPopUpVesselInterface mOpenPoUpInterface, String strIsVesselForsale,
                                  OnClickInterface onClickInterface, FavoriteVesselInterface favoriteVesselInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.strNormalText = strNormalText;
        this.strNewsTEXt = strNewsTEXt;
        this.strPhotoURL = strPhotoURL;
        this.mPagination = mPagination;
        this.strIsVesselForsale = strIsVesselForsale;
        this.mOpenPoUpInterface = mOpenPoUpInterface;
        this.onClickInterface = onClickInterface;
        this.favoriteVesselInterface = favoriteVesselInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vessels, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        AllVessel tempValue = modelArrayList.get(position);
        holder.imgMultiSelectIV.setVisibility(View.GONE);

//        if (position >= modelArrayList.size() - 1) {
//            mPagination.mPaginationforVessels(true);
//        }

        if (position == 0) {
//            holder.relativeLL.setVisibility(View.VISIBLE);
            holder.normalTextTV.setText(strNormalText);
        } else {
            holder.relativeLL.setVisibility(View.GONE);
        }

        holder.relativeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenPoUpInterface.mOpenPopUpVesselInterface(strNewsTEXt, strPhotoURL);
            }
        });

        holder.allnewsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strIsVesselForsale.equals("true")) {
                    JaoharConstants.IS_CLICK_FROM_VESSELS_FOR_SALE = true;
                    Intent mIntent = new Intent(mActivity, AllNewsActivity.class);
                    mActivity.startActivity(mIntent);
                } else {
                    JaoharConstants.IS_SEE_ALL_NEWS_CLICK = true;
                    Intent mIntent = new Intent(mActivity, AllNewsActivity.class);
                    mActivity.startActivity(mIntent);
                }

            }
        });

        holder.normalTextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenPoUpInterface.mOpenPopUpVesselInterface(strNewsTEXt, strPhotoURL);
            }
        });

        holder.linkIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenPoUpInterface.mOpenPopUpVesselInterface(strNewsTEXt, strPhotoURL);
            }
        });
        if (tempValue.getPhoto1().length() > 0 && tempValue.getPhoto1().contains("http")) {
//            Glide.with(mActivity)
//                    .load(tempValue.getPhoto1())
//                    .asBitmap()
//                    .placeholder(R.drawable.palace_holder)
//                    .into(new SimpleTarget<Bitmap>() {
//                        @Override
//                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                            // you can do something with loaded bitmap here
////                            Bitmap scaleBitmap = Utilities.getScaledBitMapBaseOnScreenSize(resource, mActivity);
//                            if (resource != null) {
//                                holder.item_Image.setImageBitmap(resource);
//                            }
//                        }
//                    });
            Glide.with(mActivity).load(tempValue.getPhoto1()).into(holder.item_Image);

//      holder.item_Image.setImageUrl(tempValue.getPhoto1(), JaoharApplication.getInstance().getImageLoader());
        } else {
            holder.item_Image.setImageResource(R.drawable.palace_holder);
        }

        if (tempValue.getVesselName().length() > 0) {
            holder.item_Title.setText(tempValue.getVesselName());
        }
        if (tempValue.getVesselType().length() > 0) {
            holder.item_DateTime.setText(tempValue.getVesselType() + "/" + tempValue.getYearBuilt() + "/" + tempValue.getPlaceOfBuilt());
        }
        if (tempValue.getShortDescription().length() > 0) {
            holder.item_Type.setText("DWT " + tempValue.getCapacity() + "/" + tempValue.getPriceIdea() + " " + tempValue.getCurrency());
        }

        holder.imgRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//              if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                Intent mIntent = new Intent(mActivity, DetailsActivity.class);
                mIntent.putExtra("Model", tempValue);
                mIntent.putExtra("isEditShow", false);
                mActivity.startActivity(mIntent);
//              }else{
//                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
//                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE,JaoharConstants.USER);
//                    mIntent.putExtra("Model",tempValue);
//                    mActivity.startActivity(mIntent);
//              }
            }
        });

        /*Vessels Status on Vessel Image*/
        if (tempValue.getStatus().equals("Sold")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_sold);
        } else if (tempValue.getStatus().equals("Withdrawn")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_withdrawn);
        } else if (tempValue.getStatus().equals("Commited")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_committed);
        } else if (tempValue.getStatus().equals("Scraped")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_scraped);
        }else if (tempValue.getStatus().equals("Hot Sale")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_hotsale);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }

        holder.optionsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(position);
            }
        });

        if (tempValue.getFavoriteStatus().equals("0")) {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_border);
            FavouriteItems.remove(String.valueOf(position));
        } else if (tempValue.getFavoriteStatus().equals("1")) {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_yellow);
            FavouriteItems.add(String.valueOf(position));
        } else {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_border);
        }

        holder.favImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (tempValue.getFavorite_status().equals("0")) {
//                    favoriteVesselInterface.mFavoriteVesselInterface(position, "1", holder.favImg);
//                } else {
//                    favoriteVesselInterface.mFavoriteVesselInterface(position, "0", holder.favImg);
//                }

                if (FavouriteItems.contains(String.valueOf(position))) {
                    FavouriteItems.remove(String.valueOf(position));
                    favoriteVesselInterface.mFavoriteVesselInterface(position, "0", holder.favImg);
                } else {
                    FavouriteItems.add(String.valueOf(position));
                    favoriteVesselInterface.mFavoriteVesselInterface(position, "1", holder.favImg);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList == null ? 0 : modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, item_DateTime, item_Type, normalTextTV, allnewsTV;
        public ImageView itemImgNext, item_Image_Status;
        public ImageView item_Image, linkIMG, favImg, optionsImg;
        public CardView cardViewItem;
        public RelativeLayout itemClickRL, relativeLL, imgRL;
        public CheckBox imgMultiSelectIV;

        ViewHolder(View itemView) {
            super(itemView);

            item_Image = (ImageView) itemView.findViewById(R.id.item_Image);
            item_Image_Status = (ImageView) itemView.findViewById(R.id.item_Image_Status);
            itemImgNext = (ImageView) itemView.findViewById(R.id.itemImgNext);
            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            item_DateTime = (TextView) itemView.findViewById(R.id.item_DateTime);
            item_Type = (TextView) itemView.findViewById(R.id.item_Type);
            cardViewItem = (CardView) itemView.findViewById(R.id.cardViewItem);
            itemClickRL = (RelativeLayout) itemView.findViewById(R.id.itemClickRL);
            imgMultiSelectIV = (CheckBox) itemView.findViewById(R.id.imgMultiSelectIV);
            relativeLL = itemView.findViewById(R.id.relativeLL);
            normalTextTV = itemView.findViewById(R.id.normalTextTV);
            allnewsTV = itemView.findViewById(R.id.allnewsTV);
            linkIMG = itemView.findViewById(R.id.linkIMG);
            imgRL = itemView.findViewById(R.id.imgRL);
            favImg = itemView.findViewById(R.id.favImg);
            optionsImg = itemView.findViewById(R.id.optionsImg);
        }
    }
}