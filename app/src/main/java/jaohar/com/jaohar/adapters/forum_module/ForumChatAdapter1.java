package jaohar.com.jaohar.adapters.forum_module;

import static jaohar.com.jaohar.beans.ForumModule.ForumChatModel.Receiver;
import static jaohar.com.jaohar.beans.ForumModule.ForumChatModel.Sender;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Vibrator;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.GalleryActivity;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.forumModule.Add_Del_Edit_forumchatInterface;
import jaohar.com.jaohar.models.forummodels.AllMessagesItem;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;

public class ForumChatAdapter1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    int lastItemPosition = -1;
    private List<AllMessagesItem> modelArrayList;
    private ArrayList<String> mImageArryList = new ArrayList<String>();
    Add_Del_Edit_forumchatInterface mAdd_Del_EditInteface;
    OnClickInterface mOnClickInterface;
    Vibrator vibe;
    String strLastPage = "";

    public ForumChatAdapter1(Context context, List<AllMessagesItem> modelArrayList,
                            Add_Del_Edit_forumchatInterface mAdd_Del_EditInteface,
                            String strLastPage, OnClickInterface mOnClickInterface) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.mAdd_Del_EditInteface = mAdd_Del_EditInteface;
        this.strLastPage = strLastPage;
        this.mOnClickInterface = mOnClickInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Receiver:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_left_chat1, parent, false);
                return new ForumChatAdapter1.RecieverViewHolder(view);
            case Sender:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_right_chat1, parent, false);
                return new ForumChatAdapter1.SenderViewHolder(view);
        }
        return null;
    }

    public void updateLastPageData(String LastPage) {
        strLastPage = LastPage;
    }

    public static String formatToYesterdayOrToday(String datee) {
        Date dateTime = null;
        try {
            dateTime = new SimpleDateFormat("EEE hh:mma MMM d, yyyy").parse(datee);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("dd MMM, yyyy");
        String date = timeFormatter.format(dateTime);

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today";
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday";
        } else {
            return date;
        }
    }

    public static String getDateFromUixTimeStamp(long unixSeconds) {
        // convert seconds to milliseconds
        Date date = new Date(unixSeconds * 1000L);
        // the format of your date
        SimpleDateFormat sdf = new SimpleDateFormat("EEE hh:mma MMM d, yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        Log.e("TAG", "formattedDate: " + formattedDate);

        return formattedDate;
    }

    public static String getDateFromUixTimeStampFormat(long unixSeconds) {
        // convert seconds to milliseconds
        Date date = new Date(unixSeconds * 1000L);
        // the format of your date
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        Log.e("TAG", "formattedDate: " + formattedDate);

        return formattedDate;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final AllMessagesItem object = modelArrayList.get(position);

//        mOnClickInterface.mOnClickInterface(position);

        vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (position > lastItemPosition) {
            // Scrolled Down
        } else {
            // Scrolled Up
        }
        lastItemPosition = position;
        if (object != null) switch (object.getIntType()) {
            /**************
             * TYPE_HEADER CASE
             * ****************/
            case Receiver:

                /* set date at top */
                if (object.getCreationDate() != null && !object.getCreationDate().equals("")) {

                    ((ForumChatAdapter1.RecieverViewHolder) holder).ChatDateTV.setText(formatToYesterdayOrToday(getDateFromUixTimeStamp(Long.parseLong(object.getCreationDate()))));

                    String thisDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position).getCreationDate()));
                    String nextDate = "";

                    if (position == 0) {
                        nextDate = thisDate;
                    } else {
                        if (modelArrayList.get(position - 1).getCreationDate() != null)
                            nextDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position - 1).getCreationDate()));
                    }

                    // enable section heading if it's the first one, or
                    // different from the previous one
                    if (nextDate.equals(thisDate)) {
                        if (strLastPage.equals("TRUE") && position == 0) {
                            ((ForumChatAdapter1.RecieverViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                        } else {
                            ((ForumChatAdapter1.RecieverViewHolder) holder).ChatDateTV.setVisibility(View.GONE);
                        }
                    } else {
                        ((ForumChatAdapter1.RecieverViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                    }
                }

                /* set user details */
                if (object.getUserDetail() != null && object.getUserDetail().getImage() != null && !object.getUserDetail().getImage().equals("")) {
                    Glide.with(context).load(object.getUserDetail().getImage()).placeholder(R.drawable.ic_user).into(((RecieverViewHolder) holder).userProfileIV);
                }

                ((RecieverViewHolder) holder).SingleMessageLL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).SingleImageRL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).SingleImgWithMsgLL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).Images2and3RL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).Images4and5RL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).ReplyForSingleMsgRL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).replyForImagesLL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).replyForMsgWithSingleImgIV.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).imageleftChatReplyRL.setVisibility(View.GONE);
                ((RecieverViewHolder) holder).replyForImagesProfileRL.setVisibility(View.GONE);

                /* set send message in message textviews */
                if (object.getMessage() != null) {
                    ((RecieverViewHolder) holder).SingleMessageTV.setText(html2text(object.getMessage()));
                    ((RecieverViewHolder) holder).SingleImgWithMsgMessageTV.setText(html2text(object.getMessage()));
                    ((RecieverViewHolder) holder).text2and3TV.setText(html2text(object.getMessage()));
                    ((RecieverViewHolder) holder).text4and5WithMsgTV.setText(html2text(object.getMessage()));
                    ((RecieverViewHolder) holder).SingleImgWithMsgMessageTV.setText(html2text(object.getMessage()));
                    ((RecieverViewHolder) holder).ReplyMsgForSingleMsgTV.setText(html2text(object.getMessage()));
                }

                /* convert seconds to milliseconds and set on time textview */
                if (object.getCreationDate() != null) {
                    if (!object.getCreationDate().equals("")) {
                        long unix_seconds = Long.parseLong(object.getCreationDate());

                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                        cal.setTimeInMillis(unix_seconds * 1000L);
                        DateFormat format = new SimpleDateFormat("hh:mm a");

                        String date = format.format(cal.getTime());
                        String strDATE = date.replace("am", "AM").replace("pm", "PM");

                        ((RecieverViewHolder) holder).SingleMessageTimeTV.setText(strDATE);
                        ((RecieverViewHolder) holder).SingleImageTimeTV.setText(strDATE);
                        ((RecieverViewHolder) holder).SingleImgWithMsgMessageTimeTV.setText(strDATE);
                        ((RecieverViewHolder) holder).timeImages2and3TV.setText(strDATE);
                        ((RecieverViewHolder) holder).time2and3TV.setText(strDATE);
                        ((RecieverViewHolder) holder).timeImages4and5TV.setText(strDATE);
                        ((RecieverViewHolder) holder).time4and5WithMsgTV.setText(strDATE);
                    }
                }

                if (object.getEdited() != null && !object.getEdited().equals("") &&
                        object.getEdited().equals("1")) {
                    ((RecieverViewHolder) holder).editedImage.setVisibility(View.VISIBLE);
                } else {
                    ((RecieverViewHolder) holder).editedImage.setVisibility(View.INVISIBLE);
                }

                if (object.getReplyFor() != null) {

                    if (object.getMessage() != null) {
                        ((RecieverViewHolder) holder).replyForImagesTV.setText(html2text(object.getMessage()));
                    }

                    if (object.getCreationDate() != null) {
                        if (!object.getCreationDate().equals("")) {
                            long unix_seconds = Long.parseLong(object.getCreationDate());

                            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                            cal.setTimeInMillis(unix_seconds * 1000L);
                            DateFormat format = new SimpleDateFormat("hh:mm a");

                            String date = format.format(cal.getTime());
                            String strDATE = date.replace("am", "AM").replace("pm", "PM");

                            ((RecieverViewHolder) holder).replyForImagestimeTV.setText(strDATE);
                        }
                    }

                    if (object.getReplyFor().getMessage() != null && !object.getReplyFor().getMessage().equals("")) {
                        ((RecieverViewHolder) holder).messageReplyForSingleMsgTV.setText(html2text(object.getReplyFor().getMessage()));
                        ((RecieverViewHolder) holder).leftChatReplyMessageTV.setText(html2text(object.getReplyFor().getMessage()));
                    }

                    if (object.getReplyFor().getMessage() != null && !object.getReplyFor().getMessage().equals("")) {
                        ((RecieverViewHolder) holder).leftChatReplyMessageTV.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).leftChatReplyMessageTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    } else {
                        ((RecieverViewHolder) holder).leftChatReplyMessageTV.setText(R.string.photo);
                        ((RecieverViewHolder) holder).leftChatReplyMessageTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_camera, 0, 0, 0);
                    }

                    if (!object.getReplyFor().getUserDetail().getImage().equals("")) {
                        Picasso.get().load(object.getReplyFor().getUserDetail().getImage()).fit().centerCrop()
                                .placeholder(R.drawable.palace_holder)
                                .error(R.drawable.palace_holder)
                                .into(((RecieverViewHolder) holder).picreplyForImagesIV);
                    }

                    if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.STAFF_ID, ""))) {
                        ((RecieverViewHolder) holder).replyForImagesNameTV.setText("You");
                        ((RecieverViewHolder) holder).replyForImagesProfileRL.setVisibility(View.GONE);
                    } else if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.ADMIN_ID, ""))) {
                        ((RecieverViewHolder) holder).replyForImagesNameTV.setText("You");
                        ((RecieverViewHolder) holder).replyForImagesProfileRL.setVisibility(View.GONE);
                    } else {
                        ((RecieverViewHolder) holder).replyForImagesNameTV.setText(object.getReplyFor().getUserDetail().getFirstName());
                        ((RecieverViewHolder) holder).replyForImagesProfileRL.setVisibility(View.VISIBLE);
                    }

                    /* purane wali images */
                    if (object.getReplyFor().getImageCount() != null &&
                            !object.getReplyFor().getImageCount().equals("") &&
                            !object.getReplyFor().getImageCount().equals("0")) {

                        ((RecieverViewHolder) holder).imageleftChatReplyRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).blurPicleftChatReplyRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).replyForImagesLL.setVisibility(View.VISIBLE);

                        /* set doc 1 */
                        if (object.getReplyFor().getImage() != null && !object.getReplyFor().getImage().equals("")) {

                            if (!Utilities.isDocAdded(object.getReplyFor().getImage())) {

                                if (Utilities.isVideoAdded(object.getReplyFor().getImage())) {
                                    ((RecieverViewHolder) holder).itemleftChatReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context)
                                            .load(object.getReplyFor().getImage())
                                            .placeholder(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemleftChatReplyIV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemleftChatReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }
                        }

                        if (Integer.parseInt(object.getReplyFor().getImageCount()) >= 2) {
                            ((RecieverViewHolder) holder).blurPicleftChatReplyRL.setVisibility(View.VISIBLE);
                            int imageCount = Integer.parseInt(object.getReplyFor().getImageCount()) - 1;
                            String strImageCount = "+" + imageCount;
                            ((RecieverViewHolder) holder).textCountleftChatReplyTV.setText(strImageCount);
                        } else {
                            ((RecieverViewHolder) holder).blurPicleftChatReplyRL.setVisibility(View.GONE);
                        }

                        /* new wali images */
                        if (object.getImageCount() != null && !object.getImageCount().equals("") && !object.getImageCount().equals("0")) {

                            /* check if message is coming with Image */
                            if (object.getReplyFor().getMessage() != null && !object.getMessage().equals("")) {
                                ((RecieverViewHolder) holder).replyForImagesTV.setVisibility(View.VISIBLE);
                            } else {
                                ((RecieverViewHolder) holder).replyForImagesTV.setVisibility(View.GONE);
                            }

                            if (object.getImageCount().equals("1")) {

                                ((RecieverViewHolder) holder).replyForMsgWithSingleImgIV.setVisibility(View.VISIBLE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((RecieverViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).replyForMsgWithSingleImgIV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (object.getImageCount().equals("2")) {

                                ((RecieverViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                                /* hide blur layout */
                                ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic31IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic32IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (object.getImageCount().equals("3")) {

                                ((RecieverViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                                /* show blur layout */
                                ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic31IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic32IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (object.getImageCount().equals("4")) {

                                ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                                /* hide blur layout */
                                ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 3 */
                                if (!Utilities.isDocAdded(object.getImage3())) {
                                    if (Utilities.isVideoAdded(object.getImage3())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 4 */
                                if (!Utilities.isDocAdded(object.getImage4())) {
                                    if (Utilities.isVideoAdded(object.getImage4())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (Integer.parseInt(object.getImageCount()) >= 5) {

                                ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                                /* show blur layout */
                                ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                                /* show images count */
                                int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                                String strImageCount = "+" + imageCount;
                                ((RecieverViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 3 */
                                if (!Utilities.isDocAdded(object.getImage3())) {
                                    if (Utilities.isVideoAdded(object.getImage3())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 4 */
                                if (!Utilities.isDocAdded(object.getImage4())) {
                                    if (Utilities.isVideoAdded(object.getImage4())) {
                                        ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }
                            }
                        }
                        /* new wali images */
                    } else if (object.getImageCount() != null && !object.getImageCount().equals("") && !object.getImageCount().equals("0")) {

                        ((RecieverViewHolder) holder).replyForImagesLL.setVisibility(View.VISIBLE);

                        /* check if message is coming with Image */
                        if (object.getReplyFor().getMessage() != null && !object.getMessage().equals("")) {
                            ((RecieverViewHolder) holder).replyForImagesTV.setVisibility(View.VISIBLE);
                        } else {
                            ((RecieverViewHolder) holder).replyForImagesTV.setVisibility(View.GONE);
                        }

                        if (object.getImageCount().equals("1")) {

                            ((RecieverViewHolder) holder).replyForMsgWithSingleImgIV.setVisibility(View.VISIBLE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).replyForMsgWithSingleImgIV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("2")) {

                            ((RecieverViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                            /* hide blur layout */
                            ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic31IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic32IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("3")) {

                            ((RecieverViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                            /* show blur layout */
                            ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic31IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic32IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("4")) {

                            ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                            /* hide blur layout */
                            ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (Integer.parseInt(object.getImageCount()) >= 5) {

                            ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                            /* show blur layout */
                            ((RecieverViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                            /* show images count */
                            int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                            String strImageCount = "+" + imageCount;
                            ((RecieverViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        }

                    } else {
                        ((RecieverViewHolder) holder).ReplyForSingleMsgRL.setVisibility(View.VISIBLE);
                    }
                } else {

                    if (object.getImageCount() != null && !object.getImageCount().equals("") && !object.getImageCount().equals("0")) {
                        ((RecieverViewHolder) holder).SingleMessageLL.setVisibility(View.GONE);

                        if (object.getImageCount().equals("1")) {

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((RecieverViewHolder) holder).SingleImageRL.setVisibility(View.GONE);
                                ((RecieverViewHolder) holder).SingleImgWithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((RecieverViewHolder) holder).SingleImageRL.setVisibility(View.VISIBLE);
                                ((RecieverViewHolder) holder).SingleImgWithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).SingleImageIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).SingleImgWithMsgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).SingleImageIV);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).SingleImgWithMsgIV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).SingleImageIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).SingleImgWithMsgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("2")) {

                            /* hide blur layout */
                            ((RecieverViewHolder) holder).blur3RL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).blurWithMessage3RL.setVisibility(View.GONE);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((RecieverViewHolder) holder).Images2and3RL.setVisibility(View.GONE);
                                ((RecieverViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((RecieverViewHolder) holder).Images2and3RL.setVisibility(View.VISIBLE);
                                ((RecieverViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3WithMessageIV1);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3WithMessageIV2);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("3")) {

                            /* show blur layout */
                            ((RecieverViewHolder) holder).blur3RL.setVisibility(View.VISIBLE);
                            ((RecieverViewHolder) holder).blurWithMessage3RL.setVisibility(View.VISIBLE);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((RecieverViewHolder) holder).Images2and3RL.setVisibility(View.GONE);
                                ((RecieverViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((RecieverViewHolder) holder).Images2and3RL.setVisibility(View.VISIBLE);
                                ((RecieverViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3WithMessageIV1);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Image2and3WithMessageIV2);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("4")) {

                            /* hide blur layout */
                            ((RecieverViewHolder) holder).blur4and5RL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).blur4and5WithMsgRL.setVisibility(View.GONE);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((RecieverViewHolder) holder).Images4and5RL.setVisibility(View.GONE);
                                ((RecieverViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((RecieverViewHolder) holder).Images4and5RL.setVisibility(View.VISIBLE);
                                ((RecieverViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV1);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV2);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((RecieverViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV3);
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV3);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((RecieverViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV4);
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV4);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (Integer.parseInt(object.getImageCount()) >= 5) {

                            /* show blur layout */
                            ((RecieverViewHolder) holder).blur4and5RL.setVisibility(View.VISIBLE);
                            ((RecieverViewHolder) holder).blur4and5WithMsgRL.setVisibility(View.VISIBLE);

                            int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                            String strImageCount = "+" + imageCount;
                            ((RecieverViewHolder) holder).textCount4and5Tv.setText(strImageCount);
                            ((RecieverViewHolder) holder).textCount4and5WithMsgTv.setText(strImageCount);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((RecieverViewHolder) holder).Images4and5RL.setVisibility(View.GONE);
                                ((RecieverViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((RecieverViewHolder) holder).Images4and5RL.setVisibility(View.VISIBLE);
                                ((RecieverViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((RecieverViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV1);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((RecieverViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV2);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((RecieverViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV3);
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV3);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((RecieverViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((RecieverViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5IV4);
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((RecieverViewHolder) holder).Images4and5WithMsgIV4);
                                }
                            } else {
                                ((RecieverViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((RecieverViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }
                        }

                    } else {
                        ((RecieverViewHolder) holder).SingleMessageLL.setVisibility(View.VISIBLE);
                    }
                }

                ((RecieverViewHolder) holder).mainLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).SingleMessageLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).SingleImageRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).SingleImgWithMsgLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).Images2and3RL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).layout2and3WithMsgLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).Images4and5RL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).Images4and5WithMsgLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).ReplyForSingleMsgRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).replyForImagesLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).SingleImageRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((RecieverViewHolder) holder).Images2and3RL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((RecieverViewHolder) holder).layout2and3WithMsgLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((RecieverViewHolder) holder).Images4and5RL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((RecieverViewHolder) holder).Images4and5WithMsgLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((RecieverViewHolder) holder).replyForImagesLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                break;

            /******************
             * TYPE_ITEM CASE
             *****************/
            case Sender:

                ((SenderViewHolder) holder).SingleMessageLL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).SingleImageRL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).SingleImgWithMsgLL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).Images2and3RL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).Images4and5RL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).ReplyForSingleMsgRL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).replyForImagesLL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).replyForMsgWithSingleImgIV.setVisibility(View.GONE);
                ((SenderViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).imageleftChatReplyRL.setVisibility(View.GONE);
                ((SenderViewHolder) holder).replyForImagesProfileRL.setVisibility(View.GONE);

                /* set date at top */
                if (object.getCreationDate() != null && !object.getCreationDate().equals("")) {

                    ((ForumChatAdapter1.SenderViewHolder) holder).ChatDateTV.setText(formatToYesterdayOrToday(getDateFromUixTimeStamp(Long.parseLong(object.getCreationDate()))));

                    String thisDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position).getCreationDate()));
                    String nextDate = "";

                    if (position == 0) {
                        nextDate = thisDate;
                    } else {
                        if (modelArrayList.get(position - 1).getCreationDate() != null)
                            nextDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position - 1).getCreationDate()));
                    }

                    // enable section heading if it's the first one, or
                    // different from the previous one
                    if (nextDate.equals(thisDate)) {
                        if (strLastPage.equals("TRUE") && position == 0) {
                            ((ForumChatAdapter1.SenderViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                        } else {
                            ((ForumChatAdapter1.SenderViewHolder) holder).ChatDateTV.setVisibility(View.GONE);
                        }
                    } else {
                        ((ForumChatAdapter1.SenderViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                    }
                }

                /* set send message in message textviews */
                if (object.getMessage() != null) {
                    ((SenderViewHolder) holder).SingleMessageTV.setText(html2text(object.getMessage()));
                    ((SenderViewHolder) holder).SingleImgWithMsgMessageTV.setText(html2text(object.getMessage()));
                    ((SenderViewHolder) holder).text2and3TV.setText(html2text(object.getMessage()));
                    ((SenderViewHolder) holder).text4and5WithMsgTV.setText(html2text(object.getMessage()));
                    ((SenderViewHolder) holder).SingleImgWithMsgMessageTV.setText(html2text(object.getMessage()));
                    ((SenderViewHolder) holder).ReplyMsgForSingleMsgTV.setText(html2text(object.getMessage()));
                }

                /* convert seconds to milliseconds and set on time textview */
                if (object.getCreationDate() != null) {
                    if (!object.getCreationDate().equals("")) {
                        long unix_seconds = Long.parseLong(object.getCreationDate());

                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                        cal.setTimeInMillis(unix_seconds * 1000L);
                        DateFormat format = new SimpleDateFormat("hh:mm a");

                        String date = format.format(cal.getTime());
                        String strDATE = date.replace("am", "AM").replace("pm", "PM");

                        ((SenderViewHolder) holder).SingleMessageTimeTV.setText(strDATE);
                        ((SenderViewHolder) holder).SingleImageTimeTV.setText(strDATE);
                        ((SenderViewHolder) holder).SingleImgWithMsgMessageTimeTV.setText(strDATE);
                        ((SenderViewHolder) holder).timeImages2and3TV.setText(strDATE);
                        ((SenderViewHolder) holder).time2and3TV.setText(strDATE);
                        ((SenderViewHolder) holder).timeImages4and5TV.setText(strDATE);
                        ((SenderViewHolder) holder).time4and5WithMsgTV.setText(strDATE);
                    }
                }

                if (object.getEdited() != null && !object.getEdited().equals("") &&
                        object.getEdited().equals("1")) {
                    ((SenderViewHolder) holder).editedImage.setVisibility(View.VISIBLE);
                } else {
                    ((SenderViewHolder) holder).editedImage.setVisibility(View.INVISIBLE);
                }

                if (object.getReplyFor() != null) {

                    if (object.getMessage() != null) {
                        ((SenderViewHolder) holder).replyForImagesTV.setText(html2text(object.getMessage()));
                    }

                    if (object.getCreationDate() != null) {
                        if (!object.getCreationDate().equals("")) {
                            long unix_seconds = Long.parseLong(object.getCreationDate());

                            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                            cal.setTimeInMillis(unix_seconds * 1000L);
                            DateFormat format = new SimpleDateFormat("hh:mm a");

                            String date = format.format(cal.getTime());
                            String strDATE = date.replace("am", "AM").replace("pm", "PM");

                            ((SenderViewHolder) holder).replyForImagestimeTV.setText(strDATE);
                        }
                    }

                    if (object.getReplyFor().getMessage() != null && !object.getReplyFor().getMessage().equals("")) {
                        ((SenderViewHolder) holder).messageReplyForSingleMsgTV.setText(html2text(object.getReplyFor().getMessage()));
                        ((SenderViewHolder) holder).leftChatReplyMessageTV.setText(html2text(object.getReplyFor().getMessage()));
                    }

                    if (object.getReplyFor().getMessage() != null && !object.getReplyFor().getMessage().equals("")) {
                        ((SenderViewHolder) holder).leftChatReplyMessageTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatReplyMessageTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    } else {
                        ((SenderViewHolder) holder).leftChatReplyMessageTV.setText(R.string.photo);
                        ((SenderViewHolder) holder).leftChatReplyMessageTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_camera, 0, 0, 0);
                    }

                    if (!object.getReplyFor().getUserDetail().getImage().equals("")) {
                        Picasso.get().load(object.getReplyFor().getUserDetail().getImage()).fit().centerCrop()
                                .placeholder(R.drawable.palace_holder)
                                .error(R.drawable.palace_holder)
                                .into(((SenderViewHolder) holder).picreplyForImagesIV);
                    }

                    if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.STAFF_ID, ""))) {
                        ((SenderViewHolder) holder).replyForImagesNameTV.setText("You");
                        ((SenderViewHolder) holder).replyForImagesProfileRL.setVisibility(View.GONE);
                    } else if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.ADMIN_ID, ""))) {
                        ((SenderViewHolder) holder).replyForImagesNameTV.setText("You");
                        ((SenderViewHolder) holder).replyForImagesProfileRL.setVisibility(View.GONE);
                    } else {
                        ((SenderViewHolder) holder).replyForImagesNameTV.setText(object.getReplyFor().getUserDetail().getFirstName());
                        ((SenderViewHolder) holder).replyForImagesProfileRL.setVisibility(View.VISIBLE);
                    }

                    /* purane wali images */
                    if (object.getReplyFor().getImageCount() != null &&
                            !object.getReplyFor().getImageCount().equals("") &&
                            !object.getReplyFor().getImageCount().equals("0")) {

                        ((SenderViewHolder) holder).imageleftChatReplyRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).blurPicleftChatReplyRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).replyForImagesLL.setVisibility(View.VISIBLE);

                        /* set doc 1 */
                        if (object.getReplyFor().getImage() != null && !object.getReplyFor().getImage().equals("")) {

                            if (!Utilities.isDocAdded(object.getReplyFor().getImage())) {

                                if (Utilities.isVideoAdded(object.getReplyFor().getImage())) {
                                    ((SenderViewHolder) holder).itemleftChatReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context)
                                            .load(object.getReplyFor().getImage())
                                            .placeholder(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemleftChatReplyIV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemleftChatReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }
                        }

                        if (Integer.parseInt(object.getReplyFor().getImageCount()) >= 2) {
                            ((SenderViewHolder) holder).blurPicleftChatReplyRL.setVisibility(View.VISIBLE);
                            int imageCount = Integer.parseInt(object.getReplyFor().getImageCount()) - 1;
                            String strImageCount = "+" + imageCount;
                            ((SenderViewHolder) holder).textCountleftChatReplyTV.setText(strImageCount);
                        } else {
                            ((SenderViewHolder) holder).blurPicleftChatReplyRL.setVisibility(View.GONE);
                        }

                        /* new wali images */
                        if (object.getImageCount() != null && !object.getImageCount().equals("") && !object.getImageCount().equals("0")) {

                            /* check if message is coming with Image */
                            if (object.getReplyFor().getMessage() != null && !object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).replyForImagesTV.setVisibility(View.VISIBLE);
                            } else {
                                ((SenderViewHolder) holder).replyForImagesTV.setVisibility(View.GONE);
                            }

                            if (object.getImageCount().equals("1")) {

                                ((SenderViewHolder) holder).replyForMsgWithSingleImgIV.setVisibility(View.VISIBLE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((SenderViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).replyForMsgWithSingleImgIV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (object.getImageCount().equals("2")) {

                                ((SenderViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                                /* hide blur layout */
                                ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic31IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic32IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (object.getImageCount().equals("3")) {

                                ((SenderViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                                /* show blur layout */
                                ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic31IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic32IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (object.getImageCount().equals("4")) {

                                ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                                /* hide blur layout */
                                ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 3 */
                                if (!Utilities.isDocAdded(object.getImage3())) {
                                    if (Utilities.isVideoAdded(object.getImage3())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 4 */
                                if (!Utilities.isDocAdded(object.getImage4())) {
                                    if (Utilities.isVideoAdded(object.getImage4())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (Integer.parseInt(object.getImageCount()) >= 5) {

                                ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                                /* show blur layout */
                                ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                                /* show images count */
                                int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                                String strImageCount = "+" + imageCount;
                                ((SenderViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

                                /* set doc 1 */
                                if (!Utilities.isDocAdded(object.getImage())) {
                                    if (Utilities.isVideoAdded(object.getImage())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 2 */
                                if (!Utilities.isDocAdded(object.getImage2())) {
                                    if (Utilities.isVideoAdded(object.getImage2())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 3 */
                                if (!Utilities.isDocAdded(object.getImage3())) {
                                    if (Utilities.isVideoAdded(object.getImage3())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                                /* set doc 4 */
                                if (!Utilities.isDocAdded(object.getImage4())) {
                                    if (Utilities.isVideoAdded(object.getImage4())) {
                                        ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    } else {
                                        Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }
                            }
                        }
                        /* new wali images */
                    } else if (object.getImageCount() != null && !object.getImageCount().equals("") && !object.getImageCount().equals("0")) {

                        ((SenderViewHolder) holder).replyForImagesLL.setVisibility(View.VISIBLE);

                        /* check if message is coming with Image */
                        if (object.getReplyFor().getMessage() != null && !object.getMessage().equals("")) {
                            ((SenderViewHolder) holder).replyForImagesTV.setVisibility(View.VISIBLE);
                        } else {
                            ((SenderViewHolder) holder).replyForImagesTV.setVisibility(View.GONE);
                        }

                        if (object.getImageCount().equals("1")) {

                            ((SenderViewHolder) holder).replyForMsgWithSingleImgIV.setVisibility(View.VISIBLE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).replyForMsgWithSingleImgIV);
                                }
                            } else {
                                ((SenderViewHolder) holder).replyForMsgWithSingleImgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("2")) {

                            ((SenderViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                            /* hide blur layout */
                            ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic31IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic32IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("3")) {

                            ((SenderViewHolder) holder).imageReplyPic2and3LL.setVisibility(View.VISIBLE);

                            /* show blur layout */
                            ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic31IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic32IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("4")) {

                            ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                            /* hide blur layout */
                            ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.GONE);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (Integer.parseInt(object.getImageCount()) >= 5) {

                            ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                            /* show blur layout */
                            ((SenderViewHolder) holder).blur3ReplyRL.setVisibility(View.VISIBLE);

                            /* show images count */
                            int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                            String strImageCount = "+" + imageCount;
                            ((SenderViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        }

                    } else {
                        ((SenderViewHolder) holder).ReplyForSingleMsgRL.setVisibility(View.VISIBLE);
                    }
                } else {

                    if (object.getImageCount() != null && !object.getImageCount().equals("") && !object.getImageCount().equals("0")) {
                        ((SenderViewHolder) holder).SingleMessageLL.setVisibility(View.GONE);

                        if (object.getImageCount().equals("1")) {

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).SingleImageRL.setVisibility(View.GONE);
                                ((SenderViewHolder) holder).SingleImgWithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((SenderViewHolder) holder).SingleImageRL.setVisibility(View.VISIBLE);
                                ((SenderViewHolder) holder).SingleImgWithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).SingleImageIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).SingleImgWithMsgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).SingleImageIV);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).SingleImgWithMsgIV);
                                }
                            } else {
                                ((SenderViewHolder) holder).SingleImageIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).SingleImgWithMsgIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("2")) {

                            /* hide blur layout */
                            ((SenderViewHolder) holder).blur3RL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).blurWithMessage3RL.setVisibility(View.GONE);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).Images2and3RL.setVisibility(View.GONE);
                                ((SenderViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((SenderViewHolder) holder).Images2and3RL.setVisibility(View.VISIBLE);
                                ((SenderViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3WithMessageIV1);
                                }
                            } else {
                                ((SenderViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3WithMessageIV2);
                                }
                            } else {
                                ((SenderViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("3")) {

                            /* show blur layout */
                            ((SenderViewHolder) holder).blur3RL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).blurWithMessage3RL.setVisibility(View.VISIBLE);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).Images2and3RL.setVisibility(View.GONE);
                                ((SenderViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((SenderViewHolder) holder).Images2and3RL.setVisibility(View.VISIBLE);
                                ((SenderViewHolder) holder).layout2and3WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3WithMessageIV1);
                                }
                            } else {
                                ((SenderViewHolder) holder).Image2and3IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Image2and3WithMessageIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Image2and3WithMessageIV2);
                                }
                            } else {
                                ((SenderViewHolder) holder).Image2and3IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Image2and3WithMessageIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("4")) {

                            /* hide blur layout */
                            ((SenderViewHolder) holder).blur4and5RL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).blur4and5WithMsgRL.setVisibility(View.GONE);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).Images4and5RL.setVisibility(View.GONE);
                                ((SenderViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((SenderViewHolder) holder).Images4and5RL.setVisibility(View.VISIBLE);
                                ((SenderViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV1);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV2);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((SenderViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV3);
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV3);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((SenderViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV4);
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV4);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (Integer.parseInt(object.getImageCount()) >= 5) {

                            /* show blur layout */
                            ((SenderViewHolder) holder).blur4and5RL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).blur4and5WithMsgRL.setVisibility(View.VISIBLE);

                            int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                            String strImageCount = "+" + imageCount;
                            ((SenderViewHolder) holder).textCount4and5Tv.setText(strImageCount);
                            ((SenderViewHolder) holder).textCount4and5WithMsgTv.setText(strImageCount);

                            /* check if message is coming with Image */
                            if (object.getMessage() != null && !object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).Images4and5RL.setVisibility(View.GONE);
                                ((SenderViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.VISIBLE);
                            } else {
                                ((SenderViewHolder) holder).Images4and5RL.setVisibility(View.VISIBLE);
                                ((SenderViewHolder) holder).Images4and5WithMsgLL.setVisibility(View.GONE);
                            }

                            /* set doc 1 */
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage())) {
                                    ((SenderViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV1);
                                    Glide.with(context).load(object.getImage()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV1);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 2 */
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2())) {
                                    ((SenderViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV2);
                                    Glide.with(context).load(object.getImage2()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV2);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 3 */
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3())) {
                                    ((SenderViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV3);
                                    Glide.with(context).load(object.getImage3()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV3);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV3.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            /* set doc 4 */
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4())) {
                                    ((SenderViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    ((SenderViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                } else {
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5IV4);
                                    Glide.with(context).load(object.getImage4()).placeholder(R.drawable.palace_holder).into(((SenderViewHolder) holder).Images4and5WithMsgIV4);
                                }
                            } else {
                                ((SenderViewHolder) holder).Images4and5IV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                ((SenderViewHolder) holder).Images4and5WithMsgIV4.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }
                        }

                    } else {
                        ((SenderViewHolder) holder).SingleMessageLL.setVisibility(View.VISIBLE);
                    }
                }

                ((SenderViewHolder) holder).mainLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).SingleMessageLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).SingleImageRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).SingleImgWithMsgLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).Images2and3RL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).layout2and3WithMsgLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).Images4and5RL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).Images4and5WithMsgLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).ReplyForSingleMsgRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).replyForImagesLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).SingleImageRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((SenderViewHolder) holder).Images2and3RL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((SenderViewHolder) holder).layout2and3WithMsgLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((SenderViewHolder) holder).Images4and5RL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((SenderViewHolder) holder).Images4and5WithMsgLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                ((SenderViewHolder) holder).replyForImagesLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFullImagesIntent(object);
                    }
                });

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (modelArrayList != null) {
            AllMessagesItem object = modelArrayList.get(position);
            if (object != null) {
                return object.getIntType();
            }
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        if (modelArrayList == null)
            return 0;
        return modelArrayList.size();
    }

    public static class RecieverViewHolder extends RecyclerView.ViewHolder {
        TextView ChatDateTV, SingleMessageTV, SingleMessageTimeTV, SingleImageTimeTV, SingleImgWithMsgMessageTV, SingleImgWithMsgMessageTimeTV, textCount3TV,
                timeImages2and3TV, textWithMessageCount3TV, text2and3TV, time2and3TV, textCount4and5Tv, timeImages4and5TV, text4and5WithMsgTV, time4and5WithMsgTV,
                messageReplyForSingleMsgTV, ReplyMsgForSingleMsgTV, timeForSingleMsgTV, replyForImagesNameTV, leftChatReplyMessageTV, textCountleftChatReplyTV,
                textCount3ReplyTv, textCountReplyPicTv, replyForImagesTV, replyForImagestimeTV, textCount4and5WithMsgTv;

        ImageView editedImage, SingleImageIV, SingleImgWithMsgIV, Image2and3IV1, Image2and3IV2, Image2and3WithMessageIV1, Image2and3WithMessageIV2,
                Images4and5IV1, Images4and5IV2, Images4and5IV3, Images4and5IV4, Images4and5WithMsgIV1, Images4and5WithMsgIV2, Images4and5WithMsgIV3,
                Images4and5WithMsgIV4, picreplyForImagesIV, itemleftChatReplyIV, replyForMsgWithSingleImgIV,
                itemImageReplyPic31IV, itemImageReplyPic32IV, blur3ReplyIV, itemImageReplyPic33IV, userProfileIV,
                itemImageReplyPic41IV, itemImageReplyPic42IV, itemImageReplyPic43IV, itemImageReplyPic44IV, blurReplyPicIV;

        LinearLayout mainLL, SingleMessageLL, SingleImgWithMsgMessageLL, Images2and3LL, layout2and3WithMsgLL, Images2and3WithMsgLL, layout2and3LL,
                Images4and5LL, Images4and5WithMsgLL, layout4and5WithMsgLL, ReplyForSingleMsgLL, replyForImagesLL, ReplyreplyForImagesLL, imageReplyPic2and3LL,
                imageReplyPic4LL;

        RelativeLayout SingleImageRL, Images2and3RL, blur3RL, blurWithMessage3RL, blurWithMessage3IV, Images4and5RL, blur4and5RL, blur4and5IV, blur4and5WithMsgRL,
                blur4and5WithMsgIV, ReplyForSingleMsgRL, replyForImagesRL, replyForImagesProfileRL, imageleftChatReplyRL, blurPicleftChatReplyRL, blur3ReplyRL,
                blurReplyPicRL, blur3IV, SingleImgWithMsgLL;

        View replyViewBottom;

        public RecieverViewHolder(View view) {
            super(view);

            userProfileIV = view.findViewById(R.id.userProfileIV);

            ChatDateTV = view.findViewById(R.id.ChatDateTV);
            SingleMessageTV = view.findViewById(R.id.SingleMessageTV);
            SingleMessageTimeTV = view.findViewById(R.id.SingleMessageTimeTV);
            SingleImageTimeTV = view.findViewById(R.id.SingleImageTimeTV);
            SingleImgWithMsgMessageTV = view.findViewById(R.id.SingleImgWithMsgMessageTV);
            SingleImgWithMsgMessageTimeTV = view.findViewById(R.id.SingleImgWithMsgMessageTimeTV);
            textCount3TV = view.findViewById(R.id.textCount3TV);
            timeImages2and3TV = view.findViewById(R.id.timeImages2and3TV);
            textWithMessageCount3TV = view.findViewById(R.id.textWithMessageCount3TV);
            text2and3TV = view.findViewById(R.id.text2and3TV);
            time2and3TV = view.findViewById(R.id.time2and3TV);
            textCount4and5Tv = view.findViewById(R.id.textCount4and5Tv);
            timeImages4and5TV = view.findViewById(R.id.timeImages4and5TV);
            text4and5WithMsgTV = view.findViewById(R.id.text4and5WithMsgTV);
            time4and5WithMsgTV = view.findViewById(R.id.time4and5WithMsgTV);
            messageReplyForSingleMsgTV = view.findViewById(R.id.messageReplyForSingleMsgTV);
            ReplyMsgForSingleMsgTV = view.findViewById(R.id.ReplyMsgForSingleMsgTV);
            timeForSingleMsgTV = view.findViewById(R.id.timeForSingleMsgTV);
            replyForImagesNameTV = view.findViewById(R.id.replyForImagesNameTV);
            leftChatReplyMessageTV = view.findViewById(R.id.leftChatReplyMessageTV);
            textCountleftChatReplyTV = view.findViewById(R.id.textCountleftChatReplyTV);
            textCount3ReplyTv = view.findViewById(R.id.textCount3ReplyTv);
            textCountReplyPicTv = view.findViewById(R.id.textCountReplyPicTv);
            replyForImagesTV = view.findViewById(R.id.replyForImagesTV);
            replyForImagestimeTV = view.findViewById(R.id.replyForImagestimeTV);

            editedImage = view.findViewById(R.id.editedImage);
            SingleImageIV = view.findViewById(R.id.SingleImageIV);
            SingleImgWithMsgIV = view.findViewById(R.id.SingleImgWithMsgIV);
            Image2and3IV1 = view.findViewById(R.id.Image2and3IV1);
            Image2and3IV2 = view.findViewById(R.id.Image2and3IV2);
            blur3IV = view.findViewById(R.id.blur3IV);
            Image2and3WithMessageIV1 = view.findViewById(R.id.Image2and3WithMessageIV1);
            Image2and3WithMessageIV2 = view.findViewById(R.id.Image2and3WithMessageIV2);
            Images4and5IV1 = view.findViewById(R.id.Images4and5IV1);
            Images4and5IV2 = view.findViewById(R.id.Images4and5IV2);
            Images4and5IV3 = view.findViewById(R.id.Images4and5IV3);
            Images4and5IV4 = view.findViewById(R.id.Images4and5IV4);
            Images4and5WithMsgIV1 = view.findViewById(R.id.Images4and5WithMsgIV1);
            Images4and5WithMsgIV2 = view.findViewById(R.id.Images4and5WithMsgIV2);
            Images4and5WithMsgIV3 = view.findViewById(R.id.Images4and5WithMsgIV3);
            Images4and5WithMsgIV4 = view.findViewById(R.id.Images4and5WithMsgIV4);
            textCount4and5WithMsgTv = view.findViewById(R.id.textCount4and5WithMsgTv);
            picreplyForImagesIV = view.findViewById(R.id.picreplyForImagesIV);
            itemleftChatReplyIV = view.findViewById(R.id.itemleftChatReplyIV);
            replyForMsgWithSingleImgIV = view.findViewById(R.id.replyForMsgWithSingleImgIV);
            itemImageReplyPic31IV = view.findViewById(R.id.itemImageReplyPic31IV);
            itemImageReplyPic32IV = view.findViewById(R.id.itemImageReplyPic32IV);
            blur3ReplyIV = view.findViewById(R.id.blur3ReplyIV);
            itemImageReplyPic33IV = view.findViewById(R.id.itemImageReplyPic33IV);
            itemImageReplyPic41IV = view.findViewById(R.id.itemImageReplyPic41IV);
            itemImageReplyPic42IV = view.findViewById(R.id.itemImageReplyPic42IV);
            itemImageReplyPic43IV = view.findViewById(R.id.itemImageReplyPic43IV);
            itemImageReplyPic44IV = view.findViewById(R.id.itemImageReplyPic44IV);
            blurReplyPicIV = view.findViewById(R.id.blurReplyPicIV);

            mainLL = view.findViewById(R.id.mainLL);
            SingleMessageLL = view.findViewById(R.id.SingleMessageLL);
            SingleImgWithMsgLL = view.findViewById(R.id.SingleImgWithMsgLL);
            SingleImgWithMsgMessageLL = view.findViewById(R.id.SingleImgWithMsgMessageLL);
            Images2and3LL = view.findViewById(R.id.Images2and3LL);
            layout2and3WithMsgLL = view.findViewById(R.id.layout2and3WithMsgLL);
            Images2and3WithMsgLL = view.findViewById(R.id.Images2and3WithMsgLL);
            layout2and3LL = view.findViewById(R.id.layout2and3LL);
            Images4and5LL = view.findViewById(R.id.Images4and5LL);
            Images4and5WithMsgLL = view.findViewById(R.id.Images4and5WithMsgLL);
            layout4and5WithMsgLL = view.findViewById(R.id.layout4and5WithMsgLL);
            ReplyForSingleMsgLL = view.findViewById(R.id.ReplyForSingleMsgLL);
            replyForImagesLL = view.findViewById(R.id.replyForImagesLL);
            ReplyreplyForImagesLL = view.findViewById(R.id.ReplyreplyForImagesLL);
            imageReplyPic2and3LL = view.findViewById(R.id.imageReplyPic2and3LL);
            imageReplyPic4LL = view.findViewById(R.id.imageReplyPic4LL);

            SingleImageRL = view.findViewById(R.id.SingleImageRL);
            Images2and3RL = view.findViewById(R.id.Images2and3RL);
            blur3RL = view.findViewById(R.id.blur3RL);
            blurWithMessage3RL = view.findViewById(R.id.blurWithMessage3RL);
            blurWithMessage3IV = view.findViewById(R.id.blurWithMessage3IV);
            Images4and5RL = view.findViewById(R.id.Images4and5RL);
            blur4and5RL = view.findViewById(R.id.blur4and5RL);
            blur4and5IV = view.findViewById(R.id.blur4and5IV);
            blur4and5WithMsgRL = view.findViewById(R.id.blur4and5WithMsgRL);
            blur4and5WithMsgIV = view.findViewById(R.id.blur4and5WithMsgIV);
            ReplyForSingleMsgRL = view.findViewById(R.id.ReplyForSingleMsgRL);
            replyForImagesRL = view.findViewById(R.id.replyForImagesRL);
            replyForImagesProfileRL = view.findViewById(R.id.replyForImagesProfileRL);
            imageleftChatReplyRL = view.findViewById(R.id.imageleftChatReplyRL);
            blurPicleftChatReplyRL = view.findViewById(R.id.blurPicleftChatReplyRL);
            blur4and5RL = view.findViewById(R.id.blur4and5RL);
            blur3ReplyRL = view.findViewById(R.id.blur3ReplyRL);
            blurReplyPicRL = view.findViewById(R.id.blurReplyPicRL);

            replyViewBottom = view.findViewById(R.id.replyViewBottom);
        }
    }

    public static class SenderViewHolder extends RecyclerView.ViewHolder {
        TextView ChatDateTV, SingleMessageTV, SingleMessageTimeTV, SingleImageTimeTV, SingleImgWithMsgMessageTV, SingleImgWithMsgMessageTimeTV, textCount3TV,
                timeImages2and3TV, textWithMessageCount3TV, text2and3TV, time2and3TV, textCount4and5Tv, timeImages4and5TV, text4and5WithMsgTV, time4and5WithMsgTV,
                messageReplyForSingleMsgTV, ReplyMsgForSingleMsgTV, timeForSingleMsgTV, replyForImagesNameTV, leftChatReplyMessageTV, textCountleftChatReplyTV,
                textCount3ReplyTv, textCountReplyPicTv, replyForImagesTV, replyForImagestimeTV, textCount4and5WithMsgTv;

        ImageView editedImage, SingleImageIV, SingleImgWithMsgIV, Image2and3IV1, Image2and3IV2, Image2and3WithMessageIV1, Image2and3WithMessageIV2,
                Images4and5IV1, Images4and5IV2, Images4and5IV3, Images4and5IV4, Images4and5WithMsgIV1, Images4and5WithMsgIV2, Images4and5WithMsgIV3,
                Images4and5WithMsgIV4, picreplyForImagesIV, itemleftChatReplyIV, replyForMsgWithSingleImgIV,
                itemImageReplyPic31IV, itemImageReplyPic32IV, blur3ReplyIV, itemImageReplyPic33IV, itemImageReplyPic41IV, itemImageReplyPic42IV,
                itemImageReplyPic43IV, itemImageReplyPic44IV, blurReplyPicIV;

        LinearLayout mainLL, SingleMessageLL, SingleImgWithMsgMessageLL, Images2and3LL, layout2and3WithMsgLL, Images2and3WithMsgLL, layout2and3LL,
                Images4and5LL, Images4and5WithMsgLL, layout4and5WithMsgLL, ReplyForSingleMsgLL, replyForImagesLL, ReplyreplyForImagesLL, imageReplyPic2and3LL,
                imageReplyPic4LL;

        RelativeLayout SingleImageRL, Images2and3RL, blur3RL, blurWithMessage3RL, blurWithMessage3IV, Images4and5RL, blur4and5RL, blur4and5IV, blur4and5WithMsgRL,
                blur4and5WithMsgIV, ReplyForSingleMsgRL, replyForImagesRL, replyForImagesProfileRL, imageleftChatReplyRL, blurPicleftChatReplyRL, blur3ReplyRL,
                blurReplyPicRL, blur3IV, SingleImgWithMsgLL;

        View replyViewBottom;

        public SenderViewHolder(View view) {
            super(view);

            ChatDateTV = view.findViewById(R.id.ChatDateTV);
            SingleMessageTV = view.findViewById(R.id.SingleMessageTV);
            SingleMessageTimeTV = view.findViewById(R.id.SingleMessageTimeTV);
            SingleImageTimeTV = view.findViewById(R.id.SingleImageTimeTV);
            SingleImgWithMsgMessageTV = view.findViewById(R.id.SingleImgWithMsgMessageTV);
            SingleImgWithMsgMessageTimeTV = view.findViewById(R.id.SingleImgWithMsgMessageTimeTV);
            textCount3TV = view.findViewById(R.id.textCount3TV);
            timeImages2and3TV = view.findViewById(R.id.timeImages2and3TV);
            textWithMessageCount3TV = view.findViewById(R.id.textWithMessageCount3TV);
            text2and3TV = view.findViewById(R.id.text2and3TV);
            time2and3TV = view.findViewById(R.id.time2and3TV);
            textCount4and5Tv = view.findViewById(R.id.textCount4and5Tv);
            timeImages4and5TV = view.findViewById(R.id.timeImages4and5TV);
            text4and5WithMsgTV = view.findViewById(R.id.text4and5WithMsgTV);
            time4and5WithMsgTV = view.findViewById(R.id.time4and5WithMsgTV);
            messageReplyForSingleMsgTV = view.findViewById(R.id.messageReplyForSingleMsgTV);
            ReplyMsgForSingleMsgTV = view.findViewById(R.id.ReplyMsgForSingleMsgTV);
            timeForSingleMsgTV = view.findViewById(R.id.timeForSingleMsgTV);
            replyForImagesNameTV = view.findViewById(R.id.replyForImagesNameTV);
            leftChatReplyMessageTV = view.findViewById(R.id.leftChatReplyMessageTV);
            textCountleftChatReplyTV = view.findViewById(R.id.textCountleftChatReplyTV);
            textCount3ReplyTv = view.findViewById(R.id.textCount3ReplyTv);
            textCountReplyPicTv = view.findViewById(R.id.textCountReplyPicTv);
            replyForImagesTV = view.findViewById(R.id.replyForImagesTV);
            replyForImagestimeTV = view.findViewById(R.id.replyForImagestimeTV);

            editedImage = view.findViewById(R.id.editedImage);
            SingleImageIV = view.findViewById(R.id.SingleImageIV);
            SingleImgWithMsgIV = view.findViewById(R.id.SingleImgWithMsgIV);
            Image2and3IV1 = view.findViewById(R.id.Image2and3IV1);
            Image2and3IV2 = view.findViewById(R.id.Image2and3IV2);
            blur3IV = view.findViewById(R.id.blur3IV);
            Image2and3WithMessageIV1 = view.findViewById(R.id.Image2and3WithMessageIV1);
            Image2and3WithMessageIV2 = view.findViewById(R.id.Image2and3WithMessageIV2);
            Images4and5IV1 = view.findViewById(R.id.Images4and5IV1);
            Images4and5IV2 = view.findViewById(R.id.Images4and5IV2);
            Images4and5IV3 = view.findViewById(R.id.Images4and5IV3);
            Images4and5IV4 = view.findViewById(R.id.Images4and5IV4);
            Images4and5WithMsgIV1 = view.findViewById(R.id.Images4and5WithMsgIV1);
            Images4and5WithMsgIV2 = view.findViewById(R.id.Images4and5WithMsgIV2);
            Images4and5WithMsgIV3 = view.findViewById(R.id.Images4and5WithMsgIV3);
            Images4and5WithMsgIV4 = view.findViewById(R.id.Images4and5WithMsgIV4);
            textCount4and5WithMsgTv = view.findViewById(R.id.textCount4and5WithMsgTv);
            picreplyForImagesIV = view.findViewById(R.id.picreplyForImagesIV);
            itemleftChatReplyIV = view.findViewById(R.id.itemleftChatReplyIV);
            replyForMsgWithSingleImgIV = view.findViewById(R.id.replyForMsgWithSingleImgIV);
            itemImageReplyPic31IV = view.findViewById(R.id.itemImageReplyPic31IV);
            itemImageReplyPic32IV = view.findViewById(R.id.itemImageReplyPic32IV);
            blur3ReplyIV = view.findViewById(R.id.blur3ReplyIV);
            itemImageReplyPic33IV = view.findViewById(R.id.itemImageReplyPic33IV);
            itemImageReplyPic41IV = view.findViewById(R.id.itemImageReplyPic41IV);
            itemImageReplyPic42IV = view.findViewById(R.id.itemImageReplyPic42IV);
            itemImageReplyPic43IV = view.findViewById(R.id.itemImageReplyPic43IV);
            itemImageReplyPic44IV = view.findViewById(R.id.itemImageReplyPic44IV);
            blurReplyPicIV = view.findViewById(R.id.blurReplyPicIV);

            mainLL = view.findViewById(R.id.mainLL);
            SingleMessageLL = view.findViewById(R.id.SingleMessageLL);
            SingleImgWithMsgLL = view.findViewById(R.id.SingleImgWithMsgLL);
            SingleImgWithMsgMessageLL = view.findViewById(R.id.SingleImgWithMsgMessageLL);
            Images2and3LL = view.findViewById(R.id.Images2and3LL);
            layout2and3WithMsgLL = view.findViewById(R.id.layout2and3WithMsgLL);
            Images2and3WithMsgLL = view.findViewById(R.id.Images2and3WithMsgLL);
            layout2and3LL = view.findViewById(R.id.layout2and3LL);
            Images4and5LL = view.findViewById(R.id.Images4and5LL);
            Images4and5WithMsgLL = view.findViewById(R.id.Images4and5WithMsgLL);
            layout4and5WithMsgLL = view.findViewById(R.id.layout4and5WithMsgLL);
            ReplyForSingleMsgLL = view.findViewById(R.id.ReplyForSingleMsgLL);
            replyForImagesLL = view.findViewById(R.id.replyForImagesLL);
            ReplyreplyForImagesLL = view.findViewById(R.id.ReplyreplyForImagesLL);
            imageReplyPic2and3LL = view.findViewById(R.id.imageReplyPic2and3LL);
            imageReplyPic4LL = view.findViewById(R.id.imageReplyPic4LL);

            SingleImageRL = view.findViewById(R.id.SingleImageRL);
            Images2and3RL = view.findViewById(R.id.Images2and3RL);
            blur3RL = view.findViewById(R.id.blur3RL);
            blurWithMessage3RL = view.findViewById(R.id.blurWithMessage3RL);
            blurWithMessage3IV = view.findViewById(R.id.blurWithMessage3IV);
            Images4and5RL = view.findViewById(R.id.Images4and5RL);
            blur4and5RL = view.findViewById(R.id.blur4and5RL);
            blur4and5IV = view.findViewById(R.id.blur4and5IV);
            blur4and5WithMsgRL = view.findViewById(R.id.blur4and5WithMsgRL);
            blur4and5WithMsgIV = view.findViewById(R.id.blur4and5WithMsgIV);
            ReplyForSingleMsgRL = view.findViewById(R.id.ReplyForSingleMsgRL);
            replyForImagesRL = view.findViewById(R.id.replyForImagesRL);
            replyForImagesProfileRL = view.findViewById(R.id.replyForImagesProfileRL);
            imageleftChatReplyRL = view.findViewById(R.id.imageleftChatReplyRL);
            blurPicleftChatReplyRL = view.findViewById(R.id.blurPicleftChatReplyRL);
            blur4and5RL = view.findViewById(R.id.blur4and5RL);
            blur3ReplyRL = view.findViewById(R.id.blur3ReplyRL);
            blurReplyPicRL = view.findViewById(R.id.blurReplyPicRL);

            replyViewBottom = view.findViewById(R.id.replyViewBottom);
        }

    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html.replace("\n", "<br />")));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text.length() > 0)
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void openFullImagesIntent(AllMessagesItem object) {
        mImageArryList.clear();
        if (object.getImageCount().equals("1")) {
            mImageArryList.add(object.getImage());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("2")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("3")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("4")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            mImageArryList.add(object.getImage4());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("5")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            mImageArryList.add(object.getImage4());
            mImageArryList.add(object.getImage5());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("6")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            mImageArryList.add(object.getImage4());
            mImageArryList.add(object.getImage5());
            mImageArryList.add(object.getImage6());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("7")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            mImageArryList.add(object.getImage4());
            mImageArryList.add(object.getImage5());
            mImageArryList.add(object.getImage6());
            mImageArryList.add(object.getImage7());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("8")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            mImageArryList.add(object.getImage4());
            mImageArryList.add(object.getImage5());
            mImageArryList.add(object.getImage6());
            mImageArryList.add(object.getImage7());
            mImageArryList.add(object.getImage8());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("9")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            mImageArryList.add(object.getImage4());
            mImageArryList.add(object.getImage5());
            mImageArryList.add(object.getImage6());
            mImageArryList.add(object.getImage7());
            mImageArryList.add(object.getImage8());
            mImageArryList.add(object.getImage9());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        } else if (object.getImageCount().equals("10")) {
            mImageArryList.add(object.getImage());
            mImageArryList.add(object.getImage2());
            mImageArryList.add(object.getImage3());
            mImageArryList.add(object.getImage4());
            mImageArryList.add(object.getImage5());
            mImageArryList.add(object.getImage6());
            mImageArryList.add(object.getImage7());
            mImageArryList.add(object.getImage8());
            mImageArryList.add(object.getImage9());
            mImageArryList.add(object.getImage10());
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putStringArrayListExtra("LIST", mImageArryList);
            context.startActivity(intent);
        }
    }
}