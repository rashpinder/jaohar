package jaohar.com.jaohar.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.WebViewActivity;

public class ImagesOnDetailsAdapter extends RecyclerView.Adapter<ImagesOnDetailsAdapter.ViewHolder> {
    Context context;
    ArrayList<String> mArrayList;
    String id;

    public ImagesOnDetailsAdapter(Context mActivity, ArrayList<String> mArrayList, String id) {
        context = mActivity;
        this.mArrayList = mArrayList;
        this.id = id;
    }


    @NonNull
    @Override
    public ImagesOnDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_create_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesOnDetailsAdapter.ViewHolder holder, int position) {
        String extension = mArrayList.get(position).substring(mArrayList.get(position).lastIndexOf(".") + 1);
        if (extension.contains("pdf")) {
            holder.imgPostIV.setImageDrawable(context.getResources().getDrawable(R.drawable.image_pdf));
        } else {
            Glide.with(context).load(mArrayList.get(position)).placeholder(R.drawable.placeholder).into(holder.imgPostIV);
        }


        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, WebViewActivity.class);
            intent.putExtra("url", mArrayList.get(position));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPostIV, imgCrossIV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPostIV = itemView.findViewById(R.id.imgPostIV);
            imgCrossIV = itemView.findViewById(R.id.imgCrossIV);
            imgCrossIV.setVisibility(View.GONE);
        }
    }
}
