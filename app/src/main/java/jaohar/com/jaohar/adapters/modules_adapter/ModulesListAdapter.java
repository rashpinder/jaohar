package jaohar.com.jaohar.adapters.modules_adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;

import jaohar.com.jaohar.beans.staff_module.Staff_Tab_Model;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.modules.StaffModuleBottomInterface;
import jaohar.com.jaohar.models.GetAllAdminModuleModel;


public class ModulesListAdapter extends RecyclerView.Adapter<ModulesListAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<GetAllAdminModuleModel.Data.AllModule> modelArrayList;
    StaffModuleBottomInterface mModuleInterFace;
    PaginationListForumAdapter mPaginationInterFace;

    public ModulesListAdapter(Activity mActivity, ArrayList<GetAllAdminModuleModel.Data.AllModule> modelArrayList, StaffModuleBottomInterface mModuleInterFace, PaginationListForumAdapter mPaginationInterFace) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mModuleInterFace = mModuleInterFace;
        this.mPaginationInterFace = mPaginationInterFace;
    }

    @Override
    public ModulesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_module, parent, false);
        return new ModulesListAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ModulesListAdapter.ViewHolder holder, final int position) {
        final GetAllAdminModuleModel.Data.AllModule tempValue = modelArrayList.get(position);

//      pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPaginationInterFace.mPaginationforVessels(true);
        }

        holder.messageTV.setText(tempValue.getModuleName());

        if (!tempValue.getAppImage().equals("")) {
            Glide.with(mActivity)
                    .load(tempValue.getAppImage())
                    .into(holder.picIMG);
        }else {
            holder.picIMG.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.palace_holder));
        }

        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mActivity, "In Progress.. ", Toast.LENGTH_SHORT).show();
            }
        });

        holder.menuRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mModuleInterFace.mStaffModules(tempValue);
            }
        });


    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView messageTV, timeTV;
        public ImageView picIMG;
        public LinearLayout itemLL;
        public RelativeLayout menuRL;


        ViewHolder(View itemView) {
            super(itemView);
            messageTV = (TextView) itemView.findViewById(R.id.messageTV);
            timeTV = (TextView) itemView.findViewById(R.id.timeTV);
            picIMG = (ImageView) itemView.findViewById(R.id.picIMG);
            itemLL = (LinearLayout) itemView.findViewById(R.id.itemLL);
            menuRL = (RelativeLayout) itemView.findViewById(R.id.menuRL);
        }
    }
}