package jaohar.com.jaohar.adapters.invoices_module;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.pendingPayments.AllInvoice;

public class PendingPaymentsAdapter extends RecyclerView.Adapter<PendingPaymentsAdapter.ViewHolder> {
    Context activity;
    ArrayList<AllInvoice> modelArrayList;

    OnClickInterface mOnClickInterface;
    PDFdownloadInterface mPdfDownloader;
    SinglemailInvoiceInterface msinglemailInvoiceInterface;
    SendMultipleInvoiceMAIlInterface mMultimaIlInterface;

    public PendingPaymentsAdapter(FragmentActivity activity, ArrayList<AllInvoice> modelArrayList,
                                  PDFdownloadInterface mPdfDownloader, SinglemailInvoiceInterface msinglemailInvoiceInterface,
                                  SendMultipleInvoiceMAIlInterface mMultimaIlInterface, OnClickInterface mOnClickInterface) {
        this.activity = activity;
        this.modelArrayList = modelArrayList;
        this.mOnClickInterface = mOnClickInterface;
        this.mPdfDownloader = mPdfDownloader;
        this.msinglemailInvoiceInterface = msinglemailInvoiceInterface;
        this.mMultimaIlInterface = mMultimaIlInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_payments_row, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        AllInvoice allInvoice = modelArrayList.get(position);

        holder.companyNameTV.setText(allInvoice.getPpCompanyDetail().getCompanyName());

       /* StringBuilder stringBuilder = null;
        if (allInvoice.getPpVessel1Detail().getVesselName() != null) {
            stringBuilder.append(allInvoice.getPpVessel1Detail().getVesselName());
        }

        if (allInvoice.getPpVessel2Detail().getVesselName() != null) {
            stringBuilder.append(allInvoice.getPpVessel2Detail().getVesselName());
        }

        if (allInvoice.getPpVessel3Detail().getVesselName() != null) {
            stringBuilder.append(allInvoice.getPpVessel3Detail().getVesselName());
        }
        if (stringBuilder != null) {
            holder.vesselNameTextView.setText(stringBuilder.toString());
        }*/

        holder.vesselNameTextView.setText(allInvoice.getPpVessel1Detail().getVesselName() + "," + allInvoice.getPpVessel2Detail().getVesselName() + ","
                + allInvoice.getPpVessel3Detail().getVesselName());


        holder.numberValueTextView.setText(allInvoice.getAllData().getPpNo());
        holder.amountTextView.setText(allInvoice.getAllData().getPpAmtDue() + ".00 " + allInvoice.getAllData().getPpCurrency());
        holder.dateTextView.setText(allInvoice.getAllData().getPpDate());


        if (allInvoice.getAllData().getPpStatus().equalsIgnoreCase("Cancelled")) {
            holder.statusImageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.cancelled));
        } else if (allInvoice.getAllData().getPpStatus().equalsIgnoreCase("Pending")) {
            holder.statusImageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.pending));
        } else if (allInvoice.getAllData().getPpStatus().equalsIgnoreCase("Pending Approval")) {
            holder.statusImageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.pendingapproval));
        }

        holder.optionsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickInterface.mOnClickInterface(position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView companyNameTV;
        private final TextView vesselNameTextView;
        private final TextView numberValueTextView;
        private final TextView amountTextView;
        private final TextView dateTextView;
        private final ImageView optionsImageView;
        private final ImageView statusImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            companyNameTV = itemView.findViewById(R.id.companyNameTV);
            vesselNameTextView = itemView.findViewById(R.id.vesselNameTextView);
            numberValueTextView = itemView.findViewById(R.id.numberValueTextView);
            amountTextView = itemView.findViewById(R.id.amountTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            optionsImageView = itemView.findViewById(R.id.optionsImageView);
            statusImageView = itemView.findViewById(R.id.statusImageView);
        }
    }
}
