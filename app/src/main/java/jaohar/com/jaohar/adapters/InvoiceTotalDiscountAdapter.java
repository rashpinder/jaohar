package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.interfaces.EditTotalDiscountItemdata;
import jaohar.com.jaohar.utils.Utilities;

/**
 * Created by dharmaniz on 25/5/18.
 */

public class InvoiceTotalDiscountAdapter  extends RecyclerView.Adapter<InvoiceTotalDiscountAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<AddTotalInvoiceDiscountModel> modelArrayList;
    EditTotalDiscountItemdata mEditInvoiceItemData;

    public InvoiceTotalDiscountAdapter(Activity mActivity, ArrayList<AddTotalInvoiceDiscountModel> modelArrayList,  EditTotalDiscountItemdata mEditInvoiceItemData) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;

        this.mEditInvoiceItemData = mEditInvoiceItemData;
    }

    @Override
    public InvoiceTotalDiscountAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_item, parent, false);
        return new InvoiceTotalDiscountAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(InvoiceTotalDiscountAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final AddTotalInvoiceDiscountModel tempValue = modelArrayList.get(position);
        if(tempValue.getDiscountType().equals("Add Percentage")){
//            holder.txtItemsTV.setText(tempValue.getADD_Description() + ", " + Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
            holder.txtItemsTV.setText(Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
        }else if(tempValue.getDiscountType().equals("Subtract Percentage")){
//            holder.txtItemsTV.setText(tempValue.getSubtract_Description() + ", " + Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
            holder.txtItemsTV.setText(Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
        }else if(tempValue.getDiscountType().equals("Subtract Value")){
//            holder.txtItemsTV.setText(tempValue.getSubtract_Description() + ", " + Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
            holder.txtItemsTV.setText(Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
        }else if(tempValue.getDiscountType().equals("Add Value")){
//            holder.txtItemsTV.setText(tempValue.getADD_Description() + ", " + Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
            holder.txtItemsTV.setText(Utilities.convertEvalueToNormal(tempValue.getAddAndSubtractTotal()));
        }

        holder.txtItemsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent mIntent = new Intent(mActivity, EditInvoiceItemActivity.class);
//                mIntent.putExtra("Model", tempValue);
//                mIntent.putExtra("Position", position);
//                mActivity.startActivity(mIntent);
                mEditInvoiceItemData.editTotalDiscountItemdata(tempValue,position,tempValue.getDiscountType());
            }
        });
        holder.imgRemoveIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelArrayList.remove(position);
                notifyDataSetChanged();
//                mDeleteItemInvoiceInterface.deleteItemInvoice(tempValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemsTV;
        public ImageView imgRemoveIV;

        ViewHolder(View itemView) {
            super(itemView);
            imgRemoveIV = (ImageView) itemView.findViewById(R.id.imgRemoveIV);
            txtItemsTV = (TextView) itemView.findViewById(R.id.txtItemsTV);
        }
    }
}
