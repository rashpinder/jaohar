package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
    Context context;
    ArrayList<AddGalleryImagesModel> mArrayList;
    String id;

    public ImagesAdapter(Activity mActivity, ArrayList<AddGalleryImagesModel> mArrayList, String id) {
        context = mActivity;
        this.mArrayList = mArrayList;
        this.id = id;
    }


    @NonNull
    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_create_post, parent, false));
    }

    @SuppressLint({"UseCompatLoadingForDrawables", "NotifyDataSetChanged"})
    @Override
    public void onBindViewHolder(@NonNull ImagesAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        String extension = mArrayList.get(position).substring(mArrayList.get(position).lastIndexOf(".") + 1);
        if (mArrayList.get(position).getType().equals("pdf")) {
            holder.imgPostIV.setImageDrawable(context.getResources().getDrawable(R.drawable.image_pdf));
        } else {
            Glide.with(context).load(mArrayList.get(position).getmBitmap()).placeholder(R.drawable.placeholder).into(holder.imgPostIV);
        }


        holder.imgCrossIV.setOnClickListener(v -> {
           /* if (!id.equals("")) {
                for (int i = 0; i < AddPendingPaymentsActivity.imagesArrayList.size(); i++) {
                    AddPendingPaymentsActivity.imagesArrayList.remove(i);
                    break;
                }
            }*/
            mArrayList.remove(position);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPostIV, imgCrossIV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPostIV = itemView.findViewById(R.id.imgPostIV);
            imgCrossIV = itemView.findViewById(R.id.imgCrossIV);
        }
    }
}
