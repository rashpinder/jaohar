package jaohar.com.jaohar.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.DetailForAddressBookActivity;
import jaohar.com.jaohar.beans.AddressBookModel;
import jaohar.com.jaohar.interfaces.DeleteAddressBook;
import jaohar.com.jaohar.interfaces.EditAddressBookInterface;
import jaohar.com.jaohar.models.AddressBookListModel;
import jaohar.com.jaohar.models.AllContact;

/**
 * Created by dharmaniz on 18/2/19.
 */

public class AddressBookAdapter extends RecyclerView.Adapter<AddressBookAdapter.ViewHolder> {
    Context mcontext;
    ArrayList<AllContact> mArrayList;
    DeleteAddressBook mDeteleAddressBook;
    EditAddressBookInterface mEditInterface;
    PaginationListForumAdapter mPaginationInterFace;
    public AddressBookAdapter(Context mcontext,ArrayList<AllContact> mArrayList,DeleteAddressBook mDeteleAddressBook,EditAddressBookInterface mEditInterface,PaginationListForumAdapter mPaginationInterFace){
        this.mcontext=mcontext;
        this.mArrayList=mArrayList;
        this.mDeteleAddressBook=mDeteleAddressBook;
        this.mEditInterface=mEditInterface;
        this.mPaginationInterFace=mPaginationInterFace;
    }

    @Override
    public AddressBookAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_mailing_list, parent, false);
        AddressBookAdapter.ViewHolder vH = new AddressBookAdapter.ViewHolder(v);
        return vH;
    }

    @Override
    public void onBindViewHolder(AddressBookAdapter.ViewHolder holder, int position) {
        final AllContact mModel =mArrayList.get(position);
        holder.nameLL.setVisibility(View.VISIBLE);
        // pagination for smooth scrooling
        if (position >= mArrayList.size()-1 ){
            mPaginationInterFace.mPaginationforVessels(true);
        }
        if(!mModel.getName().equals("")){

            holder.listNameTV.setText(mModel.getName());
        }


            holder.emailLL.setVisibility(View.GONE);


            holder.email2LL.setVisibility(View.GONE);


            holder.email3LL.setVisibility(View.GONE);



            holder.phone1LL.setVisibility(View.GONE);


            holder.phone2LL.setVisibility(View.GONE);


            holder.phone3LL.setVisibility(View.GONE);
        holder.companyLL.setVisibility(View.VISIBLE);

        if(!mModel.getCompanyName().equals("")){

            holder.companyTV.setText(mModel.getCompanyName());
        }


            holder.modifyLL.setVisibility(View.GONE);

            holder.modifybyLL.setVisibility(View.GONE);


        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditInterface.mEditAddressBookInterface(mModel.getId());
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeteleAddressBook.mDeleteAddressBook(mModel.getId());
            }
        });
        holder.startMAINLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mcontext, DetailForAddressBookActivity.class);
                mIntent.putExtra("contactID",mModel.getId());
                mcontext.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList == null ? 0 : mArrayList.size();

    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout nameLL,emailLL,email2LL,email3LL,phone1LL,phone2LL,phone3LL,companyLL,modifyLL,modifybyLL,startMAINLL;
        public TextView listNameTV,email1TV,email2TV,email3TV,phone1TV,phone2TV,phone3TV,companyTV,modifyTV,txtEdit,txtDelete,modifyDateTV;

        public ViewHolder(View itemView) {
            super(itemView);

            startMAINLL = (LinearLayout) itemView.findViewById(R.id.startMAINLL);
            modifybyLL = (LinearLayout) itemView.findViewById(R.id.modifybyLL);
            companyLL = (LinearLayout) itemView.findViewById(R.id.companyLL);
            nameLL = (LinearLayout) itemView.findViewById(R.id.nameLL);
            modifyLL = (LinearLayout) itemView.findViewById(R.id.modifyLL);
            phone1LL = (LinearLayout) itemView.findViewById(R.id.phone1LL);
            phone2LL = (LinearLayout) itemView.findViewById(R.id.phone2LL);
            phone3LL = (LinearLayout) itemView.findViewById(R.id.phone3LL);
            emailLL = (LinearLayout) itemView.findViewById(R.id.emailLL);
            email2LL = (LinearLayout) itemView.findViewById(R.id.email2LL);
            email3LL = (LinearLayout) itemView.findViewById(R.id.email3LL);
            listNameTV = (TextView) itemView.findViewById(R.id.listNameTV);
            email1TV = (TextView) itemView.findViewById(R.id.email1TV);
            email2TV = (TextView) itemView.findViewById(R.id.email2TV);
            email3TV = (TextView) itemView.findViewById(R.id.email3TV);
            phone1TV = (TextView) itemView.findViewById(R.id.phone1TV);
            phone2TV = (TextView) itemView.findViewById(R.id.phone2TV);
            phone3TV = (TextView) itemView.findViewById(R.id.phone3TV);
            companyTV = (TextView) itemView.findViewById(R.id.companyTV);
            modifyTV = (TextView) itemView.findViewById(R.id.modifyTV);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
            modifyDateTV = (TextView) itemView.findViewById(R.id.modifyDateTV);
        }
    }
}
