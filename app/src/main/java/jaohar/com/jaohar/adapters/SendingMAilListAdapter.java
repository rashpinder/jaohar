package jaohar.com.jaohar.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.SendMailListModel;
import jaohar.com.jaohar.models.MailModel;

/**
 * Created by dharmaniz on 29/1/19.
 */

public class SendingMAilListAdapter extends RecyclerView.Adapter<SendingMAilListAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<MailModel> modelArrayList;

    public SendingMAilListAdapter(Activity mActivity, ArrayList<MailModel> modelArrayList) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;

    }

    @Override
    public SendingMAilListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mail_list, parent, false);
        return new SendingMAilListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SendingMAilListAdapter.ViewHolder holder, final int position) {
        final MailModel tempValue = modelArrayList.get(position);
        if(tempValue.getSuccess().equals("true")){
            holder.listNameTV.setText("Sent");
            holder.listNameTV.setBackgroundColor(mActivity.getResources().getColor(R.color.green));
        }else {
            holder.listNameTV.setText("Resend");
            holder.listNameTV.setBackgroundColor(mActivity.getResources().getColor(R.color.red));
            holder.mainLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            holder.listNameTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
            holder.serialNOTV.setText(tempValue.getEmail());
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serialNOTV, listNameTV;
        public LinearLayout mainLL;
        ViewHolder(View itemView) {
            super(itemView);
            serialNOTV = (TextView) itemView.findViewById(R.id.serialNOTV);
            listNameTV = (TextView) itemView.findViewById(R.id.listNameTV);
            mainLL = (LinearLayout) itemView.findViewById(R.id.mainLL);
        }
    }
}