package jaohar.com.jaohar.adapters.forum_module;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import jaohar.com.jaohar.R;

public class ForumTypeAdapter extends RecyclerView.Adapter<ForumTypeAdapter.MyViewHolder> {
    private Context context;
    private List<String> list;
    private OnClick onClick;

    public interface OnClick {
        void Click(int position);
    }

    public ForumTypeAdapter(Context context, List<String> list, OnClick onClick) {
        this.context = context;
        this.list = list;
        this.onClick = onClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.type_list, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.typeTextView.setText(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView typeTextView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            typeTextView = itemView.findViewById(R.id.typeTextView);
        }
    }
}
