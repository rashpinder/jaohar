package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.interfaces.DeleteDocumentVessels;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class EditDocumentsAdapter extends RecyclerView.Adapter<EditDocumentsAdapter.ViewHolder> {
    private final Activity mActivity;
    private final ArrayList<DocumentModel> modelArrayList;
    private final DeleteDocumentVessels mdeleteModel;

    public EditDocumentsAdapter(Activity mActivity, ArrayList<DocumentModel> modelArrayList, DeleteDocumentVessels mdeleteModel) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mdeleteModel = mdeleteModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        DocumentModel tempValue = modelArrayList.get(position);
        holder.txtItemsTV.setText(tempValue.getDocumentName());
        holder.imgRemoveIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                modelArrayList.remove(position);
                mdeleteModel.deleteDocumentVessels(tempValue, position);
//                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemsTV;
        public ImageView imgRemoveIV;

        ViewHolder(View itemView) {
            super(itemView);
            imgRemoveIV = itemView.findViewById(R.id.imgRemoveIV);
            txtItemsTV = itemView.findViewById(R.id.txtItemsTV);
        }
    }
}