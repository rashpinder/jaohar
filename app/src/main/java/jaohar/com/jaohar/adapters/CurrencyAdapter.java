package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.fonts.EditTextPoppinsMedium;
import jaohar.com.jaohar.fonts.TextViewMedium;
import jaohar.com.jaohar.models.CompanyClassData;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {

    Context context;
    ArrayList<CurrenciesModel> mArrayListCompanys;
    String company_id;
    int selected = -1;
    Dialog categoryDialog;
    EditTextPoppinsMedium companyNameET;

    public CurrencyAdapter(Context activity, ArrayList<CurrenciesModel> mArrayListCompanys, Dialog categoryDialog, EditTextPoppinsMedium companyNameET) {
        context = activity;
        this.mArrayListCompanys = mArrayListCompanys;
        this.categoryDialog = categoryDialog;
        this.companyNameET = companyNameET;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.itemNameTextView.setText(mArrayListCompanys.get(position).getCurrency_name());

        holder.itemView.setOnClickListener(v -> {
            companyNameET.setText(mArrayListCompanys.get(position).getCurrency_name());
            categoryDialog.dismiss();
        });

    }

    @Override
    public int getItemCount() {
        return mArrayListCompanys.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextViewMedium itemNameTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemNameTextView = itemView.findViewById(R.id.itemNameTextView);
        }
    }
}
