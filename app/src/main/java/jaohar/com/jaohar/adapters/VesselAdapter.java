package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.invoices_module.AddPendingPaymentsActivity;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.fonts.EditTextPoppinsMedium;
import jaohar.com.jaohar.fonts.TextViewMedium;

public class VesselAdapter extends RecyclerView.Adapter<VesselAdapter.ViewHolder> {

    Context context;
    ArrayList<VesselSearchInvoiceModel> vesselArrayList;
    String type;
    int selected = -1;
    Dialog categoryDialog;
    EditTextPoppinsMedium companyNameET;

    public VesselAdapter(Context activity, ArrayList<VesselSearchInvoiceModel> vesselArrayList, String type, Dialog categoryDialog, EditTextPoppinsMedium companyNameET) {
        context = activity;
        this.vesselArrayList = vesselArrayList;
        this.type = type;
        this.categoryDialog = categoryDialog;
        this.companyNameET = companyNameET;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.itemNameTextView.setText(vesselArrayList.get(position).getVessel_name());

        holder.itemView.setOnClickListener(v -> {

            if (type.equals("1")) {
                AddPendingPaymentsActivity.vessel1_id = vesselArrayList.get(position).getVessel_id();
            } else if (type.equals("2")) {
                AddPendingPaymentsActivity.vessel2_id = vesselArrayList.get(position).getVessel_id();
            } else {
                AddPendingPaymentsActivity.vessel3_id = vesselArrayList.get(position).getVessel_id();
            }
            companyNameET.setText(vesselArrayList.get(position).getVessel_name());
            categoryDialog.dismiss();
        });

    }

    @Override
    public int getItemCount() {
        return vesselArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextViewMedium itemNameTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemNameTextView = itemView.findViewById(R.id.itemNameTextView);
        }
    }
}
