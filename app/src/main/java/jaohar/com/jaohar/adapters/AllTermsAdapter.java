package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.models.DataItem;
import jaohar.com.jaohar.models.GetUniversalTermsModel;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class AllTermsAdapter extends RecyclerView.Adapter<AllTermsAdapter.ViewHolder> {

    private Activity mActivity;
//    private ArrayList<GetUniversalTermsModel> modelArrayList;
    private GetUniversalTermsModel modelArrayList;
    private ArrayList<DataItem> mDataArrayList;

    public AllTermsAdapter(Activity mActivity, GetUniversalTermsModel modelArrayList, ArrayList<DataItem> mDataList) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDataArrayList = mDataList;
    }

    @Override
    public AllTermsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_companies, null);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(AllTermsAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final GetUniversalTermsModel tempValue = modelArrayList;
        final DataItem dataValue = mDataArrayList.get(position);

//        holder.item_Title.setText(tempValue.getData().get(position).getTermTitle());
        holder.item_Title.setText(dataValue.getTermTitle());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", dataValue);
                returnIntent.putExtra("pos", position);
                mActivity.setResult(556, returnIntent);
                mActivity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title,txtEdit,txtDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}
