package jaohar.com.jaohar.RetrofitApi;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import jaohar.com.jaohar.models.AddBlogModel;
import jaohar.com.jaohar.models.AddForumModel;
import jaohar.com.jaohar.models.AddUniversalTermsModel;
import jaohar.com.jaohar.models.AddVesselmodel;
import jaohar.com.jaohar.models.AddressBookListModel;
import jaohar.com.jaohar.models.AddressBookSearchModel;
import jaohar.com.jaohar.models.AllAdminBySearchModel;
import jaohar.com.jaohar.models.AllCompaniesTrashModel;
import jaohar.com.jaohar.models.AllMailListContactsModel;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.CompaniesModel;
import jaohar.com.jaohar.models.CompanyClassModel;
import jaohar.com.jaohar.models.ContactByIdModel;
import jaohar.com.jaohar.models.CurrenciesTrashModel;
import jaohar.com.jaohar.models.GetAllAdminModuleModel;
import jaohar.com.jaohar.models.GetAllAdminRoleModel;
import jaohar.com.jaohar.models.GetAllBankTrashModel;
import jaohar.com.jaohar.models.GetAllInvoiceTemplate;
import jaohar.com.jaohar.models.GetAllInvoiceVesselTrash;
import jaohar.com.jaohar.models.GetAllMailListsModel;
import jaohar.com.jaohar.models.GetAllNewReqModel;
import jaohar.com.jaohar.models.GetAllVesselTrashModel;
import jaohar.com.jaohar.models.GetCompanyNewsModel;
import jaohar.com.jaohar.models.GetFavoriteVesselsModel;
import jaohar.com.jaohar.models.GetStaticLatestNewsModel;
import jaohar.com.jaohar.models.GetUniversalTermsModel;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.InvoiceModel;
import jaohar.com.jaohar.models.LinksToHomeModel;
import jaohar.com.jaohar.models.LoginModell;
import jaohar.com.jaohar.models.MailModel;
import jaohar.com.jaohar.models.NotificationModel;
import jaohar.com.jaohar.models.OwnerByIdModel;
import jaohar.com.jaohar.models.OwnersModel;
import jaohar.com.jaohar.models.ProfileModel;
import jaohar.com.jaohar.models.RecentSearchModel;
import jaohar.com.jaohar.models.SearchModel;
import jaohar.com.jaohar.models.SearchNewReqModel;
import jaohar.com.jaohar.models.StaffLoginModel;
import jaohar.com.jaohar.models.StatusModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.VesselDetailsModel;
import jaohar.com.jaohar.models.VesselForSaleModel;
import jaohar.com.jaohar.models.chatmodels.AddNewMessageModel;
import jaohar.com.jaohar.models.chatmodels.DeleteMessageModel;
import jaohar.com.jaohar.models.chatmodels.GetAllChatMessagesModel;
import jaohar.com.jaohar.models.forummodels.AddStaffForumMessageModel;
import jaohar.com.jaohar.models.forummodels.DeleteStaffForumMessageModel;
import jaohar.com.jaohar.models.forummodels.GetAllForumMessagesModel;
import jaohar.com.jaohar.models.forummodels.GetAllForumsModel;
import jaohar.com.jaohar.models.forummodels.UpdateForumHeadingModel;
import jaohar.com.jaohar.models.invoicedraftmodels.AllInvoicesDrafts;
import jaohar.com.jaohar.models.invoicemodels.GetUniversalInvoiceNoModel;
import jaohar.com.jaohar.models.pendingPayments.PendingPaymentModels;
import jaohar.com.jaohar.models.pendingPayments.SearchPendingPaymentModel;
import jaohar.com.jaohar.models.profileandtabsmodels.ProfileResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("AddForum.php")
    Call<AddForumModel> addForum11(
            @Field("user_id") String user_id,
            @Field("photo") String photo,
            @Field("subject") String description,
            @Field("forum_type") String tags,
            @Field("user_list") String postHeight
    );


    @FormUrlEncoded
    @POST("GetAllStaffForums.php")
    Call<GetAllForumsModel> getRomaniaForumListNew(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no
    );

    @FormUrlEncoded
    @POST("AddStaffForum.php")
    Call<AddForumModel> addForumStaff11(
            @Field("user_id") String user_id,
            @Field("photo") String photo,
            @Field("subject") String description,
            @Field("forum_type") String tags,
            @Field("user_list") String postHeight
    );


    @POST("GetUserListForum.php")
    Call<JSONObject> getUserListForum(@Body Map<String, String> mParams);

    @POST("GetUserListForum_Staff.php")
    Call<JSONObject> getUserListForumStaff(@Body Map<String, String> mParams);

    @POST("GetUserListStaffForum_Staff.php")
    Call<JSONObject> getUserListStaffForumStaff(@Body Map<String, String> mParams);

    @POST("MailVesselToList.php")
    Call<StatusMsgModel> mailVesseltoList(@Body Map<String, String> mParams);

    @POST("signup_android.php")
    Call<StatusMsgModel> signupAndroidRequest(@Body Map<String, String> mParams);

    @POST("MailVesselToListShell.php")
    Call<StatusMsgModel> mailVesselToListShell(@Body Map<String, String> mParams);

    @Headers("Content-Type: application/json")
    @GET("GetAllStaffForums.php")
    Call<JSONObject> getForumList(String check);


    @FormUrlEncoded
    @POST("GetAllStaffForums.php")
    Call<JsonObject> getForumList1(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no
    );

    @FormUrlEncoded
    @POST("GetMailLists.php")
    Call<GetAllMailListsModel> getMailLists(
            @Field("user_id") String user_id
    );


    @FormUrlEncoded
    @POST("GetAllForumMessages.php")
    Call<JsonObject> getAllForumList(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("page_no") String page_no
    );

    @FormUrlEncoded
    @POST("UpdateStaffForumHeading.php")
    Call<JsonObject> updateForumHeadingRequest(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("heading") String heading);

    @FormUrlEncoded
    @POST("UpdateForumHeading.php")
    Call<JsonObject> updateForumUserHeadingRequest(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("heading") String heading);


    @FormUrlEncoded
    @POST("DeleteStaffForumMessage.php")
    Call<JsonObject> deleteStaffForumMessage(
            @Field("user_id") String user_id,
            @Field("message_id") String message_id
    );

    @FormUrlEncoded
    @POST("DeleteForumMessage.php")
    Call<JsonObject> deleteForumMessage(
            @Field("user_id") String user_id,
            @Field("message_id") String message_id
    );


    @FormUrlEncoded
    @POST("GetAllForums.php")
    Call<JsonObject> getForumListStaff(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no
    );

    @FormUrlEncoded
    @POST("GetAllBlogCategories.php")
    Call<JsonObject> getAllBlogCategories(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("DeleteBlogCategory.php")
    Call<StatusMsgModel> deleteBlogCategory(
            @Field("user_id") String user_id,
            @Field("category_id") String category_id
    );

    @FormUrlEncoded
    @POST("EditBlogCategory.php")
    Call<StatusMsgModel> editBlogCategory(
            @Field("user_id") String user_id,
            @Field("category_id") String category_id,
            @Field("category") String category
    );

    @FormUrlEncoded
    @POST("AddBlogCategory.php")
    Call<StatusMsgModel> addBlogCategory(
            @Field("user_id") String user_id,
            @Field("category") String category
    );

    @FormUrlEncoded
    @POST("AddModule.php")
    Call<StatusMsgModel> addModule(
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("app_image") String app_image

    );

    @FormUrlEncoded
    @POST("EditModule.php")
    Call<StatusMsgModel> editModule(
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("app_image") String app_image,
            @Field("module_id") String module_id);

    @FormUrlEncoded
    @POST("ControlModuleAccess.php")
    Call<StatusMsgModel> controlModuleAccess(
            @Field("module_id") String module_id,
            @Field("access_type") String access_type,
            @Field("user_list") String user_list,
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("GetModuleUsers.php")
    Call<JsonObject> getModuleUsers(
            @Field("module_id") String module_id,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("ControlModuleDisability.php")
    Call<StatusMsgModel> controlModuleDisability(
            @Field("disable") String disable,
            @Field("module_id") String module_id,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllModulesAdmin.php")
    Call<GetAllAdminModuleModel> getAllModulesAdmin(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("GetAllBlogs.php")
    Call<JsonObject> getAllBlog(
            @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("DeleteModule.php")
    Call<StatusMsgModel> deleteModule(
            @Field("disable") String disable,
            @Field("module_id") String module_id
    );

    @FormUrlEncoded
    @POST("GetNotificationList.php")
    Call<JsonObject> getNotificationsList(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("AddBlogComment.php")
    Call<StatusMsgModel> addBlogComment(
            @Field("user_id") String page_no,
            @Field("blog_id") String blog_id,
            @Field("comment") String comment,
            @Field("reply_id") String reply_id
    );

    @FormUrlEncoded
    @POST("DeleteBlogComment.php")
    Call<StatusMsgModel> deleteBlogComment(
            @Field("user_id") String page_no,
            @Field("comment_id") String comment_id
    );

    @FormUrlEncoded
    @POST("DeleteBlog.php")
    Call<StatusMsgModel> deleteBlog(
            @Field("user_id") String user_id,
            @Field("blog_id") String blog_id
    );

    @FormUrlEncoded
    @POST("GetAllEducationPosts.php")
    Call<JsonObject> getEducationalPosts(
            @Field("user_id") String page_no,
            @Field("page_no") String user_id
    );

//  @FormUrlEncoded
//    @POST("GetAllEducationPosts.php")
//    Call<GetAllEducationalBlogsModel> getEducationalPosts(
//            @Field("user_id") String page_no,
//            @Field("page_no") String user_id
//    );

    @FormUrlEncoded
    @POST("GetEducationPostBySearch.php")
    Call<JsonObject> getEducationPostBySearch(
            @Field("user_id") String user_id,
            @Field("input_search") String input_search
    );

    @FormUrlEncoded
    @POST("GetAllStaffForumMessages.php")
    Call<JsonObject> getAllStaffForumMessages(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("page_no") String page_no);

    @POST("AddBlog.php")
    Call<AddBlogModel> addBlogs(@Body Map<String, String> mParams);

    @POST("EditBlog.php")
    Call<AddBlogModel> editBlogs(@Body Map<String, String> mParams);

    @POST("EditCoverImage.php")
    Call<StatusMsgModel> editCoverImage(@Body Map<String, String> mParams);

    @FormUrlEncoded
    @POST("GetAllBlogCommentFront.php")
    Call<JsonObject> getAllBlogCommentFront(
            @Field("user_id") String user_id,
            @Field("blog_id") String blog_id,
            @Field("page_no") String page_no
    );


    @POST("AddChatGroup.php")
    Call<JsonObject> addChatGroup(@Body Map<String, String> mParams1);

    @POST("CheckForumUpdation.php")
    Call<JsonObject> checkUpdateForumRequest(@Body Map<String, String> mParams1);

    @POST("CheckStaffForumUpdation.php")
    Call<JsonObject> checkStaffForumUpdation(@Body Map<String, String> mParams1);

    @FormUrlEncoded
    @POST("GetAllChatUsers.php")
    Call<JsonObject> getAllChatUsers(
            @Field("user_id") String user_id,
            @Field("page_no") int page_no
    );

    @POST("EditChatGroup.php")
    Call<JsonObject> editChatGroup(@Body Map<String, String> mParams);


    @FormUrlEncoded
    @POST("EditChatGroup.php")
    Call<JsonObject> editChatGroup1(
            @Field("user_id") String user_id,
            @Field("group_id") String group_id,
            @Field("group_name") String group_name,
            @Field("user_list") String user_list,
            @Field("photo") String photo
    );

    @FormUrlEncoded
    @POST("SearchChatUsers.php")
    Call<JsonObject> searchChatUsers(@Field("user_id") String user_id,
                                     @Field("input_search_value") String input_search_value);

    @FormUrlEncoded
    @POST("GetGroupDetail.php")
    Call<JsonObject> getGroupDetail(
            @Field("user_id") String user_id
            , @Field("group_id") String group_id);

    @FormUrlEncoded
    @POST("DeleteChat.php")
    Call<JsonObject> deleteChat(
            @Field("user_id") String user_id,
            @Field("room_id") String room_id);

    @FormUrlEncoded
    @POST("DeleteGroup.php")
    Call<JsonObject> deleteGroup(
            @Field("user_id") String user_id,
            @Field("id") String id);


    @FormUrlEncoded
    @POST("GetAllVesselsTrash.php")
    Call<GetAllVesselTrashModel> getAllVesselsTrash(
            @Field("page_no") String page_no,
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("GetAllInvoiceVesselsTrash.php")
    Call<GetAllInvoiceVesselTrash> getAllInvoiceVesselsTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllInvoiceTrash.php")
    Call<JsonObject> getAllInvoiceTrash(
            @Field("page_no") String page_no,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllUniversalInvoiceTrash.php")
    Call<JsonObject> getAllUniversalInvoiceTrash(
            @Field("page_no") String page_no,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllCurrenciesTrash.php")
    Call<CurrenciesTrashModel> getAllCurrenciesTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EmptyVesselTrash.php")
    Call<StatusMsgModel> emptyVesselTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EmptyCurrencyTrash.php")
    Call<StatusMsgModel> emptyCurrencyTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EmptyUniversalInvoiceTrash.php")
    Call<StatusMsgModel> emptyUniversalInvoiceTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("RecoverSingleVesselTrash.php")
    Call<StatusMsgModel> recoverSingleVesselTrash(
            @Field("user_id") String user_id,
            @Field("vessel_id") String vessel_id
    );

    @FormUrlEncoded
    @POST("RecoverSingleInvoiceTrash.php")
    Call<StatusMsgModel> recoverSingleInvoiceTrash(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id
    );

    @FormUrlEncoded
    @POST("RecoverSingleUniversalInvoiceTrash.php")
    Call<StatusMsgModel> recoverSingleUniversalInvoiceTrash(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id
    );

    @FormUrlEncoded
    @POST("DeleteSingleVesselTrash.php")
    Call<StatusMsgModel> deleteSingleVesselTrash(
            @Field("user_id") String user_id,
            @Field("vessel_id") String vessel_id
    );

    @FormUrlEncoded
    @POST("DeleteUniversalInvoice.php")
    Call<StatusMsgModel> deleteUniversalInvoice(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id
    );

    @FormUrlEncoded
    @POST("GetAllBankTrash.php")
    Call<GetAllBankTrashModel> getAllBankTrash(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("DeleteSingleInvoiceVesselTrash.php")
    Call<StatusMsgModel> deleteSingleInvoiceVesselTrash(
            @Field("user_id") String user_id,
            @Field("vessel_id") String vessel_id
    );

    @FormUrlEncoded
    @POST("DeleteSingleCurrencyTrash.php")
    Call<StatusMsgModel> deleteSingleCurrencyTrash(
            @Field("user_id") String user_id,
            @Field("currency_id") String currency_id
    );

    @FormUrlEncoded
    @POST("RecoverSingleCompanyTrash.php")
    Call<StatusMsgModel> recoverSingleCompanyTrash(
            @Field("user_id") String user_id,
            @Field("company_id") String company_id
    );

    @FormUrlEncoded
    @POST("DeleteSingleInvoiceTrash.php")
    Call<StatusMsgModel> deleteSingleInvoiceTrash(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id
    );

    @FormUrlEncoded
    @POST("DeleteSingleCompanyTrash.php")
    Call<StatusMsgModel> deleteSingleCompanyTrash(
            @Field("user_id") String user_id,
            @Field("company_id") String company_id
    );

    @FormUrlEncoded
    @POST("DeleteSingleBankTrash.php")
    Call<StatusMsgModel> deleteSingleBankTrash(
            @Field("user_id") String user_id,
            @Field("bank_id") String bank_id
    );

    @FormUrlEncoded
    @POST("DeleteSingleUniversalInvoiceTrash.php")
    Call<StatusMsgModel> deleteSingleUniversalInvoiceTrash(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id
    );

    @FormUrlEncoded
    @POST("RecoverSingleBankTrash.php")
    Call<StatusMsgModel> recoverSingleBankTrash(
            @Field("user_id") String user_id,
            @Field("bank_id") String bank_id
    );


    @FormUrlEncoded
    @POST("RecoverSingleInvoiceVesselTrash.php")
    Call<StatusMsgModel> recoverSingleInvoiceVesselTrash(
            @Field("user_id") String user_id,
            @Field("vessel_id") String vessel_id
    );

    @FormUrlEncoded
    @POST("RecoverSingleCurrencyTrash.php")
    Call<StatusMsgModel> recoverSingleCurrencyTrash(
            @Field("user_id") String user_id,
            @Field("currency_id") String currency_id
    );

    @FormUrlEncoded
    @POST("EmptyInvoiceTrash.php")
    Call<StatusMsgModel> emptyInvoiceTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EmptyCompanyTrash.php")
    Call<StatusMsgModel> emptyCompanyTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EmptyBankTrash.php")
    Call<StatusMsgModel> emptyBankTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EmptyInvoiceVesselTrash.php")
    Call<StatusMsgModel> emptyInvoiceVesselTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllCompaniesTrash.php")
    Call<AllCompaniesTrashModel> getAllCompaniesTrash(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllChatMessages.php")
    Call<JsonObject> getAllChatMessages(@Field("user_id") String user_id,
                                        @Field("room_id") String room_id,
                                        @Field("page_num") int page_num);

    @FormUrlEncoded
    @POST("GetAllChatRooms.php")
    Call<JsonObject> getAllChatRooms(
            @Field("user_id") String user_id,
            @Field("page_no") int page_no);

    @FormUrlEncoded
    @POST("SearchChatRooms.php")
    Call<JsonObject> searchChatRoom(
            @Field("user_id") String user_id,
            @Field("input_search_value") String input_search_value);

    @FormUrlEncoded
    @POST("AddChatGroup.php")
    Call<JsonObject> addChatGroup1(
            @Field("user_id") String user_id,
            @Field("group_name") String group_name,
            @Field("user_list") String user_list,
            @Field("photo") String photo
    );

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginModell> loginRequest(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
    );

    @POST("login_staff_android.php")
    Call<StaffLoginModel> loginStaffRequest(@Body Map<String, String> mParams);

    @POST("RecoverSelectedVesselTrash.php")
    Call<StatusMsgModel> recoverSelectedVesselTrash(@Body Map<String, String> mParams);

    @POST("RecoverSelectedCurrencyTrash.php")
    Call<StatusMsgModel> recoverSelectedCurrencyTrash(@Body Map<String, String> mParams);

    @POST("RecoverSelectedCompanyTrash.php")
    Call<StatusMsgModel> recoverSelectedCompanyTrash(@Body Map<String, String> mParams);

    @POST("RecoverSelectedBankTrash.php")
    Call<StatusMsgModel> recoverSelectedBankTrash(@Body Map<String, String> mParams);

    @POST("RecoverSelectedInvoiceVesselTrash.php")
    Call<StatusMsgModel> recoverSelectedInvoiceVesselTrash(@Body Map<String, String> mParams);

    @POST("RecoverSelectedUniversalInvoiceTrash.php")
    Call<StatusMsgModel> recoverSelectedUniversalInvoiceTrash(@Body Map<String, String> mParams);

    @POST("DeleteSelectedUniversalInvoiceTrash.php")
    Call<StatusMsgModel> deleteSelectedUniversalInvoiceTrash(@Body Map<String, String> mParams);

    @POST("DeleteSelectedCompanyTrash.php")
    Call<StatusMsgModel> deleteSelectedCompanyTrash(@Body Map<String, String> mParams);

    @POST("DeleteSelectedCurrencyTrash.php")
    Call<StatusMsgModel> deleteSelectedCurrencyTrash(@Body Map<String, String> mParams);

    @POST("DeleteSelectedBankTrash.php")
    Call<StatusMsgModel> deleteSelectedBankTrash(@Body Map<String, String> mParams);

    @POST("DeleteSelectedInvoiceVesselTrash.php")
    Call<StatusMsgModel> deleteSelectedInvoiceVesselTrash(@Body Map<String, String> mParams);

    @POST("RecoverSelectedInvoiceTrash.php")
    Call<StatusMsgModel> recoverSelectedInvoiceTrash(@Body Map<String, String> mParams);

    @POST("DeleteSelectedVesselTrash.php")
    Call<StatusMsgModel> deleteSelectedVesselTrash(@Body Map<String, String> mParams);

    @POST("signup_android.php")
    Call<StatusMsgModel> signupRequest(@Body Map<String, String> mParams);


    @FormUrlEncoded
    @POST("AddDeviceToken.php")
    Call<StatusMsgModel> sendMassPushRequest(
            @Field("user_id") String user_id,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
    );


    @FormUrlEncoded
    @POST("UpdateOnlineStatus.php")
    Call<StatusMsgModel> updateOnLineStatusRequest(
            @Field("user_id") String user_id,
            @Field("login_status") String login_status
    );

    @FormUrlEncoded
    @POST("WebLogin.php")
    Call<StatusMsgModel> webLoginRequest(
            @Field("code") String code,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("GetAllCurrencies.php")
    Call<JsonObject> getAllCurrenciesRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("ForgotPassword.php")
    Call<StatusMsgModel> forgotPasswordRequest(
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("GetAllFavoriteVessels.php")
    Call<GetFavoriteVesselsModel> getAllFavoriteVesselsRequest(
            @Field("page_no") String page_no,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("UnFavoriteVessel.php")
    Call<StatusMsgModel> unFavoriteVesselsRequest(
            @Field("vessel_id") String vessel_id,
            @Field("user_id") String user_id
    );

    @POST("GetlinksToHome.php")
    Call<LinksToHomeModel> getAllLinksToHomeRequest();

    @FormUrlEncoded
    @POST("GetAllVesselsPagination.php")
    Call<VesselForSaleModel> getAllVesselsRequest(
            @Field("page_no") String page_no,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("GetSingleVesselById.php")
    Call<VesselDetailsModel> getSingleVesselDetailRequest(
            @Field("input_vessel_id") String input_vessel_id,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("SearchVessel.php")
    Call<SearchModel> searchVesselRequest(
            @Field("input_search_value") String input_search_value,
            @Field("role") String role,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("AdvancedSearch.php")
    Call<SearchModel> advanceSearchVesselRequest(
            @Field("capacity_from") String capacity_from,
            @Field("capacity_to") String capacity_to,
            @Field("loa_from") String loa_from,
            @Field("loa_to") String loa_to,
            @Field("vessel_type") String vessel_type,
            @Field("year_of_built_from") String year_of_built_from,
            @Field("year_of_built_to") String year_of_built_to,
            @Field("grain_from") String grain_from,
            @Field("grain_to") String grain_to,
            @Field("bale_from") String bale_from,
            @Field("name") String name,
            @Field("record_id") String record_id,
            @Field("IMO_number") String IMO_number,
            @Field("bale_to") String bale_to,
            @Field("status") String status,
            @Field("geared") String geared,
            @Field("role") String role,
            @Field("teu_from") String teu_from,
            @Field("teu_to") String teu_to,
            @Field("no_passengers_from") String no_passengers_from,
            @Field("no_passengers_to") String no_passengers_to,
            @Field("draft_from") String draft_from,
            @Field("draft_to") String draft_to,
            @Field("liquid_from") String liquid_from,
            @Field("liquid_to") String liquid_to,
            @Field("class") String classs,
            @Field("place_of_built") String place_of_built,
            @Field("user_id") String user_id,
            @Field("off_market") String off_market
    );

    @FormUrlEncoded
    @POST("AdvancedFavoriteVessel.php")
    Call<SearchModel> executeAdvanceFavoriteSearchAPI(
            @Field("capacity_from") String capacity_from,
            @Field("capacity_to") String capacity_to,
            @Field("loa_from") String loa_from,
            @Field("loa_to") String loa_to,
            @Field("vessel_type") String vessel_type,
            @Field("year_of_built_from") String year_of_built_from,
            @Field("year_of_built_to") String year_of_built_to,
            @Field("grain_from") String grain_from,
            @Field("grain_to") String grain_to,
            @Field("bale_from") String bale_from,
            @Field("name") String name,
            @Field("record_id") String record_id,
            @Field("IMO_number") String IMO_number,
            @Field("bale_to") String bale_to,
            @Field("status") String status,
            @Field("geared") String geared,
            @Field("role") String role,
            @Field("teu_from") String teu_from,
            @Field("teu_to") String teu_to,
            @Field("no_passengers_from") String no_passengers_from,
            @Field("no_passengers_to") String no_passengers_to,
            @Field("draft_from") String draft_from,
            @Field("draft_to") String draft_to,
            @Field("liquid_from") String liquid_from,
            @Field("liquid_to") String liquid_to,
            @Field("class") String classs,
            @Field("place_of_built") String place_of_built,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("MakeVesselFavorite.php")
    Call<StatusMsgModel> makeFavoriteVesselsRequest(
            @Field("vessel_id") String vessel_id,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("DeleteVessels.php")
    Call<StatusMsgModel> deleteVesselsRequest(
            @Field("id") String id,
            @Field("user_id") String user_id
    );

    @POST("GetStatus.php")
    Call<StatusModel> getStatusRequest();

    @FormUrlEncoded
    @POST("DeleteNews.php")
    Call<StatusMsgModel> deleteNewsRequest(
            @Field("news_id") String news_id);

    @FormUrlEncoded
    @POST("EditStatus.php")
    Call<StatusMsgModel> editStatusRequest(
            @Field("status_name") String status_name,
            @Field("status_id") String status_id);


    @FormUrlEncoded
    @POST("DeleteStatus.php")
    Call<StatusMsgModel> deleteStatusRequest(
            @Field("status_id") String status_id);

    @FormUrlEncoded
    @POST("AddStatus.php")
    Call<StatusMsgModel> addStatusRequest(
            @Field("status_name") String status_name);

    @FormUrlEncoded
    @POST("DeleteVessel_Types.php")
    Call<StatusMsgModel> deleteVesselTypeRequest(
            @Field("vessel_id") String vessel_id);

    @FormUrlEncoded
    @POST("GetAddressBookById.php")
    Call<ContactByIdModel> getAddressBookByRequest(
            @Field("user_id") String user_id,
            @Field("contact_id") String contact_id);

    @FormUrlEncoded
    @POST("DeleteUser.php")
    Call<StatusMsgModel> deleteUserRequest(
            @Field("id") String id,
            @Field("user_id") String user_id,
            @Field("device_type") String device_type
    );

    @FormUrlEncoded
    @POST("DeleteCurrency.php")
    Call<StatusMsgModel> deleteCurrencyRequest(
            @Field("user_id") String user_id,
            @Field("id") String id);


    @FormUrlEncoded
    @POST("EditAddressBookContact.php")
    Call<StatusMsgModel> editAddressBookContactRequest(
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("company_name") String company_name,
            @Field("email1") String email1,
            @Field("email2") String email2,
            @Field("email3") String email3,
            @Field("phone1") String phone1,
            @Field("phone2") String phone2,
            @Field("phone3") String phone3,
            @Field("contact_id") String contact_id
    );

    @FormUrlEncoded
    @POST("ResetPassword.php")
    Call<StatusMsgModel> resetPasswordRequest(
            @Field("user_id") String user_id,
            @Field("email") String email,
            @Field("reset_user_id") String reset_user_id
    );

    @FormUrlEncoded
    @POST("EditRole.php")
    Call<StatusMsgModel> editRoleRequest(
            @Field("user_id") String user_id,
            @Field("new_role") String new_role
    );

    @FormUrlEncoded
    @POST("LogoutAdmin.php")
    Call<StatusMsgModel> adminLogoutRequest(
            @Field("user_id") String user_id,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("GetUserProfile.php")
    Call<JsonObject> getuserProfileRequest(
            @Field("user_id") String user_id);

//    @FormUrlEncoded
//    @POST("GetAllUniversalInvoiceOwner.php")
//    Call<JsonObject> getAllUniversalInvoiceOwnerRequest(
//            @Field("user_id") String user_id,
//            @Field("page_no") String page_no,
//            @Field("owner_id") String owner_id
//    );

    @FormUrlEncoded
    @POST("GetAllUniversalInvoiceOwnerAndroid.php")
    Call<JsonObject> getAllUniversalInvoiceOwnerRequest(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no,
            @Field("owner_id") String owner_id
    );

    @FormUrlEncoded
    @POST("GetAllAddressBookMails.php")
    Call<JsonObject> getAllAddressBookMailsRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllDynamicLinks.php")
    Call<JsonObject> getAllDynamicLinksRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("AddAddressBookContact.php")
    Call<StatusMsgModel> getAddAddressBookContactRequest(
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("company_name") String company_name,
            @Field("email1") String email1,
            @Field("email2") String email2,
            @Field("email3") String email3,
            @Field("phone1") String phone1,
            @Field("phone2") String phone2,
            @Field("phone3") String phone3);

    @FormUrlEncoded
    @POST("logout.php")
    Call<StatusMsgModel> logoutRequest(
            @Field("user_id") String user_id,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("GetAllBanks.php")
    Call<JsonObject> getAllBankDetailsRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("GetAllInvoiceVessels.php")
    Call<JsonObject> getAllInvoiceVesselsRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("DeleteInvoiceVessel.php")
    Call<StatusMsgModel> deleteInvoiceVesselsRequest(
            @Field("user_id") String user_id,
            @Field("vessel_id") String vessel_id);

    @FormUrlEncoded
    @POST("DeleteMailList.php")
    Call<StatusMsgModel> deleteMailListRequest(
            @Field("list_id") String list_id);

    @FormUrlEncoded
    @POST("GetAllMailLists.php")
    Call<GetAllMailListsModel> getAllMailsListRequest(
            @Field("page_no") String page_no,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllSigns.php")
    Call<JsonObject> getAllSignsRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("DeleteSign.php")
    Call<StatusMsgModel> deleteSignsRequest(
            @Field("user_id") String user_id,
            @Field("sign_id") String sign_id
    );

    @FormUrlEncoded
    @POST("GetAllNotifications.php")
    Call<NotificationModel> getAllNotificationsRequest(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no
    );

    @FormUrlEncoded
    @POST("DeleteMassPush.php")
    Call<StatusMsgModel> deleteMassPushRequest(
            @Field("notification_id") String notification_id,
            @Field("user_id") String user_id
    );

    @POST("GetCompanyNews.php")
    Call<GetCompanyNewsModel> getCompanyNewsRequest();

    @POST("GetInternalNews.php")
    Call<GetCompanyNewsModel> getInternalNewsRequest();

    @FormUrlEncoded
    @POST("DeleteBankDetails.php")
    Call<StatusMsgModel> deleteBankDetailsRequest(
            @Field("user_id") String user_id,
            @Field("bank_id") String bank_id
    );

    @FormUrlEncoded
    @POST("DeleteInternalNews.php")
    Call<StatusMsgModel> deleteInternalNewsRequest(
            @Field("news_id") String news_id
    );

    @FormUrlEncoded
    @POST("DeletelinkstoHome.php")
    Call<StatusMsgModel> deleteLinksToHomeRequest(
            @Field("link_id") String link_id
    );

    @FormUrlEncoded
    @POST("SearchFavoriteVessel.php")
    Call<SearchModel> searchFavoriteVesselRequest(
            @Field("input_search_value") String input_search_value,
            @Field("role") String role,
            @Field("user_id") String user_id
    );


    @FormUrlEncoded
    @POST("GetRecentSearch.php")
    Call<RecentSearchModel> getRecentSearchRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetUserProfile.php")
    Call<ProfileModel> getProfileDetailsRequest(
            @Field("user_id") String user_id);

    @POST("GetAllNewRequests.php")
    Call<GetAllNewReqModel> getAllNewRequest();

    @FormUrlEncoded
    @POST("GetInvoiceNo.php")
    Call<InvoiceModel> getInvoiceNumberRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("AcceptMemberRequest.php")
    Call<StatusMsgModel> acceptMemberRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("SearchNewRequest.php")
    Call<SearchNewReqModel> searchNewRequest(
            @Field("user_id") String user_id,
            @Field("input_search_value") String input_search_value
    );

    @FormUrlEncoded
    @POST("RejectMemberRequest.php")
    Call<StatusMsgModel> rejectMemberRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("AddInvoiceVessel.php")
    Call<InvoiceModel> addInvoiceVesselRequest(
            @Field("vessel_name") String vessel_name,
            @Field("IMO_no") String IMO_no,
            @Field("flag") String flag,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("AddMailList.php")
    Call<StatusMsgModel> addMAilListRequest(
            @Field("list_name") String list_name,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EditMailList.php")
    Call<StatusMsgModel> editMAilListRequest(
            @Field("list_name") String list_name,
            @Field("user_id") String user_id,
            @Field("list_id") String list_id
    );

    @FormUrlEncoded
    @POST("AddMailListContact.php")
    Call<StatusMsgModel> addMailListContactRequest(
            @Field("list_id") String list_id,
            @Field("user_id") String user_id,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("company") String company,
            @Field("email1") String email1,
            @Field("email2") String email2);

    @FormUrlEncoded
    @POST("EditMailListContact.php")
    Call<StatusMsgModel> editMailListContactRequest(
            @Field("contact_id") String contact_id,
            @Field("user_id") String user_id,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("company") String company,
            @Field("email1") String email1,
            @Field("email2") String email2);

    @FormUrlEncoded
    @POST("AddCurrency.php")
    Call<StatusMsgModel> addCurrencyRequest(
            @Field("currency_name") String currency_name,
            @Field("alias_name") String alias_name,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("SearchAddressBook.php")
    Call<AddressBookSearchModel> searchAddressBookRequest(
            @Field("input_search_value") String input_search_value,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllAddressBook.php")
    Call<AddressBookListModel> getAllAddressBookRequest(
            @Field("page_no") String page_no,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("DeleteAddressBookContact.php")
    Call<StatusMsgModel> deleteAddressBookContactRequest(
            @Field("contact_id") String contact_id);

    @FormUrlEncoded
    @POST("GetAllStamps.php")
    Call<JsonObject> getAlStampsRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("DeleteStamp.php")
    Call<StatusMsgModel> deleteStampsRequest(
            @Field("user_id") String user_id,
            @Field("stamp_id") String stamp_id);

    @FormUrlEncoded
    @POST("GetAllCompanies.php")
    Call<JsonObject> getAlCompaniesRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllCompanies.php")
    Call<CompanyClassModel> getAlCompaniesPendingPaymentRequest(
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("GetAllCompanies.php")
    Call<CompaniesModel> getAlCompaniesInvRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetInvoiceMails.php")
    Call<JsonObject> getInvoiceMailsRequest(
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("DeleteCompany.php")
    Call<StatusMsgModel> deleteCompaniesRequest(
            @Field("user_id") String user_id,
            @Field("company_id") String company_id);

    @FormUrlEncoded
    @POST("GetAllAdminAndRoles.php")
    Call<GetAllAdminRoleModel> getAllAdminRolesRequest(
            @Field("search") String search);

    @FormUrlEncoded
    @POST("GetAllAdminAndRolesBySearch.php")
    Call<AllAdminBySearchModel> getAllAdminRolesSearchRequest(
            @Field("search") String search);

    @FormUrlEncoded
    @POST("DisableAccount.php")
    Call<StatusMsgModel> disableAccountRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("EnableAccount.php")
    Call<StatusMsgModel> enableAccountRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("Get_all_invoice_pagination_android.php")
    Call<JsonObject> getAllInvoiceRequest(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("DownloadSinglePendingPayment.php")
    Call<JsonObject> getDownloadPdfLinkRequest(
            @Field("user_id") String user_id,
            @Field("pp_id") String pp_id);

    @FormUrlEncoded
    @POST("GetAllPendingPayments.php")
    Call<PendingPaymentModels> getAllPendingPaymentsRequest(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no);


    @FormUrlEncoded
    @POST("AddInvoiceItems.php")
    Call<StatusMsgModel> addInvoiceItemsRequest(
            @Field("serial_no") String serial_no,
            @Field("quantity") String quantity,
            @Field("price") String price,
            @Field("description") String description,
            @Field("user_id") String user_id);

//    @FormUrlEncoded
//    @POST("SearchInvoiceAndroid.php")
//    Call<JsonObject> searchInvoiceRequest(
//            @Field("user_id") String user_id,
//            @Field("search") String search,
//            @Field("role") String role);


    @FormUrlEncoded
    @POST("SimpleSearchInvoice.php")
    Call<JsonObject> searchInvoiceRequest(
            @Field("user_id") String user_id,
            @Field("search") String search,
            @Field("role") String role);

    @FormUrlEncoded
    @POST("SearchSimplePendingPayment.php")
    Call<SearchPendingPaymentModel> searchPendingPaymentRequest(
            @Field("user_id") String user_id,
            @Field("search") String search);


//    @FormUrlEncoded
//    @POST("AdvanceSearchInvoiceAndroid.php")
//    Call<AllInvoiceModel> advancedSearchInvoiceRequest(
//            @Field("invoice_no") String invoice_no,
//            @Field("role") String role,
//            @Field("inv_date_from") String inv_date_from,
//            @Field("inv_date_to") String inv_date_to,
//            @Field("search_company") String search_company,
//            @Field("invoice_vessel") String invoice_vessel,
//            @Field("invoice_currency") String invoice_currency,
//            @Field("invoice_stamp") String invoice_stamp,
//            @Field("invoice_sign") String invoice_sign,
//            @Field("invoice_status") String invoice_status,
//            @Field("invoice_bank") String invoice_bank,
//            @Field("user_id") String user_id
//            );

    @FormUrlEncoded
    @POST("AdvancedSearchInvoice.php")
    Call<JsonObject> advancedSearchInvoiceRequest(
            @Field("invoice_no") String invoice_no,
            @Field("role") String role,
            @Field("inv_date_from") String inv_date_from,
            @Field("inv_date_to") String inv_date_to,
            @Field("search_company") String search_company,
            @Field("invoice_vessel") String invoice_vessel,
            @Field("invoice_currency") String invoice_currency,
            @Field("invoice_stamp") String invoice_stamp,
            @Field("invoice_sign") String invoice_sign,
            @Field("invoice_status") String invoice_status,
            @Field("invoice_bank") String invoice_bank,
            @Field("user_id") String user_id
    );


    @FormUrlEncoded
    @POST("AdvancedSearchPendingPayment.php")
    Call<SearchPendingPaymentModel> advancedSearchPPInvoiceRequest(
            @Field("user_id") String user_id,
            @Field("pp_no") String pp_no,
            @Field("pp_company") String pp_company,
            @Field("pp_currency") String pp_currency,
            @Field("pp_date") String pp_date,
            @Field("pp_vessel1") String pp_vessel1
    );

    @FormUrlEncoded
    @POST("DeleteInvoice.php")
    Call<StatusMsgModel> deleteInvoiceRequest(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id);


    @FormUrlEncoded
    @POST("DeletePendingPayment.php")
    Call<StatusMsgModel> deletePPRequest(
            @Field("user_id") String user_id,
            @Field("pp_id") String pp_id);


    @FormUrlEncoded
    @POST("DeleteInvoice.php")
    Call<StatusMsgModel> deleteDraftInvoiceRequest(
            @Field("draft") String draft,
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id);

    @FormUrlEncoded
    @POST("AddVessel_Types.php")
    Call<StatusMsgModel> addVesselTypeRequest(
            @Field("vessel_type") String vessel_type);

    @FormUrlEncoded
    @POST("EditVessel_Types.php")
    Call<StatusMsgModel> editVesselTypeRequest(
            @Field("vessel_type") String vessel_type,
            @Field("vessel_id") String vessel_id);

    @FormUrlEncoded
    @POST("MailMultipleVessels.php")
    Call<StatusMsgModel> getMultipleVesselsRequest(@Field("to_email") String to_email,
                                                   @Field("subject") String subject,
                                                   @Field("vessel_ids") String vessel_id,
                                                   @Field("to_emails") String to_emails
    );

    @FormUrlEncoded
    @POST("MailVesselDetail.php")
    Call<StatusMsgModel> getSingleVesselsRequest(@Field("to_email") String to_email,
                                                 @Field("subject") String subject,
                                                 @Field("vessel_id") String vessel_id,
                                                 @Field("to_emails") String to_emails,
                                                 @Field("to_cc_emails") String to_cc_emails,
                                                 @Field("to_bcc_emails") String to_bcc_emails);

    @FormUrlEncoded
    @POST("Addlinks_To_Home_for_Staff.php")
    Call<StatusMsgModel> addLinksToHomeStaffRequest(@Field("link_name") String link_name,
                                                    @Field("link_value") String link_value);

    @FormUrlEncoded
    @POST("AddlinksToHome.php")
    Call<StatusMsgModel> addLinksToHomeRequest(@Field("link_name") String link_name,
                                               @Field("link_value") String link_value);

    @POST("GetAllNews.php")
    Call<AllNewsModel> getallNewsRequest();

    @POST("GetVessel_Types.php")
    Call<GetVesselTypeModel> getVesselTypeRequest();

    @POST("Getlinks_To_Home_for_Staff.php")
    Call<LinksToHomeModel> getLinksToHomeRequest();

    @FormUrlEncoded
    @POST("Deletelinks_To_Home_for_Staff.php")
    Call<StatusMsgModel> deleteStaffLinksToHomeRequest(
            @Field("link_id") String link_id
    );

    @FormUrlEncoded
    @POST("EditCurrency.php")
    Call<StatusMsgModel> editCurrencyRequest(
            @Field("currency_name") String currency_name,
            @Field("alias_name") String alias_name,
            @Field("user_id") String user_id,
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("Editlinks_To_Home_for_Staff.php")
    Call<StatusMsgModel> editStaffLinksRequest(
            @Field("link_name") String link_name,
            @Field("link_value") String link_value,
            @Field("link_id") String link_id);

    @FormUrlEncoded
    @POST("GetMassPushById.php")
    Call<JsonObject> getMassPushByIdRequest(
            @Field("user_id") String user_id,
            @Field("notification_id") String notification_id);

    @FormUrlEncoded
    @POST("GetAllDataForInvoiceEditAndroid.php")
    Call<JsonObject> editInvoiceDataRequest(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id);

    @FormUrlEncoded
    @POST("GetAllDataForUniversalInvoiceAndroid.php")
    Call<JsonObject> editUniversalInvoiceDataRequest(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id);

//    @FormUrlEncoded
//    @POST("GetAllDataForInvoiceEdit.php")
//    Call<JsonObject> getAllInvoiceDataRequest(
//            @Field("user_id") String user_id,
//            @Field("invoice_id") String invoice_id);

    @FormUrlEncoded
    @POST("GetAllDataForInvoiceCopy.php")
    Call<JsonObject> getAllInvoiceDataCopyRequest(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id);

    @FormUrlEncoded
    @POST("GetAllDataForUniversalInvoiceCopyAndroid.php")
    Call<JsonObject> getAllInvoiceDataCopyUniversalRequest(
            @Field("user_id") String user_id,
            @Field("invoice_id") String invoice_id);

    @FormUrlEncoded
    @POST("EditInvoiceVessel.php")
    Call<JsonObject> editInvoiceVesselRequest(
            @Field("vessel_name") String vessel_name,
            @Field("IMO_no") String IMO_no,
            @Field("flag") String flag,
            @Field("user_id") String user_id,
            @Field("vessel_id") String vessel_id
    );

    @FormUrlEncoded
    @POST("EditlinksToHome.php")
    Call<StatusMsgModel> editLinksToHomeRequest(
            @Field("link_name") String link_name,
            @Field("link_value") String link_value,
            @Field("link_id") String link_id
    );

    @FormUrlEncoded
    @POST("GetAllInvoiceTemplates.php")
    Call<GetAllInvoiceTemplate> getAllInvoiceTemplatesRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("GetAllOwners.php")
    Call<OwnersModel> getAllOwnersRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("DeleteOwner.php")
    Call<StatusMsgModel> deleteOwnerRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("GetOwnerById.php")
    Call<OwnerByIdModel> getOwnerByIdRequest(
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("SimpleSearchUniversalInvoice.php")
    Call<JsonObject> simpleSearchInvoiceRequest(
            @Field("user_id") String user_id,
            @Field("search") String search,
            @Field("role") String role
    );

    @FormUrlEncoded
    @POST("SimpleSearchUniversalInvoiceAndroid.php")
    Call<JsonObject> simpleSearchUniversalInvoiceRequest(
            @Field("user_id") String user_id,
            @Field("search") String search,
            @Field("role") String role
    );

    @POST("GetVessel_Class.php")
    Call<GetVesselClassModel> getVesselClassRequest();

    @POST("GetLatestStaticNews.php")
    Call<GetStaticLatestNewsModel> getLatestStaticNewsRequest();

//    @POST("AddVesselAndroid.php")
//    Call<AddVesselmodel> addVesselsRequest(@Body Map<String, String> mParams);

    @POST("AddNews.php")
    Call<StatusMsgModel> addNewsRequest(@Body Map<String, String> mParams);

    @POST("AddOwner.php")
    Call<StatusMsgModel> addOwnerRequest(@Body Map<String, String> mParams);

    @POST("EditOwner.php")
    Call<StatusMsgModel> editOwnerRequest(@Body Map<String, String> mParams);

    @POST("MailSend.php")
    Call<MailModel> sendMailRequest(@Body Map<String, String> mParams);

    @POST("EditInvoice.php")
    Call<StatusMsgModel> editInvoiceRequest(@Body JsonObject mParams);

    @POST("EditUniversalInvoice.php")
    Call<StatusMsgModel> editUniversalInvoiceRequest(@Body JsonObject mParams);

    @POST("AddBankDetails.php")
    Call<StatusMsgModel> addBankDetailsRequest(@Body Map<String, String> mParams);

    @POST("AddCompany.php")
    Call<StatusMsgModel> addCompanyRequest(@Body Map<String, String> mParams);

    @POST("EditUserProfile.php")
    Call<StatusMsgModel> editUserProfileRequest(@Body Map<String, String> mParams);

    @FormUrlEncoded
    @POST("AddInvoicePayment.php")
    Call<JsonObject> addPaymentInvoiceRequest(@Field("sub_total") String sub_total,
                                              @Field("VAT") String VAT,
                                              @Field("vat_price") String vat_price,
                                              @Field("total") String total,
                                              @Field("paid") String paid,
                                              @Field("due") String due,
                                              @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("MailMultipleInvoices.php")
    Call<StatusMsgModel> mailMultipleInvoiceRequest(@Field("to_email") String to_email,
                                                    @Field("to_emails") String to_emails,
                                                    @Field("subject") String subject,
                                                    @Field("from_mail") String from_mail,
                                                    @Field("invoice_ids") String invoice_ids,
                                                    @Field("invoice_id") String invoice_id,
                                                    @Field("owner_id") String owner_id);

    @FormUrlEncoded
    @POST("MailSingleInvoice.php")
    Call<StatusMsgModel> mailSingleInvoiceRequest(@Field("to_email") String to_email,
                                                  @Field("to_emails") String to_emails,
                                                  @Field("subject") String subject,
                                                  @Field("from_mail") String from_mail,
                                                  @Field("invoice_ids") String invoice_ids,
                                                  @Field("invoice_id") String invoice_id,
                                                  @Field("owner_id") String owner_id);

    @FormUrlEncoded
    @POST("MailSingleUniversalInvoice.php")
    Call<StatusMsgModel> mailSingleUniversalInvoiceRequest(@Field("to_email") String to_email,
                                                           @Field("to_emails") String to_emails,
                                                           @Field("subject") String subject,
                                                           @Field("from_mail") String from_mail,
                                                           @Field("invoice_ids") String invoice_ids,
                                                           @Field("invoice_id") String invoice_id,
                                                           @Field("owner_id") String owner_id);

    @FormUrlEncoded
    @POST("MailAllInvoices.php")
    Call<StatusMsgModel> mailAllInvoiceRequest(@Field("to_email") String to_email,
                                               @Field("to_emails") String to_emails,
                                               @Field("subject") String subject,
                                               @Field("from_mail") String from_mail,
                                               @Field("invoice_ids") String invoice_ids,
                                               @Field("invoice_id") String invoice_id,
                                               @Field("owner_id") String owner_id);

    @FormUrlEncoded
    @POST("MailMultipleInvoices.php")
    Call<StatusMsgModel> mailMultipleInvoiceRequestt(@Field("to_email") String to_email,
                                                     @Field("to_emails") String to_emails,
                                                     @Field("subject") String subject,
                                                     @Field("from_mail") String from_mail,
                                                     @Field("invoice_ids") String invoice_ids,
                                                     @Field("invoice_id") String invoice_id,
                                                     @Field("owner_id") String owner_id,
                                                     @Field("to_cc_email") String to_cc_email,
                                                     @Field("to_bcc_email") String to_bcc_email,
                                                     @Field("to_cc_emails") String to_cc_emails,
                                                     @Field("to_bcc_emails") String to_bcc_emails);

    @FormUrlEncoded
    @POST("MailSingleInvoice.php")
    Call<StatusMsgModel> mailSingleInvoiceRequestt(@Field("to_email") String to_email,
                                                   @Field("to_emails") String to_emails,
                                                   @Field("subject") String subject,
                                                   @Field("from_mail") String from_mail,
                                                   @Field("invoice_ids") String invoice_ids,
                                                   @Field("invoice_id") String invoice_id,
                                                   @Field("owner_id") String owner_id,
                                                   @Field("to_cc_email") String to_cc_email,
                                                   @Field("to_bcc_email") String to_bcc_email,
                                                   @Field("to_cc_emails") String to_cc_emails,
                                                   @Field("to_bcc_emails") String to_bcc_emails);

    @FormUrlEncoded
    @POST("MailAllInvoices.php")
    Call<StatusMsgModel> mailAllInvoiceRequestt(@Field("to_email") String to_email,
                                                @Field("to_emails") String to_emails,
                                                @Field("subject") String subject,
                                                @Field("from_mail") String from_mail,
                                                @Field("invoice_ids") String invoice_ids,
                                                @Field("invoice_id") String invoice_id,
                                                @Field("owner_id") String owner_id,
                                                @Field("to_cc_email") String to_cc_email,
                                                @Field("to_bcc_email") String to_bcc_email,
                                                @Field("to_cc_emails") String to_cc_emails,
                                                @Field("to_bcc_emails") String to_bcc_emails);

    @FormUrlEncoded
    @POST("MailSingleUniversalInvoice.php")
    Call<StatusMsgModel> mailSingleUniversalInvoiceRequestt(@Field("to_email") String to_email,
                                                            @Field("to_emails") String to_emails,
                                                            @Field("subject") String subject,
                                                            @Field("from_mail") String from_mail,
                                                            @Field("invoice_ids") String invoice_ids,
                                                            @Field("invoice_id") String invoice_id,
                                                            @Field("owner_id") String owner_id,
                                                            @Field("to_cc_email") String to_cc_email,
                                                            @Field("to_bcc_email") String to_bcc_email,
                                                            @Field("to_cc_emails") String to_cc_emails,
                                                            @Field("to_bcc_emails") String to_bcc_emails);


    @FormUrlEncoded
    @POST("MailSinglePendingPayment.php")
    Call<StatusMsgModel> mailSinglePendingPaymentRequest(
            @Field("to_email") String to_email,
            @Field("to_emails") String to_emails,
            @Field("subject") String subject,
            @Field("from_mail") String from_mail,
            @Field("pp_id") String pp_id,
            @Field("user_id") String owner_id,
            @Field("to_cc_email") String to_cc_email,
            @Field("to_bcc_email") String to_bcc_email,
            @Field("to_cc_emails") String to_cc_emails,
            @Field("to_bcc_emails") String to_bcc_emails);


    @FormUrlEncoded
    @POST("GetAllMailListContacts.php")
    Call<AllMailListContactsModel> getAllMailListContactsRequest(@Field("page_no") String page_no,
                                                                 @Field("user_id") String user_id,
                                                                 @Field("list_id") String list_id);

    @FormUrlEncoded
    @POST("DeleteMailListContact.php")
    Call<StatusMsgModel> deleteMailListContactsRequest(@Field("contact_id") String contact_id);

    @FormUrlEncoded
    @POST("DeleteChatMessage.php")
    Call<JsonObject> deleteChatMessageRequest(
            @Field("user_id") String user_id,
            @Field("message_id") String message_id);

    @FormUrlEncoded
    @POST("Support.php")
    Call<StatusMsgModel> supportRequest(
            @Field("name") String name,
            @Field("email") String email,
            @Field("message") String message);

    @FormUrlEncoded
    @POST("GetUniversalInvoiceNo.php")
    Call<GetUniversalInvoiceNoModel> getUniversalInvoiceNoRequest(
            @Field("owner_id") String owner_id,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetUserListForum.php")
    Call<JsonObject> getUserListForumAdminRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetUserListForum_Staff.php")
    Call<JsonObject> getUserListForumUKStaffRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetUserListStaffForum_Staff.php")
    Call<JsonObject> getUserListForumStaffRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("AddTerm.php")
    Call<AddUniversalTermsModel> addUniversalTermRequest(
            @Field("user_id") String user_id,
            @Field("term_title") String term_title,
            @Field("term_desc") String term_desc
            );

    @FormUrlEncoded
    @POST("GetAllTerms.php")
    Call<GetUniversalTermsModel> getUniversalTermRequest(
            @Field("user_id") String user_id
            );

    @POST("EditMassPush.php")
    Call<JsonObject> editMassPushRequest(@Body Map<String, String> mParams);

    @POST("MailToList.php")
    Call<JsonObject> mailToListRequest(@Body Map<String, String> mParams);

    @POST("MailToMultipleList.php")
    Call<JsonObject> mailToMultipleListRequest(@Body Map<String, String> mParams);

    @POST("EditNews.php")
    Call<StatusMsgModel> editNewsRequest(@Body Map<String, String> mParams);

    @POST("EditSignAndroid.php")
    Call<StatusMsgModel> editSignRequest(@Body Map<String, String> mParams);

    @POST("EditStampAndroid.php")
    Call<StatusMsgModel> editStampRequest(@Body Map<String, String> mParams);

    @POST("EditCompanyNews.php")
    Call<StatusMsgModel> editCompanyNewsRequest(@Body Map<String, String> mParams);

    @POST("EditInternalNews.php")
    Call<StatusMsgModel> editInternalNewsRequest(@Body Map<String, String> mParams);

    @POST("EditBankDetails.php")
    Call<StatusMsgModel> editBankDetailsRequest(@Body Map<String, String> mParams);

    @POST("EditCompany.php")
    Call<StatusMsgModel> editCompanyRequest(@Body Map<String, String> mParams);

    @POST("AddMassPush.php")
    Call<StatusMsgModel> addMassPushRequest(@Body Map<String, String> mParams);

    @POST("AddCompanyNews.php")
    Call<StatusMsgModel> addCompanyNewsRequest(@Body Map<String, String> mParams);

    @POST("AddInternalNews.php")
    Call<StatusMsgModel> addInternalNewsRequest(@Body Map<String, String> mParams);

    @POST("AddInvoice.php")
    Call<InvoiceModel> addInvoiceRequestNew(@Body JsonObject mParams);

    @POST("AddUniversalInvoice.php")
    Call<InvoiceModel> addUniversalInvoiceRequestNew(@Body JsonObject mParams);

    @POST("CopyInvoice.php")
    Call<InvoiceModel> copyInvoiceRequestNew(@Body JsonObject mParams);

    @POST("CopyUniversalInvoice.php")
    Call<InvoiceModel> copyUniversalInvoiceRequestNew(@Body JsonObject mParams);

//    @POST("UpdateInvoicePdf.php")
//    Call<StatusMsgModel> uploadPdfInvoiceRequest(@Body Map<String, String> mParams);

    @POST("AddSignAndroid.php")
    Call<StatusMsgModel> addSignRequest(@Body Map<String, String> mParams);

    @POST("AddStampAndroid.php")
    Call<StatusMsgModel> addStampRequest(@Body Map<String, String> mParams);


    @Multipart
    @POST("UpdateInvoicePdfAndroid.php")
    Call<StatusMsgModel> uploadPdfInvoiceRequest(@Part("record_id") RequestBody record_id,
                                                 @Part("name") RequestBody name,
                                                 @Part MultipartBody.Part pdf);


    @Multipart
    @POST("UpdateUniversalInvoicePdfAndroid.php")
    Call<StatusMsgModel> uploadPdfUniversalInvoiceRequest(@Part("invoice_id") RequestBody invoice_id,
                                                          @Part MultipartBody.Part pdf);

    @Multipart
    @POST("UploadFiveDocuments.php")
    Call<StatusMsgModel> uploadDocumentRequest(@Part("record_id") RequestBody record_id,
                                               @Part MultipartBody.Part Document1,
                                               @Part MultipartBody.Part Document2,
                                               @Part MultipartBody.Part Document3,
                                               @Part MultipartBody.Part Document4,
                                               @Part MultipartBody.Part Document5,
                                               @Part MultipartBody.Part Document6,
                                               @Part MultipartBody.Part Document7,
                                               @Part MultipartBody.Part Document8,
                                               @Part MultipartBody.Part Document9,
                                               @Part MultipartBody.Part Document10
    );

    @Multipart
    @POST("MailToListIOS.php")
    Call<JsonObject> mailToListIOSRequest(@Part("list_id") RequestBody list_id,
                                          @Part("list_ids[]") ArrayList<String> items,
                                          @Part("subject") RequestBody subject,
                                          @Part("message") RequestBody message,
                                          @Part("head_one") RequestBody head_one,
                                          @Part("head_two") RequestBody head_two,
                                          @Part("doc_name") RequestBody doc_name,
                                          @Part MultipartBody.Part image,
                                          @Part("user_id") RequestBody user_id,
                                          @Part("sign") RequestBody sign,
                                          @Part MultipartBody.Part doc0,
                                          @Part MultipartBody.Part doc1,
                                          @Part MultipartBody.Part doc2,
                                          @Part MultipartBody.Part doc3,
                                          @Part MultipartBody.Part doc4);


//    @Part("list_ids") RequestBody list_ids,

    @Multipart
    @POST("MailToMultipleListIOS.php")
    Call<JsonObject> mailToMultipleListIOSRequest(@Part("list_id") RequestBody list_id,
                                                  @Part("list_ids[]") ArrayList<String> items,
                                                  @Part("subject") RequestBody subject,
                                                  @Part("message") RequestBody message,
                                                  @Part("head_one") RequestBody head_one,
                                                  @Part("head_two") RequestBody head_two,
                                                  @Part("doc_name") RequestBody doc_name,
                                                  @Part MultipartBody.Part image,
                                                  @Part("user_id") RequestBody user_id,
                                                  @Part("sign") RequestBody sign,
                                                  @Part MultipartBody.Part doc0,
                                                  @Part MultipartBody.Part doc1,
                                                  @Part MultipartBody.Part doc2,
                                                  @Part MultipartBody.Part doc3,
                                                  @Part MultipartBody.Part doc4);

    @Multipart
    @POST("MailToListIOS_Shell.php")
    Call<JsonObject> mailToListIOSShellRequest(@Part("list_id") RequestBody list_id,
                                               @Part("list_ids[]") ArrayList<String> items,
                                               @Part("subject") RequestBody subject,
                                               @Part("message") RequestBody message,
                                               @Part("head_one") RequestBody head_one,
                                               @Part("head_two") RequestBody head_two,
                                               @Part("doc_name") RequestBody doc_name,
                                               @Part MultipartBody.Part image,
                                               @Part("user_id") RequestBody user_id,
                                               @Part("sign") RequestBody sign,
                                               @Part MultipartBody.Part doc0,
                                               @Part MultipartBody.Part doc1,
                                               @Part MultipartBody.Part doc2,
                                               @Part MultipartBody.Part doc3,
                                               @Part MultipartBody.Part doc4);

    @Multipart
    @POST("MailToMultipleListIOS_Shell.php")
    Call<JsonObject> mailToMultipleListIOSShellRequest(@Part("list_id") RequestBody list_id,
                                                       @Part("list_ids[]") ArrayList<String> items,
                                                       @Part("subject") RequestBody subject,
                                                       @Part("message") RequestBody message,
                                                       @Part("head_one") RequestBody head_one,
                                                       @Part("head_two") RequestBody head_two,
                                                       @Part("doc_name") RequestBody doc_name,
                                                       @Part MultipartBody.Part image,
                                                       @Part("user_id") RequestBody user_id,
                                                       @Part("sign") RequestBody sign,
                                                       @Part MultipartBody.Part doc0,
                                                       @Part MultipartBody.Part doc1,
                                                       @Part MultipartBody.Part doc2,
                                                       @Part MultipartBody.Part doc3,
                                                       @Part MultipartBody.Part doc4);


    @Multipart
    @POST("EditFiveDocumentsAndroid.php")
    Call<StatusMsgModel> editUploadDocumentRequest(@Part("record_id") RequestBody record_id,
                                                   @Part MultipartBody.Part Document1,
                                                   @Part MultipartBody.Part Document2,
                                                   @Part MultipartBody.Part Document3,
                                                   @Part MultipartBody.Part Document4,
                                                   @Part MultipartBody.Part Document5,
                                                   @Part MultipartBody.Part Document6,
                                                   @Part MultipartBody.Part Document7,
                                                   @Part MultipartBody.Part Document8,
                                                   @Part MultipartBody.Part Document9,
                                                   @Part MultipartBody.Part Document10,
                                                   @Part("doc1") RequestBody doc1,
                                                   @Part("doc2") RequestBody doc2,
                                                   @Part("doc3") RequestBody doc3,
                                                   @Part("doc4") RequestBody doc4,
                                                   @Part("doc5") RequestBody doc5,
                                                   @Part("doc6") RequestBody doc6,
                                                   @Part("doc7") RequestBody doc7,
                                                   @Part("doc8") RequestBody doc8,
                                                   @Part("doc9") RequestBody doc9,
                                                   @Part("doc10") RequestBody doc10);

    @Multipart
    @POST("AddVesselAndroidv2.php")
    Call<AddVesselmodel> addVesselsRequest(@Part("vessel_name") RequestBody vessel_name,
                                           @Part("vessel_IMO_no") RequestBody vessel_IMO_no,
                                           @Part("vessel_type") RequestBody vessel_type,
                                           @Part("vessel_built_year") RequestBody vessel_built_year,
                                           @Part("vessel_place_built") RequestBody vessel_place_built,
                                           @Part("vessel_class") RequestBody vessel_class,
                                           @Part("vessel_flag") RequestBody vessel_flag,
                                           @Part("vessel_price_idea") RequestBody vessel_price_idea,
                                           @Part("vessel_short_description") RequestBody vessel_short_description,
                                           @Part("vessel_DWT") RequestBody vessel_DWT,
                                           @Part("vessel_loa") RequestBody vessel_loa,
                                           @Part("vessel_breadth") RequestBody vessel_breadth,
                                           @Part("vessel_depth") RequestBody vessel_depth,
                                           @Part("vessel_location") RequestBody vessel_location,
                                           @Part("vessel_currency") RequestBody vessel_currency,
                                           @Part("vessel_long_description") RequestBody vessel_long_description,
                                           @Part("vessel_status") RequestBody vessel_status,
                                           @Part("bale") RequestBody bale,
                                           @Part("grain") RequestBody grain,
                                           @Part("date") RequestBody date,
                                           @Part("teu") RequestBody teu,
                                           @Part("no_passengers") RequestBody no_passengers,
                                           @Part("liquid") RequestBody liquid,
                                           @Part("builder") RequestBody builder,
                                           @Part("gross_tonnage") RequestBody gross_tonnage,
                                           @Part("net_tonnage") RequestBody net_tonnage,
                                           @Part("gas") RequestBody gas,
                                           @Part("draught") RequestBody draught,
                                           @Part("url") RequestBody url,
                                           @Part("owner_detail") RequestBody owner_detail,
                                           @Part("machinery_detail") RequestBody machinery_detail,
                                           @Part("remarks") RequestBody remarks,
                                           @Part("ldt") RequestBody ldt,
                                           @Part("tcm") RequestBody tcm,
                                           @Part("geared") RequestBody geared,
                                           @Part MultipartBody.Part photo1,
                                           @Part MultipartBody.Part photo2,
                                           @Part MultipartBody.Part photo3,
                                           @Part MultipartBody.Part photo4,
                                           @Part MultipartBody.Part photo5,
                                           @Part MultipartBody.Part photo6,
                                           @Part MultipartBody.Part photo7,
                                           @Part MultipartBody.Part photo8,
                                           @Part MultipartBody.Part photo9,
                                           @Part MultipartBody.Part photo10,
                                           @Part("user_id") RequestBody user_id,
                                           @Part("mmsi_no") RequestBody mmsi_no,
                                           @Part("off_market") RequestBody off_market);

    @Multipart
    @POST("EditVesselAndroidv2.php")
    Call<AddVesselmodel> editVesselsRequest(@Part("vessel_id") RequestBody vessel_id,
                                            @Part("vessel_name") RequestBody vessel_name,
                                            @Part("vessel_IMO_no") RequestBody vessel_IMO_no,
                                            @Part("vessel_type") RequestBody vessel_type,
                                            @Part("vessel_built_year") RequestBody vessel_built_year,
                                            @Part("vessel_place_built") RequestBody vessel_place_built,
                                            @Part("vessel_class") RequestBody vessel_class,
                                            @Part("vessel_flag") RequestBody vessel_flag,
                                            @Part("vessel_price_idea") RequestBody vessel_price_idea,
                                            @Part("vessel_short_description") RequestBody vessel_short_description,
                                            @Part("vessel_DWT") RequestBody vessel_DWT,
                                            @Part("vessel_loa") RequestBody vessel_loa,
                                            @Part("vessel_breadth") RequestBody vessel_breadth,
                                            @Part("vessel_depth") RequestBody vessel_depth,
                                            @Part("vessel_location") RequestBody vessel_location,
                                            @Part("vessel_currency") RequestBody vessel_currency,
                                            @Part("vessel_long_description") RequestBody vessel_long_description,
                                            @Part("vessel_status") RequestBody vessel_status,
                                            @Part("bale") RequestBody bale,
                                            @Part("grain") RequestBody grain,
                                            @Part("date") RequestBody date,
                                            @Part("teu") RequestBody teu,
                                            @Part("no_passengers") RequestBody no_passengers,
                                            @Part("liquid") RequestBody liquid,
                                            @Part("builder") RequestBody builder,
                                            @Part("gross_tonnage") RequestBody gross_tonnage,
                                            @Part("net_tonnage") RequestBody net_tonnage,
                                            @Part("gas") RequestBody gas,
                                            @Part("draught") RequestBody draught,
                                            @Part("url") RequestBody url,
                                            @Part("owner_detail") RequestBody owner_detail,
                                            @Part("machinery_detail") RequestBody machinery_detail,
                                            @Part("remarks") RequestBody remarks,
                                            @Part("ldt") RequestBody ldt,
                                            @Part("tcm") RequestBody tcm,
                                            @Part("geared") RequestBody geared,
                                            @Part MultipartBody.Part photo1,
                                            @Part MultipartBody.Part photo2,
                                            @Part MultipartBody.Part photo3,
                                            @Part MultipartBody.Part photo4,
                                            @Part MultipartBody.Part photo5,
                                            @Part MultipartBody.Part photo6,
                                            @Part MultipartBody.Part photo7,
                                            @Part MultipartBody.Part photo8,
                                            @Part MultipartBody.Part photo9,
                                            @Part MultipartBody.Part photo10,
                                            @Part("user_id") RequestBody user_id,
                                            @Part("mmsi_no") RequestBody mmsi_no,
                                            @Part("off_market") RequestBody off_market,
                                            @Part("pic1") RequestBody pic1,
                                            @Part("pic2") RequestBody pic2,
                                            @Part("pic3") RequestBody pic3,
                                            @Part("pic4") RequestBody pic4,
                                            @Part("pic5") RequestBody pic5,
                                            @Part("pic6") RequestBody pic6,
                                            @Part("pic7") RequestBody pic7,
                                            @Part("pic8") RequestBody pic8,
                                            @Part("pic9") RequestBody pic9,
                                            @Part("pic10") RequestBody pic10
    );

    @Multipart
    @POST("AddChatMessageNew.php")
    Call<JsonObject> addChatMessageRequest(@Part("message") RequestBody message,
                                           @Part("receiver_id") RequestBody receiver_id,
                                           @Part("room_id") RequestBody room_id,
                                           @Part("group_id") RequestBody group_id,
                                           @Part("user_id") RequestBody user_id,
                                           @Part MultipartBody.Part photo1,
                                           @Part MultipartBody.Part photo2,
                                           @Part MultipartBody.Part photo3,
                                           @Part MultipartBody.Part photo4,
                                           @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddChatMessageNew.php")
    Call<JsonObject> replyChatMessageRequest(@Part("message") RequestBody message,
                                             @Part("receiver_id") RequestBody receiver_id,
                                             @Part("room_id") RequestBody room_id,
                                             @Part("reply_id") RequestBody reply_id,
                                             @Part("group_id") RequestBody group_id,
                                             @Part("user_id") RequestBody user_id,
                                             @Part MultipartBody.Part photo1,
                                             @Part MultipartBody.Part photo2,
                                             @Part MultipartBody.Part photo3,
                                             @Part MultipartBody.Part photo4,
                                             @Part MultipartBody.Part photo5);

    @Multipart
    @POST("EditChatMessageNew.php")
    Call<JsonObject> editChatMessageRequest(@Part("message") RequestBody message,
                                            @Part("message_id") RequestBody message_id,
                                            @Part("user_id") RequestBody user_id,
                                            @Part MultipartBody.Part photo1,
                                            @Part MultipartBody.Part photo2,
                                            @Part MultipartBody.Part photo3,
                                            @Part MultipartBody.Part photo4,
                                            @Part MultipartBody.Part photo5,
                                            @Part MultipartBody.Part photo6,
                                            @Part MultipartBody.Part photo7,
                                            @Part MultipartBody.Part photo8,
                                            @Part MultipartBody.Part photo9,
                                            @Part MultipartBody.Part photo10);

    @Multipart
    @POST("AddStaffForumMessageNew.php")
    Call<JsonObject> addStaffForumMessageRequest(@Part("message") RequestBody message,
                                                 @Part("forum_id") RequestBody forum_id,
                                                 @Part("user_id") RequestBody user_id,
                                                 @Part MultipartBody.Part photo1,
                                                 @Part MultipartBody.Part photo2,
                                                 @Part MultipartBody.Part photo3,
                                                 @Part MultipartBody.Part photo4,
                                                 @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddStaffForumMessageNew.php")
    Call<JsonObject> replyStaffForumMessageRequest(@Part("message") RequestBody message,
                                                   @Part("forum_id") RequestBody forum_id,
                                                   @Part("reply_id") RequestBody reply_id,
                                                   @Part("user_id") RequestBody user_id,
                                                   @Part MultipartBody.Part photo1,
                                                   @Part MultipartBody.Part photo2,
                                                   @Part MultipartBody.Part photo3,
                                                   @Part MultipartBody.Part photo4,
                                                   @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddForumMessageNew.php")
    Call<JsonObject> addForumMessageRequest(@Part("message") RequestBody message,
                                            @Part("forum_id") RequestBody forum_id,
                                            @Part("user_id") RequestBody user_id,
                                            @Part MultipartBody.Part photo1,
                                            @Part MultipartBody.Part photo2,
                                            @Part MultipartBody.Part photo3,
                                            @Part MultipartBody.Part photo4,
                                            @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddStaffForumMessageAndroid.php")
    Call<JsonObject> addForumMessageAndroidRequest(@Part("message") RequestBody message,
                                                   @Part("forum_id") RequestBody forum_id,
                                                   @Part("user_id") RequestBody user_id,
                                                   @Part MultipartBody.Part photo1,
                                                   @Part MultipartBody.Part photo2,
                                                   @Part MultipartBody.Part photo3,
                                                   @Part MultipartBody.Part photo4,
                                                   @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddForumMessageNew.php")
    Call<JsonObject> replyForumMessageRequest(@Part("message") RequestBody message,
                                              @Part("forum_id") RequestBody forum_id,
                                              @Part("reply_id") RequestBody reply_id,
                                              @Part("user_id") RequestBody user_id,
                                              @Part MultipartBody.Part photo1,
                                              @Part MultipartBody.Part photo2,
                                              @Part MultipartBody.Part photo3,
                                              @Part MultipartBody.Part photo4,
                                              @Part MultipartBody.Part photo5);

    @Multipart
    @POST("EditStaffForumMessageNew.php")
    Call<JsonObject> editStaffForumMessageRequest(@Part("message") RequestBody message,
                                                  @Part("message_id") RequestBody message_id,
                                                  @Part("user_id") RequestBody user_id,
                                                  @Part MultipartBody.Part photo1,
                                                  @Part MultipartBody.Part photo2,
                                                  @Part MultipartBody.Part photo3,
                                                  @Part MultipartBody.Part photo4,
                                                  @Part MultipartBody.Part photo5);

    @Multipart
    @POST("EditForumMessageNew.php")
    Call<JsonObject> editForumMessageRequest(@Part("message") RequestBody message,
                                             @Part("message_id") RequestBody message_id,
                                             @Part("user_id") RequestBody user_id,
                                             @Part MultipartBody.Part photo1,
                                             @Part MultipartBody.Part photo2,
                                             @Part MultipartBody.Part photo3,
                                             @Part MultipartBody.Part photo4,
                                             @Part MultipartBody.Part photo5);

    @FormUrlEncoded
    @POST("GetAllForums.php")
    Call<GetAllForumsModel> getForumListStaffNew(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no
    );

//    @Multipart
//    @POST("UploadThirtyImages.php")
//    Call<SignUpModel> uploadThirtyImagesRequest(@Part("record_id") RequestBody record_id,
//                                                 @Part("name") RequestBody name,
//                                                 @Part("bio") RequestBody bio,
//                                                 @Part MultipartBody.Part profileImage);

    @Multipart
    @POST("AddChatMessageNewAndroid.php")
    Call<AddNewMessageModel> addChatMessageRequestNew(@Part("message") RequestBody message,
                                                      @Part("receiver_id") RequestBody receiver_id,
                                                      @Part("room_id") RequestBody room_id,
                                                      @Part("group_id") RequestBody group_id,
                                                      @Part("user_id") RequestBody user_id,
                                                      @Part MultipartBody.Part photo1,
                                                      @Part MultipartBody.Part photo2,
                                                      @Part MultipartBody.Part photo3,
                                                      @Part MultipartBody.Part photo4,
                                                      @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddChatMessageNewAndroid.php")
    Call<AddNewMessageModel> replyChatMessageRequestNew(@Part("message") RequestBody message,
                                                        @Part("receiver_id") RequestBody receiver_id,
                                                        @Part("room_id") RequestBody room_id,
                                                        @Part("reply_id") RequestBody reply_id,
                                                        @Part("group_id") RequestBody group_id,
                                                        @Part("user_id") RequestBody user_id,
                                                        @Part MultipartBody.Part photo1,
                                                        @Part MultipartBody.Part photo2,
                                                        @Part MultipartBody.Part photo3,
                                                        @Part MultipartBody.Part photo4,
                                                        @Part MultipartBody.Part photo5);

    @Multipart
    @POST("EditChatMessageNewAndroid.php")
    Call<AddNewMessageModel> editChatMessageRequestNew(@Part("message") RequestBody message,
                                                       @Part("message_id") RequestBody message_id,
                                                       @Part("user_id") RequestBody user_id,
                                                       @Part MultipartBody.Part photo1,
                                                       @Part MultipartBody.Part photo2,
                                                       @Part MultipartBody.Part photo3,
                                                       @Part MultipartBody.Part photo4,
                                                       @Part MultipartBody.Part photo5,
                                                       @Part MultipartBody.Part photo6,
                                                       @Part MultipartBody.Part photo7,
                                                       @Part MultipartBody.Part photo8,
                                                       @Part MultipartBody.Part photo9,
                                                       @Part MultipartBody.Part photo10);

    @FormUrlEncoded
    @POST("GetAllChatMessagesAndroid.php")
    Call<GetAllChatMessagesModel> getAllChatMessagesAndroid(@Field("user_id") String user_id,
                                                            @Field("room_id") String room_id,
                                                            @Field("page_num") int page_num);

    @FormUrlEncoded
    @POST("DeleteChatMessage.php")
    Call<DeleteMessageModel> deleteChatMessageRequestNew(
            @Field("user_id") String user_id,
            @Field("message_id") String message_id);

    @FormUrlEncoded
    @POST("ResetChatRooms.php")
    Call<JsonObject> ResetChatRoomsRequest(
            @Field("user_id") String user_id,
            @Field("page_no") int page_no);

    @Multipart
    @POST("AddStaffForumMessageNewAndroid.php")
    Call<AddStaffForumMessageModel> addStaffForumMessageRequestNew(@Part("message") RequestBody message,
                                                                   @Part("forum_id") RequestBody forum_id,
                                                                   @Part("user_id") RequestBody user_id,
                                                                   @Part MultipartBody.Part photo1,
                                                                   @Part MultipartBody.Part photo2,
                                                                   @Part MultipartBody.Part photo3,
                                                                   @Part MultipartBody.Part photo4,
                                                                   @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddStaffForumMessageNewAndroid.php")
    Call<AddStaffForumMessageModel> replyStaffForumMessageRequestNew(@Part("message") RequestBody message,
                                                                     @Part("forum_id") RequestBody forum_id,
                                                                     @Part("reply_id") RequestBody reply_id,
                                                                     @Part("user_id") RequestBody user_id,
                                                                     @Part MultipartBody.Part photo1,
                                                                     @Part MultipartBody.Part photo2,
                                                                     @Part MultipartBody.Part photo3,
                                                                     @Part MultipartBody.Part photo4,
                                                                     @Part MultipartBody.Part photo5);

    @Multipart
    @POST("EditStaffForumMessageNewAndroid.php")
    Call<AddStaffForumMessageModel> editStaffForumMessageRequestNew(@Part("message") RequestBody message,
                                                                    @Part("message_id") RequestBody message_id,
                                                                    @Part("user_id") RequestBody user_id,
                                                                    @Part MultipartBody.Part photo1,
                                                                    @Part MultipartBody.Part photo2,
                                                                    @Part MultipartBody.Part photo3,
                                                                    @Part MultipartBody.Part photo4,
                                                                    @Part MultipartBody.Part photo5);

    @FormUrlEncoded
    @POST("GetAllStaffForumMessagesAndroid.php")
    Call<GetAllForumMessagesModel> getAllStaffForumMessagesNew(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("page_no") String page_no);



    @FormUrlEncoded
    @POST("DeleteStaffForumMessage.php")
    Call<DeleteStaffForumMessageModel> deleteStaffForumMessageNew(
            @Field("user_id") String user_id,
            @Field("message_id") String message_id);

    @FormUrlEncoded
    @POST("DeleteForumMessage.php")
    Call<DeleteStaffForumMessageModel> deleteForumMessageNew(
            @Field("user_id") String user_id,
            @Field("message_id") String message_id
    );


    @Multipart
    @POST("AddForumMessageNewAndroid.php")
    Call<AddStaffForumMessageModel> addForumMessageRequestNew(@Part("message") RequestBody message,
                                                              @Part("forum_id") RequestBody forum_id,
                                                              @Part("user_id") RequestBody user_id,
                                                              @Part MultipartBody.Part photo1,
                                                              @Part MultipartBody.Part photo2,
                                                              @Part MultipartBody.Part photo3,
                                                              @Part MultipartBody.Part photo4,
                                                              @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddForumMessageNewAndroid.php")
    Call<AddStaffForumMessageModel> replyForumMessageRequestNew(@Part("message") RequestBody message,
                                                                @Part("forum_id") RequestBody forum_id,
                                                                @Part("reply_id") RequestBody reply_id,
                                                                @Part("user_id") RequestBody user_id,
                                                                @Part MultipartBody.Part photo1,
                                                                @Part MultipartBody.Part photo2,
                                                                @Part MultipartBody.Part photo3,
                                                                @Part MultipartBody.Part photo4,
                                                                @Part MultipartBody.Part photo5);

    @Multipart
    @POST("AddForumMessageNewAndroid.php")
    Call<AddStaffForumMessageModel> editForumMessageRequestNew(@Part("message") RequestBody message,
                                                               @Part("message_id") RequestBody message_id,
                                                               @Part("user_id") RequestBody user_id,
                                                               @Part MultipartBody.Part photo1,
                                                               @Part MultipartBody.Part photo2,
                                                               @Part MultipartBody.Part photo3,
                                                               @Part MultipartBody.Part photo4,
                                                               @Part MultipartBody.Part photo5);

    @FormUrlEncoded
    @POST("GetAllForumMessagesAndroid.php")
    Call<GetAllForumMessagesModel> getAllForumListNew(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("page_no") String page_no
    );

    @FormUrlEncoded
    @POST("UpdateForumHeading.php")
    Call<UpdateForumHeadingModel> updateForumUserHeadingRequestNew(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("heading") String heading);

    @FormUrlEncoded
    @POST("UpdateStaffForumHeading.php")
    Call<UpdateForumHeadingModel> updateForumHeadingRequestNew(
            @Field("user_id") String user_id,
            @Field("forum_id") String forum_id,
            @Field("heading") String heading);

    @FormUrlEncoded
    @POST("UpdateNotificationReadStatus.php")
    Call<UpdateForumHeadingModel> UpdateNotificationReadStatusRequest(@Field("user_id") String user_id);

    @Multipart
    @POST("CopyVesselAndroid.php")
    Call<AddVesselmodel> CopyVesselRequest(@Part("vessel_id") RequestBody vessel_id,
                                           @Part("vessel_name") RequestBody vessel_name,
                                           @Part("vessel_IMO_no") RequestBody vessel_IMO_no,
                                           @Part("vessel_type") RequestBody vessel_type,
                                           @Part("vessel_built_year") RequestBody vessel_built_year,
                                           @Part("vessel_place_built") RequestBody vessel_place_built,
                                           @Part("vessel_class") RequestBody vessel_class,
                                           @Part("vessel_flag") RequestBody vessel_flag,
                                           @Part("vessel_price_idea") RequestBody vessel_price_idea,
                                           @Part("vessel_short_description") RequestBody vessel_short_description,
                                           @Part("vessel_DWT") RequestBody vessel_DWT,
                                           @Part("vessel_loa") RequestBody vessel_loa,
                                           @Part("vessel_breadth") RequestBody vessel_breadth,
                                           @Part("vessel_depth") RequestBody vessel_depth,
                                           @Part("vessel_location") RequestBody vessel_location,
                                           @Part("vessel_currency") RequestBody vessel_currency,
                                           @Part("vessel_long_description") RequestBody vessel_long_description,
                                           @Part("vessel_status") RequestBody vessel_status,
                                           @Part("bale") RequestBody bale,
                                           @Part("grain") RequestBody grain,
                                           @Part("date") RequestBody date,
                                           @Part("teu") RequestBody teu,
                                           @Part("no_passengers") RequestBody no_passengers,
                                           @Part("liquid") RequestBody liquid,
                                           @Part("builder") RequestBody builder,
                                           @Part("gross_tonnage") RequestBody gross_tonnage,
                                           @Part("net_tonnage") RequestBody net_tonnage,
                                           @Part("gas") RequestBody gas,
                                           @Part("draught") RequestBody draught,
                                           @Part("url") RequestBody url,
                                           @Part("owner_detail") RequestBody owner_detail,
                                           @Part("machinery_detail") RequestBody machinery_detail,
                                           @Part("remarks") RequestBody remarks,
                                           @Part("ldt") RequestBody ldt,
                                           @Part("tcm") RequestBody tcm,
                                           @Part("geared") RequestBody geared,
                                           @Part MultipartBody.Part photo1,
                                           @Part MultipartBody.Part photo2,
                                           @Part MultipartBody.Part photo3,
                                           @Part MultipartBody.Part photo4,
                                           @Part MultipartBody.Part photo5,
                                           @Part MultipartBody.Part photo6,
                                           @Part MultipartBody.Part photo7,
                                           @Part MultipartBody.Part photo8,
                                           @Part MultipartBody.Part photo9,
                                           @Part MultipartBody.Part photo10,
                                           @Part("user_id") RequestBody user_id,
                                           @Part("mmsi_no") RequestBody mmsi_no,
                                           @Part("off_market") RequestBody off_market,
                                           @Part("pic1") RequestBody pic1,
                                           @Part("pic2") RequestBody pic2,
                                           @Part("pic3") RequestBody pic3,
                                           @Part("pic4") RequestBody pic4,
                                           @Part("pic5") RequestBody pic5,
                                           @Part("pic6") RequestBody pic6,
                                           @Part("pic7") RequestBody pic7,
                                           @Part("pic8") RequestBody pic8,
                                           @Part("pic9") RequestBody pic9,
                                           @Part("pic10") RequestBody pic10
    );

    @FormUrlEncoded
    @POST("GetUserProfile.php")
    Call<ProfileResponse> getUserProfileRequest(
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("GetAllInvoiceDraftAndroid.php")
    Call<AllInvoicesDrafts> GetAllInvoiceDraftNew(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("GetAllInvoiceDraftAndroid.php")
    Call<JsonObject> GetAllInvoiceDraft(
            @Field("user_id") String user_id,
            @Field("page_no") String page_no);


    @Multipart
    @POST("AddPendingPayment.php")
    Call<JsonObject> AddPendingPaymentDocs(
            @Part("pp_no") RequestBody pp_no,
            @Part("pp_date") RequestBody pp_date,
            @Part("pp_company") RequestBody pp_company,
            @Part("pp_currency") RequestBody pp_currency,
            @Part("pp_vessel1") RequestBody pp_vessel1,
            @Part("pp_vessel2") RequestBody pp_vessel2,
            @Part("pp_vessel3") RequestBody pp_vessel3,
            @Part("pp_amt_due") RequestBody pp_amt_due,
            @Part("pp_status") RequestBody pp_status,
            @Part("pp_remarks") RequestBody pp_remarks,
            @Part("user_id") RequestBody user_id,
            @Part MultipartBody.Part doc0,
            @Part MultipartBody.Part doc1,
            @Part MultipartBody.Part doc2,
            @Part MultipartBody.Part doc3,
            @Part MultipartBody.Part doc4
    );

    @Multipart
    @POST("EditPendingPayment.php")
    Call<JsonObject> EditPendingPaymentDocs(
            @Part("pp_no") RequestBody pp_no,
            @Part("pp_date") RequestBody pp_date,
            @Part("pp_company") RequestBody pp_company,
            @Part("pp_currency") RequestBody pp_currency,
            @Part("pp_vessel1") RequestBody pp_vessel1,
            @Part("pp_vessel2") RequestBody pp_vessel2,
            @Part("pp_vessel3") RequestBody pp_vessel3,
            @Part("pp_amt_due") RequestBody pp_amt_due,
            @Part("pp_status") RequestBody pp_status,
            @Part("pp_remarks") RequestBody pp_remarks,
            @Part("user_id") RequestBody user_id,
            @Part("pp_id") RequestBody pp_id,
            @Part("old_docs") RequestBody old_docs,
            @Part MultipartBody.Part doc0,
            @Part MultipartBody.Part doc1,
            @Part MultipartBody.Part doc2,
            @Part MultipartBody.Part doc3,
            @Part MultipartBody.Part doc4
    );

}
