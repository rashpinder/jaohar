package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;

/**
 * Created by Dharmani Apps on 2/15/2018.
 */

public interface InvoiceVesselDeleteInterface {
    public void invoiceVesselDelete(VesselSearchInvoiceModel mModel, int position);
}
