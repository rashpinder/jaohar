package jaohar.com.jaohar.interfaces.modules;

import jaohar.com.jaohar.models.GetAllAdminModuleModel;

public interface StaffModuleBottomInterface {
    void mStaffModules (GetAllAdminModuleModel.Data.AllModule mModel);
}
