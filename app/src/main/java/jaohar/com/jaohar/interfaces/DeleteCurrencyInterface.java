package jaohar.com.jaohar.interfaces;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.models.CurrencyData;


/**
 * Created by Dharmani Apps on 2/14/2018.
 */

public interface DeleteCurrencyInterface {
    public void deleteCurrency(CurrenciesModel mModel, int position);
}
