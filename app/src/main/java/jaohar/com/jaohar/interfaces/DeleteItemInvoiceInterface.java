package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.InvoiceAddItemModel;

/**
 * Created by Dharmani Apps on 2/3/2018.
 */

public interface DeleteItemInvoiceInterface {
    public void deleteItemInvoice(InvoiceAddItemModel model);
}
