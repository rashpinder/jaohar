package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.models.CurrencyData;
import jaohar.com.jaohar.models.CurrencyModel;

/**
 * Created by Dharmani Apps on 2/14/2018.
 */

public interface EditCurrencyInterface {
    public void editCurrency(CurrenciesModel mModel);
}
