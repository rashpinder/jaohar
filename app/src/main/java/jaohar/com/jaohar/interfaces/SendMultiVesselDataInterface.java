package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.models.AllVessel;

/**
 * Created by Dharmani Apps on 2/19/2018.
 */

public interface SendMultiVesselDataInterface {
//    public void sendMultipleVesselsData(VesselesModel mVesselesModel, boolean b);
    public void sendMultipleVesselsData(AllVessel mVesselesModel, boolean b);
}
