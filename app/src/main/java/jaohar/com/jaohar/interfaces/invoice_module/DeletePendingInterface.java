package jaohar.com.jaohar.interfaces.invoice_module;

import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.models.pendingPayments.AllInvoice;

public interface DeletePendingInterface {
 public void deleteInvoice(AllInvoice mInVoicesModel);
}
