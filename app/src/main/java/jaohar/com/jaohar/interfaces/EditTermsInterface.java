package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.models.GetUniversalTermsModel;

public interface EditTermsInterface {
    public void editTermsDetails(GetUniversalTermsModel mTermsModel);
}
