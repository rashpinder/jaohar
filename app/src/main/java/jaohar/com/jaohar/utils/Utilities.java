package jaohar.com.jaohar.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Pattern;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.PriviewModelItems;

public class Utilities {
    public static boolean hideKeyBoad(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    // validating email id
    public static boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getCurrentTimeStamp() {
        String formattedDate = new SimpleDateFormat("ddMMyy").format(Calendar.getInstance().getTime());
        return formattedDate;
    }

    public static String getRandomCharacter() {
        Random rnd = new Random();
        String[] chars = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        String strChar = chars[(int) (Math.random() * 10)];
        return strChar;
    }

    /*
     * Convert Doc to Byte Array
     * @return ByteArray
     * */
    public static byte[] convertDocumentToByteArray(String sourcePath) {
        byte[] byteArray = new byte[0];
        File file = new File(sourcePath);
        try {
            InputStream inputStream = new FileInputStream(file.getPath());
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 8];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArray;


    }



    public static byte[] getAsByteArray(URL url) throws IOException {
        URLConnection connection = url.openConnection();
        // Since you get a URLConnection, use it to get the InputStream
        InputStream in = connection.getInputStream();
        // Now that the InputStream is open, get the content length
        int contentLength = connection.getContentLength();

        // To avoid having to resize the array over and over and over as
        // bytes are written to the array, provide an accurate estimate of
        // the ultimate size of the byte array
        ByteArrayOutputStream tmpOut;
        if (contentLength != -1) {
            tmpOut = new ByteArrayOutputStream(contentLength);
        } else {
            tmpOut = new ByteArrayOutputStream(16384); // Pick some appropriate size
        }

        byte[] buf = new byte[512];
        while (true) {
            int len = in.read(buf);
            if (len == -1) {
                break;
            }
            tmpOut.write(buf, 0, len);
        }
        in.close();
        tmpOut.close(); // No effect, but good to do anyway to keep the metaphor alive

        byte[] array = tmpOut.toByteArray();

        //Lines below used to test if file is corrupt
        //FileOutputStream fos = new FileOutputStream("C:\\abc.pdf");
        //fos.write(array);
        //fos.close();

        return array;
    }



    // Converting File to Base64.encode String type using Method
    public static String convertDocumentToBase64(File f) {
        InputStream inputStream = null;
        String encodedFile = "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile = output.toString();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;

        return lastVal;
    }


    public static String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }

    public static String getRoleWithFormat(String strRole) {
        String strActualRole = "";
        if (strRole.equals("User")) {
            strActualRole = "user";
        } else if (strRole.equals("Staff")) {
            strActualRole = "staff";
        } else if (strRole.equals("Admin")) {
            strActualRole = "admin";
        }

        return strActualRole;
    }


    //create bitmap from the ScrollView
    public static Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }

    //Getting Month Name From the Number
    public static String getMonthNameFromNumber(String strNumber) {
        if (strNumber.equals("01"))
            return "January";
        else if (strNumber.equals("02"))
            return "February";
        else if (strNumber.equals("03"))
            return "March";
        else if (strNumber.equals("04"))
            return "April";
        else if (strNumber.equals("05"))
            return "May";
        else if (strNumber.equals("06"))
            return "June";
        else if (strNumber.equals("07"))
            return "July";
        else if (strNumber.equals("08"))
            return "August";
        else if (strNumber.equals("09"))
            return "September";
        else if (strNumber.equals("10"))
            return "October";
        else if (strNumber.equals("11"))
            return "November";
        else if (strNumber.equals("12"))
            return "December";

        return null;
    }


    //Add Days in the Date and getting New Date:
    public static String getNewDateFormatWhenAddDays(String strStartDate, int numofDays) {
        String strNewDateFormat = "";
//        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
//        Calendar c = Calendar.getInstance();
//        try {
//            c.setTime(sdf.parse(strStartDate));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        String strDAte = String.valueOf(c.getTime());
//        c.add(Calendar.DATE, numofDays);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
//        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMMM yyyy");
//        strNewDateFormat = sdf1.format(c.getTime());
//        String dateInString = "2011-11-20";  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(strStartDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, numofDays);
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date resultdate = new Date(c.getTimeInMillis());
        strNewDateFormat = sdf.format(resultdate);
        System.out.println("String date:" + strNewDateFormat);
        return strNewDateFormat;
    }

    //Getting Status Image From Status Text:::INVOICE
    public static int getStatusImage(String strStatus) {
        if (strStatus.equals("Cancelled"))
            return R.drawable.status_cancel;
        else if (strStatus.equals("Copy"))
            return R.drawable.status_copy;
        else if (strStatus.equals("Draft"))
            return R.drawable.status_draft;
        else if (strStatus.equals("Original"))
            return R.drawable.status_original;
        else if (strStatus.equals("OverDue"))
            return R.drawable.status_over;
        else if (strStatus.equals("Paid"))
            return R.drawable.status_paid;
        else if (strStatus.equals("Urgent"))
            return R.drawable.status_urgent;


        return 0;
    }

    public static String getDateFromUTCTimestamp(long mTimestamp, String mDateFormate) {
        String date = null;
        try {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            cal.setTimeInMillis(mTimestamp * 1000L);
            date = DateFormat.format(mDateFormate, cal.getTimeInMillis()).toString();

            SimpleDateFormat formatter = new SimpleDateFormat(mDateFormate);
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(date);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(mDateFormate);
            dateFormatter.setTimeZone(TimeZone.getDefault());
            date = dateFormatter.format(value);
            return date;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    //Convert ByteArray to Base64:
    public static String convertByteArrayToBase64(byte[] imgByteArray) {
        return Base64.encodeToString(imgByteArray, Base64.DEFAULT);
    }

    public static Bitmap getScaledBitMapBaseOnScreenSize(Bitmap bitmapOriginal, Activity context) {

        Bitmap scaledBitmap = null;
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);


            int width = bitmapOriginal.getWidth();
            int height = bitmapOriginal.getHeight();

            float scaleWidth = metrics.scaledDensity;
            float scaleHeight = metrics.scaledDensity;

            // create a matrix for the manipulation
            Matrix matrix = new Matrix();
            // resize the bit map
            matrix.postScale(scaleWidth, scaleHeight);

            // recreate the new Bitmap
            scaledBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scaledBitmap;
    }

    //Getting 10 Item Every Time
    public static ArrayList<PriviewModelItems> gettingFormatedItems(ArrayList<PriviewModelItems> mArrayList) {
        try {
            ArrayList<PriviewModelItems> tempArrayList = new ArrayList<PriviewModelItems>();
            if (mArrayList.size() == 1) {
                tempArrayList.clear();
                for (int i = 1; i <= 9; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 2) {
                tempArrayList.clear();
                for (int i = 1; i <= 8; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 3) {
                tempArrayList.clear();
                for (int i = 1; i <= 7; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 4) {
                tempArrayList.clear();
                for (int i = 1; i <= 6; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 5) {
                tempArrayList.clear();
                for (int i = 1; i <= 5; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 6) {
                tempArrayList.clear();
                for (int i = 1; i <= 4; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 7) {
                tempArrayList.clear();
                for (int i = 1; i <= 3; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 8) {
                tempArrayList.clear();
                for (int i = 1; i <= 2; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 9) {
                tempArrayList.clear();
                for (int i = 1; i <= 1; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 10) {
                tempArrayList.clear();
                for (int i = 1; i <= 0; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }

                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() == 0) {
                tempArrayList.clear();
                for (int i = 1; i <= 10; i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    tempArrayList.add(mModel);
                }
                mArrayList.addAll(tempArrayList);
                return mArrayList;
            } else if (mArrayList.size() > 10) {

                return mArrayList;
            }
//            else if (mArrayList.size() == 4) {
//                tempArrayList.clear();
//                for (int i = 1; i <= 6; i++) {
//                    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
//                    tempArrayList.add(mModel);
//                }
//
//                mArrayList.addAll(tempArrayList);
//                return mArrayList;
//            }
//            else if (mArrayList.size() == 3) {
//                tempArrayList.clear();
//                for (int i = 1; i <= 7; i++) {
//                    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
//                    tempArrayList.add(mModel);
//                }
//
//                mArrayList.addAll(tempArrayList);
//                return mArrayList;
//            }
//            else if (mArrayList.size() == 2) {
//                tempArrayList.clear();
//                for (int i = 1; i <= 8; i++) {
//                    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
//                    tempArrayList.add(mModel);
//                }
//
//                mArrayList.addAll(tempArrayList);
//                return mArrayList;
//            }
//            else if (mArrayList.size() == 1) {
//                tempArrayList.clear();
//                for (int i = 1; i <= 9; i++) {
//                    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
//                    tempArrayList.add(mModel);
//                }
//
//                mArrayList.addAll(tempArrayList);
//                return mArrayList;
//            }
//            else if (mArrayList.size() == 0) {
//                tempArrayList.clear();
//                for (int i = 1; i <= 10; i++) {
//                    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
//                    tempArrayList.add(mModel);
//                }
//                mArrayList.addAll(tempArrayList);
//                return mArrayList;
//            }
//            else if (mArrayList.size() >= 10) {
//                tempArrayList.clear();
//                for (int i = 1; i >= 10; i++) {
//                    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
//                    tempArrayList.add(mModel);
//                }
//                mArrayList.addAll(tempArrayList);
//                return mArrayList;
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public static double getRoundOff2Decimal(double mDoubleValue) {
        double finalValue = 0;
        try {

            finalValue = Math.round(mDoubleValue * 100.0) / 100.0;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalValue;
    }


    public static int getSpinnerPosition(String mItem) {
        if (mItem.equals("Select Discount Type")) {
            return 0;
        } else if (mItem.equals("Add Percentage")) {
            return 1;
        } else if (mItem.equals("Subtract Percentage")) {
            return 2;
        } else if (mItem.equals("Subtract Value")) {
            return 3;
        } else if (mItem.equals("Add Value")) {
            return 4;
        }

        return -1;
    }


    public static Bitmap eraseColor(Bitmap src, int color) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap b = src.copy(Bitmap.Config.ARGB_8888, true);
        b.setHasAlpha(true);

        int[] pixels = new int[width * height];
        src.getPixels(pixels, 0, width, 0, 0, width, height);

        for (int i = 0; i < width * height; i++) {
            if (pixels[i] == color) {
                pixels[i] = 0;
            }
        }

        b.setPixels(pixels, 0, width, 0, 0, width, height);

        return b;
    }


    public static String gettingFormatTime(String strDate) throws ParseException {
        String strActualFormat = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d = format.parse(strDate);
        format = new SimpleDateFormat("dd MMM yyyy ");
        strActualFormat = format.format(d);
        return strActualFormat;
    }

    //Remove E from Double Value
    public static String convertEvalueToNormal(double value) //Got here 6.743240136E7 or something..
    {
        DecimalFormat formatter;

        if (value - (int) value > 0.0)
            formatter = new DecimalFormat("0.00"); // Here you can also deal with rounding if you wish..
        else
            formatter = new DecimalFormat("0");

        return formatter.format(value);
    }

    // Extensions Doc Check method
    public static boolean isDocAdded(String strURL) {
        if (strURL.contains(".pdf") || strURL.contains(".xlsx") || strURL.contains(".xls")
                || strURL.contains(".doc") || strURL.contains(".docx")
                || strURL.contains(".pptx") || strURL.contains(".txt") || strURL.contains(".ppt")) {
            return true;
        } else {
            return false;
        }
    }

    // Extensions Video Check method
    public static boolean isVideoAdded(String strURL) {
        if (strURL.contains(".mp4") || strURL.contains(".avi") || strURL.contains(".flv")
                || strURL.contains(".wmv") || strURL.contains(".mov")
                || strURL.contains(".3gp")) {
            return true;
        } else {
            return false;
        }
    }

    /*
     *  To Change the Status bar
     * */
    public static void setStatusBarTransparent(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }


    /*
     * It's return date  before one week timestamp
     *  like return
     *  1 day ago
     *  2 days ago
     *  5 days ago
     *  21 April 2019
     *
     * */
    public static String getTimeAgoDate(long pastTimeStamp) {

        // for 2 min ago   use  DateUtils.MINUTE_IN_MILLIS
        // for 2 sec ago   use  DateUtils.SECOND_IN_MILLIS
        // for 1 hours ago use  DateUtils.HOUR_IN_MILLIS

        long now = System.currentTimeMillis();

        if (now - pastTimeStamp < 1000) {
            pastTimeStamp = pastTimeStamp + 1000;
        }
        CharSequence ago =
                DateUtils.getRelativeTimeSpanString(pastTimeStamp, now, DateUtils.SECOND_IN_MILLIS);
        return ago.toString();
    }


    /*
     *
     * It's return date  before one week timestamp
     *
     *  like return
     *
     *  1 day ago
     *  2 days ago
     *  5 days ago
     *  2 weeks ago
     *  2 months ago
     *  2 years ago
     *
     *
     * */

    public static String getTimeAgo(long mReferenceTime) {

        long now = System.currentTimeMillis();
        final long diff = now - mReferenceTime;
        if (diff < android.text.format.DateUtils.WEEK_IN_MILLIS) {
            return (diff <= 1000) ?
                    "just now" :
                    android.text.format.DateUtils.getRelativeTimeSpanString(mReferenceTime, now, DateUtils.MINUTE_IN_MILLIS,
                            DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        } else if (diff <= 4 * android.text.format.DateUtils.WEEK_IN_MILLIS) {
            int week = (int) (diff / (android.text.format.DateUtils.WEEK_IN_MILLIS));
            return week > 1 ? week + " weeks ago" : week + " week ago";
        } else if (diff < android.text.format.DateUtils.YEAR_IN_MILLIS) {
            int month = (int) (diff / (4 * android.text.format.DateUtils.WEEK_IN_MILLIS));
            return month > 1 ? month + " months ago" : month + " month ago";
        } else {
            int year = (int) (diff / DateUtils.YEAR_IN_MILLIS);
            return year > 1 ? year + " years ago" : year + " year ago";
        }
    }

    //used to display only time
    public static String timeConverter(String strDate) {
        String strActualFormat = "";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        Date d = null;
        try {
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            d = format.parse(strDate);
            format = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
            format.setTimeZone(TimeZone.getDefault());
            strActualFormat = format.format(d);
            return strActualFormat;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("DATATIME", "DATE " + "NULL" + strActualFormat);
        return strActualFormat;
    }


    /*
     *
     * It's return date  before one week timestamp
     *
     *  like return
     *
     *  1 day ago
     *  2 days ago
     *  5 days ago
     *  2 weeks ago
     *  All months ago
     *  1 years ago
     *
     *
     * */
    public static String parseDate(String givenDateString) {
        if (givenDateString.equalsIgnoreCase("")) {
            return "";
        }


        long timeInMilliseconds = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
        try {

            Date mDate = sdf.parse(timeConverter(givenDateString));
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String result = "now";
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        String todayDate = formatter.format(new Date());
        Calendar calendar = Calendar.getInstance();

        long dayagolong = timeInMilliseconds;
        calendar.setTimeInMillis(dayagolong);
        String agoformater = formatter.format(calendar.getTime());

        Date CurrentDate = null;
        Date CreateDate = null;

        try {
            CurrentDate = formatter.parse(todayDate);
            CreateDate = formatter.parse(agoformater);

//            long different = Math.abs(CurrentDate.getTime() - CreateDate.getTime());
            long different = CurrentDate.getTime() - CreateDate.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            different = different % secondsInMilli;
            if (elapsedDays == 0) {
                if (elapsedHours == 0) {
                    if (elapsedMinutes == 0) {
                        if (elapsedSeconds < 0) {
                            return "0" + " s";
                        } else {
                            if (elapsedDays > 0 && elapsedSeconds < 59) {
                                return "now";
                            }
                        }
                    } else {
                        return String.valueOf(elapsedMinutes) + "mins ago";
                    }
                } else {
                    return String.valueOf(elapsedHours) + "hr ago";
                }

            } else {
                if (elapsedDays <= 29) {
                    return String.valueOf(elapsedDays) + "d ago";

                } else if (elapsedDays > 29 && elapsedDays <= 58) {
                    return "1Mth ago";
                }
                if (elapsedDays > 58 && elapsedDays <= 87) {
                    return "2Mth ago";
                }
                if (elapsedDays > 87 && elapsedDays <= 116) {
                    return "3Mth ago";
                }
                if (elapsedDays > 116 && elapsedDays <= 145) {
                    return "4Mth ago";
                }
                if (elapsedDays > 145 && elapsedDays <= 174) {
                    return "5Mth ago";
                }
                if (elapsedDays > 174 && elapsedDays <= 203) {
                    return "6Mth ago";
                }
                if (elapsedDays > 203 && elapsedDays <= 232) {
                    return "7Mth ago";
                }
                if (elapsedDays > 232 && elapsedDays <= 261) {
                    return "8Mth ago";
                }
                if (elapsedDays > 261 && elapsedDays <= 290) {
                    return "9Mth ago";
                }
                if (elapsedDays > 290 && elapsedDays <= 319) {
                    return "10Mth ago";
                }
                if (elapsedDays > 319 && elapsedDays <= 348) {
                    return "11Mth ago";
                }
                if (elapsedDays > 348 && elapsedDays <= 360) {
                    return "12Mth ago";
                }

                if (elapsedDays > 360 && elapsedDays <= 720) {
                    return "1 year ago";
                }
            }

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    // Custom method for converting DP/DIP value to pixels
    public static int getPixelsFromDPs(Activity mActivity, int dps) {
        Resources r = mActivity.getResources();
        int px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }


}