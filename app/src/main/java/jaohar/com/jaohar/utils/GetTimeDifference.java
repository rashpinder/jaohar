package jaohar.com.jaohar.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GetTimeDifference {
    public static String getTime(String postTime) {
        String returnTime = "";
//        try
//        {


        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;
        final int MONTH_MILLIS = 30 * DAY_MILLIS;
        final int YEAR_MILLIS = 365 * DAY_MILLIS;

        long time = Long.parseLong(postTime);

        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }


        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else  {

            long unix_seconds = Long.parseLong(postTime);

            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(unix_seconds * 1000L);

            DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
            String date = format.format(cal.getTime());
            if((diff / DAY_MILLIS)>30){
                return diff / DAY_MILLIS + " days ago";
            }else {
                return date;
            }

        }


//            Timestamp currentTimestamp;// = new Timestamp(System.currentTimeMillis());
//
//            Timestamp postTimestamp;
//            Date currentDate=null;
//            java.util.Date postDate= new java.util.Date((Long.parseLong(postTime)*1000));
//
//
//            //java.util.Date currentDate= new java.util.Date((System.currentTimeMillis())*1000);
//
//
//            Long difference= System.currentTimeMillis()- Long.parseLong(postTime);
//            currentTimestamp= new Timestamp(System.currentTimeMillis());
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            currentDate=format.parse(currentTimestamp.toString());
//
//            long diff = currentDate.getTime() - postDate.getTime();
//            long diffSeconds = diff / 1000 % 60;
//            long diffMinutes = diff / (60 * 1000) % 60;
//            long diffHours = diff / (60 * 60 * 1000);
//            long diffyears = diff / 31536000;
//            int diffInDays = (int) ((currentDate.getTime() - postDate.getTime()) / (1000 * 60 * 60 * 24));
//
//
//            System.out.println("OUT PUT=");
//            System.out.println("OUT PUT DIFF====="+diff);
//            System.out.println("OUT PUT SECOND===="+diffSeconds);
//            System.out.println("OUT PUT diffMinutes===="+diffMinutes);
//            System.out.println("OUT PUT diffHours====="+diffHours);
//            System.out.println("OUT PUT DIFFDIFF====="+diffInDays);
//            System.out.println("OUT PUT DIFFDIFFYear====="+diffyears);
//
//            if (diffInDays >= 1) {
//
//                if(diffInDays == 1)
//                {
//                    returnTime=diffInDays +" Day ago";
//                }
//                else
//                {
//                    returnTime=diffInDays +" Days ago";
//                }
//
//
//
//                return returnTime;
//            }
//            else if(diffHours>=1 && diffHours<=24)
//            {
//
//                if(diffHours==1)
//                {
//                    returnTime=diffHours+" Hour ago";
//                }
//                else
//                {
//                    returnTime=diffHours+" Hours ago";
//                }
//
//            }
//            else if(diffMinutes >= 1 && diffMinutes <=60)
//            {
//
//                if(diffMinutes==1)
//                {
//                    returnTime=diffMinutes+" Minute ago";
//                }
//                else
//                {
//                    returnTime=diffMinutes+" Minutes ago";
//                }
//
//            }
//            else if(diffSeconds >= 1 && diffSeconds <=60)
//            {
//
//                if(diffSeconds==1)
//                {
//                    returnTime=diffSeconds+" Second ago";
//                }
//                else
//                {
//                    returnTime=diffSeconds+" Seconds ago";
//                }
//
//            }
//
//
//
//
//
//        }
//        catch(Exception ex)
//        {
//            System.out.println("ERROR======"+ex);
//        }

        /*System.out.println("OUTPUT=======*******POST" +postDate);
        System.out.println("OUTPUT=======*******CURRENT" + currentDate);

        System.out.println("Difference==="+difference);*/


    }
}
