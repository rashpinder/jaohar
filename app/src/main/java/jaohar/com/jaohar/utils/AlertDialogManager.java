package jaohar.com.jaohar.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aigestudio.wheelpicker.WheelPicker;
import com.aigestudio.wheelpicker.widgets.WheelYearPicker;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;


/**
 * Created by dharmaniz on 3/6/17.
 */

public class AlertDialogManager {
    private static final String TAG = "AlertDialogManager";
    public static int postion;
    public static Dialog progressDialog;
    //    private static final String[] PLANETS = {};
    private static ArrayList<String> PLANETS = new ArrayList<String>();

    public static void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static void showFinishAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }


    public static void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();}


    public static void showSelectItemFromArray(Activity mActivity, String strTitle, final String strStringArray[], final EditText mEditText) {
        final Dialog categoryDialog = new Dialog(mActivity);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.item_list_categories);
//        categoryDialog.setCanceledOnTouchOutside(true);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
        txtTitle.setText(strTitle);
        ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_list_item_1, android.R.id.text1, strStringArray);

        // Assign adapter to ListView
        lstListView.setAdapter(adapter);

        lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mEditText.setText(strStringArray[position]);
                categoryDialog.dismiss();

            }
        });


        categoryDialog.show();
    }

    public static void showSelectItemFromArrayText(Activity mActivity, String strTitle, final String strStringArray[], final TextView mTextView) {
        final Dialog categoryDialog = new Dialog(mActivity);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.item_list_categories);
//        categoryDialog.setCanceledOnTouchOutside(true);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
        txtTitle.setText(strTitle);
        ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        final Typeface mTypeFace = Typeface.createFromAsset(mActivity.getAssets(), "Poppins-Regular.ttf");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_list_item_1, android.R.id.text1, strStringArray) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textview = (TextView) view;
                textview.setTypeface(mTypeFace);
                return view;
            }
        };

        // Assign adapter to ListView
        lstListView.setAdapter(adapter);

        lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mTextView.setText(strStringArray[position]);
                categoryDialog.dismiss();

            }
        });


        categoryDialog.show();
    }

    /*Custom Year Picker
     * Link : https://github.com/Twinkle942910/MonthYearPicker
     * */
    public static void setYearPickerDialog(Activity mActivity, final EditText mEditText) {
        YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(mActivity, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");

                mEditText.setText(dateFormat.format(calendar.getTime()));
            }
        });
        yearMonthPickerDialog.show();

    }


    /*Calendar Custom Dialogs*/
    public static void showDatePicker(Activity mActivity, final EditText mEditText) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mEditText.setText(year + "-" + Utilities.getFormatedString("" + intMonth) + "-" + Utilities.getFormatedString("" + dayOfMonth));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }


    public static void showYearPicker(Activity mActivity, final EditText mEditText) {
        Calendar mCalendar = Calendar.getInstance();
        Log.e(TAG, "Current Year is : " + mCalendar.get(Calendar.YEAR));
        mEditText.setText("" + mCalendar.get(Calendar.YEAR));
        for (int i = 1900; i <= 2100; i++) {
            PLANETS.add("" + i);
        }
        final Dialog yearPickerDialog = new Dialog(mActivity);
        yearPickerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        yearPickerDialog.setContentView(R.layout.dialog_year_picker);
        yearPickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtTitle = (TextView) yearPickerDialog.findViewById(R.id.txtTitle);
        Button btnAdd = (Button) yearPickerDialog.findViewById(R.id.btnAdd);
        txtTitle.setText("Select Year");
        final WheelYearPicker mWheelView = (WheelYearPicker) yearPickerDialog.findViewById(R.id.mWheelView);
        mWheelView.setAtmospheric(true);
        mWheelView.setCyclic(true);
        mWheelView.setCurved(false);
        mWheelView.setIndicator(true);
        mWheelView.setIndicatorSize(3);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yearPickerDialog.dismiss();
            }
        });

        mWheelView.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                mEditText.setText("" + data.toString());
            }
        });
        yearPickerDialog.show();
    }

    public static void showAccountDiableDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                SharedPreferences preferences = mActivity.getSharedPreferences(JaoharPreference.PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                Intent mIntent = new Intent(mActivity, HomeActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mIntent.putExtra(JaoharConstants.LOGIN, JaoharConstants.LOGIN);
                mActivity.startActivity(mIntent);
                mActivity.finish();
            }
        });
        alertDialog.show();
    }


    public static void showAddItemsDialog(final Activity mActivity, final EditText mEditText) {
        final Dialog addItemDialog = new Dialog(mActivity);
        addItemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addItemDialog.setContentView(R.layout.dialog_add_items);
        addItemDialog.setCanceledOnTouchOutside(false);
        addItemDialog.setCancelable(false);
        addItemDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        final EditText editItemsET = (EditText) addItemDialog.findViewById(R.id.editItemsET);
        final EditText editQuantityET = (EditText) addItemDialog.findViewById(R.id.editQuantityET);
        final EditText editPriceET = (EditText) addItemDialog.findViewById(R.id.editPriceET);
        final EditText editDescriptionET = (EditText) addItemDialog.findViewById(R.id.editDescriptionET);
        Button btnSave = (Button) addItemDialog.findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editItemsET.getText().toString().equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.enter_items), Toast.LENGTH_SHORT).show();
                } else if (editQuantityET.getText().toString().equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.enter_quantity), Toast.LENGTH_SHORT).show();
                } else if (editPriceET.getText().toString().equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.enter_price11), Toast.LENGTH_SHORT).show();
                } else if (editDescriptionET.getText().toString().equals("")) {
                    Toast.makeText(mActivity, mActivity.getString(R.string.enter_description11), Toast.LENGTH_SHORT).show();
                } else {
                    mEditText.setText(editItemsET.getText().toString() + ", " + editPriceET.getText().toString());
                    addItemDialog.dismiss();
                }
            }
        });
        addItemDialog.show();
    }


    public static void showInvoiceDatePicker(Activity mActivity, final EditText mEditText) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;

//                        String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                        mEditText.setText(year + "-" + Utilities.getFormatedString("" + intMonth) + "-" + Utilities.getFormatedString("" + dayOfMonth));
                        //mEditText.setText(actualFormatDate);
                    }
                }, mYear, mMonth, mDay);


        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

}
