package jaohar.com.jaohar.models.invoicemodels;

import com.google.gson.annotations.SerializedName;

public class ItemdiscountItem{

	@SerializedName("discount_add_and_subtract_total")
	private double discountAddAndSubtractTotal;

	@SerializedName("discount_total_value")
	private String discountTotalValue;

	@SerializedName("discount_subtract_description")
	private String discountSubtractDescription;

	@SerializedName("discount_add_description")
	private String discountAddDescription;

	@SerializedName("discount_subtract_value")
	private String discountSubtractValue;

	@SerializedName("discount_type")
	private String discountType;

	@SerializedName("discount_percent")
	private double discountPercent;

	@SerializedName("discount_add_value")
	private String discountAddValue;

	@SerializedName("discount_add_unitprice")
	private int discountAddUnitprice;

	@SerializedName("discount_id")
	private String discountId;

	@SerializedName("discount_subtract_unitprice")
	private String discountSubtractUnitprice;

	public void setDiscountAddAndSubtractTotal(double discountAddAndSubtractTotal){
		this.discountAddAndSubtractTotal = discountAddAndSubtractTotal;
	}

	public double getDiscountAddAndSubtractTotal(){
		return discountAddAndSubtractTotal;
	}

	public void setDiscountTotalValue(String discountTotalValue){
		this.discountTotalValue = discountTotalValue;
	}

	public String getDiscountTotalValue(){
		return discountTotalValue;
	}

	public void setDiscountSubtractDescription(String discountSubtractDescription){
		this.discountSubtractDescription = discountSubtractDescription;
	}

	public String getDiscountSubtractDescription(){
		return discountSubtractDescription;
	}

	public void setDiscountAddDescription(String discountAddDescription){
		this.discountAddDescription = discountAddDescription;
	}

	public String getDiscountAddDescription(){
		return discountAddDescription;
	}

	public void setDiscountSubtractValue(String discountSubtractValue){
		this.discountSubtractValue = discountSubtractValue;
	}

	public String getDiscountSubtractValue(){
		return discountSubtractValue;
	}

	public void setDiscountType(String discountType){
		this.discountType = discountType;
	}

	public String getDiscountType(){
		return discountType;
	}

	public void setDiscountPercent(double discountPercent){
		this.discountPercent = discountPercent;
	}

	public double getDiscountPercent(){
		return discountPercent;
	}

	public void setDiscountAddValue(String discountAddValue){
		this.discountAddValue = discountAddValue;
	}

	public String getDiscountAddValue(){
		return discountAddValue;
	}

	public void setDiscountAddUnitprice(int discountAddUnitprice){
		this.discountAddUnitprice = discountAddUnitprice;
	}

	public int getDiscountAddUnitprice(){
		return discountAddUnitprice;
	}

	public void setDiscountId(String discountId){
		this.discountId = discountId;
	}

	public String getDiscountId(){
		return discountId;
	}

	public void setDiscountSubtractUnitprice(String discountSubtractUnitprice){
		this.discountSubtractUnitprice = discountSubtractUnitprice;
	}

	public String getDiscountSubtractUnitprice(){
		return discountSubtractUnitprice;
	}
}