package jaohar.com.jaohar.models;

import java.util.List;

public class FormListModel {
    private String status;

    private String message;

    private Data data;

    public void setStatus(String status){
        this.status = status;
    }
    public String getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(Data data){
        this.data = data;
    }
    public Data getData(){
        return this.data;
    }

    public class All_forums
    {
        private String id;

        private String user_id;

        private String vessel_id;

        private String room_id;

        private String heading;

        private String subject;

        private String image;

        private String creation_date;

        private String disable;

        private String reactive;

        private String access_type;

        private String user_access_to;

        private String read_by;

        private String message_time;

        private String vessel_record_id;

        private String vessel_name;

        private String IMO_number;

        private String status;

        private String pic;

        private int total_messages;

        private int unread_messages;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setVessel_id(String vessel_id){
            this.vessel_id = vessel_id;
        }
        public String getVessel_id(){
            return this.vessel_id;
        }
        public void setRoom_id(String room_id){
            this.room_id = room_id;
        }
        public String getRoom_id(){
            return this.room_id;
        }
        public void setHeading(String heading){
            this.heading = heading;
        }
        public String getHeading(){
            return this.heading;
        }
        public void setSubject(String subject){
            this.subject = subject;
        }
        public String getSubject(){
            return this.subject;
        }
        public void setImage(String image){
            this.image = image;
        }
        public String getImage(){
            return this.image;
        }
        public void setCreation_date(String creation_date){
            this.creation_date = creation_date;
        }
        public String getCreation_date(){
            return this.creation_date;
        }
        public void setDisable(String disable){
            this.disable = disable;
        }
        public String getDisable(){
            return this.disable;
        }
        public void setReactive(String reactive){
            this.reactive = reactive;
        }
        public String getReactive(){
            return this.reactive;
        }
        public void setAccess_type(String access_type){
            this.access_type = access_type;
        }
        public String getAccess_type(){
            return this.access_type;
        }
        public void setUser_access_to(String user_access_to){
            this.user_access_to = user_access_to;
        }
        public String getUser_access_to(){
            return this.user_access_to;
        }
        public void setRead_by(String read_by){
            this.read_by = read_by;
        }
        public String getRead_by(){
            return this.read_by;
        }
        public void setMessage_time(String message_time){
            this.message_time = message_time;
        }
        public String getMessage_time(){
            return this.message_time;
        }
        public void setVessel_record_id(String vessel_record_id){
            this.vessel_record_id = vessel_record_id;
        }
        public String getVessel_record_id(){
            return this.vessel_record_id;
        }
        public void setVessel_name(String vessel_name){
            this.vessel_name = vessel_name;
        }
        public String getVessel_name(){
            return this.vessel_name;
        }
        public void setIMO_number(String IMO_number){
            this.IMO_number = IMO_number;
        }
        public String getIMO_number(){
            return this.IMO_number;
        }
        public void setStatus(String status){
            this.status = status;
        }
        public String getStatus(){
            return this.status;
        }
        public void setPic(String pic){
            this.pic = pic;
        }
        public String getPic(){
            return this.pic;
        }
        public void setTotal_messages(int total_messages){
            this.total_messages = total_messages;
        }
        public int getTotal_messages(){
            return this.total_messages;
        }
        public void setUnread_messages(int unread_messages){
            this.unread_messages = unread_messages;
        }
        public int getUnread_messages(){
            return this.unread_messages;
        }
    }


    public class Data
    {
        private List<All_forums> all_forums;

        private String last_page;

        public void setAll_forums(List<All_forums> all_forums){
            this.all_forums = all_forums;
        }
        public List<All_forums> getAll_forums(){
            return this.all_forums;
        }
        public void setLast_page(String last_page){
            this.last_page = last_page;
        }
        public String getLast_page(){
            return this.last_page;
        }
    }
}
