package jaohar.com.jaohar.models.invoicedraftmodels;

import com.google.gson.annotations.SerializedName;

public class AllData{

	@SerializedName("term_days")
	private String termDays;

	@SerializedName("search_company")
	private String searchCompany;

	@SerializedName("bank_details")
	private String bankDetails;

	@SerializedName("invoice_no")
	private int invoiceNo;

	@SerializedName("invoice_added_on")
	private String invoiceAddedOn;

	@SerializedName("sign")
	private String sign;

	@SerializedName("stamp")
	private String stamp;

	@SerializedName("reference1")
	private String reference1;

	@SerializedName("reference2")
	private String reference2;

	@SerializedName("invoice_date")
	private String invoiceDate;

	@SerializedName("reference")
	private String reference;

	@SerializedName("pdf")
	private String pdf;

	@SerializedName("discounts")
	private String discounts;

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("payment_id")
	private String paymentId;

	@SerializedName("enable")
	private String enable;

	@SerializedName("is_draft")
	private String isDraft;

	@SerializedName("pdf_name")
	private String pdfName;

	@SerializedName("invoice_id")
	private String invoiceId;

	@SerializedName("currency")
	private String currency;

	@SerializedName("inv_state")
	private String invState;

	@SerializedName("search_vessel")
	private String searchVessel;

	@SerializedName("items")
	private String items;

	@SerializedName("status")
	private String status;

	public void setTermDays(String termDays){
		this.termDays = termDays;
	}

	public String getTermDays(){
		return termDays;
	}

	public void setSearchCompany(String searchCompany){
		this.searchCompany = searchCompany;
	}

	public String getSearchCompany(){
		return searchCompany;
	}

	public void setBankDetails(String bankDetails){
		this.bankDetails = bankDetails;
	}

	public String getBankDetails(){
		return bankDetails;
	}

	public void setInvoiceNo(int invoiceNo){
		this.invoiceNo = invoiceNo;
	}

	public int getInvoiceNo(){
		return invoiceNo;
	}

	public void setInvoiceAddedOn(String invoiceAddedOn){
		this.invoiceAddedOn = invoiceAddedOn;
	}

	public String getInvoiceAddedOn(){
		return invoiceAddedOn;
	}

	public void setSign(String sign){
		this.sign = sign;
	}

	public String getSign(){
		return sign;
	}

	public void setStamp(String stamp){
		this.stamp = stamp;
	}

	public String getStamp(){
		return stamp;
	}

	public void setReference1(String reference1){
		this.reference1 = reference1;
	}

	public String getReference1(){
		return reference1;
	}

	public void setReference2(String reference2){
		this.reference2 = reference2;
	}

	public String getReference2(){
		return reference2;
	}

	public void setInvoiceDate(String invoiceDate){
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceDate(){
		return invoiceDate;
	}

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setPdf(String pdf){
		this.pdf = pdf;
	}

	public String getPdf(){
		return pdf;
	}

	public void setDiscounts(String discounts){
		this.discounts = discounts;
	}

	public String getDiscounts(){
		return discounts;
	}

	public void setAddedBy(String addedBy){
		this.addedBy = addedBy;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public void setPaymentId(String paymentId){
		this.paymentId = paymentId;
	}

	public String getPaymentId(){
		return paymentId;
	}

	public void setEnable(String enable){
		this.enable = enable;
	}

	public String getEnable(){
		return enable;
	}

	public void setIsDraft(String isDraft){
		this.isDraft = isDraft;
	}

	public String getIsDraft(){
		return isDraft;
	}

	public void setPdfName(String pdfName){
		this.pdfName = pdfName;
	}

	public String getPdfName(){
		return pdfName;
	}

	public void setInvoiceId(String invoiceId){
		this.invoiceId = invoiceId;
	}

	public String getInvoiceId(){
		return invoiceId;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}

	public void setInvState(String invState){
		this.invState = invState;
	}

	public String getInvState(){
		return invState;
	}

	public void setSearchVessel(String searchVessel){
		this.searchVessel = searchVessel;
	}

	public String getSearchVessel(){
		return searchVessel;
	}

	public void setItems(String items){
		this.items = items;
	}

	public String getItems(){
		return items;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}