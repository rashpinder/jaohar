package jaohar.com.jaohar.models.invoicemodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("last_page")
	private String lastPage;

	@SerializedName("all_invoices")
	private List<AllInvoicesItem> allInvoices;

	public void setLastPage(String lastPage){
		this.lastPage = lastPage;
	}

	public String getLastPage(){
		return lastPage;
	}

	public void setAllInvoices(List<AllInvoicesItem> allInvoices){
		this.allInvoices = allInvoices;
	}

	public List<AllInvoicesItem> getAllInvoices(){
		return allInvoices;
	}
}