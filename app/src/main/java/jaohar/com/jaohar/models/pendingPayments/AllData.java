package jaohar.com.jaohar.models.pendingPayments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AllData {
   @SerializedName("pp_id")
   @Expose
   private String ppId;
   @SerializedName("pp_no")
   @Expose
   private String ppNo;
   @SerializedName("pp_date")
   @Expose
   private String ppDate;
   @SerializedName("pp_company")
   @Expose
   private String ppCompany;
   @SerializedName("pp_vessel1")
   @Expose
   private String ppVessel1;
   @SerializedName("pp_vessel2")
   @Expose
   private String ppVessel2;
   @SerializedName("pp_vessel3")
   @Expose
   private String ppVessel3;
   @SerializedName("pp_amt_due")
   @Expose
   private String ppAmtDue;
   @SerializedName("pp_currency")
   @Expose
   private String ppCurrency;
   @SerializedName("pp_status")
   @Expose
   private String ppStatus;
   @SerializedName("pp_remarks")
   @Expose
   private String ppRemarks;
   @SerializedName("pp_added_on")
   @Expose
   private String ppAddedOn;
   @SerializedName("pp_added_by")
   @Expose
   private String ppAddedBy;
   @SerializedName("disable")
   @Expose
   private String disable;
   @SerializedName("all_docs")
   @Expose
   private ArrayList<String> allDocs = null;

   public String getPpId() {
      return ppId;
   }

   public void setPpId(String ppId) {
      this.ppId = ppId;
   }

   public String getPpNo() {
      return ppNo;
   }

   public void setPpNo(String ppNo) {
      this.ppNo = ppNo;
   }

   public String getPpDate() {
      return ppDate;
   }

   public void setPpDate(String ppDate) {
      this.ppDate = ppDate;
   }

   public String getPpCompany() {
      return ppCompany;
   }

   public void setPpCompany(String ppCompany) {
      this.ppCompany = ppCompany;
   }

   public String getPpVessel1() {
      return ppVessel1;
   }

   public void setPpVessel1(String ppVessel1) {
      this.ppVessel1 = ppVessel1;
   }

   public String getPpVessel2() {
      return ppVessel2;
   }

   public void setPpVessel2(String ppVessel2) {
      this.ppVessel2 = ppVessel2;
   }

   public String getPpVessel3() {
      return ppVessel3;
   }

   public void setPpVessel3(String ppVessel3) {
      this.ppVessel3 = ppVessel3;
   }

   public String getPpAmtDue() {
      return ppAmtDue;
   }

   public void setPpAmtDue(String ppAmtDue) {
      this.ppAmtDue = ppAmtDue;
   }

   public String getPpCurrency() {
      return ppCurrency;
   }

   public void setPpCurrency(String ppCurrency) {
      this.ppCurrency = ppCurrency;
   }

   public String getPpStatus() {
      return ppStatus;
   }

   public void setPpStatus(String ppStatus) {
      this.ppStatus = ppStatus;
   }

   public String getPpRemarks() {
      return ppRemarks;
   }

   public void setPpRemarks(String ppRemarks) {
      this.ppRemarks = ppRemarks;
   }

   public String getPpAddedOn() {
      return ppAddedOn;
   }

   public void setPpAddedOn(String ppAddedOn) {
      this.ppAddedOn = ppAddedOn;
   }

   public String getPpAddedBy() {
      return ppAddedBy;
   }

   public void setPpAddedBy(String ppAddedBy) {
      this.ppAddedBy = ppAddedBy;
   }

   public String getDisable() {
      return disable;
   }

   public void setDisable(String disable) {
      this.disable = disable;
   }

   public ArrayList<String> getAllDocs() {
      return allDocs;
   }

   public void setAllDocs(ArrayList<String> allDocs) {
      this.allDocs = allDocs;
   }
}
