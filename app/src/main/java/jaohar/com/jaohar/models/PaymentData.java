package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentData {

    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("sub_total")
    @Expose
    private String subTotal;
    @SerializedName("VAT")
    @Expose
    private String vat;
    @SerializedName("vat_price")
    @Expose
    private String vatPrice;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("paid")
    @Expose
    private String paid;
    @SerializedName("due")
    @Expose
    private String due;
    @SerializedName("status")
    @Expose
    private String status;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVatPrice() {
        return vatPrice;
    }

    public void setVatPrice(String vatPrice) {
        this.vatPrice = vatPrice;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}