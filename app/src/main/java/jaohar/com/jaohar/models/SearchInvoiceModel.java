package jaohar.com.jaohar.models;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

        import java.util.ArrayList;

class AllData {

    @SerializedName("invoice_id")
    @Expose
    private String invoiceId;
    @SerializedName("invoice_no")
    @Expose
    private Boolean invoiceNo;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("term_days")
    @Expose
    private String termDays;
    @SerializedName("search_company")
    @Expose
    private String searchCompany;
    @SerializedName("search_vessel")
    @Expose
    private String searchVessel;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("stamp")
    @Expose
    private String stamp;
    @SerializedName("sign")
    @Expose
    private String sign;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("reference1")
    @Expose
    private String reference1;
    @SerializedName("reference2")
    @Expose
    private String reference2;
    @SerializedName("bank_details")
    @Expose
    private String bankDetails;
    @SerializedName("items")
    @Expose
    private String items;
    @SerializedName("discounts")
    @Expose
    private String discounts;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("invoice_added_on")
    @Expose
    private String invoiceAddedOn;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("enable")
    @Expose
    private String enable;
    @SerializedName("inv_state")
    @Expose
    private String invState;
    @SerializedName("is_draft")
    @Expose
    private String isDraft;
    @SerializedName("deleted_by")
    @Expose
    private String deletedBy;
    @SerializedName("pdf")
    @Expose
    private String pdf;
    @SerializedName("pdf_name")
    @Expose
    private String pdfName;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Boolean getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Boolean invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getTermDays() {
        return termDays;
    }

    public void setTermDays(String termDays) {
        this.termDays = termDays;
    }

    public String getSearchCompany() {
        return searchCompany;
    }

    public void setSearchCompany(String searchCompany) {
        this.searchCompany = searchCompany;
    }

    public String getSearchVessel() {
        return searchVessel;
    }

    public void setSearchVessel(String searchVessel) {
        this.searchVessel = searchVessel;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReference1() {
        return reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(String bankDetails) {
        this.bankDetails = bankDetails;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getDiscounts() {
        return discounts;
    }

    public void setDiscounts(String discounts) {
        this.discounts = discounts;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getInvoiceAddedOn() {
        return invoiceAddedOn;
    }

    public void setInvoiceAddedOn(String invoiceAddedOn) {
        this.invoiceAddedOn = invoiceAddedOn;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getInvState() {
        return invState;
    }

    public void setInvState(String invState) {
        this.invState = invState;
    }

    public String getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(String isDraft) {
        this.isDraft = isDraft;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getPdfName() {
        return pdfName;
    }

    public void setPdfName(String pdfName) {
        this.pdfName = pdfName;
    }

}

class AllSearchedInvoice {

    @SerializedName("all_data")
    @Expose
    private AllData allData;
    @SerializedName("sign_data")
    @Expose
    private String signData;
    @SerializedName("stamp_data")
    @Expose
    private String stampData;
    @SerializedName("bank_data")
    @Expose
    private String bankData;
    @SerializedName("search_vessel_data")
    @Expose
    private String searchVesselData;
    @SerializedName("search_company_data")
    @Expose
    private String searchCompanyData;
    @SerializedName("payment_data")
    @Expose
    private String paymentData;
    @SerializedName("items_data")
    @Expose
    private String itemsData;
    @SerializedName("itemdiscount")
    @Expose
    private String itemdiscount;

    public AllData getAllData() {
        return allData;
    }

    public void setAllData(AllData allData) {
        this.allData = allData;
    }

    public String getSignData() {
        return signData;
    }

    public void setSignData(String signData) {
        this.signData = signData;
    }

    public String getStampData() {
        return stampData;
    }

    public void setStampData(String stampData) {
        this.stampData = stampData;
    }

    public String getBankData() {
        return bankData;
    }

    public void setBankData(String bankData) {
        this.bankData = bankData;
    }

    public String getSearchVesselData() {
        return searchVesselData;
    }

    public void setSearchVesselData(String searchVesselData) {
        this.searchVesselData = searchVesselData;
    }

    public String getSearchCompanyData() {
        return searchCompanyData;
    }

    public void setSearchCompanyData(String searchCompanyData) {
        this.searchCompanyData = searchCompanyData;
    }

    public String getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(String paymentData) {
        this.paymentData = paymentData;
    }

    public String getItemsData() {
        return itemsData;
    }

    public void setItemsData(String itemsData) {
        this.itemsData = itemsData;
    }

    public String getItemdiscount() {
        return itemdiscount;
    }

    public void setItemdiscount(String itemdiscount) {
        this.itemdiscount = itemdiscount;
    }

}

public class SearchInvoiceModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("all_searched_invoices")
    @Expose
    private ArrayList<AllSearchedInvoice> allSearchedInvoices = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AllSearchedInvoice> getAllSearchedInvoices() {
        return allSearchedInvoices;
    }

    public void setAllSearchedInvoices(ArrayList<AllSearchedInvoice> allSearchedInvoices) {
        this.allSearchedInvoices = allSearchedInvoices;
    }

}
