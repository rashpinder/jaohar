package jaohar.com.jaohar.models.pendingPayments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PendingPaymentModels {

   @SerializedName("status")
   @Expose
   private String status;
   @SerializedName("message")
   @Expose
   private String message;
   @SerializedName("data")
   @Expose
   private PendingPaymentData data;

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public PendingPaymentData getData() {
      return data;
   }

   public void setData(PendingPaymentData data) {
      this.data = data;
   }
}
