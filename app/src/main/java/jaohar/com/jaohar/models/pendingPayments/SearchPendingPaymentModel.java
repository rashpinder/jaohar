package jaohar.com.jaohar.models.pendingPayments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchPendingPaymentModel {

   @SerializedName("status")
   @Expose
   private String status;
   @SerializedName("message")
   @Expose
   private String message;
   @SerializedName("all_searched_invoices")
   @Expose
   private ArrayList<SearchAllSearchedInvoice> allSearchedInvoices = null;

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public ArrayList<SearchAllSearchedInvoice> getAllSearchedInvoices() {
      return allSearchedInvoices;
   }

   public void setAllSearchedInvoices(ArrayList<SearchAllSearchedInvoice> allSearchedInvoices) {
      this.allSearchedInvoices = allSearchedInvoices;
   }
}
