package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class AddNewMessageModel {

    @SerializedName("append_data")
    private AllChatsItem appendData;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("update")
    private boolean update;

    @SerializedName("id")
    private int id;

    @SerializedName("message")
    private String message;



    @SerializedName("emit_full_screen_data")
    private String emit_full_screen_data;

    @SerializedName("append_full_screen_data")
    private String append_full_screen_data;

    @SerializedName("html")
    private String html;

    @SerializedName("emithtml")
    private String emithtml;

    @SerializedName("status")
    private int status;

    public AllChatsItem getAppendData() {
        return appendData;
    }

    public String getUserId() {
        return userId;
    }

    public boolean isUpdate() {
        return update;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public String getEmit_full_screen_data() {
        return emit_full_screen_data;
    }

    public void setEmit_full_screen_data(String emit_full_screen_data) {
        this.emit_full_screen_data = emit_full_screen_data;
    }

    public String getAppend_full_screen_data() {
        return append_full_screen_data;
    }

    public void setAppend_full_screen_data(String append_full_screen_data) {
        this.append_full_screen_data = append_full_screen_data;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getEmithtml() {
        return emithtml;
    }

    public void setEmithtml(String emithtml) {
        this.emithtml = emithtml;
    }
}