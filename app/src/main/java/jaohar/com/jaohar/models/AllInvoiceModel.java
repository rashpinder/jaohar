package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllInvoiceModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("all_invoices")
        @Expose
        private ArrayList<AllInvoice> allInvoices = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public ArrayList<AllInvoice> getAllInvoices() {
            return allInvoices;
        }

        public void setAllInvoices(ArrayList<AllInvoice> allInvoices) {
            this.allInvoices = allInvoices;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

        public class AllInvoice {

            @SerializedName("all_data")
            @Expose
            private AllData allData;
            @SerializedName("sign_data")
            @Expose
            private SignData signData;
            @SerializedName("stamp_data")
            @Expose
            private StampData stampData;
            @SerializedName("bank_data")
            @Expose
            private BankDataa bankData;
            @SerializedName("search_vessel_data")
            @Expose
            private SearchVesselData searchVesselData;
            @SerializedName("search_company_data")
            @Expose
            private SearchCompanyData searchCompanyData;
            @SerializedName("payment_data")
            @Expose
            private String paymentData;
            @SerializedName("items_data")
            @Expose
            private String itemsData;
            @SerializedName("itemdiscount")
            @Expose
            private String itemdiscount;

            public AllData getAllData() {
                return allData;
            }

            public void setAllData(AllData allData) {
                this.allData = allData;
            }

            public SignData getSignData() {
                return signData;
            }

            public void setSignData(SignData signData) {
                this.signData = signData;
            }

            public StampData getStampData() {
                return stampData;
            }

            public void setStampData(StampData stampData) {
                this.stampData = stampData;
            }

            public BankDataa getBankData() {
                return bankData;
            }

            public void setBankData(BankDataa bankData) {
                this.bankData = bankData;
            }

            public SearchVesselData getSearchVesselData() {
                return searchVesselData;
            }

            public void setSearchVesselData(SearchVesselData searchVesselData) {
                this.searchVesselData = searchVesselData;
            }

            public SearchCompanyData getSearchCompanyData() {
                return searchCompanyData;
            }

            public void setSearchCompanyData(SearchCompanyData searchCompanyData) {
                this.searchCompanyData = searchCompanyData;
            }

            public String getPaymentData() {
                return paymentData;
            }

            public void setPaymentData(String paymentData) {
                this.paymentData = paymentData;
            }

            public String getItemsData() {
                return itemsData;
            }

            public void setItemsData(String itemsData) {
                this.itemsData = itemsData;
            }

            public String getItemdiscount() {
                return itemdiscount;
            }

            public void setItemdiscount(String itemdiscount) {
                this.itemdiscount = itemdiscount;
            }

            public class SearchCompanyData {

                @SerializedName("id")
                @Expose
                private String id;
                @SerializedName("company_name")
                @Expose
                private String companyName;
                @SerializedName("Address1")
                @Expose
                private String address1;
                @SerializedName("Address2")
                @Expose
                private String address2;
                @SerializedName("Address3")
                @Expose
                private String address3;
                @SerializedName("Address4")
                @Expose
                private String address4;
                @SerializedName("Address5")
                @Expose
                private String address5;
                @SerializedName("added_by")
                @Expose
                private String addedBy;
                @SerializedName("modification_date")
                @Expose
                private String modificationDate;
                @SerializedName("enable")
                @Expose
                private String enable;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getCompanyName() {
                    return companyName;
                }

                public void setCompanyName(String companyName) {
                    this.companyName = companyName;
                }

                public String getAddress1() {
                    return address1;
                }

                public void setAddress1(String address1) {
                    this.address1 = address1;
                }

                public String getAddress2() {
                    return address2;
                }

                public void setAddress2(String address2) {
                    this.address2 = address2;
                }

                public String getAddress3() {
                    return address3;
                }

                public void setAddress3(String address3) {
                    this.address3 = address3;
                }

                public String getAddress4() {
                    return address4;
                }

                public void setAddress4(String address4) {
                    this.address4 = address4;
                }

                public String getAddress5() {
                    return address5;
                }

                public void setAddress5(String address5) {
                    this.address5 = address5;
                }

                public String getAddedBy() {
                    return addedBy;
                }

                public void setAddedBy(String addedBy) {
                    this.addedBy = addedBy;
                }

                public String getModificationDate() {
                    return modificationDate;
                }

                public void setModificationDate(String modificationDate) {
                    this.modificationDate = modificationDate;
                }

                public String getEnable() {
                    return enable;
                }

                public void setEnable(String enable) {
                    this.enable = enable;
                }

            }

            public class SearchVesselData {

                @SerializedName("vessel_id")
                @Expose
                private String vesselId;
                @SerializedName("vessel_name")
                @Expose
                private String vesselName;
                @SerializedName("IMO_no")
                @Expose
                private String iMONo;
                @SerializedName("flag")
                @Expose
                private String flag;
                @SerializedName("added_by")
                @Expose
                private String addedBy;
                @SerializedName("modification_date")
                @Expose
                private String modificationDate;
                @SerializedName("enable")
                @Expose
                private String enable;

                public String getVesselId() {
                    return vesselId;
                }

                public void setVesselId(String vesselId) {
                    this.vesselId = vesselId;
                }

                public String getVesselName() {
                    return vesselName;
                }

                public void setVesselName(String vesselName) {
                    this.vesselName = vesselName;
                }

                public String getIMONo() {
                    return iMONo;
                }

                public void setIMONo(String iMONo) {
                    this.iMONo = iMONo;
                }

                public String getFlag() {
                    return flag;
                }

                public void setFlag(String flag) {
                    this.flag = flag;
                }

                public String getAddedBy() {
                    return addedBy;
                }

                public void setAddedBy(String addedBy) {
                    this.addedBy = addedBy;
                }

                public String getModificationDate() {
                    return modificationDate;
                }

                public void setModificationDate(String modificationDate) {
                    this.modificationDate = modificationDate;
                }

                public String getEnable() {
                    return enable;
                }

                public void setEnable(String enable) {
                    this.enable = enable;
                }

            }

            public class SignData {

                @SerializedName("sign_id")
                @Expose
                private String signId;
                @SerializedName("sign_name")
                @Expose
                private String signName;
                @SerializedName("sign_image")
                @Expose
                private String signImage;
                @SerializedName("added_by")
                @Expose
                private String addedBy;
                @SerializedName("modification_date")
                @Expose
                private String modificationDate;
                @SerializedName("enable")
                @Expose
                private String enable;

                public String getSignId() {
                    return signId;
                }

                public void setSignId(String signId) {
                    this.signId = signId;
                }

                public String getSignName() {
                    return signName;
                }

                public void setSignName(String signName) {
                    this.signName = signName;
                }

                public String getSignImage() {
                    return signImage;
                }

                public void setSignImage(String signImage) {
                    this.signImage = signImage;
                }

                public String getAddedBy() {
                    return addedBy;
                }

                public void setAddedBy(String addedBy) {
                    this.addedBy = addedBy;
                }

                public String getModificationDate() {
                    return modificationDate;
                }

                public void setModificationDate(String modificationDate) {
                    this.modificationDate = modificationDate;
                }

                public String getEnable() {
                    return enable;
                }

                public void setEnable(String enable) {
                    this.enable = enable;
                }

            }

            public class StampData {

                @SerializedName("stamp_id")
                @Expose
                private String stampId;
                @SerializedName("stamp_name")
                @Expose
                private String stampName;
                @SerializedName("stamp_image")
                @Expose
                private String stampImage;
                @SerializedName("added_by")
                @Expose
                private String addedBy;
                @SerializedName("modification_date")
                @Expose
                private String modificationDate;
                @SerializedName("enable")
                @Expose
                private String enable;

                public String getStampId() {
                    return stampId;
                }

                public void setStampId(String stampId) {
                    this.stampId = stampId;
                }

                public String getStampName() {
                    return stampName;
                }

                public void setStampName(String stampName) {
                    this.stampName = stampName;
                }

                public String getStampImage() {
                    return stampImage;
                }

                public void setStampImage(String stampImage) {
                    this.stampImage = stampImage;
                }

                public String getAddedBy() {
                    return addedBy;
                }

                public void setAddedBy(String addedBy) {
                    this.addedBy = addedBy;
                }

                public String getModificationDate() {
                    return modificationDate;
                }

                public void setModificationDate(String modificationDate) {
                    this.modificationDate = modificationDate;
                }

                public String getEnable() {
                    return enable;
                }

                public void setEnable(String enable) {
                    this.enable = enable;
                }

            }

            public class AllData {

                @SerializedName("invoice_id")
                @Expose
                private String invoiceId;
                @SerializedName("invoice_no")
                @Expose
                private String invoiceNo;
                @SerializedName("invoice_date")
                @Expose
                private String invoiceDate;
                @SerializedName("term_days")
                @Expose
                private String termDays;
                @SerializedName("search_company")
                @Expose
                private String searchCompany;
                @SerializedName("search_vessel")
                @Expose
                private String searchVessel;
                @SerializedName("currency")
                @Expose
                private String currency;
                @SerializedName("stamp")
                @Expose
                private String stamp;
                @SerializedName("sign")
                @Expose
                private String sign;
                @SerializedName("status")
                @Expose
                private String status;
                @SerializedName("reference")
                @Expose
                private String reference;
                @SerializedName("reference1")
                @Expose
                private String reference1;
                @SerializedName("reference2")
                @Expose
                private String reference2;
                @SerializedName("bank_details")
                @Expose
                private String bankDetails;
                @SerializedName("items")
                @Expose
                private String items;
                @SerializedName("discounts")
                @Expose
                private String discounts;
                @SerializedName("payment_id")
                @Expose
                private String paymentId;
                @SerializedName("invoice_added_on")
                @Expose
                private String invoiceAddedOn;
                @SerializedName("added_by")
                @Expose
                private String addedBy;
                @SerializedName("enable")
                @Expose
                private String enable;
                @SerializedName("inv_state")
                @Expose
                private String invState;
                @SerializedName("is_draft")
                @Expose
                private String isDraft;
                @SerializedName("deleted_by")
                @Expose
                private String deletedBy;
                @SerializedName("pdf")
                @Expose
                private String pdf;
                @SerializedName("pdf_name")
                @Expose
                private String pdfName;
                @SerializedName("owner_id")
                @Expose
                private String owner_id;

                public String getOwner_id() {
                    return owner_id;
                }

                public void setOwner_id(String owner_id) {
                    this.owner_id = owner_id;
                }

                public String getInvoiceId() {
                    return invoiceId;
                }

                public void setInvoiceId(String invoiceId) {
                    this.invoiceId = invoiceId;
                }

                public String getInvoiceNo() {
                    return invoiceNo;
                }

                public void setInvoiceNo(String invoiceNo) {
                    this.invoiceNo = invoiceNo;
                }

                public String getInvoiceDate() {
                    return invoiceDate;
                }

                public void setInvoiceDate(String invoiceDate) {
                    this.invoiceDate = invoiceDate;
                }

                public String getTermDays() {
                    return termDays;
                }

                public void setTermDays(String termDays) {
                    this.termDays = termDays;
                }

                public String getSearchCompany() {
                    return searchCompany;
                }

                public void setSearchCompany(String searchCompany) {
                    this.searchCompany = searchCompany;
                }

                public String getSearchVessel() {
                    return searchVessel;
                }

                public void setSearchVessel(String searchVessel) {
                    this.searchVessel = searchVessel;
                }

                public String getCurrency() {
                    return currency;
                }

                public void setCurrency(String currency) {
                    this.currency = currency;
                }

                public String getStamp() {
                    return stamp;
                }

                public void setStamp(String stamp) {
                    this.stamp = stamp;
                }

                public String getSign() {
                    return sign;
                }

                public void setSign(String sign) {
                    this.sign = sign;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getReference() {
                    return reference;
                }

                public void setReference(String reference) {
                    this.reference = reference;
                }

                public String getReference1() {
                    return reference1;
                }

                public void setReference1(String reference1) {
                    this.reference1 = reference1;
                }

                public String getReference2() {
                    return reference2;
                }

                public void setReference2(String reference2) {
                    this.reference2 = reference2;
                }

                public String getBankDetails() {
                    return bankDetails;
                }

                public void setBankDetails(String bankDetails) {
                    this.bankDetails = bankDetails;
                }

                public String getItems() {
                    return items;
                }

                public void setItems(String items) {
                    this.items = items;
                }

                public String getDiscounts() {
                    return discounts;
                }

                public void setDiscounts(String discounts) {
                    this.discounts = discounts;
                }

                public String getPaymentId() {
                    return paymentId;
                }

                public void setPaymentId(String paymentId) {
                    this.paymentId = paymentId;
                }

                public String getInvoiceAddedOn() {
                    return invoiceAddedOn;
                }

                public void setInvoiceAddedOn(String invoiceAddedOn) {
                    this.invoiceAddedOn = invoiceAddedOn;
                }

                public String getAddedBy() {
                    return addedBy;
                }

                public void setAddedBy(String addedBy) {
                    this.addedBy = addedBy;
                }

                public String getEnable() {
                    return enable;
                }

                public void setEnable(String enable) {
                    this.enable = enable;
                }

                public String getInvState() {
                    return invState;
                }

                public void setInvState(String invState) {
                    this.invState = invState;
                }

                public String getIsDraft() {
                    return isDraft;
                }

                public void setIsDraft(String isDraft) {
                    this.isDraft = isDraft;
                }

                public String getDeletedBy() {
                    return deletedBy;
                }

                public void setDeletedBy(String deletedBy) {
                    this.deletedBy = deletedBy;
                }

                public String getPdf() {
                    return pdf;
                }

                public void setPdf(String pdf) {
                    this.pdf = pdf;
                }

                public String getPdfName() {
                    return pdfName;
                }

                public void setPdfName(String pdfName) {
                    this.pdfName = pdfName;
                }

            }

            public class BankDataa {

                @SerializedName("bank_id")
                @Expose
                private String bankId;
                @SerializedName("beneficiary")
                @Expose
                private String beneficiary;
                @SerializedName("beneficiary_add")
                @Expose
                private String beneficiaryAdd;
                @SerializedName("beneficiary_country")
                @Expose
                private String beneficiaryCountry;
                @SerializedName("bank_name")
                @Expose
                private String bankName;
                @SerializedName("address1")
                @Expose
                private String address1;
                @SerializedName("address2")
                @Expose
                private String address2;
                @SerializedName("iban_ron")
                @Expose
                private String ibanRon;
                @SerializedName("iban_usd")
                @Expose
                private String ibanUsd;
                @SerializedName("iban_eur")
                @Expose
                private String ibanEur;
                @SerializedName("iban_gbp")
                @Expose
                private String ibanGbp;
                @SerializedName("swift")
                @Expose
                private String swift;
                @SerializedName("added_by")
                @Expose
                private String addedBy;
                @SerializedName("modification_date")
                @Expose
                private String modificationDate;
                @SerializedName("enable")
                @Expose
                private String enable;

                public String getBankId() {
                    return bankId;
                }

                public void setBankId(String bankId) {
                    this.bankId = bankId;
                }

                public String getBeneficiary() {
                    return beneficiary;
                }

                public void setBeneficiary(String beneficiary) {
                    this.beneficiary = beneficiary;
                }

                public String getBeneficiaryAdd() {
                    return beneficiaryAdd;
                }

                public void setBeneficiaryAdd(String beneficiaryAdd) {
                    this.beneficiaryAdd = beneficiaryAdd;
                }

                public String getBeneficiaryCountry() {
                    return beneficiaryCountry;
                }

                public void setBeneficiaryCountry(String beneficiaryCountry) {
                    this.beneficiaryCountry = beneficiaryCountry;
                }

                public String getBankName() {
                    return bankName;
                }

                public void setBankName(String bankName) {
                    this.bankName = bankName;
                }

                public String getAddress1() {
                    return address1;
                }

                public void setAddress1(String address1) {
                    this.address1 = address1;
                }

                public String getAddress2() {
                    return address2;
                }

                public void setAddress2(String address2) {
                    this.address2 = address2;
                }

                public String getIbanRon() {
                    return ibanRon;
                }

                public void setIbanRon(String ibanRon) {
                    this.ibanRon = ibanRon;
                }

                public String getIbanUsd() {
                    return ibanUsd;
                }

                public void setIbanUsd(String ibanUsd) {
                    this.ibanUsd = ibanUsd;
                }

                public String getIbanEur() {
                    return ibanEur;
                }

                public void setIbanEur(String ibanEur) {
                    this.ibanEur = ibanEur;
                }

                public String getIbanGbp() {
                    return ibanGbp;
                }

                public void setIbanGbp(String ibanGbp) {
                    this.ibanGbp = ibanGbp;
                }

                public String getSwift() {
                    return swift;
                }

                public void setSwift(String swift) {
                    this.swift = swift;
                }

                public String getAddedBy() {
                    return addedBy;
                }

                public void setAddedBy(String addedBy) {
                    this.addedBy = addedBy;
                }

                public String getModificationDate() {
                    return modificationDate;
                }

                public void setModificationDate(String modificationDate) {
                    this.modificationDate = modificationDate;
                }

                public String getEnable() {
                    return enable;
                }

                public void setEnable(String enable) {
                    this.enable = enable;
                }

            }
        }
    }
}


