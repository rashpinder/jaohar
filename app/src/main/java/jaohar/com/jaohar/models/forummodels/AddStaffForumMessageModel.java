package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.SerializedName;

public class AddStaffForumMessageModel{

	@SerializedName("emit_full_screen_data")
	private String emitFullScreenData;

	@SerializedName("append_data")
	private AllMessagesItem appendData;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("n_users")
	private String nUsers;

	@SerializedName("update")
	private boolean update;

	@SerializedName("id")
	private int id;

	@SerializedName("message")
	private String message;

	@SerializedName("append_full_screen_data")
	private String appendFullScreenData;

	@SerializedName("status")
	private int status;

	public String getEmitFullScreenData(){
		return emitFullScreenData;
	}

	public AllMessagesItem getAppendData(){
		return appendData;
	}

	public String getUserId(){
		return userId;
	}

	public String getNUsers(){
		return nUsers;
	}

	public boolean isUpdate(){
		return update;
	}

	public int getId(){
		return id;
	}

	public String getMessage(){
		return message;
	}

	public String getAppendFullScreenData(){
		return appendFullScreenData;
	}

	public int getStatus(){
		return status;
	}
}