package jaohar.com.jaohar.models.pendingPayments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import jaohar.com.jaohar.models.PpVessel2Detail;
import jaohar.com.jaohar.models.PpVessel3Detail;

public class SearchAllSearchedInvoice {
   @SerializedName("all_data")
   @Expose
   private AllData allData;
   @SerializedName("pp_vessel1_detail")
   @Expose
   private PpVessel1Detail ppVessel1Detail;
   @SerializedName("pp_vessel2_detail")
   @Expose
   private PpVessel2Detail ppVessel2Detail;
   @SerializedName("pp_vessel3_detail")
   @Expose
   private PpVessel3Detail ppVessel3Detail;
   @SerializedName("pp_company_detail")
   @Expose
   private PpCompanyDetail ppCompanyDetail;

   public AllData getAllData() {
      return allData;
   }

   public void setAllData(AllData allData) {
      this.allData = allData;
   }

   public PpVessel1Detail getPpVessel1Detail() {
      return ppVessel1Detail;
   }

   public void setPpVessel1Detail(PpVessel1Detail ppVessel1Detail) {
      this.ppVessel1Detail = ppVessel1Detail;
   }

   public PpVessel2Detail getPpVessel2Detail() {
      return ppVessel2Detail;
   }

   public void setPpVessel2Detail(PpVessel2Detail ppVessel2Detail) {
      this.ppVessel2Detail = ppVessel2Detail;
   }

   public PpVessel3Detail getPpVessel3Detail() {
      return ppVessel3Detail;
   }

   public void setPpVessel3Detail(PpVessel3Detail ppVessel3Detail) {
      this.ppVessel3Detail = ppVessel3Detail;
   }

   public PpCompanyDetail getPpCompanyDetail() {
      return ppCompanyDetail;
   }

   public void setPpCompanyDetail(PpCompanyDetail ppCompanyDetail) {
      this.ppCompanyDetail = ppCompanyDetail;
   }
}
