package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class RoomDetail{

	@SerializedName("image")
	private String image;

	@SerializedName("role")
	private String role;

	@SerializedName("online_time")
	private String onlineTime;

	@SerializedName("online_state")
	private String onlineState;

	@SerializedName("group_id")
	private int groupId;

	@SerializedName("name")
	private String name;

	public String getImage(){
		return image;
	}

	public String getRole(){
		return role;
	}

	public String getOnlineTime(){
		return onlineTime;
	}

	public String getOnlineState(){
		return onlineState;
	}

	public int getGroupId(){
		return groupId;
	}

	public String getName(){
		return name;
	}
}