package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class AllChatsItem {

    public int intType;
    @SerializedName("room_id")
    private String roomId;
    @SerializedName("date")
    private String date;
    @SerializedName("read_by")
    private String readBy;
    @SerializedName("receiver_id")
    private String receiverId;
    @SerializedName("read_status")
    private String readStatus;
    @SerializedName("sender_id")
    private String senderId;
    @SerializedName("doc7")
    private String doc7;
    @SerializedName("doc6")
    private String doc6;
    @SerializedName("edit_refresh_app")
    private String editRefreshApp;
    @SerializedName("doc5")
    private String doc5;
    @SerializedName("doc4")
    private String doc4;
    @SerializedName("doc3")
    private String doc3;
    @SerializedName("doc2")
    private String doc2;
    @SerializedName("doc1")
    private String doc1;
    @SerializedName("enable")
    private String enable;
    @SerializedName("image_count")
    private String imageCount;
    @SerializedName("doc10")
    private String doc10;
    @SerializedName("edited")
    private String edited;
    @SerializedName("user_detail")
    private UserDetail userDetail;
    @SerializedName("edit_refresh")
    private String editRefresh;
    @SerializedName("creation_date")
    private String creationDate;
    @SerializedName("message")
    private String message;
    @SerializedName("doc9")
    private String doc9;
    @SerializedName("chat_id")
    private String chatId;
    @SerializedName("doc8")
    private String doc8;
    @SerializedName("reply_id")
    private String replyId;
    @SerializedName("group_id")
    private String groupId;
    @SerializedName("reply_for")
    private ReplyFor replyFor;
    @SerializedName("message_type")
    private String messageType;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public int getIntType() {
        return intType;
    }

    public void setIntType(int intType) {
        this.intType = intType;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReadBy() {
        return readBy;
    }

    public void setReadBy(String readBy) {
        this.readBy = readBy;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getDoc7() {
        return doc7;
    }

    public void setDoc7(String doc7) {
        this.doc7 = doc7;
    }

    public String getDoc6() {
        return doc6;
    }

    public void setDoc6(String doc6) {
        this.doc6 = doc6;
    }

    public String getEditRefreshApp() {
        return editRefreshApp;
    }

    public void setEditRefreshApp(String editRefreshApp) {
        this.editRefreshApp = editRefreshApp;
    }

    public String getDoc5() {
        return doc5;
    }

    public void setDoc5(String doc5) {
        this.doc5 = doc5;
    }

    public String getDoc4() {
        return doc4;
    }

    public void setDoc4(String doc4) {
        this.doc4 = doc4;
    }

    public String getDoc3() {
        return doc3;
    }

    public void setDoc3(String doc3) {
        this.doc3 = doc3;
    }

    public String getDoc2() {
        return doc2;
    }

    public void setDoc2(String doc2) {
        this.doc2 = doc2;
    }

    public String getDoc1() {
        return doc1;
    }

    public void setDoc1(String doc1) {
        this.doc1 = doc1;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getImageCount() {
        return imageCount;
    }

    public void setImageCount(String imageCount) {
        this.imageCount = imageCount;
    }

    public String getDoc10() {
        return doc10;
    }

    public void setDoc10(String doc10) {
        this.doc10 = doc10;
    }

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public String getEditRefresh() {
        return editRefresh;
    }

    public void setEditRefresh(String editRefresh) {
        this.editRefresh = editRefresh;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDoc9() {
        return doc9;
    }

    public void setDoc9(String doc9) {
        this.doc9 = doc9;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getDoc8() {
        return doc8;
    }

    public void setDoc8(String doc8) {
        this.doc8 = doc8;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public ReplyFor getReplyFor() {
        return replyFor;
    }

    public void setReplyFor(ReplyFor replyFor) {
        this.replyFor = replyFor;
    }
}