package jaohar.com.jaohar.models.profileandtabsmodels;

import com.google.gson.annotations.SerializedName;

public class AllNewsItem{

	@SerializedName("news_image")
	private String newsImage;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("news_text")
	private String newsText;

	@SerializedName("type")
	private String type;

	public void setNewsImage(String newsImage){
		this.newsImage = newsImage;
	}

	public String getNewsImage(){
		return newsImage;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setNewsText(String newsText){
		this.newsText = newsText;
	}

	public String getNewsText(){
		return newsText;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}
}