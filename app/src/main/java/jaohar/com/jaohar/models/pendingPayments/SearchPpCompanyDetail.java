package jaohar.com.jaohar.models.pendingPayments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class SearchPpCompanyDetail {

   @SerializedName("id")
   @Expose
   private String id;
   @SerializedName("company_name")
   @Expose
   private String companyName;
   @SerializedName("Address1")
   @Expose
   private String address1;
   @SerializedName("Address2")
   @Expose
   private String address2;
   @SerializedName("Address3")
   @Expose
   private String address3;
   @SerializedName("Address4")
   @Expose
   private String address4;
   @SerializedName("Address5")
   @Expose
   private String address5;
   @SerializedName("added_by")
   @Expose
   private String addedBy;
   @SerializedName("modification_date")
   @Expose
   private String modificationDate;
   @SerializedName("enable")
   @Expose
   private String enable;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getCompanyName() {
      return companyName;
   }

   public void setCompanyName(String companyName) {
      this.companyName = companyName;
   }

   public String getAddress1() {
      return address1;
   }

   public void setAddress1(String address1) {
      this.address1 = address1;
   }

   public String getAddress2() {
      return address2;
   }

   public void setAddress2(String address2) {
      this.address2 = address2;
   }

   public String getAddress3() {
      return address3;
   }

   public void setAddress3(String address3) {
      this.address3 = address3;
   }

   public String getAddress4() {
      return address4;
   }

   public void setAddress4(String address4) {
      this.address4 = address4;
   }

   public String getAddress5() {
      return address5;
   }

   public void setAddress5(String address5) {
      this.address5 = address5;
   }

   public String getAddedBy() {
      return addedBy;
   }

   public void setAddedBy(String addedBy) {
      this.addedBy = addedBy;
   }

   public String getModificationDate() {
      return modificationDate;
   }

   public void setModificationDate(String modificationDate) {
      this.modificationDate = modificationDate;
   }

   public String getEnable() {
      return enable;
   }

   public void setEnable(String enable) {
      this.enable = enable;
   }

}
