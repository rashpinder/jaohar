package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForumDetail{

	@SerializedName("IMO_number")
	private String iMONumber;

	@SerializedName("vessel_record_id")
	private String vesselRecordId;

	@SerializedName("heading")
	private String heading;

	@SerializedName("last_page")
	private String lastPage;

	@SerializedName("all_messages")
	private List<AllMessagesItem> allMessages;

	@SerializedName("pic")
	private String pic;

	@SerializedName("vessel_name")
	private String vesselName;

	@SerializedName("status")
	private String status;

	public String getIMONumber(){
		return iMONumber;
	}

	public String getVesselRecordId(){
		return vesselRecordId;
	}

	public String getHeading(){
		return heading;
	}

	public String getLastPage(){
		return lastPage;
	}

	public List<AllMessagesItem> getAllMessages(){
		return allMessages;
	}

	public String getPic(){
		return pic;
	}

	public String getVesselName(){
		return vesselName;
	}

	public String getStatus(){
		return status;
	}
}