package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class GetAllChatMessagesModel{

	@SerializedName("room_detail")
	private RoomDetail roomDetail;

	@SerializedName("data")
	private Data data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public RoomDetail getRoomDetail(){
		return roomDetail;
	}

	public Data getData(){
		return data;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}