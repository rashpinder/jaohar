package jaohar.com.jaohar.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataItem implements Serializable {

	@SerializedName("term_title")
	private String termTitle;

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("enable")
	private String enable;

	@SerializedName("modification_date")
	private String modificationDate;

	@SerializedName("term_id")
	private String termId;

	@SerializedName("term_desc")
	private String termDesc;

	public String getTermTitle(){
		return termTitle;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public String getEnable(){
		return enable;
	}

	public String getModificationDate(){
		return modificationDate;
	}

	public String getTermId(){
		return termId;
	}

	public String getTermDesc(){
		return termDesc;
	}


	public void setTermTitle(String termTitle) {
		this.termTitle = termTitle;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public void setModificationDate(String modificationDate) {
		this.modificationDate = modificationDate;
	}

	public void setTermId(String termId) {
		this.termId = termId;
	}

	public void setTermDesc(String termDesc) {
		this.termDesc = termDesc;
	}
}