package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllNotification {

    @SerializedName("notification_id")
    @Expose
    private String notificationId;
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("forum_id")
    @Expose
    private String forumId;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("notified_user")
    @Expose
    private String notifiedUser;
    @SerializedName("read_by")
    @Expose
    private String readBy;
    @SerializedName("creation_date")
    @Expose
    private String creationDate;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("status")
    @Expose
    private String status;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getForumId() {
        return forumId;
    }

    public void setForumId(String forumId) {
        this.forumId = forumId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getNotifiedUser() {
        return notifiedUser;
    }

    public void setNotifiedUser(String notifiedUser) {
        this.notifiedUser = notifiedUser;
    }

    public String getReadBy() {
        return readBy;
    }

    public void setReadBy(String readBy) {
        this.readBy = readBy;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
