package jaohar.com.jaohar.models.pendingPayments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PendingPaymentData {

    @SerializedName("all_invoices")
    @Expose
    private ArrayList<AllInvoice> allInvoices = null;
    @SerializedName("last_page")
    @Expose
    private String lastPage;

    public ArrayList<AllInvoice> getAllInvoices() {
        return allInvoices;
    }

    public void setAllInvoices(ArrayList<AllInvoice> allInvoices) {
        this.allInvoices = allInvoices;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }
}
