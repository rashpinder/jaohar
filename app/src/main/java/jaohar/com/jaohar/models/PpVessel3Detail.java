package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PpVessel3Detail {
   @SerializedName("vessel_id")
   @Expose
   private String vesselId;
   @SerializedName("vessel_name")
   @Expose
   private String vesselName;
   @SerializedName("IMO_no")
   @Expose
   private String iMONo;
   @SerializedName("flag")
   @Expose
   private String flag;
   @SerializedName("added_by")
   @Expose
   private String addedBy;
   @SerializedName("modification_date")
   @Expose
   private String modificationDate;
   @SerializedName("enable")
   @Expose
   private String enable;

   public String getVesselId() {
      return vesselId;
   }

   public void setVesselId(String vesselId) {
      this.vesselId = vesselId;
   }

   public String getVesselName() {
      return vesselName;
   }

   public void setVesselName(String vesselName) {
      this.vesselName = vesselName;
   }

   public String getIMONo() {
      return iMONo;
   }

   public void setIMONo(String iMONo) {
      this.iMONo = iMONo;
   }

   public String getFlag() {
      return flag;
   }

   public void setFlag(String flag) {
      this.flag = flag;
   }

   public String getAddedBy() {
      return addedBy;
   }

   public void setAddedBy(String addedBy) {
      this.addedBy = addedBy;
   }

   public String getModificationDate() {
      return modificationDate;
   }

   public void setModificationDate(String modificationDate) {
      this.modificationDate = modificationDate;
   }

   public String getEnable() {
      return enable;
   }

   public void setEnable(String enable) {
      this.enable = enable;
   }
}
