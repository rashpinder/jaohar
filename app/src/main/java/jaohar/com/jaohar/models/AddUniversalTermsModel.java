package jaohar.com.jaohar.models;

import com.google.gson.annotations.SerializedName;

public class AddUniversalTermsModel{

	@SerializedName("id")
	private int id;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public int getId(){
		return id;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}