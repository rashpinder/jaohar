package jaohar.com.jaohar.models.invoicedraftmodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllInvoicesItem{

	@SerializedName("all_data")
	private AllData allData;

	@SerializedName("search_company_data")
	private SearchCompanyData searchCompanyData;

	@SerializedName("sign_data")
	private SignData signData;

	@SerializedName("stamp_data")
	private StampData stampData;

	@SerializedName("itemdiscount")
	private List<ItemdiscountItem> itemdiscount;

	@SerializedName("search_vessel_data")
	private SearchVesselData searchVesselData;

	@SerializedName("payment_data")
	private PaymentData paymentData;

	@SerializedName("bank_data")
	private BankData bankData;

	@SerializedName("items_data")
	private List<ItemsDataItem> itemsData;

	public void setAllData(AllData allData){
		this.allData = allData;
	}

	public AllData getAllData(){
		return allData;
	}

	public void setSearchCompanyData(SearchCompanyData searchCompanyData){
		this.searchCompanyData = searchCompanyData;
	}

	public SearchCompanyData getSearchCompanyData(){
		return searchCompanyData;
	}

	public void setSignData(SignData signData){
		this.signData = signData;
	}

	public SignData getSignData(){
		return signData;
	}

	public void setStampData(StampData stampData){
		this.stampData = stampData;
	}

	public StampData getStampData(){
		return stampData;
	}

	public void setItemdiscount(List<ItemdiscountItem> itemdiscount){
		this.itemdiscount = itemdiscount;
	}

	public List<ItemdiscountItem> getItemdiscount(){
		return itemdiscount;
	}

	public void setSearchVesselData(SearchVesselData searchVesselData){
		this.searchVesselData = searchVesselData;
	}

	public SearchVesselData getSearchVesselData(){
		return searchVesselData;
	}

	public void setPaymentData(PaymentData paymentData){
		this.paymentData = paymentData;
	}

	public PaymentData getPaymentData(){
		return paymentData;
	}

	public void setBankData(BankData bankData){
		this.bankData = bankData;
	}

	public BankData getBankData(){
		return bankData;
	}

	public void setItemsData(List<ItemsDataItem> itemsData){
		this.itemsData = itemsData;
	}

	public List<ItemsDataItem> getItemsData(){
		return itemsData;
	}
}