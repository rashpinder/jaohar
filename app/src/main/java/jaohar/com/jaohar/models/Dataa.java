package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Dataa {

    @SerializedName("all_vessels")
    @Expose
    private ArrayList<AllVessel> allVessels = null;
    @SerializedName("last_page")
    @Expose
    private String lastPage;

    public ArrayList<AllVessel> getAllVessels() {
        return allVessels;
    }

    public void setAllVessels(ArrayList<AllVessel> allVessels) {
        this.allVessels = allVessels;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

}