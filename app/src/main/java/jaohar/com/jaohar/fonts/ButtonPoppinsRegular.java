package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

public class ButtonPoppinsRegular extends Button {
    /*
     * Getting Current Class Name
     * */

    private String mTag = ButtonPoppinsRegular.this.getClass().getSimpleName();

    public ButtonPoppinsRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public ButtonPoppinsRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public ButtonPoppinsRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public ButtonPoppinsRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsRegular(context).getFontFamily());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }
}
