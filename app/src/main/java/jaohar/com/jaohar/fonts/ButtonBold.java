package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

public class ButtonBold extends Button {
    /*
     * Getting Current Class Name
     * */

    private String mTag = ButtonBold.this.getClass().getSimpleName();

    public ButtonBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public ButtonBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public ButtonBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public ButtonBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new CalibriBold(context).getFontFamily());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }
}
