package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextProximaRegular extends EditText {
    public EditTextProximaRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextProximaRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextProximaRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextProximaRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ProximaRegular(context).getFontFamily());
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}

