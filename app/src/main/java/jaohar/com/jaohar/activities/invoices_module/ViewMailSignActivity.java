package jaohar.com.jaohar.activities.invoices_module;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;
import jaohar.com.jaohar.R;

public class ViewMailSignActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mail_sign);
        ButterKnife.bind(this);
    }
}