package jaohar.com.jaohar.activities.trash_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.trash_module_adapter.CompanyTrashAdapter;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteCompanyTrashInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteSelectedInterface;
import jaohar.com.jaohar.models.AllCompaniesTrashModel;
import jaohar.com.jaohar.models.CompaniesData;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class CompanyTrashActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private final Activity mActivity = CompanyTrashActivity.this;
    /*
     * Getting the Class Name
     * */

    String TAG = CompanyTrashActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     **/

    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    RecyclerView dataRV;

    ImageView recoverIV, deleteIV, emtyTrashIV;
    CompanyTrashAdapter mAdapter;


    /* *
     * Initialize the String and ArrayList
     * */
    private final String strLastPage = "FALSE";
    private String strICompanyID = "";
    String strIsClickFrom = "";
    ArrayList<CompaniesData> mArrayList = new ArrayList<CompaniesData>();
    //    ArrayList<CompaniesModel> mArrayList = new ArrayList<CompaniesModel>();
//    ArrayList<CompaniesModel> multiSelectArrayList = new ArrayList<CompaniesModel>();
    ArrayList<CompaniesData> multiSelectArrayList = new ArrayList<CompaniesData>();
    ArrayList<String> mRecordID = new ArrayList<String>();

    /**
     * To get Selected and unSelected Vessel Interface
     */
    RecoverDeleteSelectedInterface mRecoverDeleteSelected = new RecoverDeleteSelectedInterface() {
        @Override
        public void mrecoverDeletedCheckInterface(CompaniesData mModel, boolean isSelected) {
            if (mModel != null) {
                if (!isSelected) {
                    multiSelectArrayList.add(mModel);
                    String strID = mModel.getId();
                    mRecordID.add(mModel.getId());
                } else {
                    multiSelectArrayList.remove(mModel);
                    String strID = mModel.getId();
                    mRecordID.remove(mModel.getId());
                }
            }
        }
    };


    /*
     * To Recover and Delete Interface
     * */
    RecoverDeleteCompanyTrashInterface mrecoverDelete = new RecoverDeleteCompanyTrashInterface() {
        @Override
        public void mRecoverDelete(CompaniesData mModel, String strType) {
            strICompanyID = mModel.getId();
            if (strType.equals("recover")) {
                deleteConfirmDialog(getString(R.string.recover_trash_invoice_companty_), "recover");
            } else {
                deleteConfirmDialog(getString(R.string.are_you_sure_want_to_delete_invoice_company_), "delete");
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compant_trash);
    }

    /**
     * Set Widget IDS
     **/

    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        dataRV = findViewById(R.id.dataRV);
        recoverIV = findViewById(R.id.recoverIV);
        deleteIV = findViewById(R.id.deleteIV);
        emtyTrashIV = findViewById(R.id.emtyTrashIV);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.company_trash_screen));

        //Get Clicked Value
        if (getIntent() != null) {
            if (getIntent().getStringExtra("isClick").equals("manageUtilities")) {
                strIsClickFrom = getIntent().getStringExtra("isClick");
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {

            executeAPI();

        }
    }

    /**
     * Set Widget TextChangedListener & ClickListener
     */

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        emtyTrashIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mArrayList.size() > 0) {
                    deleteConfirmDialog(getString(R.string.clear_trash), "empty");
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_company_found_in_trash));

                }
            }
        });

        recoverIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if (multiSelectArrayList.size() > 0) {
                            executeRecoverSelectedTrash();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.there_is_no_selected_item));
                        }
                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_company_found_in_trash));
                }
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if (multiSelectArrayList.size() > 0) {
                            executeDeleteSelectedTrash();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }

                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_company_found_in_trash));

                }
            }
        });

    }

    /**
     * Call GetAllCompaniesTrash API for getting
     * list of all deleted companies
     *
     * @param
     * @user_id
     */
//    public void executeAPI() {
//        String strUrl = JaoharConstants.GetAllCompaniesTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//
//        AlertDialogManager.showProgressDialog(mActivity);
//
//
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseCOMTrash*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        parseResponse(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
    public void executeAPI() {
//        String strUrl = JaoharConstants.GetAllCompaniesTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AllCompaniesTrashModel> call1 = mApiInterface.getAllCompaniesTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<AllCompaniesTrashModel>() {
            @Override
            public void onResponse(Call<AllCompaniesTrashModel> call, retrofit2.Response<AllCompaniesTrashModel> response) {
                Log.e(TAG, "******ResponseCOMTrash*****" + response);
                AlertDialogManager.hideProgressDialog();
                mArrayList.clear();
                AllCompaniesTrashModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList = mModel.getData();
                    /*SetAdapter*/
                    setAdapter();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AllCompaniesTrashModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    private void parseResponse(String response) {
//        mArrayList.clear();
//        try {
//            JSONObject mJsonObject = new JSONObject(response);
//            String strStatus = mJsonObject.getString("status");
//            if (strStatus.equals("1")) {
//                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJson = mJsonArray.getJSONObject(i);
//                    CompaniesModel mModel = new CompaniesModel();
//
//                    if (!mJson.isNull("id")) {
//                        mModel.setId(mJson.getString("id"));
//                    }
//                    if (!mJson.isNull("company_name")) {
//                        mModel.setCompany_name(mJson.getString("company_name"));
//                    }
//                    if (!mJson.isNull("Address1")) {
//                        mModel.setAddress1(mJson.getString("Address1"));
//                    }
//                    if (!mJson.isNull("Address2")) {
//                        mModel.setAddress2(mJson.getString("Address2"));
//                    }
//                    if (!mJson.isNull("Address3")) {
//                        mModel.setAddress3(mJson.getString("Address3"));
//                    }
//                    if (!mJson.isNull("Address4")) {
//                        mModel.setAddress4(mJson.getString("Address4"));
//                    }
//                    if (!mJson.isNull("Address5")) {
//                        mModel.setAddress5(mJson.getString("Address5"));
//                    }
//                    if (!mJson.isNull("deleted_by")) {
//                        mModel.setDeleted_by(mJson.getString("deleted_by"));
//                    }
//
//                    mArrayList.add(mModel);
//                }
//
//                /*SetAdapter*/
//                setAdapter();
//
//            } else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void setAdapter() {
//        CompanyTrashAdapter
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        dataRV.setLayoutManager(layoutManager);
        mAdapter = new CompanyTrashAdapter(mActivity, mArrayList, mrecoverDelete, mRecoverDeleteSelected);
        dataRV.setAdapter(mAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent = new Intent(mActivity, TrashActivity.class);
        mIntent.putExtra("isClick", strIsClickFrom);
        startActivity(mIntent);
        finish();
    }

    public void deleteConfirmDialog(String strMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                if (strType.equals("empty")) {
                    /*Execute EMPTY API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeEmptyTrash();
                    }
                } else if (strType.equals("delete")) {
                    /*Execute Delete API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeDeleteTrash();
                    }
                } else {
                    /*Execute Recover API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeRecoverTrash();
                    }
                }
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /**
     * Implement API for Delete Invoice Company
     */
    private void executeDeleteTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteSingleCompanyTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), strICompanyID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mArrayList.clear();
                            executeAPI();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }


//    public void executeDeleteTrash() {
//        String strUrl = JaoharConstants.DeleteSingleCompanyTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&company_id=" + strICompanyID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseDeleteSingle*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Recover Invoice Company
     */
    private void executeRecoverTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.recoverSingleCompanyTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), strICompanyID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mArrayList.clear();
                            mRecordID.clear();
                            executeAPI();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }


//    public void executeRecoverTrash() {
//        String strUrl = JaoharConstants.RecoverSingleCompanyTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&company_id=" + strICompanyID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseDeleteSingle*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /*
     * Arrange Selected Array list and Get ID from that
     * */
    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mRecordID.size());
        for (int i = 0; i < mRecordID.size(); i++) {
            strData.add(mRecordID.get(i));
        }

        return strData;
    }

    /**
     * Implement API for Recover Selected Company
     */
    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("company_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeRecoverSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.recoverSelectedCompanyTrash(mParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList.clear();
                    mRecordID.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }


//    public void executeRecoverSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.RecoverSelectedCompanyTrash;
//
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("company_ids", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******responseRecover*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//
//                        mArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /* *
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Delete Selected Company
     */
    private Map<String, String> mRecoverParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("company_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeDeleteSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteSelectedCompanyTrash(mRecoverParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList.clear();
                    mRecordID.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }


//    public void executeDeleteSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.DeleteSelectedCompanyTrash;
//
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("company_ids", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******responseDelete*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//
//                        mArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Clear All Trash
     */
//    public void executeEmptyTrash() {
//        String strUrl = JaoharConstants.EmptyCompanyTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
////                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//
//                        mArrayList.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
    private void executeEmptyTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.emptyCompanyTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "")).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


}
