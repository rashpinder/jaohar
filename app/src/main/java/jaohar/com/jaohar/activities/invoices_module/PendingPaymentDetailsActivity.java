package jaohar.com.jaohar.activities.invoices_module;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;

import java.io.File;
import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.ImagesOnDetailsAdapter;
import jaohar.com.jaohar.fonts.ButtonRegular;
import jaohar.com.jaohar.fonts.TextViewMedium;
import jaohar.com.jaohar.fonts.TextViewPoppinsRegular;
import jaohar.com.jaohar.utils.JaoharConstants;

public class PendingPaymentDetailsActivity extends BaseActivity implements View.OnClickListener {

    public static String back = "false";
    public final int REQUEST_PERMISSIONS = 1;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    ImageView backImageView;
    RelativeLayout backRelativelayout;
    RecyclerView filesRecyclerView;
    TextViewMedium companyNameTextView, invoiceNumberTextView, vesselNumberTextView, amountDueTextView, remarksTextView;
    Intent intent;
    TextViewPoppinsRegular notAvailableTextView;
    String id = "", amount = "", vessel1 = "", remarks = "", pp_number = "", docs = "", currency = "";
    ArrayList<String> docsArrayList = new ArrayList<>();
    ImagesOnDetailsAdapter mAddDocumentsAdapter;
    ButtonRegular btn_edit, btn_download;
    String pp_date = "", companyName = "", amt_due = "", status = "",
            vessel2 = "", vessel3 = "", company_id = "", vessel1_id = "", vessel2_id = "", vessel3_id = "", position;
    String mPDF = "", mPDFName = "";

    @Override
    protected void onResume() {
        super.onResume();
        if (back.equals("true")) {
            back = "false";
            onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_payment_details);
        initViews();
        intent = getIntent();
        if (intent.hasExtra("id")) {
            id = intent.getStringExtra("id");
            pp_number = intent.getStringExtra("pp_number");
            company_id = intent.getStringExtra("company_id");
            pp_date = intent.getStringExtra("pp_date");
            companyName = intent.getStringExtra("companyName");
            vessel1 = intent.getStringExtra("vessel1");
            vessel2 = intent.getStringExtra("vessel2");
            vessel3 = intent.getStringExtra("vessel3");
            vessel1_id = intent.getStringExtra("vessel1_id");
            vessel2_id = intent.getStringExtra("vessel2_id");
            vessel3_id = intent.getStringExtra("vessel3_id");
            amt_due = intent.getStringExtra("amt_due");
            remarks = intent.getStringExtra("remarks");
            currency = intent.getStringExtra("currency");
            status = intent.getStringExtra("status");
            position = intent.getStringExtra("position");

            docsArrayList = (ArrayList<String>) getIntent().getSerializableExtra("docs");
            for (int i = 0; i < docsArrayList.size(); i++) {
                if (docsArrayList.get(i).contains(".pdf")) {
                    mPDF = docsArrayList.get(i);
                }
            }

            companyNameTextView.setText(companyName);
            invoiceNumberTextView.setText(pp_number);
            vesselNumberTextView.setText(vessel1 + ", " + vessel2 + ", " + vessel3);

            amountDueTextView.setText(amt_due + ".00 " + currency);

            if (!remarks.equals("") || remarks != null) {
                remarksTextView.setText(remarks);
            } else {
                remarksTextView.setText("NA");
            }

            if (docsArrayList.size() != 0) {
                notAvailableTextView.setVisibility(View.GONE);
                filesRecyclerView.setVisibility(View.VISIBLE);
                mAddDocumentsAdapter = new ImagesOnDetailsAdapter(getApplicationContext(), docsArrayList, id);
                filesRecyclerView.setAdapter(mAddDocumentsAdapter);
            } else {
                filesRecyclerView.setVisibility(View.GONE);
                notAvailableTextView.setVisibility(View.VISIBLE);
            }
        }
        listeners();
    }


    private void executePDFWithPermission(String position) {
        if (checkPermission()) {
            mDownloadPDFMethod(mPDF, "MyPdf" + position, position);
        } else {
            requestPermission();
        }
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS);
    }


    private void mDownloadPDFMethod(String mPDF, String mPDFName, String position) {
        showProgressDialog(this);
        downloadPDF(mPDF, outputPath(), mPDFName, position);
    }


    private String outputPath() {
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath()
                + "/jaohar");
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        } else {
            folder.delete();
            folder.mkdirs();
        }
        return path;
    }


    private void downloadPDF(String url, String dirPath, String fileName, String position) {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("PRDOWNLOADER", "Download completed at::::" + dirPath);
                        hideProgressDialog();
                        showPDFAlertDialog(PendingPaymentDetailsActivity.this, getResources().getString(R.string.app_name), "Downloaded Successfully!", position);
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("PRDOWNLOADER", "Download failed at::::" + dirPath);
                        hideProgressDialog();
                        showAlertDialog(PendingPaymentDetailsActivity.this, getResources().getString(R.string.app_name), "PDF format is not correct!");
                    }
                });
    }

    /*
     * Show Alert Dailog Box
     * */
    public void showPDFAlertDialog(Activity mActivity, String strTitle, String strMessage, String position) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
//                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
//                mIntent.putExtra("SubjectName", "Invoice    " +  modelArrayList.get(position).getInvoice_number() +" "+modelArrayList.get(position).getmVesselSearchInvoiceModel().getVessel_name());
//                mIntent.putExtra("vesselID", modelArrayList.get(position).getInvoice_id());
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        JaoharConstants.Invoice_ID = modelArrayList.get(Integer.parseInt(position)).getAllData().getPpId();
//                        mIntent.putExtra("Model", modelArrayList.get(Integer.parseInt(position)));
//                        startActivity(mIntent);
//                    }
//                },500);

//                mIntent.putExtra("Model", modelArrayList);

            }
        });
        alertDialog.show();
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(PendingPaymentDetailsActivity.this, WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(PendingPaymentDetailsActivity.this, READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }


    private void listeners() {
//        backImageView.setOnClickListener(this);
        backRelativelayout.setOnClickListener(this);
        btn_download.setOnClickListener(this);
        btn_edit.setOnClickListener(this);
    }


    private void initViews() {
        backImageView = findViewById(R.id.backImageView);
        backRelativelayout = findViewById(R.id.backRelativelayout);
        companyNameTextView = findViewById(R.id.companyNameTextView);
        invoiceNumberTextView = findViewById(R.id.invoiceNumberTextView);
        vesselNumberTextView = findViewById(R.id.vesselNumberTextView);
        amountDueTextView = findViewById(R.id.amountDueTextView);
        remarksTextView = findViewById(R.id.remarksTextView);
        notAvailableTextView = findViewById(R.id.notAvailableTextView);
        filesRecyclerView = findViewById(R.id.filesRecyclerView);
        btn_edit = findViewById(R.id.btn_edit);
        btn_download = findViewById(R.id.btn_download);
        filesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backRelativelayout:
                onBackPressed();
                break;

            case R.id.btn_download:
                Log.e("DownaloadbaleLink", JaoharConstants.DEV_SERVER_URL + id);
                String url = JaoharConstants.DEV_SERVER_URL + id;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.btn_edit:
                openAddPendingPaymentActivity();
                break;


        }
    }

    private void openAddPendingPaymentActivity() {
        Intent mIntent = new Intent(PendingPaymentDetailsActivity.this, AddPendingPaymentsActivity.class);
        mIntent.putExtra("id", id);
        mIntent.putExtra("pp_number", pp_number);
        mIntent.putExtra("company_id", company_id);
        mIntent.putExtra("pp_date", pp_date);
        mIntent.putExtra("companyName", companyName);
        mIntent.putExtra("vessel1", vessel1);
        mIntent.putExtra("vessel2", vessel2);
        mIntent.putExtra("vessel3", vessel3);
        mIntent.putExtra("vessel1_id", vessel1_id);
        mIntent.putExtra("vessel2_id", vessel2_id);
        mIntent.putExtra("vessel3_id", vessel3_id);
        mIntent.putExtra("amt_due", amt_due);
        mIntent.putExtra("remarks", remarks);
        mIntent.putExtra("currency", currency);
        mIntent.putExtra("status", status);
        mIntent.putExtra("from", "detail");
        mIntent.putStringArrayListExtra("docs", docsArrayList);
        startActivity(mIntent);
    }
}