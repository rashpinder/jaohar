package jaohar.com.jaohar.activities;

import static android.os.Build.VERSION.SDK_INT;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.fiberlink.maas360.android.richtexteditor.RichEditText;
import com.fiberlink.maas360.android.richtexteditor.RichTextActions;
import com.tom_roush.pdfbox.io.IOUtils;
import com.vincent.filepicker.Constant;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.AddImageListAdapter;
import jaohar.com.jaohar.adapters.EditDocumentsAdapter;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.interfaces.DeleteDocumentVessels;
import jaohar.com.jaohar.interfaces.DeleteImagesVessels;
import jaohar.com.jaohar.models.AddVesselmodel;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.GetVesselClassData;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class EditVesselActivity extends BaseActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    public static boolean IS_First_Click = false;
    private final String cameraStr = Manifest.permission.CAMERA;
    private final String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;
    private final String writeStorageStr = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    RichTextActions ownerTextActions;
    RichTextActions remarksTextActions;
    RichTextActions machineryTextActions;
    Activity mActivity = EditVesselActivity.this;
    String TAG = EditVesselActivity.this.getClass().getSimpleName(), strHyperLink;
    ImageView imgBack;
    LinearLayout llLeftLL;
    TextView txtCenter;
    EditText editVesselNameET, editIMONoET, editVesselTypeET, editYearBuiltET, editPlaceBuiltET, editClassET, editFlagET,
            editPriceIdeaET, editShortDescriptionET, editDWTET, editLoaET, editStatusET, editVesselBreathET, editVesselDepthET,
            editVesselLocationET, editCurrencyET, editBaleET, editGrainET, editDateET, editVesselTEUET,
            editLiquidET, editVesselPassangerET, editGasET, editVesselGrosssTonnageET, editNetTonnageET, mmsiET,
            editBuilderET, editVesselLenghtOverallET, editDraughtET, editHyperLinkET, editlightshipET,
            editTcmET, editGearedET, editOffMarketET;
    RichEditText mRichEditText, machinery_edit_text, owner_edit_text, remarks_edit_text;
    RichTextActions richTextActions;
    LinearLayout layoutFullDescriptionLL;
    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    Button btnAdd;
    ArrayList<GetVesselClassData> modelArrayList = new ArrayList<GetVesselClassData>();
    LinearLayout layoutAddImagesLL;
    RecyclerView galleryRecyclerView;
    Spinner spinnerGeared;
    String strGeared = "";
    AddImageListAdapter mAdapter;
    ArrayList<AddGalleryImagesModel> mGalleryArrayList = new ArrayList<AddGalleryImagesModel>();
    String[] arrayVesselsType, arrayStatus, arrayCurrency, arrayGeared;
    String mCurrentPhotoPath, mStoragePath = "";
    AllVessel mVesselesModel;
    ArrayList<String> mArrayListtClass = new ArrayList<>();
    String strTitleToolbar = "";
    boolean isOnResumeFirst = false;
    LinearLayout llFocusLL;
    String strRenderHtmlContent = "", strMachineryContent = "", strOwnerDetails = "", strRemarks = "";
    byte[] strByteArray;
    String strDocumentName = "";
    String strDocumentPath = "";
    TextView txtAttachDocument;
    RecyclerView mDocumentRecyclerView;
    //    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    String docEx1 = null, docEx2 = null, docEx3 = null, docEx4 = null, docEx5 = null, docEx6 = null, docEx7 = null, docEx8 = null, docEx9 = null, docEx10 = null;
    EditDocumentsAdapter mEditDocumentsAdapter;
    byte[] doc1 = null, doc2 = null, doc3 = null, doc4 = null, doc5 = null, doc6 = null, doc7 = null, doc8 = null, doc9 = null, doc10 = null, doc11 = null, doc12 = null, doc13 = null, doc14 = null, doc15 = null, doc16 = null, doc17 = null, doc18 = null, doc19 = null, doc20 = null, doc21 = null, doc22 = null, doc23 = null, doc24 = null, doc25 = null, doc26 = null, doc27 = null, doc28 = null, doc29 = null, doc30 = null;
    String doc1name = null, doc2name = null, doc3name = null, doc4name = null, doc5name = null, doc6name = null, doc7name = null, doc8name = null, doc9name = null, doc10name = null, doc11name = null, doc12name = null, doc13name = null, doc14name = null, doc15name = null, doc16name = null, doc17name = null, doc18name = null, doc19name = null, doc20name = null, doc21name = null, doc22name = null, doc23name = null, doc24name = null, doc25name = null, doc26name = null, doc27name = null, doc28name = null, doc29name = null, doc30name = null;
    String pic1 = null, pic2 = null, pic3 = null, pic4 = null, pic5 = null, pic6 = null, pic7 = null, pic8 = null, pic9 = null, pic10 = null;
    String docc1 = null, docc2 = null, docc3 = null, docc4 = null, docc5 = null, docc6 = null, docc7 = null, docc8 = null, docc9 = null, docc10 = null;
    ArrayList<GetVesselTypeData> mArrayListClass = new ArrayList<GetVesselTypeData>();
    ArrayList<String> modellArrayList = new ArrayList<>();
    RequestBody url;
    String strRecordID;
    Dialog cameraGalleryDialog;
    RequestBody docu1 = null;
    RequestBody docu2 = null;
    RequestBody docu3 = null;
    RequestBody docu4 = null;
    RequestBody docu5 = null;
    RequestBody docu6 = null;
    RequestBody docu7 = null;
    RequestBody docu8 = null;
    RequestBody docu9 = null;
    RequestBody docu10 = null;
    RequestBody picc1 = null;
    RequestBody picc2 = null;
    RequestBody picc3 = null;
    RequestBody picc4 = null;
    RequestBody picc5 = null;
    RequestBody picc6 = null;
    RequestBody picc7 = null;
    RequestBody picc8 = null;
    RequestBody picc9 = null;
    RequestBody picc10 = null;
    Bitmap bitmap1, bitmap2, bitmap3, bitmap4, bitmap5, bitmap6, bitmap7, bitmap8, bitmap9, bitmap10;
    private long mLastClickTab1 = 0;    /*
    delete vessel interface
     */

    /**
     * Turn bitmap into byte array
     *
     * @param bitmap data
     * @returen byte array
     */
    public static byte[] getFileDataFromBitmap(Context context, Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }    /*
   delete images Interface
     */

    private static byte[] getByteImageFromURL(URL url) {
        try {
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.connect();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(conn.getInputStream(), baos);
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }    DeleteDocumentVessels mDeleteDocumentInterface = new DeleteDocumentVessels() {
        @Override
        public void deleteDocumentVessels(DocumentModel model, int strPosition) {
            if (documentArrayList != null) {
                documentArrayList.remove(strPosition);
                setDocumentAdapter(documentArrayList);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_vessel);
/*
set status bar
 */
        setStatusBar();

        /*
        set views Id
         */

        setViewIDs();
        /*
        click listeners
         */
        setClickListner();

        editVesselNameET.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        if (cs.toString().matches("[a-zA-Z 0-9]+")) {
                            return cs;
                        }
                        return "";
                    }
                }
        });
    }

    private void setViewIDs() {
        llFocusLL = findViewById(R.id.llFocusLL);
        llFocusLL.requestFocus();
        arrayVesselsType = mActivity.getResources().getStringArray(R.array.vessel_type_array);
        arrayStatus = mActivity.getResources().getStringArray(R.array.status_array_user);
        arrayCurrency = mActivity.getResources().getStringArray(R.array.currency_array);
        arrayGeared = mActivity.getResources().getStringArray(R.array.geared_array);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText("Edit Vessel");
        mmsiET = findViewById(R.id.mmsiET);
        editOffMarketET = findViewById(R.id.editOffMarketET);
        editOffMarketET.setKeyListener(null);
        layoutAddImagesLL = findViewById(R.id.layoutAddImagesLL);
        galleryRecyclerView = findViewById(R.id.galleryRecyclerView);
        editVesselNameET = findViewById(R.id.editVesselNameET);
        editIMONoET = findViewById(R.id.editIMONoET);
        editVesselTypeET = findViewById(R.id.editVesselTypeET);
        editVesselTypeET.setKeyListener(null);
        editYearBuiltET = findViewById(R.id.editYearBuiltET);
        editYearBuiltET.setKeyListener(null);
        editPlaceBuiltET = findViewById(R.id.editPlaceBuiltET);
        editClassET = findViewById(R.id.editClassET);
        editFlagET = findViewById(R.id.editFlagET);
        editPriceIdeaET = findViewById(R.id.editPriceIdeaET);
        editShortDescriptionET = findViewById(R.id.editShortDescriptionET);
        editDWTET = findViewById(R.id.editDWTET);
//        editLoaET = (EditText) findViewById(R.id.editLoaET);
        editStatusET = findViewById(R.id.editStatusET);
        editStatusET.setKeyListener(null);
        editGearedET = findViewById(R.id.editGearedET);
        editGearedET.setKeyListener(null);
        editVesselBreathET = findViewById(R.id.editVesselBreathET);
        editVesselDepthET = findViewById(R.id.editVesselDepthET);
        editVesselLocationET = findViewById(R.id.editVesselLocationET);
        editCurrencyET = findViewById(R.id.editCurrencyET);
        editCurrencyET.setKeyListener(null);
        editBaleET = findViewById(R.id.editBaleET);
        editGrainET = findViewById(R.id.editGrainET);
        editDateET = findViewById(R.id.editDateET);
        editDateET.setKeyListener(null);
        editVesselTEUET = findViewById(R.id.editVesselTEUET);
        editLiquidET = findViewById(R.id.editLiquidET);
        editVesselPassangerET = findViewById(R.id.editVesselPassangerET);
        editGasET = findViewById(R.id.editGasET);
        editVesselGrosssTonnageET = findViewById(R.id.editVesselGrosssTonnageET);
        editNetTonnageET = findViewById(R.id.editNetTonnageET);
        editBuilderET = findViewById(R.id.editBuilderET);
        editVesselLenghtOverallET = findViewById(R.id.editVesselLenghtOverallET);
        editDraughtET = findViewById(R.id.editDraughtET);
        editHyperLinkET = findViewById(R.id.editHyperLinkET);
        mRichEditText = findViewById(R.id.rich_edit_text);
        mRichEditText.setOnKeyListener(null);
        editlightshipET = findViewById(R.id.editlightshipET);
        editTcmET = findViewById(R.id.editTcmET);
        ownerTextActions = findViewById(R.id.ownerTextActions);
        machineryTextActions = findViewById(R.id.machineryTextActions);
        remarksTextActions = findViewById(R.id.remarksTextActions);
        layoutFullDescriptionLL = findViewById(R.id.layoutFullDescriptionLL);
        btnAdd = findViewById(R.id.btnAdd);
        txtAttachDocument = findViewById(R.id.txtAttachDocument);
        mDocumentRecyclerView = findViewById(R.id.mDocumentRecyclerView);

//        editlightshipET.setText("0");
//        editTcmET.setText("0");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplication().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }

/*
rich edit texts
 */
        richTextActions = findViewById(R.id.richTextActions);
        mRichEditText.setRichTextActionsView(richTextActions);
        mRichEditText.setPadding(6, 0, 6, 6);

        machinery_edit_text = findViewById(R.id.machinery_edit_text);
        machinery_edit_text.setRichTextActionsView(machineryTextActions);
        machinery_edit_text.setPadding(6, 0, 6, 6);

        owner_edit_text = findViewById(R.id.owner_edit_text);
        owner_edit_text.setRichTextActionsView(ownerTextActions);
        owner_edit_text.setPadding(6, 0, 6, 6);

        remarks_edit_text = findViewById(R.id.remarks_edit_text);
        remarks_edit_text.setRichTextActionsView(remarksTextActions);
        remarks_edit_text.setPadding(6, 0, 6, 6);

        spinnerGeared = findViewById(R.id.spinnerGeared);
        setUpGearedSpinnerAdapter();
        if (getIntent() != null) {
            strTitleToolbar = getIntent().getStringExtra("Title");
            if (strTitleToolbar.equals("Edit Vessels")) {
                mGalleryArrayList.clear();
                documentArrayList.clear();
                mVesselesModel = (AllVessel) getIntent().getSerializableExtra("Model");
                strRecordID = mVesselesModel.getRecordId();
                strRenderHtmlContent = mVesselesModel.getFullDescription();
            }
        }

        /*Setting Up Data on Widgets
         * When Vessel Item is Edited
         * */
        if (mVesselesModel != null && strTitleToolbar.equals("Edit Vessels"))
            editSetDataonWidgets(mVesselesModel);
    }

    private void setUpGearedSpinnerAdapter() {
        List<String> list = new ArrayList<String>();
        list.add("Select Geared");
        list.add("Yes");
        list.add("No");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGeared.setAdapter(dataAdapter);
        spinnerGeared.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strGeared = adapterView.getItemAtPosition(i).toString();
                if (strGeared.equals("Select Geared")) {
                    strGeared = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }    DeleteImagesVessels mDeleteImagesInterface = new DeleteImagesVessels() {
        @Override
        public void deleteImagesVessels(AddGalleryImagesModel model, int strPosition) {
            if (mGalleryArrayList != null) {
                mGalleryArrayList.remove(strPosition);
                setUpGalleryAdapter();
            }
        }
    };

    private void editSetDataonWidgets(AllVessel mVesselesModel) {
        editVesselNameET.setText(mVesselesModel.getVesselName());
        editIMONoET.setText(mVesselesModel.getIMONumber());
        editVesselTypeET.setText(mVesselesModel.getVesselType());
        editVesselTypeET.setKeyListener(null);
        editYearBuiltET.setText(mVesselesModel.getYearBuilt());
        editYearBuiltET.setKeyListener(null);
        editPlaceBuiltET.setText(mVesselesModel.getPlaceOfBuilt());
        editClassET.setText(mVesselesModel.getClass_());
        editFlagET.setText(mVesselesModel.getFlag());
        editFlagET.requestFocus();
        editPriceIdeaET.setText(mVesselesModel.getPriceIdea());
        editShortDescriptionET.setText(mVesselesModel.getShortDescription());
        editDWTET.setText(mVesselesModel.getCapacity());
//        editLoaET.setText(mVesselesModel.getLOA_M());
        editStatusET.setText(mVesselesModel.getStatus());
        editStatusET.setKeyListener(null);
        editGearedET.setText(mVesselesModel.getGeared());
        editGearedET.setKeyListener(null);
        editVesselBreathET.setText(mVesselesModel.getBreadth());
        editVesselDepthET.setText(mVesselesModel.getDepth());
        editVesselLocationET.setText(mVesselesModel.getVesselLocation());
        editCurrencyET.setText(mVesselesModel.getCurrency());
        editCurrencyET.setKeyListener(null);
        editBaleET.setText(mVesselesModel.getBale());
        editGrainET.setText(mVesselesModel.getGrain());
        editDateET.setText(mVesselesModel.getDateForVessel());
        editNetTonnageET.setText(mVesselesModel.getNetTonnage());
        editDraughtET.setText(mVesselesModel.getDraught());
        editBuilderET.setText(mVesselesModel.getBuilder());
        editGasET.setText(mVesselesModel.getGas());
        editLiquidET.setText(mVesselesModel.getLiquid());
        editHyperLinkET.setText(mVesselesModel.getUrl());
        strHyperLink = mVesselesModel.getUrl();
        editVesselLenghtOverallET.setText(mVesselesModel.getLoa());
        editVesselGrosssTonnageET.setText(mVesselesModel.getGrossTonnage());
        editVesselTEUET.setText(mVesselesModel.getTeu());
        editVesselPassangerET.setText(mVesselesModel.getNoPassengers());
        editDateET.setKeyListener(null);
        editlightshipET.setText(mVesselesModel.getLdt());
        editTcmET.setText(mVesselesModel.getTcm());

        mmsiET.setText(mVesselesModel.getMmsiNo());
        editOffMarketET.setText(mVesselesModel.getOffMarket());
        editOffMarketET.setKeyListener(null);

        if (mVesselesModel.getMachineryDetail() != null && !mVesselesModel.getMachineryDetail().equals(""))
            machinery_edit_text.setHtml(String.valueOf(html2text(mVesselesModel.getMachineryDetail())));

        if (mVesselesModel.getOwnerDetail() != null && !mVesselesModel.getOwnerDetail().equals(""))
            owner_edit_text.setHtml(String.valueOf(html2text(mVesselesModel.getOwnerDetail())));

        if (mVesselesModel.getRemarks() != null && !mVesselesModel.getRemarks().equals(""))
            remarks_edit_text.setHtml(String.valueOf(html2text(mVesselesModel.getUser_remark())));

        mRichEditText.setHtml(String.valueOf(html2text(mVesselesModel.getFullDescription())));

        btnAdd.setText("Update");

        if (mVesselesModel.getPhoto1().length() > 0 && mVesselesModel.getPhoto1().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto1());
            mGallaryModel.setStrImageCount("1");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto2().length() > 0 && mVesselesModel.getPhoto2().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto2());
            mGallaryModel.setStrImageCount("2");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto3().length() > 0 && mVesselesModel.getPhoto3().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto3());
            mGallaryModel.setStrImageCount("3");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto4().length() > 0 && mVesselesModel.getPhoto4().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto4());
            mGallaryModel.setStrImageCount("4");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto5().length() > 0 && mVesselesModel.getPhoto5().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto5());
            mGallaryModel.setStrImageCount("5");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto6().length() > 0 && mVesselesModel.getPhoto6().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto6());
            mGallaryModel.setStrImageCount("6");

            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto7().length() > 0 && mVesselesModel.getPhoto7().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto7());
            mGallaryModel.setStrImageCount("7");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto8().length() > 0 && mVesselesModel.getPhoto8().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto8());
            mGallaryModel.setStrImageName("8");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto9().length() > 0 && mVesselesModel.getPhoto9().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto9());
            mGallaryModel.setStrImageCount("9");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto10().length() > 0 && mVesselesModel.getPhoto10().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto10());
            mGallaryModel.setStrImageCount("10");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto11().length() > 0 && mVesselesModel.getPhoto11().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto11());
            mGallaryModel.setStrImageCount("11");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto12().length() > 0 && mVesselesModel.getPhoto12().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto12());
            mGallaryModel.setStrImageCount("12");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto13().length() > 0 && mVesselesModel.getPhoto13().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto13());
            mGallaryModel.setStrImageCount("13");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto14().length() > 0 && mVesselesModel.getPhoto14().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto14());
            mGallaryModel.setStrImageCount("14");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto15().length() > 0 && mVesselesModel.getPhoto15().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto15());
            mGallaryModel.setStrImageCount("15");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto16().length() > 0 && mVesselesModel.getPhoto16().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto16());
            mGallaryModel.setStrImageCount("16");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto17().length() > 0 && mVesselesModel.getPhoto17().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto17());
            mGallaryModel.setStrImageCount("17");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto18().length() > 0 && mVesselesModel.getPhoto18().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto18());
            mGallaryModel.setStrImageCount("18");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto19().length() > 0 && mVesselesModel.getPhoto19().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto19());
            mGallaryModel.setStrImageCount("19");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto20().length() > 0 && mVesselesModel.getPhoto20().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto20());
            mGallaryModel.setStrImageCount("20");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto21().length() > 0 && mVesselesModel.getPhoto21().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto21());
            mGallaryModel.setStrImageCount("21");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto22().length() > 0 && mVesselesModel.getPhoto22().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto22());
            mGallaryModel.setStrImageCount("22");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto23().length() > 0 && mVesselesModel.getPhoto23().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto23());
            mGallaryModel.setStrImageCount("23");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto24().length() > 0 && mVesselesModel.getPhoto24().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto24());
            mGallaryModel.setStrImageCount("24");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto25().length() > 0 && mVesselesModel.getPhoto25().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto25());
            mGallaryModel.setStrImageCount("25");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto26().length() > 0 && mVesselesModel.getPhoto26().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto26());
            mGallaryModel.setStrImageCount("26");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto27().length() > 0 && mVesselesModel.getPhoto27().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto27());
            mGallaryModel.setStrImageCount("27");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto28().length() > 0 && mVesselesModel.getPhoto28().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto28());
            mGallaryModel.setStrImageCount("28");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto29().length() > 0 && mVesselesModel.getPhoto29().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto29());
            mGallaryModel.setStrImageCount("29");
            mGalleryArrayList.add(mGallaryModel);
        }
        if (mVesselesModel.getPhoto30().length() > 0 && mVesselesModel.getPhoto30().contains("http")) {
            AddGalleryImagesModel mGallaryModel = new AddGalleryImagesModel();
            mGallaryModel.setStrImagePath(mVesselesModel.getPhoto30());
            mGallaryModel.setStrImageCount("30");
            mGalleryArrayList.add(mGallaryModel);
        }

        //Set Up Ship Images
        setUpGalleryAdapter();

        strGeared = mVesselesModel.getGeared();
        if (strGeared.equals("Yes")) {
            spinnerGeared.setSelection(1);
        } else {
            spinnerGeared.setSelection(2);
        }
        if (mVesselesModel.getDocument1() != null) {
            if (mVesselesModel.getDocument1().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument1());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument1name());
                mDocumentModel.setDocumunetCount("1");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument2() != null) {
            if (mVesselesModel.getDocument2().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument2());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument2name());
                mDocumentModel.setDocumunetCount("2");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument3() != null) {
            if (mVesselesModel.getDocument3().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument3());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument3name());
                mDocumentModel.setDocumunetCount("3");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument4() != null) {
            if (mVesselesModel.getDocument4().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument4());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument4name());
                mDocumentModel.setDocumunetCount("4");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument5() != null) {
            if (mVesselesModel.getDocument5().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument5());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument5name());
                mDocumentModel.setDocumunetCount("5");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument6() != null) {
            if (mVesselesModel.getDocument6().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument6());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument6name());
                mDocumentModel.setDocumunetCount("6");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument7() != null) {
            if (mVesselesModel.getDocument7().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument7());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument7name());
                mDocumentModel.setDocumunetCount("7");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument8() != null) {
            if (mVesselesModel.getDocument8().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument8());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument8name());
                mDocumentModel.setDocumunetCount("8");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument9() != null) {
            if (mVesselesModel.getDocument9().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument9());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument9name());
                mDocumentModel.setDocumunetCount("9");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument10() != null) {
            if (mVesselesModel.getDocument10().length() > 0) {
                DocumentModel mDocumentModel = new DocumentModel();
                mDocumentModel.setDocumentPath(mVesselesModel.getDocument10());
                mDocumentModel.setDocumentName(mVesselesModel.getDocument10name());
                mDocumentModel.setDocumunetCount("10");
                documentArrayList.add(mDocumentModel);
            }
        }
        if (mVesselesModel.getDocument11().length() > 0) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument11());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument11name());
            mDocumentModel.setDocumunetCount("11");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument12().length() > 0 && mVesselesModel.getDocument12().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument12());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument12name());
            mDocumentModel.setDocumunetCount("12");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument13().length() > 0 && mVesselesModel.getDocument13().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument13());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument13name());
            mDocumentModel.setDocumunetCount("13");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument14().length() > 0 && mVesselesModel.getDocument14().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument14());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument14name());
            mDocumentModel.setDocumunetCount("14");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument15().length() > 0 && mVesselesModel.getDocument15().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument15());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument15name());
            mDocumentModel.setDocumunetCount("15");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument16().length() > 0 && mVesselesModel.getDocument16().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument16());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument16name());
            mDocumentModel.setDocumunetCount("16");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument17().length() > 0 && mVesselesModel.getDocument17().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument17());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument17name());
            mDocumentModel.setDocumunetCount("17");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument18().length() > 0 && mVesselesModel.getDocument18().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument18());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument18name());
            mDocumentModel.setDocumunetCount("18");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument19().length() > 0 && mVesselesModel.getDocument19().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument19());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument19name());
            mDocumentModel.setDocumunetCount("19");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument20().length() > 0 && mVesselesModel.getDocument20().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument20());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument20name());
            mDocumentModel.setDocumunetCount("20");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument21().length() > 0 && mVesselesModel.getDocument21().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument21());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument21name());
            mDocumentModel.setDocumunetCount("21");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument22().length() > 0 && mVesselesModel.getDocument22().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument22());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument22name());
            mDocumentModel.setDocumunetCount("22");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument23().length() > 0 && mVesselesModel.getDocument23().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument23());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument23name());
            mDocumentModel.setDocumunetCount("23");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument24().length() > 0 && mVesselesModel.getDocument24().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument24());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument24name());
            mDocumentModel.setDocumunetCount("24");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument25().length() > 0 && mVesselesModel.getDocument25().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument25());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument25name());
            mDocumentModel.setDocumunetCount("25");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument26().length() > 0 && mVesselesModel.getDocument26().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument26());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument26name());
            mDocumentModel.setDocumunetCount("26");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument27().length() > 0 && mVesselesModel.getDocument27().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument27());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument27name());
            mDocumentModel.setDocumunetCount("27");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument28().length() > 0 && mVesselesModel.getDocument28().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument28());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument28name());
            mDocumentModel.setDocumunetCount("28");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument29().length() > 0 && mVesselesModel.getDocument29().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument29());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument29name());
            mDocumentModel.setDocumunetCount("29");
            documentArrayList.add(mDocumentModel);
        }
        if (mVesselesModel.getDocument30().length() > 0 && mVesselesModel.getDocument30().contains("http")) {
            DocumentModel mDocumentModel = new DocumentModel();
            mDocumentModel.setDocumentPath(mVesselesModel.getDocument30());
            mDocumentModel.setDocumentName(mVesselesModel.getDocument30name());
            mDocumentModel.setDocumunetCount("30");
            documentArrayList.add(mDocumentModel);
        }

        //Set Up Documents
        setDocumentAdapter(documentArrayList);
    }

    public void setDocumentAdapter(ArrayList<DocumentModel> mArrayList) {
        mDocumentRecyclerView.setNestedScrollingEnabled(false);
        mDocumentRecyclerView.setLayoutManager(new LinearLayoutManager(EditVesselActivity.this));
        mEditDocumentsAdapter = new EditDocumentsAdapter(mActivity, mArrayList, mDeleteDocumentInterface);
        mDocumentRecyclerView.setAdapter(mEditDocumentsAdapter);
    }

    @SuppressLint("ClickableViewAccessibility")
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        layoutAddImagesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "mGalleryArrayList.size(): " + mGalleryArrayList.size());
                if (mGalleryArrayList.size() <= 10 - 1) {
                    setUpCameraGalleryDialog();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit));
                }
            }
        });


        txtAttachDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (documentArrayList.size() < 10) {
                    openInternalStorage();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
                }
            }
        });

//        txtAttachDocument.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (checkPermission()) {
//                if (documentArrayList.size() <= 10 - 1) {
//                    openInternalStorage();
//                } else {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
//                }}
//                 else {
//                        requestPermission();
//
//                    }}
//        });

        editVesselTypeET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    gettingVessal_Type();
                    return true;
                }
                return false;
            }
        });

        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Currency", arrayCurrency, editCurrencyET);
                    return true;
                }
                return false;
            }
        });

        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editGearedET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Geared", arrayGeared, editGearedET);
                    return true;
                }
                return false;
            }
        });
        editOffMarketET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Off Market", arrayGeared, editOffMarketET);
                    return true;
                }
                return false;
            }
        });
        editYearBuiltET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showYearPicker(mActivity, editYearBuiltET);
                    return true;
                }
                return false;
            }
        });

        editDateET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showDatePicker(mActivity, editDateET);
                    return true;
                }
                return false;
            }
        });

        editClassET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    gettingVessal_Class();

                    return true;
                }
                return false;
            }
        });


        machinery_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    remarksTextActions.setVisibility(View.GONE);
                    ownerTextActions.setVisibility(View.GONE);
                    machineryTextActions.setVisibility(View.VISIBLE);
                    richTextActions.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
        remarks_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    remarksTextActions.setVisibility(View.VISIBLE);
                    ownerTextActions.setVisibility(View.GONE);
                    machineryTextActions.setVisibility(View.GONE);
                    richTextActions.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
        mRichEditText.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    remarksTextActions.setVisibility(View.GONE);
                    ownerTextActions.setVisibility(View.GONE);
                    machineryTextActions.setVisibility(View.GONE);
                    richTextActions.setVisibility(View.VISIBLE);
                    return true;
                }
                return false;
            }
        });
        owner_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    remarksTextActions.setVisibility(View.GONE);
                    ownerTextActions.setVisibility(View.VISIBLE);
                    machineryTextActions.setVisibility(View.GONE);
                    richTextActions.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Preventing multiple clicks, using threshold of 2 second
                if (SystemClock.elapsedRealtime() - mLastClickTab1 < 3000) {
                    return;
                }
                mLastClickTab1 = SystemClock.elapsedRealtime();

                strHyperLink = editHyperLinkET.getText().toString();
                strRenderHtmlContent = mRichEditText.getHtml();
                strMachineryContent = machinery_edit_text.getHtml();
                strOwnerDetails = owner_edit_text.getHtml();
                strRemarks = remarks_edit_text.getHtml();
                if (mGalleryArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_seelct_at_least));
                } else if (editDateET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_date));
                } else if (editVesselNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_name));
                } else if (editVesselTypeET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_select_vessel_type));
                } else if (mmsiET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_select_mmsi_number));
                } else if (editIMONoET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_imo_number));
                } else if (editDWTET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_dwt));
                } else if (editFlagET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_flag));
                } else if (editYearBuiltET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_select_year_built));
                } else if (editOffMarketET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_off_market));
                } else if (editShortDescriptionET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_short_description));
                } else if (editClassET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_classes));
                } else if (editPlaceBuiltET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_place_built));
                } else if (editBuilderET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_builder));
                } else if (editGearedET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_geared));
                } else if (editVesselLenghtOverallET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_over_all_lenght));
                } else if (editVesselBreathET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_breath));
                } else if (editDraughtET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_draught));
                } else if (editVesselDepthET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_depth));
                } else if (editVesselGrosssTonnageET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_gross_tonnage));
                } else if (editNetTonnageET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_net_tonnage));
                } else if (editGrainET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_grain));
                } else if (editBaleET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_bale));
                } else if (editVesselTEUET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_teu));
                } else if (editVesselPassangerET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_num_of_passangers));
                } else if (editLiquidET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_liquid));
                } else if (editGasET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_gas));
                } else if (editlightshipET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_lightship));
                } else if (editTcmET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_tcm));
                } else if (editVesselLocationET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_location));
                } else if (editPriceIdeaET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_price_idea));
                } else if (editCurrencyET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_currency));
                } else if (editStatusET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_status));
                } else if (machinery_edit_text.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_machinery_details));
                } else if (mRichEditText.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_description));
                } else if (owner_edit_text.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_owner_manager_details));
                } else if (remarks_edit_text.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_remarks));
                } else {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
//                        showLoadingAlertDialog(mActivity,"JAOHAR", "Uploading images Please wait...");
//                        Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            public void run() {
//                                alertDialog.dismiss();
//                            }
//                        }, 3000);

//                        final Handler handller = new Handler();
//                        handller.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {

//                                Execute Api
                        executeEditVesselsAPI();
//                            }
//                        }, 200);
                    }
                }
            }
        });
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 205);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
        }
    }

    private void gettingVessal_Class() {
        modelArrayList.clear();
        modellArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselClassModel> call1 = mApiInterface.getVesselClassRequest();
        call1.enqueue(new Callback<GetVesselClassModel>() {
                          @Override
                          public void onResponse(Call<GetVesselClassModel> call, retrofit2.Response<GetVesselClassModel> response) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselClassModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  modelArrayList = mModel.getData();
                                  for (int i = 0; i < modelArrayList.size(); i++) {
                                      modellArrayList.add(mModel.getData().get(i).getClassName());
                                  }
                                  showPopUpAdvanceSearch();
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselClassModel> call, Throwable t) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void gettingVessal_Type() {
        mArrayListClass.clear();
        mArrayListtClass.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
                          @Override
                          public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselTypeModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  mArrayListClass = mModel.getData();
                                  for (int i = 0; i < mArrayListClass.size(); i++) {
                                      mArrayListtClass.add(mModel.getData().get(i).getName());
                                  }
                                  showPopUpType();
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void showPopUpType() {
        final Dialog categoryDialog = new Dialog(mActivity);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.item_list_categories);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
        txtTitle.setText("Vessel Type");
        ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListtClass);
        lstListView.setAdapter(adapter);
        lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                editVesselTypeET.setText(mArrayListtClass.get(position));
                categoryDialog.dismiss();
            }
        });
        categoryDialog.show();
    }

    private void openInternalStorage() {
        Log.e(TAG, String.valueOf(documentArrayList.size()));
        if (documentArrayList.size() == 0 || documentArrayList.size() <= 10) {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            String[] mimetypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "text/plain", "application/pdf", "audio/*", "video/*", "application/rtf", "text/*"};
            chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            startActivityForResult(chooseFile, Constant.REQUEST_CODE_PICK_FILE);
        } else if (documentArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOnResumeFirst = true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2296) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                } else {
                    Toast.makeText(this, "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == 123) {
            strRenderHtmlContent = data.getStringExtra("result");
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);//images.get(i).path
                            showLoadingAlertDialog(mActivity, "JAOHAR", "Loading Images Please wait...");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    alertDialog.dismiss();
                                }
                            }, 3000);

                            final Handler handller = new Handler();
                            handller.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    for (int i = 0, l = images.size(); i < l; i++) {
                                        AddGalleryImagesModel model = new AddGalleryImagesModel();
                                        byte[] btye = getFileDataFromBitmap(mActivity, getThumbnailBitmap(images.get(i).path, 500));
                                        model.setStrImagePath(images.get(i).path);
                                        model.setByteArray(btye);
                                        model.setmBitmap(getThumbnailBitmap(images.get(i).path, 500));

                                        if (mGalleryArrayList.size() < 10)
                                            mGalleryArrayList.add(model);
                                    }

                                    /* Set GalleryImages in recycler View */
                                    setUpGalleryAdapter();

                                }
                            }, 200);

//                            for (int i = 0, l = images.size(); i < l; i++) {
//                                byte[] btye = getFileDataFromBitmap(mActivity, getThumbnailBitmap(images.get(i).path, 200));
//                                AddGalleryImagesModel model = new AddGalleryImagesModel();
//                                model.setStrImagePath(images.get(i).path);
//                                model.setByteArray(btye);
//                                model.setmBitmap(getThumbnailBitmap(images.get(i).path, 200));
//                                if (mGalleryArrayList.size() < 10)
//                                    mGalleryArrayList.add(model);
//                            }
//                            /*Set GalleryImages in recycler View*/
//                            setUpGalleryAdapter();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap thumb = BitmapFactory.decodeFile(mStoragePath, options);
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(mStoragePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        default:

                    }
                    Bitmap rotate = getThumbnailBitmap(mStoragePath, 500);
                    byte[] btye = getFileDataFromBitmap(mActivity, rotate);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(mStoragePath);
                    model.setByteArray(btye);
                    model.setmBitmap(rotate);
                    if (mGalleryArrayList.size() < 10)
                        mGalleryArrayList.add(model);
                    /*Set GalleryImages in recycler View*/
                    setUpGalleryAdapter();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (requestCode == Constant.REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    showLoadingAlertDialog(mActivity, "JAOHAR", "Loading File Please wait...");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            alertDialog.dismiss();
                        }
                    }, 3000);

                    final Handler handller = new Handler();
                    handller.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Uri uri = data.getData();
                                String uriString = uri.toString();
                                File myFile = new File(uriString);
                                strDocumentPath = myFile.getAbsolutePath();
                                strDocumentName = null;

                                if (uriString.startsWith("content://")) {
                                    Cursor cursor = null;
                                    try {
                                        cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
                                        if (cursor != null && cursor.moveToFirst()) {
                                            strDocumentName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        }
                                    } finally {
                                        cursor.close();
                                    }
                                } else if (uriString.startsWith("file://")) {
                                    strDocumentName = myFile.getName();
                                }
                                InputStream iStream = null;
                                if (isVirtualFile(uri)) {
                                    try {
                                        iStream = getInputStreamForVirtualFile(uri, "*/*");
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        iStream = getContentResolver().openInputStream(uri);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                                try {
                                    strByteArray = getBytes(iStream);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


//                    InputStream iStream = getContentResolver().openInputStream(uri);
//                    strByteArray = getBytes(iStream);
                                DocumentModel mDocumentModel = new DocumentModel();
                                mDocumentModel.setId("11");
                                mDocumentModel.setDocumentName(strDocumentName);
                                mDocumentModel.setByteArray(strByteArray);
                                mDocumentModel.setmUri(uri);
                                mDocumentModel.setDocumentBase64(Utilities.convertByteArrayToBase64(strByteArray));
                                mDocumentModel.setDocumentPath(strDocumentPath);
                                documentArrayList.add(mDocumentModel);
                                setDocumentAdapter(documentArrayList);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 200);
                }
            }

//            if (requestCode == Constant.REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
////                try {
//                if (data != null) {
//                    ArrayList<NormalFile> files = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
////                        try {
////                    ArrayList<MediaFile> files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
//                            showLoadingAlertDialog(mActivity,"JAOHAR", "Loading Files Please wait...");
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//                                    alertDialog.dismiss();
//                                }
//                            }, 3000);
//
//                            final Handler handller = new Handler();
//                            handller.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                    InputStream iStream = null;
//                    for (int i = 0;i<files.size(); i++) {
//                        DocumentModel mDocumentModel = new DocumentModel();
//                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                        try {
//                            iStream = new FileInputStream(new File(files.get(i).getPath()));
//                            byte[] buf = new byte[1024];
//                            int n;
//                            while (-1 != (n = iStream.read(buf)))
//                                baos.write(buf, 0, n);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        strByteArray = baos.toByteArray();
//
//                        mDocumentModel.setDocumentPath(files.get(i).getPath());
//                        mDocumentModel.setByteArray(strByteArray);
//                        String extension = files.get(i).getPath().substring(files.get(i).getPath().lastIndexOf("."));
//                        mDocumentModel.setDocumunetExtension(extension);
////                        mDocumentModel.setDocumunetExtension(files.get(i).getMimeType());
////                                        mDocumentModel.setId("11");
//                        mDocumentModel.setDocumentName(files.get(i).getName());
//
//                        if (documentArrayList.size() <= 10)
//                            documentArrayList.add(mDocumentModel);
//                    }
//                    /* Set document in recycler View */
////                    setAdapter(documentArrayList);
//                    setDocumentAdapter(documentArrayList);
//
//                                }
//                            }, 200);
//
//
//
//                }
//                else {
//                    Log.e(TAG, "*****No Picture Selected****");
//                    return;
//                }
////            catch (Exception e) {
////                    e.printStackTrace();
//            }

            if (requestCode == 205 && resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap thumb = null;
                    Uri uri = data.getData();
                    strDocumentPath = uri.getPath();
                    strDocumentPath = strDocumentPath.replace(" ", "_");
                    strDocumentName = uri.getLastPathSegment();
                    Bitmap bitmap;
                    String strDocumentPath, strDocumentName;

                    strDocumentPath = uri.getPath();
                    strDocumentPath = strDocumentPath.replace(" ", "_");
                    strDocumentName = uri.getLastPathSegment();

                    File finalFile = new File(getRealPathFromURI(uri));

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;

                    bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
                    // For Convert And rotate image
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                    }

                    thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    byte[] btye = getFileDataFromBitmap(mActivity, thumb);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(strDocumentPath);
                    model.setByteArray(btye);
                    model.setmBitmap(thumb);
                    if (mGalleryArrayList.size() < 10)
                        mGalleryArrayList.add(model);
                    /* Set GalleryImages in recycler View */
                    setUpGalleryAdapter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isVirtualFile(Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (!DocumentsContract.isDocumentUri(mActivity, uri)) {
                return false;
            }

            Cursor cursor = getContentResolver().query(
                    uri,
                    new String[]{DocumentsContract.Document.COLUMN_FLAGS},
                    null, null, null);
            int flags = 0;
            if (cursor.moveToFirst()) {
                flags = cursor.getInt(0);
            }
            cursor.close();
            return (flags & DocumentsContract.Document.FLAG_VIRTUAL_DOCUMENT) != 0;
        } else {
            return false;
        }
    }

    private InputStream getInputStreamForVirtualFile(Uri uri, String mimeTypeFilter)
            throws IOException {

        ContentResolver resolver = getContentResolver();

        String[] openableMimeTypes = resolver.getStreamTypes(uri, mimeTypeFilter);

        if (openableMimeTypes == null ||
                openableMimeTypes.length < 1) {
            throw new FileNotFoundException();
        }

        return resolver
                .openTypedAssetFileDescriptor(uri, openableMimeTypes[0], null)
                .createInputStream();
    }

//    private void openInternalStorage() {
//        Intent intent = new Intent();
////        intent.setType("application/pdf");
//        intent.setType("*/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(intent, 505);
//    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 512;
        byte[] buffer = new byte[bufferSize];
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

//    private void openInternalStorage() {
//        Log.e(TAG, String.valueOf(documentArrayList.size()));
//        if (documentArrayList.size() == 0) {
//            Intent intent4 = new Intent(this, NormalFilePickActivity.class);
//            intent4.putExtra(Constant.MAX_NUMBER, 10);
//            intent4.putExtra(Constants.INTENT_EXTRA_LIMIT,10);
//            intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"pdf","txt","mp3","xlsx","rtf","xls","doc","docx","ppt","pptx","mp4"});
//            startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);
//        } else if (documentArrayList.size() == 10) {
//            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
//        } else if (documentArrayList.size() != 0) {
//            Intent intent4 = new Intent(this, NormalFilePickActivity.class);
//            int GallarySize = 10 - documentArrayList.size();
//            intent4.putExtra(Constant.MAX_NUMBER, GallarySize);
//            intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"pdf","txt","mp3","xlsx","rtf","xls","doc","docx","ppt","pptx","mp4"});
//            startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);
//        }}

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    /**
     * returns the thumbnail image bitmap from the given url
     *
     * @param path
     * @param thumbnailSize
     * @return
     */
    private Bitmap getThumbnailBitmap(final String path, final int thumbnailSize) {
        Bitmap bitmap;
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(path, opts);
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    public String getPath(Uri uri) {
        String path = null;
        String[] projection = {MediaStore.Files.FileColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) {
            path = uri.getPath();
        } else {
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }
        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }

    private void setUpGalleryAdapter() {
        if (mGalleryArrayList.size() >= 1) {
            galleryRecyclerView.setVisibility(View.VISIBLE);
        } else {
            galleryRecyclerView.setVisibility(View.GONE);
        }
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        galleryRecyclerView.setLayoutManager(horizontalLayoutManager);
        mAdapter = new AddImageListAdapter(mActivity, mGalleryArrayList, mDeleteImagesInterface);
        galleryRecyclerView.setAdapter(mAdapter);
    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);
        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, 300, 150, false);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void openCameraGalleryDialog() {
        cameraGalleryDialog = new Dialog(mActivity);
        cameraGalleryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraGalleryDialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = cameraGalleryDialog.findViewById(R.id.txt_camra);
        TextView text_gallery = cameraGalleryDialog.findViewById(R.id.txt_gallery);
        TextView txt_files = cameraGalleryDialog.findViewById(R.id.txt_files);
        TextView txt_cancel = cameraGalleryDialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                openGallery();
            }
        });
        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                if (mGalleryArrayList.size() <= 10 - 1) {
                    openFiles();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
                }

            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
            }
        });
        cameraGalleryDialog.show();
        Log.e("error is occured", cameraGalleryDialog.toString());
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Log.e(TAG, String.valueOf(mGalleryArrayList.size()));
        if (mGalleryArrayList.size() == 0) {
            Intent intent = new Intent(EditVesselActivity.this, AlbumSelectActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 10);
            startActivityForResult(intent, GALLERY_REQUEST);
        } else if (mGalleryArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit));
        } else if (mGalleryArrayList.size() != 0) {
            Intent intent = new Intent(EditVesselActivity.this, AlbumSelectActivity.class);
            int GallarySize = 10 - mGalleryArrayList.size();
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, GallarySize);
            startActivityForResult(intent, GALLERY_REQUEST);
        }
    }

    public void setUpCameraGalleryDialog() {
        if (checkPermissionGAllery()) {
            openCameraGalleryDialog();
        } else {
            requestPermissionGallery();

        }
    }

    private void executeEditVesselsAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;
        MultipartBody.Part mMultipartBody6 = null;
        MultipartBody.Part mMultipartBody7 = null;
        MultipartBody.Part mMultipartBody8 = null;
        MultipartBody.Part mMultipartBody9 = null;
        MultipartBody.Part mMultipartBody10 = null;
        Log.e(TAG, "executeEditVesselsAPI: " + mGalleryArrayList);

        if (mGalleryArrayList.size() > 0) {
            if (mGalleryArrayList.get(0).getStrImagePath().contains("http")) {
                pic1 = mGalleryArrayList.get(0).getStrImagePath();
//                bitmap1 = getBitmapFromURL(mGalleryArrayList.get(0).getStrImagePath());
//                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap1));
//                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericString() + ".jpeg", requestFile1);
            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(0).getmBitmap()));
                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericString() + ".jpeg", requestFile1);
                Log.e("img", "img" + mMultipartBody1);
            }
        }

        if (mGalleryArrayList.size() > 1) {
            if (mGalleryArrayList.get(1).getStrImagePath().contains("http")) {
                pic2 = mGalleryArrayList.get(1).getStrImagePath();
//                bitmap2 = getBitmapFromURL(mGalleryArrayList.get(1).getStrImagePath());
//                RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap2));
//                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericString() + ".jpeg", requestFile2);

            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(1).getmBitmap()));
                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericString() + ".jpeg", requestFile2);
                Log.e("img", "img" + mMultipartBody2);
            }
        }

        if (mGalleryArrayList.size() > 2) {
            if (mGalleryArrayList.get(2).getStrImagePath().contains("http")) {
                pic3 = mGalleryArrayList.get(2).getStrImagePath();
//                bitmap3 = getBitmapFromURL(mGalleryArrayList.get(2).getStrImagePath());
//                RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap3));
//                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericString() + ".jpeg", requestFile3);

            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(2).getmBitmap()));
                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericString() + ".jpeg", requestFile3);
                Log.e("img", "img" + mMultipartBody3);
            }
        }

        if (mGalleryArrayList.size() > 3) {
            if (mGalleryArrayList.get(3).getStrImagePath().contains("http")) {
                pic4 = mGalleryArrayList.get(3).getStrImagePath();
//                bitmap4 = getBitmapFromURL(mGalleryArrayList.get(3).getStrImagePath());
//                RequestBody requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap4));
//                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericString() + ".jpeg", requestFile4);
            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(3).getmBitmap()));
                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericString() + ".jpeg", requestFile4);
                Log.e("img", "img" + mMultipartBody4);
            }
        }

        if (mGalleryArrayList.size() > 4) {
            if (mGalleryArrayList.get(4).getStrImagePath().contains("http")) {
                pic5 = mGalleryArrayList.get(4).getStrImagePath();
//                bitmap5 = getBitmapFromURL(mGalleryArrayList.get(4).getStrImagePath());
//                RequestBody requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap5));
//                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericString() + ".jpeg", requestFile5);

            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(4).getmBitmap()));
                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericString() + ".jpeg", requestFile5);
                Log.e("img", "img" + mMultipartBody5);
            }
        }
        if (mGalleryArrayList.size() > 5) {
            if (mGalleryArrayList.get(5).getStrImagePath().contains("http")) {
                pic6 = mGalleryArrayList.get(5).getStrImagePath();
//                bitmap6 = getBitmapFromURL(mGalleryArrayList.get(5).getStrImagePath());
//                RequestBody requestFile6 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap6));
//                mMultipartBody6 = MultipartBody.Part.createFormData("photo6", getAlphaNumericString() + ".jpeg", requestFile6);
            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile6 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(5).getmBitmap()));
                mMultipartBody6 = MultipartBody.Part.createFormData("photo6", getAlphaNumericString() + ".jpeg", requestFile6);
                Log.e("img", "img" + mMultipartBody6);
            }
        }

        if (mGalleryArrayList.size() > 6) {
            if (mGalleryArrayList.get(6).getStrImagePath().contains("http")) {
                pic7 = mGalleryArrayList.get(6).getStrImagePath();
//                bitmap7 = getBitmapFromURL(mGalleryArrayList.get(6).getStrImagePath());
//                RequestBody requestFile7 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap7));
//                mMultipartBody7 = MultipartBody.Part.createFormData("photo7", getAlphaNumericString() + ".jpeg", requestFile7);

            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile7 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(6).getmBitmap()));
                mMultipartBody7 = MultipartBody.Part.createFormData("photo7", getAlphaNumericString() + ".jpeg", requestFile7);
                Log.e("img", "img" + mMultipartBody7);
            }
        }

        if (mGalleryArrayList.size() > 7) {
            if (mGalleryArrayList.get(7).getStrImagePath().contains("http")) {
                pic8 = mGalleryArrayList.get(7).getStrImagePath();
//                bitmap8 = getBitmapFromURL(mGalleryArrayList.get(7).getStrImagePath());
//                RequestBody requestFile8 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap8));
//                mMultipartBody8 = MultipartBody.Part.createFormData("photo8", getAlphaNumericString() + ".jpeg", requestFile8);
            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile8 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(7).getmBitmap()));
                mMultipartBody8 = MultipartBody.Part.createFormData("photo8", getAlphaNumericString() + ".jpeg", requestFile8);
                Log.e("img", "img" + mMultipartBody8);
            }
        }

        if (mGalleryArrayList.size() > 8) {
            if (mGalleryArrayList.get(8).getStrImagePath().contains("http")) {
                pic9 = mGalleryArrayList.get(8).getStrImagePath();
//                bitmap9 = getBitmapFromURL(mGalleryArrayList.get(8).getStrImagePath());
//                RequestBody requestFile9 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap9));
//                mMultipartBody9 = MultipartBody.Part.createFormData("photo9", getAlphaNumericString() + ".jpeg", requestFile9);
            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile9 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(8).getmBitmap()));
                mMultipartBody9 = MultipartBody.Part.createFormData("photo9", getAlphaNumericString() + ".jpeg", requestFile9);
                Log.e("img", "img" + mMultipartBody9);
            }
        }

        if (mGalleryArrayList.size() > 9) {
            if (mGalleryArrayList.get(9).getStrImagePath().contains("http")) {
                pic10 = mGalleryArrayList.get(9).getStrImagePath();
//                bitmap10 = getBitmapFromURL(mGalleryArrayList.get(9).getStrImagePath());
//                RequestBody requestFile10 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(bitmap10));
//                mMultipartBody10 = MultipartBody.Part.createFormData("photo10", getAlphaNumericString() + ".jpeg", requestFile10);
            } else {
                JaoharConstants.IS_camera_Click = true;
                RequestBody requestFile10 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(9).getmBitmap()));
                mMultipartBody10 = MultipartBody.Part.createFormData("photo10", getAlphaNumericString() + ".jpeg", requestFile10);
                Log.e("img", "img" + mMultipartBody10);
            }
        }

        RequestBody vessel_id = RequestBody.create(MediaType.parse("multipart/form-data"), mVesselesModel.getRecordId());
        RequestBody vessel_name = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselNameET.getText().toString());
        RequestBody vessel_IMO_no = RequestBody.create(MediaType.parse("multipart/form-data"), editIMONoET.getText().toString());
        RequestBody vessel_type = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselTypeET.getText().toString());
        RequestBody vessel_built_year = RequestBody.create(MediaType.parse("multipart/form-data"), editYearBuiltET.getText().toString());
        RequestBody vessel_place_built = RequestBody.create(MediaType.parse("multipart/form-data"), editPlaceBuiltET.getText().toString());
        RequestBody vessel_class = RequestBody.create(MediaType.parse("multipart/form-data"), editClassET.getText().toString());
        RequestBody vessel_flag = RequestBody.create(MediaType.parse("multipart/form-data"), editFlagET.getText().toString());
        RequestBody vessel_price_idea = RequestBody.create(MediaType.parse("multipart/form-data"), editPriceIdeaET.getText().toString());
        RequestBody vessel_short_description = RequestBody.create(MediaType.parse("multipart/form-data"), editShortDescriptionET.getText().toString());
        RequestBody vessel_DWT = RequestBody.create(MediaType.parse("multipart/form-data"), editDWTET.getText().toString());
        RequestBody vessel_loa = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselLenghtOverallET.getText().toString());
        RequestBody vessel_breadth = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselBreathET.getText().toString());
        RequestBody vessel_depth = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselDepthET.getText().toString());
        RequestBody vessel_location = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselLocationET.getText().toString());
        RequestBody vessel_currency = RequestBody.create(MediaType.parse("multipart/form-data"), editCurrencyET.getText().toString());
        RequestBody vessel_long_description = RequestBody.create(MediaType.parse("multipart/form-data"), strRenderHtmlContent);
        RequestBody vessel_status = RequestBody.create(MediaType.parse("multipart/form-data"), editStatusET.getText().toString());
        RequestBody bale = RequestBody.create(MediaType.parse("multipart/form-data"), editBaleET.getText().toString());
        RequestBody grain = RequestBody.create(MediaType.parse("multipart/form-data"), editGrainET.getText().toString());
        RequestBody date = RequestBody.create(MediaType.parse("multipart/form-data"), editDateET.getText().toString());
        RequestBody teu = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselTEUET.getText().toString());
        RequestBody no_passengers = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselPassangerET.getText().toString());
        RequestBody liquid = RequestBody.create(MediaType.parse("multipart/form-data"), editLiquidET.getText().toString());
        RequestBody builder = RequestBody.create(MediaType.parse("multipart/form-data"), editBuilderET.getText().toString());
        RequestBody gross_tonnage = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselGrosssTonnageET.getText().toString());
        RequestBody net_tonnage = RequestBody.create(MediaType.parse("multipart/form-data"), editNetTonnageET.getText().toString());
        RequestBody gas = RequestBody.create(MediaType.parse("multipart/form-data"), editGasET.getText().toString());
        RequestBody draught = RequestBody.create(MediaType.parse("multipart/form-data"), editDraughtET.getText().toString());
        if (strHyperLink.equals("")) {
            url = RequestBody.create(MediaType.parse("multipart/form-data"), strHyperLink);
        } else {
            if (strHyperLink.contains("http://")) {
                url = RequestBody.create(MediaType.parse("multipart/form-data"), strHyperLink);
            } else {
                url = RequestBody.create(MediaType.parse("multipart/form-data"), "http://" + strHyperLink);
            }
        }

        RequestBody owner_detail = RequestBody.create(MediaType.parse("multipart/form-data"), strOwnerDetails);
        RequestBody machinery_detail = RequestBody.create(MediaType.parse("multipart/form-data"), strMachineryContent);
        RequestBody remarks = RequestBody.create(MediaType.parse("multipart/form-data"), strRemarks);
        RequestBody ldt = RequestBody.create(MediaType.parse("multipart/form-data"), editlightshipET.getText().toString());
        RequestBody tcm = RequestBody.create(MediaType.parse("multipart/form-data"), editTcmET.getText().toString());
        RequestBody geared = RequestBody.create(MediaType.parse("multipart/form-data"), editGearedET.getText().toString());
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        RequestBody mmsi_no = RequestBody.create(MediaType.parse("multipart/form-data"), mmsiET.getText().toString());
        RequestBody off_market = RequestBody.create(MediaType.parse("multipart/form-data"), editOffMarketET.getText().toString());
        if (pic1 != null) {
            picc1 = RequestBody.create(MediaType.parse("multipart/form-data"), pic1);
        }
        if (pic2 != null) {
            picc2 = RequestBody.create(MediaType.parse("multipart/form-data"), pic2);
        }
        if (pic3 != null) {
            picc3 = RequestBody.create(MediaType.parse("multipart/form-data"), pic3);
        }
        if (pic4 != null) {
            picc4 = RequestBody.create(MediaType.parse("multipart/form-data"), pic4);
        }
        if (pic5 != null) {
            picc5 = RequestBody.create(MediaType.parse("multipart/form-data"), pic5);
        }
        if (pic6 != null) {
            picc6 = RequestBody.create(MediaType.parse("multipart/form-data"), pic6);
        }
        if (pic7 != null) {
            picc7 = RequestBody.create(MediaType.parse("multipart/form-data"), pic7);
        }
        if (pic8 != null) {
            picc8 = RequestBody.create(MediaType.parse("multipart/form-data"), pic8);
        }
        if (pic9 != null) {
            picc9 = RequestBody.create(MediaType.parse("multipart/form-data"), pic9);
        }
        if (pic10 != null) {
            picc10 = RequestBody.create(MediaType.parse("multipart/form-data"), pic10);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.editVesselsRequest(vessel_id, vessel_name, vessel_IMO_no, vessel_type, vessel_built_year, vessel_place_built, vessel_class, vessel_flag,
                vessel_price_idea, vessel_short_description, vessel_DWT, vessel_loa, vessel_breadth, vessel_depth,
                vessel_location, vessel_currency, vessel_long_description, vessel_status, bale, grain, date,
                teu, no_passengers, liquid, builder, gross_tonnage, net_tonnage, gas, draught, url, owner_detail, machinery_detail,
                remarks, ldt, tcm, geared, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5, mMultipartBody6, mMultipartBody7, mMultipartBody8, mMultipartBody9, mMultipartBody10,
                user_id, mmsi_no, off_market, picc1, picc2, picc3, picc4, picc5, picc6, picc7, picc8, picc9, picc10).enqueue(new Callback<AddVesselmodel>() {
            @Override
            public void onResponse(Call<AddVesselmodel> call, retrofit2.Response<AddVesselmodel> response) {
//                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                AddVesselmodel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    strRecordID = mModel.getId();
                    if (mModel.getVessel_details() != null) {
                        JaoharSingleton.getInstance().setmAllVessel(mModel.getVessel_details());
                    }
                    IS_First_Click = false;
//                    if (documentArrayList.size() != 0) {
                    executeUploadDocument();
//                    } else {
//                        alertConfirmDialog();
//                    }
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddVesselmodel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        Log.e(TAG, "**********Image Name******" + sb);
        return sb.toString();
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }

    private JSONArray getVesselImagesArray(ArrayList<AddGalleryImagesModel> mGalleryArrayList) {
        Log.e(TAG, "********SIZE*****" + mGalleryArrayList.size());
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < mGalleryArrayList.size(); i++) {
                if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                    mJsonArray.put(mGalleryArrayList.get(i).getStrImagePath());
                } else {
                    JaoharConstants.IS_camera_Click = true;
                    mJsonArray.put(ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    public void alertConfirmDialog() {
        final Dialog alert = new Dialog(mActivity);
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.setContentView(R.layout.dialog_add_vessel_confirmation);
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtOK = alert.findViewById(R.id.txtOK);
        TextView txtMessage = alert.findViewById(R.id.txtMessage);
        if (mVesselesModel != null && strTitleToolbar.equals("Edit Vessels")) {
            txtMessage.setText("Updated Successfully");
        } else {
            txtMessage.setText("Added Successfully");
        }

        txtOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editVesselNameET.setText("");
                editIMONoET.setText("");
                editVesselTypeET.setText("");
                editVesselTypeET.setKeyListener(null);
                editYearBuiltET.setText("");
                editYearBuiltET.setKeyListener(null);
                editPlaceBuiltET.setText("");
                editClassET.setText("");
                editFlagET.setText("");
                editFlagET.requestFocus();
                editPriceIdeaET.setText("");
                editShortDescriptionET.setText("");
                editDWTET.setText("");
//                editLoaET.setText("");
                editStatusET.setText("");
                editStatusET.setKeyListener(null);
                editGearedET.setText("");
                editGearedET.setKeyListener(null);
                editVesselBreathET.setText("");
                editVesselDepthET.setText("");
                editVesselLocationET.setText("");
                editCurrencyET.setText("");
                editCurrencyET.setKeyListener(null);
                editBaleET.setText("");
                editGrainET.setText("");
                editDateET.setText("");
                editBuilderET.setText("");
                editClassET.setText("");
                editDraughtET.setText("");
                editGasET.setText("");
                editHyperLinkET.setText("");
                editLiquidET.setText("");
                editNetTonnageET.setText("");
                editVesselGrosssTonnageET.setText("");
                editVesselLenghtOverallET.setText("");
                editVesselTEUET.setText("");
                editVesselPassangerET.setText("");
                editDateET.setKeyListener(null);
                editOffMarketET.setText("");
                editOffMarketET.setKeyListener(null);
                mmsiET.setText("");
                finish();
            }
        });
        alert.show();
    }
//
//    public Bitmap getBitmapFromURL(String src) {
//        Bitmap image = null;
//    try {
//        URL url = new URL(src);
//        image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//    } catch(IOException e) {
//        System.out.println(e);
//    }
//        return image;
//    }

    private void executeUploadDocument() {
//        URL myURL = null;
        for (int i = 0; i < documentArrayList.size(); i++) {
            if (i == 0) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc1 = documentArrayList.get(i).getDocumentPath();
                    docEx1 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc1 = getByteImageFromURL(myURL);
                    doc1name = "Document1";

                } else {
                    doc1 = documentArrayList.get(i).getByteArray();
                    docEx1 = documentArrayList.get(i).getDocumunetExtension();
                    doc1name = "Document1";
                }

            } else if (i == 1) {
                if (documentArrayList.get(i).getDocumentPath() != null) {
                    if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                        docc2 = documentArrayList.get(i).getDocumentPath();
                        docEx2 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc2 = getByteImageFromURL(myURL);
                        doc2name = "Document2";
                    }
                } else {
                    doc2 = documentArrayList.get(i).getByteArray();
                    docEx2 = documentArrayList.get(i).getDocumunetExtension();
                    doc2name = "Document2";
                }
            } else if (i == 2) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc3 = documentArrayList.get(i).getDocumentPath();
                    docEx3 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc3 = getByteImageFromURL(myURL);
                    doc3name = "Document3";
                } else {
                    doc3 = documentArrayList.get(i).getByteArray();
                    docEx3 = documentArrayList.get(i).getDocumunetExtension();
                    doc3name = "Document3";
                }
            } else if (i == 3) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc4 = documentArrayList.get(i).getDocumentPath();
                    docEx4 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc4 = getByteImageFromURL(myURL);
                    doc4name = "Document4";
                } else {
                    doc4 = documentArrayList.get(i).getByteArray();
                    docEx4 = documentArrayList.get(i).getDocumunetExtension();
                    doc4name = "Document4";
                }
            } else if (i == 4) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc5 = documentArrayList.get(i).getDocumentPath();
                    docEx5 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc5 = getByteImageFromURL(myURL);
                    doc5name = "Document5";
                } else {
                    doc5 = documentArrayList.get(i).getByteArray();
                    docEx5 = documentArrayList.get(i).getDocumunetExtension();
                    doc5name = "Document5";
                }
            } else if (i == 5) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc6 = documentArrayList.get(i).getDocumentPath();
                    docEx6 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc6 = getByteImageFromURL(myURL);
                    doc6name = "Document6";
                } else {
                    doc6 = documentArrayList.get(i).getByteArray();
                    docEx6 = documentArrayList.get(i).getDocumunetExtension();
                    doc6name = "Document6";
                }
            } else if (i == 6) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc7 = documentArrayList.get(i).getDocumentPath();
                    docEx7 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc7 = getByteImageFromURL(myURL);
                    doc7name = "Document7";
                } else {
                    doc7 = documentArrayList.get(i).getByteArray();
                    doc7name = "Document7";
                    docEx7 = documentArrayList.get(i).getDocumunetExtension();
                }
            } else if (i == 7) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc8 = documentArrayList.get(i).getDocumentPath();
                    docEx8 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc8 = getByteImageFromURL(myURL);
                    doc8name = "Document8";
                } else {
                    doc8 = documentArrayList.get(i).getByteArray();
                    docEx8 = documentArrayList.get(i).getDocumunetExtension();
                    doc8name = "Document8";
                }
            } else if (i == 8) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc9 = documentArrayList.get(i).getDocumentPath();
                    docEx9 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc9 = getByteImageFromURL(myURL);
                    doc9name = "Document9";
                } else {
                    doc9 = documentArrayList.get(i).getByteArray();
                    docEx9 = documentArrayList.get(i).getDocumunetExtension();
                    doc9name = "Document9";
                }
            } else if (i == 9) {
                if (documentArrayList.get(i).getDocumentPath().contains("http")) {
                    docc10 = documentArrayList.get(i).getDocumentPath();
                    docEx10 = documentArrayList.get(i).getDocumunetExtension();
//                    try {
//                        myURL = new URL(documentArrayList.get(i).getDocumentPath());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    assert myURL != null;
//                    doc10 = getByteImageFromURL(myURL);
                    doc10name = "Document10";
                } else {
                    doc10 = documentArrayList.get(i).getByteArray();
                    docEx10 = documentArrayList.get(i).getDocumunetExtension();
                    doc10name = "Document10";
                }
            }
        }

//        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBodyDoc1 = null;
        MultipartBody.Part mMultipartBodyDoc2 = null;
        MultipartBody.Part mMultipartBodyDoc3 = null;
        MultipartBody.Part mMultipartBodyDoc4 = null;
        MultipartBody.Part mMultipartBodyDoc5 = null;
        MultipartBody.Part mMultipartBodyDoc6 = null;
        MultipartBody.Part mMultipartBodyDoc7 = null;
        MultipartBody.Part mMultipartBodyDoc8 = null;
        MultipartBody.Part mMultipartBodyDoc9 = null;
        MultipartBody.Part mMultipartBodyDoc10 = null;

        if (doc1 != null) {
            RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), doc1);
            mMultipartBodyDoc1 = MultipartBody.Part.createFormData("Document1", doc1name + docEx1, requestFileDoc1);
            Log.e("img", "img" + mMultipartBodyDoc1);
        }
        if (doc2 != null) {
            RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), doc2);
            mMultipartBodyDoc2 = MultipartBody.Part.createFormData("Document2", doc2name + docEx2, requestFileDoc2);
            Log.e("img", "img" + mMultipartBodyDoc2);
        }
        if (doc3 != null) {
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), doc3);
            mMultipartBodyDoc3 = MultipartBody.Part.createFormData("Document3", doc3name + docEx3, requestFileDoc3);
            Log.e("img", "img" + mMultipartBodyDoc3);
        }
        if (doc4 != null) {
            RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), doc4);
            mMultipartBodyDoc4 = MultipartBody.Part.createFormData("Document4", doc4name + docEx4, requestFileDoc4);
            Log.e("img", "img" + mMultipartBodyDoc4);
        }
        if (doc5 != null) {
            RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), doc5);
            mMultipartBodyDoc5 = MultipartBody.Part.createFormData("Document5", doc5name + docEx5, requestFileDoc5);
            Log.e("img", "img" + mMultipartBodyDoc5);
        }
        if (doc6 != null) {
            RequestBody requestFileDoc6 = RequestBody.create(MediaType.parse("multipart/form-data"), doc6);
            mMultipartBodyDoc6 = MultipartBody.Part.createFormData("Document6", doc6name + docEx6, requestFileDoc6);
            Log.e("img", "img" + mMultipartBodyDoc6);
        }
        if (doc7 != null) {
            RequestBody requestFileDoc7 = RequestBody.create(MediaType.parse("multipart/form-data"), doc7);
            mMultipartBodyDoc7 = MultipartBody.Part.createFormData("Document7", doc7name + docEx7, requestFileDoc7);
            Log.e("img", "img" + mMultipartBodyDoc7);
        }
        if (doc8 != null) {
            RequestBody requestFileDoc8 = RequestBody.create(MediaType.parse("multipart/form-data"), doc8);
            mMultipartBodyDoc8 = MultipartBody.Part.createFormData("Document8", doc8name + docEx8, requestFileDoc8);
            Log.e("img", "img" + mMultipartBodyDoc8);
        }
        if (doc9 != null) {
            RequestBody requestFileDoc9 = RequestBody.create(MediaType.parse("multipart/form-data"), doc9);
            mMultipartBodyDoc9 = MultipartBody.Part.createFormData("Document9", doc9name + docEx9, requestFileDoc9);
            Log.e("img", "img" + mMultipartBodyDoc9);
        }
        if (doc10 != null) {
            RequestBody requestFileDoc10 = RequestBody.create(MediaType.parse("multipart/form-data"), doc10);
            mMultipartBodyDoc10 = MultipartBody.Part.createFormData("Document10", doc10name + docEx10, requestFileDoc10);
            Log.e("img", "img" + mMultipartBodyDoc10);
        }
        if (docc1 != null) {
            docu1 = RequestBody.create(MediaType.parse("multipart/form-data"), docc1);
        }
        if (docc2 != null) {
            docu2 = RequestBody.create(MediaType.parse("multipart/form-data"), docc2);
        }
        if (docc3 != null) {
            docu3 = RequestBody.create(MediaType.parse("multipart/form-data"), docc3);
        }
        if (docc4 != null) {
            docu4 = RequestBody.create(MediaType.parse("multipart/form-data"), docc4);
        }
        if (docc5 != null) {
            docu5 = RequestBody.create(MediaType.parse("multipart/form-data"), docc5);
        }
        if (docc6 != null) {
            docu6 = RequestBody.create(MediaType.parse("multipart/form-data"), docc6);
        }
        if (docc7 != null) {
            docu7 = RequestBody.create(MediaType.parse("multipart/form-data"), docc7);
        }
        if (docc8 != null) {
            docu8 = RequestBody.create(MediaType.parse("multipart/form-data"), docc8);
        }
        if (docc9 != null) {
            docu9 = RequestBody.create(MediaType.parse("multipart/form-data"), docc9);
        }
        if (docc10 != null) {
            docu10 = RequestBody.create(MediaType.parse("multipart/form-data"), docc10);
        }

        RequestBody record_id = RequestBody.create(MediaType.parse("multipart/form-data"), strRecordID);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.editUploadDocumentRequest(record_id, mMultipartBodyDoc1, mMultipartBodyDoc2, mMultipartBodyDoc3, mMultipartBodyDoc4, mMultipartBodyDoc5, mMultipartBodyDoc6, mMultipartBodyDoc7, mMultipartBodyDoc8, mMultipartBodyDoc9, mMultipartBodyDoc10, docu1, docu2, docu3, docu4, docu5, docu6, docu7, docu8, docu9, docu10).enqueue(new Callback<StatusMsgModel>() {

            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    alertConfirmDialog();
                    Log.e(TAG, "documentArrayList: " + documentArrayList);
//                    if (documentArrayList != null) {
//                        if (documentArrayList.size() > 0) {
                    JaoharSingleton.getInstance().setmAllDocument(documentArrayList);
//                        }
//                    }
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void showPopUpAdvanceSearch() {
        final Dialog categoryDialog = new Dialog(mActivity);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.item_list_categories);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
        txtTitle.setText("Vessel Class");
        ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_list_item_1, android.R.id.text1, modellArrayList);

        // Assign adapter to ListView
        lstListView.setAdapter(adapter);

        lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                editClassET.setText(modellArrayList.get(position));
                categoryDialog.dismiss();

            }
        });

        categoryDialog.show();
    }

    private boolean checkPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            int result = ContextCompat.checkSelfPermission(mActivity, readStorageStr);
            int result1 = ContextCompat.checkSelfPermission(mActivity, writeStorageStr);
            return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        }
    }

    private void requestPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", getApplicationContext().getPackageName())));
                startActivityForResult(intent, 2296);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, 2296);
            }
        } else {
            //below android 11
            ActivityCompat.requestPermissions(mActivity, new String[]{writeStorageStr}, PERMISSION_REQUEST_CODE);
        }
    }

    private boolean checkPermissionGAllery() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }

    void requestPermissionGallery() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            if (cameraGalleryDialog != null && cameraGalleryDialog.isShowing()) {
                                cameraGalleryDialog.dismiss();
                            }
                            openCameraGalleryDialog();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermissionGallery();
                        }
                    }
                }
                break;
        }
    }






}

