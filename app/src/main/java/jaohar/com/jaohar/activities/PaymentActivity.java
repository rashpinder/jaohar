package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class PaymentActivity extends BaseActivity {

    Activity mActivity = PaymentActivity.this;
    String TAG = PaymentActivity.this.getClass().getSimpleName();

    //Toolbar
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText editVatET, editPaidET;
    TextView txtSubTotalTV, txtTotalTV, txtBalaceDueTV, txtVatPriceTV;
    Button btnSubmitB;

    static double mSubTotal = 0;
    long intItems = 0;
    long intUnitPrice = 0;
    double mAmount = 0;
    double mPaid = 0;
    double mBalanceDue = 0;
    static double mTotal1 = 0;
    static double mTotalASDisc1 = 0;
    PaymentModel model = new PaymentModel();
    double mTotal = 0, mVatCost = 0;
    ArrayList<AddTotalInvoiceDiscountModel> itemsArrayList = new ArrayList<AddTotalInvoiceDiscountModel>();
    String strPaymentID = "";
    boolean isEdit;
    String strPAID, strVAt;
    ArrayList<InvoiceAddItemModel> mArrayInVoice = new ArrayList<InvoiceAddItemModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        setStatusBar();
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = findViewById(R.id.llLeftLL);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.payment));

        txtSubTotalTV = findViewById(R.id.txtSubTotalTV);
        editVatET = findViewById(R.id.editVatET);
        txtTotalTV = findViewById(R.id.txtTotalTV);
        editPaidET = findViewById(R.id.editPaidET);
        txtBalaceDueTV = findViewById(R.id.txtBalaceDueTV);
        txtVatPriceTV = findViewById(R.id.txtVatPriceTV);
        btnSubmitB = findViewById(R.id.btnSubmitB);

        /* SubTotal */
       /* if (isEdit == true) {
            txtSubTotalTV.setText(mPaymentModel.getSubTotal());
            editVatET.setText(mPaymentModel.getVAT());
            txtTotalTV.setText(mPaymentModel.getTotal());
            editPaidET.setText(mPaymentModel.getPaid());
            txtBalaceDueTV.setText(mPaymentModel.getBalanceDue());
            txtVatPriceTV.setText(mPaymentModel.getVATPrice());
        } else {
            setUpSubTotal();
        }*/

        Bundle bundle = getIntent().getExtras();
//                For Handling Jobs Edit
        if (bundle != null && bundle.containsKey("mPaymnetModel")) {
            model = (PaymentModel) getIntent().getSerializableExtra("mPaymnetModel");
            strPaymentID = model.getPayment_id();
//            mSubTotal = Double.parseDouble(model.getSubTotal());
            if (!model.getSubTotal().equals("") && model.getSubTotal() != null) {
                mSubTotal = Double.parseDouble(model.getSubTotal());
                if (model.getPaid() != null) {
                    strPAID = model.getPaid().toString();
                    editPaidET.setText(strPAID);
                }
                if (model.getVAT() != null) {
                    strVAt = model.getVAT().toString();
                    editVatET.setText(strVAt);
                }
                txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            }


        }
        if (bundle != null && bundle.containsKey("ArrayList")) {
            itemsArrayList = (ArrayList<AddTotalInvoiceDiscountModel>) getIntent().getSerializableExtra("ArrayList");
            for (int i = 0; i < itemsArrayList.size(); i++) {

                AddTotalInvoiceDiscountModel mDiscountModel = itemsArrayList.get(i);

                if (mDiscountModel.getDiscountType().equals("Add Percentage")) {
                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());

                } else if (mDiscountModel.getDiscountType().equals("Subtract Percentage")) {
                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                } else if (mDiscountModel.getDiscountType().equals("Subtract Value")) {
                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                } else if (mDiscountModel.getDiscountType().equals("Add Value")) {
                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());

                }
                Log.e(TAG, "***Final Total***" + mTotalASDisc1);
            }
            mSubTotal = Double.parseDouble(Utilities.convertEvalueToNormal(mTotal1 + mTotalASDisc1));
            mTotalASDisc1 = 0;

            txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            String strValue = strVAt;
            if (strValue != null) {
                if (!strVAt.equals("")) {
                    Float mVat = Float.parseFloat(strVAt);
                    mVatCost = (mSubTotal * mVat) / 100;
                    mTotal = mSubTotal + mVatCost;
//                    mTotal = Utilities.getRoundOff2Decimal(mTotal);
//                    mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                    txtVatPriceTV.setText("" + Utilities.convertEvalueToNormal(mVatCost));
                    txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
                    txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
                    txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                }

            } else {
                txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            }

            String strValue1 = strPAID;
            if (strValue1 != null) {
                if (!strPAID.equals("")) {
                    mPaid = Double.parseDouble(strPAID);
                    mBalanceDue = mTotal - mPaid;
//                    mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                    txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mBalanceDue));
                    txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                }

            } else {
                txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            }
        } else if (bundle != null && bundle.containsKey("mArrayList")) {
            mArrayInVoice = (ArrayList<InvoiceAddItemModel>) getIntent().getSerializableExtra("mArrayList");
            for (int i = 0; i < mArrayInVoice.size(); i++) {
                if (i == 0) {
                    mAmount = Double.parseDouble(mArrayInVoice.get(i).getTotalAmount());
                    mSubTotal = mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                } else {
                    mAmount = Double.parseDouble(mArrayInVoice.get(i).getTotalAmount());
                    mSubTotal = mSubTotal + mAmount;
//                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                }
            }
            txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));

            String strValue = strVAt;
            if (strValue != null) {
                if (!strValue.equals("")) {
                    Float mVat = Float.parseFloat(strVAt);
                    mVatCost = (mSubTotal * mVat) / 100;
                    mTotal = mSubTotal + mVatCost;
//                    mTotal = Utilities.getRoundOff2Decimal(mTotal);
//                    mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                    txtVatPriceTV.setText("" + Utilities.convertEvalueToNormal(mVatCost));
                    txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
                    txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
                    txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                }
            } else {
                txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            }
            String strValue1 = strPAID;
            if (strValue1 != null) {
                if (!strPAID.equals("")) {
                    mPaid = Double.parseDouble(strPAID);
                    mBalanceDue = mTotal - mPaid;
//                    mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                    txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mBalanceDue));
                    txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                }

            } else {
                txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
                txtSubTotalTV.setText("" + Utilities.convertEvalueToNormal(mSubTotal));
            }
        }
        setUpDataOnWidgets();
//        setUpSubTotal();

    }

    private void setUpSubTotal() {
        if (itemsArrayList.size() != 0) {
        } else if (mArrayInVoice.size() != 0) {
        }
        /*Setting Up Data*/
    }

    private void setUpDataOnWidgets() {
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        editVatET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                    String strValue = editVatET.getText().toString();
//                    if (strValue.length() > 0) {
//                        Float mVat = Float.parseFloat(editVatET.getText().toString());
//                        mVatCost = (mSubTotal * mVat) / 100;
//                        mTotal = mSubTotal + mVatCost;
////                        mTotal = Utilities.getRoundOff2Decimal(mTotal);
////                        mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
//                        txtVatPriceTV.setText("" + Utilities.convertEvalueToNormal(mVatCost));
//                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
//                        txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });

//        editPaidET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                    String strValue = editPaidET.getText().toString();
//                    if (strValue.length() > 0) {
//                        mPaid = Double.parseDouble(editPaidET.getText().toString());
//                        mBalanceDue = mTotal - mPaid;
////                        mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
//                        txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mBalanceDue));
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });

        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utilities.isNetworkAvailable(mActivity)) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {

                    JaoharConstants.IS_Payment_Update = true;
                    /*Execute Api*/
                    // executeApi();
                    PaymentModel mModel = new PaymentModel();
                    mModel.setSubTotal("" + Utilities.getRoundOff2Decimal(mSubTotal));
                    if (editVatET.getText().toString().equals("")) {
                        mModel.setVATPrice("" + Utilities.getRoundOff2Decimal(0));
                        mModel.setVAT(String.valueOf(Utilities.getRoundOff2Decimal(Double.parseDouble("0"))));
                    } else {
                        mModel.setVATPrice("" + Utilities.getRoundOff2Decimal(mVatCost));
                        mModel.setVAT(String.valueOf(Utilities.getRoundOff2Decimal(Double.parseDouble(editVatET.getText().toString()))));
                    }

                    mModel.setPaid(editPaidET.getText().toString());
                    mModel.setTotal("" + Utilities.getRoundOff2Decimal(Double.parseDouble(txtTotalTV.getText().toString())));
                    mModel.setBalanceDue("" + Utilities.getRoundOff2Decimal(Double.parseDouble(txtBalaceDueTV.getText().toString())));

                    mModel.setPayment_id(strPaymentID);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("Model", mModel);
                    mActivity.setResult(888, returnIntent);
                    mActivity.finish();
                }
            }
        });
        editVatET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String strValue = editVatET.getText().toString();
                if (strValue.length() > 0) {
                    Float mVat = Float.parseFloat(editVatET.getText().toString());
                    mVatCost = (mSubTotal * mVat) / 100;
                    mTotal = mSubTotal + mVatCost;
//                        mTotal = Utilities.getRoundOff2Decimal(mTotal);
//                        mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                    txtVatPriceTV.setText("" + Utilities.convertEvalueToNormal(mVatCost));
                    txtTotalTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
                    txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mTotal));
                    String strValue1 = editPaidET.getText().toString();
                    if (strValue1.length() > 0) {
                        mPaid = Double.parseDouble(editPaidET.getText().toString());
                        mBalanceDue = mTotal - mPaid;
//                        mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                        txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mBalanceDue));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        editPaidET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String strValue = editPaidET.getText().toString();
                if (strValue.length() > 0) {
                    mPaid = Double.parseDouble(editPaidET.getText().toString());
                    mBalanceDue = mTotal - mPaid;
//                        mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                    txtBalaceDueTV.setText("" + Utilities.convertEvalueToNormal(mBalanceDue));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void executeApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addPaymentInvoiceRequest(txtSubTotalTV.getText().toString(), editVatET.getText().toString(), txtVatPriceTV.getText().toString(), txtTotalTV.getText().toString(), editPaidET.getText().toString(), txtBalaceDueTV.getText().toString(), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    if (jsonObject.getString("status").equals("1")) {
                        strPaymentID = jsonObject.getString("id");
                        showAlerDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
                    } else if (jsonObject.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

//    private void executeApi() {
//        String strAPIUrl = "";
//        strAPIUrl = JaoharConstants.ADD_INVOICE_PAYMENT + "?sub_total=" + txtSubTotalTV.getText().toString() +
//                "&VAT=" + editVatET.getText().toString() +
//                "&vat_price=" + txtVatPriceTV.getText().toString() +
//                "&total=" + txtTotalTV.getText().toString() +
//                "&paid=" + editPaidET.getText().toString() +
//                "&due=" + txtBalaceDueTV.getText().toString() +
//                "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//                 AlertDialogManager.showProgressDialog(mActivity);
//                 StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equals("1")) {
//                        strPaymentID = jsonObject.getString("id");
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        });
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                PaymentModel mModel = new PaymentModel();
                mModel.setSubTotal("" + mSubTotal);
                mModel.setVAT(editVatET.getText().toString());
                mModel.setPaid(editPaidET.getText().toString());
                mModel.setTotal("" + txtTotalTV.getText().toString());
                mModel.setBalanceDue("" + mBalanceDue);
                mModel.setVATPrice("" + mVatCost);
                mModel.setPayment_id(strPaymentID);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", mModel);
                mActivity.setResult(888, returnIntent);
                mActivity.finish();
            }
        });
        alertDialog.show();
    }
}
