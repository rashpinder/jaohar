package jaohar.com.jaohar.activities.blog_module;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.adapters.blog_adapter.Blog_CategoriesAdapter;
import jaohar.com.jaohar.beans.blog_module.Blog_CategoriesModel;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Categories_Interface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class BlogCategoryListActivity extends BaseActivity implements Blog_Categories_Interface {
    /**
     * set Activity
     **/
    Activity mActivity = BlogCategoryListActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = BlogCategoryListActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.blogRV)
    RecyclerView blogRV;
    ArrayList<Blog_CategoriesModel> modelArrayList = new ArrayList<>();
    Blog_CategoriesAdapter mAdapter;
    Blog_Categories_Interface mClickInterface;
    String strCategoryID = "";
    RelativeLayout imgRightLL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_blog_category_list);
        ButterKnife.bind(this);
        mClickInterface = this;
        getData();
    }



    /*
     *SetUp IDs to Views
     **/
    @Override
    protected void setViewsIDs() {

        /* set tool bar */
        txtCenter.setText(getString(R.string.blog_categories));
        imgRight.setImageResource(R.drawable.add_icon);
        imgBack.setImageResource(R.drawable.back);
    }



    @OnClick({R.id.llLeftLL,R.id.imgRightLL})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgRightLL:
                addEditDialog("Add Category", "", "add");
                break;
        }
    }



    @Override
    public void onResume() {
        super.onResume();

    }
    private void getData() {
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        }
        else {
            modelArrayList.clear();
            gettingBlogDATA();
        }
    }

    /*
     * API to Get All Blog list Using API
     * */
    private void gettingBlogDATA() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBlogCategories(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {

                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("1")) {
                        parseResponce(response.body().toString());
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                    }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
            }
        });
          }

    private void parseResponce(String response) {
        JSONObject mJSonObject = null;
        try {
            mJSonObject = new JSONObject(response);
            JSONArray mjsonArrayData = mJSonObject.getJSONArray("data");
            for (int i = 0; i < mjsonArrayData.length(); i++) {
                JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                Blog_CategoriesModel mModel = new Blog_CategoriesModel();
                if (!mJsonDATA.getString("id").equals("")) {
                    mModel.setId(mJsonDATA.getString("id"));
                }
                if (!mJsonDATA.getString("category").equals("")) {
                    mModel.setCategory(mJsonDATA.getString("category"));
                }
                if (!mJsonDATA.getString("creation_date").equals("")) {
                    mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                }
                modelArrayList.add(mModel);
            }

            /*
             * SetUp Adapter
             * */
            setAdapter();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        blogRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new Blog_CategoriesAdapter(mActivity, modelArrayList, mClickInterface);
        mAdapter.setHasStableIds(true);
        blogRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void mBlogCategoriesInterface(Blog_CategoriesModel mModel, String strType) {
        strCategoryID = mModel.getId();
        if (strType.equals("del")) {
            deleteConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_delete_));
        } else {
            addEditDialog("Edit Category", mModel.getCategory(), "edit");
        }

    }

    /* *
     * PopUp to Delete or Cancel the Message
     * */
    public void deleteConfirmDialog(String strMessage) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Blog Category
                 * */
                executedeleteAPI();
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }


    /* *
     * PopUp to Add or Edit the Category
     * */
    public void addEditDialog(String strTittle, String textMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dailog_add_edit_blog_categories);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final EditText editET = (EditText) deleteConfirmDialog.findViewById(R.id.editET);
        Button btnConfirm = (Button) deleteConfirmDialog.findViewById(R.id.btnConfirm);
        Button btnCancelTV = (Button) deleteConfirmDialog.findViewById(R.id.btnCancelTV);
        TextView tittleTV = (TextView) deleteConfirmDialog.findViewById(R.id.tittleTV);
        tittleTV.setText(strTittle);
        if (strType.equals("edit")) {
            editET.setText(textMessage);
        }

        btnCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_text));
                } else {
                    /*
                     * Execute API
                     * */

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeEditAPI(editET.getText().toString(), strType);
                    }
                    deleteConfirmDialog.dismiss();

                }

            }
        });

        deleteConfirmDialog.show();
    }

    /* *
     * Execute API for getting DeleteBlogCategory
     * @param
     * @user_id
     * @comment_id
     * */
    public void executedeleteAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteBlogCategory(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),strCategoryID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    modelArrayList.clear();
                        gettingBlogDATA();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
          }


    /* *
     * Execute API for  EditBlogCategory or AddBlogCategory
     * @param
     * @user_id
     * @category_id (in Case  edit)
     * @category
     * */
    public void executeEditAPI(String strData, String strType) {
        String strUrl = "";
        AlertDialogManager.showProgressDialog(mActivity);
        if (strType.equals("add")) {
//            strUrl = JaoharConstants.AddBlogCategory + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&category=" + strData;
            executeAddBlogCategory(strData);
        } else {
//            strUrl = JaoharConstants.EditBlogCategory + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&category_id=" + strCategoryID + "&category=" + strData;
            executeEditBlogCategory(strCategoryID,strData);
        }
    }



    private void executeAddBlogCategory(String strData) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addBlogCategory(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),strData);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    modelArrayList.clear();
                        gettingBlogDATA();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    private void executeEditBlogCategory(String strCategoryID, String strData) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editBlogCategory(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),strCategoryID,strData);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    modelArrayList.clear();
                            gettingBlogDATA();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name),mModel.getMessage());
                        }

            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
