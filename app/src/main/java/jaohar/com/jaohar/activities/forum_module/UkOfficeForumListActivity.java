package jaohar.com.jaohar.activities.forum_module;

import static android.view.View.GONE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.forum_module.ForumListAdapter;
import jaohar.com.jaohar.adapters.forum_module.ForumListNewAdapter;
import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickNewInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.models.forummodels.GetAllForumsModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class UkOfficeForumListActivity extends BaseActivity {
    /*
     * set Activity
     * */
    Activity mActivity = UkOfficeForumListActivity.this;
    /*
     * set Activity TAG
     * */
    String TAG = UkOfficeForumListActivity.this.getClass().getSimpleName();
    /**
     * Widgets
     */
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.dataRV)
    RecyclerView dataRV;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    int page_no = 1;
    String strLastPage = "TRUE";
    boolean isSwipeRefresh = false;
    /*
     * Setting Up Array List
     * */
    ArrayList<ForumModel> mLoadMore = new ArrayList<>();
    ArrayList<ForumModel> modelArrayList = new ArrayList<>();
    List<GetAllForumsModel.AllForum> mGetAllForumsList = new ArrayList<>();
    List<GetAllForumsModel.AllForum> mTempAllForumsList = new ArrayList<>();
    /*
     * Setting Up Adapter
     * */
    ForumListAdapter mAdapter;
    ForumListNewAdapter mAdapterNew;
    /* *
     * Setting Up Interface For selecting Items
     * */
    ForumItemClickNewInterace mInterfaceForumNew = new ForumItemClickNewInterace() {
        @Override
        public void ForumItemClick(GetAllForumsModel.AllForum mModel) {
            Intent mIntent = new Intent(mActivity, UKOfficeChatScreenForumActivity.class);
            mIntent.putExtra("forum_id", mModel.getId());
            mIntent.putExtra("room_id", mModel.getRoomId());
            mActivity.startActivity(mIntent);
        }
    };

    /**
     * Recycler View Pagination Adapter Interface
     **/
    PaginationListForumAdapter mPaginationInterFace = new PaginationListForumAdapter() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            if (!Utilities.isNetworkAvailable(mActivity)) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                                progressBottomPB.setVisibility(View.VISIBLE);
                                ++page_no;
                                //*Execute API For Getting List *//*
                                executeAPIRetrofit();
                            }
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_uk_office_forum_list);
        ButterKnife.bind(this);
    }

    @Override
    protected void setViewsIDs() {
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        imgRight.setVisibility(View.VISIBLE);
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setImageDrawable(getResources().getDrawable(R.drawable.add_icon));
        txtCenter.setText(getResources().getText(R.string.uk_forum));
        txtCenter.setGravity(Gravity.START);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(100, 0, 0, 0);
        txtCenter.setLayoutParams(params);

        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Utilities.isNetworkAvailable(mActivity)) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    isSwipeRefresh = true;
                    modelArrayList.clear();
                    mLoadMore.clear();
                    page_no = 1;
                    //*Execute API For Getting List *//*
                    executeAPIRetrofit();
                }
            }
        });
    }

    @OnClick({R.id.llLeftLL, R.id.imgRightLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgRightLL:
                performSwitch();
                break;
        }
    }

    private void performSwitch() {
        Intent mIntent = new Intent(mActivity, AddForumActivity.class);
        mIntent.putExtra("type", "UKstaff");
        startActivity(mIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, HomeActivity.class);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//        startActivity(mIntent);
//        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute API For Getting List *//*
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
            modelArrayList.clear();
            mLoadMore.clear();
            page_no = 1;
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            executeAPIRetrofit();
        }
    }

    /* *
     * Execute API for getting Forum list
     * @param
     * @user_id
     * */
    public void executeAPIRetrofit() {
        if (mTempAllForumsList != null) {
            mTempAllForumsList.clear();
        }
        if (strLastPage.equals("FALSE")) {

            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                isSwipeRefresh = false;
                swipeToRefresh.setRefreshing(false);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllForumsModel> call1 = mApiInterface.getForumListStaffNew(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<GetAllForumsModel>() {
            @Override
            public void onResponse(Call<GetAllForumsModel> call, retrofit2.Response<GetAllForumsModel> response) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (isSwipeRefresh == true) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }

                if (response.body() != null) {
                    if (!response.body().getData().getLastPage().equals("")) {
                        strLastPage = response.body().getData().getLastPage();
                    }

                    if (page_no == 1) {
                        mGetAllForumsList = response.body().getData().getAllForums();
                    } else if (page_no > 1) {
                        mTempAllForumsList = response.body().getData().getAllForums();
                    }

                    if (mTempAllForumsList.size() > 0) {
                        mGetAllForumsList.addAll(mTempAllForumsList);
                    }

                    if (page_no == 1) {
                        setAdapterNew();
                    } else {
                        mAdapterNew.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetAllForumsModel> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    /**
     * Setting Up Adapter
     **/
    private void setAdapterNew() {
        dataRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapterNew = new ForumListNewAdapter(mActivity, mGetAllForumsList, mPaginationInterFace, mInterfaceForumNew);
        dataRV.setAdapter(mAdapterNew);
        mAdapterNew.notifyDataSetChanged();
    }
}