package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.mail_list_all_activities.SendingMailToLIst_Vessels;
import jaohar.com.jaohar.activities.vessels_module.CopyVesselActivity;
import jaohar.com.jaohar.adapters.VesselesForSaleAdapter;
import jaohar.com.jaohar.beans.VessalClassModel;
import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.GetStaticLatestNewsModel;
import jaohar.com.jaohar.models.GetVesselClassData;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.ProfileData;
import jaohar.com.jaohar.models.ProfileModel;
import jaohar.com.jaohar.models.SearchModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.StaticLatestNewsData;
import jaohar.com.jaohar.models.VesselForSaleModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class VesselForSaleActivity extends BaseActivity {
    static boolean isbtnVisible = false;
    public String strGeared, strCapacityFrom, strCapacityTo, strName, strLoaFrom, strLoaTo, strType, strGrainFrom, strGrainTo, strYearofBuildFrom, strYearofBuildTo,
            strBaleFrom, strBaleTo, strStatus, strRecordID,IMO_number, strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo, strLiquidFrom,
            strLiquidTo, strClass, strPlaceofBuilt, strOffMarket;
    public String strLastPage = "FALSE", strPhotoUrlDEs = "", strPhotoURL = "", strTextNEWS = "";
    Activity mActivity = VesselForSaleActivity.this;
    String TAG = VesselForSaleActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter, txtRight, allnewsTV;
    RelativeLayout imgRightLL, downArrowRL, resetRL1;
    ImageView linkIMG;
    CircleImageView imgRight;
    EditText editSearchET;
    TextView txtAdvancedSearhTV;
    RecyclerView vesselRecyclerViewRV;
    SwipeRefreshLayout swipeToRefresh;
    VesselesForSaleAdapter mVesselesAdapter;
    ArrayList<AllVessel> mArrayList = new ArrayList<AllVessel>();
    ArrayList<VesselesModel> filteredList = new ArrayList<VesselesModel>();
//    ArrayList<VesselesModel> loadMoreArrayList = new ArrayList<VesselesModel>();
    ArrayList<AllVessel> loadMoreArrayList = new ArrayList<AllVessel>();
    boolean isSwipeRefresh = false;
    int page_no = 1;
    TextView normalTextTV;
    jaohar.com.jaohar.views.CustomNestedScrollView parentSV;
    boolean isAdvanceSearch = false;
    boolean isNormalSearch = false;
//    ArrayList<String> modelArrayList = new ArrayList<String>();
    ArrayList<GetVesselTypeData> modelArrayList = new ArrayList<GetVesselTypeData>();
    ArrayList<ProfileData> profileDataArrayList = new ArrayList<ProfileData>();
    ArrayList<StaticLatestNewsData> newsArrayList = new ArrayList<StaticLatestNewsData>();
    VesselTypesModel vesselTypesModel = new VesselTypesModel();
//    ArrayList<String> mArrayListClass = new ArrayList<String>();
    ArrayList<GetVesselClassData> mArrayListClass = new ArrayList<GetVesselClassData>();
    VessalClassModel vessalClassModel = new VessalClassModel();
    String arrayStatus[], typeArray[], strNews;
    ProgressBar progressBottomPB;
    ArrayList<String> mImageArryList = new ArrayList<>();

    OpenPopUpVesselInterface mOpenPoUpInterface = new OpenPopUpVesselInterface() {
        @Override
        public void mOpenPopUpVesselInterface(String strTextnews, String strPhotoURLs) {
            setUpNewsDialog(strTextnews, strPhotoURLs);
        }
    };

    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                if (isAdvanceSearch == false) {
                    if (isNormalSearch == false) {
                        progressBottomPB.setVisibility(View.VISIBLE);
                        ++page_no;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (strLastPage.equals("FALSE")) {
                                    executeAPI(page_no);
                                } else {
                                    progressBottomPB.setVisibility(View.GONE);
                                }
                            }
                        }, 1000);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vessel_for_sale);
        typeArray = mActivity.getResources().getStringArray(R.array.vessel_type_array_advanced);
        arrayStatus = mActivity.getResources().getStringArray(R.array.status_array_user);
    }

    @Override
    protected void setViewsIDs() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.vessel_for_sale));
        normalTextTV = (TextView) findViewById(R.id.normalTextTV);
        allnewsTV = (TextView) findViewById(R.id.allnewsTV);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        downArrowRL = (RelativeLayout) findViewById(R.id.downArrowRL);
        resetRL1 = (RelativeLayout) findViewById(R.id.resetRL1);
        imgRight = (CircleImageView) findViewById(R.id.imgRight);
        txtRight = (TextView) findViewById(R.id.txtRight);

        if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
            imgRightLL.setVisibility(View.VISIBLE);
        } else {
            imgRightLL.setVisibility(View.GONE);
        }

        txtAdvancedSearhTV = (TextView) findViewById(R.id.txtAdvancedSearhTV);
        vesselRecyclerViewRV = (RecyclerView) findViewById(R.id.vesselRecyclerViewRV);
        editSearchET = (EditText) findViewById(R.id.editSearchET);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        linkIMG = findViewById(R.id.linkIMG);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isAdvanceSearch = false;
                isNormalSearch = false;
                page_no = 1;
                editSearchET.setText("");
                executeAPI(page_no);
            }
        });
    }

    FavoriteVesselInterface mFavoriteVesselInterface = new FavoriteVesselInterface() {
        @Override
        public void mFavoriteVesselInterface(int position, String status, ImageView imageView) {
            String vessel_id = mArrayList.get(position).getRecordId();
            if (status.equals("1")) {
                executeFavoriteAPI(vessel_id, imageView);
            } else if (status.equals("0")) {
                executeUnFavoriteAPI(vessel_id, imageView);
            }
        }
    };

    private void setAdapter() {
        vesselRecyclerViewRV.setNestedScrollingEnabled(false);
        vesselRecyclerViewRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mVesselesAdapter = new VesselesForSaleAdapter(mActivity, mArrayList, strTextNEWS, strNews,
                strPhotoUrlDEs, mPagination, mOpenPoUpInterface, "false", new OnClickInterface() {
            @Override
            public void mOnClickInterface(int position) {
                PerformOptionsClick(position);
            }
        }, mFavoriteVesselInterface);
        vesselRecyclerViewRV.setAdapter(mVesselesAdapter);
    }

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_vessels_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout shareRL = view.findViewById(R.id.shareRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailToListRL = view.findViewById(R.id.mailToListRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);
        View shareView = view.findViewById(R.id.shareView);
        View editView = view.findViewById(R.id.editView);
        View copyView = view.findViewById(R.id.copyView);
        View mailToListView = view.findViewById(R.id.mailToListView);

        shareRL.setVisibility(View.GONE);
        editRL.setVisibility(View.GONE);
        copyRL.setVisibility(View.GONE);
        mailToListRL.setVisibility(View.GONE);
        shareView.setVisibility(View.GONE);
        editView.setVisibility(View.GONE);
        copyView.setVisibility(View.GONE);
        mailToListView.setVisibility(View.GONE);

        shareRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareViaWhatsApp();
            }
        });

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, EditVesselActivity.class);
                mIntent.putExtra("Model", String.valueOf(mArrayList.get(position)));
                mIntent.putExtra("Title", "Edit Vessels");
                mActivity.startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, CopyVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                mActivity.startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_All_Vessals = true;
                Intent mIntent = new Intent(mActivity, DetailsActivity.class);
                mIntent.putExtra("Model", String.valueOf(mArrayList.get(position)));
                mIntent.putExtra("isEditShow", true);
                mActivity.startActivity(mIntent);
            }
        });

        mailToListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.IsAllVesselsClick = true;
                Intent mIntent = new Intent(mActivity, SendingMailToLIst_Vessels.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, SendingMailActivity.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void shareViaWhatsApp() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        try {
            Objects.requireNonNull(mActivity).startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=jaohar.com.jaohar")));
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JaoharConstants.IS_CLICK_FROM_USER_VESSEL = true;
                JaoharConstants.BOOLEAN_USER_LOGOUT = false;
                JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                JaoharConstants.Is_click_form_Manager = false;


                Intent mIntent = new Intent(mActivity, UserProfileActivity.class);
                mActivity.startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });
        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAdvanceSearch = false;
                isNormalSearch = false;
                mArrayList.clear();
                loadMoreArrayList.clear();
                editSearchET.setText("");
                page_no = 1;
                executeAPI(page_no);
            }
        });
        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gettingVessal_Type();
                gettingVessal_Class();
                advancedSearchView(typeArray);
            }
        });
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public void setUpNewsDialog(String strNormalTEXT, final String strPhotoURL) {
        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.activity_full_news_description);
        searchDialog.setCanceledOnTouchOutside(true);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        ImageView add_image1 = (ImageView) searchDialog.findViewById(R.id.add_image1);
        WebView showTXT = (WebView) searchDialog.findViewById(R.id.showTXT);
        final ProgressBar progressbar1 = searchDialog.findViewById(R.id.progressbar1);
        progressbar1.setVisibility(View.VISIBLE);
        if (!strPhotoURL.equals("")) {
            Picasso.get().load(strPhotoURL).into(add_image1);
        }
        add_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!strPhotoURL.equals("")) {
                    mImageArryList.clear();
                    mImageArryList.add(strPhotoURL);
                    Intent intent = new Intent(mActivity, GalleryActivity.class);
                    intent.putStringArrayListExtra("LIST", mImageArryList);
                    startActivity(intent);
                }
            }
        });
        showTXT.getSettings().setJavaScriptEnabled(true);
        showTXT.getSettings().setLoadWithOverviewMode(true);
        showTXT.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressbar1.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progressbar1.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressbar1.setVisibility(View.GONE);
            }
        });

        showTXT.loadDataWithBaseURL(null, strNormalTEXT, "text/html", "UTF-8", null);
        WebSettings webSettings = showTXT.getSettings();
        Resources res = mActivity.getResources();
        webSettings.setDefaultFontSize((int) res.getDimension(R.dimen._3sdp));
        searchDialog.show();
    }

    private void gettingVessal_Type() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
            @Override
            public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                GetVesselTypeModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    modelArrayList=mModel.getData();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}

//    private void gettingVessal_Type() {
//        String strUrl = JaoharConstants.GET_ALL_VESSEL_TYPES;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "******" + response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//
//                    if (jsonObject.getString("status").equals("1")) {
//                        if (!jsonObject.isNull("data")) {
//                            JSONArray vehiclesArray = jsonObject.getJSONArray("data");
//
//                            for (int i = 0; i < vehiclesArray.length(); i++) {
//                                JSONObject mJsonObject1 = vehiclesArray.getJSONObject(i);
//
//                                vesselTypesModel.setId(mJsonObject1.getString("id"));
//                                vesselTypesModel.setName(mJsonObject1.getString("name"));
//                                modelArrayList.add(mJsonObject1.getString("name"));
//                            }
//                        }
//
//                    } else {
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void gettingVessal_Class() {
//        String strUrl = JaoharConstants.Get_Vessel_Class;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "******" + response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//
//                    if (jsonObject.getString("status").equals("1")) {
//                        if (!jsonObject.isNull("data")) {
//                            JSONArray vehiclesArray = jsonObject.getJSONArray("data");
//
//                            for (int i = 0; i < vehiclesArray.length(); i++) {
//                                JSONObject mJsonObject1 = vehiclesArray.getJSONObject(i);
//                                vessalClassModel.setId(mJsonObject1.getString("id"));
//                                vessalClassModel.setClass_name(mJsonObject1.getString("class_name"));
//                                mArrayListClass.add(mJsonObject1.getString("class_name"));
//                            }
//                        }
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void gettingVessal_Class() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselClassModel> call1 = mApiInterface.getVesselClassRequest();
        call1.enqueue(new Callback<GetVesselClassModel>() {
            @Override
            public void onResponse(Call<GetVesselClassModel> call, retrofit2.Response<GetVesselClassModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                GetVesselClassModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mArrayListClass=mModel.getData();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetVesselClassModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


    private void executeGettingStaticNews() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetStaticLatestNewsModel> call1 = mApiInterface.getLatestStaticNewsRequest();
        call1.enqueue(new Callback<GetStaticLatestNewsModel>() {
            @Override
            public void onResponse(Call<GetStaticLatestNewsModel> call, retrofit2.Response<GetStaticLatestNewsModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                GetStaticLatestNewsModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    newsArrayList.add(mModel.getData());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetStaticLatestNewsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}

//    private void executeGettingStaticNews() {
//        String strAPIUrl = JaoharConstants.Get_Latest_Static_News;
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + response);
//                try {
//                    JSONObject mJsonObject1 = new JSONObject(response);
//                    if (mJsonObject1.getString("status").equals("1")) {
//                        JSONObject mDataJsonObject = mJsonObject1.getJSONObject("data");
//                        String formattedText = mDataJsonObject.getString("news");
//                        strNews = mDataJsonObject.getString("news");
//                        strTextNEWS = mDataJsonObject.getString("news_text");
//                        strPhotoUrlDEs = mDataJsonObject.getString("photo");
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(mStringRequest);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        mArrayList.clear();
        loadMoreArrayList.clear();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                    gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
                } else {
                    gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
                }
            } else {
                gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            }
            if (isAdvanceSearch == false) {
                if (isNormalSearch == false) {

                    if (strLastPage.equals("FALSE"))
                        editSearchET.setText("");
                    executeGettingStaticNews();
                    executeAPI(page_no);
                } else {
                    executeNormalSearch();
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent(mActivity, HomeActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mIntent.putExtra(JaoharConstants.LOGIN, "Home");
        mActivity.startActivity(mIntent);
        mActivity.finish();
    }

    private void executeNormalSearch() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.searchVesselRequest(editSearchET.getText().toString(), "user","0");
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                isNormalSearch = true;
                SearchModel mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    mArrayList.clear();
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    //mVesselesModel.setImage(imagesArray[i]);
                    mArrayList = mModel.getAllSearchedVessels();
                    setAdapter();

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }}

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });}

//    private void executeNormalSearch() {
//        String strUrl = JaoharConstants.ROLE_SEARCH_API + "?input_search_value=" + editSearchET.getText().toString() + "&role=" + "user" + "&user_id=0";
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                resetRL1.setVisibility(View.VISIBLE);
//                Log.e(TAG, "*****Response****" + response);
//                isNormalSearch = true;
//                try {
//                    JSONObject mJsonObject11 = new JSONObject(response);
//
//                    if (mJsonObject11.getString("status").equals("1")) {
//                        mArrayList.clear();
//                        JSONArray mJsonArray = mJsonObject11.getJSONArray("all_searched_vessels");
//                        for (int i = 0; i < mJsonArray.length(); i++) {
//                            JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                            VesselesModel mVesselesModel = new VesselesModel();
//                            if (!mJsonObject1.isNull("record_id")) {
//                                mVesselesModel.setRecord_ID(mJsonObject1.getString("record_id"));
//                            }
//                            if (!mJsonObject1.isNull("IMO_number")) {
//                                mVesselesModel.setIMO_Number(mJsonObject1.getString("IMO_number"));
//                            }
//                            if (!mJsonObject1.isNull("short_description")) {
//                                mVesselesModel.setShort_Discription(mJsonObject1.getString("short_description"));
//                            }
//                            if (!mJsonObject1.isNull("vessel_name")) {
//                                mVesselesModel.setVessel_Name(mJsonObject1.getString("vessel_name"));
//                            }
//                            if (!mJsonObject1.isNull("vessel_type")) {
//                                mVesselesModel.setVessel_Type(mJsonObject1.getString("vessel_type"));
//                            }
//                            if (!mJsonObject1.isNull("year_built")) {
//                                mVesselesModel.setYear_Built(mJsonObject1.getString("year_built"));
//                            }
//                            if (!mJsonObject1.isNull("place_of_built")) {
//                                mVesselesModel.setPlace_of_Built(mJsonObject1.getString("place_of_built"));
//                            }
//                            if (!mJsonObject1.isNull("class")) {
//                                mVesselesModel.setmClass(mJsonObject1.getString("class"));
//                            }
//                            if (!mJsonObject1.isNull("flag")) {
//                                mVesselesModel.setFlag(mJsonObject1.getString("flag"));
//                            }
//                            if (!mJsonObject1.isNull("capacity")) {
//                                mVesselesModel.setDWT_Capacity(mJsonObject1.getString("capacity"));
//                            }
//                            if (!mJsonObject1.isNull("loa")) {
//                                mVesselesModel.setLOA_M(mJsonObject1.getString("loa"));
//                            }
//
//                            if (!mJsonObject1.isNull("breadth")) {
//                                mVesselesModel.setBreadth_M(mJsonObject1.getString("breadth"));
//                            }
//                            if (!mJsonObject1.isNull("depth")) {
//                                mVesselesModel.setDepth(mJsonObject1.getString("depth"));
//                            }
//                            if (!mJsonObject1.isNull("price_idea")) {
//                                mVesselesModel.setPrice_Idea(mJsonObject1.getString("price_idea"));
//                            }
//                            if (!mJsonObject1.isNull("vessel_location")) {
//                                mVesselesModel.setVessel_Location(mJsonObject1.getString("vessel_location"));
//                            }
//                            if (!mJsonObject1.isNull("full_description")) {
//                                mVesselesModel.setFull_Discription(mJsonObject1.getString("full_description"));
//                            }
//                            if (!mJsonObject1.isNull("currency")) {
//                                mVesselesModel.setCurrency(mJsonObject1.getString("currency"));
//                            }
//                            if (!mJsonObject1.isNull("photo1")) {
//                                mVesselesModel.setPhoto1(mJsonObject1.getString("photo1"));
//                            }
//                            if (!mJsonObject1.isNull("photo2")) {
//                                mVesselesModel.setPhoto2(mJsonObject1.getString("photo2"));
//                            }
//                            if (!mJsonObject1.isNull("photo3")) {
//                                mVesselesModel.setPhoto3(mJsonObject1.getString("photo3"));
//                            }
//                            if (!mJsonObject1.isNull("photo4")) {
//                                mVesselesModel.setPhoto4(mJsonObject1.getString("photo4"));
//                            }
//                            if (!mJsonObject1.isNull("photo5")) {
//                                mVesselesModel.setPhoto5(mJsonObject1.getString("photo5"));
//                            }
//                            if (!mJsonObject1.isNull("photo6")) {
//                                mVesselesModel.setPhoto6(mJsonObject1.getString("photo6"));
//                            }
//                            if (!mJsonObject1.isNull("photo7")) {
//                                mVesselesModel.setPhoto7(mJsonObject1.getString("photo7"));
//                            }
//                            if (!mJsonObject1.isNull("photo8")) {
//                                mVesselesModel.setPhoto8(mJsonObject1.getString("photo8"));
//                            }
//                            if (!mJsonObject1.isNull("photo9")) {
//                                mVesselesModel.setPhoto9(mJsonObject1.getString("photo9"));
//                            }
//                            if (!mJsonObject1.isNull("photo10")) {
//                                mVesselesModel.setPhoto10(mJsonObject1.getString("photo10"));
//                            }
//                            if (!mJsonObject1.isNull("date_entered")) {
//                                mVesselesModel.setDate_Entered(mJsonObject1.getString("date_entered"));
//                            }
//                            if (!mJsonObject1.isNull("status")) {
//                                mVesselesModel.setStatus(mJsonObject1.getString("status"));
//                            }
//                            if (!mJsonObject1.isNull("bale")) {
//                                mVesselesModel.setBale(mJsonObject1.getString("bale"));
//                            }
//                            if (!mJsonObject1.isNull("grain")) {
//                                mVesselesModel.setGrain(mJsonObject1.getString("grain"));
//                            }
//                            if (!mJsonObject1.isNull("date_for_vessel")) {
//                                mVesselesModel.setDate_for_Vessel(mJsonObject1.getString("date_for_vessel"));
//                            }
//
//                            if (!mJsonObject1.isNull("vessel_added_by")) {
//                                mVesselesModel.setVessel_added_by(mJsonObject1.getString("vessel_added_by"));
//                            }
//                            if (!mJsonObject1.isNull("document1")) {
//                                mVesselesModel.setDocument1(mJsonObject1.getString("document1"));
//                            }
//                            if (!mJsonObject1.isNull("document2")) {
//                                mVesselesModel.setDocument2(mJsonObject1.getString("document2"));
//                            }
//                            if (!mJsonObject1.isNull("document3")) {
//                                mVesselesModel.setDocument3(mJsonObject1.getString("document3"));
//                            }
//                            if (!mJsonObject1.isNull("document4")) {
//                                mVesselesModel.setDocument4(mJsonObject1.getString("document4"));
//                            }
//                            if (!mJsonObject1.isNull("document5")) {
//                                mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                            }
//                            if (!mJsonObject1.isNull("document6")) {
//                                mVesselesModel.setDocument6(mJsonObject1.getString("document6"));
//                            }
//                            if (!mJsonObject1.isNull("document7")) {
//                                mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                            }
//                            if (!mJsonObject1.isNull("document7")) {
//                                mVesselesModel.setDocument7(mJsonObject1.getString("document7"));
//                            }
//                            if (!mJsonObject1.isNull("document8")) {
//                                mVesselesModel.setDocument8(mJsonObject1.getString("document8"));
//                            }
//                            if (!mJsonObject1.isNull("document9")) {
//                                mVesselesModel.setDocument9(mJsonObject1.getString("document9"));
//                            }
//                            if (!mJsonObject1.isNull("document10")) {
//                                mVesselesModel.setDocument10(mJsonObject1.getString("document10"));
//                            }
//                            if (!mJsonObject1.isNull("vessel_add_time")) {
//                                mVesselesModel.setVessel_add_time(mJsonObject1.getString("vessel_add_time"));
//                            }
//                            if (!mJsonObject1.isNull("document1name")) {
//                                mVesselesModel.setDocument1name(mJsonObject1.getString("document1name"));
//                            }
//                            if (!mJsonObject1.isNull("document2name")) {
//                                mVesselesModel.setDocument2name(mJsonObject1.getString("document2name"));
//                            }
//                            if (!mJsonObject1.isNull("document3name")) {
//                                mVesselesModel.setDocument3name(mJsonObject1.getString("document3name"));
//                            }
//                            if (!mJsonObject1.isNull("document4name")) {
//                                mVesselesModel.setDocument4name(mJsonObject1.getString("document4name"));
//                            }
//                            if (!mJsonObject1.isNull("document5name")) {
//                                mVesselesModel.setDocument5name(mJsonObject1.getString("document5name"));
//                            }
//                            if (!mJsonObject1.isNull("document6name")) {
//                                mVesselesModel.setDocument6name(mJsonObject1.getString("document6name"));
//                            }
//                            if (!mJsonObject1.isNull("document7name")) {
//                                mVesselesModel.setDocument7name(mJsonObject1.getString("document7name"));
//                            }
//                            if (!mJsonObject1.isNull("document8name")) {
//                                mVesselesModel.setDocument8name(mJsonObject1.getString("document8name"));
//                            }
//                            if (!mJsonObject1.isNull("document9name")) {
//                                mVesselesModel.setDocument9name(mJsonObject1.getString("document9name"));
//                            }
//                            if (!mJsonObject1.isNull("document10name")) {
//                                mVesselesModel.setDocument10name(mJsonObject1.getString("document10name"));
//                            }
//                            if (!mJsonObject1.isNull("mail_data")) {
//                                mVesselesModel.setMail_data(mJsonObject1.getString("mail_data"));
//                            }
//                            if (!mJsonObject1.isNull("builder")) {
//                                mVesselesModel.setBuilder(mJsonObject1.getString("builder"));
//                            }
//                            if (!mJsonObject1.isNull("draught")) {
//                                mVesselesModel.setDraught(mJsonObject1.getString("draught"));
//                            }
//                            if (!mJsonObject1.isNull("gross_tonnage")) {
//                                mVesselesModel.setGross_tonnage(mJsonObject1.getString("gross_tonnage"));
//                            }
//                            if (!mJsonObject1.isNull("net_tonnage")) {
//                                mVesselesModel.setNet_tonnage(mJsonObject1.getString("net_tonnage"));
//                            }
//                            if (!mJsonObject1.isNull("teu")) {
//                                mVesselesModel.setTeu(mJsonObject1.getString("teu"));
//                            }
//                            if (!mJsonObject1.isNull("liquid")) {
//                                mVesselesModel.setLiquid(mJsonObject1.getString("liquid"));
//                            }
//                            if (!mJsonObject1.isNull("url")) {
//                                mVesselesModel.setUrl(mJsonObject1.getString("url"));
//                            }
//                            if (!mJsonObject1.isNull("gas")) {
//                                mVesselesModel.setGas(mJsonObject1.getString("gas"));
//                            }
//                            if (!mJsonObject1.isNull("no_passengers")) {
//                                mVesselesModel.setNo_passengers(mJsonObject1.getString("no_passengers"));
//                            }
//                            //mVesselesModel.setImage(imagesArray[i]);
////                            mArrayList.add(mVesselesModel);
//                        }
//
//                        /*SetAdapter*/
//                        setAdapter();
//
//                    } else if (mJsonObject11.getString("status").equals("100")) {
//                        AlertDialogManager.hideProgressDialog();
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject11.getString("message"));
//                    } else {
//                        AlertDialogManager.hideProgressDialog();
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject11.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    public void executeAPI(int page_no) {
//        String strUrl = JaoharConstants.GET_ALL_VESSELES + "?page_no=" + page_no + "&user_id=0";// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//
//        if (page_no > 1) {
//            progressBottomPB.setVisibility(View.VISIBLE);
//            AlertDialogManager.hideProgressDialog();
//        }
//        if (page_no == 1) {
//            progressBottomPB.setVisibility(View.GONE);
//            AlertDialogManager.showProgressDialog(mActivity);
//        }
//
//        if (!isSwipeRefresh) {
//            progressBottomPB.setVisibility(View.GONE);
//
//        }
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                resetRL1.setVisibility(View.GONE);
//                progressBottomPB.setVisibility(View.GONE);
//                if (isSwipeRefresh) {
//                    swipeToRefresh.setRefreshing(false);
//                }
//
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void executeAPI(int page_no) {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
            AlertDialogManager.hideProgressDialog();
        }
        if (page_no == 1) {
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(mActivity);
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);

        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<VesselForSaleModel> call1 = mApiInterface.getAllVesselsRequest(String.valueOf(page_no), "0");
        call1.enqueue(new Callback<VesselForSaleModel>() {
            @Override
            public void onResponse(Call<VesselForSaleModel> call, retrofit2.Response<VesselForSaleModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.GONE);
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                VesselForSaleModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mArrayList = mModel.getData().getAllVessels();
                    strLastPage = mModel.getData().getLastPage();

                    if (page_no == 1) {
//                            mArrayList.add(mVesselesModel);
                    } else if (page_no > 1) {
//                            loadMoreArrayList.add(mVesselesModel);
                    }

                if (loadMoreArrayList.size() > 0) {
//                        mArrayList.addAll(loadMoreArrayList);
                }
                if (page_no == 1) {
                    /*SetAdapter*/
                    setAdapter();
                } else {
                    mVesselesAdapter.notifyDataSetChanged();
                }}
                else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" +mModel.getMessage());
                } else {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }}

            @Override
            public void onFailure(Call<VesselForSaleModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}

//    private void parseResponse(String response) {
//        try {
//            JSONObject mJsonObject = new JSONObject(response);
//
//            if (mJsonObject.getString("status").equals("1")) {
//                if (!mJsonObject.isNull("data")) {
//                    JSONObject mDataObject = mJsonObject.getJSONObject("data");
//                    JSONArray mJsonArray = mDataObject.getJSONArray("all_vessels");
//                    strLastPage = mDataObject.getString("last_page");
//
//                    for (int i = 0; i < mJsonArray.length(); i++) {
//                        JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                        VesselesModel mVesselesModel = new VesselesModel();
//                        if (!mJsonObject1.isNull("record_id")) {
//                            mVesselesModel.setRecord_ID(mJsonObject1.getString("record_id"));
//                        }
//                        if (!mJsonObject1.isNull("IMO_number")) {
//                            mVesselesModel.setIMO_Number(mJsonObject1.getString("IMO_number"));
//                        }
//                        if (!mJsonObject1.isNull("short_description")) {
//                            mVesselesModel.setShort_Discription(mJsonObject1.getString("short_description"));
//                        }
//                        if (!mJsonObject1.isNull("vessel_name")) {
//                            mVesselesModel.setVessel_Name(mJsonObject1.getString("vessel_name"));
//                        }
//                        if (!mJsonObject1.isNull("vessel_type")) {
//                            mVesselesModel.setVessel_Type(mJsonObject1.getString("vessel_type"));
//                        }
//                        if (!mJsonObject1.isNull("year_built")) {
//                            mVesselesModel.setYear_Built(mJsonObject1.getString("year_built"));
//                        }
//                        if (!mJsonObject1.isNull("place_of_built")) {
//                            mVesselesModel.setPlace_of_Built(mJsonObject1.getString("place_of_built"));
//                        }
//                        if (!mJsonObject1.isNull("class")) {
//                            mVesselesModel.setmClass(mJsonObject1.getString("class"));
//                        }
//                        if (!mJsonObject1.isNull("flag")) {
//                            mVesselesModel.setFlag(mJsonObject1.getString("flag"));
//                        }
//                        if (!mJsonObject1.isNull("capacity")) {
//                            mVesselesModel.setDWT_Capacity(mJsonObject1.getString("capacity"));
//                        }
//                        if (!mJsonObject1.isNull("loa")) {
//                            mVesselesModel.setLOA_M(mJsonObject1.getString("loa"));
//                        }
//
//                        if (!mJsonObject1.isNull("breadth")) {
//                            mVesselesModel.setBreadth_M(mJsonObject1.getString("breadth"));
//                        }
//                        if (!mJsonObject1.isNull("depth")) {
//                            mVesselesModel.setDepth(mJsonObject1.getString("depth"));
//                        }
//                        if (!mJsonObject1.isNull("price_idea")) {
//                            mVesselesModel.setPrice_Idea(mJsonObject1.getString("price_idea"));
//                        }
//                        if (!mJsonObject1.isNull("vessel_location")) {
//                            mVesselesModel.setVessel_Location(mJsonObject1.getString("vessel_location"));
//                        }
//                        if (!mJsonObject1.isNull("full_description")) {
//                            mVesselesModel.setFull_Discription(mJsonObject1.getString("full_description"));
//                        }
//                        if (!mJsonObject1.isNull("currency")) {
//                            mVesselesModel.setCurrency(mJsonObject1.getString("currency"));
//                        }
//                        if (!mJsonObject1.isNull("photo1")) {
//                            mVesselesModel.setPhoto1(mJsonObject1.getString("photo1"));
//                        }
//                        if (!mJsonObject1.isNull("photo2")) {
//                            mVesselesModel.setPhoto2(mJsonObject1.getString("photo2"));
//                        }
//                        if (!mJsonObject1.isNull("photo3")) {
//                            mVesselesModel.setPhoto3(mJsonObject1.getString("photo3"));
//                        }
//                        if (!mJsonObject1.isNull("photo4")) {
//                            mVesselesModel.setPhoto4(mJsonObject1.getString("photo4"));
//                        }
//                        if (!mJsonObject1.isNull("photo5")) {
//                            mVesselesModel.setPhoto5(mJsonObject1.getString("photo5"));
//                        }
//                        if (!mJsonObject1.isNull("photo6")) {
//                            mVesselesModel.setPhoto6(mJsonObject1.getString("photo6"));
//                        }
//                        if (!mJsonObject1.isNull("photo7")) {
//                            mVesselesModel.setPhoto7(mJsonObject1.getString("photo7"));
//                        }
//                        if (!mJsonObject1.isNull("photo8")) {
//                            mVesselesModel.setPhoto8(mJsonObject1.getString("photo8"));
//                        }
//                        if (!mJsonObject1.isNull("photo9")) {
//                            mVesselesModel.setPhoto9(mJsonObject1.getString("photo9"));
//                        }
//                        if (!mJsonObject1.isNull("photo10")) {
//                            mVesselesModel.setPhoto10(mJsonObject1.getString("photo10"));
//                        }
//                        if (!mJsonObject1.isNull("date_entered")) {
//                            mVesselesModel.setDate_Entered(mJsonObject1.getString("date_entered"));
//                        }
//                        if (!mJsonObject1.isNull("status")) {
//                            mVesselesModel.setStatus(mJsonObject1.getString("status"));
//                        }
//                        if (!mJsonObject1.isNull("bale")) {
//                            mVesselesModel.setBale(mJsonObject1.getString("bale"));
//                        }
//                        if (!mJsonObject1.isNull("grain")) {
//                            mVesselesModel.setGrain(mJsonObject1.getString("grain"));
//                        }
//                        if (!mJsonObject1.isNull("date_for_vessel")) {
//                            mVesselesModel.setDate_for_Vessel(mJsonObject1.getString("date_for_vessel"));
//                        }
//
//                        if (!mJsonObject1.isNull("vessel_added_by")) {
//                            mVesselesModel.setVessel_added_by(mJsonObject1.getString("vessel_added_by"));
//                        }
//                        if (!mJsonObject1.isNull("document1")) {
//                            mVesselesModel.setDocument1(mJsonObject1.getString("document1"));
//                        }
//                        if (!mJsonObject1.isNull("document2")) {
//                            mVesselesModel.setDocument2(mJsonObject1.getString("document2"));
//                        }
//                        if (!mJsonObject1.isNull("document3")) {
//                            mVesselesModel.setDocument3(mJsonObject1.getString("document3"));
//                        }
//                        if (!mJsonObject1.isNull("document4")) {
//                            mVesselesModel.setDocument4(mJsonObject1.getString("document4"));
//                        }
//                        if (!mJsonObject1.isNull("document5")) {
//                            mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                        }
//                        if (!mJsonObject1.isNull("document6")) {
//                            mVesselesModel.setDocument6(mJsonObject1.getString("document6"));
//                        }
//                        if (!mJsonObject1.isNull("document7")) {
//                            mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                        }
//                        if (!mJsonObject1.isNull("document7")) {
//                            mVesselesModel.setDocument7(mJsonObject1.getString("document7"));
//                        }
//                        if (!mJsonObject1.isNull("document8")) {
//                            mVesselesModel.setDocument8(mJsonObject1.getString("document8"));
//                        }
//                        if (!mJsonObject1.isNull("document9")) {
//                            mVesselesModel.setDocument9(mJsonObject1.getString("document9"));
//                        }
//                        if (!mJsonObject1.isNull("document10")) {
//                            mVesselesModel.setDocument10(mJsonObject1.getString("document10"));
//                        }
//                        if (!mJsonObject1.isNull("vessel_add_time")) {
//                            mVesselesModel.setVessel_add_time(mJsonObject1.getString("vessel_add_time"));
//                        }
//                        if (!mJsonObject1.isNull("document1name")) {
//                            mVesselesModel.setDocument1name(mJsonObject1.getString("document1name"));
//                        }
//                        if (!mJsonObject1.isNull("document2name")) {
//                            mVesselesModel.setDocument2name(mJsonObject1.getString("document2name"));
//                        }
//                        if (!mJsonObject1.isNull("document3name")) {
//                            mVesselesModel.setDocument3name(mJsonObject1.getString("document3name"));
//                        }
//                        if (!mJsonObject1.isNull("document4name")) {
//                            mVesselesModel.setDocument4name(mJsonObject1.getString("document4name"));
//                        }
//                        if (!mJsonObject1.isNull("document5name")) {
//                            mVesselesModel.setDocument5name(mJsonObject1.getString("document5name"));
//                        }
//                        if (!mJsonObject1.isNull("document6name")) {
//                            mVesselesModel.setDocument6name(mJsonObject1.getString("document6name"));
//                        }
//                        if (!mJsonObject1.isNull("document7name")) {
//                            mVesselesModel.setDocument7name(mJsonObject1.getString("document7name"));
//                        }
//                        if (!mJsonObject1.isNull("document8name")) {
//                            mVesselesModel.setDocument8name(mJsonObject1.getString("document8name"));
//                        }
//                        if (!mJsonObject1.isNull("document9name")) {
//                            mVesselesModel.setDocument9name(mJsonObject1.getString("document9name"));
//                        }
//                        if (!mJsonObject1.isNull("document10name")) {
//                            mVesselesModel.setDocument10name(mJsonObject1.getString("document10name"));
//                        }
//                        if (!mJsonObject1.isNull("mail_data")) {
//                            mVesselesModel.setMail_data(mJsonObject1.getString("mail_data"));
//                        }
//                        if (!mJsonObject1.isNull("builder")) {
//                            mVesselesModel.setBuilder(mJsonObject1.getString("builder"));
//                        }
//                        if (!mJsonObject1.isNull("draught")) {
//                            mVesselesModel.setDraught(mJsonObject1.getString("draught"));
//                        }
//                        if (!mJsonObject1.isNull("gross_tonnage")) {
//                            mVesselesModel.setGross_tonnage(mJsonObject1.getString("gross_tonnage"));
//                        }
//                        if (!mJsonObject1.isNull("net_tonnage")) {
//                            mVesselesModel.setNet_tonnage(mJsonObject1.getString("net_tonnage"));
//                        }
//                        if (!mJsonObject1.isNull("teu")) {
//                            mVesselesModel.setTeu(mJsonObject1.getString("teu"));
//                        }
//                        if (!mJsonObject1.isNull("liquid")) {
//                            mVesselesModel.setLiquid(mJsonObject1.getString("liquid"));
//                        }
//                        if (!mJsonObject1.isNull("url")) {
//                            mVesselesModel.setUrl(mJsonObject1.getString("url"));
//                        }
//                        if (!mJsonObject1.isNull("gas")) {
//                            mVesselesModel.setGas(mJsonObject1.getString("gas"));
//                        }
//                        if (!mJsonObject1.isNull("no_passengers")) {
//                            mVesselesModel.setNo_passengers(mJsonObject1.getString("no_passengers"));
//                        }
//
//                        if (page_no == 1) {
////                            mArrayList.add(mVesselesModel);
//                        } else if (page_no > 1) {
////                            loadMoreArrayList.add(mVesselesModel);
//                        }
//                    }
//
//                    if (loadMoreArrayList.size() > 0) {
////                        mArrayList.addAll(loadMoreArrayList);
//                    }
//                    if (page_no == 1) {
//                        /*SetAdapter*/
//                        setAdapter();
//                    } else {
//                        mVesselesAdapter.notifyDataSetChanged();
//                    }
//                }
//            } else {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void advancedSearchView(String[] vesselTypesArray) {
        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advanced_search);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = (Button) searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = (ImageView) searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editRecordIDET = (EditText) searchDialog.findViewById(R.id.editRecordIDET);
        final EditText editFromCapacityET = (EditText) searchDialog.findViewById(R.id.editFromCapacityET);
        final EditText editToCapacityET = (EditText) searchDialog.findViewById(R.id.editToCapacityET);
        final EditText editNameET = (EditText) searchDialog.findViewById(R.id.editNameET);
        final EditText editFromLoaET = (EditText) searchDialog.findViewById(R.id.editFromLoaET);
        final EditText editToLoaET = (EditText) searchDialog.findViewById(R.id.editToLoaET);
        final EditText editFromGrainET = (EditText) searchDialog.findViewById(R.id.editFromGrainET);
        final EditText editToGrainET = (EditText) searchDialog.findViewById(R.id.editToGrainET);
        final EditText editFromYearBuiltET = (EditText) searchDialog.findViewById(R.id.editFromYearBuiltET);
        final EditText editToYearBuiltET = (EditText) searchDialog.findViewById(R.id.editToYearBuiltET);
        final EditText editFromBaleET = (EditText) searchDialog.findViewById(R.id.editFromBaleET);
        final EditText editToBaleET = (EditText) searchDialog.findViewById(R.id.editToBaleET);
        final EditText editStatusET = (EditText) searchDialog.findViewById(R.id.editStatusET);
        final EditText editFromTEUET = (EditText) searchDialog.findViewById(R.id.editFromTEUET);
        final EditText editToTEUET = (EditText) searchDialog.findViewById(R.id.editToTEUET);
        final EditText editFromPassengerET = (EditText) searchDialog.findViewById(R.id.editFromPassengerET);
        final EditText editToPassendgerET = (EditText) searchDialog.findViewById(R.id.editToPassendgerET);
        final EditText editFromDraftrET = (EditText) searchDialog.findViewById(R.id.editFromDraftrET);
        final EditText editToDraftET = (EditText) searchDialog.findViewById(R.id.editToDraftET);
        final EditText editFromLiquidET = (EditText) searchDialog.findViewById(R.id.editFromLiquidET);
        final EditText editToLiquidET = (EditText) searchDialog.findViewById(R.id.editToLiquidET);
        final EditText editTypeET = (EditText) searchDialog.findViewById(R.id.editTypeET);
        final EditText editClassET = (EditText) searchDialog.findViewById(R.id.editClassET);
        final EditText editPlaceBuiltET = (EditText) searchDialog.findViewById(R.id.editPlaceBuiltET);
        final EditText editIMONumET = (EditText) searchDialog.findViewById(R.id.editIMONumET);
//        final Spinner typeSpinner = (Spinner) searchDialog.findViewById(R.id.typeSpinner);
        final Spinner spinnerGeared = (Spinner) searchDialog.findViewById(R.id.spinnerGeared);
        final EditText editOffMarketET = searchDialog.findViewById(R.id.editOffMarketET);
        final EditText editGearedET = searchDialog.findViewById(R.id.editGearedET);

        List<String> list = new ArrayList<String>();

        list.add("Select Geared");
        list.add("Yes");
        list.add("No");
        ArrayAdapter<String> mSpAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        mSpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGeared.setAdapter(mSpAdapter);
        spinnerGeared.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strGeared = adapterView.getItemAtPosition(i).toString();
                if (strGeared.equals("Select Geared")) {
                    strGeared = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, vesselTypesArray);
//        // Drop down layout style - list view with radio button
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        // attaching data adapter to spinner
//        typeSpinner.setAdapter(dataAdapter);
//
//        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                strType = adapterView.getItemAtPosition(i).toString();
//                if(strType.equals("Type")){
//                    strType="";
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        editTypeET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Type");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<GetVesselTypeData> adapter = new ArrayAdapter<GetVesselTypeData>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, modelArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strType = modelArrayList.get(position).getName();
                            editTypeET.setText(strType);
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editClassET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
//        categoryDialog.setCanceledOnTouchOutside(true);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Class");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    // Define a new Adapter
                    // First parameter - Context
                    // Second parameter - Layout for the row
                    // Third parameter - ID of the TextView to which the data is written
                    // Forth - the Array of data

                    ArrayAdapter<GetVesselClassData> adapter = new ArrayAdapter<GetVesselClassData>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListClass);

                    // Assign adapter to ListView
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strClass = mArrayListClass.get(position).getClassName();
                            editClassET.setText(strClass);
                            categoryDialog.dismiss();

                        }
                    });


                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });


//        editStatusET.setText("Available");
        editStatusET.setKeyListener(null);
        editStatusET.setCursorVisible(false);
        final TextView txtHeaderStatus = (TextView) searchDialog.findViewById(R.id.txtHeaderStatus);
        final LinearLayout layoutNameLL = (LinearLayout) searchDialog.findViewById(R.id.layoutNameLL);
        final LinearLayout layoutPlaceBuiltLL = (LinearLayout) searchDialog.findViewById(R.id.layoutPlaceBuiltLL);
        final LinearLayout layoutRecordIDLL = (LinearLayout) searchDialog.findViewById(R.id.layoutRecordIDLL);
        final LinearLayout layoutStatusLL = (LinearLayout) searchDialog.findViewById(R.id.layoutStatusLL);

        layoutNameLL.setVisibility(View.GONE);
        layoutPlaceBuiltLL.setVisibility(View.VISIBLE);
        layoutRecordIDLL.setVisibility(View.VISIBLE);


        layoutStatusLL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        txtHeaderStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });
        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });


        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchDialog.dismiss();
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strCapacityFrom = editFromCapacityET.getText().toString();
                strCapacityTo = editToCapacityET.getText().toString();

                strName = editNameET.getText().toString();

                strLoaFrom = editFromLoaET.getText().toString();
                strLoaTo = editToLoaET.getText().toString();


                strGrainFrom = editFromGrainET.getText().toString();
                strGrainTo = editToGrainET.getText().toString();

                strYearofBuildFrom = editFromYearBuiltET.getText().toString();
                strYearofBuildTo = editToYearBuiltET.getText().toString();

                strBaleFrom = editFromBaleET.getText().toString();
                strBaleTo = editToBaleET.getText().toString();

                strStatus = editStatusET.getText().toString();

                strRecordID = editRecordIDET.getText().toString();
                IMO_number = editIMONumET.getText().toString();

                strTEUFrom = editFromTEUET.getText().toString();
                strTEUto = editToTEUET.getText().toString();
                strDraftsTo = editToDraftET.getText().toString();
                strDraftsFrom = editFromDraftrET.getText().toString();
                strPassangerFrom = editFromPassengerET.getText().toString();
                strPassangerTo = editToPassendgerET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();
                strLiquidFrom = editFromLiquidET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();
                strPlaceofBuilt = editPlaceBuiltET.getText().toString();
//                strGeared = spinnerGeared.getSelectedItem().toString();

                strGeared = editGearedET.getText().toString();
                strOffMarket = editOffMarketET.getText().toString();

                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchAPI(searchDialog);
                }
            }
        });
        searchDialog.show();
    }

    public void executeAdvanceSearchAPI(final Dialog mDialog) {
        mArrayList.clear();
        if (strType == null) {
            strType = "";
        }
        if (strClass == null) {
            strClass = "";
        }
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.advanceSearchVesselRequest(strCapacityFrom, strCapacityTo, strLoaFrom, strLoaTo,
                strType, strYearofBuildFrom, strYearofBuildTo, strGrainFrom, strGrainTo, strBaleFrom, strName, strRecordID,IMO_number, strBaleTo, strStatus,
                strGeared, "user", strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo,
                strLiquidFrom, strLiquidTo, strClass, strPlaceofBuilt, "0", strOffMarket);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                /*Paras Search Data*/
                isAdvanceSearch = true;
                SearchModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mArrayList.clear();
                    mDialog.dismiss();
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mArrayList = mModel.getAllSearchedVessels();
                    /*SetAdapter*/
                    setAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus().equals("0")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeAdvanceSearchAPI(final Dialog mDialog) {
//        mArrayList.clear();
//        if (strType == null) {
//            strType = "";
//        }
//        if (strClass == null) {
//            strClass = "";
//        }
//        String strUrl = JaoharConstants.ADVANCED_SEARCH + "?capacity_from=" + strCapacityFrom +
//                "&role=" + "user" +
//                "&capacity_to=" + strCapacityTo +
//                "&loa_from=" + strLoaFrom +
//                "&loa_to=" + strLoaTo +
//                "&vessel_type=" + strType +
//                "&year_of_built_from=" + strYearofBuildFrom +
//                "&year_of_built_to=" + strYearofBuildTo +
//                "&grain_from=" + strGrainFrom +
//                "&grain_to=" + strGrainTo +
//                "&bale_from=" + strBaleFrom +
//                "&name=" + strName +
//                "&record_id=" + strRecordID +
//                "&bale_to=" + strBaleTo +
//                "&status=" + strStatus +
//                "&geared=" + strGeared +
//                "&teu_from=" + strTEUFrom +
//                "&teu_to=" + strTEUto +
//                "&no_passengers_from=" + strPassangerFrom +
//                "&no_passengers_to=" + strPassangerTo +
//                "&draft_from=" + strDraftsFrom +
//                "&draft_to=" + strDraftsTo +
//                "&liquid_from=" + strLiquidFrom +
//                "&liquid_to=" + strLiquidTo +
//                "&class=" + strClass +
//                "&place_of_built=" + strPlaceofBuilt +
//                "&user_id=0";
//
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                resetRL1.setVisibility(View.VISIBLE);
//                Log.e(TAG, "*****Response****" + response);
//                /*Paras Search Data*/
//                isAdvanceSearch = true;
//                parseSearchResponse(response, mDialog);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseSearchResponse(String response, Dialog mDialog) {
//        try {
//            JSONObject mJsonObject11 = new JSONObject(response);
//
//            if (mJsonObject11.getString("status").equals("1")) {
//                mDialog.dismiss();
//                mArrayList.clear();
//                JSONArray mJsonArray = mJsonObject11.getJSONArray("all_searched_vessels");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                    VesselesModel mVesselesModel = new VesselesModel();
//                    if (!mJsonObject1.isNull("record_id")) {
//                        mVesselesModel.setRecord_ID(mJsonObject1.getString("record_id"));
//                    }
//                    if (!mJsonObject1.isNull("IMO_number")) {
//                        mVesselesModel.setIMO_Number(mJsonObject1.getString("IMO_number"));
//                    }
//                    if (!mJsonObject1.isNull("short_description")) {
//                        mVesselesModel.setShort_Discription(mJsonObject1.getString("short_description"));
//                    }
//                    if (!mJsonObject1.isNull("vessel_name")) {
//                        mVesselesModel.setVessel_Name(mJsonObject1.getString("vessel_name"));
//                    }
//                    if (!mJsonObject1.isNull("vessel_type")) {
//                        mVesselesModel.setVessel_Type(mJsonObject1.getString("vessel_type"));
//                    }
//                    if (!mJsonObject1.isNull("year_built")) {
//                        mVesselesModel.setYear_Built(mJsonObject1.getString("year_built"));
//                    }
//                    if (!mJsonObject1.isNull("place_of_built")) {
//                        mVesselesModel.setPlace_of_Built(mJsonObject1.getString("place_of_built"));
//                    }
//                    if (!mJsonObject1.isNull("class")) {
//                        mVesselesModel.setmClass(mJsonObject1.getString("class"));
//                    }
//                    if (!mJsonObject1.isNull("flag")) {
//                        mVesselesModel.setFlag(mJsonObject1.getString("flag"));
//                    }
//                    if (!mJsonObject1.isNull("capacity")) {
//                        mVesselesModel.setDWT_Capacity(mJsonObject1.getString("capacity"));
//                    }
//                    if (!mJsonObject1.isNull("loa")) {
//                        mVesselesModel.setLOA_M(mJsonObject1.getString("loa"));
//                    }
//
//                    if (!mJsonObject1.isNull("breadth")) {
//                        mVesselesModel.setBreadth_M(mJsonObject1.getString("breadth"));
//                    }
//                    if (!mJsonObject1.isNull("depth")) {
//                        mVesselesModel.setDepth(mJsonObject1.getString("depth"));
//                    }
//                    if (!mJsonObject1.isNull("price_idea")) {
//                        mVesselesModel.setPrice_Idea(mJsonObject1.getString("price_idea"));
//                    }
//                    if (!mJsonObject1.isNull("vessel_location")) {
//                        mVesselesModel.setVessel_Location(mJsonObject1.getString("vessel_location"));
//                    }
//                    if (!mJsonObject1.isNull("full_description")) {
//                        mVesselesModel.setFull_Discription(mJsonObject1.getString("full_description"));
//                    }
//                    if (!mJsonObject1.isNull("currency")) {
//                        mVesselesModel.setCurrency(mJsonObject1.getString("currency"));
//                    }
//                    if (!mJsonObject1.isNull("photo1")) {
//                        mVesselesModel.setPhoto1(mJsonObject1.getString("photo1"));
//                    }
//                    if (!mJsonObject1.isNull("photo2")) {
//                        mVesselesModel.setPhoto2(mJsonObject1.getString("photo2"));
//                    }
//                    if (!mJsonObject1.isNull("photo3")) {
//                        mVesselesModel.setPhoto3(mJsonObject1.getString("photo3"));
//                    }
//                    if (!mJsonObject1.isNull("photo4")) {
//                        mVesselesModel.setPhoto4(mJsonObject1.getString("photo4"));
//                    }
//                    if (!mJsonObject1.isNull("photo5")) {
//                        mVesselesModel.setPhoto5(mJsonObject1.getString("photo5"));
//                    }
//                    if (!mJsonObject1.isNull("photo6")) {
//                        mVesselesModel.setPhoto6(mJsonObject1.getString("photo6"));
//                    }
//                    if (!mJsonObject1.isNull("photo7")) {
//                        mVesselesModel.setPhoto7(mJsonObject1.getString("photo7"));
//                    }
//                    if (!mJsonObject1.isNull("photo8")) {
//                        mVesselesModel.setPhoto8(mJsonObject1.getString("photo8"));
//                    }
//                    if (!mJsonObject1.isNull("photo9")) {
//                        mVesselesModel.setPhoto9(mJsonObject1.getString("photo9"));
//                    }
//                    if (!mJsonObject1.isNull("photo10")) {
//                        mVesselesModel.setPhoto10(mJsonObject1.getString("photo10"));
//                    }
//                    if (!mJsonObject1.isNull("date_entered")) {
//                        mVesselesModel.setDate_Entered(mJsonObject1.getString("date_entered"));
//                    }
//                    if (!mJsonObject1.isNull("status")) {
//                        mVesselesModel.setStatus(mJsonObject1.getString("status"));
//                    }
//                    if (!mJsonObject1.isNull("bale")) {
//                        mVesselesModel.setBale(mJsonObject1.getString("bale"));
//                    }
//                    if (!mJsonObject1.isNull("grain")) {
//                        mVesselesModel.setGrain(mJsonObject1.getString("grain"));
//                    }
//                    if (!mJsonObject1.isNull("date_for_vessel")) {
//                        mVesselesModel.setDate_for_Vessel(mJsonObject1.getString("date_for_vessel"));
//                    }
//
//                    if (!mJsonObject1.isNull("vessel_added_by")) {
//                        mVesselesModel.setVessel_added_by(mJsonObject1.getString("vessel_added_by"));
//                    }
//                    if (!mJsonObject1.isNull("document1")) {
//                        mVesselesModel.setDocument1(mJsonObject1.getString("document1"));
//                    }
//                    if (!mJsonObject1.isNull("document2")) {
//                        mVesselesModel.setDocument2(mJsonObject1.getString("document2"));
//                    }
//                    if (!mJsonObject1.isNull("document3")) {
//                        mVesselesModel.setDocument3(mJsonObject1.getString("document3"));
//                    }
//                    if (!mJsonObject1.isNull("document4")) {
//                        mVesselesModel.setDocument4(mJsonObject1.getString("document4"));
//                    }
//                    if (!mJsonObject1.isNull("document5")) {
//                        mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                    }
//                    if (!mJsonObject1.isNull("document6")) {
//                        mVesselesModel.setDocument6(mJsonObject1.getString("document6"));
//                    }
//                    if (!mJsonObject1.isNull("document7")) {
//                        mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                    }
//                    if (!mJsonObject1.isNull("document7")) {
//                        mVesselesModel.setDocument7(mJsonObject1.getString("document7"));
//                    }
//                    if (!mJsonObject1.isNull("document8")) {
//                        mVesselesModel.setDocument8(mJsonObject1.getString("document8"));
//                    }
//                    if (!mJsonObject1.isNull("document9")) {
//                        mVesselesModel.setDocument9(mJsonObject1.getString("document9"));
//                    }
//                    if (!mJsonObject1.isNull("document10")) {
//                        mVesselesModel.setDocument10(mJsonObject1.getString("document10"));
//                    }
//                    if (!mJsonObject1.isNull("vessel_add_time")) {
//                        mVesselesModel.setVessel_add_time(mJsonObject1.getString("vessel_add_time"));
//                    }
//                    if (!mJsonObject1.isNull("document1name")) {
//                        mVesselesModel.setDocument1name(mJsonObject1.getString("document1name"));
//                    }
//                    if (!mJsonObject1.isNull("document2name")) {
//                        mVesselesModel.setDocument2name(mJsonObject1.getString("document2name"));
//                    }
//                    if (!mJsonObject1.isNull("document3name")) {
//                        mVesselesModel.setDocument3name(mJsonObject1.getString("document3name"));
//                    }
//                    if (!mJsonObject1.isNull("document4name")) {
//                        mVesselesModel.setDocument4name(mJsonObject1.getString("document4name"));
//                    }
//                    if (!mJsonObject1.isNull("document5name")) {
//                        mVesselesModel.setDocument5name(mJsonObject1.getString("document5name"));
//                    }
//                    if (!mJsonObject1.isNull("document6name")) {
//                        mVesselesModel.setDocument6name(mJsonObject1.getString("document6name"));
//                    }
//                    if (!mJsonObject1.isNull("document7name")) {
//                        mVesselesModel.setDocument7name(mJsonObject1.getString("document7name"));
//                    }
//                    if (!mJsonObject1.isNull("document8name")) {
//                        mVesselesModel.setDocument8name(mJsonObject1.getString("document8name"));
//                    }
//                    if (!mJsonObject1.isNull("document9name")) {
//                        mVesselesModel.setDocument9name(mJsonObject1.getString("document9name"));
//                    }
//                    if (!mJsonObject1.isNull("document10name")) {
//                        mVesselesModel.setDocument10name(mJsonObject1.getString("document10name"));
//                    }
//                    if (!mJsonObject1.isNull("mail_data")) {
//                        mVesselesModel.setMail_data(mJsonObject1.getString("mail_data"));
//                    }
//                    if (!mJsonObject1.isNull("builder")) {
//                        mVesselesModel.setBuilder(mJsonObject1.getString("builder"));
//                    }
//                    if (!mJsonObject1.isNull("draught")) {
//                        mVesselesModel.setDraught(mJsonObject1.getString("draught"));
//                    }
//                    if (!mJsonObject1.isNull("gross_tonnage")) {
//                        mVesselesModel.setGross_tonnage(mJsonObject1.getString("gross_tonnage"));
//                    }
//                    if (!mJsonObject1.isNull("net_tonnage")) {
//                        mVesselesModel.setNet_tonnage(mJsonObject1.getString("net_tonnage"));
//                    }
//                    if (!mJsonObject1.isNull("teu")) {
//                        mVesselesModel.setTeu(mJsonObject1.getString("teu"));
//                    }
//                    if (!mJsonObject1.isNull("liquid")) {
//                        mVesselesModel.setLiquid(mJsonObject1.getString("liquid"));
//                    }
//                    if (!mJsonObject1.isNull("url")) {
//                        mVesselesModel.setUrl(mJsonObject1.getString("url"));
//                    }
//                    if (!mJsonObject1.isNull("gas")) {
//                        mVesselesModel.setGas(mJsonObject1.getString("gas"));
//                    }
//                    if (!mJsonObject1.isNull("no_passengers")) {
//                        mVesselesModel.setNo_passengers(mJsonObject1.getString("no_passengers"));
//                    }
//                    //mVesselesModel.setImage(imagesArray[i]);
////                    mArrayList.add(mVesselesModel);
//                }
//
//                /*SetAdapter*/
//                setAdapter();
//                AlertDialogManager.hideProgressDialog();
//            } else if (mJsonObject11.getString("status").equals("100")) {
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject11.getString("message"));
//            } else if (mJsonObject11.getString("status").equals("0")) {
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject11.getString("message"));
//            } else {
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject11.getString("message"));
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void showSelectItemFromArray(Activity mActivity, String strTitle, final String strStringArray[], final EditText mEditText) {
        final Dialog categoryDialog = new Dialog(mActivity);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.item_list_categories);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
        txtTitle.setText(strTitle);
        ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_list_item_1, android.R.id.text1, strStringArray);
        // Assign adapter to ListView
        lstListView.setAdapter(adapter);

        lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mEditText.setText(strStringArray[position]);
                categoryDialog.dismiss();

            }
        });


        categoryDialog.show();
    }

    public void gettingProfileDATA(String strLoginID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ProfileModel> call1 = mApiInterface.getProfileDetailsRequest(strLoginID);
        call1.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, retrofit2.Response<ProfileModel> response) {
                AlertDialogManager.hideProgressDialog();
                ProfileModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    profileDataArrayList.add(mModel.getData());
                    for (int i=0;i<=profileDataArrayList.size();i++){
                        if (!profileDataArrayList.get(i).getImage().equals("")){
                        Glide.with(mActivity).load(profileDataArrayList.get(i)).into(imgRight);
                        Glide.with(mActivity).load(profileDataArrayList.get(i)).into(HomeActivity.profilePICIMG);
                    } else {
                            imgRight.setImageResource(R.drawable.profile);
                            HomeActivity.profilePICIMG.setImageResource(R.drawable.profile);

                        }
                        if (!profileDataArrayList.get(i).getFirstName().equals("")) {
                            HomeActivity.nameTV.setText(profileDataArrayList.get(i).getFirstName() + " " + profileDataArrayList.get(i).getLastName());
                            JaoharPreference.writeString(mActivity, JaoharPreference.first_name,profileDataArrayList.get(i).getFirstName());
                        }
                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    private void gettingProfileDATA(String strLoginID) {
//        String strUrl = JaoharConstants.GetUserProfile_API + "?user_id=" + strLoginID;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    if (strStatus.equals("1")) {
//                        if (!mJsonObject.isNull("data")) {
//                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
//                            if (!mJsonDATA.getString("image").equals("")) {
//                                String strImage = mJsonDATA.getString("image");
//                                Glide.with(mActivity).load(strImage).into(imgRight);
//                                Glide.with(mActivity).load(strImage).into(HomeActivity.profilePICIMG);
//
//                            } else {
//                                imgRight.setImageResource(R.drawable.profile);
//                                HomeActivity.profilePICIMG.setImageResource(R.drawable.profile);
//
//                            }
//                        }
//                        if (!mJsonObject.getString("first_name").equals("")) {
//                            HomeActivity.nameTV.setText(mJsonObject.getString("first_name") + " " + mJsonObject.getString("last_name"));
//                            JaoharPreference.writeString(mActivity, JaoharPreference.first_name, mJsonObject.getString("first_name"));
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /* *
     * Execute Favorite API for vessel
     * @param
     * @user_id
     * */
    public void executeFavoriteAPI(String vessel_id, final ImageView imageView) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.makeFavoriteVesselsRequest(vessel_id,JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_yellow);
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_fav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    public void executeFavoriteAPI(String vessel_id, final ImageView imageView) {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strUrl = JaoharConstants.MakeVesselFavorite + "?vessel_id=" + vessel_id + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***URLResponce***" + response);
//
//                try {
//                    JSONObject mJsonObject1 = new JSONObject(response);
//
//                    if (mJsonObject1.getString("status").equals("1")) {
//
//                        imageView.setImageResource(R.drawable.ic_bookmark_yellow);
//
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_fav));
//
//                    } else if (mJsonObject1.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /* *
     * Execute UnFavorite API for vessel
     * @param
     * @user_id
     * */
    public void executeUnFavoriteAPI(String vessel_id, final ImageView imageView) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.unFavoriteVesselsRequest(vessel_id, JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_border);
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
                }
                else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }



//    public void executeUnFavoriteAPI(String vessel_id, final ImageView imageView) {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strUrl = JaoharConstants.UnFavoriteVessel + "?vessel_id=" + vessel_id + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***URLResponce***" + response);
//
//                try {
//                    JSONObject mJsonObject1 = new JSONObject(response);
//
//                    if (mJsonObject1.getString("status").equals("1")) {
//
//                        imageView.setImageResource(R.drawable.ic_bookmark_border);
//
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
//
//                    } else if (mJsonObject1.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

}
