package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.invoices_module.SearchInvoiceCurrencyActivity;
import jaohar.com.jaohar.adapters.CurrenciesAdapter;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.interfaces.DeleteCurrencyInterface;
import jaohar.com.jaohar.interfaces.EditCurrencyInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllCurrencyActivity extends BaseActivity {
    Activity mActivity = AllCurrencyActivity.this;
    String TAG = AllCurrencyActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    ImageView imgBack;
    TextView txtCenter;
    EditText editSearchET;
    ImageView imgSearch, imgAdd;

    //RecyclerView
    RecyclerView rvCurrencyRV;
    //    ArrayList<CurrenciesModel> mArrayList = new ArrayList<CurrenciesModel>();
    ArrayList<CurrenciesModel> mArrayList = new ArrayList<CurrenciesModel>();
    ArrayList<CurrenciesModel> filteredList = new ArrayList<CurrenciesModel>();
    //    ArrayList<CurrenciesModel> filteredList = new ArrayList<CurrenciesModel>();
    CurrenciesAdapter mCurrenciesAdapter;

    EditCurrencyInterface mEditCurrencyInterface = new EditCurrencyInterface() {
        @Override
        public void editCurrency(CurrenciesModel mCurrenciesModel) {
            Intent mIntent = new Intent(mActivity, EditCurrencyActivity.class);
            mIntent.putExtra("Model", String.valueOf(mCurrenciesModel));
            mActivity.startActivity(mIntent);
        }
    };

    DeleteCurrencyInterface mDeleteCurrencyInterface = new DeleteCurrencyInterface() {
        @Override
        public void deleteCurrency(CurrenciesModel mModel, int position) {
            deleteConfirmDialog(mModel, position);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_currency);

        setStatusBar();

        setViewsIDs();
        setClickListner();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeAPI();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (JaoharConstants.IS_CURRENCY_EDIT)
            executeAPI();
    }

    protected void setViewsIDs() {
        editSearchET = findViewById(R.id.editSearchET);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.Currencies));
        llLeftLL = findViewById(R.id.llLeftLL);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        rvCurrencyRV = findViewById(R.id.rvCurrencyRV);
        imgAdd = findViewById(R.id.imgAdd);
        imgSearch = findViewById(R.id.imgSearch);
    }

    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, AddCurrencyActivity.class);
                startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, SearchInvoiceCurrencyActivity.class);
                intent.putExtra("QuestionListExtra", mArrayList);
                startActivityForResult(intent, 777);
                overridePendingTransitionEnter();
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                charSequence = charSequence.toString().toLowerCase();
                filteredList.clear();
                for (int k = 0; k < mArrayList.size(); k++) {
                    final String text = mArrayList.get(k).getAlias_name().toLowerCase();
                    if (text.contains(charSequence)) {
                        filteredList.add(mArrayList.get(k));
                    }
                }
                rvCurrencyRV.setLayoutManager(new LinearLayoutManager(mActivity));
                mCurrenciesAdapter = new CurrenciesAdapter(mActivity, filteredList, mDeleteCurrencyInterface, mEditCurrencyInterface);
                rvCurrencyRV.setAdapter(mCurrenciesAdapter);
                mCurrenciesAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        Log.e(TAG, "Companies: " + mArrayList.size());
        rvCurrencyRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mCurrenciesAdapter = new CurrenciesAdapter(mActivity, mArrayList, mDeleteCurrencyInterface, mEditCurrencyInterface);
        rvCurrencyRV.setAdapter(mCurrenciesAdapter);
    }

    public void executeAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllCurrenciesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                JaoharConstants.IS_CURRENCY_EDIT = false;
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeAPI() {
//        String strUrl = JaoharConstants.GET_ALL_CURRENCIES + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        //AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                JaoharConstants.IS_CURRENCY_EDIT = false;
//                //AlertDialogManager.hideProgressDialog();
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    private void parseResponse(String response) {
        mArrayList.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CurrenciesModel mModel = new CurrenciesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("currency_name")) {
                        mModel.setCurrency_name(mJson.getString("currency_name"));
                    }
                    if (!mJson.isNull("alias_name")) {
                        mModel.setAlias_name(mJson.getString("alias_name"));
                    }
                    mArrayList.add(mModel);
                }

                /*SetAdapter*/
                setAdapter();

            } else if (mJsonObject.getString("status").equals("100")) {
                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteConfirmDialog(final CurrenciesModel mCurrenciesModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_currency));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mCurrenciesModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /*Execute DeleteUser API*/
    private void executeDeleteAPI(CurrenciesModel mCurrenciesModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteCurrencyRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), mCurrenciesModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {

                    mArrayList.remove(position);

                    if (mCurrenciesAdapter != null)
                        mCurrenciesAdapter.notifyDataSetChanged();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeDeleteAPI(CurrenciesModel mCurrenciesModel) {
//        String strUrl = JaoharConstants.DELETE_CURRENCY + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&id=" + mCurrenciesModel.getId();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        executeAPI();
//                    } else if (mJsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the Activity with an OK result
        if (requestCode == 777) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", data.getSerializableExtra("Model"));
                mActivity.setResult(777, returnIntent);
                mActivity.finish();
            }
        }
    }
}
