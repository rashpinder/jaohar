package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.GalleryPagerAdapter;
import jaohar.com.jaohar.interfaces.chat_module.DownloadImageInterface;

public class GalleryActivity extends BaseActivity {
    Activity mActivity = GalleryActivity.this;

    String TAG = GalleryActivity.this.getClass().getSimpleName();

    ViewPager mViewPager;
    ImageView imgRightArrow, imgLeftArrow;
    LinearLayout btnPre, btnNext, closeGalleryLL, downloadGalleryLL;
    ArrayList<String> mImagesArrayList = new ArrayList<String>();
    GalleryPagerAdapter mAdapter;
    boolean iscLickFrmInternal = false;
    String path = "";
    String img = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        downLoadImageCode();
    }

    private void downLoadImageCode() {
        PRDownloader.initialize(getApplicationContext());
        // Enabling database for resume support even after the application is killed:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

        PRDownloaderConfig configs = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), configs);
        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + "/JAOHAR/";
        Log.e(TAG, "pathname:" + path);
        File dir = new File(path);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setViewsIDs() {
        mViewPager = findViewById(R.id.mViewPager);
        imgRightArrow = findViewById(R.id.imgRightArrow);
        imgLeftArrow = findViewById(R.id.imgLeftArrow);
        closeGalleryLL = findViewById(R.id.closeGalleryLL);
        btnPre = findViewById(R.id.btnPre);
        btnNext = findViewById(R.id.btnNext);
        imgRightArrow = findViewById(R.id.imgRightArrow);
        imgLeftArrow = findViewById(R.id.imgLeftArrow);
        downloadGalleryLL = findViewById(R.id.downloadGalleryLL);

        Intent i = getIntent();
        if (i != null) {
            mImagesArrayList.clear();
            if (i.getStringArrayListExtra("LIST") != null) {
                mImagesArrayList = (ArrayList<String>) i.getStringArrayListExtra("LIST");

                if (mImagesArrayList.size() > 1) {
                    imgLeftArrow.setVisibility(View.VISIBLE);
                    imgRightArrow.setVisibility(View.VISIBLE);
                } else {
                    imgLeftArrow.setVisibility(View.GONE);
                    imgRightArrow.setVisibility(View.GONE);
                }

                mAdapter = new GalleryPagerAdapter(mActivity, mImagesArrayList, downloadImageInterface);
                mViewPager.setAdapter(mAdapter);

            } else if (i.getStringArrayListExtra("LIST1") != null) {
                mImagesArrayList.clear();
                mImagesArrayList = (ArrayList<String>) i.getStringArrayListExtra("LIST1");
                iscLickFrmInternal = true;

                if (mImagesArrayList.size() > 1) {
                    imgLeftArrow.setVisibility(View.VISIBLE);
                    imgRightArrow.setVisibility(View.VISIBLE);
                } else {
                    imgLeftArrow.setVisibility(View.GONE);
                    imgRightArrow.setVisibility(View.GONE);
                }

                mAdapter = new GalleryPagerAdapter(mActivity, mImagesArrayList, downloadImageInterface);
                mViewPager.setAdapter(mAdapter);
            }
        }
    }

    DownloadImageInterface downloadImageInterface = new DownloadImageInterface() {
        @Override
        public void mDownloadImageInterface(int position, String image, String type) {
            if (type.equals("1")) {
                downloadGalleryLL.setVisibility(View.GONE);
                img = image;
            } else {
                downloadGalleryLL.setVisibility(View.GONE);
            }
        }
    };

    @Override
    protected void setClickListner() {
        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(getItem(-1), true);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(getItem(+1), true);
            }
        });

        closeGalleryLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        downloadGalleryLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (img != null && !img.equals("")) {
                    downloadImageMethod();
                }
            }
        });
    }

    private void downloadImageMethod() {
        showProgressDialog(mActivity);
        DownloadRequest prDownloader = PRDownloader.download(img, path, getMD5EncryptedString(img) + ".jpg")
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                });

        prDownloader.start(new OnDownloadListener() {
            @Override
            public void onDownloadComplete() {
                hideProgressDialog();
                Toast.makeText(mActivity, "Saved Successfully.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Error error) {
                hideProgressDialog();
                Log.e(TAG, "ERROR" + error.toString());
                Toast.makeText(mActivity, "error" + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getMD5EncryptedString(String encTarget) {
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while (md5.length() < 32) {
            md5 = "0" + md5;
        }
        return md5;
    }

    @Override
    public void onBackPressed() {
        if (iscLickFrmInternal == true) {
            Intent mIntent = new Intent(mActivity, AllInternalNewsActivity.class);
            startActivity(mIntent);
            super.onBackPressed();
            finish();
            overridePendingTransitionExit();
        } else {
            super.onBackPressed();
            finish();
            overridePendingTransitionExit();
        }
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

}
