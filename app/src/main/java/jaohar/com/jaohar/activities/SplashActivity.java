package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class SplashActivity extends BaseActivity {
    Activity mActivity = SplashActivity.this;
    String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUpSplash();
    }

    private void setUpSplash() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                i.putExtra(JaoharConstants.LOGIN, "Splash");
                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                    i.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                }
                startActivity(i);
                overridePendingTransitionEnter();
                // close this activity
                finish();
            }
        }, JaoharConstants.SPLASH_TIME_OUT);
    }

    @Override
    protected void setViewsIDs() {

    }

    @Override
    protected void setClickListner() {

    }
}
