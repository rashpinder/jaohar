package jaohar.com.jaohar.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class EditStampActivity extends AppCompatActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private final String cameraStr = Manifest.permission.CAMERA;
    private final String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;
    Activity mActivity = EditStampActivity.this;
    String TAG = EditStampActivity.this.getClass().getSimpleName();

    //Toolbar
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText editStampNameET;
    ImageView imgStampImageIV;
    Button btnEditStampB;

    LinearLayout layoutTakeImageLL;

    String mCurrentPhotoPath, mStoragePath = "";
    String strImageBase64 = "";


    StampsModel mStampsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_stamp);

        if (getIntent() != null) {
            mStampsModel = (StampsModel) getIntent().getSerializableExtra("Model");
        }

        setViewsIDs();
        setClickListner();
    }


    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.Edit_stamps));

        editStampNameET = (EditText) findViewById(R.id.editStampNameET);
        imgStampImageIV = (ImageView) findViewById(R.id.imgStampImageIV);
        layoutTakeImageLL = (LinearLayout) findViewById(R.id.layoutTakeImageLL);
        btnEditStampB = (Button) findViewById(R.id.btnEditStampB);

        /*Set Data on Widgets*/
        if (mStampsModel != null) {
            setDataOnWidgets(mStampsModel);
        }

    }

    private void setDataOnWidgets(StampsModel mStampsModel) {
        editStampNameET.setText(mStampsModel.getStamp_name());
        editStampNameET.setSelection(editStampNameET.getText().length());
        Log.e(TAG, "****StampUrl***" + mStampsModel.getStamp_image());
        String strSTamp = mStampsModel.getStamp_image();
        Glide.with(mActivity)
                .load(mStampsModel.getStamp_image())
                .into(imgStampImageIV);
    }

    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        layoutTakeImageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpCameraGalleryDialog();
            }
        });

        btnEditStampB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editStampNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_stamp_name));
                } else if (mStampsModel.getStamp_image().equals("") && strImageBase64.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_stamp_image));
                } else {
                    /*Execute Add Comapny API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeEditStampAPI(mStampsModel);
                    }

                }
            }
        });
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }


    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }


    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            openCameraGalleryDialog();
//                            startCropImageActivity(mCropImageUri);
//                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
//                            finish();
//                            Toast.makeText(context,"on",Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();
//                            permissionAccepted = false;
                        }
                    }
                }
                break;
        }
    }

    public void setUpCameraGalleryDialog() {
        if (checkPermission()) {
            openCameraGalleryDialog();
        } else {
            requestPermission();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            Bitmap bitmap;
                            Uri uri = data.getData();
                            File finalFile = new File(getRealPathFromURI(uri));

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 0;

                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                            ExifInterface exifInterface = null;
                            try {
                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            Matrix matrix = new Matrix();
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                                case ExifInterface.ORIENTATION_NORMAL:
                                default:

                            }
                            JaoharConstants.IS_camera_Click = false;
                            Bitmap thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                            imgStampImageIV.setImageBitmap(thumb);
                            strImageBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
//                            Uri uri = data.getData();
//                            File finalFile = new File(getRealPathFromURI(uri));
//                            Bitmap thumb = ImageUtils.getInstant().rotateBitmapOrientation(finalFile.getPath());//getCompressedBitmap(mStoragePath);
//                            imgStampImageIV.setImageBitmap(thumb);
//                            strImageBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected****");
                        return;
                    }

                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {

                    JaoharConstants.IS_camera_Click = true;
                    Bitmap thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(mStoragePath, options);
                    Bitmap thumb1 = rotateImage(bitmap);
                    imgStampImageIV.setImageBitmap(thumb1);
                    strImageBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 505) {
                Bitmap bitmap;
                String strDocumentPath, strDocumentName;
                Uri uri = data.getData();
                strDocumentPath = uri.getPath();
                strDocumentPath = strDocumentPath.replace(" ", "_");
                strDocumentName = uri.getLastPathSegment();

                File finalFile = new File(getRealPathFromURI(uri));

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 0;

                bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                ExifInterface exifInterface = null;
                try {
                    exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.setRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.setRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.setRotate(270);
                        break;
                    case ExifInterface.ORIENTATION_NORMAL:
                    default:

                }
                JaoharConstants.IS_camera_Click = false;
                Bitmap thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                imgStampImageIV.setImageBitmap(thumb);
                strImageBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
            }
        }

    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("stamp_id", mStampsModel.getId());
        mMap.put("stamp_name", editStampNameET.getText().toString());
        if (strImageBase64.length() > 10) {
//                jsonObject.put("stamp_image", "\"" + strImageBase64 + "\"");
            mMap.put("stamp_image", strImageBase64);
        } else {
            mMap.put("stamp_image", mStampsModel.getStamp_image());
        }

        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));

        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeEditStampAPI(StampsModel mStampsModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editSignRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    JaoharConstants.IS_STAMP_EDIT = true;
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }


//    private void executeEditStampAPI(StampsModel mStampsModel) {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.EDIT_STAMP;
//        try {
//            jsonObject.put("stamp_id", mStampsModel.getId());
//            jsonObject.put("stamp_name", editStampNameET.getText().toString());
//            if (strImageBase64.length() > 10){
////                jsonObject.put("stamp_image", "\"" + strImageBase64 + "\"");
//                jsonObject.put("stamp_image",  strImageBase64 );
//            }
//
//            else{
//                jsonObject.put("stamp_image", mStampsModel.getStamp_image());
//            }
//
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    if (jsonObject.getString("status").equals("1")) {
//                        JaoharConstants.IS_STAMP_EDIT = true;
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), error.toString());
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }

    //       Method Correct Rotate Image when Capture From Camera....
    private Bitmap rotateImage(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }
}


