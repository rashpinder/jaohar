package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;

public class SearchActivity extends BaseActivity {
    //Toolbar
    public LinearLayout  llLeftLL;
    public RelativeLayout imgRightLL;
    public TextView txtCenter,txtAdvancedSearhTV;
    public ImageView imgRight, imgBack;
    Activity mActivity = SearchActivity.this;
    String TAG = SearchActivity.this.getClass().getSimpleName();
    Button btn_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
    }

    @Override
    protected void setViewsIDs() {
        /*Toolbar*/
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.GONE);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        btn_search = (Button) findViewById(R.id.btn_search);
        txtCenter.setText(getString(R.string.msearch));
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);




    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
