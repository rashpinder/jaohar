package jaohar.com.jaohar.activities;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;

import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.mail_list_all_activities.SendingMailToLIst_Vessels;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.activities.vessels_module.CopyVesselActivity;
import jaohar.com.jaohar.adapters.UserNewsAdapter;
import jaohar.com.jaohar.adapters.VesselsAdapter;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.MailSelectedVesselsInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.interfaces.SendingMailToVesselInterface;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.VessalClassModel;
import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.interfaces.DeleteVesselsInterface;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.SendEmailInterface;
import jaohar.com.jaohar.interfaces.SendMultiVesselDataInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.models.GetVesselClassData;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.SearchModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.VesselForSaleModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

import static jaohar.com.jaohar.adapters.VesselsAdapter.Items;

public class VesselsShipsActivity extends BaseActivity {
    public String strCapacityFrom, strCapacityTo, strName, strLoaFrom, strLoaTo, strType, strGrainFrom, strGrainTo, strClass, strYearofBuildFrom, strYearofBuildTo,
            strBaleFrom, strBaleTo, strPlaceBuild, strRecordID, IMO_number,strGeared, strStatus, strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom,
            strDraftsTo, strLiquidFrom, strLiquidTo, strTextNEWS = "", strOffMarket;
    public String strLastPage = "FALSE", strPhotoURL = "", strNormalTEXT;

    Activity mActivity = VesselsShipsActivity.this;
    String TAG = VesselsShipsActivity.this.getClass().getSimpleName();

    private LinearLayout llLeftLL, mailSelectedLL;
    private RelativeLayout imgRightLL, relativeLL, downArrowRL, resetRL1;
    private ImageView imgBack, imgRight, addImg, searchImg;
    private TextView txtCenter, allnewsTV, normalTextTV;
    private WebView scrollTextTV;
    private EditText editSearchET;
    private TextView txtMailTV, mailSelectedTV;
    private RecyclerView vesselsRV;
    private SwipeRefreshLayout swipeToRefresh;
    private VesselsAdapter mVesselsAdapter;
//    private ArrayList<VesselesModel> mArrayList = new ArrayList<>();
    private ArrayList<AllVessel> mArrayList = new ArrayList<>();
    private ArrayList<AllVessel> loadMoreArrayList = new ArrayList<>();
//    private ArrayList<VesselesModel> loadMoreArrayList = new ArrayList<>();
    private int page_no = 1;
    private NestedScrollView parentSV;

    private boolean isSwipeRefresh = false;
    private boolean isAdvanceSearch = false;
    private boolean isNormalSearch = false;
    static int IsVessalActive = 0;

    private String arrayStatus[];
//    private ArrayList<String> modelArrayList = new ArrayList<>();
    private ArrayList<GetVesselTypeData> modelArrayList = new ArrayList<>();
    private ArrayList<GetVesselClassData> mArrayListClass = new ArrayList<>();
    private VessalClassModel vessalClassModel = new VessalClassModel();
    private VesselTypesModel vesselTypesModel = new VesselTypesModel();
    ArrayList<String> mRecordID = new ArrayList<>();
    ProgressBar progressBottomPB;
    ArrayList<String> mImageArryList = new ArrayList<>();
    String UserId = "";

    RecyclerView newsRV;
    UserNewsAdapter mNewsAdapter;
    boolean isOpenDialogBOX = false;
    String strSearchText;

    //for pagination
    boolean isScrolling = false;
    LinearLayoutManager linearLayoutManager;
    NestedScrollView nestedScrollView;

//    private ArrayList<VesselesModel> multiSelectArrayList = new ArrayList<VesselesModel>();
    private ArrayList<AllVessel> multiSelectArrayList = new ArrayList<AllVessel>();

    ArrayList<Datum> mNewsArrayList = new ArrayList<Datum>();
    OpenNewsPopUpInterFace mOpenNewsPopUP = new OpenNewsPopUpInterFace() {
        @Override
        public void openNewsPopUpInterFace(String strWebTxt, String strPhotoURL) {
            if (isOpenDialogBOX == true) {
                isOpenDialogBOX = false;
            } else {
                setUpNewsDialog(strWebTxt, strPhotoURL);
            }
        }
    };

    FavoriteVesselInterface mFavoriteVesselInterface = new FavoriteVesselInterface() {
        @Override
        public void mFavoriteVesselInterface(int position, String status, ImageView imageView) {
            String vessel_id = mArrayList.get(position).getRecordId();
            if (status.equals("1")) {
                executeFavoriteAPI(vessel_id, imageView, position);
            } else if (status.equals("0")) {
                executeUnFavoriteAPI(vessel_id, imageView, position);
            }
        }
    };

    DeleteVesselsInterface mDeleteVesselsInterface = new DeleteVesselsInterface() {
        @Override
        public void mDeleteVessel(String mVesselID) {
            deleteConfirmDialog(mVesselID);
        }
    };
    SendingMailToVesselInterface mVesselToMail = new SendingMailToVesselInterface() {
        @Override
        public void mSendingMailVessel(AllVessel mModel) {
            /*Send Email Using Mail to List*/
//            stopService(new Intent(mActivity, MailingListServices.class));

            JaoharConstants.IsAllVesselsClick = true;
            Intent mIntent = new Intent(mActivity, SendingMailToLIst_Vessels.class);
            mIntent.putExtra("vessalName", "ID " + mModel.getRecordId() + "  " + "-" + "  " + mModel.getVesselName() + ", IMO No. " + mModel.getIMONumber());
            mIntent.putExtra("recordID", mModel.getRecordId());
            startActivity(mIntent);
        }
    };

    SendEmailInterface mSendEmailInterface = new SendEmailInterface() {
        @Override
        public void sendEmail(AllVessel mVesselesModel) {
//           str = String.valueOf(HtmlCompat.fromHtml(mActivity, mVesselesModel.getMail_data().toString(), 0, new URLImageParser(scrollTextTV, mActivity)));
            /*Send Email*/
            Intent mIntent = new Intent(mActivity, SendingMailActivity.class);
            mIntent.putExtra("vessalName", "ID " + mVesselesModel.getRecordId() + "  " + "-" + "  " + mVesselesModel.getVesselName() + ", IMO No. " + mVesselesModel.getIMONumber());
            mIntent.putExtra("recordID", mVesselesModel.getRecordId());
            startActivity(mIntent);
//          setMailListDialog("ID "+mVesselesModel.getRecordId()+"  " +"-"+"  " +mVesselesModel.getVessel_Name()+", IMO No. "+mVesselesModel.getIMO_Number(),mVesselesModel.getRecordId(),true);
        }
    };
    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                if (isAdvanceSearch == false) {
                    if (isNormalSearch == false) {

//                            if (Is_NEXT_Scroll) {
                        progressBottomPB.setVisibility(View.VISIBLE);

                        ++page_no;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (strLastPage.equals("FALSE")) {
                                    executeAPI(page_no);
                                } else {
                                    progressBottomPB.setVisibility(View.GONE);
                                }
                            }
                        }, 1000);
                    }
                }
            }
        }
    };

    SendMultiVesselDataInterface mSendMultiVesselDataInterface = new SendMultiVesselDataInterface() {
        @Override
        public void sendMultipleVesselsData(AllVessel mVesselesModel, boolean mDelete) {
            if (mVesselesModel != null) {
                if (!mDelete) {
                    IsVessalActive = IsVessalActive + 1;
                    multiSelectArrayList.add(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.add(mVesselesModel.getRecordId());
                } else {
                    IsVessalActive = IsVessalActive - 1;
                    multiSelectArrayList.remove(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.remove(mVesselesModel.getRecordId());
                }
                if (IsVessalActive > 0) {
                    txtMailTV.setVisibility(View.VISIBLE);
                } else {
                    txtMailTV.setVisibility(View.GONE);
                }
            }
        }
    };

    OpenPopUpVesselInterface mOpenPoUpInterface = new OpenPopUpVesselInterface() {
        @Override
        public void mOpenPopUpVesselInterface(String strTextnews, String strPhotoURLs) {
            JaoharConstants.IS_VESSAL_ACTIVITY = true;
            setUpNewsDialog(strTextnews, strPhotoURLs);
        }
    };

    MailSelectedVesselsInterface mMailSelectedVesselsInterface = new MailSelectedVesselsInterface() {
        @Override
        public void MailSelectedVessels(final int position, String Vessel_id, ArrayList<String> Items,
                                        AllVessel mVesselesModel, boolean b) {

            if (mVesselesModel != null) {
                if (!b) {
                    IsVessalActive = IsVessalActive + 1;
                    multiSelectArrayList.add(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.add(mVesselesModel.getRecordId());
                } else {
                    IsVessalActive = IsVessalActive - 1;
                    multiSelectArrayList.remove(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.remove(mVesselesModel.getRecordId());
                }
            }

            if (Items != null && Items.size() > 0) {
                mailSelectedLL.setVisibility(View.VISIBLE);

                mailSelectedTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JaoharConstants.IsAllVesselsClick = true;
                        Intent mIntent = new Intent(mActivity, SendingMailToLIst_Vessels.class);
                        mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                        mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                        startActivity(mIntent);
                    }
                });

            } else {
                mailSelectedLL.setVisibility(View.GONE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_vessels_ships);

        arrayStatus = VesselsShipsActivity.this.getResources().getStringArray(R.array.status_array_user);
        setViewData();

        if (Items != null) {
            Items.clear();
        }

        JaoharSingleton.getInstance().setSearchedVessel("");
        JaoharSingleton.getInstance().setAdvancedSearchedVesselsModel(null);
        JaoharSingleton.getInstance().setSearchedVesselsModel(null);

    }

    protected void setViewData() {
        nestedScrollView = findViewById(R.id.nestedScrollView);
        mailSelectedTV = findViewById(R.id.mailSelectedTV);
        mailSelectedLL = findViewById(R.id.mailSelectedLL);
        progressBottomPB = findViewById(R.id.progressBottomPB);
        editSearchET = (EditText) findViewById(R.id.editSearchET);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        scrollTextTV = (WebView) findViewById(R.id.scrollTextTV);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        downArrowRL = (RelativeLayout) findViewById(R.id.downArrowRL);
        resetRL1 = (RelativeLayout) findViewById(R.id.resetRL1);
        imgRightLL.setVisibility(View.VISIBLE);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        vesselsRV = (RecyclerView) findViewById(R.id.vesselsRV);
        newsRV = findViewById(R.id.newsRV);
        addImg = findViewById(R.id.addImg);
        searchImg = findViewById(R.id.searchImg);
        txtMailTV = (TextView) findViewById(R.id.txtMailTV);

        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isAdvanceSearch = false;
                isNormalSearch = false;
                mArrayList.clear();
                loadMoreArrayList.clear();
                page_no = 1;
                editSearchET.setText("");
                IsVessalActive = 0;
                txtMailTV.setVisibility(View.GONE);
                executeAPI(page_no);
            }
        });

        IsVessalActive = 0;
        txtMailTV.setVisibility(View.GONE);

        scrollTextViewMethod();
    }

    private void scrollTextViewMethod() {
        final TextView first = findViewById(R.id.first);
        final TextView second = findViewById(R.id.second);

        final ValueAnimator animator = ValueAnimator.ofFloat(1.0f, 0.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(9000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = first.getWidth();
                final float translationX = width * progress;
                first.setTranslationX(translationX);
                second.setTranslationX(translationX - width);
            }
        });
        animator.start();
    }

//    private void sendEamilAPI(final String strTO, final String strSubject, final String strRecordID, final boolean isSingleVessel) {
////      http://jaohar.com/index.php/MailVesselDetails/mail_with_doc
////      http://jaohar.com/index.php/MailVesselDetails/multiple_vessels_api
//        String strUrl;
//        if (isSingleVessel == false) {
//            strUrl = JaoharConstants.Multi_Vessals_URL;
//        } else {
//            strUrl = JaoharConstants.Single_Vessals_URL;
//        }
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*************" + response.toString());
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("to_email", strTO);
//                params.put("subject", strSubject);
//                if (isSingleVessel == false) {
//                    Log.e(TAG, "ArrayLIST_DATA============= " + String.valueOf(getMultiVesselDetailsData()));
//                    Log.e(TAG, "ArrayLIST_DATA============= " + "hggh ===" + mRecordID);
//                    params.put("vessel_ids", String.valueOf(mRecordID));
//                } else {
//                    params.put("vessel_id", strRecordID);
//                }
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        addImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, AddVesselActivity.class);
                mIntent.putExtra("From", "AllVesselsActivity");
                mActivity.startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        searchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.is_Staff_FragmentClick = true;
                startActivity(new Intent(mActivity, SearchVesselsActivity.class)
                        .putExtra(JaoharConstants.SearchType, JaoharConstants.AllVesselType));
            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    resetRL1.setVisibility(View.VISIBLE);
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });


        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gettingVessal_Type();
                gettingVessal_Class();
                mArrayList.clear();
                loadMoreArrayList.clear();
                advancedSearchView();
            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsVessalActive = 0;
                isAdvanceSearch = false;
                isNormalSearch = false;
                txtMailTV.setVisibility(View.GONE);
                mArrayList.clear();
                loadMoreArrayList.clear();
                editSearchET.setText("");
                page_no = 1;
                executeAPI(page_no);
            }
        });


        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendingMailActivity.class);
                mIntent.putExtra("vessalName1", "Your Requested list of Vessels from Jaohar UK Limited");
                mIntent.putExtra("recordIDArray", getMultiVesselDetailsData());
                startActivity(mIntent);
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        // disable scroll on touch
        scrollTextTV.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

        scrollTextTV.loadDataWithBaseURL(null, "Site is now up to date.", "text/html", "UTF-8", null);
    }

    public void setUpNewsDialog(final String strNormalTEXT, final String strPhotoURL) {

        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.activity_full_news_description);
        searchDialog.setCanceledOnTouchOutside(true);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        ImageView add_image1 = (ImageView) searchDialog.findViewById(R.id.add_image1);
        final WebView showTXT = (WebView) searchDialog.findViewById(R.id.showTXT);
        final ProgressBar progressbar1 = searchDialog.findViewById(R.id.progressbar1);
        progressbar1.setVisibility(View.VISIBLE);
        if (!strPhotoURL.equals("")) {
            Glide.with(mActivity).load(strPhotoURL).into(add_image1);
        }
        add_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!strPhotoURL.equals("")) {
                    mImageArryList.clear();
                    mImageArryList.add(strPhotoURL);
                    Intent intent = new Intent(mActivity, GalleryActivity.class);
                    intent.putStringArrayListExtra("LIST", mImageArryList);
                    startActivity(intent);
                }
            }
        });
        showTXT.getSettings().setJavaScriptEnabled(true);
        showTXT.getSettings().setLoadWithOverviewMode(true);
        showTXT.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressbar1.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progressbar1.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressbar1.setVisibility(View.GONE);
            }
        });

        showTXT.loadDataWithBaseURL(null, strNormalTEXT, "text/html", "UTF-8", null);
        WebSettings webSettings = showTXT.getSettings();
        Resources res = mActivity.getResources();
        webSettings.setDefaultFontSize((int) res.getDimension(R.dimen._3sdp));
        searchDialog.show();
    }

    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mRecordID.size());
        for (int i = 0; i < mRecordID.size(); i++) {
            strData.add(mRecordID.get(i));
        }
        mRecordID.clear();
        return strData;
    }

    private void gettingVessal_Type() {
        mArrayListClass.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
            @Override
            public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                GetVesselTypeModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    modelArrayList=mModel.getData();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}

//    private void gettingVessal_Type() {
//        String strUrl = JaoharConstants.GET_ALL_VESSEL_TYPES;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "******" + response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//
//                    if (jsonObject.getString("status").equals("1")) {
//                        if (!jsonObject.isNull("data")) {
//                            JSONArray vehiclesArray = jsonObject.getJSONArray("data");
//
//                            for (int i = 0; i < vehiclesArray.length(); i++) {
//                                JSONObject mJsonObject11 = vehiclesArray.getJSONObject(i);
//
//                                vesselTypesModel.setId(mJsonObject11.getString("id"));
//                                vesselTypesModel.setName(mJsonObject11.getString("name"));
//                                modelArrayList.add(mJsonObject11.getString("name"));
//
//                            }
//                        }
//
//                    } else {
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void gettingVessal_Class() {
        mArrayListClass.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselClassModel> call1 = mApiInterface.getVesselClassRequest();
        call1.enqueue(new Callback<GetVesselClassModel>() {
            @Override
            public void onResponse(Call<GetVesselClassModel> call, retrofit2.Response<GetVesselClassModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                GetVesselClassModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mArrayListClass=mModel.getData();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetVesselClassModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


//    private void gettingVessal_Class() {
//        String strUrl = JaoharConstants.Get_Vessel_Class;
////      AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******" + response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equals("1")) {
//                        if (!jsonObject.isNull("data")) {
//                            JSONArray vehiclesArray = jsonObject.getJSONArray("data");
//                            for (int i = 0; i < vehiclesArray.length(); i++) {
//                                JSONObject mJsonObject11 = vehiclesArray.getJSONObject(i);
//                                vessalClassModel.setId(mJsonObject11.getString("id"));
//                                vessalClassModel.setClass_name(mJsonObject11.getString("class_name"));
//                                mArrayListClass.add(mJsonObject11.getString("class_name"));
//                            }
//                        }
//                    } else {
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******" + error.toString());
//            }
//        }) {
//            /* *
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //execute API
            if (mArrayList != null)
                mArrayList.clear();
            if (loadMoreArrayList != null)
                loadMoreArrayList.clear();

            page_no = 1;
            executeGettingAllNews();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, HomeActivity.class);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//        mActivity.startActivity(mIntent);
//        mActivity.finish();
    }

    public void executeAPI(int page_no) {
        isScrolling = true;
        if (page_no == 1) {
            if (isSwipeRefresh) {

            } else {
                AlertDialogManager.showProgressDialog(mActivity);
            }
        }
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<VesselForSaleModel> call1 = mApiInterface.getAllVesselsRequest(String.valueOf(page_no), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<VesselForSaleModel>() {
            @Override
            public void onResponse(Call<VesselForSaleModel> call, retrofit2.Response<VesselForSaleModel> response) {
                Log.e(TAG, "******Response*****" + response);
                isScrolling = false;
                AlertDialogManager.hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);
                resetRL1.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                loadMoreArrayList.clear();
                VesselForSaleModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mArrayList = mModel.getData().getAllVessels();
                    strLastPage = mModel.getData().getLastPage();

                    if (page_no == 1) {
                        mArrayList = mModel.getData().getAllVessels();
                    } else if (page_no > 1) {
                        loadMoreArrayList = mModel.getData().getAllVessels();
                    }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    }

                    if (page_no == 1) {
                        /*SetAdapter*/
                        setAdapter();
                    } else {
                        mVesselsAdapter.notifyDataSetChanged();
                    }
                }
                else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<VesselForSaleModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        vesselsRV.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        vesselsRV.setLayoutManager(linearLayoutManager);
        mVesselsAdapter = new VesselsAdapter(mActivity, mArrayList, mDeleteVesselsInterface, mSendEmailInterface, mSendMultiVesselDataInterface, strTextNEWS,
                strNormalTEXT, strPhotoURL, mPagination, mOpenPoUpInterface,
                mVesselToMail, new OnClickInterface() {
            @Override
            public void mOnClickInterface(int position) {
                PerformOptionsClick(position);
            }
        }, mFavoriteVesselInterface, mMailSelectedVesselsInterface);
        vesselsRV.setAdapter(mVesselsAdapter);

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));


                if (diff == 0) {

                    if (strLastPage.equals("FALSE")) {
                        progressBottomPB.setVisibility(View.VISIBLE);
                        ++page_no;
                    }

                    // your pagination code
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                executeAPI(page_no);
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 1000);
                }
            }
        });

        //for pagination
//        vesselsRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//
//                int visibleItemCount = linearLayoutManager.getChildCount();
//                int totalItemCount = linearLayoutManager.getItemCount();
//                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
//
//                if (!isScrolling) {
//                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
//                            && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
//
//                        progressBottomPB.setVisibility(View.VISIBLE);
//
//                        if (strLastPage.equals("FALSE")) {
//                            ++page_no;
//                            executeAPI(page_no);
//                        } else {
//                            progressBottomPB.setVisibility(View.GONE);
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//
//            }
//        });

    }

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_vessels_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout shareRL = view.findViewById(R.id.shareRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailToListRL = view.findViewById(R.id.mailToListRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        shareRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                shareViaWhatsApp();
            }
        });

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, EditVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                mActivity.startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, CopyVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                mActivity.startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.Is_click_form_All_Vessals = true;
                Intent mIntent = new Intent(mActivity, DetailsActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("isEditShow", true);
                mActivity.startActivity(mIntent);
            }
        });

        mailToListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IsAllVesselsClick = true;
                Intent mIntent = new Intent(mActivity, SendingMailToLIst_Vessels.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, SendingMailActivity.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void shareViaWhatsApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra("Intent.EXTRA_SUBJECT", "Jaohar");
            String shareMessage = "\nSee this Video\n";
            shareMessage = "https://play.google.com/store/apps/details?id=jaohar.com.jaohar&hl=en_IN&gl=US";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }


//    private void executeDeleteVesselseAPI(String strVesselID) {
//        String strUrl = JaoharConstants.DELETE_VESSELS + strVesselID + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*************" + response.toString());
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equals("1")) {
//                        Toast.makeText(mActivity, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        page_no = 1;
//                        executeAPI(page_no);
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    private void executeDeleteVesselseAPI(String strVesselID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteVesselsRequest(strVesselID,JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                isNormalSearch = true;
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, "" + mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    page_no = 1;
                    executeAPI(page_no);
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }


    public void deleteConfirmDialog(final String mVesselID) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteVesselseAPI(mVesselID);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeNormalSearch() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.searchVesselRequest(strSearchText,"staff", JaoharPreference.readString(VesselsShipsActivity.this, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                isNormalSearch = true;
                SearchModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mArrayList.clear();
                    mArrayList = mModel.getAllSearchedVessels();
                    /*SetAdapter*/
                    setAdapter();

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

//    private void executeNormalSearch() {
//        String strUrl = JaoharConstants.ROLE_SEARCH_API + "?input_search_value=" + strSearchText + "&role=" + "staff" + "&user_id=" + JaoharPreference.readString(VesselsShipsActivity.this, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "*****Response****" + response);
//                isNormalSearch = true;
//                try {
//                    JSONObject mJsonObject111 = new JSONObject(response);
//                    if (mJsonObject111.getString("status").equals("1")) {
//                        JaoharConstants.IsAllVesselsDetailBack = true;
//
//                        mArrayList.clear();
//                        JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_vessels");
//                        for (int i = 0; i < mJsonArray.length(); i++) {
//                            JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                            VesselesModel mVesselesModel = new VesselesModel();
//                            if (!mJsonObject1.isNull("record_id")) {
//                                mVesselesModel.setRecord_ID(mJsonObject1.getString("record_id"));
//                            }
//                            if (!mJsonObject1.isNull("IMO_number")) {
//                                mVesselesModel.setIMO_Number(mJsonObject1.getString("IMO_number"));
//                            }
//                            if (!mJsonObject1.isNull("short_description")) {
//                                mVesselesModel.setShort_Discription(mJsonObject1.getString("short_description"));
//                            }
//                            if (!mJsonObject1.isNull("vessel_name")) {
//                                mVesselesModel.setVessel_Name(mJsonObject1.getString("vessel_name"));
//                            }
//                            if (!mJsonObject1.isNull("vessel_type")) {
//                                mVesselesModel.setVessel_Type(mJsonObject1.getString("vessel_type"));
//                            }
//                            if (!mJsonObject1.isNull("year_built")) {
//                                mVesselesModel.setYear_Built(mJsonObject1.getString("year_built"));
//                            }
//                            if (!mJsonObject1.isNull("place_of_built")) {
//                                mVesselesModel.setPlace_of_Built(mJsonObject1.getString("place_of_built"));
//                            }
//                            if (!mJsonObject1.isNull("class")) {
//                                mVesselesModel.setmClass(mJsonObject1.getString("class"));
//                            }
//                            if (!mJsonObject1.isNull("flag")) {
//                                mVesselesModel.setFlag(mJsonObject1.getString("flag"));
//                            }
//                            if (!mJsonObject1.isNull("capacity")) {
//                                mVesselesModel.setDWT_Capacity(mJsonObject1.getString("capacity"));
//                            }
//                            if (!mJsonObject1.isNull("loa")) {
//                                mVesselesModel.setLOA_M(mJsonObject1.getString("loa"));
//                            }
//
//                            if (!mJsonObject1.isNull("breadth")) {
//                                mVesselesModel.setBreadth_M(mJsonObject1.getString("breadth"));
//                            }
//                            if (!mJsonObject1.isNull("depth")) {
//                                mVesselesModel.setDepth(mJsonObject1.getString("depth"));
//                            }
//                            if (!mJsonObject1.isNull("price_idea")) {
//                                mVesselesModel.setPrice_Idea(mJsonObject1.getString("price_idea"));
//                            }
//                            if (!mJsonObject1.isNull("vessel_location")) {
//                                mVesselesModel.setVessel_Location(mJsonObject1.getString("vessel_location"));
//                            }
//                            if (!mJsonObject1.isNull("full_description")) {
//                                mVesselesModel.setFull_Discription(mJsonObject1.getString("full_description"));
//                            }
//                            if (!mJsonObject1.isNull("ldt")) {
//                                mVesselesModel.setLdt(mJsonObject1.getString("ldt"));
//                            }
//                            if (!mJsonObject1.isNull("tcm")) {
//                                mVesselesModel.setTcm(mJsonObject1.getString("tcm"));
//                            }
//                            if (!mJsonObject1.isNull("currency")) {
//                                mVesselesModel.setCurrency(mJsonObject1.getString("currency"));
//                            }
//                            if (!mJsonObject1.isNull("photo1")) {
//                                mVesselesModel.setPhoto1(mJsonObject1.getString("photo1"));
//                            }
//                            if (!mJsonObject1.isNull("photo2")) {
//                                mVesselesModel.setPhoto2(mJsonObject1.getString("photo2"));
//                            }
//                            if (!mJsonObject1.isNull("photo3")) {
//                                mVesselesModel.setPhoto3(mJsonObject1.getString("photo3"));
//                            }
//                            if (!mJsonObject1.isNull("photo4")) {
//                                mVesselesModel.setPhoto4(mJsonObject1.getString("photo4"));
//                            }
//                            if (!mJsonObject1.isNull("photo5")) {
//                                mVesselesModel.setPhoto5(mJsonObject1.getString("photo5"));
//                            }
//                            if (!mJsonObject1.isNull("photo6")) {
//                                mVesselesModel.setPhoto6(mJsonObject1.getString("photo6"));
//                            }
//                            if (!mJsonObject1.isNull("photo7")) {
//                                mVesselesModel.setPhoto7(mJsonObject1.getString("photo7"));
//                            }
//                            if (!mJsonObject1.isNull("photo8")) {
//                                mVesselesModel.setPhoto8(mJsonObject1.getString("photo8"));
//                            }
//                            if (!mJsonObject1.isNull("photo9")) {
//                                mVesselesModel.setPhoto9(mJsonObject1.getString("photo9"));
//                            }
//                            if (!mJsonObject1.isNull("photo10")) {
//                                mVesselesModel.setPhoto10(mJsonObject1.getString("photo10"));
//                            }
//
//                            if (!mJsonObject1.isNull("date_entered")) {
//                                mVesselesModel.setDate_Entered(mJsonObject1.getString("date_entered"));
//                            }
//                            if (!mJsonObject1.isNull("status")) {
//                                mVesselesModel.setStatus(mJsonObject1.getString("status"));
//                            }
//                            if (!mJsonObject1.isNull("geared")) {
//                                mVesselesModel.setGeared(mJsonObject1.getString("geared"));
//                            }
//                            if (!mJsonObject1.isNull("bale")) {
//                                mVesselesModel.setBale(mJsonObject1.getString("bale"));
//                            }
//                            if (!mJsonObject1.isNull("grain")) {
//                                mVesselesModel.setGrain(mJsonObject1.getString("grain"));
//                            }
//                            if (!mJsonObject1.isNull("date_for_vessel")) {
//                                mVesselesModel.setDate_for_Vessel(mJsonObject1.getString("date_for_vessel"));
//                            }
//
//                            if (!mJsonObject1.isNull("vessel_added_by")) {
//                                mVesselesModel.setVessel_added_by(mJsonObject1.getString("vessel_added_by"));
//                            }
//                            if (!mJsonObject1.isNull("document1")) {
//                                mVesselesModel.setDocument1(mJsonObject1.getString("document1"));
//                            }
//                            if (!mJsonObject1.isNull("document2")) {
//                                mVesselesModel.setDocument2(mJsonObject1.getString("document2"));
//                            }
//                            if (!mJsonObject1.isNull("document3")) {
//                                mVesselesModel.setDocument3(mJsonObject1.getString("document3"));
//                            }
//                            if (!mJsonObject1.isNull("document4")) {
//                                mVesselesModel.setDocument4(mJsonObject1.getString("document4"));
//                            }
//                            if (!mJsonObject1.isNull("document5")) {
//                                mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                            }
//                            if (!mJsonObject1.isNull("document6")) {
//                                mVesselesModel.setDocument6(mJsonObject1.getString("document6"));
//                            }
//                            if (!mJsonObject1.isNull("document7")) {
//                                mVesselesModel.setDocument5(mJsonObject1.getString("document5"));
//                            }
//                            if (!mJsonObject1.isNull("document7")) {
//                                mVesselesModel.setDocument7(mJsonObject1.getString("document7"));
//                            }
//                            if (!mJsonObject1.isNull("document8")) {
//                                mVesselesModel.setDocument8(mJsonObject1.getString("document8"));
//                            }
//                            if (!mJsonObject1.isNull("document9")) {
//                                mVesselesModel.setDocument9(mJsonObject1.getString("document9"));
//                            }
//                            if (!mJsonObject1.isNull("document10")) {
//                                mVesselesModel.setDocument10(mJsonObject1.getString("document10"));
//                            }
//
//                            if (!mJsonObject1.isNull("vessel_add_time")) {
//                                mVesselesModel.setVessel_add_time(mJsonObject1.getString("vessel_add_time"));
//                            }
//                            if (!mJsonObject1.isNull("document1name")) {
//                                mVesselesModel.setDocument1name(mJsonObject1.getString("document1name"));
//                            }
//                            if (!mJsonObject1.isNull("document2name")) {
//                                mVesselesModel.setDocument2name(mJsonObject1.getString("document2name"));
//                            }
//                            if (!mJsonObject1.isNull("document3name")) {
//                                mVesselesModel.setDocument3name(mJsonObject1.getString("document3name"));
//                            }
//                            if (!mJsonObject1.isNull("document4name")) {
//                                mVesselesModel.setDocument4name(mJsonObject1.getString("document4name"));
//                            }
//                            if (!mJsonObject1.isNull("document5name")) {
//                                mVesselesModel.setDocument5name(mJsonObject1.getString("document5name"));
//                            }
//                            if (!mJsonObject1.isNull("document6name")) {
//                                mVesselesModel.setDocument6name(mJsonObject1.getString("document6name"));
//                            }
//                            if (!mJsonObject1.isNull("document7name")) {
//                                mVesselesModel.setDocument7name(mJsonObject1.getString("document7name"));
//                            }
//                            if (!mJsonObject1.isNull("document8name")) {
//                                mVesselesModel.setDocument8name(mJsonObject1.getString("document8name"));
//                            }
//                            if (!mJsonObject1.isNull("document9name")) {
//                                mVesselesModel.setDocument9name(mJsonObject1.getString("document9name"));
//                            }
//                            if (!mJsonObject1.isNull("document10name")) {
//                                mVesselesModel.setDocument10name(mJsonObject1.getString("document10name"));
//                            }
//
//                            if (!mJsonObject1.isNull("favorite_status")) {
//                                mVesselesModel.setFavorite_status(mJsonObject1.getString("favorite_status"));
//                            }
//
//                            if (!mJsonObject1.isNull("mail_data")) {
//                                mVesselesModel.setMail_data(mJsonObject1.getString("mail_data"));
//                            }
//                            if (!mJsonObject1.isNull("builder")) {
//                                mVesselesModel.setBuilder(mJsonObject1.getString("builder"));
//                            }
//                            if (!mJsonObject1.isNull("draught")) {
//                                mVesselesModel.setDraught(mJsonObject1.getString("draught"));
//                            }
//                            if (!mJsonObject1.isNull("gross_tonnage")) {
//                                mVesselesModel.setGross_tonnage(mJsonObject1.getString("gross_tonnage"));
//                            }
//                            if (!mJsonObject1.isNull("net_tonnage")) {
//                                mVesselesModel.setNet_tonnage(mJsonObject1.getString("net_tonnage"));
//                            }
//                            if (!mJsonObject1.isNull("teu")) {
//                                mVesselesModel.setTeu(mJsonObject1.getString("teu"));
//                            }
//                            if (!mJsonObject1.isNull("liquid")) {
//                                mVesselesModel.setLiquid(mJsonObject1.getString("liquid"));
//                            }
//                            if (!mJsonObject1.isNull("url")) {
//                                mVesselesModel.setUrl(mJsonObject1.getString("url"));
//                            }
//                            if (!mJsonObject1.isNull("gas")) {
//                                mVesselesModel.setGas(mJsonObject1.getString("gas"));
//                            }
//
//                            if (!mJsonObject1.isNull("owner_detail")) {
//                                mVesselesModel.setOwner_detail(mJsonObject1.getString("owner_detail"));
//                            }
//
//                            if (!mJsonObject1.isNull("machinery_detail")) {
//                                mVesselesModel.setMachinery_detail(mJsonObject1.getString("machinery_detail"));
//                            }
//
//                            if (mJsonObject1.has("remarks")) {
//                                JSONArray jsonArray = mJsonObject1.getJSONArray("remarks");
//                                for (int j = 0; j < jsonArray.length(); j++) {
//                                    JSONObject jsonObject = jsonArray.getJSONObject(j);
//
//                                    if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "").contains(jsonObject.getString("user_id"))) {
//                                        String remarks = jsonObject.getString("remark");
//                                        mVesselesModel.setRemarks(remarks);
//                                    }
//                                }
//                            }
//
//                            if (!mJsonObject1.isNull("no_passengers")) {
//                                mVesselesModel.setNo_passengers(mJsonObject1.getString("no_passengers"));
//                            }
//                            //mVesselesModel.setImage(imagesArray[i]);
//                            mArrayList.add(mVesselesModel);
//                        }
//
//                        /*SetAdapter*/
//                        setAdapter();
//                    } else if (mJsonObject111.getString("status").equals("100")) {
//                        AlertDialogManager.hideProgressDialog();
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
//                    } else {
//                        AlertDialogManager.hideProgressDialog();
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void advancedSearchView() {
        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advanced_search);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = (Button) searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = (ImageView) searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editFromCapacityET = (EditText) searchDialog.findViewById(R.id.editFromCapacityET);
        final EditText editToCapacityET = (EditText) searchDialog.findViewById(R.id.editToCapacityET);
        final EditText editFromLoaET = (EditText) searchDialog.findViewById(R.id.editFromLoaET);
        final EditText editToLoaET = (EditText) searchDialog.findViewById(R.id.editToLoaET);
//        final Spinner typeSpinner = (Spinner) searchDialog.findViewById(R.id.typeSpinner);
        final EditText editFromGrainET = (EditText) searchDialog.findViewById(R.id.editFromGrainET);
        final EditText editToGrainET = (EditText) searchDialog.findViewById(R.id.editToGrainET);
        final EditText editFromYearBuiltET = (EditText) searchDialog.findViewById(R.id.editFromYearBuiltET);
        final EditText editToYearBuiltET = (EditText) searchDialog.findViewById(R.id.editToYearBuiltET);
        final EditText editFromBaleET = (EditText) searchDialog.findViewById(R.id.editFromBaleET);
        final EditText editToBaleET = (EditText) searchDialog.findViewById(R.id.editToBaleET);
        final EditText editNameET = (EditText) searchDialog.findViewById(R.id.editNameET);
        final EditText editPlaceBuiltET = (EditText) searchDialog.findViewById(R.id.editPlaceBuiltET);
        final EditText editRecordIDET = (EditText) searchDialog.findViewById(R.id.editRecordIDET);
        final EditText editStatusET = (EditText) searchDialog.findViewById(R.id.editStatusET);
        final EditText editFromTEUET = (EditText) searchDialog.findViewById(R.id.editFromTEUET);
        final EditText editToTEUET = (EditText) searchDialog.findViewById(R.id.editToTEUET);
        final EditText editFromPassengerET = (EditText) searchDialog.findViewById(R.id.editFromPassengerET);
        final EditText editToPassendgerET = (EditText) searchDialog.findViewById(R.id.editToPassendgerET);
        final EditText editFromDraftrET = (EditText) searchDialog.findViewById(R.id.editFromDraftrET);
        final EditText editToDraftET = (EditText) searchDialog.findViewById(R.id.editToDraftET);
        final EditText editFromLiquidET = (EditText) searchDialog.findViewById(R.id.editFromLiquidET);
        final EditText editToLiquidET = (EditText) searchDialog.findViewById(R.id.editToLiquidET);
        final EditText editTypeET = (EditText) searchDialog.findViewById(R.id.editTypeET);
        final EditText editClassET = (EditText) searchDialog.findViewById(R.id.editClassET);
        final EditText editIMONumET = (EditText) searchDialog.findViewById(R.id.editIMONumET);
        final TextView recordTV = (TextView) searchDialog.findViewById(R.id.recordTV);

        final EditText editOffMarketET = searchDialog.findViewById(R.id.editOffMarketET);
        final EditText editGearedET = searchDialog.findViewById(R.id.editGearedET);

//        editStatusET.setText("Available");
        recordTV.setText("IMO No.");
        editRecordIDET.setHint("IMO No.");
        editStatusET.setKeyListener(null);
        editStatusET.setCursorVisible(false);
        final TextView txtHeaderStatus = (TextView) searchDialog.findViewById(R.id.txtHeaderStatus);
        final LinearLayout layoutNameLL = (LinearLayout) searchDialog.findViewById(R.id.layoutNameLL);
        final LinearLayout layoutPlaceBuiltLL = (LinearLayout) searchDialog.findViewById(R.id.layoutPlaceBuiltLL);
        final LinearLayout layoutRecordIDLL = (LinearLayout) searchDialog.findViewById(R.id.layoutRecordIDLL);
        final LinearLayout layoutStatusLL = (LinearLayout) searchDialog.findViewById(R.id.layoutStatusLL);
        final LinearLayout layoutIMONumLL = (LinearLayout) searchDialog.findViewById(R.id.layoutIMONumLL);
        layoutNameLL.setVisibility(View.VISIBLE);
        layoutPlaceBuiltLL.setVisibility(View.VISIBLE);
        layoutIMONumLL.setVisibility(View.VISIBLE);
        layoutRecordIDLL.setVisibility(View.GONE);

        final Spinner spinnerGeared = (Spinner) searchDialog.findViewById(R.id.spinnerGeared);

        List<String> list = new ArrayList<String>();
        list.add("Select Geared");
        list.add("Yes");
        list.add("No");
        ArrayAdapter<String> mSpAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        mSpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGeared.setAdapter(mSpAdapter);
        spinnerGeared.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strGeared = adapterView.getItemAtPosition(i).toString();
                if (strGeared.equals("Select Geared")) {
                    strGeared = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editTypeET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Type");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<GetVesselTypeData> adapter = new ArrayAdapter<GetVesselTypeData>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, modelArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strType = modelArrayList.get(position).getName();
                            editTypeET.setText(strType);
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editClassET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
//                  categoryDialog.setCanceledOnTouchOutside(true);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Class");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    // Define a new Adapter
                    // First parameter - Context
                    // Second parameter - Layout for the row
                    // Third parameter - ID of the TextView to which the data is written
                    // Forth - the Array of data

                    ArrayAdapter<GetVesselClassData> adapter = new ArrayAdapter<GetVesselClassData>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListClass);

                    // Assign adapter to ListView
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strClass = mArrayListClass.get(position).getClassName();
                            editClassET.setText(strClass);
                            categoryDialog.dismiss();

                        }
                    });


                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });


        layoutStatusLL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(VesselsShipsActivity.this, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        txtHeaderStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(VesselsShipsActivity.this, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(VesselsShipsActivity.this, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });


        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchDialog.dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strCapacityFrom = editFromCapacityET.getText().toString();
                strCapacityTo = editToCapacityET.getText().toString();

                strName = editNameET.getText().toString();

                strLoaFrom = editFromLoaET.getText().toString();
                strLoaTo = editToLoaET.getText().toString();

                strGrainFrom = editFromGrainET.getText().toString();
                strGrainTo = editToGrainET.getText().toString();

                strYearofBuildFrom = editFromYearBuiltET.getText().toString();
                strYearofBuildTo = editToYearBuiltET.getText().toString();

                strBaleFrom = editFromBaleET.getText().toString();
                strBaleTo = editToBaleET.getText().toString();

                strPlaceBuild = editPlaceBuiltET.getText().toString();
                strRecordID = editRecordIDET.getText().toString();
                IMO_number = editIMONumET.getText().toString();


                strStatus = editStatusET.getText().toString();
                strTEUFrom = editFromTEUET.getText().toString();
                strTEUto = editToTEUET.getText().toString();
                strDraftsTo = editToDraftET.getText().toString();
                strDraftsFrom = editFromDraftrET.getText().toString();
                strPassangerFrom = editFromPassengerET.getText().toString();
                strPassangerTo = editToPassendgerET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();
                strLiquidFrom = editFromLiquidET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();

//                strGeared = spinnerGeared.getSelectedItem().toString();

                strGeared = editGearedET.getText().toString();
                strOffMarket = editOffMarketET.getText().toString();

                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchAPI(searchDialog);
                }
            }
        });
        searchDialog.show();
    }

//    public void executeAdvanceSearchAPI(final Dialog mDialog) {
//        mArrayList.clear();
//        if (strType == null) {
//            strType = "";
//        }
//        if (strClass == null) {
//            strClass = "";
//        }
//        String strUrl = JaoharConstants.ADVANCED_SEARCH + "?capacity_from=" + strCapacityFrom +
//                "&capacity_to=" + strCapacityTo +
//                "&loa_from=" + strLoaFrom +
//                "&loa_to=" + strLoaTo +
//                "&vessel_type=" + strType +
//                "&year_of_built_from=" + strYearofBuildFrom +
//                "&year_of_built_to=" + strYearofBuildTo +
//                "&grain_from=" + strGrainFrom +
//                "&grain_to=" + strGrainTo +
//                "&bale_from=" + strBaleFrom +
//                "&name=" + strName +
//                "&record_id=" + "" +
//                "&IMO_number=" + strRecordID +
//                "&bale_to=" + strBaleTo +
//                "&status=" + strStatus +
//                "&geared=" + strGeared +
//                "&role=" + "staff" +
//                "&teu_from=" + strTEUFrom +
//                "&teu_to=" + strTEUto +
//                "&no_passengers_from=" + strPassangerFrom +
//                "&no_passengers_to=" + strPassangerTo +
//                "&draft_from=" + strDraftsFrom +
//                "&draft_to=" + strDraftsTo +
//                "&liquid_from=" + strLiquidFrom +
//                "&liquid_to=" + strLiquidTo +
//                "&class=" + strClass +
//                "&place_of_built=" + strPlaceBuild +
//                "&user_id=" + JaoharPreference.readString(VesselsShipsActivity.this, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                resetRL1.setVisibility(View.VISIBLE);
//                Log.e(TAG, "*****Response****" + response);
//                /*Paras Search Data*/
//                isAdvanceSearch = true;
//                parseSearchResponse(response, mDialog);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

        public void executeAdvanceSearchAPI(final Dialog mDialog) {
        mArrayList.clear();
        if (strType == null) {
            strType = "";
        }
        if (strClass == null) {
            strClass = "";
        }
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.advanceSearchVesselRequest(strCapacityFrom, strCapacityTo, strLoaFrom, strLoaTo,
                strType, strYearofBuildFrom, strYearofBuildTo, strGrainFrom, strGrainTo, strBaleFrom, strName, strRecordID,IMO_number, strBaleTo, strStatus,
                strGeared, "staff", strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo,
                strLiquidFrom, strLiquidTo, strClass, strPlaceBuild, JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strOffMarket);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.VISIBLE);
                Log.e(TAG, "*****Response****" + response);
                /*Paras Search Data*/
                isAdvanceSearch = true;
                SearchModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mDialog.dismiss();
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mArrayList.clear();
                    mArrayList = mModel.getAllSearchedVessels();
                    /*SetAdapter*/
                    setAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus().equals("0")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }



//    private void parseSearchResponse(String response, Dialog mDialog) {
//        try {
//            JSONObject mJsonObject111 = new JSONObject(response);
//
//            if (mJsonObject111.getString("status").equals("1")) {
//                JaoharConstants.IsAllVesselsDetailBack = true;
//                mArrayList.clear();
//                JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_vessels");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                    VesselesModel mVesselesModel = new VesselesModel();
//                    if (!mJsonObject1.isNull("record_id")) {
//                        mVesselesModel.setRecord_ID(mJsonObject1.getString("record_id"));
//                    }
//                    if (!mJsonObject1.isNull("IMO_number")) {
//                        mVesselesModel.setIMO_Number(mJsonObject1.getString("IMO_number"));
//                    }
//                    if (!mJsonObject1.isNull("short_description")) {
//                        mVesselesModel.setShort_Discription(mJsonObject1.getString("short_description"));
//                    }
//                    if (!mJsonObject1.isNull("vessel_name")) {
//                        mVesselesModel.setVessel_Name(mJsonObject1.getString("vessel_name"));
//                    }
//                    if (!mJsonObject1.isNull("vessel_type")) {
//                        mVesselesModel.setVessel_Type(mJsonObject1.getString("vessel_type"));
//                    }
//                    if (!mJsonObject1.isNull("year_built")) {
//                        mVesselesModel.setYear_Built(mJsonObject1.getString("year_built"));
//                    }
//                    if (!mJsonObject1.isNull("place_of_built")) {
//                        mVesselesModel.setPlace_of_Built(mJsonObject1.getString("place_of_built"));
//                    }
//                    if (!mJsonObject1.isNull("class")) {
//                        mVesselesModel.setmClass(mJsonObject1.getString("class"));
//                    }
//                    if (!mJsonObject1.isNull("flag")) {
//                        mVesselesModel.setFlag(mJsonObject1.getString("flag"));
//                    }
//                    if (!mJsonObject1.isNull("capacity")) {
//                        mVesselesModel.setDWT_Capacity(mJsonObject1.getString("capacity"));
//                    }
//                    if (!mJsonObject1.isNull("loa")) {
//                        mVesselesModel.setLOA_M(mJsonObject1.getString("loa"));
//                    }
//
//                    if (!mJsonObject1.isNull("breadth")) {
//                        mVesselesModel.setBreadth_M(mJsonObject1.getString("breadth"));
//                    }
//                    if (!mJsonObject1.isNull("depth")) {
//                        mVesselesModel.setDepth(mJsonObject1.getString("depth"));
//                    }
//                    if (!mJsonObject1.isNull("price_idea")) {
//                        mVesselesModel.setPrice_Idea(mJsonObject1.getString("price_idea"));
//                    }
//                    if (!mJsonObject1.isNull("vessel_location")) {
//                        mVesselesModel.setVessel_Location(mJsonObject1.getString("vessel_location"));
//                    }
//                    if (!mJsonObject1.isNull("full_description")) {
//                        mVesselesModel.setFull_Discription(mJsonObject1.getString("full_description"));
//                    }
//                    if (!mJsonObject1.isNull("ldt")) {
//                        mVesselesModel.setLdt(mJsonObject1.getString("ldt"));
//                    }
//                    if (!mJsonObject1.isNull("tcm")) {
//                        mVesselesModel.setTcm(mJsonObject1.getString("tcm"));
//                    }
//                    if (!mJsonObject1.isNull("currency")) {
//                        mVesselesModel.setCurrency(mJsonObject1.getString("currency"));
//                    }
//
//                    if (!mJsonObject1.isNull("date_entered")) {
//                        mVesselesModel.setDate_Entered(mJsonObject1.getString("date_entered"));
//                    }
//                    if (!mJsonObject1.isNull("status")) {
//                        mVesselesModel.setStatus(mJsonObject1.getString("status"));
//                    }
//                    if (!mJsonObject1.isNull("geared")) {
//                        mVesselesModel.setGeared(mJsonObject1.getString("geared"));
//                    }
//                    if (!mJsonObject1.isNull("bale")) {
//                        mVesselesModel.setBale(mJsonObject1.getString("bale"));
//                    }
//                    if (!mJsonObject1.isNull("grain")) {
//                        mVesselesModel.setGrain(mJsonObject1.getString("grain"));
//                    }
//                    if (!mJsonObject1.isNull("date_for_vessel")) {
//                        mVesselesModel.setDate_for_Vessel(mJsonObject1.getString("date_for_vessel"));
//                    }
//
//                    if (!mJsonObject1.isNull("vessel_added_by")) {
//                        mVesselesModel.setVessel_added_by(mJsonObject1.getString("vessel_added_by"));
//                    }
//
//
//                    if (!mJsonObject1.isNull("vessel_add_time")) {
//                        mVesselesModel.setVessel_add_time(mJsonObject1.getString("vessel_add_time"));
//                    }
//
//
//                    if (!mJsonObject1.isNull("mail_data")) {
//
//                        mVesselesModel.setMail_data(mJsonObject1.getString("mail_data"));
//                    }
//                    if (!mJsonObject1.isNull("builder")) {
//                        mVesselesModel.setBuilder(mJsonObject1.getString("builder"));
//                    }
//                    if (!mJsonObject1.isNull("draught")) {
//                        mVesselesModel.setDraught(mJsonObject1.getString("draught"));
//                    }
//                    if (!mJsonObject1.isNull("gross_tonnage")) {
//                        mVesselesModel.setGross_tonnage(mJsonObject1.getString("gross_tonnage"));
//                    }
//                    if (!mJsonObject1.isNull("net_tonnage")) {
//                        mVesselesModel.setNet_tonnage(mJsonObject1.getString("net_tonnage"));
//                    }
//                    if (!mJsonObject1.isNull("teu")) {
//                        mVesselesModel.setTeu(mJsonObject1.getString("teu"));
//                    }
//                    if (!mJsonObject1.isNull("liquid")) {
//                        mVesselesModel.setLiquid(mJsonObject1.getString("liquid"));
//                    }
//                    if (!mJsonObject1.isNull("url")) {
//                        mVesselesModel.setUrl(mJsonObject1.getString("url"));
//                    }
//                    if (!mJsonObject1.isNull("gas")) {
//                        mVesselesModel.setGas(mJsonObject1.getString("gas"));
//                    }
//
//                    if (!mJsonObject1.isNull("owner_detail")) {
//                        mVesselesModel.setOwner_detail(mJsonObject1.getString("owner_detail"));
//                    }
//
//                    if (!mJsonObject1.isNull("machinery_detail")) {
//                        mVesselesModel.setMachinery_detail(mJsonObject1.getString("machinery_detail"));
//                    }
//
//                    if (mJsonObject1.has("remarks")) {
//                        JSONArray jsonArray = mJsonObject1.getJSONArray("remarks");
//                        for (int j = 0; j < jsonArray.length(); j++) {
//                            JSONObject jsonObject = jsonArray.getJSONObject(j);
//
//                            if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "").contains(jsonObject.getString("user_id"))) {
//                                String remarks = jsonObject.getString("remark");
//                                mVesselesModel.setRemarks(remarks);
//                            }
//                        }
//                    }
//
//                    if (!mJsonObject1.isNull("no_passengers")) {
//                        mVesselesModel.setNo_passengers(mJsonObject1.getString("no_passengers"));
//                    }
//                    if (!mJsonObject1.isNull("photo1")) {
//                        mVesselesModel.setPhoto1(mJsonObject1.getString("photo1"));
//                    }
//                    if (!mJsonObject1.isNull("photo2")) {
//                        mVesselesModel.setPhoto2(mJsonObject1.getString("photo2"));
//                    }
//                    if (!mJsonObject1.isNull("photo3")) {
//                        mVesselesModel.setPhoto3(mJsonObject1.getString("photo3"));
//                    }
//                    if (!mJsonObject1.isNull("photo4")) {
//                        mVesselesModel.setPhoto4(mJsonObject1.getString("photo4"));
//                    }
//                    if (!mJsonObject1.isNull("photo5")) {
//                        mVesselesModel.setPhoto5(mJsonObject1.getString("photo5"));
//                    }
//                    if (!mJsonObject1.isNull("photo6")) {
//                        mVesselesModel.setPhoto6(mJsonObject1.getString("photo6"));
//                    }
//                    if (!mJsonObject1.isNull("photo7")) {
//                        mVesselesModel.setPhoto7(mJsonObject1.getString("photo7"));
//                    }
//                    if (!mJsonObject1.isNull("photo8")) {
//                        mVesselesModel.setPhoto8(mJsonObject1.getString("photo8"));
//                    }
//                    if (!mJsonObject1.isNull("photo9")) {
//                        mVesselesModel.setPhoto9(mJsonObject1.getString("photo9"));
//                    }
//                    if (!mJsonObject1.isNull("photo10")) {
//                        mVesselesModel.setPhoto10(mJsonObject1.getString("photo10"));
//                    }
//
//                    if (!mJsonObject1.isNull("favorite_status")) {
//                        mVesselesModel.setFavorite_status(mJsonObject1.getString("favorite_status"));
//                    }
//
//                    mArrayList.add(mVesselesModel);
//                }
//
//                /*SetAdapter*/
//                setAdapter();
//                AlertDialogManager.hideProgressDialog();
//                mDialog.dismiss();
//            } else if (mJsonObject111.getString("status").equals("100")) {
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
//            } else {
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_no = 1;
                txtMailTV.setVisibility(View.GONE);
                mArrayList.clear();
                loadMoreArrayList.clear();
                executeAPI(page_no);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setUserNewsAdapter() {
        newsRV.setNestedScrollingEnabled(false);
        newsRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        newsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mNewsAdapter = new UserNewsAdapter(mActivity, mNewsArrayList, mOpenNewsPopUP);
        newsRV.setAdapter(mNewsAdapter);
    }

//    private void executeGettingAllNews() {
//        mNewsArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strAPIUrl = JaoharConstants.GET_ALL_NEWS;
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        JSONArray mArray = mJsonObject.getJSONArray("data");
//                        for (int i = 0; i < mArray.length(); i++) {
//                            JSONObject mJson = mArray.getJSONObject(i);
//                            NewsModel mModel = new NewsModel();
//                            mModel.setCreated_at(mJson.getString("created_at"));
//                            mModel.setId(mJson.getString("id"));
//                            mModel.setNews(mJson.getString("news"));
//                            mModel.setType(mJson.getString("type"));
//                            mModel.setPhoto(mJson.getString("photo"));
//                            mModel.setNews_text(mJson.getString("news_text"));
//                            mNewsArrayList.add(mModel);
//                        }
//                        /*setAdapter*/
//                        setUserNewsAdapter();
//                    }
//
//                    executeAPI(page_no);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(mStringRequest);
//    }

    private void executeGettingAllNews() {
        mNewsArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AllNewsModel> call1 = mApiInterface.getallNewsRequest();
        call1.enqueue(new Callback<AllNewsModel>() {
            @Override
            public void onResponse(Call<AllNewsModel> call, retrofit2.Response<AllNewsModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                AllNewsModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mNewsArrayList=mModel.getData();
                    /*setAdapter*/
                    setUserNewsAdapter();
                  executeAPI(page_no);}
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AllNewsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


    /* *
     * Execute Favorite API for vessel
     * @param
     * @user_id
     * */
    public void executeFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.makeFavoriteVesselsRequest(vessel_id, JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_yellow);
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_fav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
//        AlertDialogManager.showProgressDialog(mActivity);
////        String strUrl = JaoharConstants.MakeVesselFavorite + vessel_id + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        String strUrl = JaoharConstants.MakeVesselFavorite + "?vessel_id=" + vessel_id + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
////        String strUrl = JaoharConstants.MakeVesselFavorite + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***URLResponce***" + response);
//
//                try {
//                    JSONObject mJsonObject1 = new JSONObject(response);
//
//                    if (mJsonObject1.getString("status").equals("1")) {
//
//                        imageView.setImageResource(R.drawable.ic_bookmark_yellow);
//
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_fav));
//
//                    } else if (mJsonObject1.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }



    /* *
     * Execute UnFavorite API for vessel
     * @param
     * @user_id
     * */
//    public void executeUnFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
//        AlertDialogManager.showProgressDialog(mActivity);
////        String strUrl = JaoharConstants.UnFavoriteVessel + vessel_id + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        String strUrl = JaoharConstants.UnFavoriteVessel + "?vessel_id=" + vessel_id + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//
////        String strUrl = JaoharConstants.MakeVesselFavorite + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***URLResponce***" + response);
//
//                try {
//                    JSONObject mJsonObject1 = new JSONObject(response);
//
//                    if (mJsonObject1.getString("status").equals("1")) {
//
//                        imageView.setImageResource(R.drawable.ic_bookmark_border);
//
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
//
//                    } else if (mJsonObject1.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void executeUnFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.unFavoriteVesselsRequest(vessel_id, JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_border);
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
                }
                else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });


    }



}
