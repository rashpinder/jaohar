package jaohar.com.jaohar.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import im.delight.android.webview.AdvancedWebView;
import jaohar.com.jaohar.R;

public class WebViewActivity extends AppCompatActivity {
    Activity mActivity = WebViewActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = WebViewActivity.this.getClass().getSimpleName();
    AdvancedWebView mWebView;
    RelativeLayout llLeftLL;
    ProgressBar progressbar1;
    TextView txtCenter;
    ImageView imgBack,docImageView;
    Intent intent;
    String strNews, strTitle, url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view2);
        initViews();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initViews() {
        docImageView = findViewById(R.id.docImageView);
        mWebView = findViewById(R.id.mWebView);
        progressbar1 = findViewById(R.id.progressbar1);

        intent = getIntent();
        if (intent.hasExtra("url")) {
            url = intent.getStringExtra("url");
            String extension = url.substring(url.lastIndexOf(".") + 1);
            if (extension.contains("pdf")){
                mWebView.setVisibility(View.VISIBLE);
                docImageView.setVisibility(View.GONE);


                progressbar1.setVisibility(View.VISIBLE);


                mWebView.getSettings().setJavaScriptEnabled(true);
                mWebView.getSettings().setBuiltInZoomControls(true);
                mWebView.getSettings().setLoadWithOverviewMode(true);
                mWebView.getSettings().setUseWideViewPort(true);

                mWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        progressbar1.setVisibility(View.VISIBLE);
                        view.loadUrl(url);
                        return true;
                    }


                    @Override
                    public void onPageFinished(WebView view, final String url) {
                        progressbar1.setVisibility(View.GONE);
                        view.loadUrl(url);
                    }


                    @Override
                    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
                        progressbar1.setVisibility(View.GONE);
                    }
                });


                if (url.contains(".pdf")) {
                    mWebView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
                } else if (url.contains(".doc")) {
                    mWebView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);
                } else if (url.contains(".jpg")) {
//            mWebView.loadDataWithBaseURL(null, "<html><head></head><body><table style=\"width:100%; height:100%;\"><tr><td style=\"vertical-align:middle;\"><img src=\"" + url + "\"></td></tr></table></body></html>", "html/css", "utf-8", null);
                    mWebView.loadUrl(url);
                } else {
                    mWebView.loadUrl(url);
                }

            }else {
                mWebView.setVisibility(View.GONE);
                progressbar1.setVisibility(View.GONE);
                docImageView.setVisibility(View.VISIBLE);
                Glide.with(mActivity).load(url).placeholder(R.drawable.placeholder).into(docImageView);
            }
        }
        imgBack = findViewById(R.id.imgBack);
        txtCenter = findViewById(R.id.txtCenter);

        llLeftLL = findViewById(R.id.llLeftLL);





       /* mWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "myPDFfile.pdf");
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        });*/

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}