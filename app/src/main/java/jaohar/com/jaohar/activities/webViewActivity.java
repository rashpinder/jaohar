package jaohar.com.jaohar.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import im.delight.android.webview.AdvancedWebView;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.utils.JaoharConstants;

public class webViewActivity extends BaseActivity {
    /**
     * Current Activity Instance
     */
    Activity mActivity = webViewActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = webViewActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    AdvancedWebView mWebView;
    RelativeLayout llLeftLL;
    ProgressBar progressbar1;
    TextView txtCenter;
    ImageView imgBack;

    String strNews, strTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_web_view);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            if (extras.getString("news") != null)
                strNews = extras.getString("news");

            if (extras.getString(JaoharConstants.TITLE) != null)
                strTitle = extras.getString(JaoharConstants.TITLE);
        }

        setViewIDs();
    }

    @SuppressLint({"WrongViewCast", "SetJavaScriptEnabled"})
    private void setViewIDs() {
        imgBack = findViewById(R.id.imgBack);
        txtCenter = findViewById(R.id.txtCenter);
        mWebView = findViewById(R.id.mWebView);
        llLeftLL = findViewById(R.id.llLeftLL);
        progressbar1 = findViewById(R.id.progressbar1);

        if (strTitle != null && !strTitle.equals("")) {
            txtCenter.setText(strTitle);
        }

        progressbar1.setVisibility(View.VISIBLE);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressbar1.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progressbar1.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressbar1.setVisibility(View.GONE);
            }
        });

        if (strNews.contains(".txt")) {
            mWebView.loadUrl(strNews);
        } else if (strNews.contains("index.php")) {
            mWebView.loadUrl(strNews);
        } else if (strNews.contains(".pdf")) {
            mWebView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + strNews);
        } else if (strNews.contains(".doc")) {
            mWebView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + strNews);
        } else {
            mWebView.loadUrl(strNews);
        }

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
