package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.invoices_module.AllDraftsInvoicesAdapter;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.invoice_module.DeleteInvoiceDraftInterface;
import jaohar.com.jaohar.interfaces.invoice_module.EditInvoiceDraftInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.invoicedraftmodels.Data;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.views.PinchRecyclerView;
import retrofit2.Call;
import retrofit2.Callback;

public class AllDraftsInvoicesActivity extends BaseActivity {
    /**
     * set Activity
     **/
    Activity mActivity = AllDraftsInvoicesActivity.this;
    /**
     * set Activity TAG
     **/
    String TAG = AllDraftsInvoicesActivity.this.getClass().getSimpleName();
    /*
    Widgets
     */
    @BindView(R.id.inVoicesRV)
    PinchRecyclerView inVoicesRV;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;

    @BindView(R.id.txtCenter)
    TextView txtCenter;

    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;

    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;

    AllDraftsInvoicesAdapter mAllDraftsInvoicesAdapter;

    List<Data> mAllInvoicesDrafts = new ArrayList();
    //    List<AllInvoicesItem> allInvoices = new ArrayList();
    int page_no = 1;
    Boolean isScroolling = false;
    boolean isSwipeRefresh = false;
    private String strLastPage = "FALSE";

    ArrayList<InVoicesModel> modelArrayList = new ArrayList<InVoicesModel>();
    ArrayList<InVoicesModel> mLoadMore = new ArrayList<InVoicesModel>();
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_drafts_invoices);

        setStatusBar();

        ButterKnife.bind(this);

        setTopBarMenu();

//        executeAllDraftInvoicesApi();

        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });
    }

    private void setTopBarMenu() {
        imgBack.setImageResource(R.drawable.back);
        txtCenter.setText("All Drafts");

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

//    private void executeAllDraftInvoicesApi() {
//        ApiInterface api = ApiClient.getApiClient().create(ApiInterface.class);
//        api.GetAllInvoiceDraftNew(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), String.valueOf(page_no))
//                .enqueue(new Callback<AllInvoicesDrafts>() {
//                    @Override
//                    public void onResponse(Call<AllInvoicesDrafts> call, Response<AllInvoicesDrafts> response) {
//                        if (response.body() != null) {
//                            Log.e(TAG, "****Response****" + response.body().toString());
//
//                            allInvoices = response.body().getData().getAllInvoices();
//
//                            setAdapter();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<AllInvoicesDrafts> call, Throwable t) {
//                        Log.e(TAG, "****Error****" + t.toString());
//                    }
//                });
//    }

//    private void setAdapter() {
//        newsRV.setNestedScrollingEnabled(false);
//        newsRV.setLayoutManager(new LinearLayoutManager(mActivity));
//        mAllDraftsInvoicesAdapter = new AllDraftsInvoicesAdapter(mActivity, allInvoices,
//                mEditInvoiceDraftInterface, mDeleteInvoiceDraftInterface);
//        newsRV.setAdapter(mAllDraftsInvoicesAdapter);
//    }

    EditInvoiceDraftInterface mEditInvoiceDraftInterface = new EditInvoiceDraftInterface() {
        @Override
        public void mEditInvoiceDraft(InVoicesModel mModel) {
            JaoharConstants.IS_INVOICE_BACK = false;
            Intent mIntent = new Intent(mActivity, EditInvoiceDraftActivity.class);
            mIntent.putExtra("Model", mModel);
            mActivity.startActivity(mIntent);
            overridePendingTransitionEnter();
        }
    };

    DeleteInvoiceDraftInterface mDeleteInvoiceDraftInterface = new DeleteInvoiceDraftInterface() {
        @Override
        public void mDeleteInvoiceDraft(InVoicesModel mModel, int pos) {
            deleteConfirmDialog(mModel, pos);
        }
    };

    public void deleteConfirmDialog(final InVoicesModel mModel, int pos) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText("Are you sure want  to Delete Invoice Draft?");
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mModel, pos);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /*Execute Delete API*/
    private void executeDeleteAPI(InVoicesModel mModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteDraftInvoiceRequest("1",
                JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),
                mModel.getInvoice_id());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {

                    modelArrayList.remove(position);

                    if (mAllDraftsInvoicesAdapter != null)
                        mAllDraftsInvoicesAdapter.notifyDataSetChanged();

                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());

                } else if (mModel.getStatus() == 0) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeAPI() {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
        }
        if (page_no == 1) {
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.GONE);
            showProgressDialog(mActivity);
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.GetAllInvoiceDraft(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e(TAG, "*****ResponseALLk****" + response.body());
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse(String response) {
        mLoadMore.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            if (mJsonObject.getString("status").equals("1")) {
                JSONObject mDataObject = mJsonObject.getJSONObject("data");
                JSONArray mJsonArray = mDataObject.getJSONArray("all_invoices");
                strLastPage = mDataObject.getString("last_page");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    InVoicesModel mModel = new InVoicesModel();
                    JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                    if (!mAllDataObj.isNull("invoice_id"))
                        mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                    if (!mAllDataObj.getString("pdf").equals(""))
                        mModel.setPdf(mAllDataObj.getString("pdf"));
                    if (!mAllDataObj.getString("pdf_name").equals(""))
                        mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                    if (!mAllDataObj.isNull("invoice_no"))
                        mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                    if (!mAllDataObj.isNull("invoice_date"))
                        mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                    if (!mAllDataObj.isNull("term_days"))
                        mModel.setTerm_days(mAllDataObj.getString("term_days"));
                    if (!mAllDataObj.isNull("currency"))
                        mModel.setCurrency(mAllDataObj.getString("currency"));
                    if (!mAllDataObj.isNull("status"))
                        mModel.setStatus(mAllDataObj.getString("status"));
                    if (!mAllDataObj.isNull("reference"))
                        mModel.setRefrence1(mAllDataObj.getString("reference"));
                    if (!mAllDataObj.isNull("reference1"))
                        mModel.setRefrence2(mAllDataObj.getString("reference1"));
                    if (!mAllDataObj.isNull("reference2"))
                        mModel.setRefrence3(mAllDataObj.getString("reference2"));
                    if (!mAllDataObj.isNull("payment_id"))
                        mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                    if (!mAllDataObj.isNull("inv_state"))
                        mModel.setInv_state(mAllDataObj.getString("inv_state"));

                    if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                        JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                        SignatureModel mSignModel = new SignatureModel();
                        if (!mSignDataObj.isNull("sign_id"))
                            mSignModel.setId(mSignDataObj.getString("sign_id"));
                        if (!mSignDataObj.isNull("sign_name"))
                            mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                        if (!mSignDataObj.isNull("sign_image"))
                            mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                        mModel.setmSignatureModel(mSignModel);
                    }
                    if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                        JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                        StampsModel mStampsModel = new StampsModel();
                        if (!mStampDataObj.isNull("stamp_id"))
                            mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                        if (!mStampDataObj.isNull("stamp_name"))
                            mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                        if (!mStampDataObj.isNull("stamp_image"))
                            mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                        mModel.setmStampsModel(mStampsModel);
                    }
                    if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                        JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                        BankModel mBankModel = new BankModel();
                        if (!mBankDataObj.isNull("bank_id"))
                            mBankModel.setId(mBankDataObj.getString("bank_id"));
                        if (!mBankDataObj.isNull("beneficiary"))
                            mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                        if (!mBankDataObj.isNull("bank_name"))
                            mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                        if (!mBankDataObj.isNull("address1"))
                            mBankModel.setAddress1(mBankDataObj.getString("address1"));
                        if (!mBankDataObj.isNull("address2"))
                            mBankModel.setAddress2(mBankDataObj.getString("address2"));
                        if (!mBankDataObj.isNull("iban_ron"))
                            mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                        if (!mBankDataObj.isNull("iban_usd"))
                            mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                        if (!mBankDataObj.isNull("iban_eur"))
                            mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                        if (!mBankDataObj.isNull("swift"))
                            mBankModel.setSwift(mBankDataObj.getString("swift"));
                        mModel.setmBankModel(mBankModel);
                    }
                    if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                        JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                        VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                        if (!mSearchVesselObj.isNull("vessel_id"))
                            mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                        if (!mSearchVesselObj.isNull("vessel_name"))
                            mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                        if (!mSearchVesselObj.isNull("IMO_no"))
                            mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                        if (!mSearchVesselObj.isNull("flag"))
                            mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                        mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                    }
                    if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                        JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                        CompaniesModel mCompaniesModel = new CompaniesModel();
                        if (!mSearchCompanyObj.isNull("id"))
                            mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                        if (!mSearchCompanyObj.isNull("company_name"))
                            mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                        if (!mSearchCompanyObj.isNull("Address1"))
                            mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                        if (!mSearchCompanyObj.isNull("Address2"))
                            mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                        if (!mSearchCompanyObj.isNull("Address3"))
                            mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                        if (!mSearchCompanyObj.isNull("Address4"))
                            mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                        if (!mSearchCompanyObj.isNull("Address5"))
                            mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                        mModel.setmCompaniesModel(mCompaniesModel);
                    }
                    if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                        JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                        PaymentModel mPaymentModel = new PaymentModel();
                        if (!mPaymentObject.isNull("payment_id")) {
                            mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                        }
                        if (!mPaymentObject.isNull("sub_total")) {
                            mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                        }
                        if (!mPaymentObject.isNull("VAT")) {
                            mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                        }
                        if (!mPaymentObject.isNull("vat_price")) {
                            mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                        }
                        if (!mPaymentObject.isNull("total")) {
                            mPaymentModel.setTotal(mPaymentObject.getString("total"));
                        }
                        if (!mPaymentObject.isNull("paid")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                        }
                        if (!mPaymentObject.isNull("due")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("due"));
                        }
                        mModel.setmPaymentModel(mPaymentModel);
                    }
                    if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                        JSONArray mItemArray = mJson.getJSONArray("items_data");
                        for (int k = 0; k < mItemArray.length(); k++) {
                            JSONObject mItemObj = mItemArray.getJSONObject(k);
                            InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                            if (!mItemObj.isNull("item_id"))
                                mItemModel.setItemID(mItemObj.getString("item_id"));
                            if (!mItemObj.isNull("item_serial_no"))
                                mItemModel.setItem(mItemObj.getString("item_serial_no"));
                            if (!mItemObj.isNull("quantity"))
                                mItemModel.setQuantity(mItemObj.getInt("quantity"));
                            if (!mItemObj.isNull("price"))
                                mItemModel.setUnitprice(mItemObj.getString("price"));
                            if (!mItemObj.isNull("description"))
                                mItemModel.setDescription(mItemObj.getString("description"));
                            mInvoiceItemArrayList.add(mItemModel);
                        }
                        mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                    }
                    if (page_no == 1) {
                        modelArrayList.add(mModel);
                    } else if (page_no > 1) {
                        mLoadMore.add(mModel);
                    }
                }
                if (mLoadMore.size() > 0) {
                    modelArrayList.addAll(mLoadMore);
                }
                /*Set Adapter*/
                setAdapter();
            } else if (mJsonObject.getString("status").equals("100")) {
                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "*exception*" + e.toString());
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        inVoicesRV.setNestedScrollingEnabled(false);
        inVoicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAllDraftsInvoicesAdapter = new AllDraftsInvoicesAdapter(mActivity, modelArrayList,
                mEditInvoiceDraftInterface, mDeleteInvoiceDraftInterface);
        inVoicesRV.setAdapter(mAllDraftsInvoicesAdapter);
        mAllDraftsInvoicesAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (JaoharConstants.IS_INVOICE_BACK_FROM_DRAFT ) {
            finish();
        }

        if (modelArrayList != null) {
            modelArrayList.clear();
        }

        if (mLoadMore != null) {
            mLoadMore.clear();
        }

        /* execute API to list-out draft invoices */
        executeAPI();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (progressDialog.isShowing()) {
            hideProgressDialog();
        }
    }
}