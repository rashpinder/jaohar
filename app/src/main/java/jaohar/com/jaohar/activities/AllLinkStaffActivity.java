package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.LinkStaffAdapter;
import jaohar.com.jaohar.interfaces.DeleteLinkStaffInterface;
import jaohar.com.jaohar.interfaces.EditLinkStaffInterface;
import jaohar.com.jaohar.models.LinksToHomeData;
import jaohar.com.jaohar.models.LinksToHomeModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllLinkStaffActivity extends BaseActivity {
    String TAG = "AllLinkStaffActivity";
    Activity mActivity = AllLinkStaffActivity.this;

    //WIDGETS
    ImageView imgBack;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    ImageView imgRight;
    TextView txtCenter;
    RecyclerView linksRV;

    LinkStaffAdapter mAdapter;
    ArrayList<LinksToHomeData> mArrayList = new ArrayList<LinksToHomeData>();


    EditLinkStaffInterface mEditLinkStaffInterface = new EditLinkStaffInterface() {
        @Override
        public void mEditLinkAdimn(LinksToHomeData mModel) {
            Intent mIntent = new Intent(mActivity, EditLinkStaffActivity.class);
            mIntent.putExtra("Model", mModel);
            startActivity(mIntent);
        }
    };

    DeleteLinkStaffInterface mDeleteLinkStaffInterface = new DeleteLinkStaffInterface() {
        @Override
        public void mDeleteLinkAdmin(LinksToHomeData mModel) {
            deleteConfirmDialog(mModel);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_all_link_staff);
    }

    @Override
    protected void setViewsIDs() {
       /*SET UP TOOLBAR*/
        imgBack = (ImageView) findViewById(R.id.imgBack);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        linksRV = (RecyclerView) findViewById(R.id.linksRV);

        /* set tool bar */
        txtCenter.setText(getString(R.string.all_links_to_home));
        imgBack.setImageResource(R.drawable.back);
        imgRight.setImageResource(R.drawable.add_icon);

        if (!Utilities.isNetworkAvailable(mActivity)) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeGettingAllLinks();
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AddLinkStaffActivity.class));
                overridePendingTransitionEnter();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        linksRV.setNestedScrollingEnabled(false);
        linksRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new LinkStaffAdapter(mActivity, mArrayList, mEditLinkStaffInterface, mDeleteLinkStaffInterface);
        linksRV.setAdapter(mAdapter);
    }

    private void executeGettingAllLinks() {
        mArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<LinksToHomeModel> call1 = mApiInterface.getLinksToHomeRequest();
        call1.enqueue(new Callback<LinksToHomeModel>() {
            @Override
            public void onResponse(Call<LinksToHomeModel> call, retrofit2.Response<LinksToHomeModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                LinksToHomeModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mArrayList=mModel.getData();
                    /*setAdapter*/
                    setAdapter();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LinksToHomeModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}

//    private void executeGettingAllLinks() {
//        mArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strAPIUrl = JaoharConstants.ALL_LINK_STAFF;
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        JSONArray mArray = mJsonObject.getJSONArray("data");
//                        for (int i = 0; i < mArray.length(); i++) {
//                            JSONObject mJson = mArray.getJSONObject(i);
//                            LinkStaffModel mModel = new LinkStaffModel();
//                            mModel.setLink_id(mJson.getString("link_id"));
//                            mModel.setLink_name(mJson.getString("link_name"));
//                            mModel.setLink_value(mJson.getString("link_value"));
//
//                            mArrayList.add(mModel);
//                        }
//
//                        /*setAdapter*/
//                        setAdapter();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(mStringRequest);
//    }

    public void deleteConfirmDialog(final LinksToHomeData mModel) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_admin_link_staff));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                 /*Execute Delete API*/
                executeDeleteAPI(mModel);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }


    /*Execute Delete API*/
    private void executeDeleteAPI(LinksToHomeData mModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteStaffLinksToHomeRequest(mModel.getLinkId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeDeleteAPI(LinkStaffModel mModel) {
//        String strUrl = JaoharConstants.DELETE_LINK_STAFF + "?link_id=" + mModel.getLink_id();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialog(mActivity,getTitle().toString(),mJsonObject.getString("message"));
//                    }else if(mJsonObject.getString("status").equals("0")){
//                        showAlertDialog(mActivity,getTitle().toString(),mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    public  void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                executeGettingAllLinks();
            }
        });
        alertDialog.show();
    }
}
