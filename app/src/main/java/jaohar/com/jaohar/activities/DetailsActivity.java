package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;

import net.nightwhistler.htmlspanner.HtmlSpanner;

import java.io.File;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.DetailsDocumentsAdapter;
import jaohar.com.jaohar.adapters.DetailsPagerAdapter;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.interfaces.DownloadDocInterface;
import jaohar.com.jaohar.interfaces.PdfInterface;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.Remark;
import jaohar.com.jaohar.models.VesselDetailsModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class DetailsActivity extends BaseActivity {
    PdfInterface mPdfInterface = new PdfInterface() {
        @Override
        public void pdfInterface(String docPath) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(docPath));
            startActivity(browserIntent);
        }
    };
    /**
     * Getting the Current Class Name
     */
    private final String TAG = DetailsActivity.this.getClass().getSimpleName();
    private String strHyperLink = "";
    /**
     * Current Activity Instance
     */
    private final Activity mActivity = DetailsActivity.this;
    DownloadDocInterface mDownloadDocInterface = new DownloadDocInterface() {
        @Override
        public void mDownloadDocInterface(String link, String name) {
            showProgressDialog(mActivity);
            mDownloadPDFMethod(link, name);
        }
    };
    /**
     * Widgets
     */
    private LinearLayout llLeftLL, photoslink1LL, photoslink2LL, photoslink3LL, photoslink4LL;
    private TextView txtCenter;
    private ImageView imgRight;
    private TextView txtRight;
    private RelativeLayout imgRightLL;
    private ViewPager viewPagerVP;
    private TextView recordIDTV, imoNumberTV, shortDescriptionTV, vesselNameTV, vesselTypeTV, yearBuiltTV, placeBuiltTV,
            classTV, flagTV, txtGearedValueTV, mmsiNumberTV, OffMarketTV;
    private TextView dwtCapacityTV, loamTV, BreadthTV, DepthTV, priceIdeaTV, vesselLocationTV, grainTV, baleTV, dataEnteredTV,
            StatusTV, readMoreTXT, txtphotoslink11TV, txtphotoslink2TV, txtphotoslink3TV, txtphotoslink4TV, DraugthTV,
            buiderTV, GrossTonnageTV, NetTonnageTV, teuTV;
    private TextView txtStatusTV, passangersTV, liquidTV, hyperLinkTV, gasTV, lightshipTV, tcmTV;
    private DetailsPagerAdapter mDetailsPagerAdapter;
    private RecyclerView mDocumentRecyclerView;
    private DetailsDocumentsAdapter mDetailsDocumentsAdapter;
    private final ArrayList<String> mImagesArrayList = new ArrayList<String>();
    private final ArrayList<Remark> mArryList = new ArrayList<>();
    private final ArrayList<DocumentModel> mDocumentsArrayList = new ArrayList<DocumentModel>();
    private Button btnEnquiry;
    private AllVessel mVesselesModel;
    private boolean isEditShow = false;
    private RelativeLayout rlEdior;
    private String strShow_vessel_in_marine_traffic, strShip_Info, strShipspotting_Photos, strVessel_Tracker_Position;
    private LinearLayout hideDocumentLL, EmptyRemarksLL;
    private WebView mRichTextView, RemarksTV, OwnerDetailsTV, MachineryDetailsTV;
    private boolean isReradMoreClick = false;
    private int count = 0;
    private String UserId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_details);
        PRDownloader.initialize(getApplicationContext());

    }

    private void mDownloadPDFMethod(String strLink, String strTitle) {
        downloadPDF(strLink, outputPath(), strTitle);
    }


    /* get path of pdf */
    private String outputPath() {
        //String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
//        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath()
                + "/jaohar");

        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        } else {
            folder.delete();
            folder.mkdirs();
        }
        return path;
    }


    private void downloadPDF(String url, String dirPath, String fileName) {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("PRDOWNLOADER", "Download completed at::::" + dirPath);
                        hideProgressDialog();
                        showPDFAlertDialog(mActivity, getResources().getString(R.string.app_name), "Downloaded Successfully!");
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("PRDOWNLOADER", "Download failed at::::" + error.getServerErrorMessage());
                        hideProgressDialog();
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), "PDF format is not correct!");
                    }
                });
    }


    /*
     * Show Alert Dailog Box
     * */
    public void showPDFAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    @Override
    protected void setViewsIDs() {
        if (getIntent() != null) {
            mVesselesModel = (AllVessel) getIntent().getSerializableExtra("Model");
            isEditShow = getIntent().getBooleanExtra("isEditShow", false);
        }

        /*Toolbar Left Layout*/
        photoslink1LL = findViewById(R.id.photoslink1LL);
        photoslink2LL = findViewById(R.id.photoslink2LL);
        photoslink3LL = findViewById(R.id.photoslink3LL);
        photoslink4LL = findViewById(R.id.photoslink4LL);

        llLeftLL = findViewById(R.id.llLeftLL);

        /*Toolbar Center Layout*/
        txtCenter = findViewById(R.id.txtCenter);
        if (mVesselesModel != null && mVesselesModel.getVesselName().length() > 0) {
            txtCenter.setText(mVesselesModel.getVesselName());
        }

        readMoreTXT = findViewById(R.id.readMoreTXT);
        txtphotoslink11TV = findViewById(R.id.txtphotoslink1TV);
        txtphotoslink2TV = findViewById(R.id.txtphotoslink2TV);
        txtphotoslink3TV = findViewById(R.id.txtphotoslink3TV);
        txtphotoslink4TV = findViewById(R.id.txtphotoslink4TV);
        buiderTV = findViewById(R.id.buiderTV);
        passangersTV = findViewById(R.id.passangersTV);
        teuTV = findViewById(R.id.teuTV);
        NetTonnageTV = findViewById(R.id.NetTonnageTV);
        GrossTonnageTV = findViewById(R.id.GrossTonnageTV);
        DraugthTV = findViewById(R.id.DraugthTV);
        liquidTV = findViewById(R.id.liquidTV);
        gasTV = findViewById(R.id.gasTV);
        lightshipTV = findViewById(R.id.lightshipTV);
        tcmTV = findViewById(R.id.tcmTV);
        hyperLinkTV = findViewById(R.id.hyperLinkTV);
        txtRight = findViewById(R.id.txtRight);

        /*Toolbar Right Layout*/
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight = findViewById(R.id.imgRight);
        imgRight.setVisibility(View.VISIBLE);
        txtRight.setVisibility(View.GONE);

        mmsiNumberTV = findViewById(R.id.mmsiNumberTV);
        OffMarketTV = findViewById(R.id.OffMarketTV);

        viewPagerVP = findViewById(R.id.viewPagerVP);
        txtStatusTV = findViewById(R.id.txtStatusTV);
        recordIDTV = findViewById(R.id.recordIDTV);
        imoNumberTV = findViewById(R.id.imoNumberTV);
        shortDescriptionTV = findViewById(R.id.shortDescriptionTV);
        vesselNameTV = findViewById(R.id.vesselNameTV);
        vesselTypeTV = findViewById(R.id.vesselTypeTV);
        yearBuiltTV = findViewById(R.id.yearBuiltTV);
        placeBuiltTV = findViewById(R.id.placeBuiltTV);
        flagTV = findViewById(R.id.flagTV);
        classTV = findViewById(R.id.classTV);
        flagTV = findViewById(R.id.flagTV);
        dwtCapacityTV = findViewById(R.id.dwtCapacityTV);
        loamTV = findViewById(R.id.loamTV);
        BreadthTV = findViewById(R.id.BreadthTV);
        DepthTV = findViewById(R.id.DepthTV);
        priceIdeaTV = findViewById(R.id.priceIdeaTV);
        vesselLocationTV = findViewById(R.id.vesselLocationTV);
        grainTV = findViewById(R.id.grainTV);
        baleTV = findViewById(R.id.baleTV);
        dataEnteredTV = findViewById(R.id.dataEnteredTV);
        StatusTV = findViewById(R.id.StatusTV);
        btnEnquiry = findViewById(R.id.btnEnquiry);
        EmptyRemarksLL = findViewById(R.id.EmptyRemarksLL);
        mRichTextView = findViewById(R.id.mRichTextView);

        RemarksTV = findViewById(R.id.RemarksTV);
        OwnerDetailsTV = findViewById(R.id.OwnerDetailsTV);
        MachineryDetailsTV = findViewById(R.id.MachineryDetailsTV);

        mDocumentRecyclerView = findViewById(R.id.mDocumentRecyclerView);
        hideDocumentLL = findViewById(R.id.hideDocumentLL);
        txtGearedValueTV = findViewById(R.id.txtGearedValueTV);

        rlEdior = findViewById(R.id.rlEdior);
        rlEdior.setEnabled(false);

        DetailsActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }


    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{"snp@jaohar.com"});
                email.putExtra(Intent.EXTRA_SUBJECT, mVesselesModel.getVesselName() + "/" + mVesselesModel.getRecordId());
                email.setPackage("com.google.android.gm");
                email.putExtra(Intent.EXTRA_TEXT, "");

                //need this to prompts email client only
                email.setType("message/rfc822");
                try {
                    startActivity(Intent.createChooser(email, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(DetailsActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        readMoreTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isReradMoreClick == true) {
                    isReradMoreClick = false;
                    readMoreTXT.setText("Read more...");
                    mRichTextView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                    mRichTextView.getLayoutParams().height = 130;
                    mRichTextView.requestLayout();
                } else {
                    readMoreTXT.setText("Read less...");
                    isReradMoreClick = true;
                    mRichTextView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                    mRichTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    mRichTextView.requestLayout();
                }

            }
        });

        hyperLinkTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!strHyperLink.equals("")) {
                    Intent intent = new Intent(mActivity, webViewActivity.class);
                    intent.putExtra("news", strHyperLink);
                    startActivity(intent);
                }

            }
        });

        photoslink1LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, webViewActivity.class);
                intent.putExtra("news", strShow_vessel_in_marine_traffic);
                intent.putExtra(JaoharConstants.TITLE, getResources().getString(R.string.show_vessel_in_marine_traffic));
                startActivity(intent);
            }
        });
        photoslink2LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, webViewActivity.class);
                intent.putExtra("news", strShip_Info);
                intent.putExtra(JaoharConstants.TITLE, getResources().getString(R.string.ship_info));
                startActivity(intent);

            }
        });
        photoslink3LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, webViewActivity.class);
                intent.putExtra("news", strShipspotting_Photos);
                intent.putExtra(JaoharConstants.TITLE, getResources().getString(R.string.shipspotting_photos));
                startActivity(intent);
            }
        });
        photoslink4LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, webViewActivity.class);
                intent.putExtra("news", strVessel_Tracker_Position);
                intent.putExtra(JaoharConstants.TITLE, getResources().getString(R.string.vessel_tracker_position));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (mDocumentsArrayList != null)
                mDocumentsArrayList.clear();

            executeAPI();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isEditShow == true) {
            if (JaoharConstants.Is_click_form_All_Vessals == true) {
                JaoharConstants.Is_click_form_All_Vessals = false;
                super.onBackPressed();
            } else if (JaoharConstants.Is_click_form_All_Vessals_Manager == true) {
                JaoharConstants.Is_click_form_All_Vessals_Manager = false;
                super.onBackPressed();
            } else {
                FragmentManager mFragmentManager = getSupportFragmentManager();
                mFragmentManager.popBackStack(JaoharConstants.SIGNUP_FRAGMENT, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mFragmentManager.popBackStack(JaoharConstants.LOGIN_FRAGMENT, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } else {
            super.onBackPressed();
        }
    }

    public void executeAPI() {
        if (JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "").length() > 0 && JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "").length() > 0) {
            UserId = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            if (JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "").length() > 0) {
                UserId = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
            } else if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "").length() > 0) {
                UserId = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
            }
        }

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<VesselDetailsModel> call1 = mApiInterface.getSingleVesselDetailRequest(mVesselesModel.getRecordId(), UserId);
        call1.enqueue(new Callback<VesselDetailsModel>() {
            @Override
            public void onResponse(Call<VesselDetailsModel> call, retrofit2.Response<VesselDetailsModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                VesselDetailsModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    strHyperLink = mModel.getData().getUrl();
                    System.out.print("HyperLink" + strHyperLink);
                    if (mModel.getData().getRemarks() != null) {
                        for (int i = 0; i < mArryList.size(); i++) {
                            if (UserId.contains(mArryList.get(i).getUserId())) {
                                String remarks = mArryList.get(i).getRemark();
                                EmptyRemarksLL.setVisibility(View.GONE);
                            } else {
                                EmptyRemarksLL.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        EmptyRemarksLL.setVisibility(View.VISIBLE);
                    }
                    /*Set Data On Widgets/Documents*/
                    setDataonWidgets(mVesselesModel);

                    if (mModel.getLinks().getShowVesselInMarineTraffic() != null) {
                        strShow_vessel_in_marine_traffic = mModel.getLinks().getShowVesselInMarineTraffic();
                        txtphotoslink11TV.setText(R.string.show_vessel_in_marine_traffic);
                    }
                    if (mModel.getLinks().getShipInfo() != null) {
                        strShip_Info = mModel.getLinks().getShipInfo();
                        txtphotoslink2TV.setText(R.string.ship_info);
                    }
                    if (mModel.getLinks().getShipspottingPhotos() != null) {
                        strShipspotting_Photos = mModel.getLinks().getShipspottingPhotos();
                        txtphotoslink3TV.setText(R.string.shipspotting_photos);
                    }
                    if (mModel.getLinks().getShipspottingPhotos() != null) {
                        strVessel_Tracker_Position = mModel.getLinks().getShipspottingPhotos();
                        txtphotoslink4TV.setText(R.string.vessel_tracker_position);
                    }

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<VesselDetailsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    private void setDataonWidgets(AllVessel mVesselesModel12) {

        /*Set Images*/
        setViewPagerAdapter(mVesselesModel12);

        /*Set PDF And Its Name*/
        setPDFdataIntoArrayList(mVesselesModel12);

        if (mVesselesModel12.getStatus().equals("Sold")) {
            txtStatusTV.setText(getString(R.string.note) + " " + mVesselesModel12.getStatus() + ".");
            txtStatusTV.setVisibility(View.VISIBLE);
        } else if (mVesselesModel12.getStatus().equals("Withdrawn")) {
            txtStatusTV.setText(getString(R.string.note) + " " + mVesselesModel12.getStatus() + ".");
            txtStatusTV.setVisibility(View.VISIBLE);
        } else if (mVesselesModel12.getStatus().equals("Commited")) {
            txtStatusTV.setText(getString(R.string.note) + " " + mVesselesModel12.getStatus() + ".");
            txtStatusTV.setVisibility(View.VISIBLE);
        } else {
            txtStatusTV.setVisibility(View.GONE);
        }

        String strCapacity = mVesselesModel12.getCapacity();

        if (strCapacity.contains(".")) {
            String strDWT2 = "000";
            DecimalFormat df = new DecimalFormat("#.###");
            Double f1 = Double.parseDouble(strCapacity);
            String strNumDWT = df.format(f1);
            String[] separated = strNumDWT.split("\\.");
            String strDWT = separated[0];// this will contain "Fruit"
            if (strNumDWT.contains(".")) {
                strDWT2 = separated[1];
            }

            long numberGrain = Long.parseLong(strDWT);
            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
            dwtCapacityTV.setText(strNuumGrain + "." + strDWT2);


        } else {
            String strDWT = mVesselesModel12.getCapacity().trim();

            if (strDWT.matches("[0-9]+") && strDWT.length() > 2) {
                long numberGrossTonage = Long.parseLong(strDWT);
                System.out.println("KESHAV2222=====" + numberGrossTonage);
                String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                dwtCapacityTV.setText(strNumGrossTonage);
            } else {
                dwtCapacityTV.setText(strDWT);
            }
        }

        if (mVesselesModel12.getGrain().contains(".")) {
            DecimalFormat df = new DecimalFormat("#.###");
            Double f1 = Double.parseDouble(mVesselesModel12.getGrain());
            df.setRoundingMode(RoundingMode.DOWN);
            String strNumDWT = df.format(f1);
            String[] separated = strNumDWT.split("\\.");
            String strDWT = separated[0]; // this will contain "Fruit"
            String strDWT2 = separated[1];
            long numberGrain = Long.parseLong(strDWT);
            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

            grainTV.setText(strNuumGrain + "." + strDWT2);
        } else {
            String strDWT = mVesselesModel12.getGrain();
            System.out.println("KESHAV=====" + strDWT);

            if (strDWT.matches("[0-9]+") && strDWT.length() > 2) {
                long numberGrossTonage = Long.parseLong(strDWT);
                System.out.println("KESHAV2=====" + numberGrossTonage);
                String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                grainTV.setText(strNumGrossTonage);
            } else {
                grainTV.setText(strDWT);
            }
        }

        if (mVesselesModel12.getBale().contains(".")) {
            DecimalFormat df = new DecimalFormat("#.###");
            Double f1 = Double.parseDouble(mVesselesModel12.getBale());
            df.setRoundingMode(RoundingMode.DOWN);
            String strNumDWT = df.format(f1);
            String[] separated = strNumDWT.split("\\.");
            String strDWT = separated[0]; // this will contain "Fruit"
            String strDWT2 = separated[1];
            long numberGrain = Long.parseLong(strDWT);
            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

            baleTV.setText(strNuumGrain + "." + strDWT2);
        } else {
            String strBale = mVesselesModel12.getBale();

            if (strBale.matches("[0-9]+") && strBale.length() > 2) {
                long numberNetTonage = Long.parseLong(mVesselesModel12.getBale());
                String strNumNetTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberNetTonage);
                baleTV.setText(strNumNetTonage);
            } else {
                baleTV.setText(strBale);
            }
        }

        if (mVesselesModel12.getNetTonnage().contains(".")) {
            DecimalFormat df = new DecimalFormat("#.###");
            Double f1 = Double.parseDouble(mVesselesModel12.getNetTonnage());
            df.setRoundingMode(RoundingMode.DOWN);
            String strNumDWT = df.format(f1);
            String[] separated = strNumDWT.split("\\.");
            String strDWT = separated[0]; // this will contain "Fruit"
            String strDWT2 = separated[1];
            long numberGrain = Integer.parseInt(strDWT);
            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

            NetTonnageTV.setText(strNuumGrain + "." + strDWT2);
        } else {

            String strNetTonnage = mVesselesModel12.getNetTonnage();

            if (strNetTonnage.matches("[0-9]+") && strNetTonnage.length() > 2) {
                long numberNetTonage = Long.parseLong(mVesselesModel12.getNetTonnage());
                String strNumNetTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberNetTonage);
                NetTonnageTV.setText(strNumNetTonage);
            } else {
                NetTonnageTV.setText(strNetTonnage);
            }
        }

        if (mVesselesModel12.getGrossTonnage().contains(".")) {
            DecimalFormat df = new DecimalFormat("#.###");
            Double f1 = Double.parseDouble(mVesselesModel12.getGrossTonnage());
            df.setRoundingMode(RoundingMode.DOWN);
            String strNumDWT = df.format(f1);
            String[] separated = strNumDWT.split("\\.");
            String strDWT = separated[0]; // this will contain "Fruit"
            String strDWT2 = separated[1];
            long numberGrain = Long.parseLong(strDWT);
            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
            GrossTonnageTV.setText(strNuumGrain + "." + strDWT2);
        } else {

            String strGrossTonnage = mVesselesModel12.getGrossTonnage();

            if (strGrossTonnage.matches("[0-9]+") && strGrossTonnage.length() > 2) {
                long numberGrossTonage = Long.parseLong(mVesselesModel12.getGrossTonnage());
                String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                GrossTonnageTV.setText(strNumGrossTonage);
            } else {
                GrossTonnageTV.setText(strGrossTonnage);
            }
        }

        teuTV.setText(mVesselesModel12.getTeu());
        passangersTV.setText(mVesselesModel12.getNoPassengers());
        liquidTV.setText(mVesselesModel12.getLiquid());
        if (!mVesselesModel12.getUrl().equals("")) {
            hyperLinkTV.setText("Click here..");
        } else {
            hyperLinkTV.setText(" ");
        }
//        hyperLinkTV.setText(mVesselesModel.getUrl());

        gasTV.setText(mVesselesModel12.getGas());
        String str = String.valueOf((new HtmlSpanner()).fromHtml(mVesselesModel12.getFullDescription()));
        count = str.length();
        System.out.println("Output=================" + count);
        if (count >= 0 && count <= 30) {
            readMoreTXT.setVisibility(View.GONE);
        } else {
            readMoreTXT.setVisibility(View.VISIBLE);
            isReradMoreClick = false;
            readMoreTXT.setText("Read more...");
            mRichTextView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            mRichTextView.getLayoutParams().height = 130;
            mRichTextView.requestLayout();
        }
        mRichTextView.loadDataWithBaseURL(null, mVesselesModel12.getFullDescription(), "text/html", "UTF-8", null);
        mRichTextView.getSettings().setDefaultFontSize(13);
        dataEnteredTV.setText(mVesselesModel12.getDateForVessel());
        StatusTV.setText(mVesselesModel12.getStatus());
        recordIDTV.setText(mVesselesModel12.getRecordId());
        imoNumberTV.setText(mVesselesModel12.getIMONumber());
        shortDescriptionTV.setText(mVesselesModel12.getShortDescription());
        vesselNameTV.setText(mVesselesModel12.getVesselName());
        vesselTypeTV.setText(mVesselesModel12.getVesselType());
        yearBuiltTV.setText(mVesselesModel12.getYearBuilt());
        placeBuiltTV.setText(mVesselesModel12.getPlaceOfBuilt());
        classTV.setText(mVesselesModel12.getClass_());
        flagTV.setText(mVesselesModel12.getFlag());
        loamTV.setText(mVesselesModel12.getLoa());
        BreadthTV.setText(mVesselesModel12.getBreadth());
        DepthTV.setText(mVesselesModel12.getDepth());
        priceIdeaTV.setText(mVesselesModel12.getPriceIdea() + " " + mVesselesModel12.getCurrency());
        vesselLocationTV.setText(mVesselesModel12.getVesselLocation());
        String strGeared = mVesselesModel12.getGeared();
        System.out.println("getGeared=========" + strGeared);
        txtGearedValueTV.setText(mVesselesModel12.getGeared());
        buiderTV.setText(mVesselesModel12.getBuilder());
        DraugthTV.setText(mVesselesModel12.getDraught());

        mmsiNumberTV.setText(mVesselesModel12.getMmsiNo());
        OffMarketTV.setText(mVesselesModel12.getOffMarket());

        if (mVesselesModel12.getLdt() != null && !mVesselesModel12.getLdt().equals("")) {
            lightshipTV.setText(mVesselesModel12.getLdt());
        } else {
            lightshipTV.setText("0");
        }

        if (mVesselesModel12.getTcm() != null && !mVesselesModel12.getTcm().equals("")) {
            tcmTV.setText(mVesselesModel12.getTcm());
        } else {
            tcmTV.setText("0");
        }

        if (mVesselesModel12.getMachineryDetail() != null && !mVesselesModel12.getMachineryDetail().equals("")) {
            MachineryDetailsTV.loadDataWithBaseURL(null, mVesselesModel12.getMachineryDetail(), "text/html", "UTF-8", null);
            MachineryDetailsTV.getSettings().setDefaultFontSize(14);
        }
        if (mVesselesModel12.getOwnerDetail() != null && !mVesselesModel12.getOwnerDetail().equals("")) {
            OwnerDetailsTV.loadDataWithBaseURL(null, mVesselesModel12.getOwnerDetail(), "text/html", "UTF-8", null);
            OwnerDetailsTV.getSettings().setDefaultFontSize(13);
        }
        if (mVesselesModel12.getRemarks() != null && !mVesselesModel12.getRemarks().equals("") && mVesselesModel12.getRemarks().size() > 0) {
            RemarksTV.loadDataWithBaseURL(null, mVesselesModel12.getRemarks().get(0).getRemark(), "text/html", "UTF-8", null);
            RemarksTV.getSettings().setDefaultFontSize(13);
            RemarksTV.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            RemarksTV.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            RemarksTV.requestLayout();
        }

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.setPackage("com.whatsapp");
                    shareIntent.putExtra("Intent.EXTRA_SUBJECT", "Jaohar");
                    String shareMessage = "\nSee this Video\n";
                    shareMessage = "https://play.google.com/store/apps/details?id=jaohar.com.jaohar&hl=en_IN&gl=US";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
            }
        });
    }

    private void setPDFdataIntoArrayList(AllVessel mVesselesModel123) {
        mDocumentsArrayList.clear();
        if (mVesselesModel123.getDocument1() != null) {
            if (mVesselesModel123.getDocument1().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument1name());
                mModel.setDocumentPath(mVesselesModel123.getDocument1());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument2() != null) {
            if (mVesselesModel123.getDocument2().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument2name());
                mModel.setDocumentPath(mVesselesModel123.getDocument2());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument3() != null) {
            if (mVesselesModel123.getDocument3().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument3name());
                mModel.setDocumentPath(mVesselesModel123.getDocument3());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument4() != null) {
            if (mVesselesModel123.getDocument4().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument4name());
                mModel.setDocumentPath(mVesselesModel123.getDocument4());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument5() != null) {
            if (mVesselesModel123.getDocument5().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument5name());
                mModel.setDocumentPath(mVesselesModel123.getDocument5());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument6() != null) {
            if (mVesselesModel123.getDocument6().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument6name());
                mModel.setDocumentPath(mVesselesModel123.getDocument6());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument7() != null) {
            if (mVesselesModel123.getDocument7().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument7name());
                mModel.setDocumentPath(mVesselesModel123.getDocument7());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument8() != null) {
            if (mVesselesModel123.getDocument8().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument8name());
                mModel.setDocumentPath(mVesselesModel123.getDocument8());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument9() != null) {
            if (mVesselesModel123.getDocument9().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument9name());
                mModel.setDocumentPath(mVesselesModel123.getDocument9());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument10() != null) {
            if (mVesselesModel123.getDocument10().length() > 0) {
                DocumentModel mModel = new DocumentModel();
                mModel.setDocumentName(mVesselesModel123.getDocument10name());
                mModel.setDocumentPath(mVesselesModel123.getDocument10());
                mDocumentsArrayList.add(mModel);
            }
        }
        if (mVesselesModel123.getDocument11().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument11name());
            mModel.setDocumentPath(mVesselesModel123.getDocument11());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument12().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument12name());
            mModel.setDocumentPath(mVesselesModel123.getDocument12());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument13().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument13name());
            mModel.setDocumentPath(mVesselesModel123.getDocument13());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument14().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument14name());
            mModel.setDocumentPath(mVesselesModel123.getDocument14());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument15().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument15name());
            mModel.setDocumentPath(mVesselesModel123.getDocument15());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument16().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument16name());
            mModel.setDocumentPath(mVesselesModel123.getDocument16());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument17().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument17name());
            mModel.setDocumentPath(mVesselesModel123.getDocument17());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument18().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument18name());
            mModel.setDocumentPath(mVesselesModel123.getDocument18());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument19().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument19name());
            mModel.setDocumentPath(mVesselesModel123.getDocument19());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument20().length() > 0) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument20name());
            mModel.setDocumentPath(mVesselesModel123.getDocument20());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument21().length() > 0 && mVesselesModel123.getDocument21().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument21name());
            mModel.setDocumentPath(mVesselesModel123.getDocument21());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument22().length() > 0 && mVesselesModel123.getDocument22().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument22name());
            mModel.setDocumentPath(mVesselesModel123.getDocument22());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument23().length() > 0 && mVesselesModel123.getDocument23().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument23name());
            mModel.setDocumentPath(mVesselesModel123.getDocument23());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument24().length() > 0 && mVesselesModel123.getDocument24().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument24name());
            mModel.setDocumentPath(mVesselesModel123.getDocument24());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument25().length() > 0 && mVesselesModel123.getDocument25().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument25name());
            mModel.setDocumentPath(mVesselesModel123.getDocument25());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument26().length() > 0 && mVesselesModel123.getDocument26().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument26name());
            mModel.setDocumentPath(mVesselesModel123.getDocument26());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument27().length() > 0 && mVesselesModel123.getDocument27().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument27name());
            mModel.setDocumentPath(mVesselesModel123.getDocument27());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument28().length() > 0 && mVesselesModel123.getDocument28().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument28name());
            mModel.setDocumentPath(mVesselesModel123.getDocument28());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument29().length() > 0 && mVesselesModel123.getDocument29().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument29name());
            mModel.setDocumentPath(mVesselesModel123.getDocument29());
            mDocumentsArrayList.add(mModel);
        }
        if (mVesselesModel123.getDocument30().length() > 0 && mVesselesModel123.getDocument30().contains("http")) {
            DocumentModel mModel = new DocumentModel();
            mModel.setDocumentName(mVesselesModel123.getDocument30name());
            mModel.setDocumentPath(mVesselesModel123.getDocument30());
            mDocumentsArrayList.add(mModel);
        }

        setDocumentDetailAdapter();

        if (mDocumentsArrayList.size() > 0) {
            hideDocumentLL.setVisibility(View.GONE);
            mDocumentRecyclerView.setVisibility(View.VISIBLE);
        } else {
            hideDocumentLL.setVisibility(View.VISIBLE);
            mDocumentRecyclerView.setVisibility(View.GONE);
        }
    }

    private void setViewPagerAdapter(AllVessel mVesselesModel1234) {
        mImagesArrayList.clear();
        if (mVesselesModel1234.getPhoto1().length() > 0 && mVesselesModel1234.getPhoto1().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto1());
        }
        if (mVesselesModel1234.getPhoto2().length() > 0 && mVesselesModel1234.getPhoto2().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto2());
        }
        if (mVesselesModel1234.getPhoto3().length() > 0 && mVesselesModel1234.getPhoto3().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto3());
        }
        if (mVesselesModel1234.getPhoto4().length() > 0 && mVesselesModel1234.getPhoto4().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto4());
        }
        if (mVesselesModel1234.getPhoto5().length() > 0 && mVesselesModel1234.getPhoto5().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto5());
        }
        if (mVesselesModel1234.getPhoto6().length() > 0 && mVesselesModel1234.getPhoto6().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto6());
        }
        if (mVesselesModel1234.getPhoto7().length() > 0 && mVesselesModel1234.getPhoto7().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto7());
        }
        if (mVesselesModel1234.getPhoto8().length() > 0 && mVesselesModel1234.getPhoto8().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto8());
        }
        if (mVesselesModel1234.getPhoto9().length() > 0 && mVesselesModel1234.getPhoto9().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto9());
        }
        if (mVesselesModel1234.getPhoto10().length() > 0 && mVesselesModel1234.getPhoto10().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto10());
        }
        if (mVesselesModel1234.getPhoto11().length() > 0 && mVesselesModel1234.getPhoto11().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto11());
        }
        if (mVesselesModel1234.getPhoto12().length() > 0 && mVesselesModel1234.getPhoto12().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto12());
        }
        if (mVesselesModel1234.getPhoto13().length() > 0 && mVesselesModel1234.getPhoto13().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto13());
        }
        if (mVesselesModel1234.getPhoto14().length() > 0 && mVesselesModel1234.getPhoto14().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto14());
        }
        if (mVesselesModel1234.getPhoto15().length() > 0 && mVesselesModel1234.getPhoto15().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto15());
        }
        if (mVesselesModel1234.getPhoto16().length() > 0 && mVesselesModel1234.getPhoto16().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto16());
        }
        if (mVesselesModel1234.getPhoto17().length() > 0 && mVesselesModel1234.getPhoto17().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto17());
        }
        if (mVesselesModel1234.getPhoto18().length() > 0 && mVesselesModel1234.getPhoto18().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto18());
        }
        if (mVesselesModel1234.getPhoto19().length() > 0 && mVesselesModel1234.getPhoto19().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto19());
        }
        if (mVesselesModel1234.getPhoto20().length() > 0 && mVesselesModel1234.getPhoto20().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto20());
        }
        if (mVesselesModel1234.getPhoto21().length() > 0 && mVesselesModel1234.getPhoto21().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto21());
        }
        if (mVesselesModel1234.getPhoto22().length() > 0 && mVesselesModel1234.getPhoto22().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto22());
        }
        if (mVesselesModel1234.getPhoto23().length() > 0 && mVesselesModel1234.getPhoto23().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto23());
        }
        if (mVesselesModel1234.getPhoto24().length() > 0 && mVesselesModel1234.getPhoto24().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto24());
        }
        if (mVesselesModel1234.getPhoto25().length() > 0 && mVesselesModel1234.getPhoto25().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto25());
        }
        if (mVesselesModel1234.getPhoto26().length() > 0 && mVesselesModel1234.getPhoto26().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto26());
        }
        if (mVesselesModel1234.getPhoto27().length() > 0 && mVesselesModel1234.getPhoto27().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto27());
        }
        if (mVesselesModel1234.getPhoto28().length() > 0 && mVesselesModel1234.getPhoto28().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto28());
        }
        if (mVesselesModel1234.getPhoto29().length() > 0 && mVesselesModel1234.getPhoto29().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto29());
        }
        if (mVesselesModel1234.getPhoto30().length() > 0 && mVesselesModel1234.getPhoto30().contains("http")) {
            mImagesArrayList.add(mVesselesModel1234.getPhoto30());
        }

        mDetailsPagerAdapter = new DetailsPagerAdapter(mActivity, mImagesArrayList);
        Log.e(TAG, "******SIZE*****" + mImagesArrayList.size());
        viewPagerVP.setAdapter(mDetailsPagerAdapter);
    }

    /*Set Document Adapter*/
    private void setDocumentDetailAdapter() {
        mDocumentRecyclerView.setNestedScrollingEnabled(false);
        mDocumentRecyclerView.setLayoutManager(new LinearLayoutManager(DetailsActivity.this, RecyclerView.HORIZONTAL, false));
        mDetailsDocumentsAdapter = new DetailsDocumentsAdapter(mActivity, mDocumentsArrayList, mDownloadDocInterface, mPdfInterface);
        mDocumentRecyclerView.setAdapter(mDetailsDocumentsAdapter);
    }
}
