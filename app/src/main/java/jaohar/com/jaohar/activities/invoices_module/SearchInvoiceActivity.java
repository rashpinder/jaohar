package jaohar.com.jaohar.activities.invoices_module;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.EditInvoiceActivity;
import jaohar.com.jaohar.activities.SendingInvoiceEmailActivity;
import jaohar.com.jaohar.adapters.InVoicesAdapter;
import jaohar.com.jaohar.adapters.invoices_module.SearchPendingPaymentsAdapter;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.pendingPayments.SearchAllSearchedInvoice;
import jaohar.com.jaohar.models.pendingPayments.SearchPendingPaymentModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchInvoiceActivity extends BaseActivity {
    private static final int page_no = 1;
    public static String search_back = "false";
    public final int REQUEST_PERMISSIONS = 1;
    /**
     * Initialize the Activity
     */
    private final Activity mActivity = SearchInvoiceActivity.this;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    /*
     * Getting the Class Name
     * */
    String TAG = SearchInvoiceActivity.this.getClass().getSimpleName();
    /**
     * Widgets
     */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.cancelTV)
    TextView cancelTV;
    @BindView(R.id.ad_search_tv)
    TextView ad_search_tv;
    @BindView(R.id.invoicesRV)
    RecyclerView invoicesRV;
    ArrayList<InVoicesModel> modelArrayList = new ArrayList<InVoicesModel>();
    ArrayList<InVoicesModel> mLoadMore = new ArrayList<InVoicesModel>();
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    ArrayList<SearchAllSearchedInvoice> searchAllSearchedInvoices = new ArrayList<>();
    InVoicesAdapter mInVoicesAdapter;
    SearchPendingPaymentsAdapter searchPendingPaymentsAdapter;
    ArrayList<String> mArrayListStamps = new ArrayList<String>();
    ArrayList<String> mArrayListCompanys = new ArrayList<String>();
    ArrayList<String> mArrayListBankDetails = new ArrayList<String>();
    ArrayList<String> vesselArrayList = new ArrayList<String>();
    ArrayList<String> mArrayListCurrency = new ArrayList<String>();
    ArrayList<String> mArrayListSignDATA = new ArrayList<String>();
    ArrayList<String> mInvoiceID = new ArrayList<String>();
    boolean isNormalSearch = false, isAdvanceSearch = false;
    String strInvoiceNum, strInvoiceDateFrom, strInvoiceDateTo, strInvoiceCompany, strInvoiceVessel, strInvoiceCurrency, strInvoiceStamp, strInvoiceSign, strInvoiceStatus, strInvoiceBank;
    Intent intent;
    String[] arrayStatus;
    SendMultipleInvoiceMAIlInterface mMultimaIlInterface = new SendMultipleInvoiceMAIlInterface() {
        @Override
        public void mSendMutliInvoice(InVoicesModel mInVoivce, boolean mDelete) {
        }
    };
    DeleteInvoiceInterface mDeleteInvoiceInterface = new DeleteInvoiceInterface() {
        @Override
        public void deleteInvoice(InVoicesModel mInVoicesModel) {
            deleteConfirmDialog(mInVoicesModel);
        }
    };
    SinglemailInvoiceInterface msinglemailInvoiceInterface = new SinglemailInvoiceInterface() {
        @Override
        public void mSinglemailInvoice(InVoicesModel mInvoiceModel) {
            strInvoiceNumber = "JAORO" + mInvoiceModel.getInvoice_number();
            strInvoiceVesselName = "   " + mInvoiceModel.getmVesselSearchInvoiceModel().getVessel_name();
            /*Send Email*/
            Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
            mIntent.putExtra("SubjectName", "Invoice    " + strInvoiceNumber + strInvoiceVesselName);
            mIntent.putExtra("vesselID", mInvoiceModel.getInvoice_id());
            startActivity(mIntent);
        }
    };


    String mPDF = "", mPDFName = "";
    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            if (from.equalsIgnoreCase("all_invoices")) {
                PerformOptionsClick(modelArrayList.get(position).getPdf(), modelArrayList.get(position).getPdf_name(), position);
            } else {

                if (searchAllSearchedInvoices.get(position).getAllData().getAllDocs().size() == 0) {
                    PerformOptionsPPClick(position, "", "");
                } else {
                    PerformOptionsPPClick(position, searchAllSearchedInvoices.get(position).getAllData().getAllDocs().get(0) + "", "");
                }

//                PerformOptionsPPClick(position, /*modelArrayList.get(position).getPdf()*/"", /*modelArrayList.get(position).getPdf_name()*/"");

            }

        }
    };
    private String strLastPage = "FALSE", strInvoiceNumber, strInvoiceVesselName, from = "";
    private DownloadManager downloadManager;
    private long downloadReference;
    PDFdownloadInterface mPdfDownloader = new PDFdownloadInterface() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void pdfDownloadInterface(final String strPDF, final String strPDFNAme) {
            if (!strPDF.equals("")) {
                // your code
                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(strPDF);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(strPDFNAme);
                //Set a description of this download, to be displayed in notifications (if enabled)
                request.setDescription("" + "Invoice PDF is Download Please Wait...");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(mActivity, Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), "Their is no PDF");
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (search_back.equals("true")) {
            search_back = "false";
            onBackPressed();
        }
    }


    private void PerformOptionsPPClick(int position, String strPDF, String strPDFNAme) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_pending_payments_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IS_Click_From_Edit = true;
                JaoharConstants.IS_Click_From_Copy = false;
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.Invoice_ID = searchAllSearchedInvoices.get(position).getAllData().getPpId();
                Intent mIntent = new Intent(SearchInvoiceActivity.this, AddPendingPaymentsActivity.class);
                mIntent.putExtra("id", searchAllSearchedInvoices.get(position).getAllData().getPpId());
                mIntent.putExtra("pp_number", searchAllSearchedInvoices.get(position).getAllData().getPpNo());
                mIntent.putExtra("company_id", searchAllSearchedInvoices.get(position).getAllData().getPpCompany());
                mIntent.putExtra("pp_date", searchAllSearchedInvoices.get(position).getAllData().getPpDate());
                mIntent.putExtra("companyName", searchAllSearchedInvoices.get(position).getPpCompanyDetail().getCompanyName());
                mIntent.putExtra("vessel1", searchAllSearchedInvoices.get(position).getPpVessel1Detail().getVesselName());
                mIntent.putExtra("vessel2", searchAllSearchedInvoices.get(position).getPpVessel2Detail().getVesselName());
                mIntent.putExtra("vessel3", searchAllSearchedInvoices.get(position).getPpVessel3Detail().getVesselName());
                mIntent.putExtra("vessel1_id", searchAllSearchedInvoices.get(position).getAllData().getPpVessel1());
                mIntent.putExtra("vessel2_id", searchAllSearchedInvoices.get(position).getAllData().getPpVessel2());
                mIntent.putExtra("vessel3_id", searchAllSearchedInvoices.get(position).getAllData().getPpVessel3());
                mIntent.putExtra("amt_due", searchAllSearchedInvoices.get(position).getAllData().getPpAmtDue());
                mIntent.putExtra("remarks", searchAllSearchedInvoices.get(position).getAllData().getPpRemarks());
                mIntent.putExtra("currency", searchAllSearchedInvoices.get(position).getAllData().getPpCurrency());
                mIntent.putExtra("status", searchAllSearchedInvoices.get(position).getAllData().getPpStatus());
                mIntent.putExtra("from", "search_edit");
                mIntent.putStringArrayListExtra("docs", searchAllSearchedInvoices.get(position).getAllData().getAllDocs());
                startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                JaoharConstants.IS_Click_From_DRAFT = false;
//                JaoharConstants.IS_Click_From_Edit = false;
//                JaoharConstants.IS_Click_From_Copy = true;
//                JaoharConstants.Invoice_ID = modelArrayList.get(position).getInvoice_id();
//                Intent mIntent = new Intent(getActivity(), CopyInvoiceActivity.class);
//                mIntent.putExtra("Model", modelArrayList.get(position));
//                startActivity(mIntent);

                Intent mIntent = new Intent(SearchInvoiceActivity.this, PendingPaymentDetailsActivity.class);
//                mIntent.putExtra("Model", modelArrayList.get(position));
                mIntent.putExtra("id", searchAllSearchedInvoices.get(position).getAllData().getPpId());
                mIntent.putExtra("pp_number", searchAllSearchedInvoices.get(position).getAllData().getPpNo());
                mIntent.putExtra("company_id", searchAllSearchedInvoices.get(position).getAllData().getPpCompany());
                mIntent.putExtra("pp_date", searchAllSearchedInvoices.get(position).getAllData().getPpDate());
                mIntent.putExtra("companyName", searchAllSearchedInvoices.get(position).getPpCompanyDetail().getCompanyName());
                mIntent.putExtra("vessel1", searchAllSearchedInvoices.get(position).getPpVessel1Detail().getVesselName());
                mIntent.putExtra("vessel2", searchAllSearchedInvoices.get(position).getPpVessel2Detail().getVesselName());
                mIntent.putExtra("vessel3", searchAllSearchedInvoices.get(position).getPpVessel3Detail().getVesselName());
                mIntent.putExtra("vessel1_id", searchAllSearchedInvoices.get(position).getAllData().getPpVessel1());
                mIntent.putExtra("vessel2_id", searchAllSearchedInvoices.get(position).getAllData().getPpVessel2());
                mIntent.putExtra("vessel3_id", searchAllSearchedInvoices.get(position).getAllData().getPpVessel3());
                mIntent.putExtra("amt_due", searchAllSearchedInvoices.get(position).getAllData().getPpAmtDue());
                mIntent.putExtra("remarks", searchAllSearchedInvoices.get(position).getAllData().getPpRemarks());
                mIntent.putExtra("currency", searchAllSearchedInvoices.get(position).getAllData().getPpCurrency());
                mIntent.putExtra("status", searchAllSearchedInvoices.get(position).getAllData().getPpStatus());
                mIntent.putExtra("position", String.valueOf(position));
                mIntent.putStringArrayListExtra("docs", searchAllSearchedInvoices.get(position).getAllData().getAllDocs());
                startActivity(mIntent);
            }
        });


        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(SearchInvoiceActivity.this, InvoiceEmailActivity.class);
                mIntent.putExtra("SubjectName", "Invoice    " + searchAllSearchedInvoices.get(position).getAllData().getPpNo() + " " + searchAllSearchedInvoices.get(position).getPpVessel1Detail().getVesselName());
                mIntent.putExtra("vesselID", searchAllSearchedInvoices.get(position).getAllData().getPpId());
//                mIntent.putExtra("Model", modelArrayList.get(position));
                mIntent.putExtra("from", from);
                startActivity(mIntent);
            }
        });

        downloadRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*dialog.dismiss();
                if (!strPDF.equals("")) {
                    executePDFWithPermission(String.valueOf(position));
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), "Their is no PDF");
                }*/

                Log.e("DownaloadbaleLink", JaoharConstants.DEV_SERVER_URL + searchAllSearchedInvoices.get(position).getAllData().getPpId());
                String url = JaoharConstants.DEV_SERVER_URL + searchAllSearchedInvoices.get(position).getAllData().getPpId();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteConfirmSearchDialog(searchAllSearchedInvoices.get(position));
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void executePDFWithPermission(String position) {
        if (checkPermission()) {
            mDownloadPDFMethod(mPDF, mPDFName, position);
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(SearchInvoiceActivity.this, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS);
    }

    private String outputPath() {
//        String path =
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                        .toString();
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath()
                + "/jaohar");
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        } else {
            folder.delete();
            folder.mkdirs();
        }
        return path;
    }


    private void mDownloadPDFMethod(String mPDF, String mPDFName, String position) {
        // write the document content
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            new AsyncCaller().execute();
//        } else {
        showProgressDialog(SearchInvoiceActivity.this);
        downloadPDF(mPDF, outputPath(), mPDFName, position);
//        }

        // your code
//        downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
//        Uri Download_Uri = Uri.parse(mPDF);
//        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
//        //Restrict the types of networks over which this download may proceed.
//        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
//        //Set whether this download may proceed over a roaming connection.
//        request.setAllowedOverRoaming(false);
//        //Set the title of this download, to be displayed in notifications (if enabled).
//        request.setTitle(mPDFName);
//        //Set a description of this download, to be displayed in notifications (if enabled)
//        request.setDescription("" + "Invoice PDF is Download Please Wait...");
//        //Set the local destination for the downloaded file to a path within the application's external files directory
//        request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
//        //Enqueue a new download and same the referenceId
//        downloadReference = downloadManager.enqueue(request);
    }


    private void downloadPDF(String url, String dirPath, String fileName, String position) {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("PRDOWNLOADER", "Download completed at::::" + dirPath);
                        hideProgressDialog();
                        showPDFAlertDialog(SearchInvoiceActivity.this, getResources().getString(R.string.app_name), "Downloaded Successfully!", position);
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("PRDOWNLOADER", "Download failed at::::" + dirPath);
                        hideProgressDialog();
                        showAlertDialog(SearchInvoiceActivity.this, getResources().getString(R.string.app_name), "PDF format is not correct!");
                    }
                });
    }


    /*
     * Show Alert Dailog Box
     * */
    public void showPDFAlertDialog(Activity mActivity, String strTitle, String strMessage, String position) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
//                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
//                mIntent.putExtra("SubjectName", "Invoice    " +  modelArrayList.get(position).getInvoice_number() +" "+modelArrayList.get(position).getmVesselSearchInvoiceModel().getVessel_name());
//                mIntent.putExtra("vesselID", modelArrayList.get(position).getInvoice_id());
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        JaoharConstants.Invoice_ID = modelArrayList.get(Integer.parseInt(position)).getAllData().getPpId();
//                        mIntent.putExtra("Model", modelArrayList.get(Integer.parseInt(position)));
//                        startActivity(mIntent);
//                    }
//                },500);

//                mIntent.putExtra("Model", modelArrayList);

            }
        });
        alertDialog.show();
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_invoice);
        intent = getIntent();
        if (intent.hasExtra("from")) {
            from = intent.getStringExtra("from");
        }
        setStatusBar();

        ButterKnife.bind(this);

        addSearchMethod();
    }

    private void addSearchMethod() {
        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (from.equalsIgnoreCase("all_invoices")) {
                        executeNormalSearch();
                    } else {
                        executePPNormalSearch();
                    }

                    return true;
                }
                return false;
            }
        });

        ad_search_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (from.equalsIgnoreCase("all_invoices")) {
                    executeStampSAPI();
                    advancedSearchView();
                } else {
                    executeCompaniesAPI();
                    advancedSearchPPView();
                }
            }
        });

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void executePPNormalSearch() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        Call<SearchPendingPaymentModel> call1 = mApiInterface.searchPendingPaymentRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), editSearchET.getText().toString());
        call1.enqueue(new Callback<SearchPendingPaymentModel>() {
            @Override
            public void onResponse(Call<SearchPendingPaymentModel> call, retrofit2.Response<SearchPendingPaymentModel> response) {
                AlertDialogManager.hideProgressDialog();
                isNormalSearch = true;
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        searchAllSearchedInvoices = response.body().getAllSearchedInvoices();

                        invoicesRV.setNestedScrollingEnabled(false);
                        invoicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
                        searchPendingPaymentsAdapter = new SearchPendingPaymentsAdapter(mActivity, searchAllSearchedInvoices, mDeleteInvoiceInterface,
                                mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface, mOnClickInterface, from);
                        invoicesRV.setAdapter(searchPendingPaymentsAdapter);

                    } else if (response.body().getStatus().equalsIgnoreCase("0")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchPendingPaymentModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    public void executeNormalSearch() {
        modelArrayList.clear();
//        resetRL1.setVisibility(View.VISIBLE);
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.searchInvoiceRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), editSearchET.getText().toString(), "staff");
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
//                editSearchET.setText("");
                isNormalSearch = true;
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject111 = new JSONObject(response.body().toString());
                    if (mJsonObject111.getString("status").equals("1")) {
                        AlertDialogManager.hideProgressDialog();
                        JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_invoices");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            InVoicesModel mModel = new InVoicesModel();
                            JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                            if (!mAllDataObj.getString("pdf").equals("")) {
                                mModel.setPdf(mAllDataObj.getString("pdf"));
                            }
                            if (!mAllDataObj.getString("pdf_name").equals("")) {
                                mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                            }
                            if (!mAllDataObj.isNull("invoice_id"))
                                mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                            if (!mAllDataObj.isNull("invoice_no"))
                                mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                            if (!mAllDataObj.isNull("invoice_date"))
                                mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                            if (!mAllDataObj.isNull("term_days"))
                                mModel.setTerm_days(mAllDataObj.getString("term_days"));
                            if (!mAllDataObj.isNull("currency"))
                                mModel.setCurrency(mAllDataObj.getString("currency"));
                            if (!mAllDataObj.isNull("status"))
                                mModel.setStatus(mAllDataObj.getString("status"));
                            if (!mAllDataObj.isNull("reference"))
                                mModel.setRefrence1(mAllDataObj.getString("reference"));
                            if (!mAllDataObj.isNull("reference1"))
                                mModel.setRefrence2(mAllDataObj.getString("reference1"));
                            if (!mAllDataObj.isNull("reference2"))
                                mModel.setRefrence3(mAllDataObj.getString("reference2"));
                            if (!mAllDataObj.isNull("payment_id"))
                                mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                            if (!mAllDataObj.isNull("inv_state"))
                                mModel.setInv_state(mAllDataObj.getString("inv_state"));

                            if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                                JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                                SignatureModel mSignModel = new SignatureModel();
                                if (!mSignDataObj.isNull("sign_id"))
                                    mSignModel.setId(mSignDataObj.getString("sign_id"));
                                if (!mSignDataObj.isNull("sign_name"))
                                    mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                                if (!mSignDataObj.isNull("sign_image"))
                                    mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                                mModel.setmSignatureModel(mSignModel);
                            }
                            if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                                JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                                StampsModel mStampsModel = new StampsModel();
                                if (!mStampDataObj.isNull("stamp_id"))
                                    mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                                if (!mStampDataObj.isNull("stamp_name"))
                                    mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                                if (!mStampDataObj.isNull("stamp_image"))
                                    mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                                mModel.setmStampsModel(mStampsModel);
                            }
                            if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                                JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                                BankModel mBankModel = new BankModel();
                                if (!mBankDataObj.isNull("bank_id"))
                                    mBankModel.setId(mBankDataObj.getString("bank_id"));
                                if (!mBankDataObj.isNull("beneficiary"))
                                    mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                                if (!mBankDataObj.isNull("bank_name"))
                                    mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                                if (!mBankDataObj.isNull("address1"))
                                    mBankModel.setAddress1(mBankDataObj.getString("address1"));
                                if (!mBankDataObj.isNull("address2"))
                                    mBankModel.setAddress2(mBankDataObj.getString("address2"));
                                if (!mBankDataObj.isNull("iban_ron"))
                                    mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                                if (!mBankDataObj.isNull("iban_usd"))
                                    mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                                if (!mBankDataObj.isNull("iban_eur"))
                                    mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                                if (!mBankDataObj.isNull("swift"))
                                    mBankModel.setSwift(mBankDataObj.getString("swift"));
                                mModel.setmBankModel(mBankModel);
                            }
                            if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                                JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                                VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                                if (!mSearchVesselObj.isNull("vessel_id"))
                                    mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                                if (!mSearchVesselObj.isNull("vessel_name"))
                                    mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                                if (!mSearchVesselObj.isNull("IMO_no"))
                                    mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                                if (!mSearchVesselObj.isNull("flag"))
                                    mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                                mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                            }
                            if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                                JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                                CompaniesModel mCompaniesModel = new CompaniesModel();
                                if (!mSearchCompanyObj.isNull("id"))
                                    mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                                if (!mSearchCompanyObj.isNull("company_name"))
                                    mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                                if (!mSearchCompanyObj.isNull("Address1"))
                                    mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                                if (!mSearchCompanyObj.isNull("Address2"))
                                    mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                                if (!mSearchCompanyObj.isNull("Address3"))
                                    mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                                if (!mSearchCompanyObj.isNull("Address4"))
                                    mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                                if (!mSearchCompanyObj.isNull("Address5"))
                                    mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                                mModel.setmCompaniesModel(mCompaniesModel);
                            }
                            if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                                JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                                PaymentModel mPaymentModel = new PaymentModel();
                                if (!mPaymentObject.isNull("payment_id")) {
                                    mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                                }
                                if (!mPaymentObject.isNull("sub_total")) {
                                    mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                                }
                                if (!mPaymentObject.isNull("VAT")) {
                                    mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                                }
                                if (!mPaymentObject.isNull("vat_price")) {
                                    mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                                }
                                if (!mPaymentObject.isNull("total")) {
                                    mPaymentModel.setTotal(mPaymentObject.getString("total"));
                                }
                                if (!mPaymentObject.isNull("paid")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                                }
                                if (!mPaymentObject.isNull("due")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("due"));
                                }
                                mModel.setmPaymentModel(mPaymentModel);
                            }
                            if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                                JSONArray mItemArray = mJson.getJSONArray("items_data");
                                for (int k = 0; k < mItemArray.length(); k++) {
                                    JSONObject mItemObj = mItemArray.getJSONObject(k);
                                    InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                                    if (!mItemObj.isNull("item_id"))
                                        mItemModel.setItemID(mItemObj.getString("item_id"));
                                    if (!mItemObj.isNull("item_serial_no"))
                                        mItemModel.setItem(mItemObj.getString("item_serial_no"));
                                    if (!mItemObj.isNull("quantity"))
                                        mItemModel.setQuantity(mItemObj.getInt("quantity"));
                                    if (!mItemObj.isNull("price"))
                                        mItemModel.setUnitprice(mItemObj.getString("price"));
                                    if (!mItemObj.isNull("description"))
                                        mItemModel.setDescription(mItemObj.getString("description"));
                                    mInvoiceItemArrayList.add(mItemModel);
                                }
                                mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                            }

                            modelArrayList.add(mModel);
                        }

                        /*Set Adapter*/
                        setAdapter();

                    } else if (mJsonObject111.getString("status").equals("100")) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    } else {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    private void parseResponse(String response) {
        mLoadMore.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            if (mJsonObject.getString("status").equals("1")) {
                JSONObject mDataObject = mJsonObject.getJSONObject("data");
                JSONArray mJsonArray = mDataObject.getJSONArray("all_invoices");
                strLastPage = mDataObject.getString("last_page");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    InVoicesModel mModel = new InVoicesModel();
                    JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                    if (!mAllDataObj.isNull("invoice_id"))
                        mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                    if (!mAllDataObj.getString("pdf").equals("")) {
                        mModel.setPdf(mAllDataObj.getString("pdf"));
                    }
                    if (!mAllDataObj.getString("pdf_name").equals("")) {
                        mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                    }
                    if (!mAllDataObj.isNull("invoice_no"))
                        mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                    if (!mAllDataObj.isNull("invoice_date"))
                        mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                    if (!mAllDataObj.isNull("term_days"))
                        mModel.setTerm_days(mAllDataObj.getString("term_days"));
                    if (!mAllDataObj.isNull("currency"))
                        mModel.setCurrency(mAllDataObj.getString("currency"));
                    if (!mAllDataObj.isNull("status"))
                        mModel.setStatus(mAllDataObj.getString("status"));
                    if (!mAllDataObj.isNull("reference"))
                        mModel.setRefrence1(mAllDataObj.getString("reference"));
                    if (!mAllDataObj.isNull("reference1"))
                        mModel.setRefrence2(mAllDataObj.getString("reference1"));
                    if (!mAllDataObj.isNull("reference2"))
                        mModel.setRefrence3(mAllDataObj.getString("reference2"));
                    if (!mAllDataObj.isNull("payment_id"))
                        mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                    if (!mAllDataObj.isNull("inv_state"))
                        mModel.setInv_state(mAllDataObj.getString("inv_state"));

                    if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                        JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                        SignatureModel mSignModel = new SignatureModel();
                        if (!mSignDataObj.isNull("sign_id"))
                            mSignModel.setId(mSignDataObj.getString("sign_id"));
                        if (!mSignDataObj.isNull("sign_name"))
                            mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                        if (!mSignDataObj.isNull("sign_image"))
                            mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                        mModel.setmSignatureModel(mSignModel);
                    }
                    if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                        JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                        StampsModel mStampsModel = new StampsModel();
                        if (!mStampDataObj.isNull("stamp_id"))
                            mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                        if (!mStampDataObj.isNull("stamp_name"))
                            mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                        if (!mStampDataObj.isNull("stamp_image"))
                            mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                        mModel.setmStampsModel(mStampsModel);
                    }
                    if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                        JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                        BankModel mBankModel = new BankModel();
                        if (!mBankDataObj.isNull("bank_id"))
                            mBankModel.setId(mBankDataObj.getString("bank_id"));
                        if (!mBankDataObj.isNull("beneficiary"))
                            mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                        if (!mBankDataObj.isNull("bank_name"))
                            mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                        if (!mBankDataObj.isNull("address1"))
                            mBankModel.setAddress1(mBankDataObj.getString("address1"));
                        if (!mBankDataObj.isNull("address2"))
                            mBankModel.setAddress2(mBankDataObj.getString("address2"));
                        if (!mBankDataObj.isNull("iban_ron"))
                            mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                        if (!mBankDataObj.isNull("iban_usd"))
                            mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                        if (!mBankDataObj.isNull("iban_eur"))
                            mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                        if (!mBankDataObj.isNull("swift"))
                            mBankModel.setSwift(mBankDataObj.getString("swift"));
                        mModel.setmBankModel(mBankModel);
                    }
                    if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                        JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                        VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                        if (!mSearchVesselObj.isNull("vessel_id"))
                            mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                        if (!mSearchVesselObj.isNull("vessel_name"))
                            mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                        if (!mSearchVesselObj.isNull("IMO_no"))
                            mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                        if (!mSearchVesselObj.isNull("flag"))
                            mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                        mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                    }
                    if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                        JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                        CompaniesModel mCompaniesModel = new CompaniesModel();
                        if (!mSearchCompanyObj.isNull("id"))
                            mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                        if (!mSearchCompanyObj.isNull("company_name"))
                            mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                        if (!mSearchCompanyObj.isNull("Address1"))
                            mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                        if (!mSearchCompanyObj.isNull("Address2"))
                            mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                        if (!mSearchCompanyObj.isNull("Address3"))
                            mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                        if (!mSearchCompanyObj.isNull("Address4"))
                            mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                        if (!mSearchCompanyObj.isNull("Address5"))
                            mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                        mModel.setmCompaniesModel(mCompaniesModel);
                    }
                    if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                        JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                        PaymentModel mPaymentModel = new PaymentModel();
                        if (!mPaymentObject.isNull("payment_id")) {
                            mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                        }
                        if (!mPaymentObject.isNull("sub_total")) {
                            mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                        }
                        if (!mPaymentObject.isNull("VAT")) {
                            mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                        }
                        if (!mPaymentObject.isNull("vat_price")) {
                            mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                        }
                        if (!mPaymentObject.isNull("total")) {
                            mPaymentModel.setTotal(mPaymentObject.getString("total"));
                        }
                        if (!mPaymentObject.isNull("paid")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                        }
                        if (!mPaymentObject.isNull("due")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("due"));
                        }
                        mModel.setmPaymentModel(mPaymentModel);
                    }
                    if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                        JSONArray mItemArray = mJson.getJSONArray("items_data");
                        for (int k = 0; k < mItemArray.length(); k++) {
                            JSONObject mItemObj = mItemArray.getJSONObject(k);
                            InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                            if (!mItemObj.isNull("item_id"))
                                mItemModel.setItemID(mItemObj.getString("item_id"));
                            if (!mItemObj.isNull("item_serial_no"))
                                mItemModel.setItem(mItemObj.getString("item_serial_no"));
                            if (!mItemObj.isNull("quantity"))
                                mItemModel.setQuantity(mItemObj.getInt("quantity"));
                            if (!mItemObj.isNull("price"))
                                mItemModel.setUnitprice(mItemObj.getString("price"));
                            if (!mItemObj.isNull("description"))
                                mItemModel.setDescription(mItemObj.getString("description"));
                            mInvoiceItemArrayList.add(mItemModel);
                        }
                        mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                    }
                    if (page_no == 1) {
                        modelArrayList.add(mModel);
                    } else if (page_no > 1) {
                        mLoadMore.add(mModel);
                    }
                }
                if (mLoadMore.size() > 0) {
                    modelArrayList.addAll(mLoadMore);
                }
                /*Set Adapter*/
                setAdapter();
            } else if (mJsonObject.getString("status").equals("100")) {
                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "*exception*" + e);
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        invoicesRV.setNestedScrollingEnabled(false);
        invoicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mInVoicesAdapter = new InVoicesAdapter(mActivity, modelArrayList, mDeleteInvoiceInterface,
                mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface, mOnClickInterface);
        invoicesRV.setAdapter(mInVoicesAdapter);
    }

    private void PerformOptionsClick(String strPDF, String pdf_name, final int position) {
        mPDF = strPDF;
        mPDFName = pdf_name;
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_invoice_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IS_Click_From_Edit = true;
                JaoharConstants.IS_Click_From_Copy = false;
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.Invoice_ID = modelArrayList.get(position).getInvoice_id();
                Intent mIntent = new Intent(SearchInvoiceActivity.this, EditInvoiceActivity.class);
                mIntent.putExtra("Model", modelArrayList.get(position));
                startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IS_Click_From_DRAFT = false;
                JaoharConstants.IS_Click_From_Edit = false;
                JaoharConstants.IS_Click_From_Copy = true;
                JaoharConstants.Invoice_ID = modelArrayList.get(position).getInvoice_id();
                Intent mIntent = new Intent(SearchInvoiceActivity.this, CopyInvoiceActivity.class);
                mIntent.putExtra("Model", modelArrayList.get(position));
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(SearchInvoiceActivity.this, InvoiceEmailActivity.class);
                mIntent.putExtra("SubjectName", "Invoice    " + modelArrayList.get(position).getInvoice_number() + " " + modelArrayList.get(position).getmVesselSearchInvoiceModel().getVessel_name());
                mIntent.putExtra("vesselID", modelArrayList.get(position).getInvoice_id());
                mIntent.putExtra("Model", modelArrayList.get(position));
                mIntent.putExtra("from", "all_invoices");
                startActivity(mIntent);
            }
        });

        downloadRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!strPDF.equals("")) {
                    executePDFWithPermission(String.valueOf(position));
                } else {
                    AlertDialogManager.showAlertDialog(SearchInvoiceActivity.this, getResources().getString(R.string.app_name), "Their is no PDF");
                }
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteConfirmDialog(modelArrayList.get(position));
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void deleteConfirmDialog(final InVoicesModel mInVoicesModel) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice));
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mInVoicesModel);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    public void deleteConfirmSearchDialog(final SearchAllSearchedInvoice searchAllSearchedInvoice) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice));
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeSearchPPDeleteAPI(searchAllSearchedInvoice);
            }
        });


        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    public void executeDeleteAPI(InVoicesModel searchAllSearchedInvoice) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteInvoiceRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), searchAllSearchedInvoice.getInvoice_id());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    modelArrayList.clear();
//                    executeAPI();
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


    public void executeSearchPPDeleteAPI(SearchAllSearchedInvoice mInVoicesModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deletePPRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), mInVoicesModel.getAllData().getPpId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    modelArrayList.clear();
//                    executeAPI();
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeStampSAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAlStampsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                parseResponse1(response.body().toString());
                executeBankDetailAPI();


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse1(String response) {
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    StampsModel mModel = new StampsModel();
                    if (!mJson.isNull("stamp_id"))
                        mModel.setId(mJson.getString("stamp_id"));
                    if (!mJson.isNull("stamp_image"))
                        mModel.setStamp_image(mJson.getString("stamp_image"));
                    if (!mJson.isNull("stamp_name"))
                        mModel.setStamp_name(mJson.getString("stamp_name"));
                    mArrayListStamps.add(mJson.getString("stamp_name"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeCompaniesAPI() {
        mArrayListCompanys.clear();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAlCompaniesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                parseResponse12(response.body().toString());
                executeGetAllVesselsAPI();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse12(String response) {

        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CompaniesModel mModel = new CompaniesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("company_name")) {
                        mModel.setCompany_name(mJson.getString("company_name"));
                        mArrayListCompanys.add(mJson.getString("company_name"));
                    }
                    if (!mJson.isNull("Address1")) {
                        mModel.setAddress1(mJson.getString("Address1"));
                    }
                    if (!mJson.isNull("Address2")) {
                        mModel.setAddress2(mJson.getString("Address2"));
                    }
                    if (!mJson.isNull("Address3")) {
                        mModel.setAddress3(mJson.getString("Address3"));
                    }
                    if (!mJson.isNull("Address4")) {
                        mModel.setAddress4(mJson.getString("Address4"));
                    }
                    if (!mJson.isNull("Address5")) {
                        mModel.setAddress5(mJson.getString("Address5"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeBankDetailAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBankDetailsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "******Response*****" + response);
                JaoharConstants.IS_BANK_DETAILS_EDIT = false;
                AlertDialogManager.hideProgressDialog();
                parseResponse13(response.body().toString());
                executeCompaniesAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse13(String response) {
        mArrayListBankDetails.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    BankModel mModel = new BankModel();
                    mModel.setId(mJson.getString("bank_id"));
                    mModel.setBenificiary(mJson.getString("beneficiary"));
                    mModel.setBankName(mJson.getString("bank_name"));
                    mArrayListBankDetails.add(mJson.getString("bank_name"));
                    mModel.setAddress1(mJson.getString("address1"));
                    mModel.setAddress2(mJson.getString("address2"));
                    mModel.setIbanRON(mJson.getString("iban_ron"));
                    mModel.setIbanUSD(mJson.getString("iban_usd"));
                    mModel.setIbanEUR(mJson.getString("iban_eur"));
                    mModel.setSwift(mJson.getString("swift"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeGetAllVesselsAPI() {
        vesselArrayList.clear();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceVesselsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            VesselSearchInvoiceModel mModel = new VesselSearchInvoiceModel();
                            mModel.setVessel_id(mJson.getString("vessel_id"));
                            mModel.setVessel_name(mJson.getString("vessel_name"));
                            vesselArrayList.add(mJson.getString("vessel_name"));
                            mModel.setIMO_no(mJson.getString("IMO_no"));
                            mModel.setFlag(mJson.getString("flag"));
                        }
                        /*Set Adapter*/
                        executeCurrencyAPI();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeCurrencyAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllCurrenciesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();

                JaoharConstants.IS_CURRENCY_EDIT = false;
                parseResponse14(response.body().toString());
                if (from.equalsIgnoreCase("all_invoices")) {
                    executeSignDataAPI();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse14(String response) {
        mArrayListCurrency.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CurrenciesModel mModel = new CurrenciesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("currency_name")) {
                        mModel.setCurrency_name(mJson.getString("currency_name"));
                        mArrayListCurrency.add(mJson.getString("currency_name"));
                    }
                    if (!mJson.isNull("alias_name")) {
                        mModel.setAlias_name(mJson.getString("alias_name"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeSignDataAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllSignsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();

                Log.e(TAG, "******Response*****" + response);

                JaoharConstants.IS_SIGNATURE_EDIT = false;
                AlertDialogManager.hideProgressDialog();
                parseResponse15(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse15(String response) {
        mArrayListSignDATA.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    SignatureModel mModel = new SignatureModel();
                    if (!mJson.isNull("sign_id"))
                        mModel.setId(mJson.getString("sign_id"));
                    if (!mJson.isNull("sign_name"))
                        mModel.setSignature_name(mJson.getString("sign_name"));
                    mArrayListSignDATA.add(mJson.getString("sign_name"));
                    if (!mJson.isNull("sign_image"))
                        mModel.setSignature_image(mJson.getString("sign_image"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public void advancedSearchView() {
        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advance_search_invoice);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editInvoiceET = searchDialog.findViewById(R.id.editInvoiceET);
        final EditText editDateFromET = searchDialog.findViewById(R.id.editDateFromET);
        final EditText editDateToET = searchDialog.findViewById(R.id.editDateToET);
        final EditText editCompanyET = searchDialog.findViewById(R.id.editCompanyET);
        final EditText editVesselET = searchDialog.findViewById(R.id.editVesselET);
        final EditText editCurrencyET = searchDialog.findViewById(R.id.editCurrencyET);
        final EditText editStampET = searchDialog.findViewById(R.id.editStampET);
        final EditText editSignET = searchDialog.findViewById(R.id.editSignET);
        final EditText editStatusET = searchDialog.findViewById(R.id.editStatusET);
        final EditText editBankET = searchDialog.findViewById(R.id.editBankET);
        final EditText editNameET = searchDialog.findViewById(R.id.editNameET);

        TextView txtCenter = searchDialog.findViewById(R.id.txtCenter);

        arrayStatus = getResources().getStringArray(R.array.status_array);
        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });
        editDateFromET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    int mYear, mMonth, mDay, mHour, mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;
//                                  String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateFromET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });

        editDateToET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final int mYear;
                    final int mMonth;
                    final int mDay;
                    int mHour;
                    int mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;

//                          String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateToET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });


        editStampET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Stamps");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListStamps);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editStampET.setText(mArrayListStamps.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editCompanyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Company");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCompanys);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editCompanyET.setText(mArrayListCompanys.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editBankET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Bank Details");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListBankDetails);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editBankET.setText(mArrayListBankDetails.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editVesselET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessels");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, vesselArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editVesselET.setText(vesselArrayList.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });
        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Currency");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCurrency);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editCurrencyET.setText(mArrayListCurrency.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });
        editSignET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent != null && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Signatures");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListSignDATA);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editSignET.setText(mArrayListSignDATA.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strInvoiceNum = editInvoiceET.getText().toString();
                strInvoiceDateFrom = editDateFromET.getText().toString();
                strInvoiceDateTo = editDateToET.getText().toString();
                strInvoiceCompany = editCompanyET.getText().toString();
                strInvoiceVessel = editVesselET.getText().toString();
                strInvoiceCurrency = editCurrencyET.getText().toString();
                strInvoiceStamp = editStampET.getText().toString();
                strInvoiceSign = editSignET.getText().toString();
                strInvoiceStatus = editStatusET.getText().toString();
                strInvoiceBank = editBankET.getText().toString();
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchAPI(searchDialog);
                }
            }
        });

        txtCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        searchDialog.show();
    }


    @SuppressLint("ClickableViewAccessibility")
    public void advancedSearchPPView() {
        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advance_search_pending_payments_invoice);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editInvoiceET = searchDialog.findViewById(R.id.editInvoiceET);
        final EditText editDateFromET = searchDialog.findViewById(R.id.editDateFromET);
        final EditText editCompanyET = searchDialog.findViewById(R.id.editCompanyET);
        final EditText editVesselET = searchDialog.findViewById(R.id.editVesselET);
        final EditText editCurrencyET = searchDialog.findViewById(R.id.editCurrencyET);
        final ImageView calenderImageView = searchDialog.findViewById(R.id.calenderImageView);

        TextView txtCenter = searchDialog.findViewById(R.id.txtCenter);

        arrayStatus = getResources().getStringArray(R.array.status_array);

        editDateFromET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    int mYear, mMonth, mDay, mHour, mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;
//                                  String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateFromET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });

        calenderImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    int mYear, mMonth, mDay, mHour, mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;
//                                  String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateFromET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });


        editCompanyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Company");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCompanys);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editCompanyET.setText(mArrayListCompanys.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });


        editVesselET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessels");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, vesselArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editVesselET.setText(vesselArrayList.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });
        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Currency");
                    ListView lstListView = categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCurrency);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            editCurrencyET.setText(mArrayListCurrency.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });


        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strInvoiceNum = editInvoiceET.getText().toString();
                strInvoiceDateFrom = editDateFromET.getText().toString();
                strInvoiceCompany = editCompanyET.getText().toString();
                strInvoiceVessel = editVesselET.getText().toString();
                strInvoiceCurrency = editCurrencyET.getText().toString();
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchPPAPI(searchDialog);
                }
            }
        });

        txtCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        searchDialog.show();
    }

    public void executeAdvanceSearchAPI(final Dialog mDialog) {
        modelArrayList.clear();
        editSearchET.setText("");
//        resetRL1.setVisibility(View.VISIBLE);
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.advancedSearchInvoiceRequest(strInvoiceNum, "staff", strInvoiceDateFrom, strInvoiceDateTo,
                strInvoiceCompany, strInvoiceVessel, strInvoiceCurrency, strInvoiceStamp, strInvoiceSign, strInvoiceStatus, strInvoiceBank, JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);

                if (response.body() != null)
                    try {
                        JSONObject mJsonObject111 = new JSONObject(response.body().toString());
                        if (mJsonObject111.getString("status").equals("1")) {
                            isAdvanceSearch = true;
                            JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_invoices");
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                JSONObject mJson = mJsonArray.getJSONObject(i);
                                InVoicesModel mModel = new InVoicesModel();
                                JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                                if (!mAllDataObj.isNull("invoice_id"))
                                    mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                                if (!mAllDataObj.getString("pdf").equals("")) {
                                    mModel.setPdf(mAllDataObj.getString("pdf"));
                                }
                                if (!mAllDataObj.getString("pdf_name").equals("")) {
                                    mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                                }
                                if (!mAllDataObj.isNull("invoice_no"))
                                    mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                                if (!mAllDataObj.isNull("invoice_date"))
                                    mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                                if (!mAllDataObj.isNull("term_days"))
                                    mModel.setTerm_days(mAllDataObj.getString("term_days"));
                                if (!mAllDataObj.isNull("currency"))
                                    mModel.setCurrency(mAllDataObj.getString("currency"));
                                if (!mAllDataObj.isNull("status"))
                                    mModel.setStatus(mAllDataObj.getString("status"));
                                if (!mAllDataObj.isNull("reference"))
                                    mModel.setRefrence1(mAllDataObj.getString("reference"));
                                if (!mAllDataObj.isNull("reference1"))
                                    mModel.setRefrence2(mAllDataObj.getString("reference1"));
                                if (!mAllDataObj.isNull("reference2"))
                                    mModel.setRefrence3(mAllDataObj.getString("reference2"));
                                if (!mAllDataObj.isNull("payment_id"))
                                    mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                                if (!mAllDataObj.isNull("inv_state"))
                                    mModel.setInv_state(mAllDataObj.getString("inv_state"));

                                if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                                    JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                                    SignatureModel mSignModel = new SignatureModel();
                                    if (!mSignDataObj.isNull("sign_id"))
                                        mSignModel.setId(mSignDataObj.getString("sign_id"));
                                    if (!mSignDataObj.isNull("sign_name"))
                                        mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                                    if (!mSignDataObj.isNull("sign_image"))
                                        mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                                    mModel.setmSignatureModel(mSignModel);
                                }
                                if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                                    JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                                    StampsModel mStampsModel = new StampsModel();
                                    if (!mStampDataObj.isNull("stamp_id"))
                                        mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                                    if (!mStampDataObj.isNull("stamp_name"))
                                        mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                                    if (!mStampDataObj.isNull("stamp_image"))
                                        mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                                    mModel.setmStampsModel(mStampsModel);
                                }
                                if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                                    JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                                    BankModel mBankModel = new BankModel();
                                    if (!mBankDataObj.isNull("bank_id"))
                                        mBankModel.setId(mBankDataObj.getString("bank_id"));
                                    if (!mBankDataObj.isNull("beneficiary"))
                                        mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                                    if (!mBankDataObj.isNull("bank_name"))
                                        mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                                    if (!mBankDataObj.isNull("address1"))
                                        mBankModel.setAddress1(mBankDataObj.getString("address1"));
                                    if (!mBankDataObj.isNull("address2"))
                                        mBankModel.setAddress2(mBankDataObj.getString("address2"));
                                    if (!mBankDataObj.isNull("iban_ron"))
                                        mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                                    if (!mBankDataObj.isNull("iban_usd"))
                                        mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                                    if (!mBankDataObj.isNull("iban_eur"))
                                        mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                                    if (!mBankDataObj.isNull("swift"))
                                        mBankModel.setSwift(mBankDataObj.getString("swift"));
                                    mModel.setmBankModel(mBankModel);
                                }
                                if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                                    JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                                    VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                                    if (!mSearchVesselObj.isNull("vessel_id"))
                                        mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                                    if (!mSearchVesselObj.isNull("vessel_name"))
                                        mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                                    if (!mSearchVesselObj.isNull("IMO_no"))
                                        mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                                    if (!mSearchVesselObj.isNull("flag"))
                                        mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                                    mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                                }
                                if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                                    JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                                    CompaniesModel mCompaniesModel = new CompaniesModel();
                                    if (!mSearchCompanyObj.isNull("id"))
                                        mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                                    if (!mSearchCompanyObj.isNull("company_name"))
                                        mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                                    if (!mSearchCompanyObj.isNull("Address1"))
                                        mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                                    if (!mSearchCompanyObj.isNull("Address2"))
                                        mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                                    if (!mSearchCompanyObj.isNull("Address3"))
                                        mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                                    if (!mSearchCompanyObj.isNull("Address4"))
                                        mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                                    if (!mSearchCompanyObj.isNull("Address5"))
                                        mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                                    mModel.setmCompaniesModel(mCompaniesModel);
                                }
                                if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                                    JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                                    PaymentModel mPaymentModel = new PaymentModel();
                                    if (!mPaymentObject.isNull("payment_id")) {
                                        mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                                    }
                                    if (!mPaymentObject.isNull("sub_total")) {
                                        mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                                    }
                                    if (!mPaymentObject.isNull("VAT")) {
                                        mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                                    }
                                    if (!mPaymentObject.isNull("vat_price")) {
                                        mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                                    }
                                    if (!mPaymentObject.isNull("total")) {
                                        mPaymentModel.setTotal(mPaymentObject.getString("total"));
                                    }
                                    if (!mPaymentObject.isNull("paid")) {
                                        mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                                    }
                                    if (!mPaymentObject.isNull("due")) {
                                        mPaymentModel.setPaid(mPaymentObject.getString("due"));
                                    }
                                    mModel.setmPaymentModel(mPaymentModel);
                                }
                                if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                                    JSONArray mItemArray = mJson.getJSONArray("items_data");
                                    for (int k = 0; k < mItemArray.length(); k++) {
                                        JSONObject mItemObj = mItemArray.getJSONObject(k);
                                        InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                                        if (!mItemObj.isNull("item_id"))
                                            mItemModel.setItemID(mItemObj.getString("item_id"));
                                        if (!mItemObj.isNull("item_serial_no"))
                                            mItemModel.setItem(mItemObj.getString("item_serial_no"));
                                        if (!mItemObj.isNull("quantity"))
                                            mItemModel.setQuantity(mItemObj.getInt("quantity"));
                                        if (!mItemObj.isNull("price"))
                                            mItemModel.setUnitprice(mItemObj.getString("price"));
                                        if (!mItemObj.isNull("description"))
                                            mItemModel.setDescription(mItemObj.getString("description"));
                                        mInvoiceItemArrayList.add(mItemModel);
                                    }
                                    mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                                }
                                modelArrayList.add(mModel);
                            }

                            /*Set Adapter*/
                            setAdapter();
                            mDialog.dismiss();
                        } else if (mJsonObject111.getString("status").equals("0")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                        } else if (mJsonObject111.getString("status").equals("100")) {
                            AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                            mDialog.dismiss();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                            mDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }


    public void executeAdvanceSearchPPAPI(final Dialog mDialog) {
        searchAllSearchedInvoices.clear();
        editSearchET.setText("");
//        resetRL1.setVisibility(View.VISIBLE);
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchPendingPaymentModel> call1 = mApiInterface.advancedSearchPPInvoiceRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),
                strInvoiceNum, strInvoiceCompany, strInvoiceCurrency, strInvoiceDateFrom, strInvoiceVessel);
        call1.enqueue(new Callback<SearchPendingPaymentModel>() {
            @Override
            public void onResponse(Call<SearchPendingPaymentModel> call, retrofit2.Response<SearchPendingPaymentModel> response) {
                AlertDialogManager.hideProgressDialog();
                mDialog.dismiss();
                Log.e(TAG, "*****Response****" + response);
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        isAdvanceSearch = true;
                        searchAllSearchedInvoices = response.body().getAllSearchedInvoices();

                        invoicesRV.setNestedScrollingEnabled(false);
                        invoicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
                        searchPendingPaymentsAdapter = new SearchPendingPaymentsAdapter(mActivity, searchAllSearchedInvoices, mDeleteInvoiceInterface,
                                mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface, mOnClickInterface, from);
                        invoicesRV.setAdapter(searchPendingPaymentsAdapter);
                        mDialog.dismiss();
                    } else if (response.body().getStatus().equals("100")) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + response.body().getMessage());
                    } else {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + response.body().getMessage());
                    }

                }
            }


            @Override
            public void onFailure(Call<SearchPendingPaymentModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }
}