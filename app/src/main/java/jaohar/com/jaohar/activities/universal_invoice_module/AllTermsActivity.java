package jaohar.com.jaohar.activities.universal_invoice_module;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.AllTermsAdapter;
import jaohar.com.jaohar.models.DataItem;
import jaohar.com.jaohar.models.GetUniversalTermsModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllTermsActivity extends BaseActivity {
    Activity mActivity = AllTermsActivity.this;
    String TAG = AllTermsActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    ImageView imgBack, imgRight;
    TextView txtCenter, txtRight;
    EditText editSearchET;

    //RecyclerView
    RecyclerView rvTermsRV;
    ArrayList<DataItem> mDataArrayList = new ArrayList<DataItem>();
    ArrayList<DataItem> filteredDataList = new ArrayList<DataItem>();
    GetUniversalTermsModel mArrayList;
    GetUniversalTermsModel filteredList;
// ArrayList<GetUniversalTermsModel> mArrayList = new ArrayList<GetUniversalTermsModel>();
//    ArrayList<GetUniversalTermsModel> filteredList = new ArrayList<GetUniversalTermsModel>();

    AllTermsAdapter mAllTermsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_terms);

        setViewsIDs();

        setClickListner();


    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (JaoharConstants.IS_BANK_DETAILS_EDIT)
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {

            executeAPI();
        }
    }

    protected void setViewsIDs() {
        editSearchET = findViewById(R.id.editSearchET);
        txtCenter = findViewById(R.id.txtCenter);
        txtRight = findViewById(R.id.txtRight);
        txtRight.setVisibility(View.GONE);
        txtCenter.setText("All Terms");
        llLeftLL = findViewById(R.id.llLeftLL);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        imgRight = findViewById(R.id.imgRight);
        imgRight.setImageResource(R.drawable.plus_symbol);
        rvTermsRV = findViewById(R.id.rvTermsRV);
    }

    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, AddAdditionalTermsActivity.class);
                startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                charSequence = charSequence.toString().toLowerCase();
                filteredDataList.clear();
//                for (int k = 0; k < mArrayList.size(); k++) {
//                    final String text = mArrayList.get(k).getData().get(k).getTermTitle().toLowerCase();
//                    if (text.contains(charSequence)) {
//                        filteredList.add(mArrayList.get(k));
//                    }
//                }

                for (int k = 0; k < mDataArrayList.size(); k++) {
                    final String text = mDataArrayList.get(k).getTermTitle().toLowerCase();
                    if (text.contains(charSequence)) {
                        filteredDataList.add(mDataArrayList.get(k));
                    }
                }

                rvTermsRV.setLayoutManager(new LinearLayoutManager(mActivity));
                mAllTermsAdapter = new AllTermsAdapter(mActivity, filteredList,filteredDataList);
                rvTermsRV.setAdapter(mAllTermsAdapter);
                mAllTermsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
//        Log.e(TAG, "Stamps: " + mArrayList.size());
        rvTermsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAllTermsAdapter = new AllTermsAdapter(mActivity, mArrayList, mDataArrayList);
        rvTermsRV.setAdapter(mAllTermsAdapter);
    }

    private void executeAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetUniversalTermsModel> call1 = mApiInterface.getUniversalTermRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<GetUniversalTermsModel>() {
            @Override
            public void onResponse(Call<GetUniversalTermsModel> call, retrofit2.Response<GetUniversalTermsModel> response) {
                Log.e(TAG, "******Response*****" + response);
                mDataArrayList.clear();
//                JaoharConstants.IS_BANK_DETAILS_EDIT = false;
//                AlertDialogManager.hideProgressDialog();
            GetUniversalTermsModel mModel=response.body();
                if (response.body().getStatus().equals("1")) {
                    mArrayList=mModel;
                    mDataArrayList.addAll(mModel.getData());
                    /*SetAdapter*/
                    setAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetUniversalTermsModel> call, Throwable t) {
//                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

}
