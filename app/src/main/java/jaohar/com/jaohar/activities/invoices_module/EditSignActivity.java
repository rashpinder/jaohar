package jaohar.com.jaohar.activities.invoices_module;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class EditSignActivity extends BaseActivity {
    Activity mActivity = EditSignActivity.this;
    String TAG = EditSignActivity.this.getClass().getSimpleName();
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    public static final int REQUEST_PERMISSION_CODE = 100;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private String cameraStr = Manifest.permission.CAMERA;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;


    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgSignatureIV)
    ImageView imgSignatureIV;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.btnSelectSignatureB)
    Button btnSelectSignatureB;
    @BindView(R.id.mSignaturePad)
    SignaturePad mSignaturePad;
    @BindView(R.id.editSignHolderNameET)
    EditText editSignHolderNameET;

    String strImageBase64 = "";
    Bitmap mSignatureBitmap = null;
    SignatureModel mSignatureModel;
    //globally
    private boolean isSignatured = false;
    boolean isSignPad = false;
    String mCurrentPhotoPath, mStoragePath = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sign);
        ButterKnife.bind(this);
//        if (getIntent() != null) {
//            mSignatureModel = (SignatureModel) getIntent().getSerializableExtra("Model");
//        }
//        setViews();
//        setClick();
    }


    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

//
//    protected void setViews() {
//        //Toolbar
//        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
//        imgBack = (ImageView) findViewById(R.id.imgBack);
//        imgBack.setImageResource(R.drawable.back);
//        txtCenter = (TextView) findViewById(R.id.txtCenter);
//        txtCenter.setText(getString(R.string.edit_signature));
//        //Signature Layout
//        mSignaturePad = (SignaturePad) findViewById(R.id.mSignaturePad);
//
//        imgSignatureIV = (ImageView) findViewById(R.id.imgSignatureIV);
//
//        btnSelectSignatureB = (Button) findViewById(R.id.btnSelectSignatureB);
//        editSignHolderNameET = (EditText) findViewById(R.id.editSignHolderNameET);
//
//        /*SetData OnWidgets*/
//        if (mSignatureModel != null)
//            setDataOnWidgets(mSignatureModel);
//
//    }

    private void setDataOnWidgets(SignatureModel mSignatureModel) {
        editSignHolderNameET.setText(mSignatureModel.getSignature_name());
        editSignHolderNameET.setSelection(mSignatureModel.getSignature_name().length());
        mSignaturePad.setVisibility(View.VISIBLE);
        imgSignatureIV.setVisibility(View.GONE);

        strImageBase64 =mSignatureModel.getSignature_image();
        if(!strImageBase64.equals("")){

            Glide.with(mActivity).asBitmap().load(mSignatureModel.getSignature_image()).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    // you can do something with loaded bitmap here
                    mSignaturePad.setSignatureBitmap(resource);
                }
            });
//            Glide.with(mActivity).load(mSignatureModel.getSignature_image()).into(mSignaturePad);

        }else {
            mSignaturePad.setVisibility(View.VISIBLE);
            imgSignatureIV.setVisibility(View.GONE);
        }


    }


    protected void setClick() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        imgRightLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mSignaturePad.setVisibility(View.VISIBLE);
//                imgSignatureIV.setVisibility(View.GONE);
//                mSignaturePad.clear();
//                strImageBase64="";
//                mSignatureBitmap=null;
//                isSignPad = true;
//            }
//        });
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                isSignatured = true;
            }

            @Override
            public void onSigned() {
                //Event triggered when the pad is signed
                isSignatured = true;
            }

            @Override
            public void onClear() {
                //Event triggered when the pad is cleared
                isSignatured = false;
            }
        });

//        btnEditSignatureB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (editSignHolderNameET.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_signature_holder_name));
//                } else if (strImageBase64.equals("")) {
////                    if (isSignPad){
//                    //in your on click
//                    if(isSignatured){
//                        mSignatureBitmap = mSignaturePad.getTransparentSignatureBitmap(true);
//                        executeEditSignatureAPI(mSignatureModel);
//                        //do next process
//                    }else{
//                        mSignaturePad.setVisibility(View.VISIBLE);
//                        imgSignatureIV.setVisibility(View.GONE);
//                        mSignaturePad.clear();
//                        strImageBase64="";
//                        mSignatureBitmap=null;
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_signature));
//                        //intimate to sign
//                    }
//
////                        executeEditSignatureAPI(mSignatureModel);
//
//
//                } else {
//                    if (isSignPad){
//                        mSignatureBitmap = mSignaturePad.getTransparentSignatureBitmap(true);
//                    }else{
//                        strImageBase64 = mSignatureModel.getSignature_image();
//
//                    }
//                    executeEditSignatureAPI(mSignatureModel);
//                    /*Execute Add Comapny API*/
//
//                }
//            }
//        });
//        btnSelectSignatureB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setUpCameraGalleryDialog();
//            }
//        });
//
    }

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }


    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            openCameraGalleryDialog();
//                            startCropImageActivity(mCropImageUri);
//                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
//                            finish();
//                            Toast.makeText(context,"on",Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();
//                            permissionAccepted = false;
                        }
                    }
                }
                break;
        }
    }

    public void setUpCameraGalleryDialog() {
        if (checkPermission()) {
            openCameraGalleryDialog();
        } else {
            requestPermission();

        }
    }


    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
//      intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//        overridePendingTransitionExit();
//    }

    private void executeEditSignatureAPI(SignatureModel mSignatureModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editSignRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    JaoharConstants.IS_SIGNATURE_EDIT = true;
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("sign_id", mSignatureModel.getId());
        mMap.put("sign_name", editSignHolderNameET.getText().toString());
        if (strImageBase64.length() > 10) {
            if(mSignatureBitmap!=null){
                mMap.put("sign_image", ImageUtils.getInstant().getBase64FromBitmap(mSignatureBitmap));
            }else if(mSignatureBitmap==null){
                mMap.put("sign_image", strImageBase64);
            }
        } else {
            if(mSignatureBitmap!=null){
                mMap.put("sign_image", ImageUtils.getInstant().getBase64FromBitmap(mSignatureBitmap));
            }
        }
        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


//    private void executeEditSignatureAPI(SignatureModel mSignatureModel) {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.EDIT_SIGN;
//        try {
//            jsonObject.put("sign_id", mSignatureModel.getId());
//            jsonObject.put("sign_name", editSignHolderNameET.getText().toString());
//            if (strImageBase64.length() > 10) {
//                if(mSignatureBitmap!=null){
//                    jsonObject.put("sign_image", ImageUtils.getInstant().getBase64FromBitmap(mSignatureBitmap));
//                }else if(mSignatureBitmap==null){
//                    jsonObject.put("sign_image", strImageBase64);
//                }
//
//            } else {
//                if(mSignatureBitmap!=null){
//                    jsonObject.put("sign_image", ImageUtils.getInstant().getBase64FromBitmap(mSignatureBitmap));
//                }
//
//            }
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    if (jsonObject.getString("status").equals("1")) {
//                        JaoharConstants.IS_SIGNATURE_EDIT = true;
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap thumb;
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            strImageBase64="";
                            Bitmap bitmap;
                            Uri uri = data.getData();
                            File finalFile = new File(getRealPathFromURI(uri));
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 8;
                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                            ExifInterface exifInterface = null;
                            try {
                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            Matrix matrix = new Matrix();
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                            }
                            mSignaturePad.clear();
                            JaoharConstants.IS_camera_Click = false;
                            thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                            mSignaturePad.setSignatureBitmap(thumb);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected****");
                        return;
                    }
                } finally {
                }
            }

            if (requestCode == CAMERA_REQUEST) {
                try {
                    strImageBase64="";
                    mSignaturePad.clear();
                    JaoharConstants.IS_camera_Click = false;
                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(mStoragePath, options);
                    Bitmap thumb1 = rotateImage(bitmap);
                    mSignaturePad.setSignatureBitmap(thumb);
//                    imgStampImageIV.setImageURI(uri);
                    //strImageBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (requestCode == 505) {
                strImageBase64="";
                mSignaturePad.clear();
                Bitmap bitmap;
                String strDocumentPath, strDocumentName;
                Uri uri = data.getData();
                strDocumentPath = uri.getPath();
                strDocumentPath = strDocumentPath.replace(" ", "_");
                strDocumentName = uri.getLastPathSegment();
                File finalFile = new File(getRealPathFromURI(uri));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//              For COnvert And rotate image
                ExifInterface exifInterface = null;
                try {
                    exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.setRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.setRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.setRotate(270);
                        break;
                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                }
                JaoharConstants.IS_camera_Click=false;
                thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                mSignaturePad.setSignatureBitmap(thumb);
            }
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    //       Method Correct Rotate Image when Capture From Camera....
    private Bitmap rotateImage(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }
}
