package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.fonts.ButtonPoppinsMedium;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class AddBankActivity extends BaseActivity {
    private final Activity mActivity = AddBankActivity.this;
    /**
     * Widgets
     */
    @BindView(R.id.beneficiaryNameET)
    EditText beneficiaryNameET;
    @BindView(R.id.editAddreLine1ET)
    EditText editAddreLine1ET;
    @BindView(R.id.editAddreLine2ET)
    EditText editAddreLine2ET;
    @BindView(R.id.beneficiaryAddressET)
    EditText beneficiaryAddressET;
    @BindView(R.id.editCountryET)
    EditText editCountryET;
    @BindView(R.id.editBankNameET)
    EditText editBankNameET;
    @BindView(R.id.editIBanRonET)
    EditText editIBanRonET;
    @BindView(R.id.editIBanUsdET)
    EditText editIBanUsdET;
    @BindView(R.id.editIBanEurET)
    EditText editIBanEurET;
    @BindView(R.id.editIBanGbpET)
    EditText editIBanGbpET;
    @BindView(R.id.editSwiftET)
    EditText editSwiftET;
    @BindView(R.id.savebtn)
    ButtonPoppinsMedium savebtn;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    private String strContactID = "", strUSERID = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank);
        ButterKnife.bind(this);
        if (JaoharConstants.is_Staff_FragmentClick == true) {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
    }


    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }}
