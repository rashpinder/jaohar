package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.fonts.ButtonPoppinsMedium;
import jaohar.com.jaohar.models.InvoiceModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddVesselActivity extends BaseActivity {
    Activity mActivity = AddVesselActivity.this;
    String TAG = AddVesselActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText vesselNameET, imoET, editFlagET;
    
    ButtonPoppinsMedium savebtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vessel2);
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        vesselNameET = (EditText) findViewById(R.id.vesselNameET);
        vesselNameET.requestFocus();
        imoET = (EditText) findViewById(R.id.imoET);
        editFlagET = (EditText) findViewById(R.id.editFlagET);
        savebtn = findViewById(R.id.savebtn);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vesselNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_vessel_name));
                } else if (imoET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_imo_num));
                } else if (editFlagET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_flag));
                } else {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeAddVessel();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void executeAddVessel() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<InvoiceModel> call1 = mApiInterface.addInvoiceVesselRequest(vesselNameET.getText().toString(),imoET.getText().toString(),editFlagET.getText().toString(),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, retrofit2.Response<InvoiceModel> response) {
                AlertDialogManager.hideProgressDialog();
                InvoiceModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IS_CURRENCY_EDIT = true;
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


    public static void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }


}
