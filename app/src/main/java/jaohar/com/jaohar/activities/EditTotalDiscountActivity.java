package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;

public class EditTotalDiscountActivity extends BaseActivity {
    Activity mActivity = EditTotalDiscountActivity.this;
    String TAG = EditTotalDiscountActivity.this.getClass().getSimpleName();
    TextView txtTotalTV, txtUnitPriceTV;
    LinearLayout llLeftLL;
    EditText AddDiscountValueTV, txtDescriptionTV;
    Button btnSubmitB;
    Spinner txtSpinnerTV;
    ArrayList<String> mdiscountTypeArray;
    ArrayList<InvoiceAddItemModel> itemsArrayList = new ArrayList<InvoiceAddItemModel>();
    AddTotalInvoiceDiscountModel mModel;
    String strTOTALValue, strDescription, strType;
    int mPosition;
    double mSubTotal;
    double totalPercent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_total_discount);
    }

    @Override
    protected void setViewsIDs() {
        btnSubmitB = (Button) findViewById(R.id.btnSubmitB);
        txtSpinnerTV = (Spinner) findViewById(R.id.txtSpinnerTV);
        AddDiscountValueTV = (EditText) findViewById(R.id.AddDiscountValueTV);
        txtDescriptionTV = (EditText) findViewById(R.id.txtDescriptionTV);
        txtTotalTV = (TextView) findViewById(R.id.txtTotalTV);
        txtUnitPriceTV = (TextView) findViewById(R.id.txtUnitPriceTV);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        setUpTypeSpinnerAdapter();
        if (getIntent() != null) {
            mModel = (AddTotalInvoiceDiscountModel) getIntent().getSerializableExtra("Model");
            mPosition = getIntent().getIntExtra("Position", 0);
            strType = mModel.getDiscountType();
            if (strType.equals("Add Percentage")) {
                mSubTotal = mModel.getADD_UnitPrice();
                txtSpinnerTV.setSelection(Utilities.getSpinnerPosition(strType));
//                String subTotal = String.valueOf(mSubTotal);
                DecimalFormat decimalFormat = new DecimalFormat("###0.00");
                String subTotal = decimalFormat.format(mSubTotal);
//                String subTotal = String.format ("%.0f", mSubTotal);
                txtUnitPriceTV.setText(subTotal);
                txtDescriptionTV.setText(mModel.getADD_Description());
                AddDiscountValueTV.setText(mModel.getADD_DiscountValue());
                String strFullTotal = String.valueOf(mModel.getAddAndSubtractTotal());
                totalPercent = Double.parseDouble(mModel.getStrAddAndSubtracttotalPercent());

                txtTotalTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strFullTotal)));
            } else if (strType.equals("Subtract Percentage")) {
                mSubTotal = mModel.getSubtract_UnitPrice();
                txtSpinnerTV.setSelection(Utilities.getSpinnerPosition(strType));
//                String subTotal = String.valueOf(mSubTotal);
                DecimalFormat decimalFormat = new DecimalFormat("###0.00");
                String subTotal = decimalFormat.format(mSubTotal);
                txtUnitPriceTV.setText(subTotal);
                txtDescriptionTV.setText(mModel.getSubtract_Description());
                AddDiscountValueTV.setText(mModel.getSubtract_DiscountValue());
                totalPercent = Double.parseDouble(mModel.getStrAddAndSubtracttotalPercent());
                String strFullTotal = String.valueOf(mModel.getAddAndSubtractTotal());
                txtTotalTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strFullTotal)));
            }else if (strType.equals("Subtract Value")) {
                mSubTotal = mModel.getSubtract_UnitPrice();
                txtSpinnerTV.setSelection(Utilities.getSpinnerPosition(strType));
                DecimalFormat decimalFormat = new DecimalFormat("###0.00");
                String subTotal = decimalFormat.format(mSubTotal);
//                strTotalUnitPrice =String.format ("%.0f", mSubTotal);
                txtUnitPriceTV.setText(subTotal);
                txtDescriptionTV.setText(mModel.getSubtract_Description());
                AddDiscountValueTV.setText(mModel.getSubtract_DiscountValue());
                totalPercent = Double.parseDouble(mModel.getStrAddAndSubtracttotalPercent());
                String strFullTotal = String.valueOf(mModel.getAddAndSubtractTotal());
                txtTotalTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strFullTotal)));
            }else if (strType.equals("Add Value")) {
                mSubTotal = mModel.getADD_UnitPrice();
                txtSpinnerTV.setSelection(Utilities.getSpinnerPosition(strType));
//                String subTotal = String.valueOf(mSubTotal);
                DecimalFormat decimalFormat = new DecimalFormat("###0.00");
                String subTotal = decimalFormat.format(mSubTotal);
//                String subTotal = String.format ("%.0f", mSubTotal);
                txtUnitPriceTV.setText(subTotal);
                txtDescriptionTV.setText(mModel.getADD_Description());
                AddDiscountValueTV.setText(mModel.getADD_DiscountValue());
                String strFullTotal = String.valueOf(mModel.getAddAndSubtractTotal());
                totalPercent = Double.parseDouble(mModel.getStrAddAndSubtracttotalPercent());

                txtTotalTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strFullTotal)));
            }
        }
    }

    private void setUpTypeSpinnerAdapter() {
        List<String> list = new ArrayList<String>();
        list.add("Select Discount Type");
        list.add("Add Percentage");
        list.add("Subtract Percentage");
        list.add("Subtract Value");
        list.add("Add Value");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        txtSpinnerTV.setAdapter(dataAdapter);
        txtSpinnerTV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strType = adapterView.getItemAtPosition(i).toString();
                double basePrice = 0;
                double sum;
                String str = AddDiscountValueTV.getText().toString();
                try {
                    if (strType.equals("Add Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = (Float.parseFloat(str)) / 100;
                        totalPercent= ((basePrice * Double.parseDouble(str)) / 100);
//                        totalPercent = basePrice * percentValue;
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = basePrice + totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Subtract Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = (Float.parseFloat(str)) / 100;
                        totalPercent= ((basePrice * Double.parseDouble(str)) / 100);
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);

                        sum = basePrice - totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + sum);
                        strTOTALValue = String.valueOf(sum);
                    }else if (strType.equals("Subtract Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = Float.parseFloat(str);

                        totalPercent = basePrice - percentValue;
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);

                        sum = totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    }else if (strType.equals("Add Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = (Float.parseFloat(str)) / 100;

                        totalPercent = Double.parseDouble(str);
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = basePrice + totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    }
                } catch (Exception ex) {
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }


    @Override
    protected void setClickListner() {
        // Creating adapter for spinner
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        AddDiscountValueTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strType.equals("Select Discount Type")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_seelct_discount_type));
                }
            }
        });

        AddDiscountValueTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double basePrice = 0;
                double sum;
                String str = AddDiscountValueTV.getText().toString();
                try {
                    if (strType.equals("Add Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = (Float.parseFloat(str)) / 100;

                        totalPercent= ((basePrice * Double.parseDouble(str)) / 100);
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = basePrice + totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Subtract Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = (Float.parseFloat(str)) / 100;

                        totalPercent= ((basePrice * Double.parseDouble(str)) / 100);
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = basePrice - totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Subtract Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = Float.parseFloat(str);

                        totalPercent = basePrice - percentValue;
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    }else if (strType.equals("Add Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = (Float.parseFloat(str)) / 100;

                        totalPercent= Double.parseDouble(str);
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = basePrice + totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    }

                } catch (Exception ex) {
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strType.equals("Select Discount Type")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_seelct_discount_type));
                }else if (txtDescriptionTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description));
                } else if (AddDiscountValueTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_discount_value));
                } else {
                    mModel = new AddTotalInvoiceDiscountModel();
                    if (strType.equals("Add Percentage")) {
                        mModel.setDiscountType(strType);
                        mModel.setADD_Description(txtDescriptionTV.getText().toString());
                        mModel.setADD_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setADD_UnitPrice(mSubTotal);
                        mModel.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        returnIntent.putExtra("Position", mPosition);
                        setResult(1212, returnIntent);
                        finish();
                    } else if (strType.equals("Subtract Percentage")) {
                        mModel.setDiscountType(strType);
                        mModel.setSubtract_Description(txtDescriptionTV.getText().toString());
                        mModel.setSubtract_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setSubtract_UnitPrice(mSubTotal);
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        mModel.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        returnIntent.putExtra("Position", mPosition);
                        setResult(1212, returnIntent);
                        finish();
                    }else if (strType.equals("Subtract Value")) {
                        mModel.setDiscountType(strType);
                        mModel.setSubtract_Description(txtDescriptionTV.getText().toString());
                        mModel.setSubtract_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setSubtract_UnitPrice(mSubTotal);
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        mModel.setStrAddAndSubtracttotalPercent(AddDiscountValueTV.getText().toString());
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        returnIntent.putExtra("Position", mPosition);
                        setResult(1212, returnIntent);
                        finish();
                    }else if (strType.equals("Add Value")) {
                        mModel.setDiscountType(strType);
                        mModel.setADD_Description(txtDescriptionTV.getText().toString());
                        mModel.setADD_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setADD_UnitPrice(mSubTotal);
                        mModel.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        returnIntent.putExtra("Position", mPosition);
                        setResult(1212, returnIntent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
