package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;

public class AddTotalDisount extends BaseActivity {

    Activity mActivity = AddTotalDisount.this;
    String TAG = AddTotalDisount.this.getClass().getSimpleName();

    ImageView imgBack;
    TextView txtTotalTV, txtUnitPriceTV, txtCenter;
    LinearLayout llLeftLL;
    EditText AddDiscountValueTV, txtDescriptionTV;
    Button btnSubmitB;
    Spinner txtSpinnerTV;
    ArrayList<String> mdiscountTypeArray;
    ArrayList<InvoiceAddItemModel> itemsArrayList = new ArrayList<InvoiceAddItemModel>();
    double mSubTotal = 0;
    double intItems = 0;
    double intUnitPrice = 0;
    double mAmount = 0;
    AddTotalInvoiceDiscountModel mModel;
    String strTOTALValue, strType, strTotalUnitPrice;
    double totalPercent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_total_disount);

        setStatusBar();
    }

    @Override
    protected void setViewsIDs() {
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText("Add Invoice Discount");

        btnSubmitB = (Button) findViewById(R.id.btnSubmitB);
        txtSpinnerTV = (Spinner) findViewById(R.id.txtSpinnerTV);
        AddDiscountValueTV = (EditText) findViewById(R.id.AddDiscountValueTV);
        txtDescriptionTV = (EditText) findViewById(R.id.txtDescriptionTV);
        txtTotalTV = (TextView) findViewById(R.id.txtTotalTV);
        txtUnitPriceTV = (TextView) findViewById(R.id.txtUnitPriceTV);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);

        if (getIntent().getExtras() != null) {
            itemsArrayList = (ArrayList<InvoiceAddItemModel>) getIntent().getSerializableExtra("mArraYList");
            for (int i = 0; i < itemsArrayList.size(); i++) {
                intItems = itemsArrayList.get(i).getQuantity();
                intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                if (i == 0) {
                    mAmount = intUnitPrice;
                    mSubTotal = mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                } else {
                    mAmount = intUnitPrice;
                    mSubTotal = mSubTotal + mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                }
            }

            DecimalFormat decimalFormat = new DecimalFormat("###0.00");
            strTotalUnitPrice = decimalFormat.format(mSubTotal);
            Log.e(TAG, "UnitPrice" + strTotalUnitPrice);
            txtUnitPriceTV.setText(strTotalUnitPrice);
        }
    }

    @Override
    protected void setClickListner() {


        // Creating adapter for spinner
        mdiscountTypeArray = new ArrayList<String>();
        mdiscountTypeArray.add("Select Discount Type");
        mdiscountTypeArray.add("Add Percentage");
        mdiscountTypeArray.add("Subtract Percentage");
        mdiscountTypeArray.add("Subtract Value");
        mdiscountTypeArray.add("Add Value");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mdiscountTypeArray);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        txtSpinnerTV.setAdapter(dataAdapter);

        txtSpinnerTV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strType = adapterView.getItemAtPosition(i).toString();
                double basePrice = 0;
                double sum;
                String str = AddDiscountValueTV.getText().toString();
                if (str.length() > 0) {
                    if (strType.equals("Add Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice + totalPercent;
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Subtract Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice - totalPercent;
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Subtract Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = Float.parseFloat(str);
                        totalPercent = basePrice - percentValue;
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + sum);
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Add Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        totalPercent = Double.parseDouble(str);
                        sum = basePrice + totalPercent;
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        AddDiscountValueTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strType.equals("Select Discount Type")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_seelct_discount_type));
                }
            }
        });

        AddDiscountValueTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double basePrice = 0;
                double sum;
                String str = AddDiscountValueTV.getText().toString();
                try {
                    if (strType.equals("Add Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice + totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Subtract Percentage")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice - totalPercent;
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Subtract Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        float percentValue = Float.parseFloat(str);
                        totalPercent = basePrice - percentValue;
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + sum);
                        strTOTALValue = String.valueOf(sum);
                    } else if (strType.equals("Add Value")) {
                        basePrice = mSubTotal;//   Integer.parseInt()
                        totalPercent = Double.parseDouble(str);
                        sum = basePrice + totalPercent;
                        sum = Utilities.getRoundOff2Decimal(sum);
                        txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                        strTOTALValue = String.valueOf(sum);
                    }

                } catch (Exception ex) {
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strType.equals("Select Discount Type")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_seelct_discount_type));
                } else if (txtDescriptionTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description));
                } else if (AddDiscountValueTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_discount_value));
                } else {
                    mModel = new AddTotalInvoiceDiscountModel();
                    if (strType.equals("Add Percentage")) {
                        mModel.setDiscountType(strType);
                        mModel.setADD_Description(txtDescriptionTV.getText().toString());
                        mModel.setADD_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setADD_UnitPrice(mSubTotal);
                        mModel.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        setResult(999, returnIntent);
                        finish();
                    } else if (strType.equals("Add Value")) {
                        mModel.setDiscountType(strType);
                        mModel.setADD_Description(txtDescriptionTV.getText().toString());
                        mModel.setADD_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setADD_UnitPrice(mSubTotal);
                        mModel.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        setResult(999, returnIntent);
                        finish();
                    } else if (strType.equals("Subtract Percentage")) {
                        mModel.setDiscountType(strType);
                        mModel.setSubtract_Description(txtDescriptionTV.getText().toString());
                        mModel.setSubtract_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setSubtract_UnitPrice(mSubTotal);
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        mModel.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        setResult(999, returnIntent);
                        finish();
                    } else if (strType.equals("Subtract Value")) {
                        mModel.setDiscountType(strType);
                        mModel.setSubtract_Description(txtDescriptionTV.getText().toString());
                        mModel.setSubtract_DiscountValue(AddDiscountValueTV.getText().toString());
                        mModel.setSubtract_UnitPrice(mSubTotal);
                        mModel.setAddAndSubtractTotal(Double.parseDouble(strTOTALValue));
                        mModel.setStrAddAndSubtracttotalPercent(AddDiscountValueTV.getText().toString());
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Model", mModel);
                        setResult(999, returnIntent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
