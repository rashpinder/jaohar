package jaohar.com.jaohar.activities.invoices_module;

import static android.os.Build.VERSION.SDK_INT;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.gson.JsonObject;
import com.vincent.filepicker.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.CurrencyAdapter;
import jaohar.com.jaohar.adapters.ImagesAdapter;
import jaohar.com.jaohar.adapters.ItemsAdapter;
import jaohar.com.jaohar.adapters.VesselAdapter;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.fonts.ButtonRegular;
import jaohar.com.jaohar.fonts.EditTextPoppinsMedium;
import jaohar.com.jaohar.fonts.EditTextPoppinsRegular;
import jaohar.com.jaohar.fonts.TextViewPoppinsMedium;
import jaohar.com.jaohar.models.CompanyClassData;
import jaohar.com.jaohar.models.CompanyClassModel;
import jaohar.com.jaohar.models.InvoiceModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.pendingPayments.AllInvoice;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.utils.WrapContentLinearLayoutManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class AddPendingPaymentsActivity extends BaseActivity implements View.OnClickListener {

    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    public static ArrayList<AddGalleryImagesModel> mGalleryArrayList = new ArrayList<>();
    public static ArrayList<String> imagesArrayList = new ArrayList<>();
    public static String company_id = "", vessel1_id = "", vessel2_id = "", vessel3_id = "";
    private final String cameraStr = Manifest.permission.CAMERA;
    private final String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;
    private final String writeStorageStr = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    ImageView addCompanyImageView, addVesselOneIV, addVesselTwoIV, addVesselThreeIV, currencyIV, statusIV, backImageView;
    ButtonRegular btn_save;
    TextViewPoppinsMedium titleTextView;
    EditTextPoppinsMedium companyNameET, currencyEditText, vesselThreeEditText, vesselTwoEditText, vesselOneEditText, invoiceNumberEditText;
    ArrayList<CompanyClassData> mArrayListCompanys = new ArrayList<>();
    ArrayList<VesselSearchInvoiceModel> vesselArrayList = new ArrayList<>();
    ArrayList<CurrenciesModel> mArrayListCurrency = new ArrayList<>();
    String[] arrayStatus;
    EditTextPoppinsMedium statusEditText, invoiceDateEditText, amountEditText, remarksEditText;
    AllInvoice allInvoice;
    RecyclerView imagesRecyclerView;
    ItemsAdapter itemsAdapter;
    CurrencyAdapter currencyAdapter;
    VesselAdapter vesselAdapter;
    VesselSearchInvoiceModel mModel;
    RelativeLayout selectFilesLayout;
    Dialog cameraGalleryDialog;
    ArrayList<String> docsArrayList = new ArrayList<>();
    Activity mActivity = AddPendingPaymentsActivity.this;
    String mCurrentPhotoPath, mStoragePath = "", picturePath = "";
    String strRenderHtmlContent = "", strMachineryContent = "", strOwnerDetails = "", strRemarks = "";
    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    byte[] strByteArray;
    byte[] strByteArray1;
    ImagesAdapter mAddDocumentsAdapter;
    String strDocumentName = "";
    String strDocumentPath = "";
    Intent intent;
    RelativeLayout backRelativelayout;
    String id = "", pp_number = "", pp_date = "", companyName = "", amt_due = "", remarks = "", currency = "", status = "",
            vessel1 = "", vessel2 = "", vessel3 = "", old_docs = "", from = "";

    public static void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static byte[] getFileDataFromBitmap(Context context, Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pending_payments);
        arrayStatus = getResources().getStringArray(R.array.status_array_pending);
        initViews();
        listeners();
        getCompaniesList("onCreate");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        company_id = "";
        vessel1_id = "";
        vessel2_id = "";
        vessel3_id = "";
        imagesArrayList.clear();
        mGalleryArrayList.clear();
    }


    private boolean checkPermissionGAllery() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        int writeStorage = ContextCompat.checkSelfPermission(getApplicationContext(), writeStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED && writeStorage == PackageManager.PERMISSION_GRANTED;
    }


    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Log.e("TAG", String.valueOf(mGalleryArrayList.size()));
        if (mGalleryArrayList.size() == 0) {
            Intent intent = new Intent(AddPendingPaymentsActivity.this, AlbumSelectActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 10);
            startActivityForResult(intent, GALLERY_REQUEST);
        } else if (mGalleryArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit));
        } else if (mGalleryArrayList.size() != 0) {
            Intent intent = new Intent(AddPendingPaymentsActivity.this, AlbumSelectActivity.class);
            int GallarySize = 10 - mGalleryArrayList.size();
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, GallarySize);
            startActivityForResult(intent, GALLERY_REQUEST);
        }
    }

    private void openCameraGalleryDialog() {
        cameraGalleryDialog = new Dialog(mActivity);
        cameraGalleryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraGalleryDialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = cameraGalleryDialog.findViewById(R.id.txt_camra);
        TextView text_gallery = cameraGalleryDialog.findViewById(R.id.txt_gallery);
        TextView txt_files = cameraGalleryDialog.findViewById(R.id.txt_files);
        TextView txt_cancel = cameraGalleryDialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                openGallery();
            }
        });
        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                if (mGalleryArrayList.size() <= 10 - 1) {
                    openFiles();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
                }

            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
            }
        });
        cameraGalleryDialog.show();
        Log.e("error is occured", cameraGalleryDialog.toString());
    }

    public void openFiles() {

        if (documentArrayList.size() == 0 || documentArrayList.size() <= 5) {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            String[] mimetypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "text/plain", "application/pdf", "audio/*", "video/*", "application/rtf", "text/*"};
            chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            startActivityForResult(chooseFile, Constant.REQUEST_CODE_PICK_FILE);
        } else if (documentArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
        }
    }


    /* Execute api */

    public void setUpCameraGalleryDialog() {
        if (checkPermissionGAllery()) {
            openCameraGalleryDialog();
        } else {
            requestPermissionGallery();

        }
    }

    void requestPermissionGallery() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr, writeStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            if (cameraGalleryDialog != null && cameraGalleryDialog.isShowing()) {
                                cameraGalleryDialog.dismiss();
                            }
                            openCameraGalleryDialog();

                        } else {
                            Log.e("TAG", "onRequestPermissionsResult: failed");
//                            if (SDK_INT >= Build.VERSION_CODES.R) {
//                                requestPermission();
//                            }
//                            else {
//                                requestPermissionGallery();
//                            }
                        }
                    }
                }
                break;
        }
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e("value--", String.valueOf(byteArray));
        return byteArray;
    }

    private void captureImage() {
        Log.e("mGalleryArrayList", String.valueOf(mGalleryArrayList.size()));
        if (mGalleryArrayList.size() <= 10 - 1) {
            setUpCameraGalleryDialog();
        } else {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit));
        }
    }

    private Bitmap getThumbnailBitmap(final String path, final int thumbnailSize) {
        Bitmap bitmap;
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(path, opts);
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2296) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                } else {
                    Toast.makeText(this, "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == 123) {
            strRenderHtmlContent = data.getStringExtra("result");
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);//images.get(i).path
                            int count = images.size() + mGalleryArrayList.size();
                            if (count <= 5) {
                                showLoadingAlertDialog(mActivity, "JAOHAR", "Loading File Please wait...");
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        alertDialog.dismiss();
                                    }
                                }, 3000);
                            }
                            final Handler handller = new Handler();
                            handller.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (count <= 5) {
                                        for (int i = 0, l = images.size(); i < l; i++) {
                                            AddGalleryImagesModel model = new AddGalleryImagesModel();
                                            byte[] btye = getFileDataFromBitmap(mActivity, getThumbnailBitmap(images.get(i).path, 500));
                                            model.setStrImagePath(images.get(i).path.substring(images.get(i).path.lastIndexOf("/") + 1));
                                            model.setByteArray(btye);
                                            model.setType("image");
                                            model.setmBitmap(getThumbnailBitmap(images.get(i).path, 500));
                                            mGalleryArrayList.add(model);
                                            /* Set GalleryImages in recycler View */
                                            setUpGalleryAdapter();
                                        }
                                    } else {
                                        showAlertDialog(mActivity, "JAOHAR", "You can upload max. 5 files");
                                    }
                                }
                            }, 200);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("TAG", "*****No Picture Selected****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                if (mGalleryArrayList.size() <= 5) {
                    showLoadingAlertDialog(mActivity, "JAOHAR", "Loading File Please wait...");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            alertDialog.dismiss();
                        }
                    }, 3000);
                }
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    options.inJustDecodeBounds = true;
                    Bitmap thumb = BitmapFactory.decodeFile(mStoragePath, options);
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(mStoragePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                    }

                    Bitmap rotate = getThumbnailBitmap(mStoragePath, 500);
                    byte[] btye = getFileDataFromBitmap(mActivity, rotate);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(mStoragePath.substring(mStoragePath.lastIndexOf("/") + 1));
                    model.setByteArray(btye);
                    model.setType("image");
                    model.setmBitmap(rotate);
                    if (mGalleryArrayList.size() <= 5) {
                        mGalleryArrayList.add(model);
                        /* Set GalleryImages in recycler View */
                        setUpGalleryAdapter();
                    } else {
                        showAlertDialog(mActivity, "JAOHAR", "You can upload max. 5 files");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (requestCode == Constant.REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    if (mGalleryArrayList.size() <= 5) {
                        showLoadingAlertDialog(mActivity, "JAOHAR", "Loading File Please wait...");
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                alertDialog.dismiss();
                            }
                        }, 3000);
                    }


                    final Handler handller = new Handler();
                    handller.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Uri uri = data.getData();


                                String uriString = uri.toString();
                                File myFile = new File(uriString);
                                strDocumentPath = myFile.getAbsolutePath();
//                                strDocumentPath =  getRealPathFromUri(getApplicationContext(), uri);
                                strDocumentName = null;

                                if (uriString.startsWith("content://")) {
                                    Cursor cursor = null;
                                    try {
                                        cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
                                        if (cursor != null && cursor.moveToFirst()) {
                                            strDocumentName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        }
                                    } finally {
                                        cursor.close();
                                    }
                                } else if (uriString.startsWith("file://")) {
                                    strDocumentName = myFile.getName();
                                }

//                    Uri uri = data.getData();
//                    strDocumentPath = uri.getPath();
//                    strDocumentPath = strDocumentPath.replace(" ", "_");
//                    strDocumentName = uri.getLastPathSegment();
                                InputStream iStream = null;
                                if (isVirtualFile(uri)) {
                                    try {
                                        iStream = getInputStreamForVirtualFile(uri, "*/*");
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        iStream = getContentResolver().openInputStream(uri);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                                try {
                                    strByteArray = getBytes(iStream);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                AddGalleryImagesModel mDocumentModel = new AddGalleryImagesModel();
                                mDocumentModel.setIdDoc("11");
                                mDocumentModel.setDocumentName(strDocumentName);
                                mDocumentModel.setByteArray(strByteArray);
                                mDocumentModel.setmUri(uri);
                                mDocumentModel.setType("pdf");
                                mDocumentModel.setDocumentBase64(Utilities.convertByteArrayToBase64(strByteArray));
                                mDocumentModel.setDocumentPath(strDocumentPath);
                                if (mGalleryArrayList.size() <= 5) {
                                    mGalleryArrayList.add(mDocumentModel);
                                    /* Set GalleryImages in recycler View */
                                    setUpGalleryAdapter();
                                } else {
                                    showAlertDialog(mActivity, "JAOHAR", "You can upload max. 5 files");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 200);
                }
            }
        /*    if (requestCode == 205 && resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap thumb = null;
                    Uri uri = data.getData();
                    strDocumentPath = uri.getPath();
                    strDocumentPath = strDocumentPath.replace(" ", "_");
                    strDocumentName = uri.getLastPathSegment();
                    Bitmap bitmap;
                    String strDocumentPath, strDocumentName;

                    strDocumentPath = uri.getPath();
                    strDocumentPath = strDocumentPath.replace(" ", "_");
                    strDocumentName = uri.getLastPathSegment();

                    File finalFile = new File(getRealPathFromURI(AddPendingPaymentsActivity.this, uri));

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    options.inJustDecodeBounds = true;
                    bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
                    // For Convert And rotate image
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                    }

                    thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    byte[] btye = getFileDataFromBitmap(mActivity, thumb);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(strDocumentPath);
                    model.setByteArray(btye);
                    model.setmBitmap(thumb);
                    if (mGalleryArrayList.size() < 5)
                        mGalleryArrayList.add(model);
                    *//* Set GalleryImages in recycler View *//*
//                    setUpGalleryAdapter();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/
        }
    }


    String getRealPathFromUri(Context ctx, Uri uri) {
        String[] filePathColumn = {MediaStore.Files.FileColumns.DATA};

        Cursor cursor = ctx.getContentResolver().query(uri, filePathColumn,
                null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            mStoragePath = cursor.getString(columnIndex);
            Log.e("", "picturePath : " + mStoragePath);
            cursor.close();
        }
        return mStoragePath;
    }


    private void openInternalStorage() {
        Log.e("TAG", String.valueOf(documentArrayList.size()));
        if (documentArrayList.size() == 0 || documentArrayList.size() <= 10) {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            String[] mimetypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "text/plain", "application/pdf", "audio/*", "video/*", "application/rtf", "text/*"};
            chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            startActivityForResult(chooseFile, Constant.REQUEST_CODE_PICK_FILE);
        } else if (documentArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
        }
    }

    public void setAdapter(ArrayList<DocumentModel> mArrayList) {
//        imagesRecyclerView.setNestedScrollingEnabled(false);
//        imagesRecyclerView.setLayoutManager(new LinearLayoutManager(AddPendingPaymentsActivity.this));
//        mAddDocumentsAdapter = new ImagesAdapter(mActivity, mArrayList);
//        imagesRecyclerView.setAdapter(mAddDocumentsAdapter);
//        mAdapter.notifyDataSetChanged();
    }

    private InputStream getInputStreamForVirtualFile(Uri uri, String mimeTypeFilter)
            throws IOException {

        ContentResolver resolver = getContentResolver();

        String[] openableMimeTypes = resolver.getStreamTypes(uri, mimeTypeFilter);

        if (openableMimeTypes == null ||
                openableMimeTypes.length < 1) {
            throw new FileNotFoundException();
        }

        return resolver
                .openTypedAssetFileDescriptor(uri, openableMimeTypes[0], null)
                .createInputStream();
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 512;
        byte[] buffer = new byte[bufferSize];
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private boolean isVirtualFile(Uri uri) {
        if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (!DocumentsContract.isDocumentUri(mActivity, uri)) {
                return false;
            }

            Cursor cursor = getContentResolver().query(
                    uri,
                    new String[]{DocumentsContract.Document.COLUMN_FLAGS},
                    null, null, null);
            int flags = 0;
            if (cursor.moveToFirst()) {
                flags = cursor.getInt(0);
            }
            cursor.close();
            return (flags & DocumentsContract.Document.FLAG_VIRTUAL_DOCUMENT) != 0;
        } else {
            return false;
        }
    }



    private void setUpGalleryAdapter() {
//        if (mGalleryArrayList.size() >= 1) {
//            imagesRecyclerView.setVisibility(View.VISIBLE);
//        } else {
//        imagesRecyclerView.setVisibility(View.GONE);
//        }
        if (mGalleryArrayList.size() <= 5) {
            WrapContentLinearLayoutManager horizontalLayoutManager = new WrapContentLinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
            imagesRecyclerView.setLayoutManager(horizontalLayoutManager);
            mAddDocumentsAdapter = new ImagesAdapter(mActivity, mGalleryArrayList, id);
            imagesRecyclerView.setAdapter(mAddDocumentsAdapter);
            mAddDocumentsAdapter.notifyDataSetChanged();
        } else {
            mGalleryArrayList.clear();
            Toast.makeText(mActivity, "You can upload max. 5 files", Toast.LENGTH_SHORT).show();
        }

    }


    private void executeGetAllVesselsAPI() {
        vesselArrayList.clear();
        AlertDialogManager.showProgressDialog(AddPendingPaymentsActivity.this);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceVesselsRequest(JaoharPreference.readString(AddPendingPaymentsActivity.this, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
//                hideProgressDialog();
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        vesselArrayList = new ArrayList<>();
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            mModel = new VesselSearchInvoiceModel();
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            mModel.setVessel_id(mJson.getString("vessel_id"));
                            mModel.setVessel_name(mJson.getString("vessel_name"));
//                            vesselArrayList.add(mJson.getString("vessel_name"));
                            mModel.setIMO_no(mJson.getString("IMO_no"));
                            mModel.setFlag(mJson.getString("flag"));
                            vesselArrayList.add(mModel);
                        }


//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>
//                                (AddPendingPaymentsActivity.this, android.R.layout.select_dialog_item, vesselArrayList);
//                        vesselTwoEditText.setThreshold(1);//will start working from first character
//                        vesselTwoEditText.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
//
//
//                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>
//                                (AddPendingPaymentsActivity.this, android.R.layout.select_dialog_item, vesselArrayList);
//                        vesselOneEditText.setThreshold(1);//will start working from first character
//                        vesselOneEditText.setAdapter(adapter1);//setting the adapter data into the AutoCompleteTextView
//
//
//                        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>
//                                (AddPendingPaymentsActivity.this, android.R.layout.select_dialog_item, vesselArrayList);
//                        vesselThreeEditText.setThreshold(1);//will start working from first character
//                        vesselThreeEditText.setAdapter(adapter2);//setting the adapter data into the AutoCompleteTextView


                        /*Set Adapter*/
                        executeCurrencyAPI();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("******error*****", t.getMessage());
            }
        });
    }

    public void executeCurrencyAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllCurrenciesRequest(JaoharPreference.readString(AddPendingPaymentsActivity.this, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();

                JaoharConstants.IS_CURRENCY_EDIT = false;
                parseResponse14(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("******error*****", t.getMessage());
            }
        });
    }


    private void parseResponse14(String response) {
        mArrayListCurrency.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CurrenciesModel mModel = new CurrenciesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("currency_name")) {
                        mModel.setCurrency_name(mJson.getString("currency_name"));
//                        mArrayListCurrency.add(mJson.getString("currency_name"));
                    }
                    if (!mJson.isNull("alias_name")) {
                        mModel.setAlias_name(mJson.getString("alias_name"));
                    }

                    mArrayListCurrency.add(mModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCompaniesList(String from) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<CompanyClassModel> call1 = mApiInterface.getAlCompaniesPendingPaymentRequest(JaoharPreference.readString(AddPendingPaymentsActivity.this, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<CompanyClassModel>() {
            @Override
            public void onResponse(Call<CompanyClassModel> call, retrofit2.Response<CompanyClassModel> response) {
                if (imagesArrayList.size() == 0) {
                    AlertDialogManager.hideProgressDialog();
                }
                mArrayListCompanys = response.body().getData();
                if (from.equalsIgnoreCase("onCreate"))
                    executeGetAllVesselsAPI();
            }

            @Override
            public void onFailure(Call<CompanyClassModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("******error*****", t.getMessage());
            }
        });
    }


    @SuppressLint("ClickableViewAccessibility")
    private void listeners() {
        addCompanyImageView.setOnClickListener(this);
        addVesselOneIV.setOnClickListener(this);
        addVesselTwoIV.setOnClickListener(this);
        addVesselThreeIV.setOnClickListener(this);
        currencyIV.setOnClickListener(this);
        btn_save.setOnClickListener(this);
//        backImageView.setOnClickListener(this);
        backRelativelayout.setOnClickListener(this);

        statusEditText.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(AddPendingPaymentsActivity.this, "Status", arrayStatus, statusEditText);
                    return true;
                }
                return false;
            }
        });

        statusIV.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(AddPendingPaymentsActivity.this, "Status", arrayStatus, statusEditText);
                    return true;
                }
                return false;
            }
        });


        invoiceDateEditText.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    int mYear, mMonth, mDay, mHour, mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(AddPendingPaymentsActivity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;
//                                  String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    invoiceDateEditText.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });


        companyNameET.setFocusable(false);
        companyNameET.setClickable(true);

        vesselThreeEditText.setFocusable(false);
        vesselThreeEditText.setClickable(true);

        vesselTwoEditText.setFocusable(false);
        vesselTwoEditText.setClickable(true);

        vesselOneEditText.setFocusable(false);
        vesselOneEditText.setClickable(true);

        currencyEditText.setFocusable(false);
        currencyEditText.setClickable(true);


        companyNameET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooseCompanyPopup(mArrayListCompanys);
            }
        });

        vesselThreeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooseVesselPopup(vesselThreeEditText, "3");
            }
        });

        vesselTwoEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooseVesselPopup(vesselTwoEditText, "2");
            }
        });

        vesselOneEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooseVesselPopup(vesselOneEditText, "1");
            }
        });

        currencyEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooseCurrencyPopup(currencyEditText);
            }
        });


        selectFilesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGalleryArrayList.size() < 5) {
                    captureImage();
                } else {
                    showAlertDialog(mActivity, "JAOHAR", "You can't upload more than 5 files");
                }
            }
        });
    }

    private void openChooseVesselPopup(EditTextPoppinsMedium vesselEditText, String type) {
        final Dialog categoryDialog = new Dialog(AddPendingPaymentsActivity.this);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.select_item_list_categories);
        categoryDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
        RecyclerView itemsRecyclerView = categoryDialog.findViewById(R.id.itemsRecyclerView);
        txtTitle.setText("Choose Vessel");

        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        vesselAdapter = new VesselAdapter(getApplicationContext(), vesselArrayList, type, categoryDialog, vesselEditText);
        itemsRecyclerView.setAdapter(vesselAdapter);


        categoryDialog.show();
    }


    private void openChooseCompanyPopup(ArrayList<CompanyClassData> mArrayListCompanys) {
        final Dialog categoryDialog = new Dialog(AddPendingPaymentsActivity.this);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.select_item_list_categories);
        categoryDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
        RecyclerView itemsRecyclerView = categoryDialog.findViewById(R.id.itemsRecyclerView);
        txtTitle.setText("Choose Company");

        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        itemsAdapter = new ItemsAdapter(getApplicationContext(), mArrayListCompanys, company_id, categoryDialog, companyNameET);
        itemsRecyclerView.setAdapter(itemsAdapter);


        categoryDialog.show();
    }


    private void openChooseCurrencyPopup(EditTextPoppinsMedium mArrayListCompanys) {
        final Dialog categoryDialog = new Dialog(AddPendingPaymentsActivity.this);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.select_item_list_categories);
        categoryDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtTitle = categoryDialog.findViewById(R.id.txtTitle);
        RecyclerView itemsRecyclerView = categoryDialog.findViewById(R.id.itemsRecyclerView);
        txtTitle.setText("Choose Currency");

        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        currencyAdapter = new CurrencyAdapter(getApplicationContext(), mArrayListCurrency, categoryDialog, currencyEditText);
        itemsRecyclerView.setAdapter(currencyAdapter);

        categoryDialog.show();
    }

    private void initViews() {
        addCompanyImageView = findViewById(R.id.addCompanyImageView);
        addVesselOneIV = findViewById(R.id.addVesselOneIV);
        addVesselTwoIV = findViewById(R.id.addVesselTwoIV);
        addVesselThreeIV = findViewById(R.id.addVesselThreeIV);
        currencyIV = findViewById(R.id.currencyIV);
        btn_save = findViewById(R.id.btn_save);
        companyNameET = findViewById(R.id.companyNameET);
        currencyEditText = findViewById(R.id.currencyEditText);
        vesselThreeEditText = findViewById(R.id.vesselThreeEditText);
        vesselTwoEditText = findViewById(R.id.vesselTwoEditText);
        vesselOneEditText = findViewById(R.id.vesselOneEditText);
        statusEditText = findViewById(R.id.statusEditText);
        statusIV = findViewById(R.id.statusIV);
        invoiceDateEditText = findViewById(R.id.invoiceDateEditText);
        amountEditText = findViewById(R.id.amountEditText);
        remarksEditText = findViewById(R.id.remarksEditText);
        backImageView = findViewById(R.id.backImageView);
        backRelativelayout = findViewById(R.id.backRelativelayout);
        imagesRecyclerView = findViewById(R.id.imagesRecyclerView);
        selectFilesLayout = findViewById(R.id.selectFilesLayout);
        invoiceNumberEditText = findViewById(R.id.invoiceNumberEditText);
        titleTextView = findViewById(R.id.titleTextView);
        intent = getIntent();
        if (intent.hasExtra("id")) {
            id = intent.getStringExtra("id");
            pp_number = intent.getStringExtra("pp_number");
            company_id = intent.getStringExtra("company_id");
            pp_date = intent.getStringExtra("pp_date");
            companyName = intent.getStringExtra("companyName");
            vessel1 = intent.getStringExtra("vessel1");
            vessel2 = intent.getStringExtra("vessel2");
            vessel3 = intent.getStringExtra("vessel3");
            vessel1_id = intent.getStringExtra("vessel1_id");
            vessel2_id = intent.getStringExtra("vessel2_id");
            vessel3_id = intent.getStringExtra("vessel3_id");
            amt_due = intent.getStringExtra("amt_due");
            remarks = intent.getStringExtra("remarks");
            currency = intent.getStringExtra("currency");
            status = intent.getStringExtra("status");
            from = intent.getStringExtra("from");

            imagesArrayList = (ArrayList<String>) getIntent().getSerializableExtra("docs");

            Log.e("imagesArray", String.valueOf(mGalleryArrayList.size()));
            if (imagesArrayList.size() != 0) {
                AsyncCallWS asyncTask = new AsyncCallWS();
                asyncTask.execute();
            }


            companyNameET.setText(companyName);
            invoiceNumberEditText.setText(pp_number);
            invoiceDateEditText.setText(pp_date);
            vesselOneEditText.setText(vessel1);
            vesselTwoEditText.setText(vessel2);
            vesselThreeEditText.setText(vessel3);
            amountEditText.setText(amt_due);
            remarksEditText.setText(remarks);
            currencyEditText.setText(currency);
            statusEditText.setText(status);

            titleTextView.setText("Edit Invoice");

        }

        imagesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addCompanyImageView:
                openAddCompany();
                break;

            case R.id.addVesselOneIV:

            case R.id.addVesselThreeIV:

            case R.id.addVesselTwoIV:
                openAddVessel1Dialog();
                break;

            case R.id.currencyIV:
                openAddCurrencyDialog();
                break;

            case R.id.btn_save:
                if (validations()) {
                    if (isNetworkAvailable(this)) {
                        AlertDialogManager.showProgressDialog(mActivity);
                        executeAddInvoiceService();
                    } else {
                        showAlerDialog(this, getString(R.string.app_name), getString(R.string.internetconnection));
                    }
                }
                break;

            case R.id.backRelativelayout:
                onBackPressed();
                break;
        }
    }

    private boolean validations() {
        boolean isValid = true;

        if (companyNameET.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Enter company name");
            isValid = false;
        } else if (invoiceNumberEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Enter invoice number");
            isValid = false;
        } else if (invoiceDateEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Select invoice date");
            isValid = false;
        }/* else if (vesselOneEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Choose vessel 1");
            isValid = false;
        } else if (vesselTwoEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Choose vessel 2");
            isValid = false;
        } else if (vesselThreeEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Choose vessel 3");
            isValid = false;
        }*/ else if (amountEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Enter amount");
            isValid = false;
        } else if (currencyEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Choose currency");
            isValid = false;
        } else if (statusEditText.getText().toString().isEmpty()) {
            showAlerDialog(mActivity, getString(R.string.app_name), "Select status");
            isValid = false;
        }
        return isValid;
    }

    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        Log.e("**********Image Name", sb.toString());
        return sb.toString();
    }

    private void executeAddInvoiceService() {
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;


        if (mGalleryArrayList.size() > 0) {
            if (mGalleryArrayList.get(0).getType().equals("image")) {
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(0).getmBitmap()));
                mMultipartBody1 = MultipartBody.Part.createFormData("doc[0]", "attachment0" + ".jpg", requestFile1);
            } else {
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), mGalleryArrayList.get(0).getByteArray());
                mMultipartBody1 = MultipartBody.Part.createFormData("doc[0]", "attachment0" + ".pdf", requestFileDoc1);
            }
        }


        if (mGalleryArrayList.size() > 1) {
            if (mGalleryArrayList.get(1).getType().equals("image")) {
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(1).getmBitmap()));
                mMultipartBody2 = MultipartBody.Part.createFormData("doc[1]", "attachment1" + ".jpg", requestFile1);
            } else {
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), mGalleryArrayList.get(1).getByteArray());
                mMultipartBody2 = MultipartBody.Part.createFormData("doc[1]", "attachment1" + ".pdf", requestFileDoc1);
            }
        }

        if (mGalleryArrayList.size() > 2) {
            if (mGalleryArrayList.get(2).getType().equals("image")) {
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(2).getmBitmap()));
                mMultipartBody3 = MultipartBody.Part.createFormData("doc[2]", "attachment2" + ".jpg", requestFile1);
            } else {
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), mGalleryArrayList.get(2).getByteArray());
                mMultipartBody3 = MultipartBody.Part.createFormData("doc[2]", "attachment2" + ".pdf", requestFileDoc1);
            }
        }

        if (mGalleryArrayList.size() > 3) {
            if (mGalleryArrayList.get(3).getType().equals("image")) {
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(3).getmBitmap()));
                mMultipartBody4 = MultipartBody.Part.createFormData("doc[3]", "attachment3" + ".jpg", requestFile1);
            } else {
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), mGalleryArrayList.get(3).getByteArray());
                mMultipartBody4 = MultipartBody.Part.createFormData("doc[3]", "attachment3" + ".pdf", requestFileDoc1);
            }
        }

        if (mGalleryArrayList.size() > 4) {
            if (mGalleryArrayList.get(4).getType().equals("image")) {
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(4).getmBitmap()));
                mMultipartBody5 = MultipartBody.Part.createFormData("doc[4]", "attachment4" + ".jpg", requestFile1);
            } else {
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), mGalleryArrayList.get(4).getByteArray());
                mMultipartBody5 = MultipartBody.Part.createFormData("doc[4]", "attachment4" + ".pdf", requestFileDoc1);
            }
        }

        RequestBody ppNumber = RequestBody.create(MediaType.parse("multipart/form-data"), invoiceNumberEditText.getText().toString());
        RequestBody ppDate = RequestBody.create(MediaType.parse("multipart/form-data"), invoiceDateEditText.getText().toString());
        RequestBody ppCompany = RequestBody.create(MediaType.parse("multipart/form-data"), company_id);
        RequestBody ppCurrency = RequestBody.create(MediaType.parse("multipart/form-data"), currencyEditText.getText().toString());
        RequestBody ppVessel1 = RequestBody.create(MediaType.parse("multipart/form-data"), vessel1_id);
        RequestBody ppVessel2 = RequestBody.create(MediaType.parse("multipart/form-data"), vessel2_id);
        RequestBody ppVessel3 = RequestBody.create(MediaType.parse("multipart/form-data"), vessel3_id);
        RequestBody ppAmountDue = RequestBody.create(MediaType.parse("multipart/form-data"), amountEditText.getText().toString());
        RequestBody ppStatus = RequestBody.create(MediaType.parse("multipart/form-data"), statusEditText.getText().toString());
        RequestBody ppRemarks = RequestBody.create(MediaType.parse("multipart/form-data"), remarksEditText.getText().toString());
        RequestBody ppID = RequestBody.create(MediaType.parse("multipart/form-data"), id);
        RequestBody userId = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(AddPendingPaymentsActivity.this, JaoharPreference.STAFF_ID, ""));

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = null;
        if (!id.equals("")) { //edit
            StringBuilder str = new StringBuilder();
            for (AddGalleryImagesModel eachstring : mGalleryArrayList) {
                str.append(eachstring.getOld_url()).append(",");
            }
            String commaseparatedlist = str.toString();
            if (commaseparatedlist.length() > 0)
                old_docs = commaseparatedlist.substring(0, commaseparatedlist.length() - 1);

            call1 = mApiInterface.EditPendingPaymentDocs(ppNumber, ppDate, ppCompany, ppCurrency, ppVessel1, ppVessel2, ppVessel3,
                    ppAmountDue, ppStatus, ppRemarks, userId, ppID,
                    RequestBody.create(MediaType.parse("multipart/form-data"), old_docs), mMultipartBody1, mMultipartBody2,
                    mMultipartBody3, mMultipartBody4, mMultipartBody5);
        } else {
            call1 = mApiInterface.AddPendingPaymentDocs(ppNumber, ppDate, ppCompany, ppCurrency, ppVessel1, ppVessel2, ppVessel3,
                    ppAmountDue, ppStatus, ppRemarks, userId, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4, mMultipartBody5);
        }


        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e("addInvoiceResponse", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    if (jsonObject.getInt("status") == 1) {
                        if (from.equals("edit")) {
                            AlertDialogManager.showFinishAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name),
                                    jsonObject.getString("message"));
                        } else if (from.equals("search_edit")) {
                            AlertDialogManager.showFinishAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name),
                                    jsonObject.getString("message"));
                            SearchInvoiceActivity.search_back = "true";
                        } else {
                            AlertDialogManager.showFinishAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name),
                                    jsonObject.getString("message"));
                            PendingPaymentDetailsActivity.back = "true";
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name),
                        getString(R.string.server_error));
                Log.e("******error*****", t.getMessage());
            }
        });
    }

    private void openAddCurrencyDialog() {
        final Dialog addCurrencyDialog = new Dialog(AddPendingPaymentsActivity.this);
        addCurrencyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addCurrencyDialog.setContentView(R.layout.dialog_add_currency);
        addCurrencyDialog.setCanceledOnTouchOutside(true);
        addCurrencyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ButtonRegular saveButton = addCurrencyDialog.findViewById(R.id.btn_save);
        EditTextPoppinsRegular currencyNameET = addCurrencyDialog.findViewById(R.id.currencyNameET);
        EditTextPoppinsRegular currencyCodeET = addCurrencyDialog.findViewById(R.id.currencyCodeET);
        ButtonRegular cancelButton = addCurrencyDialog.findViewById(R.id.btn_cancel);
        ImageView crossButton = addCurrencyDialog.findViewById(R.id.crossButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCurrencyDialog.dismiss();
                if (currencyNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Currency Name");
                } else if (currencyCodeET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Currency Code");
                } else {
                    executeAddCurrencyAPI(currencyNameET, currencyCodeET);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCurrencyDialog.dismiss();
            }
        });
        crossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCurrencyDialog.dismiss();
            }
        });

        addCurrencyDialog.show();
    }

    private void executeAddCurrencyAPI(EditTextPoppinsRegular currencyNameET, EditTextPoppinsRegular currencyCodeET) {
        AlertDialogManager.showProgressDialog(AddPendingPaymentsActivity.this);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addCurrencyRequest(currencyNameET.getText().toString(),
                currencyCodeET.getText().toString(),
                JaoharPreference.readString(AddPendingPaymentsActivity.this, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    JaoharConstants.IS_CURRENCY_EDIT = true;
                    showAlerDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("******error*****", t.getMessage());
            }
        });
    }

    private void openAddVessel1Dialog() {
        final Dialog addVessel1Dialog = new Dialog(AddPendingPaymentsActivity.this);
        addVessel1Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addVessel1Dialog.setContentView(R.layout.dialog_add_vessel);
        addVessel1Dialog.setCanceledOnTouchOutside(true);
        addVessel1Dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ButtonRegular saveButton = addVessel1Dialog.findViewById(R.id.btn_save);
        EditTextPoppinsRegular vesselNameET = addVessel1Dialog.findViewById(R.id.vesselNameET);
        EditTextPoppinsRegular imoET = addVessel1Dialog.findViewById(R.id.imoET);
        EditTextPoppinsRegular flagET = addVessel1Dialog.findViewById(R.id.flagET);
        ButtonRegular cancelButton = addVessel1Dialog.findViewById(R.id.btn_cancel);
        ImageView crossButton = addVessel1Dialog.findViewById(R.id.crossButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addVessel1Dialog.dismiss();
                if (vesselNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Vessel Name");
                } else if (imoET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter IMO Number");
                } else if (flagET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Flag");
                } else {
                    executeAddVesselAPI(vesselNameET, imoET, flagET);
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addVessel1Dialog.dismiss();
            }
        });
        crossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addVessel1Dialog.dismiss();
            }
        });

        addVessel1Dialog.show();
    }

    private void executeAddVesselAPI(EditTextPoppinsRegular vesselNameET, EditTextPoppinsRegular imoET, EditTextPoppinsRegular flagET) {
        AlertDialogManager.showProgressDialog(AddPendingPaymentsActivity.this);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<InvoiceModel> call1 = mApiInterface.addInvoiceVesselRequest(vesselNameET.getText().toString(), imoET.getText().toString(), flagET.getText().toString(), JaoharPreference.readString(AddPendingPaymentsActivity.this, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, retrofit2.Response<InvoiceModel> response) {
                AlertDialogManager.hideProgressDialog();
                InvoiceModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IS_CURRENCY_EDIT = true;
                    showAlerDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("******error*****", t.getMessage());
            }
        });
    }

    private void openAddCompany() {
        final Dialog addCompanyDialog = new Dialog(AddPendingPaymentsActivity.this);
        addCompanyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addCompanyDialog.setContentView(R.layout.dialog_add_company);
        addCompanyDialog.setCanceledOnTouchOutside(true);
        addCompanyDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        addCompanyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ButtonRegular saveButton = addCompanyDialog.findViewById(R.id.btn_save);
        EditTextPoppinsRegular companyNameET = addCompanyDialog.findViewById(R.id.companyNameET);
        EditTextPoppinsRegular addressOneET = addCompanyDialog.findViewById(R.id.addressOneET);
        EditTextPoppinsRegular addressTwoET = addCompanyDialog.findViewById(R.id.addressTwoET);
        EditTextPoppinsRegular addressThreeET = addCompanyDialog.findViewById(R.id.addressThreeET);
        EditTextPoppinsRegular addressFourET = addCompanyDialog.findViewById(R.id.addressFourET);
        EditTextPoppinsRegular addressFiveET = addCompanyDialog.findViewById(R.id.addressFiveET);
        ButtonRegular cancelButton = addCompanyDialog.findViewById(R.id.btn_cancel);
        ImageView crossButton = addCompanyDialog.findViewById(R.id.crossButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCompanyDialog.dismiss();
                if (companyNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Company Name");
                } else if (addressOneET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Address1");
                } else if (addressTwoET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Address2");
                } else if (addressThreeET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Address3");
                } else if (addressFourET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Address4");
                } else if (addressFiveET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "Enter Address5");
                } else {
                    executeAddCompanyAPI(companyNameET, addressOneET, addressTwoET, addressThreeET,
                            addressFourET, addressFiveET);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCompanyDialog.dismiss();
            }
        });

        crossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCompanyDialog.dismiss();
            }
        });

        addCompanyDialog.show();
    }

    private void executeAddCompanyAPI(EditTextPoppinsRegular companyNameET, EditTextPoppinsRegular addressOneET, EditTextPoppinsRegular addressTwoET, EditTextPoppinsRegular addressThreeET, EditTextPoppinsRegular addressFourET, EditTextPoppinsRegular addressFiveET) {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("company_name", companyNameET.getText().toString());
        mMap.put("Address1", addressOneET.getText().toString());
        mMap.put("Address2", addressTwoET.getText().toString());
        mMap.put("Address3", addressThreeET.getText().toString());
        mMap.put("Address4", addressFourET.getText().toString());
        mMap.put("Address5", addressFiveET.getText().toString());
        mMap.put("user_id", JaoharPreference.readString(AddPendingPaymentsActivity.this, JaoharPreference.STAFF_ID, ""));

        AlertDialogManager.showProgressDialog(AddPendingPaymentsActivity.this);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addCompanyRequest(mMap).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e("**RESPONSE**", String.valueOf(response.body()));
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    JaoharConstants.IS_COMPANY_EDIT = true;
                    showAlerDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());

                    getCompaniesList("onAdd");
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(AddPendingPaymentsActivity.this, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e("**ERROR**", t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        company_id = "";
        vessel1_id = "";
        vessel2_id = "";
        vessel3_id = "";
        mGalleryArrayList.clear();
        imagesArrayList.clear();
    }


    private class AsyncCallWS extends AsyncTask<Void, Void, ArrayList<AddGalleryImagesModel>> {

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected ArrayList<AddGalleryImagesModel> doInBackground(Void... params) {
            Bitmap image = null;
            AddGalleryImagesModel addGalleryImagesModel;
            for (int i = 0; i < imagesArrayList.size(); i++) {
                String extension = imagesArrayList.get(i).substring(imagesArrayList.get(i).lastIndexOf(".") + 1);
                addGalleryImagesModel = new AddGalleryImagesModel();
                if (extension.contains("pdf")) {
                    try {
                        URL url = new URL(imagesArrayList.get(i));
                        addGalleryImagesModel.setByteArray(Utilities.getAsByteArray(url));
                        addGalleryImagesModel.setType("pdf");
                        addGalleryImagesModel.setOld_url(imagesArrayList.get(i));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        URL url = new URL(imagesArrayList.get(i));
                        image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    } catch (IOException e) {
                        System.out.println(e);
                    }

                    addGalleryImagesModel.setOld_url(imagesArrayList.get(i));
                    addGalleryImagesModel.setmBitmap(image);
                    addGalleryImagesModel.setType("image");
                }

                mGalleryArrayList.add(addGalleryImagesModel);
            }
            return mGalleryArrayList;
        }

        @Override
        protected void onPostExecute(ArrayList<AddGalleryImagesModel> result) {
            super.onPostExecute(result);
            LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
            imagesRecyclerView.setLayoutManager(horizontalLayoutManager);
            mAddDocumentsAdapter = new ImagesAdapter(mActivity, mGalleryArrayList, id);
            imagesRecyclerView.setAdapter(mAddDocumentsAdapter);
            AlertDialogManager.hideProgressDialog();
            // your result.(ArrayList<Chef>)
        }
    }
}