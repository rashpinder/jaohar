package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.services.MailingListServices;
import jaohar.com.jaohar.adapters.SendingMAilListAdapter;
import jaohar.com.jaohar.beans.MailingListSendingModel;
import jaohar.com.jaohar.beans.SendMailListModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;

public class ListSendingMailingListActivity extends BaseActivity {
private Activity mActivity = ListSendingMailingListActivity.this;
    String TAG = ListSendingMailingListActivity.this.getClass().getSimpleName();
private ImageView imgBack;
private LinearLayout llLeftLL;
private TextView txtCenter;
private RecyclerView mRecyclerView;
private String strtypeOfUSer;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();
    ArrayList<MailingListSendingModel> mListDATA = new ArrayList<MailingListSendingModel>();

    SendingMAilListAdapter mAdapter;
     static int a ;
     boolean isResPonse = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sending_mailing_list);
        mListDATA.clear();
        if(getIntent().getSerializableExtra("subject")!=null){
            JaoharConstants.strSubject=   getIntent().getStringExtra("subject");
            strtypeOfUSer=   getIntent().getStringExtra("type");
        }
        AlertDialogManager.showProgressDialog(mActivity);
        if(Utilities.isNetworkAvailable(mActivity)){

            startService(new Intent(mActivity, MailingListServices.class));
//        Log.e(TAG, "ArrayListSize*********:" + "Start");
//        for(a= 0 ;a<  JaoharConstants.mArrayLISTDATA.size();a++){
//            Log.e(TAG, "ArrayListSize*********:" + "DATA========================"+  JaoharConstants.mArrayLISTDATA.get(a).getContent()+" "+  JaoharConstants.mArrayLISTDATA.get(a).getEmail());
////            if(isResPonse==false){
////                AlertDialogManager.showProgressDialog(mActivity);
////                isResPonse=true;
//
//    sendEamilAPI(JaoharConstants.mArrayLISTDATA.get(a).getContent(),  JaoharConstants.mArrayLISTDATA.get(a).getEmail());
//
//
////            }
//
//        }

        }else {
AlertDialogManager.showAlertDialog(mActivity,getResources().getString(R.string.app_name),getResources().getString(R.string.internetconnection));
        }
    }

    @Override
    protected void setViewsIDs() {
        mRecyclerView = findViewById(R.id.mRecyclerView);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText(getResources().getString(R.string.sending_mails));
        startTimer();
    }

    //To start timer
    private void startTimer() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        //your code is here
                        if (JaoharConstants.Is_Value == true) {
                            JaoharConstants.Is_Value=false;
                            setAdapter();
                        }else {

                        }
                    }
                });
            }
        };
        timer.schedule(timerTask, 500, 7000);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(strtypeOfUSer.equals("manager")){
            super.onBackPressed();
            Intent mIntent = new Intent(mActivity,MailIstManagerActivity.class);
            mIntent.putExtra("type",strtypeOfUSer);
            startActivity(mIntent);
            finish();
        }else {
            super.onBackPressed();
            Intent mIntent = new Intent(mActivity,MaillingStaffActivity.class);
            mIntent.putExtra("type",strtypeOfUSer);
            startActivity(mIntent);
            finish();
        }

    }



//    private void sendEamilAPI(String strContent,String strEmail) {
////        http://root.jaohar.com/Staging/JaoharWebServicesNew/MailSend.php
//        String strAPIUrl = "";
//
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.Mail_Send;
//        try {
//            jsonObject.put("content", strContent);
//            jsonObject.put("subject","test");
//            jsonObject.put("email", strEmail);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                isResPonse=false;
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    SendMailListModel mailListModel = new SendMailListModel();
//                    mailListModel.setSuccess(jsonObject.getString("success"));
//                    mailListModel.setEmail(jsonObject.getString("email"));
//                    JaoharConstants.mArraySendMailListModel.add(mailListModel);
//                    setAdapter();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void setAdapter() {
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new SendingMAilListAdapter(mActivity, JaoharConstants.mArraySendMailListModel);
        mRecyclerView.setAdapter(mAdapter);
//      mAdapter.notifyDataSetChanged();
        AlertDialogManager.hideProgressDialog();
//      stopTimer();
    }

    //To stop timer
    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

}
