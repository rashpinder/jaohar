package jaohar.com.jaohar.activities.forum_module;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.SnappingRecyclerView;
import jaohar.com.jaohar.adapters.forum_module.ForumTypeAdapter;
import jaohar.com.jaohar.adapters.forum_module.SelectedForumUsersAdapter;
import jaohar.com.jaohar.beans.ForumModule.ForumUserTypeModel;
import jaohar.com.jaohar.interfaces.forumModule.RemoveForumUserInterface;
import jaohar.com.jaohar.interfaces.forumModule.SpinnerDataAdd;
import jaohar.com.jaohar.models.AddForumModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.views.MultiSelectionSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddForumActivity extends BaseActivity implements SpinnerDataAdd {
    /*
     * Initialize the Activity
     */
    private Activity mActivity = AddForumActivity.this;

    /*
     * Getting the Class Name
     */
    String TAG = AddForumActivity.this.getClass().getSimpleName();

    /*
     * Some Variable and data Types
     */
    String mCurrentPhotoPath, mStoragePath = "", strBase64 = "";
    Bitmap thumb = null;

    /*
     * Widgets
     */
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.imgImageIV)
    ImageView imgImageIV;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.btnSendMAILB)
    Button btnSendMAILB;
    @BindView(R.id.editSubjectET)
    EditText editSubjectET;
    @BindView(R.id.editforumtypeET)
    EditText editforumtypeET;
    @BindView(R.id.selectTypeSpn)
    Spinner selectTypeSpn;
    @BindView(R.id.selectUserSpn)
    MultiSelectionSpinner selectUserSpn;
    @BindView(R.id.txt_prompt_from)
    TextView txt_prompt_from;
    @BindView(R.id.userSpnRL)
    RelativeLayout userSpnRL;
    @BindView(R.id.usersTV)
    TextView usersTV;
    @BindView(R.id.selectedUsersRV)
    RecyclerView selectedUsersRV;

    BottomSheetDialog bottomSheetDialog;
    File imageFile = null;

    /*
     * Array List And Models
     * */
    ArrayList<ForumUserTypeModel> mArrayListData = new ArrayList<>();
    ArrayList<String> mUsersListData = new ArrayList<>();
    SpinnerDataAdd mSpinnerData;
    SelectedForumUsersAdapter mSelectedForumUsersAdapter;

    /* Permissions Value */
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;

    /* Permissions */
    private String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String CAMERA = Manifest.permission.CAMERA;

    String strType = "", strForumType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_add_forum);

        ButterKnife.bind(this);
        mSpinnerData = this;
        setData();
        setUpForumAdapter();
    }

    void setData() {
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.add_forum));
        txtCenter.setGravity(Gravity.START);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(100, 0, 0, 0);
        txtCenter.setLayoutParams(params);

        /*
         * Retrieve data from previous Activity
         */
        if (getIntent() != null) {
            strType = getIntent().getStringExtra("type");
        }
        /*
         * Execute getting Api of User Email List
         */
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            mArrayListData.clear();
            executeGettingUserAPI();
        }
    }

    @OnClick({R.id.llLeftLL, R.id.imgImageIV, R.id.btnSendMAILB, R.id.txt_prompt_from, R.id.usersTV, R.id.editforumtypeET})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgImageIV:
                mToGrantPermission();
                break;
            case R.id.btnSendMAILB:
                try {
                    performSendMail();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.txt_prompt_from:
                checkTextPromptForm();
                break;
            case R.id.usersTV:
                startActivity(new Intent(mActivity, UsersWithForumAccessActivity.class).putExtra("type", strType));
                break;
            case R.id.editforumtypeET:
                PerformForumTypeClick();
                break;
        }
    }

    /******
     * PERMISSIONS
     ******/
    public void mToGrantPermission() {
        if (mCheckPermission()) {
            openCameraGalleryDialog();
        } else {
            mRequestPermission();
        }
    }

    private boolean mCheckPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int read_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int write_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return camera == PackageManager.PERMISSION_GRANTED && read_external_storage == PackageManager.PERMISSION_GRANTED && write_external_storage == PackageManager.PERMISSION_GRANTED;
    }

    private void mRequestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 100);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            //All Permissions Granted
//                            openCameraGalleryDialog();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            Toast.makeText(mActivity, getString(R.string.goto_settings), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView tittleTV = dialog.findViewById(R.id.tittleTV);
        TextView text_camra = dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);
        txt_files.setVisibility(View.GONE);
        tittleTV.setVisibility(View.GONE);
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });

        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            /* Potentially direct the user to the Market with a Dialog */
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void openInternalStorage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf|text/plain");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 606);
        } catch (android.content.ActivityNotFoundException ex) {
            /* Potentially direct the user to the Market with a Dialog */
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        /* Ensure that there's a camera activity to handle the intent */
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            /* Create the File where the photo should go */
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                /* Error occurred while creating the File */
                ex.printStackTrace();
            }
            /* Continue only if the File was successfully created */
            if (photoFile != null) {
                String authorities = getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        /* Create an image file name */
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        /* Save a file: path for use with ACTION_VIEW intents */
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            Bitmap bitmap;
                            Uri uri = data.getData();
                            File finalFile = new File(getRealPathFromURI(uri));

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 0;

                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
                            /* For Convert And rotate image */
                            ExifInterface exifInterface = null;
                            try {
                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            Matrix matrix = new Matrix();
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                            }
                            JaoharConstants.IS_camera_Click = false;
                            thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                            imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            imgImageIV.setImageBitmap(thumb);
                            strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected*****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {
                    JaoharConstants.IS_camera_Click = true;
                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    imgImageIV.setImageBitmap(thumb);
                    strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (JaoharSingleton.getInstance().getmForumUsersList() != null) {
            JaoharSingleton.getInstance().getmForumUsersList().clear();
        }

        if (JaoharSingleton.getInstance().getmUsersArrayList() != null) {
            JaoharSingleton.getInstance().getmUsersArrayList().clear();
        }
        finish();
    }

    /**
     * Implemented API to Add forum data in Server @AddStaffForum
     *
     * @params
     * @user_id
     * @subject
     * @photo
     **/
    private void executeADDAPI(final ArrayList<String> mStringArray) throws IOException {
        showProgressDialog(mActivity);
        if (strType.equals("UKadmin")) {
            executeForumApi();
        } else if (strType.equals("UKstaff")) {
            executeForumApi();
        } else {
            executeForumStaffApi();
        }
    }

    private void executeForumApi() throws IOException {
        String user_id = null;
        if (strType.equals("staff")) {
            user_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else if (strType.equals("UKstaff")) {
            user_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            user_id = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AddForumModel> call1 = mApiInterface.addForum11(user_id, strBase64, editSubjectET.getText().toString(), strForumType, String.valueOf(mUsersListData));
        call1.enqueue(new Callback<AddForumModel>() {
            @Override
            public void onResponse(Call<AddForumModel> call, Response<AddForumModel> response) {
                hideProgressDialog();

                if (response.body() != null) {
                    AddForumModel addForumModel = response.body();
                    Log.e(TAG, "==Response1==" + response.body().getMessage());
                    if (addForumModel.getStatus() == 1) {
                        showFinishAlertDialog(mActivity, getString(R.string.app_name), "" + addForumModel.getMessage());
                    } else {
                        Log.e(TAG, "Unexpected*********:" + addForumModel.getMessage());
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + addForumModel.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddForumModel> call, Throwable t) {
                hideProgressDialog();
                showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
            }
        });
    }

    private void executeForumStaffApi() throws IOException {
        String user_id = null;
        if (strType.equals("staff")) {
            user_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else if (strType.equals("UKstaff")) {
            user_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            user_id = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AddForumModel> call1 = mApiInterface.addForumStaff11(user_id, strBase64, editSubjectET.getText().toString(),
                strForumType, String.valueOf(mUsersListData));
        call1.enqueue(new Callback<AddForumModel>() {
            @Override
            public void onResponse(Call<AddForumModel> call, Response<AddForumModel> response) {
                hideProgressDialog();

                if (response.body() != null) {
                    AddForumModel addForumModel = response.body();
                    Log.e(TAG, "==Response1==" + response.body().getMessage());
                    if (addForumModel.getStatus() == 1) {
                        showFinishAlertDialog(mActivity, getString(R.string.app_name), "" + addForumModel.getMessage());
                    } else {
                        Log.e(TAG, "Unexpected*********:" + addForumModel.getMessage());
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + addForumModel.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddForumModel> call, Throwable t) {
                hideProgressDialog();
                showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
            }
        });
    }

    /**
     * Implemented API to Add forum data in Server @AddStaffForum
     *
     * @params
     * @user_id
     * @subject
     * @photo
     **/
    private void executeGettingUserAPI() {
        showProgressDialog(mActivity);
        if (strType.equals("UKadmin")) {
            executeListFourmAdminApi();
        } else if (strType.equals("UKstaff")) {
            executeGetUserListForum();
        } else {
            executeGetUserListStaffForum();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        HashMap<String, String> params = new HashMap<>();
        if (strType.equals("staff")) {
            params.put("user_id", getStaffID());
        } else if (strType.equals("UKstaff")) {
            params.put("user_id", getStaffID());
        } else {
            params.put("user_id", getadminID());
        }
        return params;
    }

    private void executeListFourmAdminApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserListForum(mParams2()).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                Log.e(TAG, "==Response==" + response);
                hideProgressDialog();
                executeSecondApiData(response.body().toString());
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.toString());
                hideProgressDialog();
                showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
            }
        });
    }

    private void executeGetUserListForum() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserListForumStaff(mParams2()).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                Log.e(TAG, "==Response==" + response);
                hideProgressDialog();
                executeSecondApiData(response.body().toString());
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.toString());
                hideProgressDialog();
                showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
            }
        });
    }

    private void executeGetUserListStaffForum() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserListStaffForumStaff(mParams2()).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                Log.e(TAG, "==Response==" + response);
                hideProgressDialog();
                executeSecondApiData(response.toString());
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.toString());
                hideProgressDialog();
                showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
            }
        });
    }

    private void executeSecondApiData(String response) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(response));
            hideProgressDialog();
            String status = jsonObject.getString("status");
            String message = jsonObject.getString("message");
            if (status.equals("1")) {
                parseResponce(jsonObject);
            } else {
                Log.e(TAG, "Unexpected*********:" + message);
                showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * Parse Data And Set It on Spinner Adapter
     */
    private void parseResponce(JSONObject jsonObject) {
        JSONArray mjsonArrayData = null;
        try {
            mjsonArrayData = jsonObject.getJSONArray("users");
            for (int i = 0; i < mjsonArrayData.length(); i++) {
                JSONObject mJsonData = mjsonArrayData.getJSONObject(i);
                ForumUserTypeModel mModel = new ForumUserTypeModel();
                if (!mJsonData.getString("id").equals("")) {
                    mModel.setId(mJsonData.getString("id"));
                }
                if (!mJsonData.getString("email").equals("")) {
                    mModel.setEmail(mJsonData.getString("email"));
                }
                mArrayListData.add(mModel);
            }
            /*
             * SetUp Adapter
             * */
            setSpinnerAdpter();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * Setting Up Spinner Adapter
     */
    private void setSpinnerAdpter() {
        selectUserSpn.setItems(mArrayListData, mActivity, mSpinnerData);
    }

    /*
     * Setting Up Forum Type Spinner Adapter
     * */
    private void setUpForumAdapter() {
        final Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "Poppins-Medium.ttf");
        final String[] mStatusArray = getResources().getStringArray(R.array.forumType_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mStatusArray) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTextView);
                itemTextView.setTypeface(font);
                itemTextView.setText(mStatusArray[position]);
                return v;
            }
        };
        selectTypeSpn.setAdapter(adapter);

        /* Status Spinner Click Listener */
        selectTypeSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (String.valueOf(parent.getItemAtPosition(position)).equals("Open")) {
                    strForumType = "open";
                    usersTV.setVisibility(View.GONE);
                    selectedUsersRV.setVisibility(View.GONE);
                    ((TextView) view).setTypeface(font);
                    if (JaoharSingleton.getInstance().getmForumUsersList() != null) {
                        JaoharSingleton.getInstance().getmForumUsersList().clear();
                    }
                    if (JaoharSingleton.getInstance().getmUsersArrayList() != null) {
                        JaoharSingleton.getInstance().getmUsersArrayList().clear();
                    }
                } else if (String.valueOf(parent.getItemAtPosition(position)).equals("Restricted")) {
                    strForumType = "restricted";
                    usersTV.setVisibility(View.VISIBLE);
                    selectedUsersRV.setVisibility(View.VISIBLE);
                    ((TextView) view).setTypeface(font);
                } else {
                    strForumType = "";
                    usersTV.setVisibility(View.GONE);
                    selectedUsersRV.setVisibility(View.GONE);
                    ((TextView) view).setTypeface(font);
                }
                ((TextView) view).setText(String.valueOf(parent.getItemAtPosition(position)));
                ((TextView) view).setTextColor(Color.BLACK); //Change selected text color
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void isDataAdded(boolean isClicked) {
        /* To get the selected Item list */
        ArrayList<String> selectedItems = selectUserSpn.getSelectedItems();
        Log.e(TAG, "DATA===" + selectedItems.size());
        if (selectedItems.size() == 0) {
            txt_prompt_from.setVisibility(View.VISIBLE);
            selectUserSpn.setVisibility(View.GONE);

        } else {
            txt_prompt_from.setVisibility(View.GONE);
            selectUserSpn.setVisibility(View.VISIBLE);
        }
    }

    public void setAdapter(final ArrayList<ForumUserTypeModel> mArrayList) {
        selectedUsersRV.setNestedScrollingEnabled(false);
        selectedUsersRV.setLayoutManager(new GridLayoutManager(mActivity, 2));
        mSelectedForumUsersAdapter = new SelectedForumUsersAdapter(mActivity, mArrayList, new RemoveForumUserInterface() {
            @Override
            public void mRemoveForumUserInterface(int position) {
                if (JaoharSingleton.getInstance().getmForumUsersList().contains(mArrayList.get(position).getId())) {
                    JaoharSingleton.getInstance().getmForumUsersList().remove(position);
                }
                mArrayList.remove(position);
                mSelectedForumUsersAdapter.notifyDataSetChanged();
            }
        });
        selectedUsersRV.setAdapter(mSelectedForumUsersAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ArrayList<ForumUserTypeModel> mUsersData = new ArrayList<>();

        if (JaoharSingleton.getInstance().getmUsersArrayList() != null) {
            mUsersData = JaoharSingleton.getInstance().getmUsersArrayList();
            setAdapter(mUsersData);
        }
    }

    private List<String> list = new ArrayList<>();

    private SnappingRecyclerView typeRV;

    ForumTypeAdapter mForumTypeAdapter;

    private void PerformForumTypeClick() {
        bottomSheetDialog = new BottomSheetDialog(mActivity);

        View view = getLayoutInflater().inflate(R.layout.forum_type_bottom_sheet, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();
        typeRV = view.findViewById(R.id.typeRV);
        final TextView doneTv = view.findViewById(R.id.doneTv);

        if (list != null) {
            list.clear();
        }
        list.add("open");
        list.add("restricted");

        setTypeAdapter();

        doneTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void setTypeAdapter() {
        final LinearSnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(typeRV);
        typeRV.setOnFlingListener(snapHelper);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        typeRV.setLayoutManager(linearLayoutManager);
        mForumTypeAdapter = new ForumTypeAdapter(mActivity, list, new ForumTypeAdapter.OnClick() {
            @Override
            public void Click(int position) {
            }
        });
        typeRV.setAdapter(mForumTypeAdapter);
        typeRV.setOnViewSelectedListener(new SnappingRecyclerView.OnViewSelectedListener() {
            @Override
            public void onSelected(View view, int position) {
            }
        });
    }

    private void performSendMail() throws IOException {
        /* To get the selected Item list */
        if (JaoharSingleton.getInstance().getmForumUsersList() != null) {
            mUsersListData = JaoharSingleton.getInstance().getmForumUsersList();
        }

        /*
         *Implement API to Submit Data
         */
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                btnSendMAILB.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnSendMAILB.setClickable(true);
                    }
                }, 1500);

                executeADDAPI(mUsersListData);
            }
        }
    }

    /* Check Validations of views */
    public boolean isValidate() {
        boolean flag = true;
        if (editSubjectET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_forum_name));
            flag = false;
        } else if (strForumType.equals("")) {
            showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_choose_forum_type));
            flag = false;
        } else if (strForumType.equals("restricted") && mUsersListData.size() == 0) {
            showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_choose_forum_users));
            flag = false;
        }
        return flag;
    }

    private void checkTextPromptForm() {
        if (txt_prompt_from.getVisibility() == View.VISIBLE) {
            // Its visible
            txt_prompt_from.setVisibility(View.GONE);
            selectUserSpn.setVisibility(View.VISIBLE);
            selectUserSpn.performClick();
        } else {
            txt_prompt_from.setVisibility(View.VISIBLE);
            selectUserSpn.setVisibility(View.GONE);
            // Either gone or invisible
        }
    }
}
