package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.invoices_module.InvVesselAdapter;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.InvoiceVesselDeleteInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class InvVesselActivity extends BaseActivity {
    Activity mActivity = InvVesselActivity.this;
    String TAG = InvVesselActivity.this.getClass().getSimpleName();

    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.imgAdd)
    ImageView imgAdd;
    @BindView(R.id.vesselRV)
    RecyclerView vesselRV;
    ArrayList<VesselSearchInvoiceModel> vesselArrayList = new ArrayList<VesselSearchInvoiceModel>();
    InvVesselAdapter mInvoiceVesselsAdapter;


    InvoiceVesselDeleteInterface mInvoiceVesselDeleteInterface = new InvoiceVesselDeleteInterface() {
        @Override
        public void invoiceVesselDelete(VesselSearchInvoiceModel mModel, int position) {
            deleteConfirmDialog(mModel, position);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inv_vessel);
        ButterKnife.bind(this);
        setStatusBar();

//        setAdapter();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeGetAllVesselsAPI();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            PerformOptionsClick(position);
        }
    };

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_invoice_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);
        View copyView = view.findViewById(R.id.copyView);
        View mailView = view.findViewById(R.id.mailView);
        View downloadView = view.findViewById(R.id.downloadView);
        copyRL.setVisibility(View.GONE);
        mailRL.setVisibility(View.GONE);
        downloadRL.setVisibility(View.GONE);
        copyRL.setVisibility(View.GONE);
        copyView.setVisibility(View.GONE);
        mailView.setVisibility(View.GONE);
        downloadView.setVisibility(View.GONE);



        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, EditVesselActivity.class);
                intent.putExtra("Model", (Serializable) vesselArrayList);
                startActivity(intent);
                overridePendingTransitionEnter();
                dialog.dismiss();
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

//        imgAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent mIntent = new Intent(mActivity, AddCurrencyActivity.class);
//                startActivity(mIntent);
//                overridePendingTransitionEnter();
//            }
//        });
//
//        imgSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mActivity, SearchInvoiceCurrencyActivity.class);
//                intent.putExtra("QuestionListExtra", mArrayList);
//                startActivityForResult(intent, 777);
//                overridePendingTransitionEnter();
//            }
//        });

//        editSearchET.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                charSequence = charSequence.toString().toLowerCase();
//                filteredList.clear();
//                for (int k = 0; k < mArrayList.size(); k++) {
//                    final String text = mArrayList.get(k).getAlias_name().toLowerCase();
//                    if (text.contains(charSequence)) {
//                        filteredList.add(mArrayList.get(k));
//                    }
//                }
//                vesselRV.setLayoutManager(new LinearLayoutManager(mActivity));
//                mInvoiceVesselsAdapter = new CurrenciesAdapter(mActivity, filteredList, mDeleteCurrencyInterface, mEditCurrencyInterface);
//                vesselRV.setAdapter(mInvoiceVesselsAdapter);
//                mInvoiceVesselsAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });



    @OnClick({R.id.llLeftLL, R.id.imgAdd, R.id.imgSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgAdd:
                performAddClick();
                break;
            case R.id.imgSearch:
                performSearchClick();
                break;
        }
    }


    private void performSearchClick() {
//        Intent intent = new Intent(mActivity, SearchInvoiceStampsActivity.class);
//        intent.putExtra("QuestionListExtra", mArrayList);
//        startActivityForResult(intent, 333);
//        overridePendingTransitionEnter();
    }

    private void performAddClick() {
        Intent intent = new Intent(mActivity, AddVesselActivity.class);
        startActivity(intent);
        overridePendingTransitionEnter();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        Log.e(TAG, "Companies: " + vesselArrayList.size());
        vesselRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mInvoiceVesselsAdapter = new InvVesselAdapter(mActivity,vesselArrayList,mOnClickInterface);
        vesselRV.setAdapter(mInvoiceVesselsAdapter);
    }

    private void executeGetAllVesselsAPI() {
        vesselArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceVesselsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            VesselSearchInvoiceModel mModel = new VesselSearchInvoiceModel();
                            mModel.setVessel_id(mJson.getString("vessel_id"));
                            mModel.setVessel_name(mJson.getString("vessel_name"));
                            mModel.setIMO_no(mJson.getString("IMO_no"));
                            mModel.setFlag(mJson.getString("flag"));

                            vesselArrayList.add(mModel);
                        }

                        /*Set Adapter*/
                        setAdapter();

                    } else if (mJsonObject.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    } else if (mJsonObject.getString("status").equals("0")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }
            

            public void deleteConfirmDialog(final VesselSearchInvoiceModel mModel, int position) {
                final Dialog deleteConfirmDialog = new Dialog(mActivity);
                deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
                deleteConfirmDialog.setCanceledOnTouchOutside(true);
                deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
                txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_vessel));
                TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
                TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

                txtConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteConfirmDialog.dismiss();
                        /*Execute Delete API*/
                        executeDeleteAPI(mModel, position);
                    }
                });
                txtCacel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteConfirmDialog.dismiss();
                    }
                });
                deleteConfirmDialog.show();
            }

            public void executeDeleteAPI(VesselSearchInvoiceModel model, int position) {
                AlertDialogManager.showProgressDialog(mActivity);
                ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                Call<StatusMsgModel> call1 = mApiInterface.deleteInvoiceVesselsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), model.getVessel_id());
                call1.enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            vesselArrayList.remove(position);

                            if (mInvoiceVesselsAdapter != null)
                                mInvoiceVesselsAdapter.notifyDataSetChanged();

                        } else if (mModel.getStatus() == 100) {
                            AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
            }
            

            
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the Activity with an OK result
        if (requestCode == 777) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", data.getSerializableExtra("Model"));
                mActivity.setResult(777, returnIntent);
                mActivity.finish();
            }
        }
    }
}
