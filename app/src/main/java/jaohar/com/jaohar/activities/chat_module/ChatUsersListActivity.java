package jaohar.com.jaohar.activities.chat_module;

import static android.view.View.GONE;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.chat_module.ChatUsersListAdapter;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.ChatMoreOptionsInterface;
import jaohar.com.jaohar.interfaces.chat_module.ChatUsersListInterface;
import jaohar.com.jaohar.interfaces.chat_module.ClickChatUsersInterface;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatUsersListActivity extends BaseActivity {
    /*
     * Getting the Class Name
     * */
    String TAG = ChatUsersListActivity.this.getClass().getSimpleName();
    /**
     * Widgets
     */
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.txtRight)
    TextView txtRight;
    @BindView(R.id.newGroupTV)
    TextView newGroupTV;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.usersRV)
    RecyclerView usersRV;
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    int page_no = 1;
    String strLastPage = "TRUE", strPushType = "";
    boolean isSwipeRefresh = false;
    /*
     * Setting Up Array List
     * */
    ArrayList<ChatUsersModel> mLoadMore = new ArrayList<>();
    ArrayList<ChatUsersModel> modelArrayList = new ArrayList<>();
    /*
     * Setting Up Adapter
     * */
    ChatUsersListAdapter mChatUsersListAdapter;
    /**
     * Initialize the Activity
     */
    private final Activity mActivity = ChatUsersListActivity.this;
    ClickChatUsersInterface mClickChatUsersInterface = new ClickChatUsersInterface() {
        @Override
        public void mChatUsersListInterface(int position, ArrayList<ChatUsersModel> modelArrayList) {
            String FullName = modelArrayList.get(position).getName();
            String ReceiverID = modelArrayList.get(position).getReceiver_id();
            String ProfilePic = modelArrayList.get(position).getImage();

            mActivity.startActivity(new Intent(mActivity, ChatMessagesListActivity.class)
                    .putExtra(JaoharConstants.USER_NAME, FullName)
                    .putExtra(JaoharConstants.USER_FIRST_NAME, modelArrayList.get(position).getFirst_name())
                    .putExtra(JaoharConstants.USER_LAST_NAME, modelArrayList.get(position).getLast_name())
                    .putExtra(JaoharConstants.ROOM_ID, modelArrayList.get(position).getRoom_id())
                    .putExtra(JaoharConstants.RECEIVER_ID, ReceiverID)
                    .putExtra(JaoharConstants.RECEIVER_PIC, ProfilePic)
                    .putExtra(JaoharConstants.GROUP_ID, modelArrayList.get(position).getGroup_id())
                    .putExtra(JaoharConstants.ONLINE_STATE, modelArrayList.get(position).getOnline_state())
                    .putExtra(JaoharConstants.ROLE, modelArrayList.get(position).getRole()));
        }
    };

    ChatMoreOptionsInterface mChatMoreOptionsInterface = new ChatMoreOptionsInterface() {
        @Override
        public void mChatMoreOptionsInterface(int position, ChatUsersModel chatUsersModel) {
            PerformMoreClick(position, chatUsersModel);
        }
    };

    /**
     * Recycler View Pagination Adapter Interface
     **/
    ChatUsersListInterface mPaginationInterFace = new ChatUsersListInterface() {
        @Override
        public void mChatUsersListInterface(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_no;
                            executeAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };

    private void PerformMoreClick(final int position, final ChatUsersModel chatUsersModel) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_chat_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        TextView groupInfoTV = view.findViewById(R.id.groupInfoTV);
        TextView editGroupTV = view.findViewById(R.id.editGroupTV);
        TextView clearChatTV = view.findViewById(R.id.clearChatTV);
        TextView deleteGroupTV = view.findViewById(R.id.deleteGroupTV);
        TextView cancelTV = view.findViewById(R.id.cancelTV);

        if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "").equals(modelArrayList.get(position).getAdmin_id())) {
            editGroupTV.setVisibility(View.VISIBLE);
            deleteGroupTV.setVisibility(View.VISIBLE);
        } else {

            if (modelArrayList.get(position).getGroup_id().equals("0")) {
                groupInfoTV.setVisibility(GONE);
                editGroupTV.setVisibility(View.GONE);
                deleteGroupTV.setVisibility(GONE);
            } else {
                editGroupTV.setVisibility(View.GONE);
                deleteGroupTV.setVisibility(GONE);
            }
        }

        groupInfoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(mActivity, ChatGroupInfoActivity.class)
                        .putExtra(JaoharConstants.GROUP_ID, chatUsersModel.getGroup_id()));

            }
        });

        editGroupTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(mActivity, EditGroupActivity.class)
                        .putExtra(JaoharConstants.GROUP_ID, modelArrayList.get(position).getGroup_id())
                        .putExtra(JaoharConstants.GROUP_NAME, modelArrayList.get(position).getName())
                        .putExtra(JaoharConstants.GROUP_IMAGE, modelArrayList.get(position).getImage()));
            }
        });

        clearChatTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteChatConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_clear_chat), modelArrayList.get(position).getRoom_id(), position);
            }
        });

        deleteGroupTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                deleteGroupConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_group), chatUsersModel.getGroup_id(), position);
            }
        });

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_users_list);
        ButterKnife.bind(this);

        //set status bar
        setStatusBar();

        /* swipe refresh at top */
        swipeToRefresh.setColorSchemeResources(R.color.colorPrimary);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;

                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (modelArrayList != null)
                    modelArrayList.clear();

                setAdapter(modelArrayList);
                if (editSearchET.getText().toString().length() == 0) {
                    page_no = 1;
                    executeAPI();
                } else {
                    executeChatGroupsSearch(editSearchET.getText().toString());
                }
            }
        });

        /*Retrive Data from Privious Activity*/
        if (getIntent() != null) {
            if (getIntent().getStringExtra(JaoharConstants.NOTIFICATION_TYPE) != null) {
                strPushType = getIntent().getStringExtra(JaoharConstants.NOTIFICATION_TYPE);
            }
        }
    }

    /**
     * Set Widget IDS
     **/
    @Override
    protected void setViewsIDs() {
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.chat));
        txtCenter.setGravity(Gravity.START);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(100, 0, 0, 0);
        txtCenter.setLayoutParams(params);

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event == null || event.getAction() != KeyEvent.ACTION_DOWN) {
                    //do something

                    if (modelArrayList != null)
                        modelArrayList.clear();

                    executeChatGroupsSearch(editSearchET.getText().toString());
                }
                return false;
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 0) {
                    if (modelArrayList != null)
                        modelArrayList.clear();

                    if (progressDialog != null && progressDialog.isShowing()) {
                        AlertDialogManager.hideProgressDialog();
                    }
                    executeAPI();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick({R.id.llLeftLL, R.id.newGroupTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.newGroupTV:
                performNewGroupClick();
                break;
        }
    }

    private void performNewGroupClick() {
        startActivity(new Intent(mActivity, AddParticipantActivity.class));
    }

    public void setAdapter(ArrayList<ChatUsersModel> modelArrayList) {
        usersRV.setNestedScrollingEnabled(false);
        usersRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mChatUsersListAdapter = new ChatUsersListAdapter(mActivity, modelArrayList, mPaginationInterFace, mClickChatUsersInterface, mChatMoreOptionsInterface);
        usersRV.setAdapter(mChatUsersListAdapter);
    }

    /* *
     * Execute API for getting Users list
     * @param
     * @user_id
     * */
    public void executeAPI() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                if (isSwipeRefresh) {

                } else {
                    AlertDialogManager.showProgressDialog(mActivity);
                }
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllChatRooms(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), page_no);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (page_no == 1) {
                    AlertDialogManager.hideProgressDialog();
                }
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                Log.e(TAG, "***URLResponce***" + response.body().toString());
                parseResponce(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");

            if (strStatus.equals("1")) {

                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_users");
                if (mjsonArrayData != null) {

                    ArrayList<ChatUsersModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();

                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ChatUsersModel mModel = new ChatUsersModel();

                        if (mJsonDATA.has("id")) {
                            if (!mJsonDATA.getString("id").equals("")) {
                                mModel.setId(mJsonDATA.getString("id"));
                            }
                        }
                        if (mJsonDATA.has("room_id")) {
                            if (!mJsonDATA.getString("room_id").equals("")) {
                                mModel.setRoom_id(mJsonDATA.getString("room_id"));
                            }
                        }
                        if (mJsonDATA.has("sender_id")) {
                            if (!mJsonDATA.getString("sender_id").equals("")) {
                                mModel.setSender_id(mJsonDATA.getString("sender_id"));
                            }
                        }
                        if (mJsonDATA.has("receiver_id")) {
                            if (!mJsonDATA.getString("receiver_id").equals("")) {
                                mModel.setReceiver_id(mJsonDATA.getString("receiver_id"));
                            }
                        }
                        if (mJsonDATA.has("room_status")) {
                            if (!mJsonDATA.getString("room_status").equals("")) {
                                mModel.setRoom_status(mJsonDATA.getString("room_status"));
                            }
                        }
                        if (mJsonDATA.has("request_time")) {
                            if (!mJsonDATA.getString("request_time").equals("")) {
                                mModel.setRequest_time(mJsonDATA.getString("request_time"));
                            }
                        }
                        if (mJsonDATA.has("room_type")) {
                            if (!mJsonDATA.getString("room_type").equals("")) {
                                mModel.setRoom_type(mJsonDATA.getString("room_type"));
                            }
                        }
                        if (mJsonDATA.has("admin_id")) {
                            if (!mJsonDATA.getString("admin_id").equals("")) {
                                mModel.setAdmin_id(mJsonDATA.getString("admin_id"));
                            }
                        }
                        if (mJsonDATA.has("last_message")) {
                            if (!mJsonDATA.getString("last_message").equals("")) {
                                mModel.setLast_message(mJsonDATA.getString("last_message"));
                            }
                        }
                        if (mJsonDATA.has("name")) {
                            if (!mJsonDATA.getString("name").equals("")) {
                                mModel.setName(mJsonDATA.getString("name"));
                            }
                        }
                        if (mJsonDATA.has("group_id")) {
                            if (!mJsonDATA.getString("group_id").equals("")) {
                                mModel.setGroup_id(mJsonDATA.getString("group_id"));
                            }
                        }
                        if (mJsonDATA.has("message_time")) {
                            if (!mJsonDATA.getString("message_time").equals("")) {
                                mModel.setMessage_time(mJsonDATA.getString("message_time"));
                            }
                        }
                        if (mJsonDATA.has("image")) {
                            if (!mJsonDATA.getString("image").equals("")) {
                                mModel.setImage(mJsonDATA.getString("image"));
                            }
                        }
                        if (mJsonDATA.has("role")) {
                            if (!mJsonDATA.getString("role").equals("")) {
                                mModel.setRole(mJsonDATA.getString("role"));
                            }
                        }
                        if (mJsonDATA.has("message")) {
                            if (!mJsonDATA.getString("message").equals("")) {
                                mModel.setMessage(mJsonDATA.getString("message"));
                            }
                        }
                        if (mJsonDATA.has("unread_chat")) {
                            if (!mJsonDATA.getString("unread_chat").equals("")) {
                                mModel.setUnread_chat(mJsonDATA.getString("unread_chat"));
                            }
                        }
                        if (mJsonDATA.has("online_state")) {
                            if (!mJsonDATA.getString("online_state").equals("")) {
                                mModel.setOnline_state(mJsonDATA.getString("online_state"));
                            }
                        }
                        if (mJsonDATA.has("email")) {
                            if (!mJsonDATA.getString("email").equals("")) {
                                mModel.setEmail(mJsonDATA.getString("email"));
                            }
                        }
                        if (mJsonDATA.has("company_name")) {
                            if (!mJsonDATA.getString("company_name").equals("")) {
                                mModel.setCompany_name(mJsonDATA.getString("company_name"));
                            }
                        }
                        if (mJsonDATA.has("first_name")) {
                            if (!mJsonDATA.getString("first_name").equals("")) {
                                mModel.setFirst_name(mJsonDATA.getString("first_name"));
                            }
                        }
                        if (mJsonDATA.has("last_name")) {
                            if (!mJsonDATA.getString("last_name").equals("")) {
                                mModel.setLast_name(mJsonDATA.getString("last_name"));
                            }
                        }
                        if (mJsonDATA.has("job")) {
                            if (!mJsonDATA.getString("job").equals("")) {
                                mModel.setJob(mJsonDATA.getString("job"));
                            }
                        }
                        if (mJsonDATA.has("chat_status")) {
                            if (!mJsonDATA.getString("chat_status").equals("")) {
                                mModel.setChat_status(mJsonDATA.getString("chat_status"));
                            }
                        }
                        mTempraryList.add(mModel);
                    }
                    modelArrayList.addAll(mTempraryList);
                    if (page_no == 1) {
                        setAdapter(modelArrayList);
                    } else {
                        mChatUsersListAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* *
     * Execute API for Delete chat groups
     * @param
     * @message_id
     * @user_id
     * */
    private void executeDeleteGroupAPI(final String groupId, final int pos) {
        AlertDialogManager.showProgressDialog(mActivity);
        String strUrl = JaoharConstants.DeleteGroup + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&id=" + groupId;
        Log.e(TAG, "***URL***" + strUrl);
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                try {
                    JSONObject mJsonDATA = new JSONObject(response);
                    if (mJsonDATA.getString("status").equals("1")) {
                        modelArrayList.remove(pos);
                        setAdapter(modelArrayList);
                    } else {
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonDATA.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    /* *
     * PopUp to Delete or Cancel the Message
     * */
    public void deleteChatConfirmDialog(String strMessage, final String roomId, final int pos) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Blog Category
                 * */
                executeDeleteChatAPI(roomId, pos);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    public void deleteGroupConfirmDialog(String strMessage, final String groupId, final int pos) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Blog Category
                 * */
                executeDeleteGroupAPI(groupId, pos);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeDeleteChatAPI(String strRoomID, final int pos) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.deleteChat(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strRoomID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                try {
                    JSONObject mJsonDATA = new JSONObject(response.body().toString());
                    if (mJsonDATA.getInt("status") == 1) {

                        modelArrayList.get(pos).setMessage("");
                        modelArrayList.get(pos).setUnread_chat("0");
                        mChatUsersListAdapter.notifyItemChanged(pos);

                    } else {
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonDATA.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });

    }

    /* *
     * Execute API for Search chat groups
     * */
    private void executeChatGroupsSearch(String searchText) {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
//                AlertDialogManager.showProgressDialog(mActivity);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Log.e("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        Log.e("search_keyword", searchText);
        Call<JsonObject> call1 = mApiInterface.searchChatRoom(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), searchText);

        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {

                Log.e(TAG, "*****Response****" + response);

                if (page_no == 1) {
                    AlertDialogManager.hideProgressDialog();
                }

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }

                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }

                parseResponce(response.body().toString());

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (!strPushType.equals("") && strPushType.equals(JaoharConstants.PUSH)) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
            startActivity(mIntent);
            finish();
        }
    }

    /* *
     * Execute API for getting Users list
     * @param
     * @user_id
     * */
    public void executeResetChatRoomsAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.ResetChatRoomsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), page_no);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "***URLResponce***" + response.body().toString());
                parseResetChatRoomsResponce(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    void parseResetChatRoomsResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_users");
                if (mjsonArrayData != null) {
                    ArrayList<ChatUsersModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ChatUsersModel mModel = new ChatUsersModel();

                        if (mJsonDATA.has("id")) {
                            if (!mJsonDATA.getString("id").equals("")) {
                                mModel.setId(mJsonDATA.getString("id"));
                            }
                        }
                        if (mJsonDATA.has("room_id")) {
                            if (!mJsonDATA.getString("room_id").equals("")) {
                                mModel.setRoom_id(mJsonDATA.getString("room_id"));
                            }
                        }
                        if (mJsonDATA.has("sender_id")) {
                            if (!mJsonDATA.getString("sender_id").equals("")) {
                                mModel.setSender_id(mJsonDATA.getString("sender_id"));
                            }
                        }
                        if (mJsonDATA.has("receiver_id")) {
                            if (!mJsonDATA.getString("receiver_id").equals("")) {
                                mModel.setReceiver_id(mJsonDATA.getString("receiver_id"));
                            }
                        }
                        if (mJsonDATA.has("room_status")) {
                            if (!mJsonDATA.getString("room_status").equals("")) {
                                mModel.setRoom_status(mJsonDATA.getString("room_status"));
                            }
                        }
                        if (mJsonDATA.has("request_time")) {
                            if (!mJsonDATA.getString("request_time").equals("")) {
                                mModel.setRequest_time(mJsonDATA.getString("request_time"));
                            }
                        }
                        if (mJsonDATA.has("room_type")) {
                            if (!mJsonDATA.getString("room_type").equals("")) {
                                mModel.setRoom_type(mJsonDATA.getString("room_type"));
                            }
                        }
                        if (mJsonDATA.has("admin_id")) {
                            if (!mJsonDATA.getString("admin_id").equals("")) {
                                mModel.setAdmin_id(mJsonDATA.getString("admin_id"));
                            }
                        }
                        if (mJsonDATA.has("last_message")) {
                            if (!mJsonDATA.getString("last_message").equals("")) {
                                mModel.setLast_message(mJsonDATA.getString("last_message"));
                            }
                        }
                        if (mJsonDATA.has("name")) {
                            if (!mJsonDATA.getString("name").equals("")) {
                                mModel.setName(mJsonDATA.getString("name"));
                            }
                        }
                        if (mJsonDATA.has("group_id")) {
                            if (!mJsonDATA.getString("group_id").equals("")) {
                                mModel.setGroup_id(mJsonDATA.getString("group_id"));
                            }
                        }
                        if (mJsonDATA.has("message_time")) {
                            if (!mJsonDATA.getString("message_time").equals("")) {
                                mModel.setMessage_time(mJsonDATA.getString("message_time"));
                            }
                        }
                        if (mJsonDATA.has("image")) {
                            if (!mJsonDATA.getString("image").equals("")) {
                                mModel.setImage(mJsonDATA.getString("image"));
                            }
                        }
                        if (mJsonDATA.has("role")) {
                            if (!mJsonDATA.getString("role").equals("")) {
                                mModel.setRole(mJsonDATA.getString("role"));
                            }
                        }
                        if (mJsonDATA.has("message")) {
                            if (!mJsonDATA.getString("message").equals("")) {
                                mModel.setMessage(mJsonDATA.getString("message"));
                            }
                        }
                        if (mJsonDATA.has("unread_chat")) {
                            if (!mJsonDATA.getString("unread_chat").equals("")) {
                                mModel.setUnread_chat(mJsonDATA.getString("unread_chat"));
                            }
                        }
                        if (mJsonDATA.has("online_state")) {
                            if (!mJsonDATA.getString("online_state").equals("")) {
                                mModel.setOnline_state(mJsonDATA.getString("online_state"));
                            }
                        }
                        if (mJsonDATA.has("email")) {
                            if (!mJsonDATA.getString("email").equals("")) {
                                mModel.setEmail(mJsonDATA.getString("email"));
                            }
                        }
                        if (mJsonDATA.has("company_name")) {
                            if (!mJsonDATA.getString("company_name").equals("")) {
                                mModel.setCompany_name(mJsonDATA.getString("company_name"));
                            }
                        }
                        if (mJsonDATA.has("first_name")) {
                            if (!mJsonDATA.getString("first_name").equals("")) {
                                mModel.setFirst_name(mJsonDATA.getString("first_name"));
                            }
                        }
                        if (mJsonDATA.has("last_name")) {
                            if (!mJsonDATA.getString("last_name").equals("")) {
                                mModel.setLast_name(mJsonDATA.getString("last_name"));
                            }
                        }
                        if (mJsonDATA.has("job")) {
                            if (!mJsonDATA.getString("job").equals("")) {
                                mModel.setJob(mJsonDATA.getString("job"));
                            }
                        }
                        if (mJsonDATA.has("chat_status")) {
                            if (!mJsonDATA.getString("chat_status").equals("")) {
                                mModel.setChat_status(mJsonDATA.getString("chat_status"));
                            }
                        }
                        mTempraryList.add(mModel);
                    }
                    if (modelArrayList != null) {
                        modelArrayList.clear();
                    }
                    modelArrayList.addAll(mTempraryList);
                    if (mChatUsersListAdapter != null) {
                        mChatUsersListAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute API For Getting List *//*
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
            if (modelArrayList != null)
                modelArrayList.clear();

            if (editSearchET.getText().toString().length() == 0) {
                page_no = 1;
                executeAPI();
            } else {
                executeChatGroupsSearch(editSearchET.getText().toString());
            }
        }

        /* implement api after few seconds */
        resetUsersData();
    }

    Handler handler = new Handler();
    Runnable runnable;
    int delay = 5 * 1000; //Delay for 15 seconds.  One second = 1000 milliseconds.

    private void resetUsersData() {
        //start handler as activity become visible
        handler.postDelayed(runnable = new Runnable() {
            public void run() {

                if (editSearchET.getText().toString().length() == 0) {
                    //do something
                    executeResetChatRoomsAPI();
                }

                handler.postDelayed(runnable, delay);
            }
        }, delay);
    }

    // If onPause() is not included the threads will double up when you
    // reload the activity
    @Override
    protected void onPause() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        handler.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
}
