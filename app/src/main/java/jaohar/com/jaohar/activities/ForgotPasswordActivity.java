package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class ForgotPasswordActivity extends BaseActivity {
    Activity mActivity = ForgotPasswordActivity.this;
    String TAG = ForgotPasswordActivity.this.getClass().getSimpleName();

    //Widgets
    EditText editEmailET;
    Button btnSignInB;
    String strLoginType;
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_forgot_password);

        if (getIntent() != null) {
            strLoginType = getIntent().getStringExtra(JaoharConstants.LOGIN_TYPE);
        }

    }


    @Override
    protected void setViewsIDs() {
        editEmailET = (EditText) findViewById(R.id.editEmailET);
        btnSignInB = (Button) findViewById(R.id.btnSignInB);
        txtCenter =  findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.reset_password));
        llLeftLL =  findViewById(R.id.llLeftLL);
        imgBack =  findViewById(R.id.imgBack);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
    }

    @Override
    protected void setClickListner() {
        btnSignInB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utilities.isValidEmaillId(editEmailET.getText().toString()) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else {
                    /*Execute Api*///?email=
                    executeAPI();
                }
            }
        });


        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

//    private void executeAPI() {
//        String strUrl = JaoharConstants.FORGOT_PASSWORD_API + "?email=" + editEmailET.getText().toString();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void executeAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.forgotPasswordRequest(editEmailET.getText().toString());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" +t.getMessage());
            }
        });



    }


    @Override
    public void onBackPressed() {
        if (strLoginType.equals(JaoharConstants.STAFF)) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "SignUP");
            mIntent.putExtra("TYPE", JaoharConstants.STAFF);
            mActivity.startActivity(mIntent);
            mActivity.finish();
        } else if (strLoginType.equals(JaoharConstants.USER)) {
            mActivity.finish();
            overridePendingTransitionExit();
        } else if (strLoginType.equals("VesselForSaleActivity")) {
            mActivity.finish();
            overridePendingTransitionExit();
        }else if (strLoginType.equals(JaoharConstants.ADMIN)){
            mActivity.finish();
            overridePendingTransitionExit();
        }
    }


    public void showAccountDiableDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strLoginType.equals(JaoharConstants.STAFF)) {
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "SignUP");
                    mIntent.putExtra("TYPE", JaoharConstants.STAFF);
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
                } else if (strLoginType.equals(JaoharConstants.USER)) {
                    mActivity.finish();
                    overridePendingTransitionExit();
                } else if (strLoginType.equals("VesselForSaleActivity")) {
                    mActivity.finish();
                    overridePendingTransitionExit();
                }else if (strLoginType.equals(JaoharConstants.ADMIN)){
                    mActivity.finish();
                    overridePendingTransitionExit();
                }
            }
        });
        alertDialog.show();
    }


}
