package jaohar.com.jaohar.activities.chat_module;

import static android.view.View.GONE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.chat_module.AllChatParticipantsAdapter;
import jaohar.com.jaohar.adapters.chat_module.SelectedUsersAdapter;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.ChatUsersListInterface;
import jaohar.com.jaohar.interfaces.chat_module.ClickChatUsersInterface;
import jaohar.com.jaohar.interfaces.chat_module.RemoveParticipantInterface;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class EditGroupActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = EditGroupActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = EditGroupActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.txtCancelTV)
    TextView txtCancelTV;
    @BindView(R.id.txtNextTV)
    TextView txtNextTV;
    @BindView(R.id.selectedUsersRV)
    RecyclerView selectedUsersRV;
    @BindView(R.id.groupNameET)
    EditText groupNameET;
    @BindView(R.id.addImageRL)
    RelativeLayout addImageRL;
    @BindView(R.id.imgImageIV)
    ImageView imgImageIV;
    @BindView(R.id.NoOfParticipantsTV)
    TextView NoOfParticipantsTV;
    @BindView(R.id.allUsersRV)
    RecyclerView allUsersRV;
    @BindView(R.id.txtDeleteTV)
    TextView txtDeleteTV;
    @BindView(R.id.DifView)
    View DifView;
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;

    /*
     * Setting Up Adapter
     * */
    SelectedUsersAdapter mSelectedUsersAdapter;
    AllChatParticipantsAdapter mChatUsersListAdapter;



    ArrayList<ChatUsersModel> mSelectedArrayList = new ArrayList<>();
    ArrayList<ChatUsersModel> modelArrayList = new ArrayList<>();

    ArrayList<String> mSelectedist = new ArrayList<>();

    String strLastPage = "TRUE";
    int page_no = 1;

    /*
     * Some Variable and data Types
     * */
    String mCurrentPhotoPath, mStoragePath = "", strBase64 = "", strGroupId = "";
    Bitmap thumb = null;

    // Permissions Value
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    // Permissions
    private String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String CAMERA = Manifest.permission.CAMERA;

    /**
     * Recycler View Pagination Adapter Interface
     **/
    ChatUsersListInterface mPaginationInterFace = new ChatUsersListInterface() {
        @Override
        public void mChatUsersListInterface(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_no;
                            executeUsersAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };

    ClickChatUsersInterface mClickChatUsersInterface = new ClickChatUsersInterface() {
        @Override
        public void mChatUsersListInterface(int position, ArrayList<ChatUsersModel> mModelArrayList) {
            if (!mSelectedist.contains(mModelArrayList.get(position).getId())) {
                mSelectedArrayList.add(mModelArrayList.get(position));
                mSelectedist.add(mModelArrayList.get(position).getId());
                setSelectedUsersAdapter(mSelectedArrayList, "1");
            }

            NoOfParticipantsTV.setText(String.valueOf(mSelectedist.size()));

            if (mSelectedist.size() > 0) {
                DifView.setVisibility(View.VISIBLE);
            } else {
                DifView.setVisibility(GONE);
            }
        }
    };

    RemoveParticipantInterface removeParticipantInterface = new RemoveParticipantInterface() {
        @Override
        public void mRemoveParticipantInterface(int position, ArrayList<ChatUsersModel> modelArrayList) {

            if (mSelectedist != null) {
                mSelectedist.clear();

                for (int k = 0; k < modelArrayList.size(); k++) {
                    mSelectedist.add(modelArrayList.get(k).getId());
                }
            }
            NoOfParticipantsTV.setText(String.valueOf(mSelectedist.size()));

            if (mSelectedist.size() > 0) {
                DifView.setVisibility(View.VISIBLE);
            } else {
                DifView.setVisibility(GONE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        setStatusBar();
        setContentView(R.layout.activity_edit_group);
        ButterKnife.bind(this);
        getIntentData();
      //  setClickListeners();

        if (mSelectedist != null)
            mSelectedist.clear();

        executeDetailsAPI();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            strGroupId = getIntent().getStringExtra(JaoharConstants.GROUP_ID);
        }
    }

    @OnClick({R.id.txtCancelTV, R.id.txtNextTV, R.id.imgImageIV, R.id.addImageRL})
    public  void onViewCLicked(View view)
    {
        switch (view.getId())
        {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtNextTV:
                nextClick();
                break;
            case R.id.imgImageIV:
                mToGrantPermission();
                break;
            case R.id.addImageRL:
                mToGrantPermission();
                break;
        }
    }

    private void nextClick() {
        if (isValidate()) {
            executeEditChatGroupAPI(mSelectedist);
        }
    }




    public void setSelectedUsersAdapter(ArrayList<ChatUsersModel> selectedArrayList, String type) {
        selectedUsersRV.setNestedScrollingEnabled(false);
        selectedUsersRV.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.HORIZONTAL, false));
        mSelectedUsersAdapter = new SelectedUsersAdapter(mActivity, selectedArrayList, type, removeParticipantInterface);
        selectedUsersRV.setAdapter(mSelectedUsersAdapter);
    }

    private Boolean isValidate() {
        boolean flag = true;

        if (groupNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_group_name));
            flag = false;
        } else if (mSelectedArrayList.size() == 0) {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_choose_participants));
            flag = false;
        }
        return flag;
    }

    public void mToGrantPermission() {
        if (mCheckPermission()) {
            openCameraGalleryDialog();
        } else {
            mRequestPermission();
        }
    }

    private boolean mCheckPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int read_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int write_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return camera == PackageManager.PERMISSION_GRANTED && read_external_storage == PackageManager.PERMISSION_GRANTED && write_external_storage == PackageManager.PERMISSION_GRANTED;
    }

    private void mRequestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 100);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            //All Permissions Granted
//                            openCameraGalleryDialog();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            Toast.makeText(mActivity, getString(R.string.goto_settings), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }


    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView tittleTV = (TextView) dialog.findViewById(R.id.tittleTV);
        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_files.setVisibility(View.GONE);
        tittleTV.setVisibility(View.GONE);
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });

        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void openInternalStorage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf|text/plain");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 606);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            Bitmap bitmap;
                            Uri uri = data.getData();
                            File finalFile = new File(getRealPathFromURI(uri));

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 0;

                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                            ExifInterface exifInterface = null;
                            try {
                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            Matrix matrix = new Matrix();
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                            }
                            JaoharConstants.IS_camera_Click = false;
                            thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                            addImageRL.setVisibility(GONE);
                            imgImageIV.setVisibility(View.VISIBLE);

                            imgImageIV.setImageBitmap(thumb);
                            strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected*****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {
                    JaoharConstants.IS_camera_Click = true;
                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;

                    addImageRL.setVisibility(GONE);
                    imgImageIV.setVisibility(View.VISIBLE);

                    imgImageIV.setImageBitmap(thumb);
                    strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * Show Alert Dailog Box
     * */
    public void showAddGroupAlertDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                Intent mIntent = new Intent(mActivity, ChatUsersListActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);
            }
        });
        alertDialog.show();
    }
   Map<String, String> mParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        params.put("group_id", strGroupId);
        params.put("group_name", groupNameET.getText().toString().trim());
        params.put("user_list", String.valueOf(mSelectedist));
        params.put("photo", strBase64);
        Log.e(TAG, "**PARAMS**" + params);
        return params;
    }
    public void executeEditChatGroupAPI(final ArrayList<String> mStringArray) {
        AlertDialogManager.showProgressDialog(mActivity);
        String mApiUrl = JaoharConstants.EditChatGroup;
        Log.e(TAG, "**Api Url**" + mApiUrl);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.editChatGroup1(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),strGroupId, groupNameET.getText().toString().trim(),
                String.valueOf(mSelectedist),strBase64);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "==Response==" + response);
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    AlertDialogManager.hideProgressDialog();
                    String status = String.valueOf(jsonObject.getInt("status"));
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        showAddGroupAlertDialog(mActivity, getString(R.string.app_name), "" + getString(R.string.group_updated_successfully));
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");

            }
        });

//        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
//        mApiInterface.editChatGroup(mParams()).enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
//                Log.e(TAG, "==Response==" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response.body().toString());
//                    AlertDialogManager.hideProgressDialog();
//                    String status = String.valueOf(jsonObject.getInt("status"));
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        showAddGroupAlertDialog(mActivity, getString(R.string.app_name), "" + getString(R.string.group_updated_successfully));
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                Log.e(TAG, "***Error**" + t.getMessage());
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
//            }
//        });

    }

    /* *
     * Execute API for getting group details
     * @param
     * @user_id
     * */
    public void executeDetailsAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getGroupDetail(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),strGroupId);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                parseResponce(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
        
    }

    void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");

                ChatUsersModel mModel = new ChatUsersModel();

                if (!mjsonDATA.getString("group_id").equals("")) {
                    mModel.setGroup_id(mjsonDATA.getString("group_id"));
                }
                if (!mjsonDATA.getString("admin_id").equals("")) {
                    mModel.setAdmin_id(mjsonDATA.getString("admin_id"));
                }
                if (!mjsonDATA.getString("room_id").equals("")) {
                    mModel.setRoom_id(mjsonDATA.getString("room_id"));
                }
                if (!mjsonDATA.getString("chat_users").equals("")) {
                    mModel.setChat_users(mjsonDATA.getString("chat_users"));
                }
                if (!mjsonDATA.getString("group_name").equals("")) {
                    mModel.setName(mjsonDATA.getString("group_name"));
                    groupNameET.setText(mjsonDATA.getString("group_name"));

                    groupNameET.setSelection(groupNameET.getText().toString().length());
                }
                if (!mjsonDATA.getString("group_image").equals("")) {
                    mModel.setImage(mjsonDATA.getString("group_image"));
                    Glide.with(mActivity).load(mjsonDATA.getString("group_image")).placeholder(R.drawable.profile)
                            .into(imgImageIV);
                    imgImageIV.setVisibility(View.VISIBLE);
                    addImageRL.setVisibility(GONE);
                } else {
                    imgImageIV.setVisibility(GONE);
                    addImageRL.setVisibility(View.VISIBLE);
                }
                if (!mjsonDATA.getString("room_id").equals("")) {
                    mModel.setRoom_id(mjsonDATA.getString("room_id"));
                }

                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("participants");
                if (mjsonArrayData != null) {

                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ChatUsersModel mParticipantsModel = new ChatUsersModel();

                        if (!mJsonDATA.getString("id").equals("")) {
                            mParticipantsModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("role").equals("")) {
                            mParticipantsModel.setRole(mJsonDATA.getString("role"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("company_name").equals("")) {
                            mParticipantsModel.setCompany_name(mJsonDATA.getString("company_name"));
                        }
                        if (!mJsonDATA.getString("first_name").equals("")) {
                            mParticipantsModel.setFirst_name(mJsonDATA.getString("first_name"));
                        }
                        if (!mJsonDATA.getString("last_name").equals("")) {
                            mParticipantsModel.setLast_name(mJsonDATA.getString("last_name"));
                        }
                        if (!mJsonDATA.getString("job").equals("")) {
                            mParticipantsModel.setJob(mJsonDATA.getString("job"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("image").equals("")) {
                            mParticipantsModel.setImage(mJsonDATA.getString("image"));
                        }
                        if (!mJsonDATA.getString("chat_status").equals("")) {
                            mParticipantsModel.setChat_status(mJsonDATA.getString("chat_status"));
                        }
                        if (!mJsonDATA.getString("image").equals("")) {
                            mParticipantsModel.setImage(mJsonDATA.getString("image"));
                        }
                        mSelectedArrayList.add(mParticipantsModel);
                    }

                    NoOfParticipantsTV.setText(String.valueOf(mSelectedArrayList.size()));

                    for (int k = 0; k < mSelectedArrayList.size(); k++) {
                        mSelectedist.add(mSelectedArrayList.get(k).getId());
                    }

                    setSelectedUsersAdapter(mSelectedArrayList, "1");

                    executeUsersAPI();
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /* *
     * Execute API for getting Users list
     * @param
     * @user_id
     * */
    public void executeUsersAPI() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllChatUsers(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),page_no);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "***URLResponce***" + response);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
          }

    void parseResponse(String response) {
        try {
            JSONObject mJSonObject = new JSONObject(response);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_users");
                if (mjsonArrayData != null) {
                    ArrayList<ChatUsersModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ChatUsersModel mModel = new ChatUsersModel();
                        if (!mJsonDATA.getString("id").equals("")) {
                            mModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("role").equals("")) {
                            mModel.setRole(mJsonDATA.getString("role"));
                        }
                        if (!mJsonDATA.getString("company_name").equals("")) {
                            mModel.setCompany_name(mJsonDATA.getString("company_name"));
                        }
                        if (!mJsonDATA.getString("first_name").equals("")) {
                            mModel.setFirst_name(mJsonDATA.getString("first_name"));
                        }
                        if (!mJsonDATA.getString("last_name").equals("")) {
                            mModel.setLast_name(mJsonDATA.getString("last_name"));
                        }
                        if (!mJsonDATA.getString("job").equals("")) {
                            mModel.setJob(mJsonDATA.getString("job"));
                        }
                        if (!mJsonDATA.getString("image").equals("")) {
                            mModel.setImage(mJsonDATA.getString("image"));
                        }
                        if (!mJsonDATA.getString("chat_status").equals("")) {
                            mModel.setChat_status(mJsonDATA.getString("chat_status"));
                        }
                        if (!mJsonDATA.getString("unread_chat").equals("")) {
                            mModel.setUnread_chat(mJsonDATA.getString("unread_chat"));
                        }
                        if (!mJsonDATA.getString("online_state").equals("")) {
                            mModel.setOnline_state(mJsonDATA.getString("online_state"));
                        }
                        if (!mJsonDATA.getString("message_time").equals("")) {
                            mModel.setMessage_time(mJsonDATA.getString("message_time"));
                        }
                        if (!mJsonDATA.getString("message").equals("")) {
                            mModel.setMessage(mJsonDATA.getString("message"));
                        }
                        if (!mJsonDATA.getString("room_id").equals("")) {
                            mModel.setRoom_id(mJsonDATA.getString("room_id"));
                        }
                        mTempraryList.add(mModel);
                    }
                    modelArrayList.addAll(mTempraryList);
                    if (page_no == 1) {
                        setAdapter();
                    } else {
                        mChatUsersListAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setAdapter() {
        allUsersRV.setNestedScrollingEnabled(false);
        allUsersRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mChatUsersListAdapter = new AllChatParticipantsAdapter(mActivity, modelArrayList, mPaginationInterFace, mClickChatUsersInterface);
        allUsersRV.setAdapter(mChatUsersListAdapter);
    }
}