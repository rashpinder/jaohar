package jaohar.com.jaohar.activities;

import static jaohar.com.jaohar.utils.JaoharConstants.IS_INVOICE_UPLOADED;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.InVoiceItemsAdapter;
import jaohar.com.jaohar.adapters.InvoiceTotalDiscountAdapter;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.PriviewModelItems;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.DeleteItemInvoiceInterface;
import jaohar.com.jaohar.interfaces.EditInvoiceItemData;
import jaohar.com.jaohar.interfaces.EditTotalDiscountItemdata;
import jaohar.com.jaohar.models.InvoiceModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddInvoiceActivity extends BaseActivity {
    Activity mActivity = AddInvoiceActivity.this;
    String TAG = AddInvoiceActivity.this.getClass().getSimpleName();

    ImageView imgBack;
    LinearLayout llLeftLL;
    TextView txtCenter;
    EditText editInVoiceNumET, editInVoiceDateET, editTermsDaysET, editSearchCompanyET, /*editSearchVesselsET,*/
            editCurrencyET, editStampET, editSignInvoiceET, editStatusET, editRefrance1ET, editRefrance2ET,
            editRefrance3ET, editBankET/*, editQuantityET, editPriceET, editDescriptionET*/;
    TextView txtItemsET, txtdiscountET;
    Button btnPreview, btnSave, btnPaymentB, saveDraftBtn;
    String arrayVesselsType[], arrayStatus[], arrayCurrency[];
    InVoicesModel mInVoicesModel = new InVoicesModel();
    InvoiceAddItemModel mInvoiceAddItemModel = new InvoiceAddItemModel();
    CompaniesModel mCompaniesModel;
    StampsModel mStampsModel;
    SignatureModel mSignatureModel;
    BankModel mBankModel;
    VesselSearchInvoiceModel mVesselSearchModel;
    CurrenciesModel mCurrenciesModel;
    PaymentModel mPaymentModel = new PaymentModel();

    RecyclerView mRecyclerView, mRecyclerViewDiscount;
    NestedScrollView mNestedScrollView;
    InVoiceItemsAdapter mInVoiceItemsAdapter;
    InvoiceTotalDiscountAdapter mInvoiceTotalDiscountAdapter;
    ArrayList<PriviewModelItems> mPriviewModelItemArray = new ArrayList<PriviewModelItems>();
    ArrayList<InvoiceAddItemModel> itemsArrayList = new ArrayList<InvoiceAddItemModel>();
    EditText editSearchVesselsET, invoiceNumET;

    int mSelectedItemPosition;
    boolean isSelectedItemPosition = false;
    boolean isDiscountPosition = false;
    boolean isDiscountPositionNEW = false;
    static double mSubTotal1 = 0;
    static double mTotal1 = 0;
    static double mTotalASDisc1 = 0;
    long strInvoicenum;
    AddTotalInvoiceDiscountModel mTotalDiscountModel = new AddTotalInvoiceDiscountModel();
    AddTotalInvoiceDiscountModel mTotalDiscountModelNew;
    ArrayList<AddTotalInvoiceDiscountModel> mArrayTotalDiscount = new ArrayList<AddTotalInvoiceDiscountModel>();
    ArrayList<AddTotalInvoiceDiscountModel> mArrayTotalDiscountNEW;
    String strPAID = "", strVAt = "", strPaymentID = "";

    double mPaid = 0;
    double mBalanceDue = 0;
    double mTotal = 0, mVatCost = 0;
    double mSubTotal = 0;
    double intItems = 0;
    double intUnitPrice = 0;
    double mAmount = 0;
    String strVessalID = "";
    String strStamPId = "";
    String strSignatireId = "";
    String strBankId = "";
    String strCompanyId = "";

    EditInvoiceItemData mEditInvoiceItemData = new EditInvoiceItemData() {
        @Override
        public void editInvoiceItemData(InvoiceAddItemModel mModel, int position) {
            Intent mIntent = new Intent(mActivity, ShowEditInvoiceItemAciivity.class);
            mIntent.putExtra("Model", mModel);
            mIntent.putExtra("Position", position);
            mSelectedItemPosition = position;
            startActivityForResult(mIntent, 111);
        }
    };

    EditTotalDiscountItemdata mEditInvoiceDiscountItemData = new EditTotalDiscountItemdata() {
        @Override
        public void editTotalDiscountItemdata(AddTotalInvoiceDiscountModel mModel, int position, String strType) {
            for (int i = 0; i < itemsArrayList.size(); i++) {
                intItems = itemsArrayList.get(i).getQuantity();
                intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                if (i == 0) {
                    mAmount = intUnitPrice;
                    mSubTotal = mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                } else {
                    mAmount = intUnitPrice;
                    mSubTotal = mSubTotal + mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                    mModel.setADD_UnitPrice(mSubTotal);
                    mModel.setSubtract_UnitPrice(mSubTotal);
                }
            }
            Intent mIntent = new Intent(mActivity, EditTotalDiscountActivity.class);
            mIntent.putExtra("Model", mModel);
            mIntent.putExtra("Position", position);
            startActivityForResult(mIntent, 1212);
        }
    };

    DeleteItemInvoiceInterface mDeleteItemInvoiceInterface = new DeleteItemInvoiceInterface() {
        @Override
        public void deleteItemInvoice(InvoiceAddItemModel model) {
            if (Utilities.isNetworkAvailable(mActivity) == false) {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                showAlertDeleteDialog(mActivity, model);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_invoice);

        setStatusBar();

        arrayVesselsType = mActivity.getResources().getStringArray(R.array.vessel_type_array);
        arrayStatus = mActivity.getResources().getStringArray(R.array.status_array);
        arrayCurrency = mActivity.getResources().getStringArray(R.array.currency_array);

        setViews();
        setClickListn();

        IS_INVOICE_UPLOADED = false;
    }

    protected void setViews() {
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText("Add Invoice");
        saveDraftBtn = findViewById(R.id.saveDraftBtn);
        editInVoiceNumET = (EditText) findViewById(R.id.editInVoiceNumET);
        editInVoiceDateET = (EditText) findViewById(R.id.editInVoiceDateET);
        editInVoiceDateET.setKeyListener(null);
        editTermsDaysET = (EditText) findViewById(R.id.editTermsDaysET);
//        editTermsDaysET.setText("1");
        editSearchCompanyET = (EditText) findViewById(R.id.editSearchCompanyET);
        editSearchCompanyET.setKeyListener(null);
        editSearchVesselsET = (EditText) findViewById(R.id.editSearchVesselsET);
        editSearchVesselsET.setKeyListener(null);
        editCurrencyET = (EditText) findViewById(R.id.editCurrencyET);
        editCurrencyET.setKeyListener(null);
        editStampET = (EditText) findViewById(R.id.editStampET);
        editStampET.setKeyListener(null);
        editSignInvoiceET = (EditText) findViewById(R.id.editSignInvoiceET);
        editSignInvoiceET.setKeyListener(null);
        editStatusET = (EditText) findViewById(R.id.editStatusET);
        editStatusET.setKeyListener(null);

        editRefrance1ET = (EditText) findViewById(R.id.editRefrance1ET);
        editRefrance2ET = (EditText) findViewById(R.id.editRefrance2ET);
        editRefrance3ET = (EditText) findViewById(R.id.editRefrance3ET);

        editBankET = (EditText) findViewById(R.id.editBankET);
        invoiceNumET = (EditText) findViewById(R.id.invoiceNumET);
        editBankET.setKeyListener(null);

        txtItemsET = (TextView) findViewById(R.id.txtItemsET);
        txtdiscountET = (TextView) findViewById(R.id.txtdiscountET);

        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mRecyclerViewDiscount = (RecyclerView) findViewById(R.id.mRecyclerViewDiscount);
        mNestedScrollView = (NestedScrollView) findViewById(R.id.mNestedScrollView);
        btnPreview = (Button) findViewById(R.id.btnPreview);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnPaymentB = (Button) findViewById(R.id.btnPaymentB);
        gettingInvoiceNumber();
    }

    public void gettingInvoiceNumber() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<InvoiceModel> call1 = mApiInterface.getInvoiceNumberRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, retrofit2.Response<InvoiceModel> response) {
                AlertDialogManager.hideProgressDialog();
                InvoiceModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    strInvoicenum = Long.parseLong(mModel.getData());
                    invoiceNumET.setText(mModel.getData());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    protected void setClickListn() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        editInVoiceDateET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showInvoiceDatePicker(mActivity, editInVoiceDateET);
                    return true;
                }
                return false;
            }
        });

        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editSearchCompanyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent mIntent = new Intent(mActivity, SearchCompanyActivity.class);
                    startActivityForResult(mIntent, 222);

                    return true;
                }
                return false;
            }
        });

        editStampET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (editStampET.getText().toString().equals("")) {
                        Intent mIntent = new Intent(mActivity, StampsActivity.class);
                        startActivityForResult(mIntent, 333);
                    } else {
                        openDeleteDialog(editStampET, "stamp");
                    }
                    return true;
                }
                return false;
            }
        });

        editSignInvoiceET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (editSignInvoiceET.getText().toString().equals("")) {
                        Intent mIntent = new Intent(mActivity, SignatureActivity.class);
                        startActivityForResult(mIntent, 444);
                    } else {
                        openDeleteDialog(editSignInvoiceET, "sign");
                    }
                    return true;
                }
                return false;
            }
        });

        editBankET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent mIntent = new Intent(mActivity, AllBankDetailsActivity.class);
                    startActivityForResult(mIntent, 555);

                    return true;
                }
                return false;
            }
        });

        editSearchVesselsET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent mIntent = new Intent(mActivity, InvoiceVesselsActivity.class);
                    startActivityForResult(mIntent, 666);
                    return true;
                }
                return false;
            }
        });

        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    //AlertDialogManager.showSelectItemFromArray(mActivity, "Currency", arrayCurrency, editCurrencyET);
                    Intent mIntent = new Intent(mActivity, AllCurrencyActivity.class);
                    startActivityForResult(mIntent, 777);

                    return true;
                }
                return false;
            }
        });

        btnPaymentB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mArrayTotalDiscount.size() == 0) {
                    if (itemsArrayList.size() == 0) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                    } else {
                        Intent mIntent = new Intent(mActivity, PaymentActivity.class);
                        mIntent.putExtra("IsEdit", false);
                        mIntent.putExtra("mPaymnetModel", mPaymentModel);
                        mIntent.putExtra("mArrayList", itemsArrayList);
                        startActivityForResult(mIntent, 888);
                    }
                } else {
                    if (itemsArrayList.size() == 0) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                    } else {
                        if (isDiscountPositionNEW == true) {
                            Intent mIntent = new Intent(mActivity, PaymentActivity.class);
                            mIntent.putExtra("IsEdit", false);
                            mIntent.putExtra("ArrayList", mArrayTotalDiscountNEW);
                            mIntent.putExtra("mPaymnetModel", mPaymentModel);
                            startActivityForResult(mIntent, 888);
                        } else {
                            isDiscountPositionNEW = false;
                            Intent mIntent = new Intent(mActivity, PaymentActivity.class);
                            mIntent.putExtra("IsEdit", false);
                            mIntent.putExtra("ArrayList", mArrayTotalDiscount);
                            mIntent.putExtra("mPaymnetModel", mPaymentModel);
                            startActivityForResult(mIntent, 888);
                        }
                    }
                }
            }
        });

        btnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long InvoiceNum = Long.parseLong(invoiceNumET.getText().toString());
                if (InvoiceNum < strInvoicenum) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_Invoice));
                } else if (invoiceNumET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_Invoice_Num));
                } else if (editInVoiceDateET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_date));
                } else if (editTermsDaysET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_termdays));
                } else if (editSearchCompanyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_search_company));
                } else if (editCurrencyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_currency));
                } else if (itemsArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                } else if (editBankET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_bank_details));
                } else {
                    mInVoicesModel.setInvoice_number(invoiceNumET.getText().toString());
                    mInVoicesModel.setInvoice_date(editInVoiceDateET.getText().toString());
                    mInVoicesModel.setTerm_days(editTermsDaysET.getText().toString());
                    mInVoicesModel.setSearch_company(editSearchCompanyET.getText().toString());
                    mInVoicesModel.setSearch_vessel(editSearchVesselsET.getText().toString());
                    mInVoicesModel.setCurrency(editCurrencyET.getText().toString());
                    mInVoicesModel.setStamp(editStampET.getText().toString());
                    mInVoicesModel.setSign_invoice(editSignInvoiceET.getText().toString());
                    mInVoicesModel.setStatus(editStatusET.getText().toString());
                    mInVoicesModel.setRefrence1(editRefrance1ET.getText().toString());
                    mInVoicesModel.setRefrence2(editRefrance2ET.getText().toString());
                    mInVoicesModel.setRefrence3(editRefrance3ET.getText().toString());

                    if (JaoharConstants.IS_Payment_Update == true) {
                        JaoharConstants.IS_Payment_Update = false;
                        mInVoicesModel.setmPaymentModel(mPaymentModel);
                    } else {
                        if (mPaymentModel != null) {
                            if (!mPaymentModel.getSubTotal().equals("")) {
                                mSubTotal1 = Double.parseDouble(mPaymentModel.getSubTotal());
                                strPaymentID = mPaymentModel.getPayment_id();
                                if (mPaymentModel.getPaid() != null) {
                                    strPAID = mPaymentModel.getPaid().toString();
                                }
                                if (mPaymentModel.getVAT() != null) {
                                    strVAt = mPaymentModel.getVAT().toString();
                                }
                            }
                        }
                        if (itemsArrayList.size() != 0) {
                            double mAmount1 = 0;
                            for (int i = 0; i < itemsArrayList.size(); i++) {
                                if (i == 0) {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                } else {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mSubTotal1 + mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                }
                            }
                            String strValue = strVAt;
                            if (!strVAt.equals("")) {
                                if (strValue.length() > 0) {
                                    Float mVat = Float.parseFloat(strVAt);
                                    mVatCost = (mSubTotal1 * mVat) / 100;
                                    mTotal = mSubTotal1 + mVatCost;
                                    mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                    mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                    mBalanceDue = mTotal;
                                }
                            }
                            if (!strPAID.equals("")) {
                                String strValue1 = strPAID;
                                if (strValue1.length() > 0) {
                                    mPaid = Double.parseDouble(strPAID);
                                    mBalanceDue = mTotal - mPaid;
                                    mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                                }
                                Log.e(TAG, String.valueOf(mSubTotal1));
                            }

                        }
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                AddTotalInvoiceDiscountModel mDiscountModel = mArrayTotalDiscount.get(i);
                                if (mDiscountModel.getDiscountType().equals("Add Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Add Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                }
                                Log.e(TAG, "***Final Total***" + mTotalASDisc1);
                            }
                            mSubTotal1 = Utilities.getRoundOff2Decimal(mTotal1 + mTotalASDisc1);
                            mTotalASDisc1 = 0;
                            String strValue = strVAt;
                            if (strValue.length() > 0) {
                                Float mVat = Float.parseFloat(strVAt);
                                mVatCost = (mSubTotal1 * mVat) / 100;
                                mTotal = mSubTotal1 + mVatCost;
                                mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                mBalanceDue = mTotal;
                            }

                            String strValue1 = strPAID;
                            if (strValue1.length() > 0) {
                                mPaid = Double.parseDouble(strPAID);
                                mBalanceDue = mTotal - mPaid;
                                mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                            }
                            Log.e(TAG, String.valueOf(mSubTotal1));
                        }
                        mPaymentModel.setSubTotal("" + mSubTotal1);
                        mPaymentModel.setVAT(strVAt);
                        mPaymentModel.setPaid(strPAID);
                        if (mTotal == 0) {
                            mPaymentModel.setBalanceDue("" + mSubTotal1);
                            mPaymentModel.setTotal("" + mSubTotal1);
                        } else {
                            mPaymentModel.setBalanceDue("" + mBalanceDue);
                            mPaymentModel.setTotal("" + mTotal);
                        }
                        mPaymentModel.setVATPrice("" + mVatCost);
                        mPaymentModel.setPayment_id(strPaymentID);
                        mInVoicesModel.setmPaymentModel(mPaymentModel);
                    }

                    mPriviewModelItemArray.clear();
                    if (itemsArrayList.size() != 0) {
                        for (int i = 0; i < itemsArrayList.size(); i++) {
                            PriviewModelItems mModel = new PriviewModelItems();
                            double intItems = itemsArrayList.get(i).getQuantity();
                            double intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getUnitprice());
                            double mAmount = intItems * intUnitPrice;
                            mModel.setStrAmount(String.valueOf(mAmount));
                            mModel.setStrDescription(itemsArrayList.get(i).getDescription());
                            mModel.setStrItem(itemsArrayList.get(i).getItem());
                            mModel.setStrQuantity(String.valueOf(itemsArrayList.get(i).getQuantity()));
                            mModel.setStrUnitPrice(String.valueOf(itemsArrayList.get(i).getUnitprice()));
                            mPriviewModelItemArray.add(mModel);
                            for (int k = 0; k < itemsArrayList.get(i).getmDiscountModelArrayList().size(); k++) {
                                String strType = itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getType();
                                if (strType.equals("ADD")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    String finals = "+" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                    mModel1.setStrAmount(finals);
                                    mModel1.setStrUnitPrice(finals);
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("ADD_VALUE")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    String finals = "+" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                    mModel1.setStrAmount(finals);
                                    mModel1.setStrUnitPrice(finals);
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("SUBTRACT")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                        mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    } else {
                                        mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    }

                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("SUBTRACT_VALUE")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                        mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    } else {
                                        mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    }
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                    mPriviewModelItemArray.add(mModel1);
                                }
                            }
                        }
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                String strType = mArrayTotalDiscount.get(i).getDiscountType();
                                if (strType.equals("Add Percentage")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    mModel.setStrUnitPrice("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Subtract Percentage")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                        mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    } else {
                                        mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    }

                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Subtract Value")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                        mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    } else {
                                        mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    }

                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Add Value")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    mModel.setStrAmount("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrUnitPrice("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");

                                    mPriviewModelItemArray.add(mModel);
                                }
                            }
                        }
                        Log.e("TAGSIZE", String.valueOf(mPriviewModelItemArray.size()));
                        Intent mIntent = new Intent(mActivity, PreviewActivity.class);
                        mIntent.putExtra("Model", mInVoicesModel);
                        mIntent.putExtra("mPriviewModelArray", mPriviewModelItemArray);
                        mActivity.startActivity(mIntent);
                    }
                }
            }
        });

        txtItemsET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, AddInvoiceItemActivity.class);
                startActivityForResult(mIntent, 101);
            }
        });

        txtdiscountET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemsArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                } else {
                    Intent mIntent = new Intent(mActivity, AddTotalDisount.class);
                    mIntent.putExtra("mArraYList", itemsArrayList);
                    startActivityForResult(mIntent, 999);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long InvoiceNum = Long.parseLong(invoiceNumET.getText().toString());
                if (InvoiceNum < strInvoicenum) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_Invoice));
                } else if (invoiceNumET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_Invoice_Num));
                } else if (editInVoiceDateET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_date));
                } else if (editTermsDaysET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_termdays));
                } else if (editSearchCompanyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_search_company));
                } else if (editCurrencyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_currency));
                } else if (itemsArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                } else if (editBankET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_bank_details));
                } else {
                    mInVoicesModel.setInvoice_number(invoiceNumET.getText().toString());
                    mInVoicesModel.setInvoice_date(editInVoiceDateET.getText().toString());
                    mInVoicesModel.setTerm_days(editTermsDaysET.getText().toString());
                    mInVoicesModel.setSearch_company(editSearchCompanyET.getText().toString());
                    mInVoicesModel.setSearch_vessel(editSearchVesselsET.getText().toString());
                    mInVoicesModel.setCurrency(editCurrencyET.getText().toString());
                    mInVoicesModel.setStamp(editStampET.getText().toString());
                    mInVoicesModel.setSign_invoice(editSignInvoiceET.getText().toString());
                    mInVoicesModel.setStatus(editStatusET.getText().toString());
                    mInVoicesModel.setRefrence1(editRefrance1ET.getText().toString());
                    mInVoicesModel.setRefrence2(editRefrance2ET.getText().toString());
                    mInVoicesModel.setRefrence3(editRefrance3ET.getText().toString());

                    if (JaoharConstants.IS_Payment_Update == true) {
                        JaoharConstants.IS_Payment_Update = false;
                        mInVoicesModel.setmPaymentModel(mPaymentModel);
                    } else {
                        if (mPaymentModel != null) {
                            if (!mPaymentModel.getSubTotal().equals("")) {
                                mSubTotal1 = Double.parseDouble(mPaymentModel.getSubTotal());
                                strPaymentID = mPaymentModel.getPayment_id();
                                if (mPaymentModel.getPaid() != null) {
                                    strPAID = mPaymentModel.getPaid().toString();
                                }
                                if (mPaymentModel.getVAT() != null) {
                                    strVAt = mPaymentModel.getVAT().toString();
                                }
                            }
                        }
                        if (itemsArrayList.size() != 0) {
                            double mAmount1 = 0;
                            for (int i = 0; i < itemsArrayList.size(); i++) {
                                if (i == 0) {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                } else {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mSubTotal1 + mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                }
                            }
                            String strValue = strVAt;
                            if (!strVAt.equals("")) {
                                if (strValue.length() > 0) {
                                    Float mVat = Float.parseFloat(strVAt);
                                    mVatCost = (mSubTotal1 * mVat) / 100;
                                    mTotal = mSubTotal1 + mVatCost;
                                    mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                    mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                    mBalanceDue = mTotal;
                                }
                            }
                            if (!strPAID.equals("")) {
                                String strValue1 = strPAID;
                                if (strValue1.length() > 0) {
                                    mPaid = Double.parseDouble(strPAID);
                                    mBalanceDue = mTotal - mPaid;
                                    mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                                }
                                Log.e(TAG, String.valueOf(mSubTotal1));
                            }
                        }
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                AddTotalInvoiceDiscountModel mDiscountModel = mArrayTotalDiscount.get(i);
                                if (mDiscountModel.getDiscountType().equals("Add Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Add Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                }
                                Log.e(TAG, "***Final Total***" + mTotalASDisc1);
                            }
                            mSubTotal1 = Utilities.getRoundOff2Decimal(mTotal1 + mTotalASDisc1);
                            mTotalASDisc1 = 0;
                            String strValue = strVAt;
                            if (strValue.length() > 0) {
                                Float mVat = Float.parseFloat(strVAt);
                                mVatCost = (mSubTotal1 * mVat) / 100;
                                mTotal = mSubTotal1 + mVatCost;
                                mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                mBalanceDue = mTotal;
                            }

                            String strValue1 = strPAID;
                            if (strValue1.length() > 0) {
                                mPaid = Double.parseDouble(strPAID);
                                mBalanceDue = mTotal - mPaid;
                                mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                            }
                            Log.e(TAG, String.valueOf(mSubTotal1));
                        }
                        mPaymentModel.setSubTotal("" + mSubTotal1);
                        mPaymentModel.setVAT(strVAt);
                        mPaymentModel.setPaid(strPAID);
                        if (mTotal == 0) {
                            mPaymentModel.setBalanceDue("" + mSubTotal1);
                            mPaymentModel.setTotal("" + mSubTotal1);
                        } else {
                            mPaymentModel.setBalanceDue("" + mBalanceDue);
                            mPaymentModel.setTotal("" + mTotal);
                        }
                        mPaymentModel.setVATPrice("" + mVatCost);
                        mPaymentModel.setPayment_id(strPaymentID);
                        mInVoicesModel.setmPaymentModel(mPaymentModel);
                    }
                    mPriviewModelItemArray.clear();
                    if (itemsArrayList.size() != 0) {
                        for (int i = 0; i < itemsArrayList.size(); i++) {
                            PriviewModelItems mModel = new PriviewModelItems();
                            double intItems = itemsArrayList.get(i).getQuantity();
                            double intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getUnitprice());
                            double mAmount = intItems * intUnitPrice;
                            mModel.setStrAmount(String.valueOf(mAmount));
                            mModel.setStrDescription(itemsArrayList.get(i).getDescription());
                            mModel.setStrItem(itemsArrayList.get(i).getItem());
                            mModel.setStrQuantity(String.valueOf(itemsArrayList.get(i).getQuantity()));
                            mModel.setStrUnitPrice(String.valueOf(itemsArrayList.get(i).getUnitprice()));
                            mPriviewModelItemArray.add(mModel);
                            for (int k = 0; k < itemsArrayList.get(i).getmDiscountModelArrayList().size(); k++) {
                                String strType = itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getType();
                                if (strType.equals("ADD")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    String finals = "+" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                    mModel1.setStrAmount(finals);
                                    mModel1.setStrUnitPrice(finals);
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("ADD_VALUE")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    String finals = "+" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                    mModel1.setStrAmount(finals);
                                    mModel1.setStrUnitPrice(finals);
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("SUBTRACT")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                        mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    } else {
                                        mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    }

                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("SUBTRACT_VALUE")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                        mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    } else {
                                        mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    }
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                    mPriviewModelItemArray.add(mModel1);
                                }
                            }
                        }
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                String strType = mArrayTotalDiscount.get(i).getDiscountType();
                                if (strType.equals("Add Percentage")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    mModel.setStrUnitPrice("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Subtract Percentage")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                        mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    } else {
                                        mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    }

                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Subtract Value")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                        mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    } else {
                                        mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    }

                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Add Value")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    mModel.setStrAmount("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrUnitPrice("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");

                                    mPriviewModelItemArray.add(mModel);
                                }
                            }
                        }
                        JaoharConstants.IS_Click_From_Edit = false;
                        Log.e("TAGSIZE", String.valueOf(mPriviewModelItemArray.size()));
                        Intent mIntent = new Intent(mActivity, UploadInvoiceODF.class);
                        mIntent.putExtra("Model", mInVoicesModel);
                        mIntent.putExtra("mPriviewModelArray", mPriviewModelItemArray);
                        mIntent.putExtra("mItemModelArray", itemsArrayList);
                        mIntent.putExtra("mTotalDiscountModelArray", mArrayTotalDiscount);
                        mActivity.startActivity(mIntent);
                    }
                }
            }
        });

        saveDraftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JaoharConstants.IS_Click_From_DRAFT = true;
                JaoharConstants.IS_Click_From_Edit = false;
                long InvoiceNum = Long.parseLong(invoiceNumET.getText().toString());
                if (InvoiceNum < strInvoicenum) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_Invoice));
                } else if (invoiceNumET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_Invoice_Num));
                } else if (editInVoiceDateET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_date));
                } else if (editTermsDaysET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_termdays));
                } else if (editSearchCompanyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_search_company));
                } else if (editCurrencyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_currency));
                } else if (itemsArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                } else if (editBankET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_bank_details));
                } else {
                    mInVoicesModel.setInvoice_number(invoiceNumET.getText().toString());
                    mInVoicesModel.setInvoice_date(editInVoiceDateET.getText().toString());
                    mInVoicesModel.setTerm_days(editTermsDaysET.getText().toString());
                    mInVoicesModel.setSearch_company(editSearchCompanyET.getText().toString());
                    mInVoicesModel.setSearch_vessel(editSearchVesselsET.getText().toString());
                    mInVoicesModel.setCurrency(editCurrencyET.getText().toString());
                    mInVoicesModel.setStamp(editStampET.getText().toString());
                    mInVoicesModel.setSign_invoice(editSignInvoiceET.getText().toString());
                    mInVoicesModel.setStatus(editStatusET.getText().toString());
                    mInVoicesModel.setRefrence1(editRefrance1ET.getText().toString());
                    mInVoicesModel.setRefrence2(editRefrance2ET.getText().toString());
                    mInVoicesModel.setRefrence3(editRefrance3ET.getText().toString());

                    if (JaoharConstants.IS_Payment_Update == true) {
                        JaoharConstants.IS_Payment_Update = false;
                        mInVoicesModel.setmPaymentModel(mPaymentModel);
                    } else {
                        if (mPaymentModel != null) {
                            if (!mPaymentModel.getSubTotal().equals("")) {
                                mSubTotal1 = Double.parseDouble(mPaymentModel.getSubTotal());
                                strPaymentID = mPaymentModel.getPayment_id();
                                if (mPaymentModel.getPaid() != null) {
                                    strPAID = mPaymentModel.getPaid().toString();
                                }
                                if (mPaymentModel.getVAT() != null) {
                                    strVAt = mPaymentModel.getVAT().toString();
                                }
                            }
                        }
                        if (itemsArrayList.size() != 0) {
                            double mAmount1 = 0;
                            for (int i = 0; i < itemsArrayList.size(); i++) {
                                if (i == 0) {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                } else {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mSubTotal1 + mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                }
                            }
                            String strValue = strVAt;
                            if (!strVAt.equals("")) {
                                if (strValue.length() > 0) {
                                    Float mVat = Float.parseFloat(strVAt);
                                    mVatCost = (mSubTotal1 * mVat) / 100;
                                    mTotal = mSubTotal1 + mVatCost;
                                    mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                    mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                    mBalanceDue = mTotal;
                                }
                            }
                            if (!strPAID.equals("")) {
                                String strValue1 = strPAID;
                                if (strValue1.length() > 0) {
                                    mPaid = Double.parseDouble(strPAID);
                                    mBalanceDue = mTotal - mPaid;
                                    mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                                }
                                Log.e(TAG, String.valueOf(mSubTotal1));
                            }
                        }
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                AddTotalInvoiceDiscountModel mDiscountModel = mArrayTotalDiscount.get(i);
                                if (mDiscountModel.getDiscountType().equals("Add Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Add Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                }
                                Log.e(TAG, "***Final Total***" + mTotalASDisc1);
                            }
                            mSubTotal1 = Utilities.getRoundOff2Decimal(mTotal1 + mTotalASDisc1);
                            mTotalASDisc1 = 0;
                            String strValue = strVAt;
                            if (strValue.length() > 0) {
                                Float mVat = Float.parseFloat(strVAt);
                                mVatCost = (mSubTotal1 * mVat) / 100;
                                mTotal = mSubTotal1 + mVatCost;
                                mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                mBalanceDue = mTotal;
                            }

                            String strValue1 = strPAID;
                            if (strValue1.length() > 0) {
                                mPaid = Double.parseDouble(strPAID);
                                mBalanceDue = mTotal - mPaid;
                                mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
                            }
                            Log.e(TAG, String.valueOf(mSubTotal1));
                        }
                        mPaymentModel.setSubTotal("" + mSubTotal1);
                        mPaymentModel.setVAT(strVAt);
                        mPaymentModel.setPaid(strPAID);
                        if (mTotal == 0) {
                            mPaymentModel.setBalanceDue("" + mSubTotal1);
                            mPaymentModel.setTotal("" + mSubTotal1);
                        } else {
                            mPaymentModel.setBalanceDue("" + mBalanceDue);
                            mPaymentModel.setTotal("" + mTotal);
                        }
                        mPaymentModel.setVATPrice("" + mVatCost);
                        mPaymentModel.setPayment_id(strPaymentID);
                        mInVoicesModel.setmPaymentModel(mPaymentModel);
                    }
                    mPriviewModelItemArray.clear();
                    if (itemsArrayList.size() != 0) {
                        for (int i = 0; i < itemsArrayList.size(); i++) {
                            PriviewModelItems mModel = new PriviewModelItems();
                            double intItems = itemsArrayList.get(i).getQuantity();
                            double intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getUnitprice());
                            double mAmount = intItems * intUnitPrice;
                            mModel.setStrAmount(String.valueOf(mAmount));
                            mModel.setStrDescription(itemsArrayList.get(i).getDescription());
                            mModel.setStrItem(itemsArrayList.get(i).getItem());
                            mModel.setStrQuantity(String.valueOf(itemsArrayList.get(i).getQuantity()));
                            mModel.setStrUnitPrice(String.valueOf(itemsArrayList.get(i).getUnitprice()));
                            mPriviewModelItemArray.add(mModel);
                            for (int k = 0; k < itemsArrayList.get(i).getmDiscountModelArrayList().size(); k++) {
                                String strType = itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getType();
                                if (strType.equals("ADD")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    String finals = "+" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                    mModel1.setStrAmount(finals);
                                    mModel1.setStrUnitPrice(finals);
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("ADD_VALUE")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    String finals = "+" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                    mModel1.setStrAmount(finals);
                                    mModel1.setStrUnitPrice(finals);
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("SUBTRACT")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                        mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    } else {
                                        mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    }

                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                    mPriviewModelItemArray.add(mModel1);
                                } else if (strType.equals("SUBTRACT_VALUE")) {
                                    PriviewModelItems mModel1 = new PriviewModelItems();
                                    mModel1.setStrItem("w");
                                    mModel1.setStrQuantity("1");
                                    if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                        mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    } else {
                                        mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                        mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                    }
                                    mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                    mPriviewModelItemArray.add(mModel1);
                                }
                            }
                        }
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                String strType = mArrayTotalDiscount.get(i).getDiscountType();
                                if (strType.equals("Add Percentage")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    mModel.setStrUnitPrice("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Subtract Percentage")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                        mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    } else {
                                        mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    }

                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Subtract Value")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                        mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    } else {
                                        mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                        mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    }

                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");
                                    mPriviewModelItemArray.add(mModel);
                                } else if (strType.equals("Add Value")) {
                                    PriviewModelItems mModel = new PriviewModelItems();
                                    mModel.setStrAmount("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrUnitPrice("+" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                    mModel.setStrItem("w");
                                    mModel.setStrQuantity("1");

                                    mPriviewModelItemArray.add(mModel);
                                }
                            }
                        }
                        Log.e("TAGSIZE", String.valueOf(mPriviewModelItemArray.size()));
                        Intent mIntent = new Intent(mActivity, UploadInvoiceODF.class);
                        mIntent.putExtra("Model", mInVoicesModel);
                        mIntent.putExtra("mPriviewModelArray", mPriviewModelItemArray);
                        mIntent.putExtra("mItemModelArray", itemsArrayList);
                        mIntent.putExtra("mTotalDiscountModelArray", mArrayTotalDiscount);
                        mActivity.startActivity(mIntent);
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (IS_INVOICE_UPLOADED) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the Activity with an OK result
        if (requestCode == 101) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mInvoiceAddItemModel = (InvoiceAddItemModel) data.getSerializableExtra("Model");
                mInvoiceAddItemModel.setTotalAmount(String.valueOf(Double.parseDouble(mInvoiceAddItemModel.getUnitprice()) * mInvoiceAddItemModel.getQuantity()));

                if (!itemsArrayList.contains(mInvoiceAddItemModel))
                    itemsArrayList.add(mInvoiceAddItemModel);

                if (itemsArrayList.size() > 0) {
                    setAdapter();
                }

//              **************when item is Newly Added than overall discount Reffresh According to new data*************
                if (mArrayTotalDiscount.size() != 0) {
                    mArrayTotalDiscountNEW = new ArrayList<>();

                    for (int i = 0; i < itemsArrayList.size(); i++) {
                        intItems = itemsArrayList.get(i).getQuantity();
                        intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                        if (i == 0) {
                            mAmount = intUnitPrice;
                            mSubTotal = mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        } else {
                            mAmount = intUnitPrice;
                            mSubTotal = mSubTotal + mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        }
                    }
                    Log.e("TAg", String.valueOf(mSubTotal));
                    Log.e("TAg", String.valueOf(mInvoiceAddItemModel.getmDiscountModelArrayList().size()));
                    mArrayTotalDiscountNEW.clear();
                    for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                        isDiscountPosition = true;
                        mTotalDiscountModelNew = new AddTotalInvoiceDiscountModel();
                        String strType = mArrayTotalDiscount.get(i).getDiscountType();
                        if (strType.equals("Add Percentage")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);

                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(Double.parseDouble(Utilities.convertEvalueToNormal(sum)));
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);

                            setAdapterTotalDiscount();
                        } else if (strType.equals("Add Value")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = Double.parseDouble(str);

                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(Double.parseDouble(Utilities.convertEvalueToNormal(sum)));
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());

                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);

                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Percentage")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                            sum = basePrice - totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);

                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Value")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            float percentValue = Float.parseFloat(str);
                            double totalPercent;
                            totalPercent = basePrice - percentValue;
                            totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                            sum = totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);

                            setAdapterTotalDiscount();
                        }
                    }
                }
            }
        }

        if (requestCode == 111) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mInvoiceAddItemModel = (InvoiceAddItemModel) data.getSerializableExtra("Model");
                isSelectedItemPosition = true;
                itemsArrayList.set(mSelectedItemPosition, mInvoiceAddItemModel);
                if (itemsArrayList.size() > 0) {
                    setAdapter();
                }
//               ************** when item is edited  than overall discount Reffresh According to new data***************

                if (mArrayTotalDiscount.size() != 0) {
                    mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();

                    for (int i = 0; i < itemsArrayList.size(); i++) {
                        intItems = itemsArrayList.get(i).getQuantity();
                        intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                        if (i == 0) {
                            mAmount = intUnitPrice;
                            mSubTotal = mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        } else {
                            mAmount = intUnitPrice;
                            mSubTotal = mSubTotal + mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        }
                    }

                    Log.e("TAg", String.valueOf(mSubTotal));
                    Log.e("TAg", String.valueOf(mInvoiceAddItemModel.getmDiscountModelArrayList().size()));
                    for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                        isDiscountPosition = true;
                        mTotalDiscountModelNew = new AddTotalInvoiceDiscountModel();
                        String strType = mArrayTotalDiscount.get(i).getDiscountType();
                        if (strType.equals("Add Percentage")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        } else if (strType.equals("Add Value")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = Double.parseDouble(str);
                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Percentage")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                            sum = basePrice - totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Value")) {
                            double basePrice = 0;
                            double sum;
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            float percentValue = Float.parseFloat(str);
                            double totalPercent;
                            totalPercent = basePrice - percentValue;
                            totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                            sum = totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        }
                    }
                }
            }
        }

        if (requestCode == 222) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mCompaniesModel = (CompaniesModel) data.getSerializableExtra("Model");
                if (mCompaniesModel != null)
                    editSearchCompanyET.setText(mCompaniesModel.getCompany_name());
                mInVoicesModel.setmCompaniesModel(mCompaniesModel);
            }
        }

        if (requestCode == 333) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mStampsModel = (StampsModel) data.getSerializableExtra("Model");
                if (mStampsModel != null)
                    editStampET.setText(mStampsModel.getStamp_name());
                mInVoicesModel.setmStampsModel(mStampsModel);
            }
        }

        if (requestCode == 444) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mSignatureModel = (SignatureModel) data.getSerializableExtra("Model");
                if (mSignatureModel != null)
                    editSignInvoiceET.setText(mSignatureModel.getSignature_name());
                mInVoicesModel.setmSignatureModel(mSignatureModel);
            }
        }

        if (requestCode == 555) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mBankModel = (BankModel) data.getSerializableExtra("Model");
                if (mBankModel != null)
                    editBankET.setText(mBankModel.getBenificiary());
                mInVoicesModel.setmBankModel(mBankModel);
            }
        }

        if (requestCode == 666) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mVesselSearchModel = (VesselSearchInvoiceModel) data.getSerializableExtra("Model");
                if (mVesselSearchModel != null)
                    editSearchVesselsET.setText(mVesselSearchModel.getVessel_name());
                mInVoicesModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
            }
        }

        if (requestCode == 777) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mCurrenciesModel = (CurrenciesModel) data.getSerializableExtra("Model");
                if (mCurrenciesModel != null)
                    editCurrencyET.setText(mCurrenciesModel.getCurrency_name());
                mInVoicesModel.setmCurrenciesModel(mCurrenciesModel);
            }
        }

        if (requestCode == 888) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mPaymentModel = (PaymentModel) data.getSerializableExtra("Model");
                mInVoicesModel.setmPaymentModel(mPaymentModel);
            }
        }

        if (requestCode == 999) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mTotalDiscountModel = (AddTotalInvoiceDiscountModel) data.getSerializableExtra("Model");
                isDiscountPositionNEW = false;
                mArrayTotalDiscount.add(mTotalDiscountModel);
                if (mArrayTotalDiscount.size() > 0) {

                    setAdapterTotalDiscount();
                }
            }
        }

        if (requestCode == 1212) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mTotalDiscountModel = (AddTotalInvoiceDiscountModel) data.getSerializableExtra("Model");
                int postion = data.getIntExtra("Position", 0);
                mArrayTotalDiscount.set(postion, mTotalDiscountModel);
                if (mArrayTotalDiscount.size() > 0) {
                    setAdapterTotalDiscount();
                }
            }
        }
    }

    private void executeDeleteItemInvoice(final InvoiceAddItemModel model) {
        itemsArrayList.remove(model);
        if (itemsArrayList.size() > 0) {
            setAdapter();
        }

        // ***************Array List Refresh after deletion*************
        if (itemsArrayList.size() == 0) {
            mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();
            itemsArrayList.clear();
            mInVoiceItemsAdapter.notifyDataSetChanged();
            mArrayTotalDiscount.clear();
            if (mArrayTotalDiscountNEW.size() != 0) {
                isDiscountPosition = true;
                mArrayTotalDiscountNEW.clear();
                setAdapterTotalDiscount();
            } else {
                isDiscountPosition = false;
                setAdapterTotalDiscount();
            }

            mInvoiceTotalDiscountAdapter.notifyDataSetChanged();
        }

        // Discount Array list referesh when item is deleted According to new data
        if (mArrayTotalDiscount.size() != 0) {
            mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();

            for (int i = 0; i < itemsArrayList.size(); i++) {
                intItems = itemsArrayList.get(i).getQuantity();
                intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                if (i == 0) {
                    mAmount = intUnitPrice;
                    mSubTotal = mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                } else {
                    mAmount = intUnitPrice;
                    mSubTotal = mSubTotal + mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                }
            }

            if (mArrayTotalDiscount != null && mArrayTotalDiscount.size() != 0) {
                for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                    isDiscountPosition = true;
                    mTotalDiscountModelNew = new AddTotalInvoiceDiscountModel();
                    String strType = mArrayTotalDiscount.get(i).getDiscountType();
                    if (strType.equals("Add Percentage")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                        double totalPercent;
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice + totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                        mTotalDiscountModelNew.setADD_DiscountValue(str);
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    } else if (strType.equals("Add Value")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                        double totalPercent;
                        totalPercent = Double.parseDouble(str);
                        sum = basePrice + totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                        mTotalDiscountModelNew.setADD_DiscountValue(str);
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    } else if (strType.equals("Subtract Percentage")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                        double totalPercent;
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice - totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                        mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    } else if (strType.equals("Subtract Value")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                        float percentValue = Float.parseFloat(str);
                        double totalPercent;
                        totalPercent = basePrice - percentValue;
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                        mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                        mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    }
                }
            }
            setAdapterTotalDiscount();
        }
    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + itemsArrayList.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mInVoiceItemsAdapter = new InVoiceItemsAdapter(mActivity, itemsArrayList, mDeleteItemInvoiceInterface, mEditInvoiceItemData);
        mRecyclerView.setAdapter(mInVoiceItemsAdapter);
    }

    private void setAdapterTotalDiscount() {
        Log.e(TAG, "VesselsAdapter: " + mArrayTotalDiscount.size());
        mRecyclerViewDiscount.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewDiscount.setNestedScrollingEnabled(false);
        mRecyclerViewDiscount.setHasFixedSize(false);
        mRecyclerViewDiscount.setLayoutManager(layoutManager);
        if (isDiscountPosition == true) {
            isDiscountPosition = false;
            isDiscountPositionNEW = true;
            mInvoiceTotalDiscountAdapter = new InvoiceTotalDiscountAdapter(mActivity, mArrayTotalDiscountNEW, mEditInvoiceDiscountItemData);
        } else {
            isDiscountPositionNEW = false;
            mInvoiceTotalDiscountAdapter = new InvoiceTotalDiscountAdapter(mActivity, mArrayTotalDiscount, mEditInvoiceDiscountItemData);
        }
        mRecyclerViewDiscount.setAdapter(mInvoiceTotalDiscountAdapter);
    }

    private JSONArray gettingItemIDsArray(ArrayList<InvoiceAddItemModel> itemArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < itemArrayList.size(); i++) {
                JSONObject mJsonObject = new JSONObject();
                InvoiceAddItemModel mModel = itemArrayList.get(i);
                mJsonObject.put("serial_no", mModel.getItem());
                mJsonObject.put("quantity", mModel.getQuantity());
                mJsonObject.put("price", mModel.getUnitprice());
                mJsonObject.put("description", mModel.getDescription());
                mJsonObject.put("TotalAmount", mModel.getTotalAmount());
                mJsonObject.put("TotalunitPrice", mModel.getStrTotalunitPrice());
                mJsonObject.put("TotalAmountUnit", mModel.getStrTotalAmountUnit());
                mJsonObject.put("arraylistDiscount", getJSONArray(mModel.getmDiscountModelArrayList()));
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    private JSONArray gettingItemDiscounArray(ArrayList<AddTotalInvoiceDiscountModel> itemArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < itemArrayList.size(); i++) {
                JSONObject mJsonObject = new JSONObject();
                AddTotalInvoiceDiscountModel mModel = itemArrayList.get(i);
                mJsonObject.put("discount_add_value", mModel.getADD_DiscountValue());
                mJsonObject.put("discount_type", mModel.getDiscountType());
                mJsonObject.put("discount_add_description", mModel.getADD_Description());
                mJsonObject.put("discount_add_unitprice", mModel.getADD_UnitPrice());
                mJsonObject.put("discount_percent", mModel.getStrAddAndSubtracttotalPercent());
                mJsonObject.put("discount_total_value", mModel.getStrTotalvalue());
                mJsonObject.put("discount_add_and_subtract_total", mModel.getAddAndSubtractTotal());
                mJsonObject.put("discount_subtract_unitprice", mModel.getSubtract_UnitPrice());
                mJsonObject.put("discount_subtract_description", mModel.getSubtract_Description());
                mJsonObject.put("discount_subtract_value", mModel.getSubtract_DiscountValue());
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    private JSONArray getJSONArray(ArrayList<DiscountModel> mDiscountModelArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < mDiscountModelArrayList.size(); i++) {
                DiscountModel mDiscount = mDiscountModelArrayList.get(i);
                JSONObject mJsonObject = new JSONObject();
                mJsonObject.put("ADD_items", mDiscount.getADD_items());
                mJsonObject.put("ADD_quantity", mDiscount.getADD_quantity());
                mJsonObject.put("Add_description", mDiscount.getAdd_description());
                mJsonObject.put("ADD_unitPrice", mDiscount.getADD_unitPrice());
                mJsonObject.put("Add_Value", mDiscount.getAdd_Value());
                mJsonObject.put("Type", mDiscount.getType());
                mJsonObject.put("Subtract_items", mDiscount.getSubtract_items());
                mJsonObject.put("Subtract_quantity", mDiscount.getSubtract_quantity());
                mJsonObject.put("Subtract_description", mDiscount.getSubtract_description());
                mJsonObject.put("Subtract_unitPrice", mDiscount.getSubtract_unitPrice());
                mJsonObject.put("Subtract_Value", mDiscount.getSubtract_Value());
                mJsonObject.put("totalPercentValue", mDiscount.getStrtotalPercentValue());
                mJsonObject.put("addAndSubtractTotal", mDiscount.getAddAndSubtractTotal());
                mJsonObject.put("TotalEditAmount", mDiscount.getStrTotalEditAmount());
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent mIntent = new Intent(mActivity, AllInVoicesActivity.class);
                startActivity(mIntent);
                finish();
            }
        });
        alertDialog.show();
    }

    private void openDeleteDialog(final EditText editText, final String strName) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_stamp);
        TextView txt_Choose = (TextView) dialog.findViewById(R.id.txt_Choose);
        final TextView txt_Delete = (TextView) dialog.findViewById(R.id.txt_Delete);
        final TextView txt_Cancel = (TextView) dialog.findViewById(R.id.txt_Cancel);

        if (strName.equals("sign")) {
            txt_Choose.setText("Choose Signature");
            txt_Delete.setText("Delete Signature");
        } else if (strName.equals("stamp")) {
            txt_Choose.setText("Choose Stamp");
            txt_Delete.setText("Delete Stamp");
        }

        txt_Choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strName.equals("sign")) {
                    dialog.dismiss();
                    Intent mIntent = new Intent(mActivity, SignatureActivity.class);
                    startActivityForResult(mIntent, 444);
                } else if (strName.equals("stamp")) {
                    dialog.dismiss();
                    Intent mIntent = new Intent(mActivity, StampsActivity.class);
                    startActivityForResult(mIntent, 333);
                }
            }
        });

        txt_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strName.equals("sign")) {
                    mSignatureModel = null;
                    mInVoicesModel.setmSignatureModel(mSignatureModel);
                } else if (strName.equals("stamp")) {
                    mStampsModel = null;
                    mInVoicesModel.setmStampsModel(mStampsModel);
                }
                editText.setText("");
                dialog.dismiss();
            }
        });

        txt_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void showAlertDeleteDialog(Activity mActivity, final InvoiceAddItemModel model) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_confirmation);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtConfirm = (TextView) alertDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) alertDialog.findViewById(R.id.txtCacel);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);

        txtMessage.setText("Are you sure want to delete item?");

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeDeleteItemInvoice(model);
            }
        });
        alertDialog.show();
    }
}
